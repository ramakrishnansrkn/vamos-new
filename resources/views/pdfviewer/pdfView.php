<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title><?php echo Lang::get('content.gps'); ?></title>

<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link href="../app/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
<link href="../app/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap.min.css" rel="stylesheet">
<script src="../app/views/pdfviewer/pdf.compat.js"></script>
<script src="../app/views/pdfviewer/pdf.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
<script src="../app/views/pdfviewer/ng-pdfviewer.js"></script>
   
<style>
body {
      font-family: 'Lato', sans-serif;
   /* font-weight: bold; */  
   /* font-family: 'Lato', sans-serif;
      font-family: 'Roboto', sans-serif;
      font-family: 'Open Sans', sans-serif;
      font-family: 'Raleway', sans-serif;
      font-family: 'Faustina', serif;
      font-family: 'PT Sans', sans-serif;
      font-family: 'Ubuntu', sans-serif;
      font-family: 'Droid Sans', sans-serif;
      font-family: 'Source Sans Pro', sans-serif;
      */
  }
.empty{
    height: 1px; width: 1px; padding-right: 30px; float: left;
}
.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
background-color: #ffffff;
}

</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2X3F316479');
</script>
</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
    <div ng-controller = "mainCtrl" class="ng-cloak">
        <div id="wrapper-site">
            <div id="sidebar-wrapper-site" style="min-width:92px;">
                <ul class="sidebar-nav" ">
                    <li class="sidebar-brand"><a href="javascript:void(0);"><img id="imagesrc" src=""/></i></a></li>
                    <li class="track"><a href="../public/track"><div></div><label><?php echo Lang::get('content.track'); ?></label></a></li>
                    <!-- <li class="history"><a href="../public/track?maps=replay"><div></div><label>History</label></a></li> -->
                    <li class="alert01"><a href="../public/reports"><div></div><label><?php echo Lang::get('content.reports'); ?></label></a></li>
                    <li class="stastics"><a href="../public/statistics?ind=1"><div></div><label><?php echo Lang::get('content.statistics'); ?></label></a></li>
                    <li class="admin"><a href="../public/settings"><div></div><label><?php echo Lang::get('content.scheduled'); ?></label></a></li>
                    <li class="fms"><a href="../public/fleetManagement?fms=FleetManagement"><div></div><label><?php echo Lang::get('content.FMS'); ?></label></a></li>
                    <li class="ReleaseNotes"><a ng-href="pdfView" class="active"><div></div><label>Release</br> Notes</label></a></li>
                    <li><a href="../public/logout"><img src="assets/imgs/logout.png"/></a></li>         
                </ul>
                <!-- <ul class="sidebar-subnav" style="max-height: 100vh; overflow-y: auto;" ng-init="vehicleStatus='ALL'">
                    <li style="margin-bottom: 15px;"><div class="right-inner-addon" align="center"><i class="fa fa-search"></i><input type="search" class="form-control" placeholder="Search" ng-model="searchbox" name="search" /></div>
                    </li>
                    <li ng-repeat="location in locations02"><a href="javascript:void(0);" ng-click="groupSelection(location.group, location.rowId)" ng-cloak >{{trimColon(location.group)}}</a>
                        <ul class="nav nav-second-level" style="max-height: 400px; overflow-y: auto;">
                            <li ng-repeat="loc in location.vehicleLocations | filter:searchbox"><a href="" ng-class="{active: $index == selected, red:loc.status=='OFF'}" ng-click="genericFunction(loc.vehicleId, $index, loc.shortName)"><img ng-src="assets/imgs/{{loc.vehicleType}}.png" fall-back-src="assets/imgs/Car.png" width="16" height="16"/><span>{{loc.shortName}}</span></a></li>

                        </ul>
                    </li>
                </ul> -->
            </div>
            <div id="testLoad"></div>
        
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default">
                 
                </div>   
            </div>
        </div>
    
      <div class="row" style="margin-left: 0px;" ng-show="FileNames.length!=0">
         
        <!-- <div class="col-md-9" style="border-color: unset!important;margin-top: -80px;">
        <pdfviewer src="https://sgp1.digitaloceanspaces.com/vamotest/ReleaseNotes{{pdfURL}}" on-page-load='pageLoaded(page,total)' id="viewer" load-progress='loadProgress(loaded, total, state)'></pdfviewer>
        </div> -->
        <div class="col-md-4">
        </div>
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-heading" align="center">RELEASE NOTES</div>
            <div class="panel-body" style="background-color: #787e84;">
              <div class="list-group">
                
                <a ng-repeat="files in FileNames | limitTo:quantity" " class="list-group-item" ng-click="changeReleaseNotes(files)" style="cursor: pointer;" ng-class="{active:pdfURL ==files}" href ="https://sgp1.digitaloceanspaces.com/vamotest/ReleaseNotes/{{files}}" target="blank">
                  <span style="text-align: center;">{{files}}</span>
                </a>
                  <a class="list-group-item"  style="cursor: pointer;" ng-click="changeQuantity('more')" ng-show="!less&&quantity>10">
                    <span>  View More &#x25BC;</span>
                  </a>
                  <a class="list-group-item"  style="cursor: pointer;"  ng-click="changeQuantity('less')" ng-show="less&&quantity>10">
                    <span>  View Less &#x25B2;</span>
                  </a>
              </div>
            </div>
          </div>
          <div class="col-md-4">
        </div>
          <!-- <div class="row" style="
  position: fixed;
   right: 40px;
   bottom: 5px;"> -->
        <!-- <div class="btn-group">
          
        </div> -->

       <!--  Page <span class="label" ng-show="totalPages">{{currentPage}}/{{totalPages}}</span>
      
        <div class="btn-group" >
          <button class="btn" ng-click="gotoPage(1)" title="first page">|&lt;</button>
          <button class="btn" ng-click="prevPage()" title="prev">&lt;</button>
          <button class="btn" ng-click="nextPage()" title="next">&gt;</button>
          <button class="btn" ng-click="gotoPage(totalPages)" title="last page">&gt;|</button>
          <button class="btn" ng-click="zoom('zoomOut')" title=" Font decrease" style="margin-left: 20px;">-</button>
          <button class="btn" ng-click="zoom('zoomIn')" title=" Font increase">+</button>
        </div>
      </div> -->
        </div>
      </div>
      <div ng-show="FileNames.length==0" class="err" style="text-align: center;">
                           <h5>There is No Release Notes </h5>
                      </div>
   

    <script src="assets/js/static.js"></script>
    <script src="assets/js/jquery-1.11.0.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script> -->
    <script src="../app/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script> -->
    <script src="assets/js/angular-translate.js"></script>
    <script src="../app/views/reports/customjs/html5csv.js"></script>
    <script src="../app/views/reports/customjs/moment.js"></script>
    <script src="../app/views/reports/customjs/FileSaver.js"></script>
    <script src="../app/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="../app/views/reports/datatable/jquery.dataTables.js"></script>
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/services.js"></script>
    <script src="assets/js/pdfView.js?v=<?php echo Config::get('app.version');?>"></script>
    
    <script>

   
        $("#example1").dataTable();
          
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        
        $(function () {
                $('#dateFrom, #dateTo').datetimepicker({
                    format:'YYYY-MM-DD',
                    useCurrent:true,
                    pickTime: false
                });
                $('#timeFrom').datetimepicker({
                    pickDate: false,
                    
                });
                $('#timeTo').datetimepicker({
                    pickDate: false,
                    
                });
        });      
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });


  </script>
    
</body>
</html>
