<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title><?php echo Lang::get('content.gps'); ?></title>

<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">

<link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">

<style>
.feeds li .col1>.cont {
    float: left;
    margin-right: 75px;
    overflow: hidden;
}
.feeds li .col1, .feeds li .col1>.cont>.cont-col2 {
    width: 98%;
    float: left;
}
.feeds li .col2 {
    float: left;
    width: 75px;
    margin-left: -105px;
}
.feeds li .col3 {
    float: left;
}
.feeds li .col1 {
    clear: both;
}
.feeds li .col1 {
    color: #82949a;
}
.feeds li .col2>.date {
    padding: 4px 9px 5px 4px;
    text-align: right;
    font-style: italic;
    color: #c1cbd0;
}


body {
      font-family: 'Lato', sans-serif;
   /* font-weight: bold; */  
   /* font-family: 'Lato', sans-serif;
      font-family: 'Roboto', sans-serif;
      font-family: 'Open Sans', sans-serif;
      font-family: 'Raleway', sans-serif;
      font-family: 'Faustina', serif;
      font-family: 'PT Sans', sans-serif;
      font-family: 'Ubuntu', sans-serif;
      font-family: 'Droid Sans', sans-serif;
      font-family: 'Source Sans Pro', sans-serif;
      */
  }
.empty{
    height: 1px; width: 1px; padding-right: 30px; float: left;
}
.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
background-color: #ffffff;
}

</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2X3F316479');
</script>
</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
    <div ng-controller="mainCtrl" class="ng-cloak">
      <div id="wrapper">
     <?php include('sidebarList.php');?> 
        
        <div id="testLoad"></div>
        
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default">
                 
                </div>   
            </div>
        </div>
      <div class="col-md-12" style="top:0px;">

       <div class="box box-primary" style="padding-top: 5px;margin-top: -20px;padding-left: 10px;box-shadow: unset;">
        <!-- <div class="row"> -->

                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
                    <h3 class="box-title"><?php echo Lang::get('content.notifications'); ?></h3>
                </div>
                 
                  <div class="right-inner-addon pull-right" align="center" style="padding-left:25px;width: 18%;margin-top: 15px;margin-right: 10px;" > 
                    <i class="fa fa-search"></i>
                    <input type="search" class="form-control" placeholder="Search" ng-model="notnsearch" name="search" />
                  </div>
                  <div class="nav-tabs-custom" style="margin-top: 10px;">
                        <!-- <div class="caption">
                            <i class="icon-globe font-dark hide"></i>
                            <span class="caption-subject font-dark bold uppercase">Feeds</span>
                        </div> -->
                        <ul class="nav nav-tabs" style="border-bottom-color: white;">
                            <li class="active">
                                <a href="#tab_1_1" class="active" data-toggle="tab"> <?php echo Lang::get('content.parked'); ?> <span class="badge badge-default" >{{parked_list.length}}</span></a>
                            </li>
                            <li>
                                <a href="#tab_1_2" data-toggle="tab"> <?php echo Lang::get('content.ign'); ?> 
                                    <span class="badge badge-default" >
                                {{ignition_list.length}} </span></a>
                            </li>
                            <li>
                                <a href="#tab_1_3" data-toggle="tab"> <?php echo Lang::get('content.idle'); ?> 
                                    <span class="badge badge-default" >
                                {{idle_list.length}} </span></a>
                            </li>
                            <li>
                                <a href="#tab_1_4" data-toggle="tab" > <?php echo Lang::get('content.sos'); ?> 
                                    <span class="badge badge-default" ng-if="sosAlarmClicked">
                                {{sos_list.length}} </span></a>
                            </li>
                        </ul>
                    </div>
                    <div class="box-body">
                        <!--BEGIN TABS-->
                        <div class="tab-content" style="margin-top: -10px;">
                            
                            <div class="tab-pane active" id="tab_1_1">
                                    <ul class="feeds" style="list-style-type: none;">
                                         <li ng-if="parked_list.length==0" style="color: #fb1f27; text-align:center;font-size: 16px;font-weight: 500; line-height: 1.1;"><?php echo Lang::get('content.no_notification'); ?></li>
                                        <li ng-if="parked_list.length!=0" ng-repeat="SingleParkedList in parked_list  | filter:notnsearch" style="margin-top: 5px;">
                                            <div class="col1">
                                                <div class="cont" style="margin-bottom: 10px;">
                                                   
                                                    <div class="cont-col2">
                                                        <div class="desc"> 
                                                           <div class="label label-sm label-default">
                                                               <i class=" fa fa-map-marker" aria-hidden="true"></i>
                                                            </div>
                                                          &nbsp;&nbsp;
                                                        <b>{{SingleParkedList.status}} :</b>
                                                        <font color= #87CEFA>{{SingleParkedList.vehicleName }}</font>
                                                         <font color=#C0C0C0>| Data & Time :</font>
                                                            {{SingleParkedList.dateTime }}
                                                           <font color="meroon">|  Location : </font>
                                                            {{SingleParkedList.location }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> {{SingleParkedList.duration }}</div>
                                            </div>
                                             <div class="col3">
                                                <div> <span class="glyphicon glyphicon-trash" ng-click="deleteNotn($index,'parked')" style="top: 5px;cursor: pointer;"></span></div>
                                            </div>
                                        </li>
                                    </ul>
                            </div>
                            <div class="tab-pane" id="tab_1_2">
                                    <ul class="feeds" style="list-style-type: none;">
                                        <li ng-if="ignition_list.length==0" style="color: #fb1f27; text-align:center;font-size: 16px;font-weight: 500; line-height: 1.1;"><?php echo Lang::get('content.no_notification'); ?></li>
                                         <li ng-if="ignition_list.length!=0" ng-repeat="SingleIgnitionList in ignition_list | filter:notnsearch">
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col2">
                                                        <div class="desc" style="margin-bottom: 25px;">
                                                          <span ng-class="(SingleIgnitionList.status == 'Ignition ON'); ?>?'label label-sm label-success':'label label-sm label-danger'">
                                                              <i class="fa fa-power-off"></i>
                                                          </span> 
                                                        <b>{{SingleIgnitionList.status}} :</b>
                                                        <font color=#87CEFA >{{SingleIgnitionList.vehicleName }}</font>
                                                         <font color=#C0C0C0> | Data & Time :</font>
                                                            {{SingleIgnitionList.dateTime }}
                                                           <font color="meroon"> | Location : </font>
                                                            {{SingleIgnitionList.location }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> {{SingleIgnitionList.duration }} </div>
                                            </div>
                                            <div class="col3">
                                                <div> <span class="glyphicon glyphicon-trash" ng-click="deleteNotn($index,'ign')" style="top: 5px;cursor: pointer;"></span></div>
                                            </div>
                                        </li>
                                    </ul>
                            </div>
                             <div class="tab-pane" id="tab_1_3">
                                    <ul class="feeds" style="list-style-type: none;">
                                         <li ng-if="idle_list.length==0" style="color: #fb1f27; text-align:center;font-size: 16px;font-weight: 500; line-height: 1.1;"><?php echo Lang::get('content.no_notification'); ?></li>
                                        <li ng-if="idle_list.length!=0" ng-repeat="SingleIdleList in idle_list  | filter:notnsearch" style="margin-top: 5px;">
                                            <div class="col1">
                                                <div class="cont" style="margin-bottom: 20px;">
                                                   
                                                    <div class="cont-col2">
                                                        <div class="desc"> 
                                                           <div class="label label-sm label-default">
                                                               <i class="fa fa-clock-o" aria-hidden="true"></i>
                                                            </div>
                                                          &nbsp;&nbsp;
                                                        <b>{{SingleIdleList.status}} :</b>
                                                        <font color= #87CEFA>{{SingleIdleList.vehicleName }}</font>
                                                         <font color=#C0C0C0>| Data & Time :</font>
                                                            {{SingleIdleList.dateTime }}
                                                           <font color="meroon">|  Location : </font>
                                                            {{SingleIdleList.location }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col2">
                                                <div class="date"> {{SingleIdleList.duration }}</div>
                                            </div>
                                             <div class="col3">
                                                <div> <span class="glyphicon glyphicon-trash" ng-click="deleteNotn($index,'idle')" style="top: 5px;cursor: pointer;"></span></div>
                                            </div>
                                        </li>
                                    </ul>
                            </div>
                            <div class="tab-pane" id="tab_1_4">
                                    <ul class="feeds" style="list-style-type: none;">
                                         <li ng-if="sos_list.length==0" style="color: #fb1f27; text-align:center;font-size: 16px;font-weight: 500; line-height: 1.1;"><?php echo Lang::get('content.no_notification'); ?></li>
                                        <li ng-if="sos_list.length!=0" ng-repeat="SingleSOSList in sos_list  | filter:notnsearch" style="margin-top: 5px;">
                                            <div class="col1">
                                                <div class="cont" style="margin-bottom: 20px;">
                                                   
                                                    <div class="cont-col2">
                                                        <div class="desc"> 
                                                           <div class="label label-sm">
                                                            <img style="cursor: pointer;width: 35px;height: 35px;" src="assets/imgs/sos.png" />
                                                            </div>
                                                          &nbsp;&nbsp;
                                                        <b>{{SingleSOSList.alarmType}} &nbsp;&nbsp;:</b>
                                                        <font color= #87CEFA>&nbsp;&nbsp;{{shortNam }}</font>
                                                         <font color=#C0C0C0>| Data & Time :</font>
                                                            {{SingleSOSList.alarmTime | date:'yyyy-MM-dd HH:mm:ss' }}
                                                           <font color="meroon">|  Location : </font>
                                                            {{SingleSOSList.address }}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="col2">
                                                <div class="date"> {{SingleSOSList.alarmTime }}</div>
                                            </div> -->
                                             <div class="col3">
                                                <div> <span class="glyphicon glyphicon-trash" ng-click="deleteNotn($index,'sos')" style="top: 5px;cursor: pointer;"></span></div>
                                            </div>
                                        </li>
                                    </ul>
                            </div>
                            </div>
                        <!--END TABS-->
                    </div>
                </div>
                <!-- END BOX-->
            </div>
        </div>

    

    <script src="assets/js/static.js"></script>
    <script src="assets/js/jquery-1.11.0.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
    <script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="assets/js/angular-translate.js"></script>
    <script src="../resources/views/reports/customjs/html5csv.js"></script>
    <script src="../resources/views/reports/customjs/moment.js"></script>
    <script src="../resources/views/reports/customjs/FileSaver.js"></script>
    <script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="../resources/views/reports/datatable/jquery.dataTables.js"></script>
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/services.js"></script>
    <script src="assets/js/notification.js?v=<?php echo Config::get('app.version');?>"></script>
    
    
    <script>

   
        $("#example1").dataTable();
          
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        
        $(function () {
                $('#dateFrom, #dateTo').datetimepicker({
                    format:'YYYY-MM-DD',
                    useCurrent:true,
                    pickTime: false
                });
                $('#timeFrom').datetimepicker({
                    pickDate: false,
                    
                });
                $('#timeTo').datetimepicker({
                    pickDate: false,
                    
                });
        });      
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });


  </script>
    
</body>
</html>
