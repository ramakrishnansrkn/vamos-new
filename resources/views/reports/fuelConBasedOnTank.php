<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title><?php echo Lang::get('content.gps'); ?></title> 
<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
<link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
<style>

body {
      font-family: 'Lato', sans-serif;
   /* font-weight: bold; */  
   /* font-family: 'Lato', sans-serif;
      font-family: 'Roboto', sans-serif;
      font-family: 'Open Sans', sans-serif;
      font-family: 'Raleway', sans-serif;
      font-family: 'Faustina', serif;
      font-family: 'PT Sans', sans-serif;
      font-family: 'Ubuntu', sans-serif;
      font-family: 'Droid Sans', sans-serif;
      font-family: 'Source Sans Pro', sans-serif; */
}

.empty {
   height: 1px; width: 1px; padding-right: 30px; float: left;
}

.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
   background-color: #ffffff;
}

.tableId > tbody > tr > td {
   font-weight: unset !important;
}

</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2X3F316479');
</script>
</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
    <div ng-controller="mainCtrl" class="ng-cloak">
      <div id="wrapper">
        <?php include('sidebarList.php');?> 
        
        <div id="testLoad"></div>
        
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default">
                 
                </div>   
            </div>
        </div>

    <div class="col-md-12">
       <div class="box box-primary" style="padding-top: 5px;margin-top: 10px;">
        <!-- <div class="row"> -->
                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
                    <h3 class="box-title"> <?php echo Lang::get('content.tank_based'); ?> <?php echo Lang::get('content.fuel_consolidated'); ?> </h3>
                </div>
               <div class="col-md-3" align="center"></div>
               <div class="row">
                    <div class="col-md-1" align="center"></div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">

                           <div style="margin-top: 8px;"><?php echo Lang::get('content.select'); ?> <?php echo Lang::get('content.date'); ?>:</div>
                        </div>                       
                    </div>
                 
                    <div class="col-md-2" align="center">
                        <div class="form-group">

                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.fromdate" class="form-control placholdercolor" id="dateFrom"  placeholder="From date">
                                <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                            </div>
                        </div>                       
                    </div>
 <!--                   <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.fromtime" class="form-control placholdercolor" id="timeFrom" placeholder="From time">
                                <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
<!--                            </div>
                        </div>
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.todate" class="form-control placholdercolor" id="dateTo" placeholder="From date">
                                <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
<!--                            </div>
                        </div>
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.totime" class="form-control placholdercolor" id="timeTo" placeholder="From time">
                                <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
<!--                            </div>
                        </div>
                    </div> -->
                    <!--
                    <div class="col-md-1" align="center">
                        <div class="form-group">
                            
                                <select class="input-sm form-control" ng-model="interval">
                                     <option value="">Interval</option>
                                     <option label="1 mins">1</option>
                                     <option label="2 mins">2</option>
                                     <option label="5 mins">5</option>
                                     <option label="10 mins">10</option>
                                     <option label="15 mins">15</option>
                                     <option label="30 mins">30</option>
                                </select>
                           
                        </div>
                    </div>
                    -->
                    <div class="col-md-1" align="center"></div>
                    <div class="col-md-1" align="center">
                        <button style="margin-left: -100%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
                    </div>
                </div>

              <!--  </div> -->
              <div class="row">
              <div class="col-md-1" align="center"></div>

               <div class="col-md-2" align="center">
                        <div class="form-group">

                         </div>
                  </div>
              </div>

            </div>
        </div>

        <div class="col-md-12">
            <div class="box box-primary" style="min-height:570px;">            
          
                   <div class="pull-right" style="margin-top: 10px;margin-right: 5px;">
                    <!-- <button type="button" class="btn btn-success" ng-click="durationFilter('today')" ng-disabled="todayDisabled"><?php echo Lang::get('content.today'); ?></button>  -->
                    <button type="button" class="btn btn-primary" ng-click="durationFilter('yesterday')" ng-disabled="yesterdayDisabled"><?php echo Lang::get('content.yesterday'); ?></button>
                    <!-- <button type="button" class="btn btn-success" ng-click="durationFilter('lastweek')" ng-disabled="weekDisabled">Last Week</button>
                    <button type="button" class="btn btn-info" ng-click="durationFilter('month')" ng-disabled="monthDisabled">Month</button> -->
                                       
                        <img style="cursor: pointer;" ng-click="exportData('FuelConBasedOnTank')"  src="../resources/views/reports/image/xls.png" />
                        <img style="cursor: pointer;" ng-click="exportDataCSV('FuelConBasedOnTank')"  src="../resources/views/reports/image/csv.jpeg" />
                         <img style="cursor: pointer;" onclick="generatePDF()"   src="../resources/views/reports/image/Adobe.png" />
                    </div>            
              
                 <div class="box-body" id="FuelConBasedOnTank1">

                          <p style="margin-left: 60px;"><span><b><?php echo Lang::get('content.organization'); ?> <?php echo Lang::get('content.name'); ?> :<b> {{orgName}}</span> &nbsp;&nbsp;&nbsp;&nbsp;<span style="margin-left: 40px;"><b><?php echo Lang::get('content.date'); ?></b> : &nbsp;{{uiDate.fromdate}}</span> </p>
                            <div id="">
                            <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top:20px;">
                              <thead>
                                                     
                                <tr style="text-align:center;">
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.total_fuel_consumption'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{roundOffDecimal(fuelConsumption)}}</th>
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.tot_fuel_fill'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{fuelFilling}}</th>
                                </tr>
                                <tr style="text-align:center;">                   
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total_distance'); ?></th>
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{parseInts(dist)}}</th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_theft'); ?></th>
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelTheft}}</th>
                                </tr>
                                <tr style="text-align:center;">
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.tot_engine_idle_hrs'); ?></th>
                                    <th ng-if="engineIdleHrs != 'NaN'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{msToTime2(engineIdleHrs)}}</th>
                                    <th ng-if="engineIdleHrs == 'NaN'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">-</th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.tot_engine_hrs'); ?></th>
                                    <th ng-if="ignitionHrs != 'NaN'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{msToTime2(ignitionHrs)}}</th>
                                    <th ng-if="ignitionHrs == 'NaN'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">-</th>
                                </tr>
                                <tr style="text-align:center;">
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.KMPL'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{totalKmpl(dist,fuelConsumption)}}</th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.LTPH'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{totalLtph(fuelConsumption,ignitionHrs)}}</th> 
                                </tr>

                              </thead>
                           </table>   
                           <div style="overflow-y: auto;">
                           <table class="table table-bordered table-condensed table-hover table-fixed-header" style="margin-top:20px;">                                          
                                  <thead class='header' style=" z-index: 1;">                         
                                      <tr>
                                        <th class="id" custom-sort order="'vehicleName'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.veh_name'); ?></b></th>
                                        <th class="id" custom-sort order="'orgId'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.organization'); ?> <?php echo Lang::get('content.name'); ?></b></th>
                                        <th class="id" custom-sort order="'nTanks'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.tank'); ?></b></th>
                                        <th class="id" custom-sort order="'startFuel'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.start_fuel'); ?></b></th>
                                        <th class="id" custom-sort order="'endFuel'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.end_fuel'); ?></b></th>
                                        <th class="id" custom-sort order="'fuelConsumption'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_consumption'); ?></b></th>
                                        <th class="id" custom-sort order="'fuelFilling'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_filling'); ?></b></th>
                                        <th class="id" custom-sort order="'fuelTheft'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_theft'); ?></b></th>
                                        <th class="id" custom-sort order="'startKms'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.start_kms'); ?></b></th>
                                        <th class="id" custom-sort order="'endKms'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.end_kms'); ?></b></th>
                                        <th class="id" custom-sort order="'dist'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.dis_travelled'); ?></b></th>
                                        <th class="id" custom-sort order="'kmpl'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.kmpl'); ?></b></th>   
                                   <!-- <th style="font-size:12px;background-color:#ecf7fb;"><b>Engine Start Hrs</b></th>
                                        <th style="font-size:12px;background-color:#ecf7fb;"><b>Engine End Hrs</b></th>-->
                                        <th style="font-size:12px;background-color:#ecf7fb;white-space: nowrap;"><b><?php echo Lang::get('content.running_hrs'); ?></b></th>
                                        <th style="font-size:12px;background-color:#ecf7fb;white-space: nowrap;"><b><?php echo Lang::get('content.eng_idle_hrs'); ?></b></th>
                                        <th class="id" custom-sort order="'ltrsPerHrs'" sort="sort"  style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.ltrs_per_hrs'); ?></b></th>   
                                        <th style="font-size:12px;background-color:#ecf7fb;white-space: nowrap;"><b><?php echo Lang::get('content.eng_hrs'); ?></b></th> 
                                        <th style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.secondary_engine_hrs'); ?></b></th> 
                                         <th style="font-size:12px;background-color:#ecf7fb;" class="addr"><b>Start Address</b></th>   
                                        <th style="font-size:12px;background-color:#ecf7fb;" class="addr"><b>End Address</b></th>
                                        <th style="font-size:12px;background-color:#ecf7fb;" class="loc"><b><?php echo Lang::get('content.start_loc'); ?></b></th>   
                                        <th style="font-size:12px;background-color:#ecf7fb;" class="loc"><b><?php echo Lang::get('content.end_loc'); ?></b></th>
                                        <th style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.dri_name'); ?></b></th>
                                        <th style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.dri_mob'); ?></b></th>
										                    <th style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.remarks'); ?></b></th>                              
                                      </tr>
                                  </thead>

                                   <tbody>

                                   <span id="tableId">
                                     
                                      <tr ng-if="!conFuelError" ng-repeat-start="(key , data) in fuelConData | orderBy:natural(sort.sortingOrder):sort.reverse" ng-style="{ 'background-color': $index % 2 ? '#ffffff' : '#f9f9f9' }">
                                                        
                                        <td style="font-weight: normal !important;padding-top: 25px;" rowspan="{{data.nTanks}}">{{data.vehicleName}}</td>
                                        <td style="font-weight: normal !important;padding-top: 25px;" rowspan="{{data.nTanks}}">{{data.orgId}}</td>
                                        <td style="font-weight: normal !important;">
                                          1
                                        </td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(data.tankData[0].startFuel) }}</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(data.tankData[0].endFuel) }}</td>
                                        <td style="font-weight: normal !important;">{{roundOffDecimal(data.tankData[0].fuelConsumption)}}</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(data.tankData[0].fuelFilling)}}</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(data.tankData[0].fuelTheft)}}</td>
                                        <td style="font-weight: normal !important;">{{parseInts(data.startKms)}}</td>
                                        <td style="font-weight: normal !important;">{{parseInts(data.endKms)}}</td>
                                        <td style="font-weight: normal !important;">{{data.dist}}</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(data.tankData[0].kmpl) }}</td>
                                   <!-- <td>-</td>
                                        <td>-</td>-->
                                        <td style="font-weight: normal !important;">{{msToTime2(data.engineRunningHrs)}}</td>
                                        <td style="font-weight: normal !important;">{{msToTime2(data.engineIdleHrs)}}</td>
                                        <td style="font-weight: normal !important;"> {{ roundOffDecimal(data.tankData[0].ltrsPerHrs) }} </td>
                                        <td style="font-weight: normal !important;">{{msToTime2(data.ignitionHrs)}}</td>
                                        <td style="font-weight: normal !important;">{{msToTime2(parseInts(data.secondaryEngineDuration))}}</td>
                                         <td style="font-weight: normal !important;"  class="addr"> {{data.startAddress}}</td>
                                        <td style="font-weight: normal !important;" class="addr">{{data.endAddress}}</td>
                    										<td style="font-weight: normal !important;" class="loc"><a href="https://www.google.com/maps?q=loc:{{data.startLoc}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
                    										<td style="font-weight: normal !important;" class="loc"><a href="https://www.google.com/maps?q=loc:{{data.endLoc}}" target="_blank"><?php echo Lang::get('content.link'); ?></td>
                                        <td ng-if="data.driverName==''" style="font-weight: normal !important;" >-</td>
                    										<td ng-if="data.driverName!=''" style="font-weight: normal !important;">{{data.driverName}}</td>
                    										<td ng-if="data.driverMobileNo==''" style="font-weight: normal !important;" >-</td>
                    										<td ng-if="data.driverMobileNo!=''" style="font-weight: normal !important;">{{data.driverMobileNo}}</td>
                    										<td ng-if="data.remarks==''" style="font-weight: normal !important;" >-</td>
                    										<td ng-if="data.remarks!=''" style="font-weight: normal !important;">{{data.remarks}}</td>

                                      </tr>
                                      <tr ng-repeat-end ng-repeat="objFuel in data.tankData.slice(1,(parseInts(data.nTanks)>1)?data.tankData.length:data.tankData.length-1)" ng-style="{ 'background-color': key % 2 ? '#ffffff' : '#f9f9f9' }">
                                        <td style="font-weight: normal !important;">{{ objFuel.sensor }}</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(objFuel.startFuel) }}</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(objFuel.endFuel) }}</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(objFuel.fuelConsumption) }}</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(objFuel.fuelFilling) }}</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(objFuel.fuelTheft) }}</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(objFuel.kmpl) }}</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(objFuel.ltrsPerHrs) }}</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">-</td>
                                      </tr>

                                    </span>  

                                      <tr ng-if="!conFuelError">                  

                                        <td colspan="2" style="font-size: 13px;font-weight: bold;"><?php echo Lang::get('content.total'); ?></td>                                       
                                        <td style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{roundOffDecimal(fuelConsumption)}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{fuelFilling}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{fuelTheft}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{parseInts(dist)}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{totalKmpl(dist,fuelConsumption)}}</td>                                   
                                        <td ng-if="engineHrs != 'NaN'" style="font-size: 13px;font-weight: bold;">{{msToTime2(engineHrs)}}</td>
                                        <td ng-if="engineHrs == 'NaN'" style="font-size: 13px;font-weight: bold;">-</td>
                                        <td ng-if="engineIdleHrs != 'NaN'" style="font-size: 13px;font-weight: bold;">{{msToTime2(engineIdleHrs)}}</td>
                                        <td ng-if="engineIdleHrs == 'NaN'" style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{totalLtph(fuelConsumption,ignitionHrs)}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{msToTime2(ignitionHrs)}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{msToTime2(secIgnitionHrs)}}</td>
                                        <td>-</td>
                    										<td>-</td>
                    										<td>-</td>
                    										<td>-</td>
                    										<td>-</td>
                                      </tr>
                      
                                    <tr ng-if="conFuelError" style="text-align: center">
                                        <td colspan="22" class="err"><h5>{{conFuelError}}</h5></td>
                                    </tr>

                            </table> 
                          </div>              
                        </div>

                         <div class="box-body"  id="FuelConBasedOnTank" ng-hide='true'>

                          <p style="margin-left: 60px;"><span><b><?php echo Lang::get('content.organization'); ?> <?php echo Lang::get('content.name'); ?> :<b> {{orgName}}</span> &nbsp;&nbsp;&nbsp;&nbsp;<span style="margin-left: 40px;"><b><?php echo Lang::get('content.date'); ?></b> : &nbsp;{{uiDate.fromdate}}</span> </p>
                            <div id="">
                            <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top:20px;">
                              <thead>
                                                     
                                <tr style="text-align:center;">
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.total_fuel_consumption'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{roundOffDecimal(fuelConsumption)}}</th>
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.tot_fuel_fill'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{fuelFilling}}</th>
                                </tr>
                                <tr style="text-align:center;">                   
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total_distance'); ?></th>
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{parseInts(dist)}}</th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_theft'); ?></th>
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelTheft}}</th>
                                </tr>
                                <tr style="text-align:center;">
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.tot_engine_idle_hrs'); ?></th>
                                    <th ng-if="engineIdleHrs != 'NaN'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{msToTime2(engineIdleHrs)}}</th>
                                    <th ng-if="engineIdleHrs == 'NaN'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">-</th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.tot_engine_hrs'); ?></th>
                                    <th ng-if="ignitionHrs != 'NaN'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{msToTime2(ignitionHrs)}}</th>
                                    <th ng-if="ignitionHrs == 'NaN'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">-</th>
                                </tr>
                                <tr style="text-align:center;">
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.KMPL'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{totalKmpl(dist,fuelConsumption)}}</th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.LTPH'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{totalLtph(fuelConsumption,ignitionHrs)}}</th> 
                                </tr>

                              </thead>
                           </table>   
                           <div style="overflow-y: auto;">
                           <table class="table table-bordered table-condensed table-hover table-fixed-header" style="margin-top:20px;">                                          
                                  <thead class='header' style=" z-index: 1;">                         
                                      <tr>
                                        <th class="id" custom-sort order="'vehicleName'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.veh_name'); ?></b></th>
                                        <th class="id" custom-sort order="'orgId'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.organization'); ?> <?php echo Lang::get('content.name'); ?></b></th>
                                        <th class="id" custom-sort order="'nTanks'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.tank'); ?></b></th>
                                        <th class="id" custom-sort order="'startFuel'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.start_fuel'); ?></b></th>
                                        <th class="id" custom-sort order="'endFuel'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.end_fuel'); ?></b></th>
                                        <th class="id" custom-sort order="'fuelConsumption'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_consumption'); ?></b></th>
                                        <th class="id" custom-sort order="'fuelFilling'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_filling'); ?></b></th>
                                        <th class="id" custom-sort order="'fuelTheft'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_theft'); ?></b></th>
                                        <th class="id" custom-sort order="'startKms'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.start_kms'); ?></b></th>
                                        <th class="id" custom-sort order="'endKms'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.end_kms'); ?></b></th>
                                        <th class="id" custom-sort order="'dist'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.dis_travelled'); ?></b></th>
                                        <th class="id" custom-sort order="'kmpl'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.kmpl'); ?></b></th>   
                                   <!-- <th style="font-size:12px;background-color:#ecf7fb;"><b>Engine Start Hrs</b></th>
                                        <th style="font-size:12px;background-color:#ecf7fb;"><b>Engine End Hrs</b></th>-->
                                        <th style="font-size:12px;background-color:#ecf7fb;white-space: nowrap;"><b><?php echo Lang::get('content.running_hrs'); ?></b></th>
                                        <th style="font-size:12px;background-color:#ecf7fb;white-space: nowrap;"><b><?php echo Lang::get('content.eng_idle_hrs'); ?></b></th>
                                        <th class="id" custom-sort order="'ltrsPerHrs'" sort="sort"  style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.ltrs_per_hrs'); ?></b></th>   
                                        <th style="font-size:12px;background-color:#ecf7fb;white-space: nowrap;"><b><?php echo Lang::get('content.eng_hrs'); ?></b></th> 
                                        <th style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.secondary_engine_hrs'); ?></b></th> 
                                         <th style="font-size:12px;background-color:#ecf7fb;" class="addr"><b>Start Address</b></th>   
                                        <th style="font-size:12px;background-color:#ecf7fb;" class="addr"><b>End Address</b></th>
                                        <th style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.dri_name'); ?></b></th>
                                        <th style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.dri_mob'); ?></b></th>
                                        <th style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.remarks'); ?></b></th>                              
                                      </tr>
                                  </thead>

                                   <tbody>

                                   <span id="tableId">
                                     
                                      <tr ng-if="!conFuelError" ng-repeat-start="(key , data) in fuelConData | orderBy:natural(sort.sortingOrder):sort.reverse" ng-style="{ 'background-color': $index % 2 ? '#ffffff' : '#f9f9f9' }">
                                                        
                                        <td style="font-weight: normal !important;padding-top: 25px;" rowspan="{{data.nTanks}}">{{data.vehicleName}}</td>
                                        <td style="font-weight: normal !important;padding-top: 25px;" rowspan="{{data.nTanks}}">{{data.orgId}}</td>
                                        <td style="font-weight: normal !important;">
                                          1
                                        </td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(data.tankData[0].startFuel) }}</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(data.tankData[0].endFuel) }}</td>
                                        <td style="font-weight: normal !important;">{{roundOffDecimal(data.tankData[0].fuelConsumption)}}</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(data.tankData[0].fuelFilling)}}</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(data.tankData[0].fuelTheft)}}</td>
                                        <td style="font-weight: normal !important;">{{parseInts(data.startKms)}}</td>
                                        <td style="font-weight: normal !important;">{{parseInts(data.endKms)}}</td>
                                        <td style="font-weight: normal !important;">{{data.dist}}</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(data.tankData[0].kmpl) }}</td>
                                   <!-- <td>-</td>
                                        <td>-</td>-->
                                        <td style="font-weight: normal !important;">{{msToTime2(data.engineRunningHrs)}}</td>
                                        <td style="font-weight: normal !important;">{{msToTime2(data.engineIdleHrs)}}</td>
                                        <td style="font-weight: normal !important;"> {{ roundOffDecimal(data.tankData[0].ltrsPerHrs) }} </td>
                                        <td style="font-weight: normal !important;">{{msToTime2(data.ignitionHrs)}}</td>
                                        <td style="font-weight: normal !important;">{{msToTime2(parseInts(data.secondaryEngineDuration))}}</td>
                                         <td style="font-weight: normal !important;"> {{data.startAddress}}</td>
                                        <td style="font-weight: normal !important;">{{data.endAddress}}</td>
                                        <td ng-if="data.driverName==''" style="font-weight: normal !important;" >-</td>
                                        <td ng-if="data.driverName!=''" style="font-weight: normal !important;">{{data.driverName}}</td>
                                        <td ng-if="data.driverMobileNo==''" style="font-weight: normal !important;" >-</td>
                                        <td ng-if="data.driverMobileNo!=''" style="font-weight: normal !important;">{{data.driverMobileNo}}</td>
                                        <td ng-if="data.remarks==''" style="font-weight: normal !important;" >-</td>
                                        <td ng-if="data.remarks!=''" style="font-weight: normal !important;">{{data.remarks}}</td>
                                      </tr>
                                      <tr ng-repeat-end ng-repeat="objFuel in data.tankData.slice(1,(parseInts(data.nTanks)>1)?data.tankData.length:data.tankData.length-1)">
                                        <td style="font-weight: normal !important;">{{ objFuel.sensor }}</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(objFuel.startFuel) }}</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(objFuel.endFuel) }}</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(objFuel.fuelConsumption) }}</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(objFuel.fuelFilling) }}</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(objFuel.fuelTheft) }}</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(objFuel.kmpl) }}</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">{{ roundOffDecimal(objFuel.ltrsPerHrs) }}</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">-</td>
                                        <td style="font-weight: normal !important;">-</td>
                                      </tr>

                                    </span>  

                                      <tr ng-if="!conFuelError">                  

                                        <td colspan="2" style="font-size: 13px;font-weight: bold;"><?php echo Lang::get('content.total'); ?></td>                                       
                                        <td style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{roundOffDecimal(fuelConsumption)}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{fuelFilling}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{fuelTheft}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{parseInts(dist)}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{totalKmpl(dist,fuelConsumption)}}</td>                                   
                                        <td ng-if="engineHrs != 'NaN'" style="font-size: 13px;font-weight: bold;">{{msToTime2(engineHrs)}}</td>
                                        <td ng-if="engineHrs == 'NaN'" style="font-size: 13px;font-weight: bold;">-</td>
                                        <td ng-if="engineIdleHrs != 'NaN'" style="font-size: 13px;font-weight: bold;">{{msToTime2(engineIdleHrs)}}</td>
                                        <td ng-if="engineIdleHrs == 'NaN'" style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{totalLtph(fuelConsumption,ignitionHrs)}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{msToTime2(ignitionHrs)}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{msToTime2(secIgnitionHrs)}}</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                      </tr>
                      
                                    <tr ng-if="conFuelError" style="text-align: center">
                                        <td colspan="22" class="err"><h5>{{conFuelError}}</h5></td>
                                    </tr>

                              </table> 
                          </div>              
                        </div>

                </div>
            </div>

          </div>
        </div>

    <script src="assets/js/static.js"></script>
    <script src="assets/js/jquery-1.11.0.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script data-require="angular-ui-bootstrap@0.11.0" data-semver="0.11.0" src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>
    <script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/angular-translate.js"></script>
<!--<script src="assets/js/highcharts.js"></script> -->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="../resources/views/reports/customjs/moment.js"></script>
    <script src="../resources/views/reports/customjs/FileSaver.js"></script>
    <script src="../resources/views/reports/customjs/html5csv.js"></script>
    <script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="../resources/views/reports/datatable/jquery.dataTables.js"></script>
    <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
<!--<script src="assets/js/naturalSortVersionDates.js"></script> -->
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/services.js"></script>
    <script src="assets/js/fuelConsolidate.js?v=<?php echo Config::get('app.version');?>"></script>
    
    <script>

   
        $("#example1").dataTable();
          
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        
        $(function () {
                $('#dateFrom, #dateTo').datetimepicker({
                    format:'YYYY-MM-DD',
                    useCurrent:true,
                    pickTime: false
                });
                $('#timeFrom').datetimepicker({
                    pickDate: false,
                    
                });
                $('#timeTo').datetimepicker({
                    pickDate: false,
                    
                });
        });      
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    $('.addr').hide();
    var generatePDF = function() {
            $('.addr').show();
            $('.loc').hide();
      kendo.drawing.drawDOM($("#FuelConBasedOnTank1")).then(function(group) {
        kendo.drawing.pdf.saveAs(group, "ConsolidateFuelReport.pdf");
            $('.loc').show();
            $('.addr').hide();
      });
}

  </script>
    
</body>
</html>
