<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title><?php echo Lang::get('content.gps'); ?></title>
  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link rel="stylesheet" href="assets/css/popup.bootstrap.min.css">
  <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
  <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
  <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
  <style>
    <style>
    body{
      font-family: 'Lato', sans-serif;
      /*font-weight: bold;*/  
/* font-family: 'Lato', sans-serif;
font-family: 'Roboto', sans-serif;
font-family: 'Open Sans', sans-serif;
font-family: 'Raleway', sans-serif;
font-family: 'Faustina', serif;
font-family: 'PT Sans', sans-serif;
font-family: 'Ubuntu', sans-serif;
font-family: 'Droid Sans', sans-serif;
font-family: 'Source Sans Pro', sans-serif;
*/
}
.empty{
  height: 1px; width: 1px; padding-right: 30px; float: left;
}

.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
  background-color: #ffffff;
}
</style>
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2X3F316479');
</script>
</head>
<div id="preloader" >
  <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
  <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
  <div id="wrapper" ng-controller="mainCtrl" class="ng-cloak">
    
    <?php include('sidebarList.php');?> 
    
    <div id="testLoad"></div>
    
    <div id="page-content-wrapper">
      <div class="container-fluid">
        <div class="panel panel-default">
         
        </div>   
      </div>
    </div>
    
    <!-- AdminLTE css box-->

    <div class="col-md-12">
     <div class="box box-primary">
      <!-- <div class="row"> -->
        <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
          <h3 class="box-title"><span><?php echo Lang::get('content.trip_summary'); ?></span>
            <span><?php include('OutOfOrderDataInfo.php');?></span> 
          </h3>
        </div>
        <div class="row">
         <!-- <div class="col-md-1" align="center"></div>-->
         <div class="col-md-2" align="center">
          <div class="form-group" >
            <h5 style="color: grey;">{{shortNam}}</h5>
          </div>
                                   <!-- <div class="form-group" ng-if="shortNam==undefined || shortNam==null">
                                        <h5 style="color: red;">{{ vehiLabel | translate }} not found </h5>
                                      </div> -->
                                    </div>
                                    <?php include('dateTime.php');?>
                                    <div class="col-md-1" align="center"></div>
                                    <div class="col-md-1" align="center">
                                      <button style="margin-left: -100%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
                                    </div>
                                  </div>
                                  <div class="col-md-12" align="right" style="margin-bottom: 10px;">
                                   <?php include('DateButtonWithWeekMonth.php');?>
                                 </div>
                                 <!--  </div> -->
                               </div>
                               
                             </div>

                             <div class="col-md-12">
                              <div class="box box-primary">
                                <div style="overflow-x: auto;">
                                  <div class="col-md-12">
                                    <div class="pull-left" style="margin-top: 1%;margin-left: 10px;">
                                      <button ng-show="!showLocTrips" type="button" class="btn btn-primary" ng-click="getLocationTrips(1)" ng-disabled="todayDisabled">Location Trips</button>
                                      <button ng-show="showLocTrips" type="button" class="btn btn-primary" ng-click="getLocationTrips(0)">Trip Summary</button>
                                    </div>
                                    <div class="pull-right" style="margin-top: 1%;">
                                      

                                      <img style="cursor: pointer;" ng-click="exportData('tripreportkms')"  src="../resources/views/reports/image/xls.png" />
                                      <img style="cursor: pointer;" ng-click="exportDataCSV('tripreportkms')"  src="../resources/views/reports/image/csv.jpeg" />
                                      <img style="cursor: pointer;" onclick="generatePDF()"  src="../resources/views/reports/image/Adobe.png" />
                                    </div>
                                    <div class="box-body" id="tripreportkms" >
                                     <div class="empty" align="center"></div> 
                                     <div id="formConfirmation">
                                      <table class="table table-bordered table-striped table-condensed table-hover table-fixed-header1" style="margin-top: 45px;">
                                        <thead style="font-weight: bold;">
                                          <tr class="info">
                                            <td colspan="12"> <div class="col-md-12" style="text-transform: uppercase;">
                                              <?php echo Lang::get('content.trip_summary'); ?> </div> </td>
                                            </tr> 
                                            <tr class="info">
                                              <td colspan="2"> <div class="col-md-12" style="text-transform: uppercase;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.name'); ?></div> </td>
                                              <td colspan="2"> <div class="col-md-12">{{shortNam}}</div> </td>
                                              <td colspan="2"> <div class="col-md-12" style="text-transform: uppercase;"><?php echo Lang::get('content.start_time'); ?></div> </td>
                                              <td colspan="2"> <div class="col-md-12" >{{siteData.startTime |  date:'hh:mm a dd-MM-yyyy'}}</div>  </td>
                                              <td colspan="2"><div class="col-md-12" style="text-transform: uppercase;"><?php echo Lang::get('content.end_time'); ?></div></td>
                                              <td colspan="2"> <div class="col-md-12" >{{siteData.endTime |  date:'hh:mm a dd-MM-yyyy'}}</div>  </td>
                                            </tr>
                                            <tr class="info">
                                              <td colspan="2"> <div class="col-md-12"><span ng-show="showLocTrips">TOTAL</span> ON TIME ACHIEVED TRIPS </div> </td>
                                              <td colspan="1"> <div class="col-md-12">{{siteData.achievedTripsCount }}</div> </td>
                                              <td colspan="2"> <div class="col-md-12"><span ng-show="showLocTrips">TOTAL</span> ON TIME NOT ACHIEVED TRIPS</div> </td>
                                              <td colspan="1"> <div class="col-md-12" > {{siteData.nonAchievedTripsCount }} </div>  </td>
                                              <td colspan="2"><div class="col-md-12"><span ng-show="showLocTrips">TOTAL</span> DELAY TIME </div></td>
                                              <td colspan="1"> <div class="col-md-12">{{msToTime(siteData.totalDelayTime) }}</div>  </td>
                                              <td colspan="2"><div class="col-md-12">KM DISTANCE </div></td>
                                              <td colspan="1"> <div class="col-md-12">{{siteData.distanceCovered }}</div>  </td>
                                            </tr>
                                          </thead>
                                          
                                          <thead class='header' style=" z-index: 1;" ng-show="!showLocTrips">
                                            
                                            <tr class="info">
                                              <th colspan="2" class="id" custom-sort order="'location'" sort="sort" style="text-align:center; font-weight: bold;"><?php echo Lang::get('content.location'); ?></th>
                                              <th colspan="2" class="id" custom-sort order="'planTime'" sort="sort" style="text-align:center; font-weight: bold;">Plan Time</th>
                                              <th colspan="2" class="id" custom-sort order="'startTime'" sort="sort" style="text-align:center; font-weight: bold;">Gate IN time (A)</th>
                                              <th colspan="2" class="id" custom-sort order="'endTime'" sort="sort" style="text-align:center; font-weight: bold;">Gate OUT time (B)</th>
                                              <th class="id" custom-sort order="'duration'" sort="sort" style="text-align:center; font-weight: bold;">Actual Total time ( A-B )</th>
                                              <th class="id" custom-sort order="'delayTime'" sort="sort" style="text-align:center; font-weight: bold;">RESULT Delay/Ok</th>
                                              <th class="id" custom-sort order="'transistTime'" sort="sort" style="text-align:center; font-weight: bold;">Transit time</th>
                                              <th class="id" custom-sort order="'remarks'" sort="sort" style="text-align:center; font-weight: bold;">Remarks</th>
                                              
                                            </tr>
                                          </thead>
                                          <tbody ng-show="!showLocTrips">
                                            <tr ng-repeat="tripsummary in siteData.tripData | orderBy:sort.sortingOrder:sort.reverse" style="text-align : center;" ng-if="!errMsg.includes('expired')">
                                              <!-- ng-click="getInput(tripsummary, siteData)" data-toggle="modal" data-target="#mapmodals" -->
                                              <td  colspan="2" >{{ tripsummary.location }}</td>
                                              <td  colspan="2" >{{msToTime(tripsummary.planTime)}}</td>
                                              <td  colspan="2" >{{tripsummary.startTime  | date:'HH:mm:ss dd-MM-yyyy' }}</td>
                                              <td  colspan="2" >{{tripsummary.endTime  | date:'HH:mm:ss dd-MM-yyyy' }}</td>
                                              <!--  <td >{{msToTime(tripsummary.duration)}}</td> -->
                                              <td >{{msToTime(tripsummary.duration)}}</td>
                                              <td>
                                                <span ng-if="tripsummary.delayTime<0" style="color: green">{{msToTime(tripsummary.delayTime)}}</span>
                                                <span ng-if="tripsummary.delayTime>0" style="color: red">{{msToTime(tripsummary.delayTime)}}</span>
                                                <span ng-if="tripsummary.delayTime==0" style="color: green">-</span>
                                              </td>
                                              <td >{{msToTime(tripsummary.transistTime)}}</td>
                                              <td >{{tripsummary.remarks}}</td>
                                            </tr>
                                   <!-- <tr style="text-align : center;" ng-if="siteData.countDetail.length!= 0">
                                    <td rowspan="{{rowSpan}}" colspan="6">Route wise Average results</td>
                                    <td colspan="2">{{siteData.countDetail[0].location}}</td>
                                    <td>{{msToTime(siteData.countDetail[0].averageActualTime)}}</td>
                                    <td rowspan="{{rowSpan}}" colspan="3"></td>
                                   </tr>
                                   <tr ng-repeat="data in siteData.countDetail" style="text-align : center;" ng-hide="$first">
                                    <td colspan="2">{{data.location}}</td>
                                    <td>{{msToTime(data.averageActualTime)}}</td>
                                  </tr> -->
                                  <tr  ng-if="siteData.tripData==null || siteData.tripData.length== 0" align="center">
                                    <td colspan="12" class="err" ng-if="!errMsg"><h5><?php echo Lang::get('content.no_trip'); ?></h5></td>
                                    <td colspan="12" class="err" ng-if="errMsg"><h5>{{errMsg}}</h5></td>
                                  </tr>
                                </tbody>
                                <thead class='header' style=" z-index: 1;" ng-show="showLocTrips">
                                  
                                  <tr class="info">
                                    <th colspan="3" class="id" custom-sort order="'location'" sort="sort" style="text-align:center; font-weight: bold;">Location</th>
                                    <th colspan="3" class="id" custom-sort order="'onTimeAchievedCount'" sort="sort" style="text-align:center; font-weight: bold;">On Time Achieved Trips</th>
                                    <th colspan="3" class="id" custom-sort order="'onTimeNotAchievedCount'" sort="sort" style="text-align:center; font-weight: bold;">On Time Not Achieved Trips</th>
                                    <th colspan="3" class="id" custom-sort order="'totalDelay'" sort="sort" style="text-align:center; font-weight: bold;">Delay Time</th>
                                    
                                  </tr>
                                </thead>
                                <tbody ng-show="showLocTrips">
                                  <tr ng-repeat="data in siteData.countDetail | orderBy:sort.sortingOrder:sort.reverse" style="text-align : center;">
                                    <!-- ng-click="getInput(tripsummary, siteData)" data-toggle="modal" data-target="#mapmodals" -->
                                    <td  colspan="3" >{{ data.location }}</td>
                                    <td  colspan="3" >{{ data.onTimeAchievedCount }}</td>
                                    <td  colspan="3" >{{ data.onTimeNotAchievedCount }}</td>
                                    <td  colspan="3" >{{msToTime(data.totalDelay)}}</td>
                                  </tr>
                                  <tr  ng-if="siteData.countDetail==null || siteData.countDetail.length== 0" align="center">
                                    <td colspan="12" class="err" ng-if="!errMsg"><h5><?php echo Lang::get('content.no_trip'); ?></h5></td>
                                    <td colspan="12" class="err" ng-if="errMsg"><h5>{{errMsg}}</h5></td>
                                  </tr>
                                </tbody>
                              </table>
                            </div>
                          </div>

                        </div>
                      </div>
                    </div>

       <!--  <div class="modal fade" id="myModal" style=" top :70px">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Trip Summary Routes</h4>
                    </div>
                    <div class="modal-body" id="map_canvas" style="width: 100%; height: 500px;"></div>
                   
                </div>
            </div>
          </div> -->
          <div class="modal fade" id="mapmodals">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="myCity"><?php echo Lang::get('content.trip_summary'); ?></h4>
                </div>
                <div class="modal-body">
                  <div class="map_container">
                    <div id="map_canvas" class="map_canvas" style="width: 100%; height: 500px;"></div>
                  </div>
                </div>
                <div class="modal-footer">
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div><!-- /.modal -->
        </div>
      </div>
      <script>
        var apikey_url = JSON.parse(localStorage.getItem('apiKey'));
        var url = "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places,geometry";

        if(apikey_url != null || apikey_url != undefined) {
         url = "https://maps.googleapis.com/maps/api/js?key="+apikey_url+"&libraries=places,geometry";
       }   
/*  var apikey_url = JSON.parse(localStorage.getItem('apiKey'));
    var url = "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places";

    if(apikey_url != null ||  apikey_url != undefined)
      url = "https://maps.googleapis.com/maps/api/js?key="+apikey_url+"&libraries=places"; */
    
    function loadJsFilesSequentially(scriptsCollection, startIndex, librariesLoadedCallback) {
     if (scriptsCollection[startIndex]) {
       var fileref = document.createElement('script');
       fileref.setAttribute("type","text/javascript");
       fileref.setAttribute("src", scriptsCollection[startIndex]);
       fileref.onload = function(){
         startIndex = startIndex + 1;
         loadJsFilesSequentially(scriptsCollection, startIndex, librariesLoadedCallback)
       };
       
       document.getElementsByTagName("head")[0].appendChild(fileref)
     }
     else {
       librariesLoadedCallback();
     }
   }
   
   // An array of scripts you want to load in order
   var scriptLibrary = [];
   
   scriptLibrary.push("assets/js/static.js");
   scriptLibrary.push("assets/js/jquery-1.11.0.js");
   scriptLibrary.push("assets/js/bootstrap.min.js");
   scriptLibrary.push("https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js");
   scriptLibrary.push(url);
   scriptLibrary.push("../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js");
   scriptLibrary.push("https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js");
   scriptLibrary.push("../resources/views/reports/customjs/html5csv.js");
   scriptLibrary.push("../resources/views/reports/customjs/moment.js");
   scriptLibrary.push("../resources/views/reports/customjs/FileSaver.js");
   scriptLibrary.push("../resources/views/reports/datepicker/bootstrap-datetimepicker.js");
 //scriptLibrary.push(url);
 scriptLibrary.push("https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.1/js/bootstrap.min.js");
 scriptLibrary.push("assets/js/angular-translate.js");
 //scriptLibrary.push("assets/js/infobubble.js");
 //scriptLibrary.push("assets/js/moment.js");
 //scriptLibrary.push("assets/js/bootstrap-datetimepicker.js");
 //scriptLibrary.push("assets/js/infobox.js");
 scriptLibrary.push("assets/js/naturalSortVersionDatesCaching.js");
 //scriptLibrary.push("assets/js/naturalSortVersionDates.js");
 scriptLibrary.push("assets/js/vamoApp.js");
 scriptLibrary.push("assets/js/services.js");
 scriptLibrary.push("assets/js/siteReport.js?v=<?php echo Config::get('app.version');?>");

// Pass the array of scripts you want loaded in order and a callback function to invoke when its done
loadJsFilesSequentially(scriptLibrary, 0, function(){
       // application is "ready to be executed"
       // startProgram();
     });

    //     $("#menu-toggle").click(function(e) {
    //     e.preventDefault();
    //     $("#wrapper").toggleClass("toggled");
    // });

    var generatePDF = function() {
      kendo.drawing.drawDOM($("#formConfirmation"),{paperSize: "A4",multiPage: true,margin: {
        left   : "4mm",
        top    : "10mm",
        right  : "4mm",
        bottom : "10mm"
      } ,landscape: true }).then(function(group) {
        kendo.drawing.pdf.saveAs(group, "TripSummaryReport.pdf");
      });
    }

    
  </script>
  
</body>
</html>
