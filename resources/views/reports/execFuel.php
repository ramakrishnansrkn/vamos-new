<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Satheesh">
    <title><?php echo Lang::get('content.gps'); ?></title>
    <link rel="shortcut icon" href="assets/imgs/tab.ico">
    <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
    <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="../resources/views/reports/datepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css">
    <link href="assets/css/jVanilla.css" rel="stylesheet">
    <link href="assets/css/simple-sidebar.css" rel="stylesheet">
    <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
    <link href="../resources/views/assets/css/datebutton.css" rel="stylesheet" type="text/css" />
    <!-- <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css"> -->
    <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
    <link href="../resources/views/reports/c3chart/c3.css" rel="stylesheet" type="text/css">
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
    <!-- pdfgen -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
    <script src="https://unpkg.com/jspdf-autotable@2.3.2/dist/jspdf.plugin.autotable.js"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-2X3F316479');
  </script>
</head>
<style>
   /* .box > .loading-img {
      z-index: 1020;
      background: transparent url('assets/imgs/status.gif') 50% 50% no-repeat;
      }*/
      text.highcharts-credits {
        display: none;
    }
    .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
        background-color: #ffffff;
    }
    .striped {
        color:white;
        background-color:green;
    }

    body{
        font-family: 'Lato', sans-serif;
        /*font-weight: bold;*/  
/* font-family: 'Lato', sans-serif;
font-family: 'Roboto', sans-serif;
font-family: 'Open Sans', sans-serif;
font-family: 'Raleway', sans-serif;
font-family: 'Faustina', serif;
font-family: 'PT Sans', sans-serif;
font-family: 'Ubuntu', sans-serif;
font-family: 'Droid Sans', sans-serif;
font-family: 'Source Sans Pro', sans-serif;
*/
}
</style>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>
<body ng-app="mapApp" style="overflow-x:auto;">
    <div id="wrapper" ng-controller="mainCtrl" class="ng-cloak"> 
      <?php include('sidebarList.php');?>
      <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="panel panel-default">
            </div>          
        </div>
    </div>
    <div id="testLoad"></div>

    <div ng-show="reportBanShow" class="modal fade" id="allReport" role="dialog" style="top: 100px">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <p class="err" style="text-align: center;"> <?php echo Lang::get('content.premium_user'); ?> </p>
                </div>
            </div>
        </div>
    </div> 

    <div ng-show="reportBanShow" class="col-md-10" >
        <div class="box box-primary" style="height:90px; padding-top:30px; margin-top:5%; margin-left:8%;">
            <p ><h5 class="err" style="text-align: center;"> <?php echo Lang::get('content.no_report_found'); ?> </h5></p>
        </div>
    </div>


    <div ng-hide="reportBanShow" class="col-md-12">
        <div class="box box-primary">
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                <h3 class="box-title"> <span><?php echo Lang::get('content.executive_fuel'); ?></span>
                    <span><?php include('OutOfOrderDataInfo.php');?></span> 
                </h3>
                <div class="box-tools pull-right">
                    <img style="cursor: pointer;" ng-click="exportData(downloadid)"  src="../resources/views/reports/image/xls.png" />
                    <img width=30 height=30 style="cursor: pointer;" ng-click="exportDataCSV(downloadid)"  src="../resources/views/reports/image/csv.jpeg" />
                    <img style="cursor: pointer;"  ng-click="exportPDF(downloadid)"  src="../resources/views/reports/image/Adobe.png" />
                </div>
            </div>
            <div class="row" ng-show="showDate" style="margin-top: 20px;">
                <div class="col-md-2" align="center"></div>
                <div class="col-md-2" align="center">
                    <div class="form-group">
                        <div class="input-group datecomp">
                            <input type="text" ng-model="fromdate" class="form-control placholdercolor" id="dtFrom" placeholder="<?php echo Lang::get('content.fromdate'); ?>">
                        </div>
                    </div>
                </div>

                <div  class="col-md-2" align="center" >
                    <div class="form-group">
                        <div class="input-group datecomp">
                            <input type="text" ng-model="todate" class="form-control placholdercolor" id="dtTo" placeholder="<?php echo Lang::get('content.todate'); ?>">
                        </div>
                    </div>
                </div>
                <div class="col-md-1" align="center">
                  <select style="
                  height: 35px;
                  width: 115px;"  ng-model="orgTime" ng-show="showOrgTime" ng-change="changeTime(orgTime)" >
                  
                  <option style="width: 100%;height: 100%"  ng-repeat="time in orgShiftTime track by $index | orderBy:time"    >{{time}}</option>
                  
              </select>
          </div>
          <div class="col-md-2" align="center">
            <button ng-click="plotHist()"><?php echo Lang::get('content.submit'); ?></button>
        </div>
        

        <div class="col-md-1" align="center"></div>
        <div class="col-md-8"  align="right" style="margin-bottom: 10px;margin-left: 200px;">
           <button type="button" class="btnDate" ng-click="durationFilter('yesterday')" ng-disabled="yesterdayDisabled" onclick="setToTime('yesterday')"><?php echo Lang::get('content.yesterday'); ?></button>
           <button type="button" class="btnDate" ng-click="durationFilter('thisweek')" ng-disabled="thisweekDisabled" onclick="setToTime('thisweek')"><?php echo Lang::get('content.thisweek'); ?></button>
           <button type="button" class="btnDate" ng-click="durationFilter('lastweek')" ng-disabled="weekDisabled" onclick="setToTime('lastweek')"><?php echo Lang::get('content.lastweek'); ?></button>
           <button type="button" class="btnDate" ng-click="durationFilter('month')" ng-disabled="monthDisabled" onclick="setToTime('thismonth')"><?php echo Lang::get('content.thismonth'); ?></button>
           <button type="button" class="btnDate" ng-click="durationFilter('lastmonth')" ng-disabled="lastmonthDisabled" onclick="setToTime('lastmonth')"><?php echo Lang::get('content.lastmonth'); ?></button>    

       </div>
       

   </div>
   <div class="row" style="text-align: right;padding-right: 10px;padding-bottom: 10px;">
      <span style="background-color: red;
      color: white;
      padding: 2px 15px;"></span>
      <span style="padding-left: 5px;margin-right: 10px;font-weight: bold;">No Data Vehicles</span>
  </div>

</div>

</div>

<hr>
<div class="col-md-12" ng-hide="donut">
 <div id="container" style="min-width:800px; max-width:800px; height: 350px;"></div>
</div>

<div class="col-md-12" ng-show="donut_new">
 <div id="container_new" style="min-width:800px; max-width:800px; height: 350px;"></div>
</div>

<div id="singleDiv" class="row">     
    <div class="col-md-5">
        <div style="min-width:400px; max-width:300px; height: 350px;" id="chart3" ></div>
    </div>
    <div class="col-md-5">
        <div style="min-width:400px; max-width:300px; height: 350px;" id="chart4"></div>     
    </div>
</div>

<div class="box-body" ng-class="overallEnable?'col-md-9':'col-md-12'" id="statusreport">
    <div id="formConfirmation">
        <p ng-if="showOrgTime" style="margin-left: 30px;margin-top: 5px;" class="pull-left"><span style="margin-left: 40px;"><b><?php echo Lang::get('content.date_time'); ?></b> : &nbsp;{{fromdate}}&nbsp;{{getAMPM(fromtotime[0])}} - {{todate}}&nbsp;{{getAMPM(fromtotime[1])}}</span> </p>
        <p ng-if="!showOrgTime" style="margin-left: 30px;margin-top: 5px;" class="pull-left"><span style="margin-left: 40px;"><b><?php echo Lang::get('content.date_time'); ?></b> : &nbsp;{{fromdate}}&nbsp;- {{todate}}&nbsp;</span> </p>
        <div class="form-group pull-right">
            <input type="search" class="form-control input-sm" placeholder="<?php echo Lang::get('content.enter'); ?> {{ vehiLabel | translate }} <?php echo Lang::get('content.name'); ?>" align="center" ng-model="searchbox" name="search" />
        </div>

        <!-- <h4 style="margin:0" class="page-header"><?php echo Lang::get('content.executive_fuel'); ?></h4> -->

        <div id='execFuel'>
          <!-- <div id="formConfirmation"> -->
            <table class="table table-bordered table-striped table-condensed table-hover table-striped table-fixed-header4" id='ExecFuel'>
               <thead class='header' style=" z-index: 1;">
                <tr style="text-align:center" >
                    <th class="id" ng-if="showOrgTime && showOrganization" custom-sort order="'organName'" sort="sort" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.organization'); ?> <?php echo Lang::get('content.name'); ?></th>
                    <th class="id" custom-sort order="'date'" sort="sort" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.date'); ?></th>
                    <th class="id" custom-sort order="'vehicleName'" sort="sort" style="text-align:center;background-color:#C2D2F2;">{{ vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></th>
                    <th class="id" custom-sort order="'vehicleId'" sort="sort" style="text-align:center;background-color:#C2D2F2;">{{ vehiLabel | translate }} <?php echo Lang::get('content.id'); ?></th>
                    <th class="id" custom-sort order="'startFuelLevel'" sort="sort" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.start_fuel'); ?></th>
                    <th class="id" custom-sort order="'endFuelLevel'" sort="sort" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.end_fuel'); ?></th>
                    <th class="id" custom-sort order="'fuelFills'" sort="sort" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.fuel_fill'); ?></th>
                    <th class="id" custom-sort order="'fuelDrops'" sort="sort" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.fuel_theft'); ?></th>
                    <th class="id" custom-sort order="'fuelConsume'" sort="sort" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.fuel_consume'); ?></th>
                    <th class="id" custom-sort order="'distanceToday'" sort="sort" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.distcovered'); ?></th>
                    <th class="id" custom-sort order="'milage'" sort="sort" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.KMPL'); ?></th>
                    <th class="id" custom-sort order="'totalPrimaryEngine'" sort="sort" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.engine_on'); ?> <?php echo Lang::get('content.Hrs'); ?></th>
                    <th class="id" custom-sort order="'ltrsphr'" sort="sort" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.ltrs_per_hrs'); ?></th>
                    <!--<th class="id" custom-sort order="'mileage'" sort="sort" style="text-align:center;background-color:#C2D2F2;">Mileage</th>-->
                </tr>
            </thead>


            <tr ng-repeat="data in execFuelData | orderBy:natural(sort.sortingOrder):sort.reverse | filter:searchbox" ng-style="{ 'color':data.position=='U'?'red':'black' }" class="active" style="text-align:center" ng-hide="execFuelData.length==1&&execFuelData[0].error">
               <td ng-if="showOrgTime && showOrganization">{{organName}}</td>
               <td>{{data.date | date:'dd-MM-yyyy'}}</td>
               <td>{{data.vehicleName}}</td> 
               <td>{{data.vehicleId}}</td>                               
               <td>{{data.startFuelLevel}}</td>
               <td>{{data.endFuelLevel}}</td>
               <td>{{data.fuelFills}}</td>
               <td>{{(data.vehicleMode=='Dispenser')?'-':roundOffDecimal(data.fuelDrops)}}</td>
               <td>{{parseInts(data.fuelConsume)}}</td>
               <td>{{ (data.vehicleMode=='DG')? '-' : data.distanceToday}}</td>
               <td>{{ (data.vehicleMode=='Machinery' || data.vehicleMode == 'DG' || data.milage <= 0)? '-' : data.milage}}</td>
               <td>{{ msToTime(data.totalPrimaryEngine)}}</td>
               <td>{{ (data.vehicleMode=='DG' || data.vehicleMode=='Machinery')? data.ltrsphr : '-'}}</td>
           </tr>

           <tr ng-if="execFuelData=='' || execFuelData==null || execFuelData.length==0" align="center">
            <td colspan="12" class="err"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
        </tr> 
        <tr ng-if="execFuelData.length==1&&execFuelData[0].error" align="center">
            <td colspan="12" class="err"><h5>{{execFuelData[0].error}}</h5></td>
        </tr>   

    </table>
    <!-- </div> -->
</div>
</div>
</div>
</div>  
</div>
</div>
<script src="assets/js/static.js"></script>   
<script src="assets/js/jquery-1.11.0.js"></script>
<!--  <script src="assets/js/jquery-3.1.1.min.js"></script> -->
<script src="assets/js/bootstrap.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script> 
        <script data-require="angular-ui-bootstrap@0.11.0" data-semver="0.11.0" src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script> -->
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>

        <script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
        <script src="assets/js/highcharts_new.js"></script>
        <script src="assets/js/highcharts_exporting.js"></script>
        <script src="assets/js/highcharts_statistics.js"></script>
        <script src="../resources/views/reports/customjs/FileSaver.js"></script>
        <script src="../resources/views/reports/customjs/moment.js"></script>
        <script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
        <script src="../resources/views/reports/datatable/jquery.dataTables.js"></script>
        <script src="assets/js/angular-translate.js"></script>
        <script src="../resources/views/reports/customjs/html5csv.js"></script>
        <script src="../resources/views/reports/c3chart/d3.js"></script>
        <script src="../resources/views/reports/c3chart/c3.min.js"></script>
        <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
        <script src='assets/js/table-fixed-header.js'></script>
        <!--<script src="assets/js/naturalSortVersionDates.js"></script> -->
        <script src="assets/js/vamoApp.js"></script>
        <script src="assets/js/services.js"></script>
        <script src="../resources/views/reports/customjs/statistics.js?v=<?php echo Config::get('app.version');?>"></script>
        <script>
    // $("#testLoad").load("../public/menu");
    // var logo =document.location.host;
    // var imgName= '/vamo/public/assets/imgs/'+logo+'.small.png';
    // $('#imagesrc').attr('src', imgName);
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
    
    $(function () {

        $('#monthFrom, #monthFroms').datetimepicker({
            minViewMode: 'months',
            viewMode: 'months',
            pickTime: false,
            useCurrent:true,
            format:'MM/YYYY',
            maxDate: new Date,
            minDate: new Date(2015, 12, 1)
        });
    });  
    $(document).ready(function(){
      $("#searchbox").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#poiId tbody tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
  });
    
</script>

</body>
</html>

