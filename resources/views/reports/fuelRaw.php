<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title><?php echo Lang::get('content.gps'); ?></title>
  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
  <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
  <link href="assets/css/switchToggleBtn.css" rel="stylesheet">
  <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
  <style>
    .tooltip-color + .tooltip > .tooltip-inner {background-color: white;border: 1px solid black;color: black;}
    .empty{
      height: 1px; width: 1px; padding-right: 30px; float: left;
    }
    .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
      background-color: #ffffff;
    }
    .modal-header {
      background: #2f6581 !important;
      color: white
    }
    body{
      font-family: 'Lato', sans-serif;
/*font-weight: bold;
font-family: 'Lato', sans-serif;
font-family: 'Roboto', sans-serif;
font-family: 'Open Sans', sans-serif;
font-family: 'Raleway', sans-serif;
font-family: 'Faustina', serif;
font-family: 'PT Sans', sans-serif;
font-family: 'Ubuntu', sans-serif;
font-family: 'Droid Sans', sans-serif;
font-family: 'Source Sans Pro', sans-serif;
*/
} 

.col-md-12 {
  width: 98% !important;
  left: 15px !important;
  padding-left: 20px !important;
}

/*
.chart {
    min-width: 320px;
    max-width: 800px;
    height: 280px;
    margin: 0 auto;
}

#container {
    width: 80%;
    height: unset !important; 
    max-width: 80%;
    max-height: unset !important; 
    min-height: 500px !important;
    margin-top: 10px !important;
}
*/

</style>

</head>
<!-- <div id="preloader" >
    <div id="status">&nbsp;
      <p>Loading {{totalData}} Data </p></div>
    </div> -->
    <div id="preloader02" >
      <div id="status02">&nbsp;</div>
    </div> 

    <body  ng-controller="mainCtrl" ng-app="mapApp" style="" when-scrolled="loadMore()">

      <div class="ng-cloak">
        <div id="preloader" >
          <div id="status01">&nbsp;
            <p style="margin-top: 110px;margin-left: 10px;" ng-hide="fuelfill">Loading <span ng-bind="totalData"></span> Data </p>
          </div>
        </div>
        <div id="wrapper">

          <?php include('sidebarList.php');?> 

          <div id="testLoad"></div>

          <div id="page-content-wrapper">
            <div class="container-fluid">
              <div class="panel panel-default"></div>   
            </div>
          </div>

          <div class="col-md-12">
           <div class="box box-primary" style="padding-top: 5px;margin-top: -20px;">

            <!-- <div class="row"> -->
              <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
                <h3 class="box-title"><?php echo Lang::get('content.fuel_raw_data'); ?> (<?php echo Lang::get('content.graph'); ?>)
                </h3>
                <div class="box-tools pull-right">
                 <?php include('helpVideos.php');?>
               </div>
             </div>
             <div class="row">
              <div class="col-md-2" align="center">
                <div class="form-group" ng-if="shortNam!=undefined || shortNam!=null">
                  <h5 style="color: grey;">{{shortNam}}</h5>
                </div>
                <div class="form-group" ng-if="shortNam==undefined || shortNam==null">
                  <h5 style="color: red;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.not_found'); ?></h5>
                </div>
              </div>
              <div class="col-md-2" align="center">
                <div class="form-group">
                  <div class="input-group datecomp">
                    <input type="text" ng-change="getfromtodates(uiDate.fromdate,uiDate.todate)" ng-model="uiDate.fromdate" class="form-control placholdercolor" id="dateFrom"  placeholder="<?php echo Lang::get('content.from_date'); ?>">
                    <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                  </div>
                </div>
              </div> 
              <div class="col-md-2" align="center">
                <div class="form-group">
                  <div class="input-group datecomp">
                    <input type="text" ng-model="uiDate.fromtime" class="form-control placholdercolor" id="timeFrom" placeholder="<?php echo Lang::get('content.from_time'); ?>">
                    <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                  </div>
                </div>
              </div>
              <div class="col-md-2" align="center">
                <div class="form-group">
                  <div class="input-group datecomp">
                    <input type="text" ng-change="getfromtodates(uiDate.fromdate,uiDate.todate)" ng-model="uiDate.todate" class="form-control placholdercolor" id="dateTo" placeholder="<?php echo Lang::get('content.to_date'); ?>">
                    <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                  </div>
                </div>
              </div>
              <div class="col-md-2" align="center">
                <div class="form-group">
                  <div class="input-group datecomp">
                    <input type="text" ng-model="uiDate.totime" class="form-control placholdercolor" id="timeTo" placeholder="<?php echo Lang::get('content.to_time'); ?>">
                    <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                  </div>
                </div>
              </div>
              <div class="col-md-1" align="center">
                <div class="form-group">
                  <div class="input-group datecomp" style="margin-left: -35%;margin-right: -26%;">
                    <select ng-change="intervalChange(interval)" class="input-sm form-control" ng-model="interval">
                     <option ng-show="showInterval" value=""><?php echo Lang::get('content.interval'); ?></option>
                     <option label="5 <?php echo Lang::get('content.Mins'); ?>">5</option>
                     <option label="10 <?php echo Lang::get('content.Mins'); ?>">10</option>
                     <option label="15 <?php echo Lang::get('content.Mins'); ?>">15</option>
                     <option label="20 <?php echo Lang::get('content.Mins'); ?>">20</option>
                     <option label="30 <?php echo Lang::get('content.Mins'); ?>">30</option>
                     <option label="1 <?php echo Lang::get('content.hour'); ?>">60</option>
                   </select>
                 </div>
               </div>
             </div> 
             <div class="col-md-1" align="center">
              <button style="margin-left: -42%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
            </div>
          </div>
          <div class="col-md-6" style="padding-left: 10%">
            <?php include('days.php');?>
          </div>

          <!--  </div> -->
          <div class="row">
            <div class="col-md-1" align="center"></div>
            <div class="col-md-2" align="center">
              <div class="form-group"></div>
            </div>
          </div>

        </div>
      </div>

      <div class="col-md-12" style="border-color: unset!important;">
       <!-- modal box -->
       <div class="modal fade" id="myModal" role="dialog" style="top: 100px">
        <div class="modal-dialog">

          <div class="modal-content">
            <div class="modal-header">
              <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
              <h4 class="modal-title"><?php echo Lang::get('content.fuel_raw_data'); ?> </h4>
            </div>
            <div class="modal-body">
              <p><?php echo Lang::get('content.report_loading_msg'); ?></p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" data-dismiss="modal" ng-click="showRecords('fuelData')"><?php echo Lang::get('content.ok'); ?></button>
              <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Lang::get('content.close'); ?></button>
            </div>
          </div>

        </div>
      </div>
      <!-- end modal -->
      <div class="box box-primary" style="min-height:570px;background-color:#fdfdfd;">
        <div>
          <div style="margin-top: 10px;margin-right: 5px;margin-bottom:10px;">
            <span style="margin-left: 2%;"><b><?php echo Lang::get('content.tanksize'); ?> : {{tankSize}} <?php echo Lang::get('content.Ltrs'); ?> </b></span>
            <span style="margin-left: 2%;"><b><?php echo Lang::get('content.cur_fuel'); ?> : <span ng-bind="fueLiter"></span>  </b></span>
            <span style="margin-left: 2%;"><b><?php echo Lang::get('content.mode'); ?> : {{vehicleMode ? vehicleMode : ' - '}} </b></span>
            <span style="margin-left: 2%;"><b><?php echo Lang::get('content.engine_on'); ?> : 
              <span ng-if="(engineType=='Voltage_Ignition')"> <?php echo Lang::get('content.voltage'); ?> + <?php echo Lang::get('content.ign'); ?> <span ng-if="engineType.includes('Voltage')"> ( {{voltageValue | translate}} )</span></span>
              <span ng-if="(engineType!='Voltage_Ignition')">{{engineType | translate}} <span ng-if="engineType.includes('Voltage')"> ( {{voltageValue }} )</span> </span></b>
            </span>
          </div>
          <div class="pull-left" ng-if="sensorCount>1"  ng-repeat="sensorNo in range(1,sensorCount)"  style="margin-top: 10px;margin-left: 5px;margin-bottom:10px;">
           <button type="button" class="btn btn-info" ng-click="sensorChange(sensorNo)" id="sensor{{sensorNo}}" ><?php echo Lang::get('content.sensor'); ?> {{sensorNo}}</button>
         </div> 
         <div class="pull-left" ng-if="sensorCount>1" style="margin-top: 10px;margin-left: 5px;margin-bottom:10px;">
           <button type="button" class="btn btn-info" ng-click="sensorChange('All')" id="sensorAll" ><?php echo Lang::get('content.all'); ?></button>
         </div> 
         <div class="pull-right" style="margin-top: 10px;margin-right: 5px;margin-bottom:10px;">

          <img style="cursor: pointer;" ng-click="exportData('fuelRawReport')"  src="../resources/views/reports/image/xls.png" ng-show="datashow" / >
          <img style="cursor: pointer;" ng-click="exportDataCSV('fuelRawReport')"  src="../resources/views/reports/image/csv.jpeg" ng-show="datashow" />
          <img style="cursor: pointer;" onclick="generatePDF()"  src="../resources/views/reports/image/Adobe.png" ng-show="datashow"/>
          <!-- <img title="Details View" style="cursor: pointer;margin-right: 10px;" ng-click="showRecords('yes')"  src="../resources/views/reports/image/records.png" ng-hide="datashow" /> -->
        </div>

        <div class="box-body">

          <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto;" ng-hide="hideGraph">

          </div>

        </tabset>

        <div ng-show="(showFuelData && !error) && !errMsg" style="text-align: center;padding-top: 5px;"><b>Fuel fill and theft details</b></div>
        <div  ng-show="!showFuelData && !errMsg" class="pull-right" style="margin-top: 10px;margin-right: 5px;margin-bottom:10px;cursor: pointer;">
          <span ng-hide="error" ng-click="showRecords('filltheft')"><img title="Details View" style="cursor: pointer;margin-right: 5px;" src="../resources/views/reports/image/records.png" /> <i ng-style="{'color': fuelfill ? '#3c8dbc' : '#fffff' }"> Click here to view Fuel Fill & Theft</i></span>
          <span ng-hide="(datashow || error)" data-toggle="modal" data-target="#myModal"><img title="Details View" style="cursor: pointer;margin-right: 5px;margin-left: 10px;" src="../resources/views/reports/image/records.png"  /> <i ng-style="{'color': datashow ? '#3c8dbc' : '#fffff' }"> Click here to view Fuel Data</i></span>
          <span ng-show="(!error && datashow)"><img title="Details View" style="cursor: pointer;margin-right: 10px;margin-left: 10px;" src="../resources/views/reports/image/records.png"  /> <i ng-style="{'color': datashow ? '#3c8dbc' : '#fffff' }"> Click here to view Fuel Data</i></span>
          <span ng-show="showSubmit"><button type="button" class="btn btn-info" style="margin-left: 5px;" ng-click="submitDates()" ><?php echo Lang::get('content.submit'); ?></button></span>
        </div>
        
      </div>


      <div style="margin-top: 20px;" id="fuelRawReport" ng-show="datashow">
        <div id="formConfirmation">
          <table class="table table-bordered table-striped table-condensed table-hover" >
            <thead>
              <tr style="text-align:center;">
                <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.group'); ?></th>
                <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(gName)}}</th>
                <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.veh_name'); ?></th>            
                <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{shortNam}}</th>   
              </tr>
            </thead>
          </table>

          <table class="table table-bordered table-striped table-condensed table-hover table-fixed-header1" style="margin-top: 10px;"> 
            <thead class='header' style=" z-index: 1;">
              <tr style="text-align:center" >
                <th ng-show="(!error && datashow)" custom-sort order="'dt'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.status'); ?></th>
                <th class="id" custom-sort order="'dt'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.date_time'); ?></th>
                <th class="id" custom-sort order="'fuelLitr'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.fuel'); ?> <?php echo Lang::get('content.ltrs'); ?></th>

                <th class="id" custom-sort order="'ignitionStatus'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.ign'); ?></th>
                <th class="id" custom-sort order="'primaryEngine'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.primary_engine'); ?></th>
                <th class="id" custom-sort order="'secondaryEngine'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.secondary_engine'); ?></th>
                <th class="id" custom-sort order="'sp'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.speed'); ?></th>
                <th class="id" custom-sort order="'odoMeterReading'" sort="sort" ng-hide="vehicleMode == 'DG' || defaultMileage=='yes'" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.odo'); ?></th>
                <th class="id" custom-sort order="'odoMeterDevice'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;" ng-if="defaultMileage=='yes'"><?php echo Lang::get('content.device_odo'); ?></th>
                <th class="id" custom-sort order="'deviceVolt'"  sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;">{{fuelType}}</th>
                <th class="id" custom-sort order="'mainBatteryVolt'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.battery'); ?></th>
                <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.gmap'); ?></th>    

              </tr>
            </thead>
            <tbody ng-repeat="fuel in fuelRawData | orderBy:natural(sort.sortingOrder):sort.reverse" ng-show="(datashow&&error=='')&&!errMsg" >
              <tr ng-hide="fuel.injected" ng-style="{ 'color':fuel.recordStatus==1?'red':'black' }">
              <td ng-show="(!error && datashow)">
  <input type="checkbox" name="fuelRawCheckbox" ng-change="getValueCheck(fuel, fuelRawData.indexOf(fuel))" ng-model="fuel.recordStatus" ng-checked="fuel.recordStatus === true">
</td>

               <td>{{fuel.dt | date:'HH:mm:ss dd-MM-yyyy'}}</td>

               <td>{{FuelValParseFloat(fuel.fuelLitr)}}</td>
               <td>{{fuel.ignitionStatus}}</td>
               <td>{{fuel.primaryEngine}}</td>
               <td>{{fuel.secondaryEngine}}</td>
               <td>{{fuel.sp}}</td>
               <td ng-hide="vehicleMode == 'DG' || defaultMileage=='yes'">{{fuel.odoMeterReading}}</td>
               <td ng-if="defaultMileage=='yes'">{{fuel.odoMeterDevice}}</td>
               <td ng-if="fuel.deviceVolt=='-1.0'">0</td>
               <td ng-if="fuel.deviceVolt!='-1.0'">{{fuel.deviceVolt}}</td>
               <td>{{fuel.mainBatteryVolt}}</td>
               <!--<td>{{fuel.address}}</td>-->    
               <td><a href="https://www.google.com/maps?q=loc:{{fuel.address}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
             </tr> 
           </tbody>
           <tr align="center" ng-if="!errMsg">
                     <!--  <td colspan="9" ng-if="error!=''" class="err" style="text-align: center;">
                           <h5><?php echo Lang::get('content.no_records_avaiable'); ?></h5>
                         </td> -->
                         <td colspan="{{ defaultMileage=='yes'?11:10}}" ng-if="error!=''" class="err" style="text-align: center;">
                           <h5>{{error}}</h5>
                         </td>
                       </tr>
                       <tr align="center" ng-if="errMsg">
                        <td colspan="{{defaultMileage=='yes'?11:10}}" class="err" ><h5>{{errMsg}}</h5></td>
                      </tr>
                    </table>  
                  </div> 
                </div>
                <!-- fuel analytics -->
                <div style="margin-top: 20px;" id="fuelMachineReport1" ng-show="fuelfill && !errMsg">
                 <table class="table table-bordered table-striped table-condensed table-hover">
                  <thead>
                    <tr style="text-align:center;">
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.group'); ?></th>
                      <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(gName)}}</th>
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total_distance'); ?></th>
                      <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(fuelMachineData.sensorMode == 'DG') ? '-' : fuelMachineData.tdistance}}</th> 
                    </tr>
                    <tr style="text-align:center;">
                     <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.start_fuel_level'); ?></th>
                     <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelMachineData.sFuelLevel}}</th>
                     <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.end_fuel_level'); ?></th>       
                     <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelMachineData.eFuelLevel}}</th>                      
                   </tr> 
                   <tr style="text-align:center;">
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.tot_fuel_fill'); ?></th>
                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(sensor=='All')? fuelMachineData.totalFuelFillforAll :roundOffDecimal(fuelMachineData.totalFuelFills)}}</th> 
                    <th colspan="2" ng-hide="fuelMachineData.sensorMode=='Dispenser'" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_theft'); ?></th>
                    <th colspan="2" ng-hide="fuelMachineData.sensorMode=='Dispenser'" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(sensor=='All')? fuelMachineData.totalFuelDropforAll :roundOffDecimal(fuelMachineData.totalFuelDrops)}}</th> 
                    <th colspan="2" ng-show="fuelMachineData.sensorMode=='Dispenser'" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_dispensed'); ?></th> 
                    <th colspan="2" ng-show="fuelMachineData.sensorMode=='Dispenser'" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{roundOffDecimal(fuelMachineData.fuelDispenser)}}</th>  
                  </tr>
                  <tr style="text-align:center;">
                    <th colspan="2"  style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.tot_fuel_consume'); ?></th>
                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelMachineData.tFuelCon}}</th> 
                    <th colspan="2" ng-if="fuelMachineData.sensorMode != 'DG'&&fuelMachineData.sensorMode != 'Machinery'" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.kmpl'); ?></th>
                    <th colspan="2" ng-if="fuelMachineData.sensorMode != 'DG'&&fuelMachineData.sensorMode != 'Machinery'" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelMachineData.kmpl}}</th> 
                    <th colspan="2" ng-if="fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery'" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.LTPH'); ?></th>
                    <th colspan="2" ng-if="fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery'" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{ fuelMachineData.lph}}</th> 
                  </tr> 
                </thead>
              </table>
              <div class="panel panel-default" style="margin-top: 20px;margin-bottom: 5px;" ng-show="sensor!='All' && theftBeta && fuelfill" >
                <div class="panel-body">
                  <span style="font-size: 14px;" ng-show="sensor!='All' && theftBeta && (fuelMachineData.sensorMode != 'DG'&&fuelMachineData.sensorMode != 'Machinery')">
                    <span style="color: #2f6581;"> <?php echo Lang::get('content.mileage'); ?> : </span>
                    <input ng-model="millage" ng-change="applyFilter()" onkeydown="validateval(event)" style="padding:3px 3px 3px 3px; font-size: 12px;width:100px;margin-left: 20px;" />

                  </span>
                  <span style="font-size: 14px;" ng-show="sensor!='All' && theftBeta && (fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery')">
                    <span style="color: #2f6581;"> <?php echo Lang::get('content.LTPH'); ?> : </span>
                    <select ng-model="ltphValue" ng-change="applyFilter()" style="padding:3px 3px 3px 3px; font-size: 12px;width:100px;margin-left: 20px;"  >
                      <option value=""><?php echo Lang::get('content.all'); ?></option>
                      <option value="3">  > 3 </option>
                      <option value="4">  > 4 </option>
                      <option value="5">  > 5 </option>
                      <option value="6">  > 6  </option>
                      <option value="7">  > 7  </option>
                      <option value="8">  > 8 </option>
                      <option value="9">  > 9 </option>
                      <option value="10">  > 10 </option>
                    </select>
                  </span>
                  <span style="font-size: 14px;margin-left: 20px;" ng-show="sensor!='All' && theftBeta">
                    <span style="color: #2f6581;"> <?php echo Lang::get('content.ign'); ?> : </span>
                    <select ng-model="ignitionOnPer" ng-change="applyFilter()" style="padding:3px 3px 3px 3px; font-size: 12px;width:100px;margin-left: 5px;"  >
                      <option value=""><?php echo Lang::get('content.all'); ?></option>
                      <option value="10">  <= 10 %</option>
                      <option value="20">  <= 20 %</option>
                      <option value="30">  <= 30 %</option>
                      <option value="40">  <= 40 %</option>
                      <option value="50">  <= 50 %</option>
                      <option value="60">  <= 60 %</option>
                      <option value="70">  <= 70 %</option>
                      <option value="80">  <= 80 %</option>
                      <option value="90">  <= 90 %</option>
                      <option value="100">  <= 100 %</option>
                    </select>
                  </span>
                  <span style="font-size: 14px;margin-left: 20px;" ng-show="sensor!='All' && theftBeta " >
                    <span style="color: #2f6581;"> <?php echo Lang::get('content.excess_consumption'); ?> Filter : </span>
                        <!-- <label class="rate-hit" style="margin-left: 5px;">
                           <input type="radio" ng-model="showFueltheftFilter" value="ON" ng-checked="showFueltheftFilter=='ON'">
                           ON
                       </label>
                       &nbsp;&nbsp;
                       <label class="rate-miss">
                           <input type="radio" ng-model="showFueltheftFilter" value="OFF" ng-checked="showFueltheftFilter=='OFF'">
                           OFF 
                         </label> -->
                         <label class="switch">
                          <input type="checkbox" ng-click="handleFuelFilter('btnClick')" ng-checked="showFueltheftFilter">
                          <span class="slider round"></span>
                        </label>

                      </span>
                      <span style="font-size: 14px;margin-left: 20px;" ng-show="showFueltheftFilter && sensor!='All' && theftBeta " >
                        <select ng-model="minFuelTheft" ng-change="applyFilter()" style="padding:3px 3px 3px 3px; font-size: 12px;width:100px;margin-left: 5px;">
                          <option value=""><?php echo Lang::get('content.all'); ?> </option>
                          <option value="5">  >= 5 </option>
                          <option value="10">  >= 10  </option>
                          <option value="20">  >= 20  </option>
                          <option value="30">  >= 30 </option>
                          <option value="40">  >= 40 </option>
                          <option value="50">  >= 50 </option>
                          <option value="others"> Others </option>
                        </select>
                      </span>
                      <span style="font-size: 14px;margin-left: 20px;" ng-show="showFueltheftFilter && minFuelTheft=='others' && sensor!='All' && theftBeta" >
                        <input type="number" ng-model="customMinFuelTheftFilterValue" ng-change="applyFilter()" placeholder="Enter Fuel in ltrs">
                      </span>
                    </div>
                  </div>

                  <div style="padding-top: 15px;" ng-hide="sensor=='All'"> 
                    <div style="text-align: right;padding-bottom: 5px;padding-right: 5px;">
                    </div>
                    <tabset class="nav-tabs-custom">

                      <tab heading="<?php echo Lang::get('content.fuel_fill'); ?>" active="true" ng-click="handleFuelTheftBeta('no')">

                        <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top: 10px;"> 
                          <thead class='header' style=" z-index: 1;">
                            <tr style="text-align:center">
                              <th class="id" custom-sort order="'time'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.from'); ?> <?php echo Lang::get('content.time'); ?></th>
                              <th class="id" custom-sort order="'time'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.to'); ?> <?php echo Lang::get('content.time'); ?></th>
                              <th class="id" custom-sort order="'pFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.pre_fuel'); ?></th>
                              <th class="id" custom-sort order="'cFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.cur_fuel'); ?></th>
                              <th  width="25%" ng-show="fuelMachineData.sensorMode=='Moving Vehicle' && fuelMachineData.tankInterlinked!='yes' &&
                              fuelMachineData.consumption=='yes'"  class="id" custom-sort order="'fuelConsumption'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><span><?php echo Lang::get('content.consumption_during_fuel_fill'); ?></span>
                              <span><i style="margin-left: 10px;color:#2f6581;font-size: 15px;" class="fa fa-info-circle tooltip-color" aria-hidden="true" data-toggle="tooltip" title="
                                <?php echo Lang::get('content.consumption_during_fuel_fill'); ?> = <?php echo Lang::get('content.distance_covered'); ?> / <?php echo Lang::get('content.fuel'); ?> <?php echo Lang::get('content.average'); ?>"></i></span>
                              </th>
                              <th class="id" custom-sort order="'lt'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.fuel_fill'); ?></th>                              
                              <th class="id" custom-sort order="'odo'" sort="sort" ng-hide="fuelMachineData.sensorMode == 'DG'" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.odo'); ?></th>
                              <th class="id" custom-sort order="'address'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.address'); ?></th>  
                              <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;" class="loc"><?php echo Lang::get('content.gmap'); ?></th>                    
                            </tr>
                          </thead>
                          <tbody ng-repeat="fuel in fuelMachineData.fuelFills | orderBy:natural(sort.sortingOrder):sort.reverse" ng-style="{ 'color':fuel.position=='U'?'red':'black' }">
                            <tr ng-if="!errMsg.includes('expired')" >
                              <td>{{fuel.startTime | date:'HH:mm:ss'}}&nbsp;&nbsp;&nbsp;{{fuel.startTime | date:'dd-MM-yyyy'}}</td>
                              <td>{{fuel.time | date:'HH:mm:ss'}}&nbsp;&nbsp;&nbsp;{{fuel.time | date:'dd-MM-yyyy'}}</td>
                              <td>{{fuel.pFuel}}</td>                    
                              <td>{{fuel.cFuel}}</td>
                              <td width="25%" ng-show="fuelMachineData.sensorMode=='Moving Vehicle' && fuelMachineData.tankInterlinked!='yes' &&
                              fuelMachineData.consumption=='yes'">{{roundOffDecimal(fuel.fuelConsumption)}}</td>
                              <td>{{roundOffDecimal(fuel.lt)}}</td>
                              <td ng-hide="fuelMachineData.sensorMode == 'DG'">{{fuel.odo}}</td> 
                              <td ng-if="fuel.address!=null">{{fuel.address}}</td> 
                              <td style="text-decoration: underline;" ng-if="fuel.address==null"><a href="https://www.google.com/maps?q=loc:{{fuel.lat}},{{fuel.lng}}" target="_blank">{{fuel.lat}},{{fuel.lng}}</a></td> 
                              <td><a href="https://www.google.com/maps?q=loc:{{fuel.lat}},{{fuel.lng}}" target="_blank">Link</a></td>
                            </tr> 
                          </tbody>
                          <tr align="center" ng-if="!errMsg">
                            <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 10 : fuelMachineData.sensorMode=='Moving Vehicle' && fuelMachineData.tankInterlinked!='yes' &&
                            fuelMachineData.consumption=='yes'?12:11 }}" ng-if="(fuelMachineData.fuelFills==null||fuelMachineData.fuelFills.length==0)&&!fuelMachineData.error" class="err" style="text-align: center;">
                            <h5><?php echo Lang::get('content.no_records_avaiable'); ?></h5>
                          </td>
                          <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 10 : fuelMachineData.sensorMode=='Moving Vehicle' && fuelMachineData.tankInterlinked!='yes' &&
                          fuelMachineData.consumption=='yes'?12:11 }}" ng-if="(fuelMachineData.fuelFills==null||fuelMachineData.fuelFills.length==0)&&fuelMachineData.error" class="err" style="text-align: center;">
                          <h5>{{fuelMachineData.error}}</h5>
                        </td>
                      </tr>
                      <tr align="center" ng-if="errMsg">
                        <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 10 : fuelMachineData.sensorMode=='Moving Vehicle' && fuelMachineData.tankInterlinked!='yes' &&
                        fuelMachineData.consumption=='yes'?12:11 }}" class="err" ><h5>{{errMsg}}</h5></td>
                      </tr>
                    </table> 

                  </tab>

                  <tab heading="<?php echo Lang::get('content.fuel_theft'); ?>" active="false" ng-hide="fuelMachineData.sensorMode=='Dispenser'" ng-click="handleFuelTheftBeta('no')">


                    <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top: 10px;"> 
                      <thead class='header' style=" z-index: 1;">
                        <tr style="text-align:center">
                          <th class="id" custom-sort order="'startTime'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.from'); ?> <?php echo Lang::get('content.time'); ?></th>
                          <th class="id" custom-sort order="'time'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.to'); ?> <?php echo Lang::get('content.time'); ?></th>
                          <th class="id" custom-sort order="'pFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.pre_fuel'); ?></th>
                          <th class="id" custom-sort order="'cFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.cur_fuel'); ?></th>  <th class="id" custom-sort order="'lt'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.fuel_theft'); ?></th>
                          <th class="id" custom-sort order="'odo'" sort="sort" ng-hide="fuelMachineData.sensorMode == 'DG'" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.odo'); ?></th>
                          <th class="id" custom-sort order="'address'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.address'); ?></th>
                          <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;" class="loc"> <?php echo Lang::get('content.gmap'); ?></th>                    
                        </tr>
                      </thead>
                      <tbody ng-repeat="fuel in fuelMachineData.fuelDrops | orderBy:natural(sort.sortingOrder):sort.reverse" ng-style="{ 'color':fuel.position=='U'?'red':'black' }">
                        <tr ng-if="!errMsg.includes('expired')" >
                          <td>{{fuel.startTime | date:'HH:mm:ss'}}&nbsp;&nbsp;&nbsp;{{fuel.startTime | date:'dd-MM-yyyy'}}</td>
                          <td>{{fuel.time | date:'HH:mm:ss'}}&nbsp;&nbsp;&nbsp;{{fuel.time | date:'dd-MM-yyyy'}}</td>
                          <td>{{fuel.pFuel}}</td>
                          <td>{{fuel.cFuel}}</td>                   
                          <td>{{roundOffDecimal(fuel.lt)}}</td>
                          <td ng-hide="fuelMachineData.sensorMode == 'DG'">{{fuel.odo}}</td>      
                          <td ng-if="fuel.address!=null">{{fuel.address}}</td> 
                          <td style="text-decoration: underline;" ng-if="fuel.address==null"><a href="https://www.google.com/maps?q=loc:{{fuel.lat}},{{fuel.lng}}" target="_blank">{{fuel.lat}},{{fuel.lng}}</a></td>  
                          <td><a href="https://www.google.com/maps?q=loc:{{fuel.lat}},{{fuel.lng}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>                  
                        </tr> 
                      </tbody>
                      <tr align="center" ng-if="!errMsg">
                        <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 10 : 11 }}" ng-if="(fuelMachineData.fuelDrops==null||fuelMachineData.fuelDrops.length==0)&&!fuelMachineData.error" class="err" style="text-align: center;">
                         <h5><?php echo Lang::get('content.no_records_avaiable'); ?></h5>
                       </td>
                       <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 10 : 11 }}" ng-if="(fuelMachineData.fuelDrops==null||fuelMachineData.fuelDrops.length==0)&&fuelMachineData.error" class="err" style="text-align: center;">
                         <h5>{{fuelMachineData.error}}</h5>
                       </td>
                     </tr>
                     <tr align="center" ng-if="errMsg">
                      <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 10 : 11 }}" class="err" ><h5>{{errMsg}}</h5></td>
                    </tr>
                  </table> 


                </tab>
                  <!-- <tab heading="<?php echo Lang::get('content.fuel_fill'); ?> <?php echo Lang::get('content.beta'); ?>" active="false" ng-click="handleFuelTheftBeta('no')">

                    <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top: 10px;"> 
                      <thead class='header' style=" z-index: 1;">
                      <tr style="text-align:center">
                        <th class="id" custom-sort order="'time'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.from'); ?> <?php echo Lang::get('content.time'); ?></th>
                        <th class="id" custom-sort order="'time'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.to'); ?> <?php echo Lang::get('content.time'); ?></th>
                        <th class="id" custom-sort order="'pFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.pre_fuel'); ?></th>
                        <th class="id" custom-sort order="'cFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.cur_fuel'); ?></th>
                        <th class="id" custom-sort order="'lt'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.fuel_fill'); ?> <?php echo Lang::get('content.beta'); ?></th>
                        <th class="id" custom-sort order="'odo'" sort="sort" ng-hide="vehicleMode == 'DG'" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.odo'); ?></th>    
                        <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;" class="loc"><?php echo Lang::get('content.gmap'); ?></th>                    
                      </tr>
                      </thead>
                      <tbody ng-repeat="fuel in fuelMachineData.fuelFillBeta | orderBy:natural(sort.sortingOrder):sort.reverse">
                        <tr>
                          <td>{{fuel.startTime | date:'HH:mm:ss'}}   {{fuel.startTime | date:'dd-MM-yyyy'}}</td>
                          <td>{{fuel.time | date:'HH:mm:ss'}}   {{fuel.time | date:'dd-MM-yyyy'}}</td>
                          <td>{{fuel.pFuel}}</td>                    
                          <td>{{fuel.cFuel}}</td>
                          <td>{{roundOffDecimal(fuel.lt)}}</td>
                          <td ng-hide="vehicleMode == 'DG'">{{fuel.odo}}</td>   
                          <td><a href="https://www.google.com/maps?q=loc:{{fuel.lat}},{{fuel.lng}}" target="_blank" class="loc">Link</a></td>                  
                        </tr> 
                      </tbody>
                        <tr align="center" ng-if="!errMsg">
                            <td colspan="{{ vehicleMode == 'DG'? 7 : 8 }}" ng-if="(fuelMachineData.fuelFillBeta==null||fuelMachineData.fuelFillBeta.length==0)&&!fuelMachineData.error" class="err" style="text-align: center;">
                                 <h5><?php echo Lang::get('content.no_records_avaiable'); ?></h5>
                            </td>
                            <td colspan="{{ vehicleMode == 'DG'? 7 : 8 }}" ng-if="(fuelMachineData.fuelFillBeta==null||fuelMachineData.fuelFills.length==0)&&fuelMachineData.error" class="err" style="text-align: center;">
                                 <h5>{{fuelMachineData.error}}</h5>
                            </td>
                        </tr>
                        <tr align="center" ng-if="errMsg">
                            <td colspan="{{ vehicleMode == 'DG'? 7 : 8 }}" class="err" ><h5>{{errMsg}}</h5></td>
                        </tr>
                    </table> 

                  </tab> --> 
                  <tab heading="<?php echo Lang::get('content.consumption'); ?> <?php echo Lang::get('content.rate'); ?>" ng-hide="fuelMachineData.sensorMode=='Dispenser'" active="false" ng-click="handleFuelTheftBeta('yes')">


                    <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top: 10px;"> 
                      <thead class='header' style=" z-index: 1;">
                        <tr style="text-align:center">
                          <th class="id" custom-sort order="'startTime'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.from'); ?> <?php echo Lang::get('content.time'); ?></th>
                          <th class="id" custom-sort order="'endTime'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.to'); ?> <?php echo Lang::get('content.time'); ?></th>
                          <th class="id" custom-sort order="'ignitionOnPer'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.ignition'); ?> <?php echo Lang::get('content.on'); ?> %</th>
                          <th ng-hide="fuelMachineData.sensorMode == 'DG'" class="id" custom-sort order="'distance'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.distance'); ?> (Kms)</th> 
                          <th class="id" custom-sort order="'startFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.pre_fuel'); ?> (Ltrs)</th>
                          <th class="id" custom-sort order="'endFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.cur_fuel'); ?> (Ltrs)</th> 
                          <th class="id" custom-sort order="'totalConsumption'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.actual_consumption'); ?> (Ltrs)</th>
                        </th> 
                        <th custom-sort order="'fuelMileage'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;">
                          <span ng-show="fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery'"><?php echo Lang::get('content.ltrs_per_hrs'); ?> </span>
                          <span ng-hide="fuelMachineData.sensorMode == 'Machinery' || fuelMachineData.sensorMode == 'DG'"><?php echo Lang::get('content.kmpl'); ?> </span>
                          <th class="id" custom-sort order="'fuelConsume'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.expected_consumption'); ?> (Ltrs)</th>
                          <th class="id" custom-sort order="'fuelTheftBeta'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.excess_consumption'); ?> (Ltrs)</th>
                          <th class="id" custom-sort order="'startAddress'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.start_loca'); ?></th>
                          <th class="id" custom-sort order="'endAddress'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.end_loca'); ?></th>
                        </tr>
                      </thead>
                      <tbody ng-repeat="fuel in fuelRunningTheft | orderBy:natural(sort.sortingOrder):sort.reverse" ng-style="{ 'color':fuel.position=='U'?'red':'black' }">
                        <tr ng-if="!errMsg.includes('expired')" >
                          <td>{{fuel.startTime | date:'HH:mm:ss'}}   {{fuel.startTime | date:'dd-MM-yyyy'}}</td>
                          <td>{{fuel.endTime | date:'HH:mm:ss'}}   {{fuel.endTime | date:'dd-MM-yyyy'}}</td>
                          <td>{{fuel.ignitionOnPer}}</td>
                          <td ng-hide="fuelMachineData.sensorMode == 'DG'"> {{fuel.distance }} </td> 
                          <td>{{fuel.startFuel}}</td>
                          <td>{{fuel.endFuel}}</td> 
                          <td>{{fuel.totalConsumption }} </td>
                          <td> 
                            <span ng-show="fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery'">{{fuel.ltrsphr}}</span>
                            <span ng-hide="fuelMachineData.sensorMode == 'Machinery' ||fuelMachineData.sensorMode == 'DG'">{{fuel.fuelMileage}}</span>
                          </td>                 
                          <td>{{roundOffDecimal(fuel.fuelConsume)}}</td>
                          <td>{{roundOffDecimal(fuel.fuelTheftBeta)}}</td>
                          <td ng-if="fuel.startAddress!=null && fuel.startAddress!='-'">{{fuel.startAddress}}</td>
                          <td style="text-decoration: underline;" ng-if="fuel.startAddress==null || fuel.startAddress=='-'"><a href="https://www.google.com/maps?q=loc:{{fuel.startLat}},{{fuel.startLng}}" target="_blank">{{fuel.startLat}},{{fuel.startLng}}</a></td> 
                          <td ng-if="fuel.endAddress!=null && fuel.endAddress!='-'">{{fuel.endAddress}}</td>
                          <td style="text-decoration: underline;" ng-if="fuel.endAddress==null || fuel.endAddress=='-'"><a href="https://www.google.com/maps?q=loc:{{fuel.endLat}},{{fuel.endLng}}" target="_blank">{{fuel.endLat}},{{fuel.endLng}}</a></td> 
                        </tr> 
                        
                      </tbody>
                      <tr ng-show="fuelRunningTheft.length!=0">
                        <td colspan="{{(fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery')?10:11}}" style="text-align: right;"> <b><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.excess_consumption'); ?> (Ltrs) </b> </td>
                        <td colspan="1"><b>{{roundOffDecimal(totalFuelTheftBeta)}}</b></td>
                      </tr>
                      <tr align="center" ng-if="!errMsg">
                        <td colspan="{{(fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery')?13:14}}" ng-if="(fuelRunningTheft==null||fuelRunningTheft.length==0)&&!fuelMachineData.error" class="err" style="text-align: center;">
                         <h5><?php echo Lang::get('content.no_records_avaiable'); ?></h5>
                       </td>
                       <td colspan="{{(fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery')?13:14}}" ng-if="(fuelRunningTheft==null||fuelRunningTheft.length==0)&&fuelMachineData.error" class="err" style="text-align: center;">
                         <h5>{{fuelMachineData.error}}</h5>
                       </td>
                     </tr>
                     <tr align="center" ng-if="errMsg">
                      <td colspan="{{(fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery')?13:14}}" class="err" ><h5>{{errMsg}}</h5></td>
                    </tr>
                  </table> 


                </tab>


              </tabset>
            </div>

          </div>
        </div>
      </div>

    </div>
  </div>

</div>
</div>


<script src="assets/js/static.js"></script>
<!--<script src="assets/js/jquery-1.11.0.js"></script>-->
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<!--<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>-->
<!--<script src="assets/js/bootstrap.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script data-require="angular-ui-bootstrap@0.11.0" data-semver="0.11.0" src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>
<script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
<script src="assets/js/angular-translate.js"></script>
<script src="../resources/views/reports/customjs/html5csv.js"></script>   
<script src="../resources/views/reports/customjs/moment.js"></script>
<script src="../resources/views/reports/customjs/FileSaver.js"></script>
<script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
<!--  <script src="../resources/views/reports/datatable/jquery.dataTables.js"></script> -->
<script src="assets/js/highcharts_fuel.js"></script>
<script src="assets/js/exporting.js"></script>
<script src="assets/js/export-data.js"></script>
<!--<script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>-->
  <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
  <script src='assets/js/table-fixed-header.js'></script>
  <!--<script src="assets/js/naturalSortVersionDates.js"></script>-->
  <script src="assets/js/vamoApp.js"></script>
  <script src="assets/js/services.js"></script>
  <script src="assets/js/fuelRaw.js?v=<?php echo Config::get('app.version');?>"></script>

  <script>
    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();
    });
        // $("#example1").dataTable();
        $("#menu-toggle").click(function(e) {
          e.preventDefault();
          $("#wrapper").toggleClass("toggled");
        });

        
        $("#menu-toggle").click(function(e) {
          e.preventDefault();
          $("#wrapper").toggleClass("toggled");
        });

        var generatePDF = function() {
          kendo.drawing.drawDOM($("#formConfirmation"),{paperSize: "A4",multiPage: true,margin: {
            left   : "1mm",
            top    : "5mm",
            right  : "1mm",
            bottom : "5mm"
          } ,landscape: true }).then(function(group) {
            kendo.drawing.pdf.saveAs(group, "FuelRawReport.pdf");
          });
        }

        function validateval(event){
          let valid = ( (event.keyCode >= 48 && event.keyCode <= 57) || event.keyCode == 8|| event.keyCode == 13 || event.keyCode == 0 || event.keyCode==9 );
          if (!valid) {
            event.preventDefault();
          }
        }


      </script>

    </body>
    </html>
