 <div>
                   
    <button type="button" class="btn btn-primary" ng-click="durationFilter('today')" ng-disabled="todayDisabled"><?php echo Lang::get('content.today'); ?></button>
    <button type="button" class="btn btn-primary" ng-click="durationFilter('yesterday')" ng-disabled="yesterdayDisabled"><?php echo Lang::get('content.yesterday'); ?></button>
    <button type="button" class="btn btn-primary" ng-click="durationFilter('week')" ng-disabled="weekDisabled"><?php echo Lang::get('content.week'); ?></button>
    <button type="button" class="btn btn-primary" ng-click="durationFilter('month')" ng-disabled="monthDisabled"><?php echo Lang::get('content.month'); ?></button>
</div>
 