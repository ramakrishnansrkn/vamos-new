<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="Satheesh">
  <title><?php echo Lang::get('content.gps'); ?></title>
  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="../resources/views/reports/datepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
  <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-2X3F316479');
  </script>
  <style>
/*  .box > .loading-img {
           z-index: 1020;
           background: transparent url('assets/imgs/status.gif') 50% 50% no-repeat;
           }  */
           .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
            background-color: #ffffff;
          }

          body{
            font-family: 'Lato', sans-serif;
/*font-weight: bold;
font-family: 'Lato', sans-serif;
font-family: 'Roboto', sans-serif;
font-family: 'Open Sans', sans-serif;
font-family: 'Raleway', sans-serif;
font-family: 'Faustina', serif;
font-family: 'PT Sans', sans-serif;
font-family: 'Ubuntu', sans-serif;
font-family: 'Droid Sans', sans-serif;
font-family: 'Source Sans Pro', sans-serif;
*/
}
.col-md-12 {
  width: 99.5%;
  margin-left: 8px;
}
</style>
</head>
<div id="preloader" >
  <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
  <div id="status02">&nbsp;</div>
</div>
<body ng-app="mapApp">
  <div id="wrapper" ng-controller="histCtrl" class="ng-cloak" style="overflow: hidden;">

   <?php include('sidebarList.php');?>

   <div id="testLoad"></div>
   <div id="page-content-wrapper">
    <div class="container-fluid">
      <div class="panel panel-default">
      </div>          
    </div>
    <!-- /#page-content-wrapper -->
  </div>

  <div ng-show="reportBanShow" class="modal fade" id="allReport" role="dialog" style="top: 100px">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <p class="err" style="text-align: center;"> <?php echo Lang::get('content.premium_user'); ?></p>
        </div>
      </div>
    </div>
  </div> 

  <div ng-show="reportBanShow" class="col-md-10" >
    <div class="box box-primary" style="height:90px; padding-top:30px; margin-top:5%; margin-left:8%;">
      <p ><h5 class="err" style="text-align: center;"> <?php echo Lang::get('content.no_report_found'); ?> </h5></p>
    </div>
  </div>

  <!-- AdminLTE css box-->
  <div class="col-md-12" ng-hide="reportBanShow" style="top:30px;">
    <div class="box box-primary">
      <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
        <h3 class="box-title"><?php echo Lang::get('content.his_report'); ?> 
      </h3>
      <div class="box-tools pull-right helpVideo">
        <div class="pull-right" ng-if="tn=='noData'" style="padding-left: 10px;">
         <?php include('helpVideos.php');?>
       </div>
       <div class="box-tools pull-right" ng-if="tn=='parked'" style="padding-left: 10px;">
         <?php include('helpVideos.php');?>
       </div>
       <div class="box-tools pull-right" ng-if="tn=='stoppageReport'" style="padding-left: 10px;">
         <?php include('helpVideos.php');?>
       </div>
       <div class="box-tools pull-right" ng-if="tn=='movement'" style="padding-left: 10px;">
         <?php include('helpVideos.php');?>
       </div>
       <div class="box-tools pull-right" ng-if="tn=='ignition'" style="padding-left: 10px;">
         <?php include('helpVideos.php');?>
       </div>
       <div class="box-tools pull-right" ng-if="tn=='idle'" style="padding-left: 10px;">
         <?php include('helpVideos.php');?>
       </div>
       <div class="box-tools pull-right" ng-if="tn=='all'" style="padding-left: 10px;">
         <?php include('helpVideos.php');?>
       </div>
       <div class="box-tools pull-right" ng-if="tn=='overspeed'" style="padding-left: 10px;">
         <?php include('helpVideos.php');?>
       </div>

       <span class="pull-right" style="padding-top: 2px;"><a href="reports"><input class="btn btn-xs btn-danger pull-right" type="button" value="<?php echo Lang::get('content.reports'); ?>"></a></span> 

     </div>

     <div ng-hide="true" class="box-tools pull-right">
      <a href="downloadhistory?vid={{id}}&rid={{downloadid}}" target="_blank"><img src="../resources/views/reports/image/Adobe.png" /></a>
      <img style="cursor: pointer;" ng-click="exportData(downloadid)"  src="../resources/views/reports/image/xls.png" />
      <!-- <img style="cursor: pointer;" onclick="generatePDF('formConfirmation')"  src="../resources/views/reports/image/Adobe.png" /> -->
    </div>
  </div>
  <div class="row">
    <div class="col-md-2" align="center">
      <div class="form-group" >
        <h5 style="color: grey;">{{shortNam}}</h5>
      </div>
                                   <!-- <div class="form-group" ng-if="shortNam==undefined || shortNam==null">
                                        <h5 style="color: red;">{{vehiLabel}} not found </h5>
                                      </div> -->
                                    </div>
                                    <div class="col-md-2" align="center">
                                      <div class="form-group">
                                        <div class="input-group datecomp">
                                          <input type="text" ng-model="fromdate" class="form-control placholdercolor" id="dateFrom" placeholder="<?php echo Lang::get('content.fromdate'); ?>">
                                          <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-2" align="center">
                                      <div class="form-group">
                                        <div class="input-group datecomp">
                                          <input type="text" ng-model="fromtime" class="form-control placholdercolor" id="timeFrom" placeholder="<?php echo Lang::get('content.from_time'); ?>">
                                          <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-2" align="center">
                                      <div class="form-group">
                                        <div class="input-group datecomp">
                                          <input type="text" min-date="fromdate" ng-model="todate" class="form-control placholdercolor" id="dateTo" placeholder="<?php echo Lang::get('content.todate'); ?>">
                                          <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-2" align="center">
                                      <div class="form-group">
                                        <div class="input-group datecomp">
                                          <input type="text" ng-model="totime" class="form-control placholdercolor" id="timeTo" placeholder="<?php echo Lang::get('content.to_time'); ?>">
                                          <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                                        </div>
                                      </div>
                                    </div>


                                    <div class="col-md-1" align="center">
                                      <div class="form-group">
                                        <div class="input-group datecomp">
                                          <select ng-change="intervalChange()" class="input-sm form-control" ng-model="interval">
                                           <option value=""><?php echo Lang::get('content.interval'); ?></option>
                                           <option label="1 <?php echo Lang::get('content.min'); ?>">1</option>
                                           <option label="2 <?php echo Lang::get('content.Mins'); ?>">2</option>
                                           <option label="5 <?php echo Lang::get('content.Mins'); ?>">5</option>
                                           <option label="10 <?php echo Lang::get('content.Mins'); ?>">10</option>
                                           <option label="15 <?php echo Lang::get('content.Mins'); ?>">15</option>
                                           <option label="30 <?php echo Lang::get('content.Mins'); ?>">30</option>
                                         </select>
                                       </div>
                                     </div>
                                   </div> 


                                   <div class="col-md-1" align="center">
                                     <div class="form-group">
                                       <button ng-click="plotHist()" style="margin-left: -30%; margin-top: 2px;"><?php echo Lang::get('content.submit'); ?></button>
                                     </div>
                                   </div>
                                   <div class="col-md-12" align="right" style="margin-bottom: 10px;">
                                    <?php include('DateButtonWithWeekMonth.php');?>
                                  </div>
                                </div>

                                <div class="box-body" ng-class="overallEnable?'col-md-9':'col-md-12'" id="statusreport">
                                 <!-- Tabset -->
                                 <div id="formConfirmation">
                                   <tabset class="nav-tabs-custom">
                                    <tab ng-show="tabMovementShow" select="alertMe('All')" heading="<?php echo Lang::get('content.all'); ?>" active="tabAll" >
                                     <p style="margin:0" class="page-header"> <?php echo Lang::get('content.all'); ?> <span style="float: right;font-size:15px;padding-right: 15px;"><b><?php echo Lang::get('content.from'); ?></b> : &nbsp;{{(fromtime)}} &nbsp;{{fromdate|date:'dd-MM-yyyy'}} &nbsp;&nbsp; - &nbsp;&nbsp; <b><?php echo Lang::get('content.to'); ?></b> :&nbsp; {{(totime)}} &nbsp;{{todate|date:'dd-MM-yyyy'}}</span></p>

                                     <div id='allreport' class="box-body table-responsive">

                                       <table class="table table-bordered table-striped table-condensed table-hover">
                                        <thead>
                                          <tr style="text-align:center;">
                                            <th colspan="2" style="background-color:#ecf7fb;">{{vehiLabel | translate}} <?php echo Lang::get('content.group'); ?></th>
                                            <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(gName)}}</th>
                                            <th style="background-color:#ecf7fb;">{{vehiLabel | translate}} <?php echo Lang::get('content.name'); ?></th>
                                            <th colspan="3" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{shortNam}}</th>                 
                                          </tr>
                                          <tr style="text-align:center;">
                                            <th colspan="2" style="background-color:#f9f9f9;"><?php echo Lang::get('content.reg_no'); ?></th>
                                            <th colspan="2" style="background-color:#ecf7fb;font-weight: unset;font-size:12px;">{{registerNo}}</th>
                                            <th style="background-color:#f9f9f9;"><?php echo Lang::get('content.trip_distance'); ?></th>
                                            <th colspan="3" style="background-color:#ecf7fb;font-weight: unset;font-size:12px;">{{ (vehicleMode == 'DG') ? '-' : hist.tripDistance}} {{ (vehicleMode == 'DG') ? '' : Kms}} </th>
                                          </tr>
                                          <tr ng-if="vehiAssetView" style="text-align:center;">
                                            <th colspan="2" style="background-color:#ecf7fb;"><?php echo Lang::get('content.opening_odoreading'); ?></th>
                                            <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{  (vehicleMode == 'DG') ? '-' : hist.openingOdoReading}}</th>
                                            <th style="background-color:#ecf7fb;"><?php echo Lang::get('content.closing_odoreading'); ?></th>
                                            <th colspan="3" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{ (vehicleMode == 'DG') ? '-' : hist.closingOdoReading}}</th>
                                          </tr>
                                          <tr ng-hide="true" style="text-align:center;" >
                                            <td colspan="2"><b><?php echo Lang::get('content.fromdate'); ?></b></td>
                                            <td>{{fromdate}}</td>
                                            <td colspan="1"><b><?php echo Lang::get('content.from'); ?> <?php echo Lang::get('content.time'); ?></b></td>
                                            <td colspan="3">{{fromtime}}</td>   
                                          </tr>
                                          <tr ng-hide="true" style="text-align:center;">
                                            <td colspan="2"><b><?php echo Lang::get('content.todate'); ?></b></td>
                                            <td>{{todate}}</td>
                                            <td colspan="1"><b><?php echo Lang::get('content.to'); ?> <?php echo Lang::get('content.time'); ?></b></td>
                                            <td colspan="3">{{totime}}</td>
                                          </tr>
                                        </thead>
                                      </table>
                                      <div class="col-md-12" style="height: 15px;"></div>
                                      <!--    <tr><td colspan="8"></td></tr> -->
                                      <table class="table table-bordered table-striped table-condensed table-hover table-fixed-header1"> 
                                        <thead class='header' style=" z-index: 1;">
                                          <tr style="text-align:center">
                                            <th width="12%" class="id" custom-sort order="'date'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.date_time'); ?></th>
                                            <th width="8%" class="id" custom-sort order="'speed'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.speed'); ?></th>
                                            <th width="8%" class="id" custom-sort order="'powerStatus'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.voltage'); ?></th>
                                            <!-- <th width="6%" class="id" custom-sort order="'gsmLevel'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;">Sat</th> -->
                                            <th width="10%" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.lat_long'); ?></th>
                                            <th width="6%" class="id" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.gmap'); ?></th>
                                            <th width="28%" class="id" custom-sort order="'address'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.Nearest_Loc'); ?></th>
                                            <th ng-hide="vehicleMode == 'DG'" width="8%" class="id" custom-sort order="'tmpDistance'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.dist'); ?></th>
                                            <th ng-hide="vehicleMode == 'DG'" width="12%" class="id" custom-sort order="'distanceCovered'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.cdist'); ?> <?php echo Lang::get('content.KMS'); ?></th>
                                            <th ng-show="vehicleMode != 'DG' && vehiAssetView" width="10%" class="id" custom-sort order="'odoDistance'" sort="sort"  style="text-align:center;height:3px;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.odo'); ?> <?php echo Lang::get('content.KMS'); ?></th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <tr ng-repeat="all in allData | orderBy:natural(sort.sortingOrder):sort.reverse" class="active" style="text-align:center" ng-if="!errMsg.includes('expired')">
                                            <td>{{all.date | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                                            <td>{{all.speed}}</td>
                                            <td ng-if="all.powerStatus!='null'">{{all.powerStatus}}</td>
                                            <td ng-if="all.powerStatus=='null'">-</td>
                                            <!-- <td>{{move.gsmLevel}}</td> -->
                                            <td>{{all.latitude}},{{all.longitude}}</td>
                                            <td><a href="https://www.google.com/maps?q=loc:{{all.latitude}},{{all.longitude}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
                                            <td>
                                              <p ng-if="all.address!=null">{{all.address}}</p>
                                              <p ng-if="all.address==null && maddress1[$index]!=null">{{maddress1[$index]}}</p>
                                              <p ng-if="all.address==null && maddress1[$index]==null"><img src="assets/imgs/loader.gif" align="middle"></p>
                                            </td>
                                            <!-- <td style="cursor: pointer;" get-location lat={{user.latitude}} lon={{user.longitude}} index={{$index}} ng-if="user.address==null && maddress[$index]==null">Click Me</td> -->
                                            <!-- <td get-location lat={{user.latitude}} lon={{user.longitude}} index={{$index}} ng-if="user.address==null && maddress1[$index]!=null">{{maddress1[$index]}}</td> -->
                                            <td ng-hide="vehicleMode == 'DG'">{{all.tmpDistance}}</td>
                                            <td ng-hide="vehicleMode == 'DG'">{{all.distanceCovered}}</td>
                                            <td ng-show="vehicleMode != 'DG' && vehiAssetView">{{all.odoDistance}}</td>
                                          </tr>   
                                          <tr ng-if="errMsg==null " align="center" ng-show="showErrMsg">
                                            <td ng-if="allData == null || allData.length==0" colspan="{{ vehicleMode == 'DG'? 7 : 10}}" class="err"><h5><?php echo Lang::get('content.zero_records'); ?></h5></td>
                                          </tr>
                                          <tr ng-if="errMsg != null " align="center" ng-show="showErrMsg">
                                            <td colspan="{{ vehicleMode == 'DG'? 7 : 10}}" class="err"><h5>{{errMsg}}</h5></td>
                                          </tr>
                                        </table>

                            <!-- <pagination boundary-links="true" max-size="3" items-per-page="itemsPerPage" total-items="movementdata.length" ng-model="currentPage" ng-change="pageChanged()"></pagination>  
                            -->
                          </div>
                        </tab> 


                        <tab ng-show="tabMovementShow" select="alertMe('Movement')" heading="<?php echo Lang::get('content.movement'); ?>" active="tabmovement" >
                         <p style="margin:0" class="page-header"><?php echo Lang::get('content.movement'); ?> <?php echo Lang::get('content.report'); ?> <span style="float: right;font-size:15px;padding-right: 15px;"><b><?php echo Lang::get('content.from'); ?></b> : &nbsp;{{(fromtime)}} &nbsp;{{fromdate|date:'dd-MM-yyyy'}} &nbsp;&nbsp; - &nbsp;&nbsp; <b><?php echo Lang::get('content.to'); ?></b> :&nbsp; {{(totime)}} &nbsp;{{todate|date:'dd-MM-yyyy'}}</span></p>
                         <div id='movementreport' class="box-body table-responsive">
                          <!-- <div id="formConfirmation"> -->
                           <table class="table table-bordered table-striped table-condensed table-hover">
                            <thead>
                              <tr style="text-align:center;">
                                <th colspan="2" style="background-color:#ecf7fb;">{{vehiLabel | translate}} <?php echo Lang::get('content.group'); ?></th>
                                <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(vgroup)}}</th>
                                <th style="background-color:#ecf7fb;">{{vehiLabel | translate}} <?php echo Lang::get('content.name'); ?></th>
                                <th colspan="3" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{hist.shortName}}</th>                 
                              </tr>
                              <tr style="text-align:center;">
                                <th colspan="2" style="background-color:#f9f9f9;"><?php echo Lang::get('content.reg_no'); ?></th>
                                <th colspan="2" style="background-color:#ecf7fb;font-weight: unset;font-size:12px;">{{registerNo}}</th>
                                <th style="background-color:#f9f9f9;"><?php echo Lang::get('content.trip_distance'); ?></th>
                                <th colspan="3" style="background-color:#ecf7fb;font-weight: unset;font-size:12px;">{{ (vehicleMode == 'DG') ? '-' : hist.tripDistance}} {{ (vehicleMode == 'DG') ? '' : Kms}}</th>
                              </tr>
                              <tr ng-if="vehiAssetView" style="text-align:center;">
                                <th colspan="2" style="background-color:#ecf7fb;"><?php echo Lang::get('content.opening_odoreading'); ?></th>
                                <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{ (vehicleMode == 'DG') ? '-' : hist.openingOdoReading}}</th>
                                <th style="background-color:#ecf7fb;"><?php echo Lang::get('content.closing_odoreading'); ?></th>
                                <th colspan="3" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{ (vehicleMode == 'DG') ? '-' : hist.closingOdoReading}}</th>
                              </tr>
                              <tr ng-hide="true" style="text-align:center;" >
                                <td colspan="2"><b><?php echo Lang::get('content.fromdate'); ?></b></td>
                                <td>{{fromdate}}</td>
                                <td colspan="1"><b><?php echo Lang::get('content.from'); ?> <?php echo Lang::get('content.time'); ?></b></td>
                                <td colspan="3">{{fromtime}}</td>   
                              </tr>
                              <tr ng-hide="true" style="text-align:center;">
                                <td colspan="2"><b><?php echo Lang::get('content.todate'); ?></b></td>
                                <td>{{todate}}</td>
                                <td colspan="1"><b><?php echo Lang::get('content.to'); ?> <?php echo Lang::get('content.time'); ?></b></td>
                                <td colspan="3">{{totime}}</td>
                              </tr>
                            </thead>
                          </table>
                          <div class="col-md-12" style="height: 15px;"></div>
                          <!--    <tr><td colspan="8"></td></tr> -->
                          <table class="table table-bordered table-striped table-condensed table-hover table-fixed-header8"> 
                            <thead class='header' style=" z-index: 1;">
                              <tr style="text-align:center" >
                                <th width="12%" class="id" custom-sort order="'date'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.date_time'); ?></th>
                                <th width="8%" class="id" custom-sort order="'speed'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.speed'); ?></th>
                                <!-- <th width="6%" class="id" custom-sort order="'gsmLevel'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;">Sat</th> -->
                                <th width="10%" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.lat_long'); ?></th>
                                <th width="6%" class="id" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.gmap'); ?></th>
                                <th width="28%" class="id" custom-sort order="'address'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.Nearest_Loc'); ?></th>
                                <th ng-hide="vehicleMode == 'DG'" width="8%" class="id" custom-sort order="'tmpDistance'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.dist'); ?></th>
                                <th ng-hide="vehicleMode == 'DG'" width="12%" class="id" custom-sort order="'distanceCovered'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.cdist'); ?> <?php echo Lang::get('content.KMS'); ?></th>
                                <th ng-show="vehicleMode != 'DG' && vehiAssetView" width="10%" class="id" custom-sort order="'odoDistance'" sort="sort"  style="text-align:center;height:3px;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.odo'); ?> <?php echo Lang::get('content.KMS'); ?></th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr ng-repeat="move in movementdata | orderBy:natural(sort.sortingOrder):sort.reverse" class="active" style="text-align:center" ng-if="!errMsg.includes('expired')">
                                <td>{{move.date | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                                <td>{{move.speed}}</td>
                                <!-- <td>{{move.gsmLevel}}</td> -->
                                <td>{{move.latitude}},{{move.longitude}}</td>
                                <td><a href="https://www.google.com/maps?q=loc:{{move.latitude}},{{move.longitude}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
                                <td>
                                  <p ng-if="move.address!=null">{{move.address}}</p>
                                  <p ng-if="move.address==null && maddress1[$index]!=null">{{maddress1[$index]}}</p>
                                  <p ng-if="move.address==null && maddress1[$index]==null"><img src="assets/imgs/loader.gif" align="middle"></p>
                                </td>
                                <!-- <td style="cursor: pointer;" get-location lat={{user.latitude}} lon={{user.longitude}} index={{$index}} ng-if="user.address==null && maddress[$index]==null">Click Me</td> -->
                                <!-- <td get-location lat={{user.latitude}} lon={{user.longitude}} index={{$index}} ng-if="user.address==null && maddress1[$index]!=null">{{maddress1[$index]}}</td> -->
                                <td ng-hide="vehicleMode == 'DG'">{{move.tmpDistance}}</td>
                                <td ng-hide="vehicleMode == 'DG'">{{move.distanceCovered}}</td>
                                <td ng-show="vehicleMode != 'DG' && vehiAssetView">{{move.odoDistance}}</td>
                              </tr>   
                              <tr ng-if="errMsg==null " align="center" ng-show="showErrMsg">
                                <td ng-if="movementdata == null || movementdata.length==0" colspan="{{ vehicleMode == 'DG'? 7 : 10}}" class="err"><h5><?php echo Lang::get('content.zero_records'); ?></h5></td>
                              </tr>
                              <tr ng-if="errMsg != null " align="center" ng-show="showErrMsg">
                                <td colspan="{{ vehicleMode == 'DG'? 7 : 10}}" class="err"><h5>{{errMsg}}</h5></td>
                              </tr>
                            </table>
                            <!-- </div> -->
                            <!-- <pagination boundary-links="true" max-size="3" items-per-page="itemsPerPage" total-items="movementdata.length" ng-model="currentPage" ng-change="pageChanged()"></pagination>  
                            --></div>
                          </tab>
                          <tab ng-show="tabOverspeedShow" select="alertMe('Overspeed')" heading="<?php echo Lang::get('content.overspeed'); ?>" active="taboverspeed">
                            <p style="margin:0" class="page-header"><?php echo Lang::get('content.overspeed'); ?> <?php echo Lang::get('content.report'); ?> <span style="float: right;font-size:15px;padding-right: 15px;"><b><?php echo Lang::get('content.from'); ?></b> : &nbsp;{{(fromtime)}} &nbsp;{{fromdate|date:'dd-MM-yyyy'}} &nbsp;&nbsp; - &nbsp;&nbsp; <b><?php echo Lang::get('content.to'); ?></b> :&nbsp; {{(totime)}} &nbsp;{{todate|date:'dd-MM-yyyy'}}</span></p>

                            <div style="min-width:680px; height: 300px;margin-left:20px;" id="container3" ng-if="!errMsg.includes('expired')"></div>

                            <div class="box-body table-responsive" id='overspeedreport'>
                              <!-- <div id="formConfirmation"> -->
                               <table class="table table-bordered table-striped table-condensed table-hover">
                                <thead>
                                  <tr style="text-align:center;" >
                                    <th colspan="2" style="background-color:#ecf7fb;"><b>{{vehiLabel | translate}} <?php echo Lang::get('content.group'); ?></b></th>
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(vgroup)}}</th>
                                    <th colspan="2" style="background-color:#ecf7fb;"><b>{{vehiLabel | translate}} <?php echo Lang::get('content.name'); ?></b></th>
                                    <th style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{hist.shortName}}</th> 
                                  </tr>
                                  <tr style="text-align:center;">
                                    <th colspan="2" style="background-color:#f9f9f9;"><b><?php echo Lang::get('content.speed_limit'); ?></b></th>
                                    <th colspan="2" style="background-color:#ecf7fb;font-weight: unset;font-size:12px;">{{hist.overSpeedLimit}} <?php echo Lang::get('content.kmph'); ?></th>
                                    <th colspan="2" style="background-color:#f9f9f9;"><b><?php echo Lang::get('content.overspeed'); ?> <?php echo Lang::get('content.count'); ?></b></th>
                                    <th style="background-color:#ecf7fb;font-weight: unset;font-size:12px;">{{hist.overSpeedInstances}}</th>
                                  </tr>

                                  <tr ng-hide="true" style="text-align:center; font-weight: bold;" >
                                    <th colspan="3"><b><?php echo Lang::get('content.fromdate'); ?></b></th>
                                    <th>{{fromdate}}</th>
                                    <th colspan="2"><b><?php echo Lang::get('content.from'); ?> <?php echo Lang::get('content.time'); ?></b></th>
                                    <th>{{fromtime}}</th>   
                                  </tr>
                                  <tr ng-hide="true" style="text-align:center; font-weight: bold;">
                                    <th colspan="3"><b><?php echo Lang::get('content.todate'); ?></b></th>
                                    <th>{{todate}}</th>
                                    <th colspan="2"><b><?php echo Lang::get('content.to'); ?> <?php echo Lang::get('content.time'); ?></b></th>
                                    <th>{{totime}}</th>
                                  </tr>
                                </thead>
                              </table> 
                              <div class="col-md-12" style="height: 15px;"></div>   
                              <!-- <tr><td colspan="8"></td></tr> -->
                              <table class="table table-bordered table-striped table-condensed table-hover table-fixed-header2">
                               <thead class='header' style=" z-index: 1;">
                                <tr> 
                                  <th width="15%" class="id" custom-sort order="'startTime'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.start_date'); ?></th>
                                  <th width="15%" class="id" custom-sort order="'endTime'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.end_date'); ?></th>
                                  <th width="12%" class="id" custom-sort order="'overSpeed'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.speed'); ?> (Km)</th>
                                  <th width="15%" class="id" custom-sort order="'overSpeedDuration'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.duration'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</th>
                                  <th width="10%" class="id" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.start_loc'); ?></th>
                                  <th width="10%" class="id" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.end_loc'); ?></th>
                                  <!--  <th ng-if="vehiAssetView" width="12%" class="id" custom-sort order="'odoMeter'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.odo'); ?> (Km)</th> -->
                                  <th ng-hide="vehicleMode == 'DG'" width="14%" class="id" custom-sort order="'tripDistance'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.distance'); ?> (Km)</th>
                                </tr>
                              </thead>
                              <tr ng-repeat="user in overspeeddata | orderBy:natural(sort.sortingOrder):sort.reverse" class="active" style="text-align:center" ng-if="(dealerFilter)?(user.movingTime>filterDuration):'true' && !errMsg.includes('expired')">
                                <td>{{user.startTime | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                                <td>{{user.endTime | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                                <td>{{user.overSpeed}}</td>
                                <td>{{msToTime(user.overSpeedDuration)}}</td>
                                <td><a href="https://www.google.com/maps?q=loc:{{user.startLatitude}},{{user.startLongitude}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
                                <td><a href="https://www.google.com/maps?q=loc:{{user.latitude}},{{user.longitude}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
                                <!-- <td>{{user.gsmLevel}}</td> -->
                                       <!--  <td>
                                            <p ng-if="user.address!=null">{{user.address}}</p>
                                            <p get-location lat={{user.latitude}} lon={{user.longitude}} index={{$index}} ng-if="user.address==null && oaddress[$index]!=null">{{oaddress[$index]}}</p>
                                            <p ng-if="user.address==null && oaddress[$index]==null"><img src="assets/imgs/loader.gif" align="middle"></p>
                                          </td> -->
                                        <!-- <td style="cursor: pointer;" get-location lat={{user.latitude}} lon={{user.longitude}} index={{$index}} ng-if="user.address==null && oaddress[$index]==null">Click Me</td>
                                          <td get-location lat={{user.latitude}} lon={{user.longitude}} index={{$index}} ng-if="user.address==null && oaddress[$index]!=null">{{oaddress[$index]}}</td> -->
                                          <!-- <td ng-hide="true">{{user.isOverSpeed}}</td> -->
                                          <!--  <td ng-if="vehiAssetView">{{user.odoMeter}}</td> -->
                                          <td ng-hide="vehicleMode == 'DG'">{{user.tripDistance}}</td>
                                          <!-- <td><a href="https://www.google.com/maps?q=loc:{{user.latitude}},{{user.longitude}}" target="_blank">Link</a></td>  -->            </tr>
                                          <tr ng-if="errMsg==null" align="center" ng-show="showErrMsg">
                                            <td ng-if="overspeeddata==null || overspeeddata.length == 0" colspan="{{ vehicleMode == 'DG'? 7 : 8 }}" class="err"><h5><?php echo Lang::get('content.zero_records'); ?></h5></td>
                                          </tr>
                                          <tr ng-if="errMsg != null " align="center" ng-show="showErrMsg">
                                            <td colspan="{{ vehicleMode == 'DG'? 6 : 7}}" class="err"><h5>{{errMsg}}</h5></td>
                                          </tr>
                                        </table>
                                        <!-- </div> -->
                                      </div>
                                    </tab>

                                    <tab ng-show="tabParkedShow" select="alertMe('Stopped/Parked')" heading="<?php echo Lang::get('content.parked'); ?>" active="tabparked">
                                     <p style="margin:0" class="page-header"><?php echo Lang::get('content.parked'); ?> <?php echo Lang::get('content.report'); ?> <span style="float: right;font-size:15px;padding-right: 15px;"><b><?php echo Lang::get('content.from'); ?></b> : &nbsp;{{(fromtime)}} &nbsp;{{fromdate|date:'dd-MM-yyyy'}} &nbsp;&nbsp; - &nbsp;&nbsp; <b><?php echo Lang::get('content.to'); ?></b> :&nbsp; {{(totime)}} &nbsp;{{todate|date:'dd-MM-yyyy'}}</span>
                                     </p>
                                     <div class="form-group pull-right" style="float: right;padding-right: 20px;margin-top: 7px;margin-bottom: 7px;">
                                      <span style="font-size: 14px;">
                                        <select ng-model="time_park" ng-change="filtersTime(time_park,'park')" style="padding:3px 3px 3px 3px; font-size: 12px;width:100px;">
                                          <option value="All"> <?php echo Lang::get('content.all'); ?> </option>
                                          <option value="1">  >  1 <?php echo Lang::get('content.min'); ?> </option>
                                          <option value="2">  >  2 <?php echo Lang::get('content.Mins'); ?> </option>
                                          <option value="5">  >  5 <?php echo Lang::get('content.Mins'); ?> </option>
                                          <option value="10"> > 10 <?php echo Lang::get('content.Mins'); ?> </option>
                                          <option value="15"> > 15 <?php echo Lang::get('content.Mins'); ?> </option>
                                          <option value="30"> > 30 <?php echo Lang::get('content.Mins'); ?> </option>
                                          <option value="100"> > 1 <?php echo Lang::get('content.hour'); ?> </option>
                                        </select>
                                      </span>
                                    </div>

                                    <div class="box-body table-responsive" id='stoppedparkingreport' style="padding-top:10px;">
                                      <!-- <div id="formConfirmation"> -->
                                       <table class="table table-bordered  table-striped table-condensed table-hover">
                                        <thead>
                                          <tr style="text-align:center;">
                                            <th colspan="1" style="background-color:#ecf7fb;"><b>{{vehiLabel | translate}} <?php echo Lang::get('content.group'); ?></b></th>
                                            <th style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(vgroup)}}</th>
                                            <th colspan="1" style="background-color:#ecf7fb;"><b>{{vehiLabel | translate}} <?php echo Lang::get('content.name'); ?></b></th>
                                            <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{hist.shortName}}</th>
                                   <!-- <td ng-hide="true" colspan="2"><b>Register No</b></td>
                                    <td ng-hide="true">{{hist.regNo}}</td>   -->            
                                  </tr>
                                </thead>
                              </table>
                              <div class="col-md-12" style="height: 15px;"></div>
                              <!-- <tr><td colspan="5"></td></tr>-->
                              <table class="table table-bordered  table-striped table-condensed table-hover table-fixed-header3">
                                <thead class='header' style=" z-index: 1;">
                                  <tr style="text-align:center;font-weight: bold;">
                                    <th width="14%" custom-sort order="'date'" sort="sort" style="text-align:center;background-color:#C2D2F2;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.start'); ?></th>
                                    <th width="14%" custom-sort order="'date'" sort="sort" style="text-align:center;background-color:#C2D2F2;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.end'); ?></th>
                                    <!-- <th width="7%" custom-sort order="'gsmLevel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;">Sat</th> -->
                                    <th width="40%" custom-sort order="'address'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.Nearest_Loc'); ?></th>
                                    <th width="15%" custom-sort order="'parkedTime'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.duration'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</th>
                                    <th width="10%" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.gmap'); ?></th>
                                    <!-- <th width="20%" colspan="2" class="id" custom-sort order="'totalParkedTime'" sort="sort" style="text-align:center;">Total Parked Time</th> -->
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr ng-repeat="user in parkeddata | orderBy:natural(sort.sortingOrder):sort.reverse" class="active" style="text-align:center"
                                  ng-if="(dealerFilter)?(user.parkedTime>filterDuration):'true' && !errMsg.includes('expired')">

                                  <td>{{user.startParkedTime | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                                  <td>{{user.endParkedTime | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                                  <!-- <td>{{user.gsmLevel}}</td> -->
                                  <td> 
                                    <p ng-if="user.address!=null">{{user.address}}</p>
                                    <p get-location lat={{user.latitude}} lon={{user.longitude}} index={{$index}} ng-if="user.address==null && saddressStop[$index]!=null">{{saddressStop[$index]}}</p>
                                    <p ng-if="user.address==null && saddressStop[$index]==null"><img src="assets/imgs/loader.gif" align="middle"></p>
                                  </td>
                                  <td>{{getParkedCorrectHours(user.parkedTime)}}</td>
                                  <td><a href="https://www.google.com/maps?q=loc:{{user.latitude}},{{user.longitude}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
                                </tr>
                                <tr ng-if=" errMsg==null" align="center">
                                  <td ng-if="parkeddata==null || parkeddata.length==0" colspan="6" class="err" ng-show="showErrMsg"><h5><?php echo Lang::get('content.zero_records'); ?></h5></td>
                                </tr>
                                <tr ng-if="errMsg != null " align="center" ng-show="showErrMsg">
                                  <td colspan="8" class="err"><h5>{{errMsg}}</h5></td>
                                </tr>
                              </tbody>                                
                            </table>
                            <!-- </div> -->
                          </div>
                        </tab>

                        <tab ng-show="tabIdleShow" select="alertMe('idlereport')" heading="<?php echo Lang::get('content.idle'); ?>" active="tabidle">
                         <p style="margin:0" class="page-header"><?php echo Lang::get('content.idle'); ?> <?php echo Lang::get('content.report'); ?> <span style="float: right;font-size:15px;padding-right: 15px;"><b><?php echo Lang::get('content.from'); ?></b> : &nbsp;{{(fromtime)}} &nbsp;{{fromdate|date:'dd-MM-yyyy'}} &nbsp;&nbsp; - &nbsp;&nbsp; <b><?php echo Lang::get('content.to'); ?></b> :&nbsp; {{(totime)}} &nbsp;{{todate|date:'dd-MM-yyyy'}}</span></p>
                         <!-- <div id="formConfirmation"> -->
                           <div class="box-body table-responsive" id="idlereport">
                            <table class="table table-bordered table-striped table-condensed table-hover">
                              <thead>
                                <tr style="text-align:center;">
                                  <th style="background-color:#ecf7fb;">{{vehiLabel | translate}} <?php echo Lang::get('content.group'); ?></th>
                                  <th style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(vgroup)}}</th>
                                  <th style="background-color:#ecf7fb;">{{vehiLabel | translate}} <?php echo Lang::get('content.name'); ?></th>
                                  <th style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{hist.shortName}}</th> 
                                </tr>
                              </thead>
                            </table>
                            <div class="col-md-12" style="height: 15px;"></div> 
                            <!-- <tr><td colspan="4"></td></tr> -->
                            <table class="table table-bordered table-striped table-condensed table-hover table-fixed-header4">
                             <thead class='header' style=" z-index: 1;">
                              <tr style="text-align:center;font-weight: bold;">
                                <th width="13%" class="id" custom-sort order="'date'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.start_time'); ?></th>
                                <th width="13%" class="id" custom-sort order="'idleEndDate'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.end_time'); ?></th>
                                <th width="10%" class="id" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.gmap'); ?></th>
                                <!-- <th width="13%" class="id" custom-sort order="'gsmLevel'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;">Sat</th> -->
                                <th width="40%" class="id" custom-sort order="'address'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.Nearest_Loc'); ?></th>
                                <th width="15%" class="id" custom-sort order="'idleTime'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.duration'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr ng-repeat="user in idlereport | orderBy:natural(sort.sortingOrder):sort.reverse" class="active" style="text-align:center"
                              ng-if="(dealerFilter)?(user.idleTime>filterDuration):'true' && !errMsg.includes('expired')">
                              <td>{{user.date | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                              <td>{{user.idleEndDate | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                              <td><a href="https://www.google.com/maps?q=loc:{{user.latitude}},{{user.longitude}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
                              <!-- <td>{{user.gsmLevel}}</td> -->
                              <td>
                                <p ng-if="user.address!=null">{{user.address}}</p>
                                <p get-location lat={{user.latitude}} lon={{user.longitude}} index={{$index}} ng-if="user.address==null && addressIdle[$index]!=null">{{addressIdle[$index]}}</p>
                                <p ng-if="user.address==null && addressIdle[$index]==null"><img src="assets/imgs/loader.gif" align="middle"></p>
                              </td>
                              <td>{{getParkedCorrectHours(user.idleTime)}}</td>
                            </tr>
                            <tr ng-if="errMsg==null" align="center" ng-show="showErrMsg">
                              <td ng-if="idlereport==null || idlereport.length==0" colspan="6" class="err"><h5><?php echo Lang::get('content.zero_records'); ?></h5></td>
                            </tr>
                            <tr ng-if="errMsg != null" align="center" ng-show="showErrMsg">
                              <td colspan="9" class="err"><h5>{{errMsg}}</h5></td>
                            </tr>
                          </tbody>
                        </table>
                        <!-- </div> -->
                      </div>
                    </tab>
                    <tab ng-hide="true" select="alertMe('eventReport')" heading="<?php echo Lang::get('content.event'); ?>" active="tabevent">
                      <p style="margin:0" class="page-header"><?php echo Lang::get('content.event'); ?> <?php echo Lang::get('content.report'); ?> <span style="float: right;font-size:15px;padding-right: 15px;"><b><?php echo Lang::get('content.from'); ?></b> : &nbsp;{{fromdate}} &nbsp;{{convert_to_24hrs(fromtime)}} &nbsp;&nbsp; - &nbsp;&nbsp; <b><?php echo Lang::get('content.to'); ?></b> :&nbsp; {{todate}} &nbsp;{{convert_to_24hrs(totime)}}</span></p>
                      <div class="box-body table-responsive" id="eventReport">
                        <table style="background-color: #cecece; color: #000" class="table table-bordered table-striped table-condensed table-hover">
                          <thead>
                            <tr>
                              <td><label><input type="checkbox"  id="stop"><b> <?php echo Lang::get('content.stoppage'); ?> </b></label></td>
                              <td><label><input type="textbox" id="stop1" style="height: 20px; width:30px; border-radius: 3px" value="10"/><b> <?php echo Lang::get('content.mins'); ?> </b></label></td>
                              <td><label><input type="checkbox" id="idle"><b> <?php echo Lang::get('content.idle'); ?> </b></label></td>
                              <td><label><input type="textbox" id="idle1" style="height: 20px; width:30px; border-radius: 3px" value="10"/><b> <?php echo Lang::get('content.mins'); ?> </b></label></td>
                              <td><label><input type="checkbox" id="location" ><b> <?php echo Lang::get('content.loc'); ?> </b></label></td>
                            </tr>
                            <tr>
                              <td><label><input type="checkbox" id="overspeed"><b> <?php echo Lang::get('content.overspeed'); ?> </b></label></td>
                              <td><label><input type="textbox" id="overspeed1" style="height: 20px; width:30px; border-radius: 3px" value="10"/><b> <?php echo Lang::get('content.km'); ?> </b></label></td>
                              <td><label><input type="checkbox" id="notreach"><b> <?php echo Lang::get('content.notreachable'); ?> </b></label><br/></td>
                              <td><label><input type="textbox" id="notreach1" style="height: 20px; width:30px; border-radius: 3px" value="10"/><b> <?php echo Lang::get('content.mins'); ?> </b></label></td>
                              <td><label><input type="checkbox" id="site"><b> <?php echo Lang::get('content.site'); ?> </b></label>   </td>
                            </tr>


                          </table>
                          <hr>
                          <table class="table table-bordered table-striped table-condensed table-hover">
                           <thead>
                            <tr style="text-align:center;">
                              <th style="background-color:#ecf7fb;">{{vehiLabel}} <?php echo Lang::get('content.group'); ?></th>
                              <th style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(vgroup)}}</th>
                              <th colspan="2" style="background-color:#ecf7fb;">{{vehiLabel}} <?php echo Lang::get('content.name'); ?></th>
                              <th style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{hist.shortName}}</th> 
                            </tr>
                          </thead>
                        </table>
                        <div class="col-md-12" style="height: 15px;"></div>
                        <!--    <tr><td colspan="5"></td></tr>-->
                        <table class="table table-bordered table-striped table-condensed table-hover">
                          <thead>     
                            <tr style="text-align:center;font-weight: bold;">
                              <th width="20%" class="id" custom-sort order="'startTime'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.start_time'); ?></th>
                              <th width="20%" class="id" custom-sort order="'duration'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.duration'); ?></th>
                              <!-- <th width="5%" class="id" custom-sort order="'duration'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;">Sat</th> -->
                              <th colspan="2" width="40%" class="id" custom-sort order="'address'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.Nearest_Loc'); ?></th>
                              <th width="20%" class="id" custom-sort order="'state'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.state'); ?></th>
                            </tr>
                          </thead>

                          <tbody>
                            <tr ng-repeat="user in eventReportData | orderBy:natural(sort.sortingOrder):sort.reverse" class="active" style="text-align:center">
                              <td>{{user.startTime | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                              <td>
                                <p ng-if="user.duration!=null">{{msToTime(user.duration)}}</p>
                                <p ng-if="user.duration==null">--</p>
                              </td>
                              <!-- <td>{{user.gsmLevel}}</td> -->
                              <td colspan="2">
                                <p ng-if="user.address!=null">{{user.address}}</p>
                                <p get-location lat={{user.latitude}} lon={{user.longitude}} index={{$index}} ng-if="user.address==null && addressEvent[$index]!=null">{{addressEvent[$index]}}</p>
                                <p ng-if="user.address==null && addressEvent[$index]==null"><img src="assets/imgs/loader.gif" align="middle"></p>
                              </td>
                              <td>{{user.state}}</td>
                            </tr>
                            <tr ng-if="errMsg == null" align="center">
                              <td ng-if="eventReportData==null" colspan="5" class="err" ng-show="showErrMsg"><h5><?php echo Lang::get('content.zero_records'); ?></h5></td>
                            </tr>
                            <tr ng-if="errMsg != null" align="center" ng-show="showErrMsg">
                              <td colspan="8" class="err"><h5>{{errMsg}}</h5></td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </tab>
                    <tab ng-hide="true" select="alertMe('sitereport')" heading="Site" active="tabsite">
                     <p style="margin:0" class="page-header"><?php echo Lang::get('content.site'); ?> <?php echo Lang::get('content.report'); ?> <span style="float: right;font-size:15px;padding-right: 15px;"><b><?php echo Lang::get('content.from'); ?></b> : &nbsp;{{fromdate}} &nbsp;{{convert_to_24hrs(fromtime)}} &nbsp;&nbsp; - &nbsp;&nbsp; <b><?php echo Lang::get('content.to'); ?></b> :&nbsp; {{todate}} &nbsp;{{convert_to_24hrs(totime)}}</span></p>
                     <div class="box-body table-responsive" id="sitereport">
                      <table class="table table-bordered table-striped table-condensed table-hover">
                        <thead>
                          <tr style="text-align:center;">
                            <th style="background-color:#ecf7fb;">{{vehiLabel}} <?php echo Lang::get('content.group'); ?></th>
                            <th style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(vgroup)}}</th>
                            <th style="background-color:#ecf7fb;">{{vehiLabel}} <?php echo Lang::get('content.name'); ?></th>
                            <th style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{hist.shortName}}</th> 
                          </tr>
                        </thead>
                      </table>
                      <div class="col-md-12" style="height: 15px;"></div>     
                      <!--<tr><td colspan="4"></td></tr> -->
                      <table class="table table-bordered table-striped table-condensed table-hover">
                        <thead> 
                          <tr style="text-align:center;font-weight: bold;">
                            <th width="20%" class="id" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.start_time'); ?></th>
                            <!-- <th width="10%" class="id" style="text-align:center;">G-Map</th> -->
                            <th colspan="2" class="id" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.Nearest_Loc'); ?></th>
                            <th width="20%" class="id" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.state'); ?></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr ng-repeat="user in siteReport   " class="active" style="text-align:center">
                            <td>{{user.startTime | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                            <td colspan="2">{{user.address}}</td>
                            <td>{{user.state}}</td>
                          </tr>
                          <tr ng-if="errMsg==null" align="center" ng-show="showErrMsg">
                            <td ng-if="siteReport==null" colspan="4" class="err"><h5><?php echo Lang::get('content.zero_records'); ?></h5></td>
                          </tr>
                          <tr ng-if="errMsg != null " align="center" ng-show="showErrMsg">
                            <td colspan="8" class="err"><h5>{{errMsg}}</h5></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>

                  </tab>
                  <tab select="alertMe('loadreport')" heading="Load" active="tabload" ng-hide="true">
                    <p style="margin:0" class="page-header"><?php echo Lang::get('content.Load'); ?> <?php echo Lang::get('content.report'); ?> <span style="float: right;font-size:15px;padding-right: 15px;"><b><?php echo Lang::get('content.from'); ?></b> : &nbsp;{{fromdate}} &nbsp;{{convert_to_24hrs(fromtime)}} &nbsp;&nbsp; - &nbsp;&nbsp; <b><?php echo Lang::get('content.to'); ?></b> :&nbsp; {{todate}} &nbsp;{{convert_to_24hrs(totime)}}</span></p>
                    <div class="box-body table-responsive" id="loadreport">
                      <table class="table table-bordered table-striped table-condensed table-hover">
                        <thead>
                          <tr style="text-align:center;">
                            <th style="background-color:#ecf7fb;">{{vehiLabel}} <?php echo Lang::get('content.group'); ?></th>
                            <th style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(vgroup)}}</th>
                            <th style="background-color:#ecf7fb;">{{vehiLabel}} <?php echo Lang::get('content.name'); ?></th>
                            <th style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{hist.shortName}}</th> 
                          </tr>
                        </thead>
                      </table>
                      <div class="col-md-12" style="height: 15px;"></div> 
                      <!--    <tr><td colspan="4"></td></tr> -->
                      <table class="table table-bordered table-striped table-condensed table-hover">
                        <thead>   
                          <tr style="text-align:center;font-weight: bold;">
                            <th class="id"  custom-sort order="'date'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;" width="30%"><?php echo Lang::get('content.date_time'); ?></th>
                            <th class="id"  custom-sort order="'load'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;" width="20%"><?php echo Lang::get('content.loadtruck'); ?></th>
                            <!-- <th class="id"  custom-sort order="'gsmLevel'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;" width="5%">Sat</th> -->
                            <th class="id" custom-sort order="'address'" sort="sort" style="text-align: center;background-color:#d3e0f1;font-size:12px;" width="35%"><?php echo Lang::get('content.Nearest_Loc'); ?></th>
                            <th width="15%" class="id" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.gmap'); ?></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr class="active" style="text-align:center" ng-repeat="load in loadreport.load | orderBy:natural(sort.sortingOrder):sort.reverse">
                            <td>{{load.date | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                            <td>{{load.load}}</td>
                            <!-- <td>{{load.gsmLevel}}</td> -->
                            <td>{{load.address}}</td>
                            <td><a href="https://www.google.com/maps?q=loc:{{load.lat}},{{load.lng}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
                          </tr>
                          <tr ng-if="errMsg==null" align="center" ng-show="showErrMsg">
                            <td  ng-if="loadreport.load ==null || loadreport.load.length==0" colspan="4" class="err"><h5><?php echo Lang::get('content.zero_records'); ?>.</h5></td>
                          </tr>
                          <tr ng-if="errMsg != null " align="center" ng-show="showErrMsg">
                            <td colspan="8" class="err"><h5>{{errMsg}}</h5></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </tab>
                  <tab ng-show="tabFuelShow" heading="<?php echo Lang::get('content.fuel'); ?>" select="alertMe('fuelreport')" active="tabfuel">
                    <p style="margin:0" class="page-header"><?php echo Lang::get('content.fuel'); ?> <?php echo Lang::get('content.report'); ?> <span style="float: right;font-size:15px;padding-right: 15px;"><b><?php echo Lang::get('content.from'); ?></b> : &nbsp;{{fromdate}} &nbsp;{{convert_to_24hrs(fromtime)}} &nbsp;&nbsp; - &nbsp;&nbsp; <b><?php echo Lang::get('content.to'); ?></b> :&nbsp; {{todate}} &nbsp;{{convert_to_24hrs(totime)}}</span></p>


                    <tabset class="nav-tabs-custom">

                      <tab heading="<?php echo Lang::get('content.Fuel&Time'); ?>" active="true">
                       <div style="min-width:680px; height: 300px;margin-left:20px;" id="container"></div> 
                     </tab>

                     <tab heading="<?php echo Lang::get('content.Fuel&Distance'); ?>" active="false">
                      <div style="height: 300px;" id="container2"></div> 
                    </tab>

                    <tab heading="<?php echo Lang::get('content.Fuel&Speed'); ?>" active="false">
                      <div style="height: 300px;" id="container4"></div> 
                    </tab>

                  </tabset>


                         <!--   <div class="panel-group" id="accordion">
                                <div class="panel panel-default">
                                  <div class="panel-heading">
                                    <h4 class="panel-title">
                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Fuel</a>
                                    </h4>
                                  </div>
                                  <div id="collapse1" class="panel-collapse collapse in">
                                    <div class="panel-body">  <div style="min-width:680px; height: 300px;margin-left:20px;" id="container"></div>    </div>
                                  </div>
                                </div>
                                <div class="panel panel-default">
                                  <div class="panel-heading">
                                    <h4 class="panel-title">
                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Fuel and Distance</a>
                                    </h4>
                                  </div>
                                  <div id="collapse2" class="panel-collapse collapse">
                                    <div class="panel-body"> <div style="height: 300px;" id="container2"></div>  </div>
                                  </div>
                                </div>

                                <div class="panel panel-default"> 
                                  <div class="panel-heading">
                                    <h4 class="panel-title">
                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Collapsible Group 3</a>
                                    </h4>
                                  </div>
                                  <div id="collapse3" class="panel-collapse collapse">
                                    <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
                                  </div>
                                </div> 

                              </div> -->

                              <div class="box-body table-responsive" id="fuelreport">
                                <table class="table table-bordered table-striped table-condensed table-hover">
                                  <thead>
                                    <tr style="text-align:center;">
                                      <th style="background-color:#ecf7fb;">{{vehiLabel}} <?php echo Lang::get('content.group'); ?></th>
                                      <th style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(vgroup)}}</th>
                                      <th style="background-color:#ecf7fb;">{{vehiLabel}} <?php echo Lang::get('content.name'); ?></th>
                                      <th style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{hist.shortName}}</th> 
                                    </tr>
                                    <!--  <tr><td colspan="3"></td></tr> -->
                                  </thead>
                                </table>
                                
                                <div class="col-md-12" style="height: 15px;"></div>
                                <table class="table table-bordered table-striped table-condensed table-hover">
                                  <thead>
                                    <tr style="text-align:center;font-weight: bold;">
                                      <th class="id"  custom-sort order="'date'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;" width="10%"><?php echo Lang::get('content.date_time'); ?></th>
                                      <th class="id"  custom-sort order="'fuelLitre'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;" width="10%"><?php echo Lang::get('content.fuel'); ?> <?php echo Lang::get('content.ltr'); ?></th>
                                      <th class="id"  custom-sort order="'ignitionStatus'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;" width="10%"><?php echo Lang::get('content.ign'); ?></th>
                                      <th class="id"  custom-sort order="'distanceCovered'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;" width="10%"><?php echo Lang::get('content.dist_cov'); ?></th>
                                      <th class="id"  custom-sort order="'speed'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;" width="10%"><?php echo Lang::get('content.speed'); ?></th>
                                      <th class="id"  custom-sort order="'odoDistance'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;" width="15%"><?php echo Lang::get('content.odo'); ?></th>
                                      <!-- <th class="id"  custom-sort order="'gsmLevel'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;" width="5%">Sat</th> -->
                                      <th class="id" custom-sort order="'address'" sort="sort" style="text-align: center;background-color:#d3e0f1;font-size:12px;" width="20%"><?php echo Lang::get('content.Nearest_Loc'); ?></th>
                                      <th class="id" style="text-align: center;background-color:#d3e0f1;font-size:12px;" width="10%"><?php echo Lang::get('content.gmap'); ?></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr class="active" style="text-align:center" ng-repeat="fuel in fuelValue | orderBy:natural(sort.sortingOrder):sort.reverse">
                                      <td>{{fuel.date | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                                      <td>{{fuel.fuelLitre}}</td>
                                      <td>{{fuel.ignitionStatus}}</td>
                                      <td>{{fuel.distanceCovered}}</td>
                                      <td>{{fuel.speed}}</td>
                                      <td>{{fuel.odoDistance}}</td>
                                      <!-- <td>{{fuel.gsmLevel}}</td> -->
                                      <td> 
                                        <p ng-if="fuel.address!=null">{{fuel.address}}</p>
                                        <p ng-if="fuel.address==null && addressFuel[$index]!=null">{{addressFuel[$index]}}</p>
                                        <p ng-if="fuel.address==null && addressFuel[$index]==null"><img src="assets/imgs/loader.gif" align="middle"></p>
                                      </td> 
                                      <td><a href="https://www.google.com/maps?q=loc:{{fuel.latitude}},{{fuel.longitude}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
                                    </tr>
                                    <tr ng-if="errMsg==null" align="center" ng-show="showErrMsg">
                                      <td ng-if="fuelValue==null || fuelValue.length==0" colspan="9" class="err"><h5><?php echo Lang::get('content.zero_records'); ?></h5></td>
                                    </tr>
                                    <tr ng-if="errMsg != null " align="center" ng-show="showErrMsg">
                                      <td colspan="9" class="err"><h5>{{errMsg}}</h5></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </tab>
                            <tab ng-show="tabIgnitionShow" heading="<?php echo Lang::get('content.ign'); ?>" select="alertMe('ignitionreport')" active="tabignition">
                              <p style="margin:0" class="page-header"><?php echo Lang::get('content.ign'); ?> <?php echo Lang::get('content.report'); ?> <span style="float: right;font-size:15px;padding-right: 15px;"><b><?php echo Lang::get('content.from'); ?></b> : &nbsp;{{(fromtime)}} &nbsp;{{fromdate|date:'dd-MM-yyyy'}} &nbsp;&nbsp; - &nbsp;&nbsp; <b><?php echo Lang::get('content.to'); ?></b> :&nbsp; {{(totime)}} &nbsp;{{todate|date:'dd-MM-yyyy'}}</span></p>
                              <div class="box-body table-responsive" id="ignitionreport">
                                <table class="table table-bordered table-striped table-condensed table-hover">
                                  <thead>
                                    <tr style="text-align:center;">
                                      <th style="background-color:#ecf7fb;">{{vehiLabel | translate}} <?php echo Lang::get('content.group'); ?></th>
                                      <th style="text-align:center;background-color:#f9f9f9;font-weight: unset;font-size:12px;" colspan="1">{{trimColon(vgroup)}}</th>
                                      <th colspan="2" style="background-color:#ecf7fb;">{{vehiLabel | translate}} <?php echo Lang::get('content.name'); ?></th>
                                      <th colspan="2" style="text-align:center;background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{dataignition2.vehicleName}}</th>   
                                    </tr>
                                    <tr style="text-align:center;">
                                      <th style="background-color:#f9f9f9;"><?php echo Lang::get('content.reg_no'); ?></th>
                                      <th style="text-align:center;background-color:#ecf7fb;font-weight: unset;font-size:12px;" colspan="1">{{registerNo}}</th>
                                      <th colspan="2" style="background-color:#f9f9f9;"> <?php echo Lang::get('content.total_duration'); ?></th>
                                      <th colspan="2" style="text-align:center;background-color:#ecf7fb;font-weight: unset;font-size:12px;">{{msToTime2(dataignition2.totalDuration)}}</th>   
                                    </tr>
                                  </thead>
                                </table>

                                <div class="col-md-12" style="height: 15px;"></div>     
                                <table class="table table-bordered table-striped table-condensed table-hover table-fixed-header5">

                                  <thead class='header' style=" z-index: 1;">
                                    <!--<tr><td colspan="6"></td></tr> -->
                                    <tr style="text-align:center;font-weight: bold;" >
                                      <th style="text-align:center;background-color:#d3e0f1;font-size:12px;" width="20%"><?php echo Lang::get('content.date_time'); ?></th>
                                      <th style="text-align:center;background-color:#d3e0f1;font-size:12px;" width="20%"><?php echo Lang::get('content.status'); ?></th>
                                      <th style="text-align:center;background-color:#d3e0f1;font-size:12px;" width="15%"><?php echo Lang::get('content.duration'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</th>
                                      <!-- <th style="text-align:center;background-color:#d3e0f1;font-size:12px;" width="5%">Sat</th> -->
                                      <th style="text-align: center;background-color:#d3e0f1;font-size:12px;" width="30%"><?php echo Lang::get('content.Nearest_Loc'); ?></th>
                                      <th style="text-align: center;background-color:#d3e0f1;font-size:12px;" width="15%"><?php echo Lang::get('content.gmap'); ?></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr class="active" style="text-align:center" ng-repeat="ignition in dataignition" ng-if="!errMsg.includes('expired')">
                                      <td ng-if="ignition.ignitionStatus == 'ON' || ignition.ignitionStatus == 'OFF'">{{ignition.dateTime | date:'HH:mm:ss'}}  {{ignition.dateTime | date:'dd-MM-yyyy'}}</td>
                                      <td ng-if="ignition.ignitionStatus == 'ON' || ignition.ignitionStatus == 'OFF'">{{ignition.ignitionStatus}}</td>
                                      <!-- <td rowspan="2" ng-if="ac.ignitionStatus == 'ON'">{{msToTime(acData[$index+1].date-ac.date)}}</td> -->
                                      <td rowspan="2" style="padding-top: 36px;" ng-if="(ignition.ignitionStatus == 'ON')&&($index+1<dataignition.length)">{{msToTime(dataignition[$index+1].dateTime-ignition.dateTime)}}</td>
                                      <td rowspan="2" style="padding-top: 36px;" ng-if="(ignition.ignitionStatus == 'ON')&&($index+1==dataignition.length)">{{msToTime(todaymillisec-ignition.dateTime)}}</td>
                                      <td ng-if="ignition.ignitionStatus == 'ON' || ignition.ignitionStatus == 'OFF'"><p>{{ignition.address}}</p></td>
                                      <td ng-if="ignition.ignitionStatus == 'ON' || ignition.ignitionStatus == 'OFF'"><a href="https://www.google.com/maps?q=loc:{{ignition.latitude}},{{ignition.longitude}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
                                    </tr>
                                    
                                    <tr ng-if="dataignition == null || dataignition == undefined || dataignition.length==0" align="center">
                                      <td colspan="8" class="err" ng-if="errMsg==null"><h5>{{dataignition2.error}}</h5></td>
                                      <td colspan="8" class="err" ng-if="errMsg!=null"><h5>{{errMsg}}</h5></td>
                                    </tr>
                                  </tbody>
                                </table>
                              </div>
                            </tab>
<!--                     <tab ng-show="tabAcreportShow" heading="A/C" select="alertMe('acreport')" active="tabac">
                            <p style="margin:0" class="page-header">A/C Report <span style="float: right;font-size:15px;padding-right: 15px;"><b>From</b> : &nbsp;{{fromdate}} &nbsp;{{convert_to_24hrs(fromtime)}} &nbsp;&nbsp; - &nbsp;&nbsp; <b>To</b> :&nbsp; {{todate}} &nbsp;{{convert_to_24hrs(totime)}}</span></p>
                            <br>
                            
                            <div class="box-body table-responsive" id="acreport">
                                <table class="table table-bordered table-striped table-condensed table-hover">
                                    <thead>
                                        <tr style="text-align:center;">
                                            <th style="background-color:#ecf7fb;">Vehicle Group</th>
                                            <th style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(vgroup)}}</th>
                                            <th style="background-color:#ecf7fb;">Vehicle Name</th>
                                            <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{hist.shortName}}</th> 
                                        </tr>
                                    </thead>
                                </table>
                                <div class="col-md-12" style="height: 15px;"></div>
                                <table class="table table-bordered table-striped table-condensed table-hover">
                                <thead>
                                 <tr><td colspan="5 "></td></tr> -->
<!--                                        <tr style="text-align:center;font-weight: bold;">
                                            <th style="text-align:center;background-color:#C2D2F2;" width="20%">Date &amp; Time</th>
                                            <th style="text-align:center;background-color:#C2D2F2;" width="15%">Status</th>
                                            <th style="text-align:center;background-color:#C2D2F2;" width="15%">Duration (<?php echo Lang::get('content.h:m:s'); ?>)</th>
                                            <th style="text-align: center;background-color:#C2D2F2;" width="35%">Nearest Location</th>
                                            
                                            <th class="id" style="text-align: center;background-color:#C2D2F2;" width="15%">G-Map</th>
                                        </tr>
                                </thead>
                                <tbody>
                                    <tr class="active" style="text-align:center" ng-repeat="ac in acReport">
                                        <td>{{ac.date | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                                        <td>
                                            <span ng-if="ac.vehicleBusy == 'yes'">ON</span>
                                            <span ng-if="ac.vehicleBusy == 'no'">OFF</span>
                                        </td>
                                        <td rowspan="2" ng-if="ac.vehicleBusy == 'yes'">{{msToTime(acReport[$index+1].date-ac.date)}} </td>
                                        <td> {{ac.address}}
                                             <p ng-if="ignitionignitionignition.address!=null">{{ignition.address}}</p>
                                            <p ng-if="ignitionignition.address==null && addressFuel[$index]!=null">{{addressFuel[$index]}}</p>
                                            <p ng-if="ignition.address==null && addressFuel[$index]==null"><img src="assets/imgs/loader.gif" align="middle"></p> -->
<!--                                        </td> 
                                        <td><a href="https://www.google.com/maps?q=loc:{{ac.latitude}},{{ac.longitude}}" target="_blank">Link</a></td>
                                    </tr>
                                    <tr ng-if="errMsg == null" align="center" ng-show="showErrMsg">
                                        <td  ng-if="acReport==null || acReport.length == 0" colspan="6" class="err"><h5>Got zero records. Please change the dates and try.</h5></td>
                                    </tr>
                                    <tr ng-if="errMsg != null" align="center" ng-show="showErrMsg">
                                        <td colspan="8" class="err"><h5>{{errMsg}}</h5></td>
                                    </tr>
                                </tbody>
                                </table>
                            </div>
                          </tab>-->
                          <tab ng-show="tabStopShow" select="alertMe('stoppageReport')" heading="<?php echo Lang::get('content.stoppage'); ?>" active="tabStop" >
                           <p style="margin:0" class="page-header"><?php echo Lang::get('content.stoppage'); ?> <?php echo Lang::get('content.report'); ?> <span style="float: right;font-size:15px;padding-right: 15px;"><b><?php echo Lang::get('content.from'); ?></b> : &nbsp;{{(fromtime)}} &nbsp;{{fromdate|date:'dd-MM-yyyy'}} &nbsp;&nbsp; - &nbsp;&nbsp; <b><?php echo Lang::get('content.to'); ?></b> :&nbsp; {{(totime)}} &nbsp;{{todate|date:'dd-MM-yyyy'}}</span></p>

                           <div class="form-group pull-right" style="float: right;padding-right: 20px;margin-top: 7px;margin-bottom: 7px;">
                            <span style="font-size: 14px;">

                              <select ng-model="time_stop" ng-change="filtersTime(time_stop,'stop')" style="padding:3px 3px 3px 3px; font-size: 12px;width:100px;">
                                <option value="All"> <?php echo Lang::get('content.all'); ?> </option>
                                <option value="1">  >  1 <?php echo Lang::get('content.min'); ?> </option>
                                <option value="2">  >  2 <?php echo Lang::get('content.Mins'); ?> </option>
                                <option value="5">  >  5 <?php echo Lang::get('content.Mins'); ?> </option>
                                <option value="10"> > 10 <?php echo Lang::get('content.Mins'); ?> </option>
                                <option value="15"> > 15 <?php echo Lang::get('content.Mins'); ?> </option>
                                <option value="30"> > 30 <?php echo Lang::get('content.Mins'); ?> </option>
                                <option value="100"> > 1 <?php echo Lang::get('content.hour'); ?> </option>
                              </select>
                            </span>
                          </div>
                          <div class="box-body table-responsive" id="stoppageReport">
                            <table class="table table-bordered table-striped table-condensed table-hover">
                              <thead>
                                <tr style="text-align:center;" ng-if="!errMsg.includes('expired')">
                                  <th style="background-color:#ecf7fb;">{{vehiLabel | translate}} <?php echo Lang::get('content.group'); ?></th>
                                  <th colspan="3" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(vgroup)}}</th>
                                  <th style="background-color:#ecf7fb;" colspan="2">{{vehiLabel | translate}} <?php echo Lang::get('content.name'); ?></th>
                                  <th style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{hist.shortName}}</th> 
                                </tr>
                              </thead>
                            </table>
                            <div class="col-md-12" style="height: 15px;"></div>
                            <table class="table table-bordered table-striped table-condensed table-hover table-fixed-header6">
                              <thead class='header' style=" z-index: 1;">
                                <!--<tr><td colspan="7"></td></tr>-->
                                <tr style="text-align:center;font-weight: bold;">
                                  <th width="15%" class="id" custom-sort order="'date'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.start_time'); ?></th>
                                  <th width="15%" class="id" custom-sort order="'endStoppageTime'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.end_time'); ?></th>
                                  <th width="10%" class="id" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.gmap'); ?></th>
                                  <!-- <th width="7%" class="id" custom-sort order="'gsmLevel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;">Sat</th> -->
                                  <th width="26%" class="id" custom-sort order="'address'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.loc'); ?></th>
                                  <th width="22%" class="id" custom-sort order="'stoppageTime'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.duration'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</th>
                                  <th ng-hide="vehicleMode == 'DG'" width="10%" class="id" custom-sort order="'distanceCovered'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.cdist'); ?> <?php echo Lang::get('content.KMS'); ?></th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr ng-repeat="user in stopReport | orderBy:natural(sort.sortingOrder):sort.reverse" class="active" style="text-align:center"
                                ng-if="(dealerFilter)?(user.stoppageTime>filterDuration):'true' && !errMsg.includes('expired')">
                                <td>{{user.date | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                                <td>{{user.endStoppageTime | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                                <td><a href="https://www.google.com/maps?q=loc:{{user.latitude}},{{user.longitude}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
                                <!-- <td>{{user.gsmLevel}}</td> -->
                                <td>
                                  <p ng-if="user.address!=null">{{user.address}}</p>
                                  <!--    <p get-location lat={{user.latitude}} lon={{user.longitude}} index={{$index}} ng-if="user.address==null ">{{addressIdle[$index]}}</p>-->
                                  <p ng-if="user.address==null"><img src="assets/imgs/loader.gif" align="middle"></p>
                                </td>
                                <td>{{msToTime(user.stoppageTime)}}</td>
                                <td ng-hide="vehicleMode == 'DG'">{{user.distanceCovered}}</td>
                              </tr>
                              <tr ng-if=" errMsg == null " align="center" ng-show="showErrMsg">
                                <td ng-if="stopReport==null || stopReport.length==0" colspan="{{ vehicleMode == 'DG'? 6 : 7 }}"  class="err"><h5><?php echo Lang::get('content.zero_records'); ?></h5></td>
                              </tr>
                              <tr ng-if="errMsg != null" align="center" ng-show="showErrMsg">
                                <td colspan="{{ vehicleMode == 'DG'? 7 : 8}}"  class="err"><h5>{{errMsg}}</h5></td>
                              </tr>
                            </tbody>
                          </table>
                        </div>

                      </tab>

                      <tab ng-show="tabMovementShow" select="alertMe('noData')" heading="<?php echo Lang::get('content.n_data'); ?>" active="tabNoData">
                       <p style="margin:0" class="page-header"> <?php echo Lang::get('content.n_data'); ?> <span style="float: right;font-size:15px;padding-right: 15px;"><b><?php echo Lang::get('content.from'); ?></b> : &nbsp;{{(fromtime)}} &nbsp;{{fromdate|date:'dd-MM-yyyy'}} &nbsp;&nbsp; - &nbsp;&nbsp; <b><?php echo Lang::get('content.to'); ?></b> :&nbsp; {{(totime)}} &nbsp;{{todate|date:'dd-MM-yyyy'}}</span></p>
                       <div id='nodatareport' class="box-body table-responsive">
                         <table class="table table-bordered table-striped table-condensed table-hover">
                          <thead>
                            <tr style="text-align:center;">
                              <th colspan="2" style="background-color:#ecf7fb;">{{vehiLabel | translate}} <?php echo Lang::get('content.group'); ?></th>
                              <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(vgroup)}}</th>
                              <th style="background-color:#ecf7fb;">{{vehiLabel | translate}} <?php echo Lang::get('content.name'); ?></th>
                              <th colspan="3" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{hist.shortName}}</th>                 
                            </tr>
                                   <!-- <tr style="text-align:center;">
                                        <th colspan="2" style="background-color:#f9f9f9;">Regn No</th>
                                        <th colspan="2" style="background-color:#ecf7fb;font-weight: unset;font-size:12px;">{{hist.regNo}}</th>
                                        <th style="background-color:#f9f9f9;">Trip Distance</th>
                                        <th colspan="3" style="background-color:#ecf7fb;font-weight: unset;font-size:12px;">{{hist.tripDistance}} Kms</th>
                                    </tr>
                                    <tr ng-if="vehiAssetView" style="text-align:center;">
                                        <th colspan="2" style="background-color:#ecf7fb;">Opening OdoReading</th>
                                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{hist.openingOdoReading}}</th>
                                        <th style="background-color:#ecf7fb;">Closing OdoReading</th>
                                        <th colspan="3" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{hist.closingOdoReading}}</th>
                                      </tr> -->
                                      <tr ng-hide="true" style="text-align:center;" >
                                        <td colspan="2"><b><?php echo Lang::get('content.fromdate'); ?></b></td>
                                        <td>{{fromdate}}</td>
                                        <td colspan="1"><b><?php echo Lang::get('content.from'); ?> <?php echo Lang::get('content.time'); ?></b></td>
                                        <td colspan="3">{{fromtime}}</td>   
                                      </tr>
                                      <tr ng-hide="true" style="text-align:center;">
                                        <td colspan="2"><b><?php echo Lang::get('content.todate'); ?></b></td>
                                        <td>{{todate}}</td>
                                        <td colspan="1"><b><?php echo Lang::get('content.to'); ?> <?php echo Lang::get('content.time'); ?></b></td>
                                        <td colspan="3">{{totime}}</td>
                                      </tr>
                                    </thead>
                                  </table>
                                  <div class="col-md-12" style="height: 15px;"></div>
                                  <!-- <tr><td colspan="8"></td></tr> -->
                                  <table class="table table-bordered table-striped table-condensed table-hover table-fixed-header7"> 
                                    <thead class='header' style=" z-index: 1;">
                                      <tr style="text-align:center" ng-if="!errMsg.includes('expired')">
                                        <th width="25%" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.date_time'); ?></th>
                                        <!--  <th width="8%" class="id" custom-sort order="'speed'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;">Speed</th> -->
                                        <!-- <th width="6%" class="id" custom-sort order="'gsmLevel'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;">Sat</th> -->
                                        <!--  <th width="10%" style="text-align:center;background-color:#d3e0f1;font-size:12px;">Lat&Long</th> -->
                                        <th width="20%" style="text-align:center;background-color:#d3e0f1;font-size:12px;" width="15%"><?php echo Lang::get('content.duration'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</th>
                                        <th width="40%" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.Nearest_Loc'); ?></th>
                                        <th width="15%" class="id" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.gmap'); ?></th>
                                   <!-- <th width="8%" class="id" custom-sort order="'tmpDistance'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;">Dist</th>
                                        <th width="12%" class="id" custom-sort order="'distanceCovered'" sort="sort"  style="text-align:center;background-color:#d3e0f1;font-size:12px;">C-Dist (Kms)</th>
                                        <th ng-if="vehiAssetView" width="10%" class="id" custom-sort order="'odoDistance'" sort="sort"  style="text-align:center;height:3px;background-color:#d3e0f1;font-size:12px;">Odo (Kms)</th>-->
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr ng-repeat="val in noData" class="active" style="text-align:center"
                                      ng-if="(dealerFilter)?(val.noDataTime>filterDuration):'true'">
                                      <td>{{val.date | date:'HH:mm:ss'}}  {{val.date | date:'dd-MM-yyyy'}}</td>
                                      <!-- <td>{{val.speed}}</td> -->
                                      <!-- <td>{{move.gsmLevel}}</td> -->
                                      <!-- <td>{{val.latitude}},{{val.longitude}}</td> -->
                                      <td>{{msToTime(val.noDataTime)}}</td>
                                      <td>
                                        <p ng-if="val.address!=null">{{val.address}}</p>
                                        <p ng-if="val.address==null && maddress1[$index]!=null">{{maddress1[$index]}}</p>
                                        <p ng-if="val.address==null && maddress1[$index]==null"><img src="assets/imgs/loader.gif" align="middle"></p>
                                      </td>
                                      <td><a href="https://www.google.com/maps?q=loc:{{val.latitude}},{{val.longitude}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
                                      <!-- <td style="cursor: pointer;" get-location lat={{user.latitude}} lon={{user.longitude}} index={{$index}} ng-if="user.address==null && maddress[$index]==null">Click Me</td> -->
                                      <!-- <td get-location lat={{user.latitude}} lon={{user.longitude}} index={{$index}} ng-if="user.address==null && maddress1[$index]!=null">{{maddress1[$index]}}</td> -->
                                 <!--  <td>{{val.tmpDistance}}</td>
                                        <td>{{val.distanceCovered}}</td>
                                        <td ng-if="vehiAssetView">{{val.odoDistance}}</td> -->
                                      </tr>   
                                      <tr ng-if="errMsg == null " align="center" ng-show="showErrMsg">
                                        <td ng-if="noData == null || noData.length==0" colspan="8" class="err"><h5><?php echo Lang::get('content.zero_records'); ?></h5></td>
                                      </tr>
                                      <tr ng-if="errMsg != null " align="center" ng-show="showErrMsg">
                                        <td colspan="10" class="err"><h5>{{errMsg}}</h5></td>
                                      </tr>
                                    </table>
                            <!-- <pagination boundary-links="true" max-size="3" items-per-page="itemsPerPage" total-items="movementdata.length" ng-model="currentPage" ng-change="pageChanged()"></pagination>  
                            -->
                          </div>
                        </tab> 

                        <div class="pull-right" style="margin-top: 1%">
                          <!-- <a href="downloadhistory?vvid={{id}}&rid={{downloadid}}&fd={{fromdate}}&ft={{fromtime}}&td={{todate}}&tt={{totime}}&dvg={{vgroup}}&interval={{interval}}" target="_blank"><img src="../resources/views/reports/image/Adobe.png" /></a> -->

                          <img style="cursor: pointer;" ng-click="exportData(downloadid)"  src="../resources/views/reports/image/xls.png" />
                          <img width=30 height=30 style="cursor: pointer;" ng-click="exportDataCSV(downloadid)"  src="../resources/views/reports/image/csv.jpeg" />
                          <img style="cursor: pointer;" ng-click="generatePDF()"  src="../resources/views/reports/image/Adobe.png" />
                        </div>
                      </tabset>
                    </div>
                    <!-- End Tabset -->

                  </div><!-- /.box-body -->
                <!-- <div ng-show="loading" class="overlay"></div>
                  <div ng-show="loading" class="loading-img"></div> -->
                <!--<div ng-show="true" class="overlay"></div>
                  <div ng-show="true" class="loading-img"></div> -->
                  <div class="box-body col-md-3" ng-show="overallEnable">
                    <!-- <div class="small-box bg-aqua">
                        <div class="inner">
                            <h3>{{hist.overSpeedInstances}}</h3>
                            <p>
                                <b> Overspeed Instances </b>
                            </p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                      </div> -->

                      <div class="small-box bg-green">
                        <div class="inner">
                          <h4 ng-if ="hist.topSpeed != 0 && hist.topSpeedTimeUTC != 0" ng-show="cardData">
                            {{hist.topSpeed}} &nbsp; {{hist.topSpeedTimeUTC | date:'yyyy-MM-dd HH:mm:ss'}}
                          </h4>
                          <h4 ng-if ="hist.topSpeed == 0 && hist.topSpeedTimeUTC == 0">
                            0
                          </h4>
                          <p><b><a style="color: white;" href="https://www.google.com/maps?q=loc:{{hist.topSpeedGeoLocation}}" target="_blank"><?php echo Lang::get('content.top_speed'); ?></a></b>

                          </p>

                        </div>
                        <div class="icon">
                          <i class="ion ion-speedometer"></i>
                        </div>
                      </div>

                      <div class="small-box bg-red">
                        <div class="inner">
                          <h4 ng-if ="hist.tripDistance!=null" ng-show="cardData">
                            {{ (vehicleMode == 'DG') ? '-' : hist.tripDistance}} {{ (vehicleMode == 'DG') ? '-' : 'Kms'}} 
                          </h4>
                          <h4 ng-if ="hist.tripDistance==null" ng-show="cardData">
                            0
                          </h4>
                          <p><b style="color: white;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.distance'); ?></a></b>

                          </p>

                        </div>
                        <div class="icon">
                          <i class="ion ion-speedometer"></i>
                        </div>
                      </div>

                      <div class="small-box bg-yellow">
                        <div class="inner">
                          <h4 ng-bind="msToTime(hist.totalRunningTime)" ng-show="cardData">

                          </h4>
                          <p><b><?php echo Lang::get('content.tot_running_time'); ?></b>

                          </p>

                        </div>
                        <div class="icon">
                          <i class="ion ion-clock"></i>
                        </div>
                      </div>

                      <div class="small-box bg-red">
                        <div class="inner">
                          <h4 ng-bind="msToTime(hist.totalParkedTime)" ng-show="cardData">

                          </h4>
                          <p><b><?php echo Lang::get('content.tot_parked_time'); ?></b>

                          </p>

                        </div>
                        <div class="icon">
                          <i class="ion ion-clock"></i>
                        </div>
                      </div>

                      <div class="small-box bg-dark-gray">
                        <div class="inner">
                          <h4 ng-bind="msToTime(hist.totalIdleTime)" ng-show="cardData"></h4>
                          <p><b><?php echo Lang::get('content.tot_idle_time'); ?></b></p>
                        </div>
                        <div class="icon">
                          <i class="ion ion-clock"></i>
                        </div>
                      </div>   
                      <div class="small-box bg-blue">
                        <div class="inner">
                          <h4 ng-bind="hist.parkingCount" ng-show="cardData">

                          </h4>
                          <p><b><?php echo Lang::get('content.no_of_parking'); ?></b>

                          </p>

                        </div>
                        <div class="icon">
                          <i class="ion ion-clock"></i>
                        </div>
                      </div>
                      <div class="small-box bg-aqua">
                        <div class="inner">
                          <h4 ng-bind="msToTime(hist.totalNoDataTime)" ng-show="cardData"></h4>
                          <p><b><?php echo Lang::get('content.no_data_time'); ?></b></p>
                        </div>
                        <div class="icon">
                          <i class="ion ion-clock"></i>
                        </div>
                      </div>
                    </div>
                  </div>  
                </div>

                <script src="assets/js/static.js"></script>        
                <script src="assets/js/jquery-1.11.0.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script> 
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
                <script data-require="angular-ui-bootstrap@0.11.0" data-semver="0.11.0" src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>
                <script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
                <script src="assets/js/bootstrap.min.js"></script>
                <script src="assets/js/angular-translate.js"></script>
                <!--<script src="assets/js/highcharts.js"></script> -->
                <script src="https://code.highcharts.com/highcharts.js"></script>
                <script src="https://code.highcharts.com/modules/exporting.js"></script>
                <script src="https://code.highcharts.com/modules/export-data.js"></script>
                <script src="../resources/views/reports/customjs/moment.js"></script>
                <script src="../resources/views/reports/customjs/FileSaver.js"></script>
                <script src="../resources/views/reports/customjs/html5csv.js"></script>
                <script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
                <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
                <script src='assets/js/table-fixed-header.js'></script>
                <!--<script src="assets/js/naturalSortVersionDates.js"></script> -->
                <script src="assets/js/vamoApp.js"></script>
                <script src="../resources/views/reports/customjs/history.js?v=<?php echo Config::get('app.version');?>"></script>

                <script>
                  $("#menu-toggle").click(function(e) {
                    e.preventDefault();
                    $("#wrapper").toggleClass("toggled");
                  });

                </script>
                <style>
                  .alig{
                    style="text-align:center; font-weight: bold;"
                  }
                </style>

              </body>
              </html>
