<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title><?php echo Lang::get('content.gps'); ?></title>
    <link rel="shortcut icon" href="assets/imgs/tab.ico">
    <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/popup.bootstrap.min.css">
    <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/jVanilla.css" rel="stylesheet">
    <link href="assets/css/simple-sidebar.css" rel="stylesheet">
    <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
    <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
    <style>
        <style>
        body{
            font-family: 'Lato', sans-serif;
            /*font-weight: bold;*/  
/* font-family: 'Lato', sans-serif;
font-family: 'Roboto', sans-serif;
font-family: 'Open Sans', sans-serif;
font-family: 'Raleway', sans-serif;
font-family: 'Faustina', serif;
font-family: 'PT Sans', sans-serif;
font-family: 'Ubuntu', sans-serif;
font-family: 'Droid Sans', sans-serif;
font-family: 'Source Sans Pro', sans-serif;
*/
}
.empty{
    height: 1px; width: 1px; padding-right: 30px; float: left;
}

.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
    background-color: #ffffff;
}
</style>
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2X3F316479');
</script>
</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
    <div id="wrapper" ng-controller="mainCtrl" class="ng-cloak">
        
        <?php include('sidebarList.php');?> 
        
        <div id="testLoad"></div>
        
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default">
                   
                </div>   
            </div>
        </div>
        
        <!-- AdminLTE css box-->

        <div class="col-md-12">
         <div class="box box-primary">
            <!-- <div class="row"> -->
                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                    <h3 class="box-title"><span><?php echo Lang::get('content.trip_summary'); ?></span>
                        <span><?php include('OutOfOrderDataInfo.php');?></span> 
                    </h3>
                </div>
                <div class="row">
                 <!-- <div class="col-md-1" align="center"></div>-->
                 <div class="col-md-2" align="center">
                    <div class="form-group" >
                        <h5 style="color: grey;">{{shortNam}}</h5>
                    </div>
                                   <!-- <div class="form-group" ng-if="shortNam==undefined || shortNam==null">
                                        <h5 style="color: red;">{{ vehiLabel | translate }} not found </h5>
                                    </div> -->
                                </div>
                                <?php include('dateTime.php');?>
                                <div class="col-md-1" align="center"></div>
                                <div class="col-md-1" align="center">
                                    <button style="margin-left: -100%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
                                </div>
                                <div class="col-md-12" align="right" style="margin-bottom: 10px;">
                                 <?php include('DateButtonWithWeekMonth.php');?>
                             </div>
                         </div>

                         <!--  </div> -->
                     </div>
                     
                 </div>

                 <div class="col-md-12">
                    <div class="box box-primary">
                        <div style="overflow-x: auto;">
                            <div class="col-md-12">
                                <div class="pull-right" style="margin-top: 1%;">                               

                                    <img style="cursor: pointer;" ng-click="exportData('formConfirmationForPdf')"  src="../resources/views/reports/image/xls.png"  ng-hide="siteData.historyConsilated==null || siteData.historyConsilated.length== 0"/>
                                    <img style="cursor: pointer;" ng-click="exportDataCSV('formConfirmationForPdf')"  src="../resources/views/reports/image/csv.jpeg"  ng-hide="siteData.historyConsilated==null || siteData.historyConsilated.length== 0"/>
                                    <img style="cursor: pointer;" onclick="generatePDF('tripreportkms')"  src="../resources/views/reports/image/Adobe.png"  ng-hide="siteData.historyConsilated==null || siteData.historyConsilated.length== 0"/>
                                </div>
                                <div class="box-body" id="tripreportkms" >
                                    <div class="empty" align="center"></div> <p style="margin:1px;font-size:16px;"><?php echo Lang::get('content.trip_summary_report'); ?> <span style="float: right;font-size:14px;padding-right: 8px;"><b><?php echo Lang::get('content.from'); ?></b> : &nbsp;{{(uiDate.fromtime)}} &nbsp;{{uiDate.fromdate|date:'dd-MM-yyyy'}} &nbsp;&nbsp; - &nbsp;&nbsp; <b><?php echo Lang::get('content.to'); ?></b> :&nbsp; {{(uiDate.totime)}} &nbsp;{{uiDate.todate|date:'dd-MM-yyyy'}}</span></p> 
                                    </br>
                                    <div id="formConfirmation">
                                        <table class="table table-bordered table-striped table-condensed table-hover table-fixed-header">
                                            <thead style="font-weight: bold;">                                                
                                                <tr class="active">
                                                   <td style="font-size:12px;" colspan="2" > <?php echo Lang::get('content.start_time'); ?> </td>
                                                   <td style="font-size:12px;" colspan="2">{{siteData.fromTime |  date:'hh:mm a dd-MM-yyyy'}}</td>
                                                   <td style="font-size:12px;" colspan="2"><?php echo Lang::get('content.end_time'); ?></td>
                                                   <td style="font-size:12px;" colspan="2"> {{siteData.toTime |  date:'hh:mm a dd-MM-yyyy'}}</div> </td>
                                                   <!-- <td style="font-size:12px;" colspan="1" rowspan="3">Last Sighted Location</td> -->
                                                   <td style="font-size:12px;" colspan="4" rowspan="3">
                                                    <?php echo Lang::get('content.loc'); ?> 
                                                    <p style="font-weight: normal">{{siteData.lastLocation}} </p>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="success">
                                            <td style="font-size:12px;" colspan="2">{{ vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></td>
                                            <td style="font-size:12px;" colspan="2">
                                                <span ng-show="siteData.historyConsilated.length != 0 ">{{siteData.vehicleName}}  </span>
                                                <span ng-show="siteData.historyConsilated.length == 0">{{vehTripName}}</span>
                                                <span ng-show="siteData.historyConsilated == null">{{vehTripName}}</span>
                                            </td>
                                            <td style="font-size:12px;" colspan="2"><?php echo Lang::get('content.group'); ?> <?php echo Lang::get('content.name'); ?></td>
                                            <td style="font-size:12px;" colspan="2">{{uiGroup}}</td>
                                        </tr>
                                        <tr class="danger">
                                            <td style="font-size:12px;" colspan="2"> {{ vehiLabel | translate }}  <?php echo Lang::get('content.id'); ?></td>
                                            <td style="font-size:12px;" colspan="2">{{siteData.vehicleId}}</td>
                                            <td style="font-size:12px;" colspan="1"><?php echo Lang::get('content.KMPL'); ?></td>
                                            <td style="font-size:12px;" colspan="1">{{(vehicleMode == 'DG')? '-' : siteData.kmpl}}</td>
                                            <td style="font-size:12px;" colspan="1"><?php echo Lang::get('content.trip_distance'); ?></td>
                                            <td style="font-size:12px;" colspan="1">{{(vehicleMode == 'DG')? '-' : siteData.totalTripLength}} {{(vehicleMode == 'DG')? '' : 'Kms' }}</td>                                            
                                        </tr>
                                        <tr><td style="font-size:12px;" colspan="11"></td></tr>
                                        <tr class="active">
                                            <td style="font-size:12px;" colspan="2"><?php echo Lang::get('content.status'); ?></td>
                                            <td style="font-size:12px;" colspan="2"><?php echo Lang::get('content.duration'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</td>
                                            <td style="font-size:12px;" colspan="2"><?php echo Lang::get('content.fuel_details'); ?></td>
                                            <td style="font-size:12px;" colspan="2"><?php echo Lang::get('content.liters'); ?></td>
                                            <td style="font-size:12px;" colspan="2"><?php echo Lang::get('content.statistics'); ?></td>
                                            <td style="font-size:12px;"><?php echo Lang::get('content.occurance'); ?></td>
                                        </tr>
                                        <tr class="warning">    
                                            <td style="font-size:12px;" colspan="2"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.moving'); ?></td>
                                            <td style="font-size:12px;" colspan="2">{{msToTime(siteData.totalMovingTime)}}</td>
                                            <td style="font-size:12px;" colspan="2"><?php echo Lang::get('content.initiallevel'); ?></td>
                                            <td style="font-size:12px;" colspan="2">{{siteData.initialFuel}}</td>
                                            <td style="font-size:12px;" colspan="2"><?php echo Lang::get('content.fuel_fill_count'); ?></td>
                                            <td style="font-size:12px;">{{siteData.fuelFillCount}}</td>
                                        </tr>
                                        <tr class="info">
                                            <td style="font-size:12px;" colspan="2"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.idle'); ?></td>
                                            <td style="font-size:12px;" colspan="2">{{msToTime(siteData.totalIdleTime)}}</td>
                                            <td style="font-size:12px;" colspan="2"><?php echo Lang::get('content.finallevel'); ?></td>
                                            <td style="font-size:12px;" colspan="2">{{siteData.finalFuel}}</td>
                                            <td style="font-size:12px;" colspan="2"><?php echo Lang::get('content.moving'); ?> <?php echo Lang::get('content.count'); ?></td>
                                            <td style="font-size:12px;">{{siteData.moveCount}}</td>
                                        </tr>
                                        <tr class="success">
                                            <td style="font-size:12px;" colspan="2"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.parked'); ?></td>
                                            <td style="font-size:12px;" colspan="2">{{msToTime(siteData.totalParkingTime)}}</td>
                                            <td style="font-size:12px;" colspan="2"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.filling'); ?></td>
                                            <td style="font-size:12px;" colspan="2">{{siteData.totalFuelFilled}}</td>
                                            <td style="font-size:12px;" colspan="2"><?php echo Lang::get('content.parking'); ?> <?php echo Lang::get('content.count'); ?></td>
                                            <td style="font-size:12px;">{{siteData.parkCount}}</td>
                                        </tr>
                                        <tr class="danger">
                                            <td style="font-size:12px;" colspan="2"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.data1'); ?></td>
                                            <td style="font-size:12px;" colspan="2">{{msToTime(siteData.totalNoDataTime)}}</td>
                                            <td style="font-size:12px;" colspan="2"><?php echo Lang::get('content.consumption'); ?></td>
                                            <td style="font-size:12px;" colspan="2">{{siteData.totalFuelConsume >0? siteData.totalFuelConsume : '0' }}</td>
                                            <td style="font-size:12px;" colspan="2"><?php echo Lang::get('content.idle'); ?> <?php echo Lang::get('content.count'); ?></td>
                                            <td style="font-size:12px;">{{siteData.idleCount}}</td>
                                        </tr>                                      
                                        <tr>
                                         <td style="font-size:12px;" colspan="11"></td>
                                     </tr>
                                 </thead>
                                 
                                 <thead class='header' style=" z-index: 1;">
                                    
                                    <tr>
                                        <th class="id" custom-sort order="'startTime'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.start_time'); ?></th>
                                        <th class="id" custom-sort order="'endTime'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.end_time'); ?></th>
                                        <th class="id" custom-sort order="'duration'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.duration'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</th>
                                        <th class="id" custom-sort order="'tripDistance'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.distance'); ?> <?php echo Lang::get('content.KMS'); ?></th>
                                        <th  ng-show="hasTempratue=='temperature'" class="id" custom-sort order="'startTemperature'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.start'); ?> <?php echo Lang::get('content.temperature'); ?> </th>
                                        <th ng-show="hasTempratue=='temperature'" class="id" custom-sort order="'endTemperature'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.end'); ?> <?php echo Lang::get('content.temperature'); ?> </th>
                                        
                                        <th colspan="{{(hasTempratue=='temperature') ? '2' : '3'}}" class="id" custom-sort order="'intLoc'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.start_loc'); ?></th> 
                                        
                                        <th colspan="{{(hasTempratue=='temperature') ? '2' : '3'}}" class="id" custom-sort order="'finLoc'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.end_loc'); ?></th>
                                        <th class="id" custom-sort order="'position'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.position'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="tripsummary in siteData.historyConsilated | orderBy:sort.sortingOrder:sort.reverse" style="text-align : center;" ng-if="!errMsg.includes('expired')">
                                        <td style="font-size:12px;">{{tripsummary.startTime | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                                        <td style="font-size:12px;">{{tripsummary.endTime | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                                        <td style="font-size:12px;">{{msToTime(tripsummary.duration)}}</td>
                                        <td style="font-size:12px;">{{(vehicleMode == 'DG')? '-' : tripsummary.tripDistance}}</td>
                                        <td ng-show="hasTempratue=='temperature'">{{tripsummary.startTemperature}}</td>
                                        <td style="font-size:12px;" ng-show="hasTempratue=='temperature'">{{tripsummary.endTemperature}}</td>
                                        <td style="font-size:12px;" ng-switch on="tripsummary.intLoc" colspan="{{(hasTempratue=='temperature') ? '2' : '3'}}">
                                            <span style="text-decoration: underline;" ng-switch-when="undefined"><a href="https://www.google.com/maps?q=loc:{{tripsummary.intLat}},{{tripsummary.intLon}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></span>
                                            <span ng-switch-when="-" style="text-decoration: underline;"><a href="https://www.google.com/maps?q=loc:{{tripsummary.intLat}},{{tripsummary.intLon}}" target="_blank">{{tripsummary.intLat}},{{tripsummary.intLon}}</a></span>
                                            <span ng-switch-default>
                                                <a href="#" ng-click="getInput(tripsummary, siteData)" ng-if="tripsummary.position=='M'" data-toggle="modal" data-target="#mapmodals" >{{tripsummary.intLoc}}</a>
                                                <p ng-if="tripsummary.position!= 'M'">{{tripsummary.intLoc}}</p>
                                            </span>
                                        </td>

                                        <td style="font-size:12px;" ng-switch on="tripsummary.finLoc" colspan="{{(hasTempratue=='temperature') ? '2' : '3'}}">
                                            <span style="text-decoration: underline;" ng-switch-when="undefined"><a href="https://www.google.com/maps?q=loc:{{tripsummary.finLat}},{{tripsummary.finLon}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></span>
                                            <span ng-switch-when="-" style="text-decoration: underline;"><a href="https://www.google.com/maps?q=loc:{{tripsummary.finLat}},{{tripsummary.finLon}}" target="_blank">{{tripsummary.finLat}},{{tripsummary.finLon}}</a></span>
                                            <span ng-switch-default>
                                                <a href="#" ng-click="getInput(tripsummary, siteData)" ng-if="tripsummary.position=='M'" data-toggle="modal" data-target="#mapmodals" >{{tripsummary.finLoc}}</a>
                                                <p ng-if="tripsummary.position!= 'M'">{{tripsummary.finLoc}}</p>
                                            </span>
                                        </td> 
                                        <td style="font-size:12px;" ng-switch on="tripsummary.position" >
                                            <span ng-switch-when="S" style="color : #ff480b"><?php echo Lang::get('content.idle'); ?></span>
                                            <span ng-switch-when="M" style="color : #00d736"><?php echo Lang::get('content.moving'); ?></span>
                                            <span ng-switch-when="P" style="color : #080808"><?php echo Lang::get('content.parked'); ?></span>
                                            <span ng-switch-when="U" style="color : #fc00bd"><?php echo Lang::get('content.data1'); ?></span>
                                        </td>
                                            </tr>
                                        <tr  ng-if="siteData.historyConsilated==null || siteData.historyConsilated.length== 0" align="center">
                                            <td colspan="11" class="err" ng-if="!errMsg"><h5><?php echo Lang::get('content.no_trip'); ?></h5></td>
                                            <td colspan="11" class="err" ng-if="errMsg"><h5>{{errMsg}}</h5></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- added for pdf -->
                            <div id="formConfirmationForPdf">
                                <table class="table table-bordered table-striped table-condensed table-hover">
                                    <thead style="font-weight: bold;">
                                        
                                        <tr class="active">
                                           <td style="font-size:8px;" colspan="2" > <?php echo Lang::get('content.start_time'); ?> </td>
                                           <td style="font-size:8px;" colspan="2">{{siteData.fromTime |  date:'HH:mm:ss dd-MM-yyyy'}}</td>
                                           <td style="font-size:8px;" colspan="2"><?php echo Lang::get('content.end_time'); ?></td>
                                           <td style="font-size:8px;" colspan="2"> {{siteData.toTime |  date:'HH:mm:ss dd-MM-yyyy'}}</div>  </td>
                                           
                                           <!-- <td style="font-size:8px;" colspan="1" rowspan="3">Last Sighted Location</td> -->
                                           <td style="font-size:8px;" colspan="4" rowspan="3">
                                            <?php echo Lang::get('content.loc'); ?> 
                                            <p style="font-weight: normal">{{siteData.lastLocation}} </p>
                                        </div>
                                    </td>
                                </tr>
                                <tr class="success">
                                    <td style="font-size:8px;" colspan="2">{{ vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></td>
                                    <td style="font-size:8px;" colspan="2">
                                        <span ng-show="siteData.historyConsilated.length != 0 ">{{siteData.vehicleName}}  </span>
                                        <span ng-show="siteData.historyConsilated.length == 0">{{vehTripName}}</span>
                                        <span ng-show="siteData.historyConsilated == null">{{vehTripName}}</span>
                                    </td>
                                    <td style="font-size:8px;" colspan="2"><?php echo Lang::get('content.group'); ?> <?php echo Lang::get('content.name'); ?></td>
                                    <td style="font-size:8px;" colspan="2">{{uiGroup}}</td>
                                </tr>
                                <tr class="danger">
                                    <td style="font-size:8px;" colspan="2"> {{ vehiLabel | translate }}  <?php echo Lang::get('content.id'); ?></td>
                                    <td style="font-size:8px;" colspan="2">{{shortNam}}</td>
                                    <td style="font-size:8px;" colspan="1"><?php echo Lang::get('content.KMPL'); ?></td>
                                    <td style="font-size:8px;" colspan="1">{{(vehicleMode == 'DG')? '-' : siteData.kmpl}}</td>
                                    <td style="font-size:8px;" colspan="1"><?php echo Lang::get('content.trip_distance'); ?></td>
                                    <td style="font-size:8px;" colspan="1">{{(vehicleMode == 'DG')? '-' : siteData.totalTripLength}} {{(vehicleMode == 'DG')? '' : 'Kms' }}</td>                                            
                                </tr>
                                <tr><td style="font-size:8px;" colspan="12"></td></tr>
                                <tr class="active">
                                    <td style="font-size:8px;" colspan="2"><?php echo Lang::get('content.status'); ?></td>
                                    <td style="font-size:8px;" colspan="2"><?php echo Lang::get('content.duration'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</td>
                                    <td style="font-size:8px;" colspan="2"><?php echo Lang::get('content.fuel_details'); ?></td>
                                    <td style="font-size:8px;" colspan="2"><?php echo Lang::get('content.liters'); ?></td>
                                    <td style="font-size:8px;" colspan="2"><?php echo Lang::get('content.statistics'); ?></td>
                                    <td style="font-size:8px;" colspan="2"><?php echo Lang::get('content.occurance'); ?></td>
                                </tr>
                                <tr class="warning">    
                                    <td style="font-size:8px;" colspan="2"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.moving'); ?></td>
                                    <td style="font-size:8px;" colspan="2">{{msToTime(siteData.totalMovingTime)}}</td>
                                    <td style="font-size:8px;" colspan="2"><?php echo Lang::get('content.initiallevel'); ?></td>
                                    <td style="font-size:8px;" colspan="2">{{siteData.initialFuel}}</td>
                                    <td style="font-size:8px;" colspan="2"><?php echo Lang::get('content.fuel_fill_count'); ?></td>
                                    <td style="font-size:8px;" colspan="2">{{siteData.fuelFillCount}}</td>
                                </tr>
                                <tr class="info">
                                    <td style="font-size:8px;" colspan="2"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.idle'); ?></td>
                                    <td style="font-size:8px;" colspan="2">{{msToTime(siteData.totalIdleTime)}}</td>
                                    <td style="font-size:8px;" colspan="2"><?php echo Lang::get('content.finallevel'); ?></td>
                                    <td style="font-size:8px;" colspan="2">{{siteData.finalFuel}}</td>
                                    <td style="font-size:8px;" colspan="2"><?php echo Lang::get('content.moving'); ?> <?php echo Lang::get('content.count'); ?></td>
                                    <td style="font-size:8px;" colspan="2">{{siteData.moveCount}}</td>
                                </tr>
                                <tr class="success">
                                    <td style="font-size:8px;" colspan="2"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.parked'); ?></td>
                                    <td style="font-size:8px;" colspan="2">{{msToTime(siteData.totalParkingTime)}}</td>
                                    <td style="font-size:8px;" colspan="2"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.filling'); ?></td>
                                    <td style="font-size:8px;" colspan="2">{{siteData.totalFuelFilled}}</td>
                                    <td style="font-size:8px;" colspan="2"><?php echo Lang::get('content.parking'); ?> <?php echo Lang::get('content.count'); ?></td>
                                    <td style="font-size:8px;" colspan="2">{{siteData.parkCount}}</td>
                                </tr>
                                <tr class="danger">
                                    <td style="font-size:8px;" colspan="2"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.data1'); ?></td>
                                    <td style="font-size:8px;" colspan="2">{{msToTime(siteData.totalNoDataTime)}}</td>
                                    <td style="font-size:8px;" colspan="2"><?php echo Lang::get('content.consumption'); ?></td>
                                    <td style="font-size:8px;" colspan="2">{{siteData.totalFuelConsume >0? siteData.totalFuelConsume : '0'}}</td>
                                    <td style="font-size:8px;" colspan="2"><?php echo Lang::get('content.idle'); ?> <?php echo Lang::get('content.count'); ?></td>
                                    <td style="font-size:8px;" colspan="2">{{siteData.idleCount}}</td>
                                </tr>                                      
                                <tr>
                                 <td style="font-size:8px;" colspan="12"></td>
                             </tr>
                         </thead>
                         
                         <thead class='header' style=" z-index: 1;">
                            
                            <tr>
                                <th class="id" style="font-size:8px;background-color:#ecf7fb;"><?php echo Lang::get('content.start_time'); ?></th>
                                <th class="id" style="font-size:8px;background-color:#ecf7fb;"><?php echo Lang::get('content.end_time'); ?></th>
                                <th class="id" style="font-size:8px;background-color:#ecf7fb;"><?php echo Lang::get('content.duration'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</th>
                                <th class="id" style="font-size:8px;background-color:#ecf7fb;"><?php echo Lang::get('content.distance'); ?> <?php echo Lang::get('content.KMS'); ?></th>
                                <th  ng-show="hasTempratue=='temperature'" class="id" style="font-size:8px;background-color:#ecf7fb;"><?php echo Lang::get('content.start'); ?> <?php echo Lang::get('content.temperature'); ?> </th>
                                <th ng-show="hasTempratue=='temperature'" class="id" style="font-size:8px;background-color:#ecf7fb;"><?php echo Lang::get('content.end'); ?> <?php echo Lang::get('content.temperature'); ?> </th>
                                
                                <th colspan="{{(hasTempratue=='temperature') ? '2' : '3'}}" class="id" style="font-size:8px;background-color:#ecf7fb;"><?php echo Lang::get('content.start_loc'); ?></th> 
                                
                                <th colspan="{{(hasTempratue=='temperature') ? '2' : '3'}}" class="id" style="font-size:8px;background-color:#ecf7fb;"><?php echo Lang::get('content.end_loc'); ?></th>
                                <th class="id" style="font-size:8px;background-color:#ecf7fb;"><?php echo Lang::get('content.position'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="tripsummary in siteData.historyConsilated | orderBy:sort.sortingOrder:sort.reverse" style="text-align : center;">
                                <td style="font-size:8px;">{{tripsummary.startTime | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                                <td style="font-size:8px;">{{tripsummary.endTime | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                                <td style="font-size:8px;">{{msToTime(tripsummary.duration)}}</td>
                                <td style="font-size:8px;">{{(vehicleMode == 'DG')? '-' : tripsummary.tripDistance}}</td>
                                <td ng-show="hasTempratue=='temperature'">{{tripsummary.startTemperature}}</td>
                                <td style="font-size:8px;" ng-show="hasTempratue=='temperature'">{{tripsummary.endTemperature}}</td>
                                <td style="font-size:10px;" ng-switch on="tripsummary.intLoc" colspan="{{(hasTempratue=='temperature') ? '2' : '3'}}">
                                    <span style="text-decoration: underline;" ng-switch-when="undefined"><a href="https://www.google.com/maps?q=loc:{{tripsummary.intLat}},{{tripsummary.intLon}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></span>
                                    <span ng-switch-when="-" style="text-decoration: underline;"><a href="https://www.google.com/maps?q=loc:{{tripsummary.intLat}},{{tripsummary.intLon}}" target="_blank">{{tripsummary.intLat}},{{tripsummary.intLon}}</a></span>
                                    <span ng-switch-default>
                                        <a href="#" ng-click="getInput(tripsummary, siteData)" ng-if="tripsummary.position=='M'" data-toggle="modal" data-target="#mapmodals" >{{tripsummary.intLoc}}</a>
                                        <p ng-if="tripsummary.position!= 'M'">{{tripsummary.intLoc}}</p>
                                    </span>
                                </td>

                                <td style="font-size:10px;" ng-switch on="tripsummary.finLoc" colspan="{{(hasTempratue=='temperature') ? '2' : '3'}}">
                                    <span style="text-decoration: underline;" ng-switch-when="undefined"><a href="https://www.google.com/maps?q=loc:{{tripsummary.finLat}},{{tripsummary.finLon}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></span>
                                    <span ng-switch-when="-" style="text-decoration: underline;"><a href="https://www.google.com/maps?q=loc:{{tripsummary.finLat}},{{tripsummary.finLon}}" target="_blank">{{tripsummary.finLat}},{{tripsummary.finLon}}</a></span>
                                    <span ng-switch-default>
                                        <a href="#" ng-click="getInput(tripsummary, siteData)" ng-if="tripsummary.position=='M'" data-toggle="modal" data-target="#mapmodals" >{{tripsummary.finLoc}}</a>
                                        <p ng-if="tripsummary.position!= 'M'">{{tripsummary.finLoc}}</p>
                                    </span>
                                </td> 
                                <td style="font-size:8px;" ng-switch on="tripsummary.position" >
                                    <span ng-switch-when="S" style="color : #ff480b"><?php echo Lang::get('content.idle'); ?></span>
                                    <span ng-switch-when="M" style="color : #00d736"><?php echo Lang::get('content.moving'); ?></span>
                                    <span ng-switch-when="P" style="color : #080808"><?php echo Lang::get('content.parked'); ?></span>
                                    <span ng-switch-when="U" style="color : #fc00bd"><?php echo Lang::get('content.data1'); ?></span>
                                </td>
                                <tr  ng-if="siteData.historyConsilated==null || siteData.historyConsilated.length== 0" align="center">
                                    <td colspan="11" class="err" ng-if="!errMsg"><h5><?php echo Lang::get('content.no_trip'); ?></h5></td>
                                    <td colspan="11" class="err" ng-if="errMsg"><h5>{{errMsg}}</h5></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade" id="mapmodals">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="myCity"><?php echo Lang::get('content.trip_summary'); ?></h4>
          </div>
          <div class="modal-body">
            <div class="map_container">
                <div id="map_canvas" class="map_canvas" style="width: 100%; height: 500px;"></div>
            </div>
        </div>
        <div class="modal-footer">
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</div>
</div>
<script>
    var apikey_url = JSON.parse(localStorage.getItem('apiKey'));
    var url = "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places,geometry";

    if(apikey_url != null || apikey_url != undefined) {
       url = "https://maps.googleapis.com/maps/api/js?key="+apikey_url+"&libraries=places,geometry";
   //url = "https://maps.googleapis.com/maps/api/js?key=AIzaSyABtcdlhUVm5aKq7wAlMatI56DKanIKS6o&libraries=places,geometry"; 
}     

function loadJsFilesSequentially(scriptsCollection, startIndex, librariesLoadedCallback) {
   if (scriptsCollection[startIndex]) {
     var fileref = document.createElement('script');
     fileref.setAttribute("type","text/javascript");
     fileref.setAttribute("src", scriptsCollection[startIndex]);
     fileref.onload = function(){
       startIndex = startIndex + 1;
       loadJsFilesSequentially(scriptsCollection, startIndex, librariesLoadedCallback)
   };
   
   document.getElementsByTagName("head")[0].appendChild(fileref)
}
else {
 librariesLoadedCallback();
}
}

   // An array of scripts you want to load in order
   var scriptLibrary = [];
   
   scriptLibrary.push("assets/js/static.js");
   scriptLibrary.push("assets/js/jquery-1.11.0.js");
   scriptLibrary.push("assets/js/bootstrap.min.js");
   scriptLibrary.push("https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js");
   scriptLibrary.push(url);
   scriptLibrary.push("../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js");
   scriptLibrary.push("https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js");
   scriptLibrary.push("../resources/views/reports/customjs/html5csv.js");
   scriptLibrary.push("../resources/views/reports/customjs/moment.js");
   scriptLibrary.push("../resources/views/reports/customjs/FileSaver.js");
   scriptLibrary.push("../resources/views/reports/datepicker/bootstrap-datetimepicker.js");
 //scriptLibrary.push(url);
 scriptLibrary.push("https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.1.1/js/bootstrap.min.js");
 scriptLibrary.push("assets/js/angular-translate.js");
 //scriptLibrary.push("assets/js/infobubble.js");
 //scriptLibrary.push("assets/js/moment.js");
 //scriptLibrary.push("assets/js/bootstrap-datetimepicker.js");
 //scriptLibrary.push("assets/js/infobox.js");
 scriptLibrary.push("assets/js/naturalSortVersionDatesCaching.js");
 //scriptLibrary.push("assets/js/naturalSortVersionDates.js");
 scriptLibrary.push("assets/js/vamoApp.js");
 scriptLibrary.push("assets/js/services.js");
 scriptLibrary.push("assets/js/siteReport.js?v=<?php echo Config::get('app.version');?>");

// Pass the array of scripts you want loaded in order and a callback function to invoke when its done
loadJsFilesSequentially(scriptLibrary, 0, function(){
       // application is "ready to be executed"
       // startProgram();
   });

    //     $("#menu-toggle").click(function(e) {
    //     e.preventDefault();
    //     $("#wrapper").toggleClass("toggled");
    // });
    $('#formConfirmationForPdf').hide();
    var generatePDF = function() {
      $('#formConfirmationForPdf').show();
      kendo.drawing.drawDOM($("#formConfirmationForPdf"),{paperSize: "A2",multiPage: true,margin: {
        left   : "10mm",
        top    : "10mm",
        right  : "5mm",
        bottom : "10mm"
    } }).then(function(group) {
        kendo.drawing.pdf.saveAs(group, "TripSummaryReport.pdf");
        $('#formConfirmationForPdf').hide();
    });
}



</script>

</body>
</html>
