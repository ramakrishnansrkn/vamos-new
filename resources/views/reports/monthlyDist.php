<!DOCTYPE html>
<html lang="en" style="overflow: hidden !important;">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title><?php echo Lang::get('content.gps'); ?></title>

  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
  <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
  <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
  <link href="assets/css/web.css" rel="stylesheet" /> <!-- added for table freezing -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="assets/js/gridviewscroll.js"></script> <!-- added for table freezing -->
  <!-- pdfgen -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
  <script src="https://unpkg.com/jspdf-autotable@2.3.2/dist/jspdf.plugin.autotable.js"></script>

  <!--  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script> -->

  <style>
    body {
      font-family: 'Lato', sans-serif;
      /* font-weight: bold; */  
   /* font-family: 'Lato', sans-serif;
      font-family: 'Roboto', sans-serif;
      font-family: 'Open Sans', sans-serif;
      font-family: 'Raleway', sans-serif;
      font-family: 'Faustina', serif;
      font-family: 'PT Sans', sans-serif;
      font-family: 'Ubuntu', sans-serif;
      font-family: 'Droid Sans', sans-serif;
      font-family: 'Source Sans Pro', sans-serif;
      */
    }
    .empty{
      height: 1px; width: 1px; padding-right: 30px; float: left;
    }
    .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
      background-color: #ffffff;
    }

  </style>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-2X3F316479');
  </script>
</head>
<div id="preloader" >
  <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
  <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
  <div ng-controller="mainCtrl" class="ng-cloak">
    <div id="wrapper">
      <?php include('sidebarList.php');?> 
      
      <div id="testLoad"></div>
      
      <div id="page-content-wrapper">
        <div class="container-fluid">
          <div class="panel panel-default">
           
          </div>   
        </div>
      </div>
      
      <div class="col-md-12">
       <div class="box box-primary" style="padding-top: 5px;margin-top: -10px;">
        <!-- <div class="row"> -->
          <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
            <h3 class="box-title"><span><?php echo Lang::get('content.executive_summary'); ?> - <?php echo Lang::get('content.monthly_dist'); ?></span>
              <span><?php include('OutOfOrderDataInfo.php');?></span>
            </h3>
          </div>
          
          <div class="row" ng-show="showMonth" style="margin-top: 20px;">
            <div class="col-md-2" align="center"></div>
            
            <div class="col-md-2 form-group" style="right:0px;"><?php echo Lang::get('content.month_year'); ?>:</div>
            <div class="col-md-3" align="center">
             <div class="input-group datecomp" style="padding-bottom: 20px;">
              <input type="text" ng-model="fromMonthss" class="form-control placholdercolor" id="monthFrom" placeholder="<?php echo Lang::get('content.month'); ?>">
            </div>
          </div>
          <div class="col-md-1" align="center">
           <div class="form-group">
             <button ng-click="submitMon()" onclick="undo()" style="margin-left: -100%; margin-top: 2px;"><?php echo Lang::get('content.submit'); ?></button>
           </div>
         </div>
       </div>
       <div class="row" style="text-align: right;padding-right: 10px;">
        <span style="background-color: red;
        color: white;
        padding: 2px 15px;"></span>
        <span style="padding-left: 5px;margin-right: 10px;font-weight: bold;">No Data Vehicles</span>
      </div>

      <!--  </div> -->
      <div class="row">
        <div class="col-md-1" align="center"></div>

        <div class="col-md-2" align="center">
          <div class="form-group">

          </div>
        </div>
      </div>

    </div>
  </div>

  <div class="col-md-12">
    <div class="box box-primary" style="min-height:570px;">
      
      <div>
        <div class="pull-right" style="margin-top: 10px;margin-right: 5px;">
          
          <img style="cursor: pointer;" onclick="undo()" ng-click="exportData('Monthly_Report')"  src="../resources/views/reports/image/xls.png" />
          <img width=30 height=30 style="cursor: pointer;" onclick="undo()" ng-click="exportDataCSV(downloadid)"  src="../resources/views/reports/image/csv.jpeg" />
          <img style="cursor: pointer;"  ng-click="exportPDF(downloadid)"  src="../resources/views/reports/image/Adobe.png" />

        </div>

        
        
        <div class="box-body">
          <div class="empty" align="center"></div>
          <div class="row" style="padding-top: 20px;"></div>
          <div id="Monthly_Report">
            <table class="table table-bordered table-striped table-condensed table-hover" style="width:  -webkit-fill-available;">
              <thead>
                <tr style="text-align:center;">
                  <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.group'); ?></th>
                  <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{group}}</th>
                  <th style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.month'); ?> & <?php echo Lang::get('content.year'); ?> </th>
                  <th colspan="3" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"> {{monValFrontss+curYearFrontss}} </th>         
                </tr>
              </thead>
            </table>   
            
            <table cellspacing="0" id="month" style="width: 100%; border-collapse: collapse;" >

              <tr class="GridViewScrollHeader" style="padding-bottom:20px;">
                
                <th style="background-color:#ecf7fb;font-weight: bold;">{{ vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></th>
                <th ng-repeat="dat in monthDates" style="background-color:#ecf7fb;font-weight: bold;">{{dat}}</th>
                <th style="background-color:#ecf7fb;font-weight: bold;"><?php echo Lang::get('content.total'); ?><?php echo Lang::get('content.KMS'); ?></th>
              </tr>
              
              <tr ng-repeat="vehi in distMonData" class="GridViewScrollItem" style="padding-bottom:20px;">

               <td style="font-weight: 600;" ng-style="{ 'color':vehi.position=='U'?'red':'black' }">{{vehi.vehiName}}</td>

               <td ng-repeat="val in vehi.distsTodays" ng-style="{ 'color':vehi.position=='U'?'red':'black' }">{{val.distanceToday}}</td>
               <td ng-style="{ 'color':vehi.position=='U'?'red':'black' }">{{parseInts(vehi.totDist)}}</td>
             </tr>
             <tr ng-if="monthData.comulativeDistance!=null" class="GridViewScrollItem" style="padding-bottom:20px;">
              <td style="background-color:#ecf7fb;font-weight: bold;"><?php echo Lang::get('content.total'); ?><?php echo Lang::get('content.KMS'); ?></td>
              <td ng-repeat="val in totDistVehic track by $index">{{roundOffDecimal(val)}}</td>
              <td style="background-color:#f9f9f9;"></td>
            </tr>
            
          </table>
          
        </div>                     
      </div>
    </div>
  </div>
</div>
</div>
</div>


<script src="assets/js/static.js"></script>
<script src="assets/js/jquery-1.11.0.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
<script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="assets/js/angular-translate.js"></script>
<script src="../resources/views/reports/customjs/html5csv.js"></script>
<script src="../resources/views/reports/customjs/moment.js"></script>
<script src="../resources/views/reports/customjs/FileSaver.js"></script>
<script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
<script src="assets/js/vamoApp.js"></script>
<script src="assets/js/services.js"></script>
<script src="../resources/views/reports/customjs/statistics.js?v=<?php echo Config::get('app.version');?>"></script>

<script>

 
  
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });
  
  $(function () {
    
    $('#monthFrom, #monthFroms').datetimepicker({
      minViewMode: 'months',
      viewMode: 'months',
      pickTime: false,
      useCurrent:true,
      format:'MM/YYYY',
      maxDate: new Date,
      minDate: new Date(2015, 12, 1)
    });
  });  
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });

//  var generatePDF = function() {
//   kendo.drawing.drawDOM($("#formConfirmation")).then(function(group) {
//     kendo.drawing.pdf.saveAs(group, "DayWise_Truck_Positions.pdf");
//   });
// }

</script>
<script type="text/javascript">
  var gridViewScroll = null;
  window.onload = function () {

    gridViewScroll = new GridViewScroll({
      elementID: "month",
      width: window.screen.width-280,
      height: window.screen.height-420,
      freezeColumn: true,
      freezeFooter: true,
      freezeColumnCssClass: "GridViewScrollItemFreeze",
      freezeFooterCssClass: "GridViewScrollFooterFreeze",
      freezeHeaderRowCount: 1,
      freezeColumnCount: 1,
      onscroll: function (scrollTop, scrollLeft) {
                    // console.log(scrollTop + " - " + scrollLeft);
                  }
                });
    gridViewScroll.enhance();
            //setTimeout(function(){ enhance(); }, 1000);
          }
          function enhance() {
            gridViewScroll.enhance();
          }
          function undo() {
            gridViewScroll.undo();
          }
          
        </script>
        
      </body>
      </html>
