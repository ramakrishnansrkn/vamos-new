<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title><?php echo Lang::get('content.gps'); ?></title>
  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
  <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
  <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <link href="../resources/views/assets/css/datebutton.css" rel="stylesheet" type="text/css" />
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
  <style>
    .empty {
     height: 1px; width: 1px; padding-right: 30px; float: left;
   }
   .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
     background-color: #ffffff;
   }

   body {
    font-family: 'Lato', sans-serif;
  /*font-weight: bold;
  font-family: 'Lato', sans-serif;
  font-family: 'Roboto', sans-serif;
  font-family: 'Open Sans', sans-serif;
  font-family: 'Raleway', sans-serif;
  font-family: 'Faustina', serif;
  font-family: 'PT Sans', sans-serif;
  font-family: 'Ubuntu', sans-serif;
  font-family: 'Droid Sans', sans-serif;
  font-family: 'Source Sans Pro', sans-serif;
  */
} 

.col-md-12 {
 width: 98% !important;
 left: 15px !important;
 padding-left: 20px !important;
}

/*
.chart {
    min-width: 320px;
    max-width: 800px;
    height: 280px;
    margin: 0 auto;
}

#container {
    width: 80%;
    height: unset !important; 
    max-width: 80%;
    max-height: unset !important; 
    min-height: 500px !important;
    margin-top: 10px !important;
}
*/

</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2X3F316479');
</script>
</head>

<div id="preloader" >
  <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
  <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
  <div ng-controller="mainCtrl" class="ng-cloak">
    <div id="wrapper">
      <?php include('sidebarList.php');?> 
      <div id="testLoad"></div>

      <div id="page-content-wrapper">
        <div class="container-fluid">
          <div class="panel panel-default"></div>   
        </div>
      </div>

      <div class="col-md-12">
       <div class="box box-primary" style="padding-top: 5px;margin-top: -20px;">
        <!-- <div class="row"> -->
          <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
            <h3 class="box-title"><span><?php echo Lang::get('content.dispenser_report'); ?></span>
              <span><?php include('OutOfOrderDataInfo.php');?></span> 
            </h3>
          </div>
          <div class="row">
            <div class="col-md-3" align="center">
            </div>
            <div class="col-md-2" align="center">
              <div class="form-group">
                <div class="input-group datecomp">
                  <input type="text" ng-model="uiDate.fromdate" class="form-control placholdercolor" id="dtFrom" placeholder="<?php echo Lang::get('content.fromdate'); ?>">
                </div>
              </div>
            </div>

            <div  class="col-md-2" align="center" >
              <div class="form-group">
                <div class="input-group datecomp">
                  <input type="text" ng-model="uiDate.todate" class="form-control placholdercolor" id="dtTo" placeholder="<?php echo Lang::get('content.todate'); ?>">
                </div>
              </div>
            </div>
            <div class="col-md-1" align="center">
              <button style="margin-left: -42%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
            </div>
          </div>
          <div class="col-md-12"  align="right" style="margin-bottom: 10px;margin-left: 25%;">
           <button type="button" class="btnDate" ng-click="durationFilter('yesterday')" ng-disabled="yesterdayDisabled" onclick="setToTime('yesterday')"><?php echo Lang::get('content.yesterday'); ?></button>
           <button type="button" class="btnDate" ng-if="day!=1" ng-click="durationFilter('thisweek')" ng-disabled="thisweekDisabled" onclick="setToTime('thisweek')"><?php echo Lang::get('content.thisweek'); ?></button>
           <button type="button" class="btnDate" ng-click="durationFilter('lastweek')" ng-disabled="weekDisabled" onclick="setToTime('lastweek')"><?php echo Lang::get('content.lastweek'); ?></button>                            
         </div>

         <!--  </div> -->
         <div class="row">
          <div class="col-md-1" align="center"></div>
          <div class="col-md-2" align="center">
            <div class="form-group"></div>
          </div>
        </div>

      </div>
    </div>

    <div class="col-md-12" style="border-color: unset!important;">
      <div class="box box-primary" style="min-height:570px;background-color:#fdfdfd;">
        <div>
         <!--  <div class="pull-left" ng-if="sensorCount>1"  ng-repeat="sensorNo in range(1,sensorCount)"  style="margin-top: 10px;margin-left: 5px;margin-bottom:10px;">
           <button type="button" class="btn btn-info" ng-click="sensorChange(sensorNo)" id="sensor{{sensorNo}}" ><?php echo Lang::get('content.sensor'); ?> {{sensorNo}}</button>
         </div>  -->
         <div class="pull-right" style="margin-top: 10px;margin-right: 5px;margin-bottom:10px;">
          <!-- <button type="button" class="btn btn-info" ng-click="durationFilter('month')" ng-disabled="monthDisabled"><?php echo Lang::get('content.month'); ?></button> -->

          <img style="cursor: pointer;" ng-click="exportData('DispenserReport')"  src="../resources/views/reports/image/xls.png" />
          <img style="cursor: pointer;" ng-click="exportDataCSV('DispenserReport')"  src="../resources/views/reports/image/csv.jpeg" />
          <img style="cursor: pointer;" onclick="generatePDF()"  src="../resources/views/reports/image/Adobe.png" />
        </div>
        <div class="box-body" >

          <div style="margin-top: 20px;" id="DispenserReport">
            <div id="formConfirmation">
             <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top: 10px;"> 
              <thead>
                <tr style="text-align:center">
                  <th class="id" custom-sort order="'date'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"> <?php echo Lang::get('content.date'); ?></th>
                  <th class="id" custom-sort order="'vehicleName'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.vehicle_name'); ?> 
                  <th class="id" custom-sort order="'startFuelLevel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.start_fuel'); ?></th>
                  <th class="id" custom-sort order="'endFuelLevel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.end_fuel'); ?></th>  
                  <th class="id" custom-sort order="'fuelFills'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.fuel_fill'); ?></th>  
                  <th class="id" custom-sort order="'fuelDispenser'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.fuel_dispensed'); ?></th>

                  <!--  <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.gmap'); ?></th>   -->                  
                </tr>
              </thead>
              <tbody ng-repeat="fuel in fuelMachineData | orderBy:natural(sort.sortingOrder):sort.reverse" >
                <tr ng-if="fuelMachineData[0].error==null">
                  <td>{{fuel.date | date:'dd-MM-yyyy'}}</td>
                  <td>{{fuel.vehicleName}}</td>     
                  <td>{{fuel.startFuelLevel}}</td>  
                  <td>{{fuel.endFuelLevel}}</td> 
                  <td>{{fuel.fuelFills}}</td> 
                  <td>{{fuel.fuelDispenser}}</td> 

                  <!--  <td><a href="https://www.google.com/maps?q=loc:{{fuel.lat}},{{fuel.lng}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>  -->                 
                </tr> 

                
              </tbody>
              <tr ng-if="fuelMachineData[0].error==null">
               <td style="font-size: 13px;font-weight: bold;background-color: powderblue;"><?php echo Lang::get('content.total'); ?></td>
               <td style="font-size: 13px;font-weight: bold;background-color: powderblue;">-</td>
               <td style="font-size: 13px;font-weight: bold;background-color: powderblue;">-</td>
               <td style="font-size: 13px;font-weight: bold;background-color: powderblue;">-</td>
               <td style="font-size: 13px;font-weight: bold;background-color: powderblue;">-</td>
               <td style="font-size: 13px;font-weight: bold;background-color: powderblue;">{{fuelMachineData[0].totalFuelDispensed}}</td>
             </tr>
             <tr ng-if="fuelMachineData[0].error!=null">
              <td colspan="8" class="err">
               <h5>{{fuelMachineData[0].error}}</h5>
             </td>

           </tr>

       </table> 

     </div>

   </div>

 </div>

</div>

</div>
</div>

</div>
</div>

<script src="assets/js/static.js"></script>
<!--<script src="assets/js/jquery-1.11.0.js"></script>-->
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<!--<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>-->
<!--<script src="assets/js/bootstrap.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script data-require="angular-ui-bootstrap@0.11.0" data-semver="0.11.0" src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>
<script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
<script src="assets/js/angular-translate.js"></script>
<script src="../resources/views/reports/customjs/html5csv.js"></script>   
<script src="../resources/views/reports/customjs/moment.js"></script>
<script src="../resources/views/reports/customjs/FileSaver.js"></script>
<script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<!--<script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>-->
  <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
  <!--<script src="assets/js/naturalSortVersionDates.js"></script>-->
  <script src="assets/js/vamoApp.js"></script>
  <script src="assets/js/services.js"></script>
  <script src="assets/js/fuelMachine.js?v=<?php echo Config::get('app.version');?>"></script>

  <script>
    
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });

    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });


    $('.addr').hide();
    var generatePDF = function() {
      $('.addr').show();
      $('.loc').hide();
      kendo.drawing.drawDOM($("#formConfirmation")).then(function(group) {
        kendo.drawing.pdf.saveAs(group, "DispenserReport.pdf");
        $('.loc').show();
        $('.addr').hide();
      });
    }

  </script>

</body>
</html>
