<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title><?php echo Lang::get('content.gps'); ?></title>
    <link rel="shortcut icon" href="assets/imgs/tab.ico">
    <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
    <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/jVanilla.css" rel="stylesheet">
    <link href="assets/css/simple-sidebar.css" rel="stylesheet">
    <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
    <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
    <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
    <!-- pdfgen -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
    <script src="https://unpkg.com/jspdf-autotable@2.3.2/dist/jspdf.plugin.autotable.js"></script>
    <style>
        .empty{
            height: 1px; width: 1px; padding-right: 30px; float: left;
        }
        .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
            background-color: #ffffff;
        }
        body{
            font-family: 'Lato', sans-serif;
            /*font-weight: bold;*/  

/* font-family: 'Lato', sans-serif;
font-family: 'Roboto', sans-serif;
font-family: 'Open Sans', sans-serif;
font-family: 'Raleway', sans-serif;
font-family: 'Faustina', serif;
font-family: 'PT Sans', sans-serif;
font-family: 'Ubuntu', sans-serif;
font-family: 'Droid Sans', sans-serif;
font-family: 'Source Sans Pro', sans-serif;
*/
} 
</style>
</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
    <div id="wrapper" ng-controller="mainCtrl" class="ng-cloak">
        <?php include('sidebarList.php');?>
        
        <div id="testLoad"></div>
        
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default">
                   
                </div>   
            </div>
        </div>
        
        <!-- AdminLTE css box-->

        <div class="col-md-12">
         <div class="box box-primary">
            
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                <h3 class="box-title"><?php echo Lang::get('content.geofence_fuel'); ?>
            </h3>
        </div>
        <div class="row">
            <!-- <div class="col-md-1" align="center"></div> -->
            <div class="col-md-2" align="center">
                <div class="form-group" ng-if="shortNam!=undefined || shortNam!=null">
                  <h5 style="color: grey;">{{shortNam}}</h5>
              </div>
              
          </div>
          <div>
            <?php include('dateTime.php');?>
        </div>
                   <!--  <div class="col-md-1" align="center">
                        <div class="form-group">
                            
                              
                           
                        </div>
                    </div> -->
                    <!-- <div class="col-md-1" align="center"></div> -->
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <!-- <div class="input-group"> -->
                              <!--  <select class="input-sm form-control" ng-model="interval">
                                     <option value="">Interval</option>
                                     <option label="1 mins">1</option>
                                     <option label="2 mins">2</option>
                                     <option label="5 mins">5</option>
                                     <option label="10 mins">10</option>
                                     <option label="15 mins">15</option>
                                     <option label="30 mins">30</option>
                                 </select> -->
                                 <!-- </div> -->
                             </div>
                             
                         </div>
                         
                     </div>

                     <div class="row">
                        <div class="col-md-2" align="center"></div>
                        <div class="col-md-2" align="center">
                            <div class="form-group">
                                <!-- <div class="input-group"> -->
                                    <select class="input-sm form-control" ng-model="_site1" ng-options="site.siteName for site in siteName | orderBy: 'siteName':false">
                                    </select>
                                    <!-- </div> -->
                                </div>
                                
                            </div>
                    <!-- <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.fromtime" class="form-control placholdercolor" id="timeFrom" placeholder="From time">
                                <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.todate" class="form-control placholdercolor" id="dateTo" placeholder="From date">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                            </div>
                        </div>
                    </div> -->
                   <!--  <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.totime" class="form-control placholdercolor" id="timeTo" placeholder="From time">
                                <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            
                            <select class="input-sm form-control" ng-model="_site2" ng-options="sites.siteName for sites in siteName | orderBy: 'siteName':false">
                            </select>
                        </div>
                    </div>
                    <!-- <div class="col-md-1" align="center"></div> -->
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <button ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
                        </div>
                        
                    </div>
                    <div class="col-md-12" align="right" style="margin-bottom: 10px;">
                     <?php include('weekdays.php');?>
                 </div>
             </div>
         </div>
         
         
     </div>

     <div class="col-md-12">
        

        <div class="col-md-12">
            <div class="box box-primary">
                <div>
                    <div class="pull-right" style="margin-top: 1%;">
                      
                        <img style="cursor: pointer;" ng-click="exportData('geofenceFuel')"  src="../resources/views/reports/image/xls.png" />
                        <img style="cursor: pointer;" ng-click="exportDataCSV('geofenceFuel')"  src="../resources/views/reports/image/csv.jpeg" />
                        <img style="cursor: pointer;" onclick="generatePDF()"  src="../resources/views/reports/image/Adobe.png" />


                        
                        
                    </div>
                    <div class="col-md-12" >
                        <div class="box-body" id="geofenceFuel">
                          <span style="float: right;font-size: 16px;font-family: 'Lato', sans-serif;"><?php echo Lang::get('content.from'); ?> : {{uiDate.fromdate}} {{uiDate.fromtime}} - <?php echo Lang::get('content.to'); ?> : {{uiDate.todate}} {{uiDate.totime}}</span>
                          <div id="formConfirmation">
                                    <!-- <p>
                                        <span style="margin-right: 40px;"><b>Driver Name</b> : {{siteTripFuelData.data.driverName? siteTripFuelData.data.driverName : "-" }}</span>                                     
                                        <span><b>Expected Mileage</b> : {{siteTripFuelData.data.expectedMileage? siteTripFuelData.data.expectedMileage + ' '+ "Kmpl" : "-" }}</span>
</p> -->
                            <!-- <table class="table table-bordered table-striped table-condensed table-hover" id='stf0'>
                               <thead>
                                <tr style="text-align:center; font-weight: bold;">
                                    <th colspan="2" style="background-color:#ecf7fb; width:20%;">Driver Name</th>
                                    <th colspan="2" style="background-color:#f9f9f9;  width:20%;">{{siteTripFuelData.data.driverName? siteTripFuelData.data.driverName : "-" }}</th>
                                    <th colspan="2" style="background-color:#ecf7fb;  width:20%;">Expected Mileage</th>
                                    <th colspan="2" style="background-color:#f9f9f9;  width:20%;">{{siteTripFuelData.data.expectedMileage? siteTripFuelData.data.expectedMileage + ' '+ "Kmpl" : "-" }}</th> 
                                            </tr>
                                        </thead>
                                    </table>  -->
                            <table class="table table-bordered table-striped table-condensed table-hover" id='stf0'>
                               <thead>
                                <tr style="text-align:center; font-weight: bold;">
                                    <th colspan="2" style="background-color:#ecf7fb;  width:20%;">{{ vehiLabel | translate }} <?php echo Lang::get('content.group'); ?></th>
                                    <th colspan="2" style="background-color:#f9f9f9;  width:20%;">{{uiGroup}}</th>
                                    <th colspan="2" style="background-color:#ecf7fb;  width:20%;">{{ vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></th>
                                    <th colspan="2" style="background-color:#f9f9f9;  width:20%;">{{shortNam}}</th> 
                                            <!-- <th>Trip Count</th>
                                                <th colspan="2">{{siteData.tripCount}}</th> -->
                                            </tr>
                                            <tr style="text-align:center; font-weight: bold;">
                                    <th colspan="2" style="background-color:#ecf7fb; width:20%;">Driver Name</th>
                                    <th colspan="2" style="background-color:#f9f9f9;  width:20%;">{{siteTripFuelData.data.driverName? siteTripFuelData.data.driverName : "-" }}</th>
                                    <th colspan="2" style="background-color:#ecf7fb;  width:20%;">Expected Mileage</th>
                                    <th colspan="2" style="background-color:#f9f9f9;  width:20%;">{{siteTripFuelData.data.expectedMileage? siteTripFuelData.data.expectedMileage + ' '+ "Kmpl" : "-" }}</th> 
                                            </tr>
                                        </thead>
                                    </table>
                                    <div class="col-md-12" style="height: 8px;"></div>
                                    <table class="table table-bordered table-striped table-condensed table-hover table-fixed-header" id='GeofenceFuel'>
                                      <thead class='header' style=" z-index: 1;">
                                        <tr style="text-align:center;font-weight: bold;">
                                            <th  width="5%" style="text-align:center;background-color:#C2D2F2;" custom-sort order="'startState'" sort="sort"><?php echo Lang::get('content.start_loc'); ?></th>
                                            <th  width="5%"  width="10%"  style="text-align:center;background-color:#C2D2F2;" custom-sort order="'startTime'" sort="sort"><?php echo Lang::get('content.start_time'); ?></th>
                                            <th  width="5%"  style="text-align:center;background-color:#C2D2F2;" custom-sort order="'endState'" sort="sort"><?php echo Lang::get('content.end_loc'); ?></th>
                                            <th  width="5%" style="text-align:center;background-color:#C2D2F2;" custom-sort order="'endTime'" sort="sort"><?php echo Lang::get('content.end_time'); ?></th>
                                            <!-- <th ng-hide="vehicleMode == 'DG'" width="10%" style="text-align:center;background-color:#C2D2F2;"><?php echo Lang::get('content.dist_cov'); ?></th> -->
                                            <th  width="5%"  style="text-align:center;background-color:#C2D2F2;" custom-sort order="'startFuel'" sort="sort"><?php echo Lang::get('content.start_fuel'); ?><?php echo Lang::get('content.ltrs'); ?> </th>
                                            <th  width="5%"  style="text-align:center;background-color:#C2D2F2;" custom-sort order="'endFuel'" sort="sort"><?php echo Lang::get('content.end_fuel'); ?><?php echo Lang::get('content.ltrs'); ?> </th>
                                            <th  width="5%"  style="text-align:center;background-color:#C2D2F2;" custom-sort order="'startTemperature'" sort="sort"><?php echo Lang::get('content.Start'); ?> <?php echo Lang::get('content.temperature'); ?> </th>
                                            <th  width="5%"  style="text-align:center;background-color:#C2D2F2;" custom-sort order="'endTemperature'" sort="sort"><?php echo Lang::get('content.End'); ?> <?php echo Lang::get('content.temperature'); ?></th>

                                            <th  width="5%"  style="text-align:center;background-color:#C2D2F2;" custom-sort order="'fuelFills'" sort="sort"><?php echo Lang::get('content.fuel_filled'); ?> </th>
                                            <th  width="5%"  style="text-align:center;background-color:#C2D2F2;" custom-sort order="'fuelDrops'" sort="sort"><?php echo Lang::get('content.fuel_dropped'); ?> </th>
                                            <th  width="5%"  style="text-align:center;background-color:#C2D2F2;" custom-sort order="'fuelConsume'" sort="sort"><?php echo Lang::get('content.f_consume'); ?></th>
                                            <th  width="5%" ng-hide="vehicleMode == 'DG'" width="10%" style="text-align:center;background-color:#C2D2F2;" custom-sort order="'distanceCovered'" sort="sort"><?php echo Lang::get('content.distance'); ?> <?php echo Lang::get('content.KMS'); ?></th>
                                            <th  width="5%"  ng-hide="vehicleMode == 'Machinery' || vehicleMode =='DG'" width="10%" style="text-align:center;background-color:#C2D2F2;" custom-sort order="'kmpl'" sort="sort"><?php echo Lang::get('content.kmpl'); ?> </th>
                                            <th width="5%" style="text-align:center;background-color:#C2D2F2;" custom-sort order="'runningTime'" sort="sort"><?php echo Lang::get('content.work_hours'); ?> </th>
                                        </tr>
                                    </thead>
                                    <tbody >
                                        <tr class="active" style="text-align:center" ng-repeat="site in siteTripFuelData.data.history"
                                        ng-if="!errMsg.includes('expired')" >
                                        <td ng-if-start="site.endState != 'TST'">{{site.startState}}</td> 
                                        <td>{{site.startTime | date:'HH:mm:ss dd-MM-yyyy'}}</td> 
                                        <td>{{site.endState}}</td> 
                                        <td>{{site.endTime | date:'HH:mm:ss dd-MM-yyyy'}}</td> 
                                        <td>{{site.startFuel}}</td> 
                                        <td>{{site.endFuel}}</td>
                                        <td>{{hasTempratue === 'temperature'? site.startTemperature +' '+ '°C' : '-'}}</td>
                                        <td>{{hasTempratue === 'temperature'? site.endTemperature + ' ' + '°C' : '-'}}</td>
                                        <td>{{site.fuelFills}}</td> 
                                        <td>{{site.fuelDrops}}</td>
                                        <td>{{site.fuelConsume}}</td>
                                        <td ng-hide="vehicleMode == 'DG'">{{site.distanceCovered}}</td> 
                                        <td ng-hide="vehicleMode == 'Machinery' || vehicleMode == 'DG'">{{site.kmpl}}</td>     
                                        <td ng-if-end>{{ (site.runningTime == 0)?site.runningTime:msToTime(site.runningTime)}}</td> 
                                        <td colspan="3" ng-if-start="site.endState == 'TST'" class="bg table-success"><?php echo Lang::get('content.summary'); ?></td>
                                        <td colspan="3" class="bg table-success"><?php echo Lang::get('content.total_count'); ?></td>
                                        <td colspan="2" class="bg table-success">{{site.tripCount}}</td>
                                        <td colspan="2" class="bg table-success"><?php echo Lang::get('content.total_distance'); ?></td>
                                        <td colspan="2" ng-if-end class="bg table-success">{{site.singleTripdistance}} (Kms)</td>  
                                    </tr>
                                    <tr align="center" ng-if="(siteTripFuelData.error != null&&!siteTripError) && !errMsg">
                                        <td colspan="{{ vehicleMode == 'DG'? 11 : 12}}" class="err" ><h5>{{siteTripFuelData.error}}</h5></td>
                                    </tr>
                                    <tr ng-if="siteTripError && !errMsg" style="text-align: center">
                                        <td colspan="11" class="err"><h5>{{siteTripError}}</h5></td>
                                    </tr>
                                    <tr ng-if="errMsg" style="text-align: center">
                                        <td colspan="11" class="err"><h5>{{siteTripError}}</h5></td>
                                    </tr>
                                </tbody>
                                
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script src="assets/js/static.js"></script>
<script src="assets/js/jquery-1.11.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
<script src="assets/js/ui-bootstrap-0.6.0.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places" type="text/javascript"></script>
<script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
<script src="assets/js/angular-translate.js"></script>
<script src="../resources/views/reports/customjs/html5csv.js"></script>
<script src="../resources/views/reports/customjs/FileSaver.js"></script>
<script src="../resources/views/reports/customjs/moment.js"></script>
<script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
<script src="../resources/views/reports/datatable/jquery.dataTables.js"></script>
<script src="assets/js/naturalSortVersionDatesCaching.js"></script>
<!--<script src="assets/js/naturalSortVersionDates.js"></script> -->
<script src="assets/js/vamoApp.js"></script>
<script src="assets/js/services.js"></script>
<script src="assets/js/siteReport.js?v=<?php echo Config::get('app.version');?>"></script>
<script>

        // $("#example1").dataTable();
        // $("#menu-toggle").click(function(e) {
        //     e.preventDefault();
        //     $("#wrapper").toggleClass("toggled");
        // });
        
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });

        var generatePDF = function() {
          let tableName='Geofence_Fuel';
          var doc = new jsPDF({
           orientation: 'l',
           unit: 'mm',
           format: 'a4',
       });
          
          doc.setFontSize(12);
          doc.text(`${tableName.toUpperCase()}`,130,10,'center');
          
          var oneData1=doc.autoTableHtmlToJson(document.getElementById("stf0"));
        //   var oneData=doc.autoTableHtmlToJson(document.getElementById("stf1"));
    // var twoData=doc.autoTableHtmlToJson(document.getElementById("stf2"));
    var tableData = doc.autoTableHtmlToJson(document.getElementById("GeofenceFuel"));
    doc.autoTable(oneData1.columns, oneData1.rows, {
    margin: [15, 5, 10, 5],
    tableLineColor: [189, 195, 199],
    styles: {
        fontSize: 8,
        overflow: 'linebreak',
        lineWidth: 0.01
    }
});
//     doc.autoTable(oneData.columns, oneData.rows, {
//         margin: [27,5,10,5],
//         tableLineColor: [189, 195, 199],
//         styles: {
//           fontSize: 8,
//           overflow: 'linebreak',
//           lineWidth: 0.01
//       }
//   });
    // doc.autoTable(twoData.columns, twoData.rows, {
    //         margin: [21,5,10,5],
    //         tableLineColor: [189, 195, 199],
    //         styles: {
    //           fontSize: 8,
    //            overflow: 'linebreak',
    //       lineWidth: 0.01
    //         }
    //     });
    tableData.rows.shift();
    doc.autoTable(tableData.columns, tableData.rows, {
        margin: [39,5,10,5],
        tableLineColor: [189, 195, 199],
        styles: {
          fontSize: 8,
          overflow: 'linebreak',
          lineWidth: 0.01
      }
  });
    doc.save(tableName+'.pdf');
}

</script>

</body>
</html>
