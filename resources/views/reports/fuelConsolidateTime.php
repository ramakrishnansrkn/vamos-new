<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title><?php echo Lang::get('content.gps'); ?></title> 
  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
  <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
  <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
  <style>
    body {
      font-family: 'Lato', sans-serif;
      /* font-weight: bold; */  
   /* font-family: 'Lato', sans-serif;
      font-family: 'Roboto', sans-serif;
      font-family: 'Open Sans', sans-serif;
      font-family: 'Raleway', sans-serif;
      font-family: 'Faustina', serif;
      font-family: 'PT Sans', sans-serif;
      font-family: 'Ubuntu', sans-serif;
      font-family: 'Droid Sans', sans-serif;
      font-family: 'Source Sans Pro', sans-serif;
      */
    }
    .empty{
     height: 1px; width: 1px; padding-right: 30px; float: left;
   }
   .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
     background-color: #ffffff;
   }

 </style>
 <!-- Global site tag (gtag.js) - Google Analytics -->
 <script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
 <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2X3F316479');
</script>
</head>
<div id="preloader" >
  <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
  <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
  <div ng-controller="mainCtrl" class="ng-cloak">
    <div id="wrapper">
      <?php include('sidebarList.php');?> 
      
      <div id="testLoad"></div>
      
      <div id="page-content-wrapper">
        <div class="container-fluid">
          <div class="panel panel-default">

          </div>   
        </div>
      </div>

      <div class="col-md-12">
       <div class="box box-primary" style="padding-top: 5px;margin-top: 10px;">
        <!-- <div class="row"> -->
          <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
            <h3 class="box-title"><?php echo Lang::get('content.vehicle_wise_intraday'); ?>
          </h3>
        </div>
        
        <div class="row" style="margin-right: 30px">
          <div class="col-md-1" align="center"></div>

          <div class="col-md-2" align="center">
            <div class="form-group" ng-if="shortNam!=undefined || shortNam!=null">
              <h5 style="color: grey;">{{shortNam}}</h5>
            </div>
                       <!-- <div class="form-group" ng-if="shortNam==undefined || shortNam==null">
                          <h5 style="color: red;">Vehicle not found</h5>
                        </div> -->
                      </div>
                      
                      <div>
                        <?php include('dateTime.php');?>
                      </div>
                    <!--
                    <div class="col-md-1" align="center">
                        <div class="form-group">
                            
                                <select class="input-sm form-control" ng-model="interval">
                                     <option value="">Interval</option>
                                     <option label="1 mins">1</option>
                                     <option label="2 mins">2</option>
                                     <option label="5 mins">5</option>
                                     <option label="10 mins">10</option>
                                     <option label="15 mins">15</option>
                                     <option label="30 mins">30</option>
                                </select>
                           
                        </div>
                    </div>
                  -->
                  <div class="col-md-1" align="center"></div>
                  <div class="col-md-1" align="center">
                    <button style="margin-left: -40%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
                  </div>
                </div>
                <div class="col-md-12" align="right">
                  <?php include('days.php');?>
                </div>
                <div class="row" style="text-align: right;padding-right: 10px;">
                  <span style="background-color: red;
                  color: white;
                  padding: 2px 15px;"></span>
                  <span style="padding-left: 5px;margin-right: 10px;font-weight: bold;">No Data Vehicles</span>
                </div>

                <!--  </div> -->
                <div class="row">
                  <div class="col-md-1" align="center"></div>

                  <div class="col-md-2" align="center">
                    <div class="form-group">

                    </div>
                  </div>
                </div>

              </div>
            </div>

            <div class="col-md-12">
              <div class="box box-primary" style="min-height:570px;"> 
                <div class="pull-left" ng-if="sensorCount>1"  ng-repeat="sensorNo in range(1,sensorCount)"  style="margin-top: 10px;margin-left: 5px;margin-bottom:10px;">
                  <button type="button" class="btn btn-info" ng-click="sensorChange(sensorNo)" id="sensor{{sensorNo}}" ><?php echo Lang::get('content.sensor'); ?> {{sensorNo}}</button>
                </div> 
                <div class="pull-left" ng-if="sensorCount>1" style="margin-top: 10px;margin-left: 5px;margin-bottom:10px;">
                 <button type="button" class="btn btn-info" ng-click="sensorChange('All')" id="sensorAll" ><?php echo Lang::get('content.all'); ?></button>
               </div> 
               <div class="pull-right" style="margin-top: 10px;margin-right: 5px;"> 

                <!-- <button type="button" class="btn btn-info" ng-click="durationFilter('month')" ng-disabled="monthDisabled">Month</button>    -->                 
                <img style="cursor: pointer;" ng-click="exportData('TimeBasedFuelReport')"  src="../resources/views/reports/image/xls.png" />
                <img style="cursor: pointer;" ng-click="exportDataCSV('TimeBasedFuelReport')"  src="../resources/views/reports/image/csv.jpeg" />
                <img style="cursor: pointer;" onclick="generatePDF()"  src="../resources/views/reports/image/Adobe.png" />
              </div>                           
              <div class="col-md-12">
                <div class="box-body" id="TimeBasedFuelReport">

                  <p style="margin-left: 35px;"><span><b>Group :</b> {{fuelConData.orgId}}</span> &nbsp;&nbsp;&nbsp;&nbsp;<span style="margin-left: 40px;"><b><?php echo Lang::get('content.fromdate'); ?></b> : &nbsp;{{uiDate.fromdate}}</span> <span style="margin-left: 40px;"><b>From Time</b> : &nbsp;{{uiDate.fromtime}}</span> <span style="margin-left: 40px;"><b><?php echo Lang::get('content.todate'); ?></b> : &nbsp;{{uiDate.todate}}</span><span style="margin-left: 40px;"><b>To Time</b> : &nbsp;{{uiDate.totime}}</span> </p>

<!--                             <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top:20px;" ng-if="fuelConData[0].error==null">
                              <thead>
                               <tr style="text-align:center;">
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.group'); ?> <?php echo Lang::get('content.name'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{gName}}</th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.veh_name'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{shortNam}}</th>                               
                                </tr>                        
                            
                                <tr style="text-align:center;">
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total <?php echo Lang::get('content.fuel_consumption'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;">{{fuelConsumption}}</th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total <?php echo Lang::get('content.fuel_fill'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;">{{fuelFilling}}</th>
                                </tr>
                                <tr style="text-align:center;">                   
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.total_distance'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{parseInts(dist)}}</th>
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_theft'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{fuelTheft}}</th>
                                </tr>
                                <tr style="text-align:center;">
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total_engine_idle_hrs'); ?></th>
                                    <th ng-if="engineIdleHrs != 'NaN'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{msToTime2(engineIdleHrs)}}</th>
                                    <th ng-if="engineIdleHrs == 'NaN'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">-</th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total_engine_hrs'); ?></th>
                                    <th ng-if="ignitionHrs != 'NaN'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{msToTime2(ignitionHrs)}}</th>
                                    <th ng-if="ignitionHrs == 'NaN'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">-</th>
                                </tr>
                                <tr style="text-align:center;">
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.KMPL'); ?></th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{totalKmpl(dist,fuelConsumption)}}</th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.LTPH'); ?></th>
                                <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{totalLtph(fuelConsumption,ignitionHrs)}}</th> 
                                <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">-</th>-->
                                <!-- </tr> -->
                        <!--   <tr style="text-align:center;">
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Odo Distance</th>
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelData.odoDistance}}</th>
                                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total Trip Distance (Kms)</th>
                                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelData.tripDistance}}</th>
                                </tr> 
                              </thead>
                            </table> -->   
                            <div id="formConfirmation
                            ">
                             <table class="table table-striped table-bordered table-condensed table-hover" style="margin-top:20px;">                                          
                               <thead class='header' style=" z-index: 1;">                              
                                <tr>
                                  <!-- <td class="id" custom-sort order="'date'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.date'); ?></b></td> -->   
                                  <td class="id" custom-sort order="'vehicleName'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.vehicle_name'); ?></b></td>    
                                  <td class="id" custom-sort order="'vehicleMode'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.mode'); ?></b></td>   
                                  <td class="id" custom-sort order="'vehicleModel'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.veh_model'); ?></b></td>                                  
                                  <td class="id" custom-sort order="'startFuel'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.start_fuel'); ?></b></td>
                                  <td class="id" custom-sort order="'endFuel'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.end_fuel'); ?></b></td>
                                  
                                  <td class="id" custom-sort order="'fuelFilling'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_filling'); ?></b></td>
                                  <td class="id" ng-hide="vehiMode=='Dispenser'"  custom-sort order="'fuelTheft'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_theft'); ?></b></td>
                                  <td class="id" ng-hide="vehiMode=='Dispenser'" custom-sort order="'fuelConsumption'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_consumption'); ?></b></td>
                                  <td class="id" ng-show="vehiMode=='Dispenser'" custom-sort order="'fuelConsumption'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_dispensed'); ?></b></td>
                                  <td class="id" custom-sort order="'startKms'" sort="sort" ng-hide="vehicleMode == 'DG'" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.start_kms'); ?></b></td>
                                  <td class="id" custom-sort order="'endKms'" sort="sort" ng-hide="vehicleMode == 'DG'" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.end_kms'); ?></b></td>
                                  <td class="id" custom-sort order="'dist'" sort="sort" ng-hide="vehicleMode == 'DG'" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.dis_travelled'); ?></b></td>
                                  <td class="id" custom-sort order="'kmpl'" sort="sort" ng-hide="vehicleMode == 'Machinery' || vehicleMode == 'DG'" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.kmpl'); ?></b></td>  
                                  <td ng-hide="vehicleMode == 'DG'" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.running_hrs'); ?></b></td> 
                                  <td style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.engine_on'); ?> <?php echo Lang::get('content.hours'); ?></b></td> 
                                  <td  style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.secondary_engine_hrs'); ?></b></td>
                                  <td ng-hide="vehicleMode == 'DG'" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.engine_idle_hrs'); ?></b></td>
                                  <td ng-if="vehicleMode== 'DG'|| vehicleMode== 'Machinery'"class="id" custom-sort order="'ltrsPerHrs'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.ltrs_per_hrs'); ?></b></td>   
                                  <td class="id" custom-sort order="'startLoc'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.start_loc'); ?></b></td> 
                                  <td class="id" custom-sort order="'endLoc'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.end_loc'); ?></b></td>                                   
                                  
                                  <!-- <td style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.remarks'); ?></b></td>   -->                            
                                </tr>
                              </thead>

                              <tbody>                                    
                                <tr ng-if="fuelConData.error==null&&!errMsg.includes('expired')" >                  
                                  <!-- <td>{{fuelConData.date}}</td> -->
                                  <td ng-style="{ 'color':fuelConData.position=='U'?'red':'black' }">{{fuelConData.vehicleName}}</td>
                                  <td ng-style="{ 'color':fuelConData.position=='U'?'red':'black' }">{{fuelConData.vehicleMode?fuelConData.vehicleMode:'-'}}</td>
                                  <td ng-style="{ 'color':fuelConData.position=='U'?'red':'black' }">{{fuelConData.vehicleModel?fuelConData.vehicleModel:'-'}}</td>
                                  <td ng-style="{ 'color':fuelConData.position=='U'?'red':'black' }">{{fuelConData.startFuel}}</td>
                                  <td ng-style="{ 'color':fuelConData.position=='U'?'red':'black' }">{{fuelConData.endFuel}}</td>
                                  
                                  <td ng-style="{ 'color':fuelConData.position=='U'?'red':'black' }">{{fuelConData.fuelFilling}}</td>
                                  <td ng-hide="vehiMode=='Dispenser'" ng-style="{ 'color':fuelConData.position=='U'?'red':'black' }">{{roundOffDecimal(fuelConData.fuelTheft)}}</td>
                                  <td ng-hide="vehiMode=='Dispenser'" ng-style="{ 'color':fuelConData.position=='U'?'red':'black' }">{{fuelConData.fuelConsumption}}</td>
                                  <td ng-show="vehiMode=='Dispenser'" ng-style="{ 'color':fuelConData.position=='U'?'red':'black' }">{{roundOffDecimal(fuelConData.fuelDispenser)}}</td>
                                  <td ng-hide="vehicleMode == 'DG'" ng-style="{ 'color':fuelConData.position=='U'?'red':'black' }">{{parseInts(fuelConData.startKms)}}</td>
                                  <td ng-hide="vehicleMode == 'DG'" ng-style="{ 'color':fuelConData.position=='U'?'red':'black' }">{{parseInts(fuelConData.endKms)}}</td>
                                  <td ng-hide="vehicleMode == 'DG'" ng-style="{ 'color':fuelConData.position=='U'?'red':'black' }">{{fuelConData.dist}}</td>
                                  <td ng-hide="vehicleMode == 'Machinery' || vehicleMode == 'DG'" ng-style="{ 'color':fuelConData.position=='U'?'red':'black' }">{{fuelConData.kmpl}}</td>
                                    <!--<td>-</td>
                                      <td>-</td>-->
                                      <td ng-hide="vehicleMode == 'DG'" ng-style="{ 'color':fuelConData.position=='U'?'red':'black' }">{{msToTime2(fuelConData.engineRunningHrs)}}</td>
                                      <td ng-style="{ 'color':fuelConData.position=='U'?'red':'black' }">{{msToTime2(fuelConData.ignitionHrs)}}</td>
                                      <td  ng-style="{ 'color':fuelConData.position=='U'?'red':'black' }">{{msToTime2(parseInts(fuelConData.secondaryEngineDuration))}}</td>
                                      <td ng-hide="vehicleMode == 'DG'" ng-style="{ 'color':fuelConData.position=='U'?'red':'black' }">{{msToTime2(fuelConData.engineIdleHrs)}}</td>
                                      <td ng-if="vehicleMode=='DG' || vehicleMode == 'Machinery'" ng-style="{ 'color':fuelConData.position=='U'?'red':'black' }">{{fuelConData.ltrsPerHrs}}</td>
                                      <td style="font-size:12px;" ng-style="{ 'color':fuelConData.position=='U'?'red':'black' }">
                                        <span ng-if="fuelConData.startLoc==null || fuelConData.startLoc==undefined || fuelConData.startLoc=='-'">-</span>
                                        <span style="text-decoration: underline;" ng-if="fuelConData.startLoc!=null && fuelConData.startLoc!=undefined && fuelConData.startLoc!='-'">
                                          <a href="#" ng-click="showMap(fuelConData,'start')" data-toggle="modal" data-target="#mapmodals" >{{fuelConData.startLoc}}</a>
                                        </span>
                                      </td> 
                                      <td style="font-size:12px;" ng-style="{ 'color':fuelConData.position=='U'?'red':'black' }">
                                        <span ng-if="fuelConData.endLoc==null || fuelConData.endLoc==undefined || fuelConData.endLoc=='-'">-</span>
                                        <span style="text-decoration: underline;" ng-if="fuelConData.endLoc!=null && fuelConData.endLoc!=undefined && fuelConData.endLoc!='-'">
                                          <a href="#" ng-click="showMap(fuelConData,'end')" data-toggle="modal" data-target="#mapmodals" >{{fuelConData.endLoc}}</a>
                                        </span>
                                      </td> 
                                      
                                      <!-- <td>{{fuelConData.remarks}}</td> -->
                                    </tr>

                                      <!-- <tr ng-if="fuelConData.error==null">                  

                                        <td style="font-size: 13px;font-weight: bold;"><?php echo Lang::get('content.total'); ?></td> -->                                       
                                   <!-- <td style="font-size: 13px;font-weight: bold;">{{startFuel}}</td>
                                    <td style="font-size: 13px;font-weight: bold;">{{endFuel}}</td> -->
                                        <!-- <td style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{fuelConsumption}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{fuelFilling}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{fuelTheft}}</td> -->
                                   <!-- <td style="font-size: 13px;font-weight: bold;">{{parseInts(startKms)}}</td>
                                    <td style="font-size: 13px;font-weight: bold;">{{parseInts(endKms)}}</td> -->
                                        <!-- <td style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">-</td> 
                                        <td style="font-size: 13px;font-weight: bold;">{{parseInts(dist)}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{totalKmpl(dist,fuelConsumption)}}</td>                                   
                                        <td ng-if="engineHrs != 'NaN'" style="font-size: 13px;font-weight: bold;">{{msToTime2(engineHrs)}}</td>
                                        <td ng-if="engineHrs == 'NaN'" style="font-size: 13px;font-weight: bold;">-</td>
                                        <td ng-if="engineIdleHrs != 'NaN'" style="font-size: 13px;font-weight: bold;">{{msToTime2(engineIdleHrs)}}</td>
                                        <td ng-if="engineIdleHrs == 'NaN'" style="font-size: 13px;font-weight: bold;">-</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{totalLtph(fuelConsumption,ignitionHrs)}}</td>
                                        <td style="font-size: 13px;font-weight: bold;">{{msToTime2(ignitionHrs)}}</td>
                                        <td>-</td>
                                      </tr> -->
                                      
                                      <tr ng-if="fuelConData.error!=null" style="text-align: center">
                                        <td colspan="{{ vehicleMode == 'DG'?14:20 }}" class="err" ng-if="!errMsg"><h5>{{fuelConData.error}}</h5></td>
                                        
                                      </tr>
                                      <tr ng-if="errMsg" style="text-align: center">
                                        <td colspan="{{ vehicleMode == 'DG'?14:20 }}" class="err"><h5>{{errMsg}}</h5></td>
                                      </tr>

                                 <!-- <tr ng-if="fuelConData.length==0" style="text-align: center">
                                        <td colspan="20" class="err"><h5>No Data Found! Choose some other date and time!</h5></td>
                                      </tr> -->
                                    </table> 
                                  </div>                  
                                </div>


                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="modal fade" id="mapmodals">
                        <div class="modal-dialog modal-lg">
                          <div class="modal-content">
                            <div class="modal-header" style="background-color: #196481;
                            color: #fff";>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: #fff">&times;</button>
                            <h4  id="myCity">Map View
                            </h4>
                          </div>
                          <div class="modal-body">
                            <div class="map_container">
                              <div id="map_canvas" class="map_canvas" style="width: 100%; height: 500px;"></div>
                            </div>
                          </div>
                        </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                    <script>
                      var scriptLibrary = [];
                      var apikey_url = JSON.parse(localStorage.getItem('apiKey'));
                      console.log('Api_Key : '+apikey_url);
                      var url = "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places,geometry";

                      if(apikey_url != null || apikey_url != undefined) {
                       url = "https://maps.googleapis.com/maps/api/js?key="+apikey_url+"&libraries=places,geometry";
                     }

                     scriptLibrary.push(url); 
                     var fileref = document.createElement('script');
                     fileref.setAttribute("type","text/javascript");
                     fileref.setAttribute("src", scriptLibrary[0]);
                     document.getElementsByTagName("head")[0].appendChild(fileref)
                   </script>
                   <script src="assets/js/static.js"></script>
                   <script src="assets/js/jquery-1.11.0.js"></script>
                   <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script> 
                   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
                   <script data-require="angular-ui-bootstrap@0.11.0" data-semver="0.11.0" src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>
                   <script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
                   <script src="assets/js/bootstrap.min.js"></script>
                   <script src="assets/js/angular-translate.js"></script>
                   <!--<script src="assets/js/highcharts.js"></script> -->
                   <script src="https://code.highcharts.com/highcharts.js"></script>
                   <script src="https://code.highcharts.com/modules/exporting.js"></script>
                   <script src="https://code.highcharts.com/modules/export-data.js"></script>
                   <script src="../resources/views/reports/customjs/moment.js"></script>
                   <script src="../resources/views/reports/customjs/FileSaver.js"></script>
                   <script src="../resources/views/reports/customjs/html5csv.js"></script>
                   <script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
                   <script src="../resources/views/reports/datatable/jquery.dataTables.js"></script>
                   <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
                   <!--<script src="assets/js/naturalSortVersionDates.js"></script> -->
                   <script src="assets/js/vamoApp.js"></script>
                   <script src="assets/js/services.js"></script>
                   <script src="assets/js/fuelConsolidateTime.js?v=<?php echo Config::get('app.version');?>"></script>


                   <script>
        //$("#example1").dataTable();
        
        $("#menu-toggle").click(function(e) {
          e.preventDefault();
          $("#wrapper").toggleClass("toggled");
        });
        
        $("#menu-toggle").click(function(e) {
          e.preventDefault();
          $("#wrapper").toggleClass("toggled");
        });
        var generatePDF = function() {
          kendo.drawing.drawDOM($("#formConfirmation"),{paperSize: "A4",multiPage: true,margin: {
            left   : "4mm",
            top    : "10mm",
            right  : "4mm",
            bottom : "10mm"
          } ,landscape: true }).then(function(group) {
            kendo.drawing.pdf.saveAs(group, "VehicleWiseIntradayFuelReport.pdf");
          });
        }

      </script>
      
    </body>
    </html>
