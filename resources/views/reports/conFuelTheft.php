<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title><?php echo Lang::get('content.gps'); ?></title>

  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
  <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
  <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
  <link href="assets/css/web.css" rel="stylesheet" /> <!-- added for table freezing -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
  <!-- pdfgen -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
  <script src="https://unpkg.com/jspdf-autotable@2.3.2/dist/jspdf.plugin.autotable.js"></script>
  <script src="assets/js/gridviewscroll.js"></script> <!-- added for table freezing -->

  <style>
    body {
      font-family: 'Lato', sans-serif;
      /* font-weight: bold; */  
   /* font-family: 'Lato', sans-serif;
      font-family: 'Roboto', sans-serif;
      font-family: 'Open Sans', sans-serif;
      font-family: 'Raleway', sans-serif;
      font-family: 'Faustina', serif;
      font-family: 'PT Sans', sans-serif;
      font-family: 'Ubuntu', sans-serif;
      font-family: 'Droid Sans', sans-serif;
      font-family: 'Source Sans Pro', sans-serif;
      */
    }
    .empty{
      height: 1px; width: 1px; padding-right: 30px; float: left;
    }
    .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
     background-color: #ffffff;
   }

 </style>
 <!-- Global site tag (gtag.js) - Google Analytics -->
 <script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
 <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2X3F316479');
</script>
</head>
<div id="preloader" >
  <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
  <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
  <div ng-controller="mainCtrl" class="ng-cloak">
    <div id="wrapper">
     <?php include('sidebarList.php');?> 
     
     <div id="testLoad"></div>
     
     <div id="page-content-wrapper">
      <div class="container-fluid">
        <div class="panel panel-default">
         
        </div>   
      </div>
    </div>
    


    <div class="col-md-12">
     <div class="box box-primary" style="padding-top: 5px;margin-top: 10px;">
      <!-- <div class="row"> -->
        <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
          <h3 class="box-title"><span><?php echo Lang::get('content.consolidated'); ?>  <?php echo Lang::get('content.fuel_theft'); ?> <?php echo Lang::get('content.report'); ?></span>
            <span><?php include('OutOfOrderDataInfo.php');?></span> 
          </h3>
          <div class="box-tools pull-right" style="padding-bottom: 10px;">
           <?php include('helpVideos.php');?>
         </div>
       </div>
       <div class="row">
         <div class="col-md-2" align="center"></div>
         
         <div class="col-md-2 form-group" style="right:0px;"><?php echo Lang::get('content.month_year'); ?>:</div>
         <div class="col-md-2" align="center">
           <div class="input-group datecomp" style="padding-bottom: 20px;">
             <input type="text" ng-model="fromMonthss" class="form-control placholdercolor" id="monthFrom" placeholder="<?php echo Lang::get('content.month'); ?>">
           </div>
         </div>
         <div class="col-md-1" align="center">
          <button style=" padding : 5px" ng-click="submitMon()"><?php echo Lang::get('content.submit'); ?></button>
        </div>
        
        
      </div>
      <div class="row" style="text-align: right;padding-right: 10px;padding-bottom: 10px;">
        <span style="background-color: red;
        color: white;
        padding: 2px 15px;"></span>
        <span style="padding-left: 5px;margin-right: 10px;font-weight: bold;">No Data vehicles</span>
      </div>
    </div>
  </div>

  <div class="col-md-12">
    <div class="box box-primary" style="min-height:570px;">
      
      <div>

        <div class="pull-right" style="margin-top: 10px;margin-right: 5px;">
          <img style="cursor: pointer;" ng-click="exportData('ConFuelTheftReport')"  src="../resources/views/reports/image/xls.png" />
          <img style="cursor: pointer;" ng-click="exportDataCSV('ConFuelTheftReport')"  src="../resources/views/reports/image/csv.jpeg" />
          <img style="cursor: pointer;" onclick="generateAutoPDF('Consolidated_Fuel_Theft_Report','table')"  src="../resources/views/reports/image/Adobe.png" />
        </div>

        
        
        <div class="box-body" id="ConFuelTheftReport"  style="padding-top:20px;" > 

          <div class="empty" align="center"></div>
          

          <div class="row" style="padding-top: 20px;"></div>

          <div>
            <div style="height:35px;text-align: center;">
              <div colspan="33" style="font-size:14px;font-weight: bold;background-color:#C2D2F2;padding: 5px">
              {{monthsVal[monVals-1]}}-{{yearVals}}</div>
            </div>
            <table ng-hide="fuelTheftError" cellspacing="0" style="width: 100%; border-collapse: collapse;" id='gvMain'>

             <!-- <thead class='header' style=" z-index: 1;"> -->
              
              <tr class="GridViewScrollHeader">
                <th style="background-color:#f9f9f9;font-size: 12px;"> <?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.name'); ?></th>
                <th ng-repeat="dat in monthDates" style="background-color:#ecf7fb;font-weight: bold;">{{dat}}</th>
                <th style="background-color:#ecf7fb;font-weight: bold;"><?php echo Lang::get('content.total'); ?><?php echo Lang::get('content.ltrs'); ?> </th>
              </tr>
                            <!-- </thead>
                              <tbody > -->
                                <tr class="GridViewScrollItem" ng-repeat="fuelTheft in monthFuelTheftData" >

                                 <td ng-if="$odd" style="background-color:#f9f9f9;font-weight: bold;" ng-style="{ 'color':fuelTheft.position=='U'?'red':'black' }" >{{fuelTheft.shortName}}</td>
                                 <td ng-if="$even" style="background-color:#ecf7fb;font-weight:bold;" ng-style="{ 'color':fuelTheft.position=='U'?'red':'black' }" >{{fuelTheft.shortName}}</td>
                                 <td ng-repeat="fuelVal in fuelTheft.fuel track by $index" ng-style="{ 'color':fuelTheft.position=='U'?'red':'black' }">{{fuelVal}}</td>
                                 <td>{{fuelTheft.totalFuel}}</td>
                               </tr>
                               <!-- <tr ng-if="monthFuelTheftData!=null">
                                  <td style="background-color:#ecf7fb;font-weight: bold;"><?php echo Lang::get('content.total'); ?><?php echo Lang::get('content.KMS'); ?></td>
                                  <td ng-repeat="val in totDistVehic track by $index">{{parseInts(val)}}</td>
                                  <td style="background-color:#f9f9f9;"></td>
                                </tr> -->
                                
                                <!-- </tbody>      -->       
                              </table>
                              <table ng-if="fuelTheftError " cellspacing="0" style="width: 100%; border-collapse: collapse;">
                                <tr ng-if="fuelTheftError" style="text-align: center">
                                  <td colspan="22" class="err"><h5>{{fuelTheftError}}</h5></td>
                                </tr>        
                              </table>
                            </div>
                          </div>
                        </div>
                        
                      </div>
                    </div>

                  </div>
                </div>


                <script src="assets/js/static.js"></script>
                <script src="assets/js/jquery-1.11.0.js"></script>
                <script src="assets/js/bootstrap.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
                <script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
                <script src="assets/js/angular-translate.js"></script>
                <script src="../resources/views/reports/customjs/html5csv.js"></script>
                <script src="../resources/views/reports/customjs/moment.js"></script>
                <script src="../resources/views/reports/customjs/FileSaver.js"></script>
                <script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
                
                <script src="assets/js/vamoApp.js"></script>
                <script src="assets/js/services.js"></script>
                <script src="assets/js/conFuelTheft.js?v=<?php echo Config::get('app.version');?>"></script>
                
                <script>

                 
        //$("#example1").dataTable();
        
        $("#menu-toggle").click(function(e) {
          e.preventDefault();
          $("#wrapper").toggleClass("toggled");
        });
        
        $('#monthFrom').datetimepicker({
          minViewMode: 'months',
          viewMode: 'months',
          pickTime: false,
          useCurrent:true,
          format:'MM/YYYY',
          maxDate: new Date,
          minDate: new Date(2015, 12, 1)
        });

      </script>
      <script type="text/javascript">
        var gridViewScroll = null;
        window.onload = function () {

          gridViewScroll = new GridViewScroll({
            elementID: "gvMain",
            width: window.screen.width-320,
            height: window.screen.height-420,
            freezeColumn: true,
                //freezeFooter: true,
                freezeColumnCssClass: "GridViewScrollItemFreeze",
                //freezeFooterCssClass: "GridViewScrollFooterFreeze",
                freezeHeaderRowCount: 1,
                freezeColumnCount: 1,
                onscroll: function (scrollTop, scrollLeft) {
                    // console.log(scrollTop + " - " + scrollLeft);
                  }
                });
          gridViewScroll.enhance();
            //setTimeout(function(){ enhance(); }, 1000);
          }
          function enhance() {
            gridViewScroll.enhance();
          }
          function undo() {
            gridViewScroll.undo();
          }
          
        </script>
        
      </body>
      </html>