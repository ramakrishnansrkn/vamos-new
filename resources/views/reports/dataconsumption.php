<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title><?php echo Lang::get('content.gps'); ?></title>
  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
  <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
  <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
  <link href="../resources/views/assets/css/datebutton.css" rel="stylesheet" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
  <!-- pdfgen -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
  <script src="https://unpkg.com/jspdf-autotable@2.3.2/dist/jspdf.plugin.autotable.js"></script>
  <style>
    .empty{
      height: 1px; width: 1px; padding-right: 30px; float: left;
    }
    .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
      background-color: #ffffff;
    }

    body{
      font-family: 'Lato', sans-serif;
/*font-weight: bold;
font-family: 'Lato', sans-serif;
font-family: 'Roboto', sans-serif;
font-family: 'Open Sans', sans-serif;
font-family: 'Raleway', sans-serif;
font-family: 'Faustina', serif;
font-family: 'PT Sans', sans-serif;
font-family: 'Ubuntu', sans-serif;
font-family: 'Droid Sans', sans-serif;
font-family: 'Source Sans Pro', sans-serif;
*/
} 

.col-md-12 {
  width: 98% !important;
  left: 15px !important;
  padding-left: 20px !important;
}

/*
.chart {
    min-width: 320px;
    max-width: 800px;
    height: 280px;
    margin: 0 auto;
}

#container {
    width: 80%;
    height: unset !important; 
    max-width: 80%;
    max-height: unset !important; 
    min-height: 500px !important;
    margin-top: 10px !important;
}
*/
</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2X3F316479');
</script>
</head>
<div id="preloader" >
  <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
  <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
  <div ng-controller="mainCtrl" class="ng-cloak">
    <div id="wrapper">

      <?php include('sidebarList.php');?>

      <div id="testLoad"></div>
      
      <div id="page-content-wrapper">
        <div class="container-fluid">
          <div class="panel panel-default"></div>   
        </div>
      </div>
      
      <div class="col-md-12">
       <div class="box box-primary" style="padding-top: 5px;margin-top: -20px;">
        <!-- <div class="row"> -->
          <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
            <h3 class="box-title"><?php echo Lang::get('content.data_consumption'); ?>
          </h3>
        </div>
        <div class="row">
          <div class="col-md-2" align="center">
            <div class="form-group" ng-if="shortNam!=undefined || shortNam!=null">
              <h5 style="color: grey;">{{shortNam}}</h5>
            </div>
                  <!--  <div class="form-group" ng-if="shortNam==undefined || shortNam==null">
                          <h5 style="color: red;">Vehicle not found</h5>
                        </div> -->
                      </div>
                      <div class="col-md-1" align="center"></div>
                      <div>
                        <div class="col-md-2" align="center">
                          <div class="form-group">
                            <div class="input-group datecomp">
                              <input type="text" ng-model="uiDate.fromdate" class="form-control placholdercolor" id="dateFrom"  placeholder="<?php echo Lang::get('content.from_date'); ?>">
                              <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                            </div>
                          </div>
                        </div> 
                        <div class="col-md-2" align="center">
                          <div class="form-group">
                            <div class="input-group datecomp">
                              <input type="text" ng-model="uiDate.todate" class="form-control placholdercolor" id="dateTo" placeholder="<?php echo Lang::get('content.to_date'); ?>">
                              <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                            </div>
                          </div>
                        </div>
                      </div>
                      
             <!--       <div class="col-md-1" align="center">
                          <div class="form-group">
                              <div class="input-group datecomp">
                                <select class="input-sm form-control" ng-model="interval">
                                   <option value="">Interval</option>
                                   <option label="1 mins">1</option>
                                   <option label="2 mins">2</option>
                                   <option label="5 mins">5</option>
                                   <option label="10 mins">10</option>
                                   <option label="15 mins">15</option>
                                   <option label="30 mins">30</option>
                                </select>
                            </div>
                          </div>
                        </div>  -->

                        <div class="col-md-1" align="center">
                          <button style="margin-left: -42%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
                        </div>
                        <div class="col-md-12" align="right" style="margin-bottom: 10px;margin-left:100px ">
                         <div class="col-md-2"></div>
                         <button type="button" class="btnDate" ng-click="durationFilter('yesterday')" ng-disabled="yesterdayDisabled" onclick="setToTime('yesterday')"><?php echo Lang::get('content.yesterday'); ?></button>
                         <button type="button" class="btnDate" ng-if="day!=1" ng-click="durationFilter('thisweek')" ng-disabled="thisweekDisabled" onclick="setToTime('thisweek')"><?php echo Lang::get('content.thisweek'); ?></button>
                         <button type="button" class="btnDate" ng-click="durationFilter('lastweek')" ng-disabled="weekDisabled" onclick="setToTime('lastweek')"><?php echo Lang::get('content.lastweek'); ?></button>
                         <button type="button" class="btnDate" ng-click="durationFilter('month')" ng-disabled="monthDisabled" onclick="setToTime('thismonth')"><?php echo Lang::get('content.thismonth'); ?></button>  
                         <button type="button" class="btnDate" ng-click="durationFilter('lastmonth')" ng-disabled="lastmonthDisabled" style="margin-right: 10px;" onclick="setToTime('lastmonth')"><?php echo Lang::get('content.lastmonth'); ?></button>             
                       </div>
                     </div>

                     <!-- </div> -->
                     <div class="row">
                      <div class="col-md-1" align="center"></div>
                      <div class="col-md-2" align="center">
                        <div class="form-group"></div>
                      </div>
                    </div>
                  </div>

                </div>

                <div class="col-md-12" style="border-color: unset!important;">
                  <div class="box box-primary" style="min-height:570px;background-color:#fdfdfd;">
                    <div>
                      <div class="pull-right" style="margin-top: 10px;margin-right: 5px;margin-bottom:10px;">

                        <img style="cursor: pointer;" ng-click="exportData('dataConsumptionReport')"  src="../resources/views/reports/image/xls.png" />
                        <img style="cursor: pointer;" ng-click="exportDataCSV('dataConsumptionReport')"  src="../resources/views/reports/image/csv.jpeg" />
                        <img style="cursor: pointer;" onclick="generateAutoPDF2('DataConsumption_report','table2','table_address2');"  src="../resources/views/reports/image/Adobe.png" />

                      </div>
                      
                      <div class="box-body">

                        <div style="margin-top: 20px;" id="dataConsumptionReport">
                          <div id="formConfirmation">
                            <table class="table table-bordered table-striped table-condensed table-hover" id='table2'>
                              <head><meta charset='UTF-8'></head>
                              <thead>
                                <tr style="text-align:center;">
                                  <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.group'); ?></th>
                                  <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(gName)}}</th>
                                  <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.veh_name'); ?></th>       
                                  <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{shortNam}}</th>   
                                </tr>
               <!-- <tr style="text-align:center;">
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Vehicle Name</th>
                      <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{shortNam}}</th>
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total Theft Fuel(ltrs)</th>
                      <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{roundOff(fuelTheft.totalTheftFuel)}}</th>   
                    </tr> -->
                  </thead>
                </table>

                <table class="table table-striped table-bordered table-condensed table-hover" id="table_address2"  style="margin-top: 10px;"> 
                  <thead id="protocolscroll" style=" z-index: 1;">
                    <tr style="text-align:center">
                      <th class="id" custom-sort order="'date'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.date'); ?></th>
                      <th class="id" custom-sort order="'deviceModel'" sort="sort" style="background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.device_model'); ?></th>
                      <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;" class="id" custom-sort order="'dataUsage'" sort="sort"><?php echo Lang::get('content.data_usage'); ?></th>
                    </tr>
                  </thead>

                  <tbody>
                    <tr ng-repeat="data in dataConsumption  | orderBy:natural(sort.sortingOrder):sort.reverse"  class="active" ng-if="dataConsumption[0].error=='-'" ng-if="!errMsg.includes('expired')">
                      <td>{{data.date | date:'dd-MM-yyyy'}}</td>   
                      <td >{{data.deviceModel}}</td>               
                      <td>{{data.dataUsage}}</td>
                      
                    </tr> 
                    <tr align="center">
                      <td colspan="3" ng-if="(dataConsumption[0].error != '-'&&!errMsg)" class="err" style="text-align: center;">
                       <h5><?php echo Lang::get('content.no_data'); ?></h5>
                     </td>
                     <td colspan="3"  ng-if="errMsg" class="err" style="text-align: center;">
                       <h5>{{errMsg}}</h5>
                     </td>
                   </tr>
                 </tbody>
                 
               </table> 
               
             </div>
           </div>

         </div>
       </div>
       
     </div>
   </div>

 </div>
</div>

<script src="assets/js/static.js"></script>
<!--<script src="assets/js/jquery-1.11.0.js"></script>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<!--<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>-->
<!--<script src="assets/js/bootstrap.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
<script src="assets/js/angular-translate.js"></script>
<script src="../resources/views/reports/customjs/html5csv.js"></script>
<script src="../resources/views/reports/customjs/moment.js"></script>
<script src="../resources/views/reports/customjs/FileSaver.js"></script>
<script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
<script src="../resources/views/reports/datatable/jquery.dataTables.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<!--<script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>-->
  <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
  <script src='assets/js/table-fixed-header.js'></script>
  <!--<script src="assets/js/naturalSortVersionDates.js"></script>-->
  <script src="assets/js/vamoApp.js"></script>
  <script src="assets/js/services.js"></script>
  <script src="assets/js/fuelProtocol.js?v=<?php echo Config::get('app.version');?>"></script>
  
  <script>

    $("#example1").dataTable();
    
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
    
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });

    var generatePDF = function() {
      let tableName='dataConsumptionReport';
      generateAutoPDF2(tableName,'table2','table_address2');
    }
    var scrollinit=1
    $(window).scroll(function (event) {
      if(scrollinit){
        scrollinit=0;
        $('#table_address2').addClass('table-fixed-header1');
        $('#protocolscroll').addClass('header');
        $('.table-fixed-header1').fixedHeader();
      }
      
    });
  </script>
  
</body>
</html>
