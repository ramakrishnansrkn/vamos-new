<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title><?php echo Lang::get('content.gps'); ?></title> 
  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
  <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
  <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
  <link href="../resources/views/assets/css/datebutton.css" rel="stylesheet" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
  <!-- pdfgen -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
  <script src="https://unpkg.com/jspdf-autotable@2.3.2/dist/jspdf.plugin.autotable.js"></script>
  <style>
    body {
      font-family: 'Lato', sans-serif;
      /* font-weight: bold; */  
   /* font-family: 'Lato', sans-serif;
      font-family: 'Roboto', sans-serif;
      font-family: 'Open Sans', sans-serif;
      font-family: 'Raleway', sans-serif;
      font-family: 'Faustina', serif;
      font-family: 'PT Sans', sans-serif;
      font-family: 'Ubuntu', sans-serif;
      font-family: 'Droid Sans', sans-serif;
      font-family: 'Source Sans Pro', sans-serif;
      */
    }
    .empty{
     height: 1px; width: 1px; padding-right: 30px; float: left;
   }
   .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
     background-color: #ffffff;
   }

 </style>
 <!-- Global site tag (gtag.js) - Google Analytics -->
 <script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
 <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2X3F316479');
</script>
</head>
<div id="preloader" >
  <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
  <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
  <div ng-controller="mainCtrl" class="ng-cloak">
    <div id="wrapper">
      <?php include('sidebarList.php');?> 

      <div id="testLoad"></div>

      <div id="page-content-wrapper">
        <div class="container-fluid">
          <div class="panel panel-default">

          </div>   
        </div>
      </div>

      <div class="col-md-12">
       <div class="box box-primary" style="padding-top: 5px;margin-top: 10px;">
        <!-- <div class="row"> -->
          <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
            <h3 class="box-title"><span><?php echo Lang::get('content.vehicle_wise_report'); ?></span>
              <span><?php include('OutOfOrderDataInfo.php');?></span> 
            </h3>
          </div>
          
          <div class="row">
            <div class="col-md-1" align="center"></div>

            <div class="col-md-2" align="center">
              <div class="form-group" ng-if="shortNam!=undefined || shortNam!=null">
                <h5 style="color: grey;">{{shortNam}}</h5>
              </div>
                       <!-- <div class="form-group" ng-if="shortNam==undefined || shortNam==null">
                          <h5 style="color: red;">Vehicle not found</h5>
                        </div> -->
                      </div>

                      <div class="col-md-2" align="center">
                        <div class="form-group">

                          <div class="input-group datecomp">
                            <input type="text" ng-model="uiDate.fromdate" class="form-control placholdercolor" id="dtFrom"  placeholder="<?php echo Lang::get('content.fromdate'); ?>">
                            <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                          </div>
                        </div>                       
                      </div>
                      <div class="col-md-2" align="center">
                        <div class="form-group">
                          <div class="input-group datecomp">

                            <input type="text" ng-model="uiDate.todate" class="form-control placholdercolor" id="dtTo" placeholder="<?php echo Lang::get('content.todate'); ?>">
                            <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                          </div>
                        </div>
                      </div>
                      
                      <div class="col-md-2" align="center">
                        <select style="
                        height: 35px;
                        width: 115px;"  ng-model="orgTime" ng-change="changeTime(orgTime,$index)"  ng-show="showOrgTime"   >
                        
                        <option style="width: 100%;height: 100%"  ng-repeat="time in orgShiftTime track by $index | orderBy:time"    >{{time}}</option>
                        
                      </select>
                    </div>
                    <div class="col-md-1" align="center">
                      <button style="margin-left: -150%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
                    </div>
                    
                  </div>
                  <div  style="margin-bottom: 20px;margin-left: 300px">  
                    <button type="button" class="btnDate" ng-click="durationFilter('yesterday')" ng-disabled="yesterdayDisabled" onclick="setToTime('yesterday')"><?php echo Lang::get('content.yesterday'); ?></button>
                    <button type="button" class="btnDate" ng-click="durationFilter('thisweek')" ng-disabled="thisweekDisabled" onclick="setToTime('thisweek')"><?php echo Lang::get('content.thisweek'); ?></button>
                    <button type="button" class="btnDate" ng-click="durationFilter('lastweek')" ng-disabled="weekDisabled" onclick="setToTime('lastweek')"><?php echo Lang::get('content.lastweek'); ?></button>
                    <button type="button" class="btnDate" ng-click="durationFilter('month')" ng-disabled="monthDisabled" onclick="setToTime('thismonth')"><?php echo Lang::get('content.thismonth'); ?></button>
                    <!-- <button type="button" class="btnDate" ng-click="durationFilter('lastmonth')" ng-disabled="lastmonthDisabled" style="margin-right: 10px;" onclick="setToTime('lastmonth')"><?php echo Lang::get('content.lastmonth'); ?></button> -->
                    
                  </div>
                  <div class="row" style="text-align: right;padding-right: 10px;">
                    <span style="background-color: red;
                    color: white;
                    padding: 2px 15px;"></span>
                    <span style="padding-left: 5px;margin-right: 10px;font-weight: bold;">No Data Vehicles</span>
                  </div>

                  <!--  </div> -->
                  <div class="row">
                    <div class="col-md-1" align="center"></div>

                    <div class="col-md-2" align="center">
                      <div class="form-group">

                      </div>
                    </div>
                  </div>

                </div>
              </div>

              <div class="col-md-12">
                <div class="box box-primary" style="min-height:570px;"> 

                  <div class="pull-right" style="margin-top: 10px;margin-right: 5px;">  
                    <img style="cursor: pointer;" ng-click="exportData('VehicleWiseFuelReport')"  src="../resources/views/reports/image/xls.png" />
                    <img style="cursor: pointer;" ng-click="exportDataCSV('VehicleWiseFuelReport')"  src="../resources/views/reports/image/csv.jpeg" />
                    <img style="cursor: pointer;" ng-click="generatePDF()"   src="../resources/views/reports/image/Adobe.png" />
                  </div>                           

                  <div class="box-body" id="VehicleWiseFuelReport1">
                    <p ng-if="showOrgTime" style="margin-top: 5px;" class="pull-left"><span style="margin-left: 40px;"><b><?php echo Lang::get('content.date_time'); ?></b> : &nbsp;{{uiDate.fromdate}}&nbsp;{{getAMPM(fromtotime[0])}} - {{uiDate.todate}}&nbsp;{{getAMPM(fromtotime[1])}}</span> </p>
                    <p ng-if="!showOrgTime" style="margin-left: 30px;margin-top: 5px;" class="pull-left"><span style="margin-left: 40px;"><b><?php echo Lang::get('content.organization'); ?><?php echo Lang::get('content.name'); ?></b> : &nbsp;{{orgName}}</span><span style="margin-left: 40px;"><b><?php echo Lang::get('content.date_time'); ?></b> : &nbsp;{{uiDate.fromdate}}&nbsp;- {{uiDate.todate}}&nbsp;</span> </p>

                    <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top:20px;" ng-if="(fuelConData[0].error==null)&&!errMsg.includes('expired')" id='table1'>
                      <thead ng-if="vehicleMode == 'DG'">
                       <tr style="text-align:center;">
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.group'); ?> <?php echo Lang::get('content.name'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{gName}}</th>
                        <th colspan="2" ng-hide="vehiMode=='Dispenser'" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_theft'); ?></th>
                        <th colspan="2" ng-hide="vehiMode=='Dispenser'" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{roundOffDecimal(fuelTheft)}}</th>                              
                      </tr>                        
                      <tr style="text-align:center;">
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.mode'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{vehicleMode?vehicleMode:'-'}}</th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.veh_model'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{vehicleModel?vehicleModel:'-'}}</th>                               
                      </tr>  
                      <tr style="text-align:center;">
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.engine_on'); ?> <?php echo Lang::get('content.hours'); ?>  </th>
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(ignitionHrs != 'NaN')?msToTime2(ignitionHrs):'-'}}</th>
                        <th colspan="2"  style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total <?php echo Lang::get('content.fuel_consumption'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;">{{roundOffDecimal(fuelConsumption)}}</th>
                      </tr>
                      <tr style="text-align:center;">                   
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.secondary_engine_hrs'); ?></th>
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(secIgnitionHrs != 'NaN')?msToTime2(secIgnitionHrs):'-'}}</th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.engine_on'); ?> <?php echo Lang::get('content.hours'); ?>  </th>
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(ignitionHrs != 'NaN')?msToTime2(ignitionHrs):'-'}}</th>                  
                      </tr>
                      <tr style="text-align:center;">
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total <?php echo Lang::get('content.fuel_fill'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;">{{roundOffDecimal(fuelFilling)}}</th>
                       
                        <th  colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"> <?php echo Lang::get('content.LTPH'); ?></th>
                        <th  colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{ totalLtph(fuelConsumption,ignitionHrs)}}</th> 
                      </tr>
                    </thead>
                     <thead ng-if="vehicleMode =='Machinery'">
                       <tr style="text-align:center;">
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.group'); ?> <?php echo Lang::get('content.name'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{gName}}</th>
                        <th colspan="2"  style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total <?php echo Lang::get('content.fuel_consumption'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;">{{roundOffDecimal(fuelConsumption)}}</th>                         
                      </tr>                        
                      <tr style="text-align:center;">
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.mode'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{vehicleMode?vehicleMode:'-'}}</th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.veh_model'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{vehicleModel?vehicleModel:'-'}}</th>                               
                      </tr>  
                      <tr style="text-align:center;">                  
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.engine_on'); ?> <?php echo Lang::get('content.hours'); ?>  </th>
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(ignitionHrs != 'NaN')?msToTime2(ignitionHrs):'-'}}</th>
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.total_distance'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{ parseInts(dist)}}</th>                    
                      </tr>
                      <tr style="text-align:center;">
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.engine_on'); ?> <?php echo Lang::get('content.hours'); ?>  </th>
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(ignitionHrs != 'NaN')?msToTime2(ignitionHrs):'-'}}</th>
                        <th  colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total_engine_idle_hrs'); ?></th>
                        <th  colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(engineIdleHrs != 'NaN')?msToTime2(engineIdleHrs):'-'}}</th>
                      </tr>
                      <tr>
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.veh_name'); ?></th>
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{shortNam}}</th>  
                      <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_theft'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{roundOffDecimal(fuelTheft)}}</th>                             
                      </tr>
                      <tr style="text-align:center;">
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total <?php echo Lang::get('content.fuel_fill'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;">{{roundOffDecimal(fuelFilling)}}</th>
                        
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.LTPH'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{ totalLtph(fuelConsumption,ignitionHrs)}}</th> 
                      </tr>
                    </thead>
                    <thead ng-if="vehicleMode !='DG' && vehicleMode !='Machinery'">
                       <tr style="text-align:center;">
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.group'); ?> <?php echo Lang::get('content.name'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{gName}}</th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.veh_name'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{shortNam}}</th>                               
                      </tr>                        
                      <tr style="text-align:center;">
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.mode'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{vehicleMode?vehicleMode:'-'}}</th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.veh_model'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{vehicleModel?vehicleModel:'-'}}</th>                               
                      </tr>  
                      <tr style="text-align:center;">
                        <th colspan="2" ng-hide="vehicleMode=='Dispenser'" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_theft'); ?></th>
                        <th colspan="2" ng-hide="vehicleMode=='Dispenser'" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{roundOffDecimal(fuelTheft)}}</th>
                        <th colspan="2" ng-show="vehicleMode=='Dispenser'" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_dispensed'); ?></th>
                        <th colspan="2" ng-show="vehicleMode=='Dispenser'" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{roundOffDecimal(fuelDispenser)}}</th>
                        <th colspan="2"  style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total <?php echo Lang::get('content.fuel_consumption'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;">{{roundOffDecimal(fuelConsumption)}}</th>
                      </tr>
                      <tr style="text-align:center;">                  
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.engine_on'); ?> <?php echo Lang::get('content.hours'); ?>  </th>
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(ignitionHrs != 'NaN')?msToTime2(ignitionHrs):'-'}}</th>
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.total_distance'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{ parseInts(dist)}}</th>                    
                      </tr>
                      <tr style="text-align:center;">
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.engine_on'); ?> <?php echo Lang::get('content.hours'); ?>  </th>
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(ignitionHrs != 'NaN')?msToTime2(ignitionHrs):'-'}}</th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total_engine_idle_hrs'); ?></th>
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(engineIdleHrs != 'NaN')?msToTime2(engineIdleHrs):'-'}}</th>
                      </tr>
                      <tr style="text-align:center;">
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total <?php echo Lang::get('content.fuel_fill'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;">{{roundOffDecimal(fuelFilling)}}</th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.KMPL'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{totalKmpl(dist,fuelConsumption)}}</th>
                      </tr>
                    </thead> 
                  </table>   
                  <div id="">
                   <table class="table table-bordered table-condensed table-hover table-fixed-header1" style="margin-top:20px;" >                                          
                     <thead class='header' style=" z-index: 1;">                           
                      <tr>
                        <th class="id" ng-if="showOrgTime && showOrganization" custom-sort order="'date'" sort="sort" style="font-size:12px;background-color:#ecf7fb;white-space: nowrap;"><b><?php echo Lang::get('content.organization'); ?> <?php echo Lang::get('content.name'); ?></b></th>
                        <th class="id" custom-sort order="'date'" sort="sort" style="font-size:12px;background-color:#ecf7fb;white-space: nowrap;"><b><?php echo Lang::get('content.date'); ?></b></th>   
                        <th class="id" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.sensor'); ?></b></th>                                     
                        <th class="id" custom-sort order="'startFuel'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.start_fuel'); ?></b></th>
                        <th class="id" custom-sort order="'endFuel'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.end_fuel'); ?></b></th>

                        <th class="id" custom-sort order="'fuelFilling'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_filling'); ?></b></th>
                        <th class="id" ng-hide="vehiMode=='Dispenser'" custom-sort order="'fuelTheft'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_theft'); ?></b></th>
                        <th class="id"  custom-sort order="'fuelConsumption'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_consumption'); ?></b></th>
                        <th class="id" ng-show="vehiMode=='Dispenser'" custom-sort order="'fuelDispenser'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_dispensed'); ?></b></th>
                        <th ng-show="vehicleMode != 'DG'&&showDelta" class="id" custom-sort order="'fuelBalance'" sort="sort" style="font-size:12px;background-color:#ecf7fb;" ng-show=""><b><?php echo Lang::get('content.delta'); ?> <span style="white-space: nowrap;"><?php echo Lang::get('content.fill_sub_theft'); ?></span></b></th>
                        <th ng-hide="vehicleMode == 'DG'" class="id" custom-sort order="'startKms'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.start_kms'); ?></b></th>
                        <th ng-hide="vehicleMode == 'DG'" class="id" custom-sort order="'endKms'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.end_kms'); ?></b></th>
                        <th ng-hide="vehicleMode == 'DG'" class="id" custom-sort order="'dist'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.dis_travelled'); ?></b></th>
                        <th ng-hide="vehicleMode == 'Machinery' || vehicleMode == 'DG' " class="id" custom-sort order="'kmpl'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.kmpl'); ?></b></th>   
                        <th ng-hide="vehicleMode == 'DG'" style="font-size:12px;background-color:#ecf7fb;" ng-hide="vehicleMode == 'DG'"><b><?php echo Lang::get('content.running_hrs'); ?></b></th>
                        <th style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.engine_on'); ?> <?php echo Lang::get('content.hours'); ?></b></th> 
                        <th style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.secondary_engine_hrs'); ?></b></th>
                        <th  ng-hide="vehicleMode == 'DG'" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.engine_idle_hrs'); ?></b></th>
                        <th ng-show="vehicleMode == 'DG' || vehicleMode == 'Machinery'" class="id" custom-sort order="'ltrsPerHrs'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.ltrs_per_hrs'); ?></b></th>   
                        

                        <th style="font-size:12px;background-color:#ecf7fb;" ng-hide="vehicleMode == 'DG'"><b><?php echo Lang::get('content.start_loca'); ?></b></th>   
                        <th style="font-size:12px;background-color:#ecf7fb;" ng-hide="vehicleMode == 'DG'"><b><?php echo Lang::get('content.end_loca'); ?></b></th>
                        <th style="font-size:12px;background-color:#ecf7fb;" ng-hide="vehicleMode == 'DG'"><b><?php echo Lang::get('content.dri_name'); ?>
                        <th style="font-size:12px;background-color:#ecf7fb;" ng-hide="vehicleMode == 'DG'"><b><?php echo Lang::get('content.dri_mob'); ?></b></th>
                        <th style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.remarks'); ?></b></th>                              
                      </tr>
                    </thead>

                    <tbody>     
                      <tr ng-if="fuelConData[0].error==null && !errMsg.includes('expired')" ng-repeat-start="(key,data) in fuelConData | orderBy:natural(sort.sortingOrder):sort.reverse" ng-style="{ 'background-color': $index % 2 ? '#ffffff' : '#f9f9f9','color':data.position=='U'?'red':'black' }">
                        <td ng-if="showOrgTime && showOrganization" rowspan="{{(parseInts(data.sensor)>1 && !isTankBased)?parseInts(data.sensor)+1:data.sensor}}" >{{organName }}</td>
                        <td rowspan="{{(parseInts(data.sensor)>1 && !isTankBased)?parseInts(data.sensor)+1:data.sensor}}" style="white-space: nowrap;">{{data.date | date:'dd-MM-yyyy'}}</td>
                        <td style="font-weight: normal !important;" >1</td>
                        <td>{{ roundOffDecimal(data.fuelsensors[0].startFuel) }}</td>
                        <td>{{ roundOffDecimal(data.fuelsensors[0].endFuel) }}</td>

                        <td>{{ roundOffDecimal(data.fuelsensors[0].fuelFilling) }}</td>
                        <td ng-hide="vehiMode=='Dispenser'">{{ roundOffDecimal(data.fuelsensors[0].fuelTheft) }}</td>
                        <td >{{ (parseInts(data.sensor)>1) ? '-' : roundOffDecimal(data.fuelsensors[0].fuelConsumption  )}}</td>
                        <td ng-show="vehiMode=='Dispenser'">{{roundOffDecimal(data.fuelsensors[0].fuelDispenser)}}</td>          
                        <td ng-show="vehicleMode != 'DG' && showDelta"> - </td>
                        <td ng-hide="vehicleMode == 'DG'">{{parseInts(data.startKms)}}</td>
                        <td ng-hide="vehicleMode == 'DG'">{{parseInts(data.endKms)}}</td>
                        <td ng-hide="vehicleMode == 'DG'">{{data.dist}}</td>
                        <td ng-hide="vehicleMode == 'Machinery' || vehicleMode == 'DG'">{{(parseInts(data.sensor)>1 || data.fuelsensors[0].kmpl <= 0) ? '-' : roundOffDecimal(data.fuelsensors[0].kmpl) }}</td>
                        <td ng-hide="vehicleMode == 'DG'">{{msToTime2(data.engineRunningHrs)}}</td>
                        <td>{{msToTime2(data.ignitionHrs)}}</td>
                        <td>{{msToTime2(parseInts(data.secondaryEngineDuration))}}</td>

                        <td ng-hide="vehicleMode == 'DG'">{{msToTime2(data.engineIdleHrs)}}</td>
                        <td ng-show="vehicleMode =='DG' || vehicleMode == 'Machinery'">{{ (parseInts(data.sensor)>1)? '-' : roundOffDecimal(data.fuelsensors[0].ltrsPerHrs) }}</td>
     
                        <td ng-hide="vehicleMode == 'DG'"><a href="https://www.google.com/maps?q=loc:{{data.startLoc}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
                        <td ng-hide="vehicleMode == 'DG'"><a href="https://www.google.com/maps?q=loc:{{data.endLoc}}" target="_blank"><?php echo Lang::get('content.link'); ?></td>
                          <td ng-if="data.driverName==''">-</td>
                          <td ng-hide="vehicleMode == 'DG'" ng-if="data.driverName!=''">{{data.driverName}}</td>
                          <td ng-hide="vehicleMode == 'DG'" ng-if="data.driverMobileNo==''">-</td>
                          <td ng-hide="vehicleMode == 'DG'" ng-if="data.driverMobileNo!=''">{{data.driverMobileNo}}</td>
                          <td ng-hide="vehicleMode == 'DG'" ng-if="data.remarks!=''">{{data.remarks}}</td>
                          <td ng-hide="vehicleMode == 'DG'" ng-if="data.remarks==''">-</td>
                        </tr>
                        <tr ng-repeat-end ng-repeat="objFuel in data.fuelsensors" ng-hide="$first"  ng-style="{ 'background-color': $index % 2 ? '#ffffff' : '#f9f9f9','color':data.position=='U'?'red':'black' }">
                          <td ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}">{{($last && !isTankBased) ? 'Total' : objFuel.sensor  }}</td>
                          <td ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}">{{ roundOffDecimal(objFuel.startFuel) }}</td>
                          <td ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}">{{ roundOffDecimal(objFuel.endFuel) }}</td>

                          <td ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}">{{ roundOffDecimal(objFuel.fuelFilling) }}</td>
                          <td ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}" ng-hide="vehiMode=='Dispenser'">{{ roundOffDecimal(objFuel.fuelTheft) }}</td>
                          <td ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}" >{{ !$last || objFuel.fuelConsumption<=0? '-' : roundOffDecimal(objFuel.fuelConsumption) }}</td>
                          <td ng-show="vehiMode == 'Dispenser'" ng-style="{ 'font-weight' : ($last) ? 'bold' : 'normal'}">{{($last)? roundOffDecimal(objFuel.fuelDispenser) : '-' }}</td>
                          <td ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}" ng-show="vehicleMode != 'DG'&&showDelta">{{($last)? roundOffDecimal(objFuel.fuelBalance) : '-'  }}</td>
                          <td ng-hide="vehicleMode == 'DG'" ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}">{{($last)? parseInts(data.startKms) : '-' }}</td>
                          <td ng-hide="vehicleMode == 'DG'" ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}">{{ ($last)? parseInts(data.endKms) : '-' }}</td>
                          <td ng-hide="vehicleMode == 'DG'" ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}">{{ ($last)? data.dist : '-' }}</td>
                          <td ng-hide="vehicleMode == 'DG'" ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}"> {{(objFuel.kmpl<=0 && $last )?'-' : roundOffDecimal(objFuel.kmpl)}}</td>
                          <td ng-hide="vehicleMode == 'DG'" ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}">{{ ($last)? msToTime2(data.engineRunningHrs) : '-' }}</td>
                          <td ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}">{{($last)?msToTime2(data.ignitionHrs) : '-' }}</td>
                          <td ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}">{{($last)?msToTime2(parseInts(data.secondaryEngineDuration)): '-' }}</td>
                          <td ng-hide="vehicleMode=='DG'" ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}">{{ ($last)? msToTime2(data.engineIdleHrs) : '-' }}</td>
                          <td ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}">  {{ (!$last || objFuel.ltrsPerHrs<=0 )?'-' : roundOffDecimal(objFuel.ltrsPerHrs) }}</td>
                          

                          
                          <td ng-hide="vehicleMode == 'DG'" ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}">-</td>
                          <td ng-hide="vehicleMode == 'DG'" ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}">-</td>
                          <td ng-hide="vehicleMode == 'DG'" ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}">-</td>
                          <td ng-hide="vehicleMode == 'DG'" ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}">-</td>
                          <td  ng-style="{ 'font-weight' : ($last && !isTankBased) ? 'bold' : 'normal'}">-</td>
                        </tr>

                        <tr ng-if="(fuelConData.length>1) && (fuelConData[0].error==null)&&(!errMsg.includes('expired'))">                  

                          <td style="font-size: 13px;font-weight: bold;"><?php echo Lang::get('content.total'); ?></td>    
                          <td ng-if="showOrgTime && showOrganization">-</td>                                   
                          <td style="font-size: 13px;font-weight: bold;">-</td>
                          <td style="font-size: 13px;font-weight: bold;">-</td>
                          <td style="font-size: 13px;font-weight: bold;">-</td>

                          <td style="font-size: 13px;font-weight: bold;">{{roundOffDecimal(fuelFilling)}}</td>
                          <td style="font-size: 13px;font-weight: bold;" ng-hide="vehiMode=='Dispenser'">{{roundOffDecimal(fuelTheft)}}</td>
                          <td style="font-size: 13px;font-weight: bold;">{{roundOffDecimal(fuelConsumption)}}</td>
                          <td ng-show="vehiMode=='Dispenser'" style="font-size: 13px;font-weight: bold;">{{ roundOffDecimal(fuelDispenser) }}</td> 
                          <td ng-show="vehicleMode != 'DG'&&showDelta" style="font-size: 13px;font-weight: bold;"> {{ roundOffDecimal(fuelBalance) }} </td>
                          <td style="font-size: 13px;font-weight: bold;" ng-hide="vehicleMode == 'DG'">-</td>
                          <td style="font-size: 13px;font-weight: bold;" ng-hide="vehicleMode == 'DG'">-</td> 
                          <td style="font-size: 13px;font-weight: bold;" ng-hide="vehicleMode == 'DG'">{{parseInts(dist)}}</td>
                          <td style="font-size: 13px;font-weight: bold;" ng-hide="vehicleMode == 'DG' || vehicleMode == 'Machinery'">{{(totalKmpl(dist,fuelConsumption) <= 0)? "-" : totalKmpl(dist,fuelConsumption)}}</td>      
                          <td ng-hide="vehicleMode == 'DG'" ng-if="engineHrs != 'NaN'" style="font-size: 13px;font-weight: bold;">{{msToTime2(engineHrs)}}</td>
                          <td ng-hide="vehicleMode == 'DG'" ng-if="engineHrs == 'NaN'"  style="font-size: 13px;font-weight: bold;">-</td>

                          <td style="font-size: 13px;font-weight: bold;">{{msToTime2(ignitionHrs)}}</td>
                          <td style="font-size: 13px;font-weight: bold;">{{msToTime2(secIgnitionHrs)}}</td>
                          <td ng-hide="vehicleMode=='DG'" style="font-size: 13px;font-weight: bold;">{{(engineIdleHrs != 'NaN')?msToTime2(engineIdleHrs):'-'}}</td>
                          <td style="font-size: 13px;font-weight: bold;">{{totalLtph(fuelConsumption,ignitionHrs)}}</td>
                          
                          
                          <td ng-hide="vehicleMode == 'DG'">-</td>
                          <td ng-hide="vehicleMode == 'DG'">-</td>
                          <td ng-hide="vehicleMode == 'DG'">-</td>
                          <td ng-hide="vehicleMode == 'DG'">-</td>
                          <td>-</td>
                        </tr>

                        <tr ng-if="(fuelConData[0].error!=null)&&(!errMsg)" style="text-align: center">
                          <td colspan="{{ vehicleMode == 'DG'? 12 : 21}}" class="err" ng-if="!errMsg"><h5>{{fuelConData[0].error}}</h5></td>
                        </tr>
                        <tr ng-if="errMsg" style="text-align: center">
                          <td colspan="{{ vehicleMode == 'DG'? 12 : 21}}" class="err"><h5>{{errMsg}}</h5></td>
                        </tr>
                      </tbody>   
                    </table> 
                  </div>                   
                </div>
                <div class="box-body" id="VehicleWiseFuelReport" ng-hide='true'>
                 <p ng-if="showOrgTime" style="margin-left: 30px;margin-top: 5px;" class="pull-left"><span style="margin-left: 40px;"><b><?php echo Lang::get('content.date_time'); ?></b> : &nbsp;{{uiDate.fromdate}}&nbsp;{{getAMPM(fromtotime[0])}} - {{uiDate.todate}}&nbsp;{{getAMPM(fromtotime[1])}}</span> </p>
                 <p ng-if="!showOrgTime" style="margin-left: 30px;margin-top: 5px;" class="pull-left"><span style="margin-left: 40px;"><b><?php echo Lang::get('content.organization'); ?><?php echo Lang::get('content.name'); ?></b> : &nbsp;{{orgName}}</span><span style="margin-left: 40px;"><b><?php echo Lang::get('content.date_time'); ?></b> : &nbsp;{{uiDate.fromdate}}&nbsp;- {{uiDate.todate}}&nbsp;</span></p>
                 <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top:20px;" ng-if="(fuelConData[0].error==null)&&(!errMsg.includes('expired'))">
                 <thead><tr><th>{{"VehicleName: "+shortNam}}</th></tr></thead>
                  <thead ng-if="vehicleMode == 'DG'">
                       <tr style="text-align:center;">
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.group'); ?> <?php echo Lang::get('content.name'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{gName}}</th>
                        <th colspan="2" ng-hide="vehiMode=='Dispenser'" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_theft'); ?></th>
                        <th colspan="2" ng-hide="vehiMode=='Dispenser'" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{roundOffDecimal(fuelTheft)}}</th>                              
                      </tr>                        
                      <tr style="text-align:center;">
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.mode'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{vehicleMode?vehicleMode:'-'}}</th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.veh_model'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{vehicleModel?vehicleModel:'-'}}</th>                               
                      </tr>  
                      <tr style="text-align:center;">
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.engine_on'); ?> <?php echo Lang::get('content.hours'); ?>  </th>
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(ignitionHrs != 'NaN')?msToTime2(ignitionHrs):'-'}}</th>
                        <th colspan="2"  style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total <?php echo Lang::get('content.fuel_consumption'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;">{{roundOffDecimal(fuelConsumption)}}</th>
                      </tr>
                      <tr style="text-align:center;">                   
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.secondary_engine_hrs'); ?></th>
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(secIgnitionHrs != 'NaN')?msToTime2(secIgnitionHrs):'-'}}</th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.engine_on'); ?> <?php echo Lang::get('content.hours'); ?>  </th>
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(ignitionHrs != 'NaN')?msToTime2(ignitionHrs):'-'}}</th>                  
                      </tr>
                      <tr style="text-align:center;">
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total <?php echo Lang::get('content.fuel_fill'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;">{{roundOffDecimal(fuelFilling)}}</th>
                       
                        <th  colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"> <?php echo Lang::get('content.LTPH'); ?></th>
                        <th  colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{ totalLtph(fuelConsumption,ignitionHrs)}}</th> 
                      </tr>
                    </thead>
                     <thead ng-if="vehicleMode =='Machinery'">
                       <tr style="text-align:center;">
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.group'); ?> <?php echo Lang::get('content.name'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{gName}}</th>
                        <th colspan="2"  style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total <?php echo Lang::get('content.fuel_consumption'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;">{{roundOffDecimal(fuelConsumption)}}</th>                         
                      </tr>                        
                      <tr style="text-align:center;">
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.mode'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{vehicleMode?vehicleMode:'-'}}</th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.veh_model'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{vehicleModel?vehicleModel:'-'}}</th>                               
                      </tr>  
                      <tr style="text-align:center;">                  
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.engine_on'); ?> <?php echo Lang::get('content.hours'); ?>  </th>
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(ignitionHrs != 'NaN')?msToTime2(ignitionHrs):'-'}}</th>
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.total_distance'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{ parseInts(dist)}}</th>                    
                      </tr>
                      <tr style="text-align:center;">
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.engine_on'); ?> <?php echo Lang::get('content.hours'); ?>  </th>
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(ignitionHrs != 'NaN')?msToTime2(ignitionHrs):'-'}}</th>
                        <th  colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total_engine_idle_hrs'); ?></th>
                        <th  colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(engineIdleHrs != 'NaN')?msToTime2(engineIdleHrs):'-'}}</th>
                      </tr>
                      <tr>
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.veh_name'); ?></th>
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{shortNam}}</th>  
                      <th colspan="2" ng-hide="vehiMode=='Dispenser'" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_theft'); ?></th>
                        <th colspan="2" ng-hide="vehiMode=='Dispenser'" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{roundOffDecimal(fuelTheft)}}</th>                             
                      </tr>
                      <tr style="text-align:center;">
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total <?php echo Lang::get('content.fuel_fill'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;">{{roundOffDecimal(fuelFilling)}}</th>
                        
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"> <?php echo Lang::get('content.LTPH'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{ totalLtph(fuelConsumption,ignitionHrs)}}</th> 
                      </tr>
                    </thead>
                    <thead ng-if="vehicleMode !='DG' && vehicleMode !='Machinery'">
                       <tr style="text-align:center;">
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.group'); ?> <?php echo Lang::get('content.name'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{gName}}</th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.veh_name'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{shortNam}}</th>                               
                      </tr>                        
                      <tr style="text-align:center;">
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.mode'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{vehicleMode?vehicleMode:'-'}}</th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.veh_model'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{vehicleModel?vehicleModel:'-'}}</th>                               
                      </tr>  
                      <tr style="text-align:center;">
                        <th colspan="2" ng-if="vehicleMode!='Dispenser'" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_theft'); ?></th>
                        <th colspan="2" ng-if="vehicleMode!='Dispenser'" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{roundOffDecimal(fuelTheft)}}</th>
                        <th colspan="2" ng-if="vehicleMode=='Dispenser'" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_dispensed'); ?></th>
                        <th colspan="2" ng-if="vehicleMode=='Dispenser'" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{roundOffDecimal(fuelDispenser)}}</th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total <?php echo Lang::get('content.fuel_consumption'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;">{{roundOffDecimal(fuelConsumption)}}</th>
                      </tr>
                      <tr style="text-align:center;">                   
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.engine_on'); ?> <?php echo Lang::get('content.hours'); ?>  </th>
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(ignitionHrs != 'NaN')?msToTime2(ignitionHrs):'-'}}</th>
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.total_distance'); ?></th>
                        <th style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{ parseInts(dist)}}</th>                    
                      </tr>
                      <tr style="text-align:center;">
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.engine_on'); ?> <?php echo Lang::get('content.hours'); ?>  </th>
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(ignitionHrs != 'NaN')?msToTime2(ignitionHrs):'-'}}</th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total_engine_idle_hrs'); ?></th>
                        <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(engineIdleHrs != 'NaN')?msToTime2(engineIdleHrs):'-'}}</th>
                      </tr>
                      <tr style="text-align:center;">
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total <?php echo Lang::get('content.fuel_fill'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;">{{roundOffDecimal(fuelFilling)}}</th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.KMPL'); ?></th>
                        <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{totalKmpl(dist,fuelConsumption)}}</th>
                      </tr>
                    </thead> 
                   <!--<tr style="text-align:center;">
                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php //echo Lang::get('content.group'); ?> <?php //echo Lang::get('content.name'); ?></th>
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{gName}}</th>
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php //echo Lang::get('content.veh_name'); ?></th>
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{shortNam}}</th>                               
                  </tr> 
                  <tr style="text-align:center;">
                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php //echo Lang::get('content.vehicle'); ?> <?php //echo Lang::get('content.mode'); ?></th>
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{vehicleMode?vehicleMode:'-'}}</th>
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php// echo Lang::get('content.veh_model'); ?></th>
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{vehicleModel?vehicleModel:'-'}}</th>                               
                  </tr>                       

                  <tr style="text-align:center;">
                    <th colspan="2" ng-if="vehiMode!='Dispenser'" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php //echo Lang::get('content.total'); ?> <?php //echo Lang::get('content.fuel_theft'); ?></th>
                    <th colspan="2" ng-if="vehiMode!='Dispenser'" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{roundOffDecimal(fuelTheft)}}</th>

                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php //echo Lang::get('content.total'); ?> <?php //echo Lang::get('content.LTPH'); ?></th>
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{(vehicleMode == "DG" || vehicleMode== "Machinery")? totalLtph(fuelConsumption,ignitionHrs) : "-"}}</th> 
                  </tr>
                  <tr style="text-align:center;">                   
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total <?php //echo Lang::get('content.fuel_fill'); ?></th>
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;">{{roundOffDecimal(fuelFilling)}}</th>
                    <th colspan="2" ng-if="vehiMode=='Dispenser'" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php// echo Lang::get('content.total'); ?> <?php //echo Lang::get('content.fuel_dispensed'); ?></th>
                    <th colspan="2" ng-if="vehiMode=='Dispenser'" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{roundOffDecimal(fuelDispenser)}}</th>
                    <th colspan="2"  style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">Total <?php //echo Lang::get('content.fuel_consumption'); ?></th>
                    <th colspan="2"  style="font-weight:unset;font-size:12px;background-color:#f9f9f9;">{{roundOffDecimal(fuelConsumption)}}</th>
                  </tr>
                  <tr style="text-align:center;">
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php //echo Lang::get('content.total_engine_hrs'); ?></th>
                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(ignitionHrs != 'NaN')?msToTime2(ignitionHrs):'-'}}</th>
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">
                      <span ng-if="vehicleMode!='DG'">
                        <?php //echo Lang::get('content.total_engine_idle_hrs'); ?>
                      </span>
                      <span ng-if="vehicleMode=='DG'">
                        <?php //echo Lang::get('content.secondary_engine_hrs'); ?>
                      </span>
                    </th>
                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">   
                      <span ng-if="vehicleMode!='DG'">
                        {{(engineIdleHrs != 'NaN')?msToTime2(engineIdleHrs):'-'}} 
                      </span>
                      <span ng-if="vehicleMode=='DG'">
                        {{(secIgnitionHrs != 'NaN')?msToTime2(secIgnitionHrs):'-'}}
                      </span>
                    </th>
                  </tr>
                  <tr style="text-align:center;">
                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php //echo Lang::get('content.total_distance'); ?></th>
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{(vehicleMode =="DG")? "-" : parseInts(dist)}}</th>
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php //echo Lang::get('content.total'); ?> <?php //echo Lang::get('content.KMPL'); ?></th>
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{ (vehicleMode =="Machinery")? "-" : totalKmpl(dist,fuelConsumption)}}</th>
                  </tr>-->
              </table>   
              <div id="">
               <table class="table table-bordered table-condensed table-hover" style="margin-top:20px;" id='table2'>                                          
                 <thead class='header' style=" z-index: 1;">                           
                  <tr>
                    <th ng-if="showOrgTime && showOrganization" class="id" custom-sort order="'organName'" sort="sort" style="font-size:12px;background-color:#ecf7fb;white-space: nowrap;"><b><?php echo Lang::get('content.organization'); ?><?php echo Lang::get('content.name'); ?></b></th> 
                    <th class="id" custom-sort order="'date'" sort="sort" style="font-size:12px;background-color:#ecf7fb;white-space: nowrap;"><b><?php echo Lang::get('content.date'); ?></b></th>   
                    <th class="id" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.sensor'); ?></b></th>                                     
                    <th class="id" custom-sort order="'startFuel'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.start_fuel'); ?></b></th>
                    <th class="id" custom-sort order="'endFuel'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.end_fuel'); ?></b></th>

                    <th class="id" custom-sort order="'fuelFilling'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_filling'); ?></b></th>
                    <th class="id" ng-if="vehiMode!='Dispenser'" custom-sort order="'fuelTheft'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_theft'); ?></b></th>
                    <th class="id"  custom-sort order="'fuelConsumption'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_consumption'); ?></b></th>
                    <th class="id" ng-if="vehiMode=='Dispenser'"  custom-sort order="'fuelDispenser'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.fuel_dispensed'); ?></b></th>
                    <th ng-if="vehicleMode!='DG' && showDelta" class="id" custom-sort order="'fuelBalance'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.delta'); ?> <span style="white-space: nowrap;"><?php echo Lang::get('content.fill_sub_theft'); ?></span></b></th>
                    <th ng-if="vehicleMode!='DG'" class="id" custom-sort order="'startKms'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.start_kms'); ?></b></th>
                    <th ng-if="vehicleMode!='DG'" class="id" custom-sort order="'endKms'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.end_kms'); ?></b></th>
                    <th ng-if="vehicleMode!='DG'" class="id" custom-sort order="'dist'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.dis_travelled'); ?></b></th>
                    <th ng-if="vehicleMode !='Machinery' && vehicleMode != 'DG'" class="id" custom-sort order="'kmpl'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.kmpl'); ?></b></th>   
                    <th style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.engine_on'); ?> <?php echo Lang::get('content.hours'); ?></b></th> 

                    <th  ng-if="vehicleMode != 'DG'" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.engine_idle_hrs'); ?></b></th>
                    <th ng-if="vehicleMode == 'DG' || vehicleMode == 'Machinery'" class="id" custom-sort order="'ltrsPerHrs'" sort="sort" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.ltrs_per_hrs'); ?></b></th>   
                    <th ng-if="vehicleMode!='DG'" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.running_hrs'); ?></b></th>
                    <th style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.secondary_engine_hrs'); ?></b></th> 
                    <th ng-if="vehicleMode!='DG'" style="font-size:12px;background-color:#ecf7fb;" class="addr"><b> Start Address</b></th>
                    <th ng-if="vehicleMode!='DG'" style="font-size:12px;background-color:#ecf7fb;" class="addr"><b> End Address</b></th>
                    <th ng-if="vehicleMode!='DG'" style="font-size:12px;background-color:#ecf7fb;"><b>Driver Name</b></th>
                    <th ng-if="vehicleMode!='DG'" style="font-size:12px;background-color:#ecf7fb;"><b>Driver Mobile Number</b></th>
                    <th style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.remarks'); ?></b></th>                              
                  </tr>
                </thead>

                <tbody>     
                  <tr ng-if="fuelConData[0].error==null" ng-repeat-start="(key,data) in fuelConData | orderBy:natural(sort.sortingOrder):sort.reverse" ng-style="{ 'color':data.position=='U'?'red':'black' }">
                    <td ng-if="showOrgTime && showOrganization" style="white-space: nowrap;"  >{{organName }}</td>
                    <td style="white-space: nowrap;">{{data.date}}</td>
                    <td style="font-weight: normal !important;" >1</td>
                    <td>{{ roundOffDecimal(data.fuelsensors[0].startFuel) }}</td>
                    <td>{{ roundOffDecimal(data.fuelsensors[0].endFuel) }}</td>

                    <td>{{ roundOffDecimal(data.fuelsensors[0].fuelFilling) }}</td>
                    <td ng-if="vehiMode!='Dispenser'">{{ roundOffDecimal(data.fuelsensors[0].fuelTheft) }}</td>
                    <td>{{ (parseInts(data.sensor)>1) ? '-' : roundOffDecimal(data.fuelsensors[0].fuelConsumption  )}}</td>
                    <td ng-if="vehiMode=='Dispenser'">{{ roundOffDecimal(data.fuelsensors[0].fuelDispenser)}}</td>
                    <td ng-if="vehicleMode != 'DG' && showDelta"> - </td>
                    <td ng-if="vehicleMode!='DG'">{{parseInts(data.startKms)}}</td>
                    <td ng-if="vehicleMode!='DG'">{{parseInts(data.endKms)}}</td>
                    <td ng-if="vehicleMode!='DG'">{{data.dist}}</td>
                    <td ng-if="vehicleMode!='Machinery' && vehicleMode != 'DG'">{{(data.fuelsensors[0].kmpl <= 0)? "-" : roundOffDecimal(data.fuelsensors[0].kmpl) }}</td>
                    <td>{{msToTime2(data.ignitionHrs)}}</td>

                    <td ng-if="vehicleMode!='DG'">{{msToTime2(data.engineIdleHrs)}}</td>
                    <td ng-if="vehicleMode == 'DG' || vehicleMode == 'Machinery'">{{ roundOffDecimal(data.fuelsensors[0].ltrsPerHrs) }}</td>
                    <td ng-if="vehicleMode!='DG'">{{msToTime2(data.engineRunningHrs)}}</td>
                    <td>{{msToTime2(parseInts(data.secondaryEngineDuration))}}</td>
                    <td ng-if="vehicleMode!='DG'" class="addr">{{data.startAddress}}</td>
                    <td ng-if="vehicleMode!='DG'" class="addr">{{data.endAddress}}</td>
                    <td ng-if="data.driverName=='' && vehicleMode!='DG'">-</td>
                    <td ng-if="data.driverName!='' && vehicleMode!='DG'">{{data.driverName}}</td>
                    <td ng-if="data.driverMobileNo=='' && vehicleMode!='DG'">-</td>
                    <td ng-if="data.driverMobileNo!='' && vehicleMode!='DG'">{{data.driverMobileNo}}</td>
                    <td ng-if="data.remarks==''">-</td>
                    <td ng-if="data.remarks!=''">{{data.remarks}}</td>
                  </tr>
                  <tr ng-repeat-end ng-repeat="objFuel in data.fuelsensors.slice(1,(parseInts(data.sensor)>1)?data.fuelsensors.length:data.fuelsensors.length-1)" ng-style="{ 'color':data.position=='U'?'red':'black' }">
                    <td></td>
                    <td>{{($last && !isTankBased) ? 'Total' : objFuel.sensor  }}</td>
                    <td>{{ roundOffDecimal(objFuel.startFuel) }}</td>
                    <td>{{ roundOffDecimal(objFuel.endFuel) }}</td>

                    <td>{{ roundOffDecimal(objFuel.fuelFilling) }}</td>
                    <td ng-if="vehiMode!='Dispenser'">{{ roundOffDecimal(objFuel.fuelTheft) }}</td>
                    <td ng-if="vehiMode=='Dispenser'">{{ roundOffDecimal(objFuel.fuelDispenser) }}</td>
                    <td>{{ $last? roundOffDecimal(objFuel.fuelConsumption) : '-' }}</td>
                    <td ng-if="vehicleMode!='DG' && showDelta">{{($last)? roundOffDecimal(objFuel.fuelBalance) : '-'  }}</td>
                    <td ng-if="vehicleMode!='DG'">{{($last)? parseInts(data.startKms) : '-' }}</td>
                    <td ng-if="vehicleMode!='DG'">{{ ($last)? parseInts(data.endKms) : '-' }}</td>
                    <td ng-if="vehicleMode!='DG'">{{ ($last)? data.dist : '-' }}</td>
                    <td ng-if="vehicleMode!='DG'">{{ (objFuel.kmpl<=0 && $last )?'-' : roundOffDecimal(objFuel.kmpl) }} </td>
                    <td>{{($last)?msToTime2(data.ignitionHrs) : '-' }}</td>

                    <td ng-if="vehicleMode!='DG'">{{ ($last)? msToTime2(data.engineIdleHrs) : '-' }}</td>
                    <td>{{ (objFuel.ltrsPerHrs<=0 && $last )?'-' : roundOffDecimal(objFuel.ltrsPerHrs) }}</td>
                    <td ng-if="vehicleMode!='DG'">{{ ($last)? msToTime2(data.engineRunningHrs) : '-' }}</td>
                    <td>{{($last)?msToTime2(parseInts(data.secondaryEngineDuration)): '-' }}</td>
                    <td ng-if="vehicleMode!='DG'">-</td>
                    <td ng-if="vehicleMode!='DG'">-</td>
                    <td ng-if="vehicleMode!='DG'">-</td>
                    <td ng-if="vehicleMode!='DG'">-</td>
                    <td>-</td>
                  </tr>

                  <tr ng-if="(fuelConData.length>1) && (fuelConData[0].error==null)&&(!errMsg.includes('expired'))">                  

                    <td style="font-size: 13px;font-weight: bold;"><?php echo Lang::get('content.total'); ?></td>    
                    <td  ng-if="showOrgTime && showOrganization">-</td>                                   
                    <td style="font-size: 13px;font-weight: bold;">-</td>
                    <td style="font-size: 13px;font-weight: bold;">-</td>
                    <td style="font-size: 13px;font-weight: bold;">-</td>

                    <td style="font-size: 13px;font-weight: bold;">{{fuelFilling}}</td>
                    <td style="font-size: 13px;font-weight: bold;" ng-if="vehiMode!='Dispenser'">{{fuelTheft}}</td>
                    <td style="font-size: 13px;font-weight: bold;">{{roundOffDecimal(fuelConsumption)}}</td>
                    <td style="font-size: 13px;font-weight: bold;" ng-if="vehiMode=='Dispenser'">{{roundOffDecimal(fuelDispenser)}}</td>
                    <td ng-if="vehicleMode!='DG' && showDelta" style="font-size: 13px;font-weight: bold;"> {{ roundOffDecimal(fuelBalance) }} </td>
                    <td ng-if="vehicleMode!='DG'" style="font-size: 13px;font-weight: bold;">-</td>
                    <td ng-if="vehicleMode!='DG'" style="font-size: 13px;font-weight: bold;">-</td> 
                    <td ng-if="vehicleMode!='DG'" style="font-size: 13px;font-weight: bold;">{{parseInts(dist)}}</td>
                    <td ng-if="vehicleMode!='DG'" style="font-size: 13px;font-weight: bold;">{{(totalKmpl(dist,fuelConsumption) <= 0)? "-" :totalKmpl(dist,fuelConsumption)}}</td>      
                    <td style="font-size: 13px;font-weight: bold;">{{msToTime2(ignitionHrs)}}</td>

                    <td ng-if="vehicleMode!='DG'" style="font-size: 13px;font-weight: bold;">{{(engineIdleHrs != 'NaN')?msToTime2(engineIdleHrs):'-'}}</td>
                    <td style="font-size: 13px;font-weight: bold;">{{totalLtph(fuelConsumption,ignitionHrs)}}</td>
                    <td  ng-if="engineHrs != 'NaN' && vehicleMode!='DG'" style="font-size: 13px;font-weight: bold;">{{msToTime2(engineHrs)}}</td>
                    <td ng-if="engineHrs == 'NaN' && vehicleMode!='DG'" style="font-size: 13px;font-weight: bold;">-</td>
                    <td style="font-size: 13px;font-weight: bold;">{{msToTime2(secIgnitionHrs)}}</td>
                    <td ng-if="vehicleMode!='DG'">-</td>
                    <td ng-if="vehicleMode!='DG'">-</td>
                    <td ng-if="vehicleMode!='DG'">-</td>
                    <td ng-if="vehicleMode!='DG'">-</td>
                    <td>-</td>
                  </tr>
                  
                  <tr ng-if="(fuelConData[0].error!=null)&&(!errMsg)" style="text-align: center">
                    <td colspan="{{ vehicleMode == 'DG'? 12 : 21}}" class="err" ng-if="!errMsg"><h5>{{fuelConData[0].error}}</h5></td>
                  </tr>
                  <tr ng-if="errMsg" style="text-align: center">
                    <td colspan="{{ vehicleMode == 'DG'? 12 : 21}}" class="err"><h5>{{errMsg}}</h5></td>
                  </tr>
                </tbody>    
              </table> 
            </div>                   
          </div>


        </div>
      </div>



    </div>
  </div>

  <script src="assets/js/static.js"></script>
  <script src="assets/js/jquery-1.11.0.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script data-require="angular-ui-bootstrap@0.11.0" data-semver="0.11.0" src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>
  <script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="assets/js/angular-translate.js"></script>
  <!--<script src="assets/js/highcharts.js"></script> -->
  <script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>
  <script src="https://code.highcharts.com/modules/export-data.js"></script>
  <script src="../resources/views/reports/customjs/moment.js"></script>
  <script src="../resources/views/reports/customjs/FileSaver.js"></script>
  <script src="../resources/views/reports/customjs/html5csv.js"></script>
  <script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
  <!-- <script src="../resources/views/reports/datatable/jquery.dataTables.js"></script> -->
  <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
  <!--<script src="assets/js/naturalSortVersionDates.js"></script> -->
  <script src="assets/js/vamoApp.js"></script>
  <script src="assets/js/services.js"></script>
  <script src="assets/js/fuelConsolidate2.js?v=<?php echo Config::get('app.version');?>"></script>

  <script>


    // $("#example1").dataTable();

    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
    
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });


  </script>

</body>
</html>
