<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="Satheesh">
	<title><?php echo Lang::get('content.gps'); ?></title>
	<link rel="shortcut icon" href="assets/imgs/tab.ico">
	<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
	<link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
	<link href="../resources/views/reports/datepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css">
	<link href="assets/css/bootstrap.css" rel="stylesheet">
	<link href="assets/css/jVanilla.css" rel="stylesheet">
	<link href="assets/css/simple-sidebar.css" rel="stylesheet">
	<link href="../resources/views/assets/css/datebutton.css" rel="stylesheet" type="text/css" />
	<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
	<link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
<!-- <link rel="stylesheet" type="text/css" href="http://www.highcharts.com/joomla/media/com_demo/highslide.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css"> -->
	<!--  <link rel="stylesheet" href="/resources/demos/style.css">  -->

	<!-- popup code -->
	<style type="text/css">
		body {
			font-family: 'Lato', sans-serif;
	/*font-weight: bold;
	font-family: 'Lato', sans-serif;
	font-family: 'Roboto', sans-serif;
	font-family: 'Open Sans', sans-serif;
	font-family: 'Raleway', sans-serif;
	font-family: 'Faustina', serif;
	font-family: 'PT Sans', sans-serif;
	font-family: 'Ubuntu', sans-serif;
	font-family: 'Droid Sans', sans-serif;
	font-family: 'Source Sans Pro', sans-serif;
	*/
}
.ng-modal-overlay {
	/* A dark translucent div that covers the whole screen */
	position:absolute;
	z-index:9999;
	top:0;
	left:0;
	width:100%;
	height:100%;
	background-color:grey;
	opacity: 0.8;
}
.ng-modal-dialog {
	/* A centered div above the overlay with a box shadow. */
	z-index:10000;
	position: absolute;
	width: 50%; /* Default */

	/* Center the dialog */
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
	-webkit-transform: translate(-50%, -50%);
	-moz-transform: translate(-50%, -50%);

	background-color: #fff;
	box-shadow: 4px 4px 80px #000; -->
}
.ng-modal-dialog-content {
	padding:10px;
	text-align: left;
}
.ng-modal-close {
	position: absolute;
	top: 3px;
	right: 5px;
	padding: 5px;
	cursor: pointer;
	font-size: 120%;
	display: inline-block;
	font-weight: bold;
	font-family: 'arial', 'sans-serif';
}
.modal-body {
	max-height: calc(100vh - 210px);
	overflow-y: auto;
}

</style>
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'G-2X3F316479');
</script>
</head>
<div id="preloader" >
	<div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
	<div id="status02">&nbsp;</div>
</div>
<body ng-app="mapApp">
	<div id="wrapper" ng-controller="mainCtrl" class="ng-cloak">
		<?php include('sidebarList.php');?>

		<div id="page-content-wrapper">
			<div class="container-fluid">
				<div class="panel panel-default">
				</div>   		
			</div>
		</div>
		
		<!-- loading menu -->
		<div id="testLoad"></div>

		<div class="col-md-12">
			<div class="box box-primary" ng-show="monthly">
				<div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
					<h3 class="box-title"><span><?php echo Lang::get('content.monthly_performance'); ?></span>
						<span><?php include('OutOfOrderDataInfo.php');?></span> 
					</h3>
				</div>

				<div class="row">
					<div class="col-md-1" align="center"></div>
					<div class="col-md-2" align="center">
						<div class="form-group">
							<div class="input-group datecomp">
								<input type="text" ng-model="fromdate" class="form-control placholdercolor" id="monthFrom" placeholder="<?php echo Lang::get('content.month'); ?>">
								<!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
							</div>
						</div>
					</div>
					<div class="col-md-1" align="center"></div>
					<div class="col-md-2" align="center">
						<button ng-click = "submitButton()" style="margin-left: -100%; padding : 5px"><?php echo Lang::get('content.submit'); ?></button>
					</div>
				</div>

			</div>

			<div class="box box-primary" ng-show="daily">
				<div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
					<h3 class="box-title"><span><?php echo Lang::get('content.daily_performance'); ?></span>
						<span><?php include('OutOfOrderDataInfo.php');?></span> 
					</h3>
				</div>
				<div class="row">
					<div class="col-md-1" align="center"></div>
					<div class="col-md-2" align="center"></div>
					<div class="col-md-2" align="center">
						<div class="form-group">
							<div class="input-group datecomp">
								<input type="text" class="form-control placholdercolor" id="dtFrom" ng-model="fromdateDaily" placeholder="<?php echo Lang::get('content.fromdate'); ?>">
								
							</div>

						</div>
					</div>
					<div class="col-md-2" align="center">
						<div class="form-group">
							<div class="input-group datecomp">
								<input type="text" class="form-control placholdercolor" id="dtTo" ng-model="todateDaily" placeholder="<?php echo Lang::get('content.fromdate'); ?>">
								
							</div>
						</div>
					</div>
                <!--  <div class="col-md-2" align="center">
                    	<div class="input-group datecomp">
                            <input type="text" class="form-control placholdercolor" id="timeFrom" ng-model="fromTime"  placeholder="From time">
                            <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                    <!--	</div> 
                    </div> -->
               <!--     <div class="col-md-2" align="center">
                    	<div class="form-group">
							<div class="input-group datecomp">
								<input type="text" class="form-control placholdercolor" id="datet" ng-model="todateDaily" placeholder="From date">
								<!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
						<!--	</div>
                      	</div>
                      </div>-->
             <!--      <div class="col-md-2" align="center">
                    	<div class="input-group datecomp">
                            <input type="text" class="form-control placholdercolor" id="timeTo" ng-model="totime" placeholder="From time">
                            <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                    <!-- </div> 
                    </div> -->
                    <div class="col-md-1" align="center"></div>
                    <div class="col-md-2" align="center">
                    	<button style="margin-left: -100%; padding : 5px" ng-click="submitButton()"><?php echo Lang::get('content.submit'); ?></button>
                    </div>
                    <div class="col-md-12"  align="right" style="margin-bottom: 10px;margin-left: 25%">

                    	<button type="button" class="btnDate" ng-click="durationFilter('yesterday')" ng-disabled="yesterdayDisabled" onclick="setToTime('yesterday')"><?php echo Lang::get('content.yesterday'); ?></button>
                    	<button type="button" class="btnDate" ng-click="durationFilter('thisweek')" ng-disabled="thisweekDisabled" onclick="setToTime('thisweek')"><?php echo Lang::get('content.thisweek'); ?></button>
                    	<button type="button" class="btnDate" ng-click="durationFilter('lastweek')" ng-disabled="weekDisabled" onclick="setToTime('lastweek')"><?php echo Lang::get('content.lastweek'); ?></button>                            
                    </div>
                </div>

            </div>

            <!--Group chart-->
            <div>
				<!-- <div id="container1"></div>
			</div>
		
			<div ng-hide="group"> -->
				<div id="container"></div>
			</div>


			<hr>
			<div class="box-body" ng-class="overallEnable?'col-md-9':'col-md-12'" id="statusreport">
				
				<div class="pull-right" style="margin-top: 10px;margin-right: 5px;">
					<img style="cursor: pointer;" ng-click="exportData('DailyPerformance')"  src="../resources/views/reports/image/xls.png" />
					<img style="cursor: pointer;" ng-click="exportDataCSV('DailyPerformance')"  src="../resources/views/reports/image/csv.jpeg" />
					<img style="cursor: pointer;" onclick="generatePDF()"  src="../resources/views/reports/image/Adobe.png" />
				</div>

				<div style="margin:0" class="page-header">
					<div><?php echo Lang::get('content.performance'); ?></div>
					<div ng-show="daily" style="font-size: medium;font-weight: normal;"><?php echo Lang::get('content.from'); ?> : {{fromdateDaily | date:'dd-MM-yyyy'}}  - <?php echo Lang::get('content.to'); ?> : {{todateDaily | date:'dd-MM-yyyy'}} </div>
				</div>
				<div id="DailyPerformance" ng-show="false">
					<table id="example" class="table table-striped table-bordered">

						<thead class='header' style=" z-index: 1;">  
							<tr>
								<th><?php echo Lang::get('content.from'); ?> : {{fromdateDaily | date:'dd-MM-yyyy'}}  - <?php echo Lang::get('content.to'); ?> : {{todateDaily | date:'dd-MM-yyyy'}}</th>
							</tr>     
							<tr style="text-align:center">
								<th width="10%" class="id" custom-sort order="'month'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;">{{vehiLabel | translate}} <?php echo Lang::get('content.name'); ?> / <?php echo Lang::get('content.months'); ?></th>
								<th width="5%" class="id" custom-sort order="'data.weightedBreakAnalysis'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.brake_analysis'); ?></th>
								<th width="5%" class="id" custom-sort order="'data.weightedSpeedAnalysis'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.speed_analysis'); ?></th>
								<th width="5%" class="id" custom-sort order="'data.weightedShockAlarmAnalysis'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.shock_analysis'); ?></th>
								<th width="5%" class="id" custom-sort order="'data.weightedAccelAnalysis'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.acceleration_analysis'); ?></th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="user in tableValue | orderBy:sort.sortingOrder:sort.reverse" class="active" style="text-align:center" ng-hide="nullValue(user)">
								<td>{{user.month}}</td>
								<td><a ng-if="user.data.weightedBreakAnalysis != 0" data-target="#myModal1" data-toggle="modal" ng-click="breakWeight(user, 'break')">{{user.data.weightedBreakAnalysis}}</a>
									<span ng-if="!user.data.weightedBreakAnalysis">0</span>
								</td>
								<td><a ng-if="user.data.weightedSpeedAnalysis != 0" data-target="#myModal" data-toggle="modal" ng-click="toggleSpeed(user, 'speed')">{{user.data.weightedSpeedAnalysis}}</a>
									<span ng-if="!user.data.weightedSpeedAnalysis">0</span>
								</td>
								<td><a ng-if="user.data.weightedShockAlarmAnalysis != 0" data-target="#myModal" data-toggle="modal" ng-click="toggleSpeed(user, 'shock')">{{user.data.weightedShockAlarmAnalysis}}</a>
									<span ng-if="!user.data.weightedShockAlarmAnalysis">0</span>
								</td>
								<td><a ng-if="user.data.weightedAccelAnalysis != 0" data-target="#myModal1" data-toggle="modal" ng-click="breakWeight(user, 'accleration')">{{user.data.weightedAccelAnalysis}}</a>
									<span ng-if="!user.data.weightedAccelAnalysis">0</span>
								</td>
							</tr>
						</tbody>
						<tr ng-if="tableValue.length==0" align="center">
							<td colspan="5" class="err"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
						</tr>
					</table>
					
				</div>

				<div id="formConfirmation">

					<table id="example" class="table table-striped table-bordered table-fixed-header">
						<thead class='header' style=" z-index: 1;">       
							<tr style="text-align:center">
								<th width="10%" class="id" custom-sort order="'month'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;">{{vehiLabel | translate}} <?php echo Lang::get('content.name'); ?> / <?php echo Lang::get('content.months'); ?></th>
								<th width="5%" class="id" custom-sort order="'data.weightedBreakAnalysis'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.brake_analysis'); ?></th>
								<th width="5%" class="id" custom-sort order="'data.weightedSpeedAnalysis'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.speed_analysis'); ?></th>
								<th width="5%" class="id" custom-sort order="'data.weightedShockAlarmAnalysis'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.shock_analysis'); ?></th>
								<th width="5%" class="id" custom-sort order="'data.weightedAccelAnalysis'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-size:12px;"><?php echo Lang::get('content.acceleration_analysis'); ?></th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="user in tableValue | orderBy:sort.sortingOrder:sort.reverse" class="active" style="text-align:center" ng-hide="nullValue(user)">
								<td ng-if="user.data.weightedBreakAnalysis || user.data.weightedSpeedAnalysis || 
								user.data.weightedShockAlarmAnalysis || user.data.weightedAccelAnalysis"><a href="#" data-target="#myModal1" data-toggle="modal" ng-click="showAll(user)">{{user.month}}</a></td>
								<td ng-if="!user.data.weightedBreakAnalysis && !user.data.weightedSpeedAnalysis && 
								!user.data.weightedShockAlarmAnalysis && !user.data.weightedAccelAnalysis">{{user.month}}</td>
								<td><span ng-if="user.data.weightedBreakAnalysis != 0">{{user.data.weightedBreakAnalysis}}</span>
									<span ng-if="!user.data.weightedBreakAnalysis">0</span>
								</td>
								<td><span ng-if="user.data.weightedSpeedAnalysis != 0">{{user.data.weightedSpeedAnalysis}}</span>
									<span ng-if="!user.data.weightedSpeedAnalysis">0</span>
								</td>
								<td><span ng-if="user.data.weightedShockAlarmAnalysis != 0">{{user.data.weightedShockAlarmAnalysis}}</span>
									<span ng-if="!user.data.weightedShockAlarmAnalysis">0</span>
								</td>
								<td><span ng-if="user.data.weightedAccelAnalysis != 0">{{user.data.weightedAccelAnalysis}}</span>
									<span ng-if="!user.data.weightedAccelAnalysis">0</span>
								</td>
							</tr>
						</tbody>
						<tr ng-if="tableValue.length==0" align="center">
							<td colspan="5" class="err"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
						</tr>
					</table>
				</div>
				
				<!-- <button type="button" class="btn btn-info btn-lg" data-target="#myModal" data-toggle="modal">Open Modal</button> -->
				<!-- <div class="modal fade" id="myModal" role="dialog">
					<div class="modal-dialog">

						<div class="modal-content">
							<div class="modal-body">
								<div ng-show="daily" class="pull-right" style="margin-right: 5px;margin-bottom: 5px;">
									<img style="cursor: pointer;" ng-click="exportData(removeSpaces(titleName))"  src="../resources/views/reports/image/xls.png" />
									<img style="cursor: pointer;" ng-click="exportDataCSV(removeSpaces(titleName))"  src="../resources/views/reports/image/csv.jpeg" />
									<img style="cursor: pointer;" onclick="generateSpeedShockPdf()"  src="../resources/views/reports/image/Adobe.png"  />
								</div>
								<div id="{{analysisName}}">
									<table class="table table-striped table-bordered table-condensed table-hover" id="speed-shock">
										<tr><th style="text-align:center;" colspan="4">Vehicle Name : {{id}}</th></tr>
										<tr style="text-align:center">
											<th style="text-align:center;" colspan="4"><?php echo Lang::get('content.speed_analysis'); ?></th>
										</tr>
										<tr style="text-align:center">
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.status'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.count'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.speed'); ?></th>
										</tr>
										<tr style="text-align:center" class="active">
											<td><?php echo Lang::get('content.excellent'); ?></td>
											<td>{{excellentCount}}</td>
											<td>{{excellentSpeed}}</td>
										</tr>
										<tr style="text-align:center" class="active">
											<td><?php echo Lang::get('content.best'); ?></td>
											<td>{{bestCount}}</td>
											<td>{{bestSpeed}}</td>
										</tr>
										<tr style="text-align:center" class="active">
											<td><?php echo Lang::get('content.average'); ?></td>
											<td>{{averageCount}}</td>
											<td>{{averageSpeed}}</td>
										</tr>
										<tr style="text-align:center" class="active">
											<td><?php echo Lang::get('content.aggressive'); ?></td>
											<td>{{worstCount}}</td>
											<td>{{worstSpeed}}</td>
										</tr>
										<tr style="text-align:center" class="active">
											<td><?php echo Lang::get('content.redliner'); ?></td>
											<td>{{redlinerCount}}</td>
											<td>{{redlinerSpeed}}</td>
										</tr>
									</table>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Lang::get('content.close'); ?></button>
							</div>
						</div>

					</div>
				</div> -->

				<div class="modal fade" id="myModal1" role="dialog">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-body">
								<div class="pull-right" style="margin-right: 5px;margin-bottom: 5px;">
									<img style="cursor: pointer;" ng-click="exportData('PerformanceAnalysis')"  src="../resources/views/reports/image/xls.png" />
									<img style="cursor: pointer;" ng-click="exportDataCSV('PerformanceAnalysis')"  src="../resources/views/reports/image/csv.jpeg" />
									<img style="cursor: pointer;" onclick="generateAnalysisPdf()"  src="../resources/views/reports/image/Adobe.png"  />
								</div>
								<div id="PerformanceAnalysis">

									<table ng-if="allDetails.data.weightedBreakAnalysis" class="table table-striped table-bordered table-condensed table-hover">
										<tr><th style="text-align:center;" colspan="4"><?php echo Lang::get('content.sudden_brakes'); ?> : {{allDetails.data.shortName}}</th></tr>
										<tr style="text-align:center">
											<th style="text-align:center;" colspan="4"><?php echo Lang::get('content.normal'); ?> : {{allDetails.data.suddenBreakList.normal.subTotalSuddenBreak!=undefined?allDetails.data.suddenBreakList.normal.subTotalSuddenBreak:0}}</th>
										</tr>
										<tr ng-if="allDetails.data.suddenBreakList.normal.subTotalSuddenBreak!=undefined" style="text-align:center">
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.place'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.speed'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.slow'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.time_date'); ?></th>
										</tr>
										<tr ng-repeat="info in allDetails.data.suddenBreakList.normal.historySuddenBrk" style="text-align:center" class="active">
											<td><a href="https://www.google.com/maps?q=loc:{{split(info,0)}},{{split(info,1)}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
											<td>{{split(info,3)}}</td>
											<td>{{split(info,2)}}</td>
											<td>{{split(info,4) | date:"dd/MM/yyyy 'at' h:mma"}}</td>
										</tr>
										<tr style="text-align:center">
											<th style="text-align:center;" colspan="4"><?php echo Lang::get('content.aggressive'); ?> : {{allDetails.data.suddenBreakList.aggressive.subTotalSuddenBreak!=undefined?allDetails.data.suddenBreakList.aggressive.subTotalSuddenBreak:0}}</th>
										</tr>
										<tr ng-if="allDetails.data.suddenBreakList.aggressive.subTotalSuddenBreak!=undefined" style="text-align:center">
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.place'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.speed'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.slow'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.time_date'); ?></th>
										</tr>
										<tr ng-repeat="info in allDetails.data.suddenBreakList.aggressive.historySuddenBrk" style="text-align:center" class="active">
											<td><a href="https://www.google.com/maps?q=loc:{{split(info,0)}},{{split(info,1)}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
											<td>{{split(info,3)}}</td>
											<td>{{split(info,2)}}</td>
											<td>{{split(info,4) | date:"dd/MM/yyyy 'at' h:mma"}}</td>
										</tr>
										<tr style="text-align:center">
											<th style="text-align:center;" colspan="4"><?php echo Lang::get('content.harsh'); ?> : {{allDetails.data.suddenBreakList.harsh.subTotalSuddenBreak!=undefined?allDetails.data.suddenBreakList.harsh.subTotalSuddenBreak:0}}</th>
										</tr>
										<tr ng-if="allDetails.data.suddenBreakList.harsh.subTotalSuddenBreak!=undefined" style="text-align:center">
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.place'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.speed'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.slow'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.time_date'); ?></th>
										</tr>
										<tr ng-repeat="info in allDetails.data.suddenBreakList.harsh.historySuddenBrk" style="text-align:center" class="active">
											<td><a href="https://www.google.com/maps?q=loc:{{split(info,0)}},{{split(info,1)}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
											<td>{{split(info,3)}}</td>
											<td>{{split(info,2)}}</td>
											<td>{{split(info,4) | date:"dd/MM/yyyy 'at' h:mma"}}</td>
										</tr>
										<tr style="text-align:center">
											<th style="text-align:center;" colspan="4"><?php echo Lang::get('content.very_harsh'); ?> : {{allDetails.data.suddenBreakList.veryharsh.subTotalSuddenBreak!=undefined?allDetails.data.suddenBreakList.veryharsh.subTotalSuddenBreak:0}}</th>
										</tr>
										<tr ng-if="allDetails.data.suddenBreakList.veryharsh.subTotalSuddenBreak!=undefined" style="text-align:center">
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.place'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.speed'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.slow'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.time_date'); ?></th>
										</tr>
										<tr ng-repeat="info in allDetails.data.suddenBreakList.veryharsh.historySuddenBrk" style="text-align:center" class="active">
											<td><a href="https://www.google.com/maps?q=loc:{{split(info,0)}},{{split(info,1)}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
											<td>{{split(info,3)}}</td>
											<td>{{split(info,2)}}</td>
											<td>{{split(info,4) | date:"dd/MM/yyyy 'at' h:mma"}}</td>
										</tr>
										<tr style="text-align:center">
											<th style="text-align:center;" colspan="4"><?php echo Lang::get('content.worst'); ?> : {{allDetails.data.suddenBreakList.worst.subTotalSuddenBreak!=undefined?allDetails.data.suddenBreakList.worst.subTotalSuddenBreak:0}}</th>
										</tr>
										<tr ng-if="allDetails.data.suddenBreakList.worst.subTotalSuddenBreak!=undefined" style="text-align:center">
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.place'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.speed'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.slow'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.time_date'); ?></th>
										</tr>
										<tr ng-repeat="info in allDetails.data.suddenBreakList.worst.historySuddenBrk" style="text-align:center" class="active">
											<td><a href="https://www.google.com/maps?q=loc:{{split(info,0)}},{{split(info,1)}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
											<td>{{split(info,3)}}</td>
											<td>{{split(info,2)}}</td>
											<td>{{split(info,4) | date:"dd/MM/yyyy 'at' h:mma"}}</td>
										</tr>
									</table>
									<table ng-if="allDetails.data.weightedSpeedAnalysis" class="table table-striped table-bordered table-condensed table-hover">
										<!-- <tr><th style="text-align:center;" colspan="4">Vehicle Name : {{id}}</th></tr> -->
										<tr style="text-align:center">
											<th style="text-align:center;" colspan="4"><?php echo Lang::get('content.speed_analysis'); ?> : {{allDetails.data.shortName}}</th>
										</tr>
										<tr style="text-align:center">
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.status'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.count'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.speed'); ?></th>
										</tr>
										<tr style="text-align:center" class="active">
											<td><?php echo Lang::get('content.excellent'); ?></td>
											<td>{{allDetails.data.historySpeedAnalysis.Excellent!=undefined?split(allDetails.data.historySpeedAnalysis.Excellent,0):0}}</td>
											<td>{{allDetails.data.historySpeedAnalysis.Excellent!=undefined?split(allDetails.data.historySpeedAnalysis.Excellent,1):0}}</td>
										</tr>
										<tr style="text-align:center" class="active">
											<td><?php echo Lang::get('content.best'); ?></td>
											<td>{{allDetails.data.historySpeedAnalysis.Best!=undefined?split(allDetails.data.historySpeedAnalysis.Best,0):0}}</td>
											<td>{{allDetails.data.historySpeedAnalysis.Best!=undeifned?split(allDetails.data.historySpeedAnalysis.Best,1):0}}</td>
										</tr>
										<tr style="text-align:center" class="active">
											<td><?php echo Lang::get('content.average'); ?></td>
											<td>{{allDetails.data.historySpeedAnalysis.Average!=undefined?split(allDetails.data.historySpeedAnalysis.Average,0):0}}</td>
											<td>{{allDetails.data.historySpeedAnalysis.Average!=undefined?split(allDetails.data.historySpeedAnalysis.Average,1):0}}</td>
										</tr>
										<tr style="text-align:center" class="active">
											<td><?php echo Lang::get('content.aggressive'); ?></td>
											<td>{{allDetails.data.historySpeedAnalysis.Aggressive!=undefined?split(allDetails.data.historySpeedAnalysis.Aggressive,0):0}}</td>
											<td>{{allDetails.data.historySpeedAnalysis.Aggressive!=undefined?split(allDetails.data.historySpeedAnalysis.Aggressive,1):0}}</td>
										</tr>
										<tr style="text-align:center" class="active">
											<td><?php echo Lang::get('content.redliner'); ?></td>
											<td>{{allDetails.data.historySpeedAnalysis.RedLiner!=undefined?split(allDetails.data.historySpeedAnalysis.RedLiner,0):0}}</td>
											<td>{{allDetails.data.historySpeedAnalysis.RedLiner!=undefined?split(allDetails.data.historySpeedAnalysis.RedLiner,1):0}}</td>
										</tr>
									</table>
									<table ng-if="allDetails.data.weightedAccelAnalysis" class="table table-striped table-bordered table-condensed table-hover">
										<tr><th style="text-align:center;" colspan="4"><?php echo Lang::get('content.sudden_acceleration'); ?> : {{allDetails.data.shortName}}</th></tr>
										<tr style="text-align:center">
											<th style="text-align:center;" colspan="4"><?php echo Lang::get('content.normal'); ?> : {{allDetails.data.suddenAcceleration.normal.totalSudAcceleration!=undefined?allDetails.data.suddenAcceleration.normal.totalSudAcceleration:0}}</th>
										</tr>
										<tr ng-if="allDetails.data.suddenAcceleration.normal.totalSudAcceleration!=undefined" style="text-align:center">
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.place'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.speed'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.slow'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.time_date'); ?></th>
										</tr>
										<tr ng-repeat="info in allDetails.data.suddenAcceleration.normal.historySuddenAcceleration" style="text-align:center" class="active">
											<td><a href="https://www.google.com/maps?q=loc:{{split(info,0)}},{{split(info,1)}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
											<td>{{split(info,3)}}</td>
											<td>{{split(info,2)}}</td>
											<td>{{split(info,4) | date:"dd/MM/yyyy 'at' h:mma"}}</td>
										</tr>
										<tr style="text-align:center">
											<th style="text-align:center;" colspan="4"><?php echo Lang::get('content.aggressive'); ?> : {{allDetails.data.suddenAcceleration.aggressive.totalSudAcceleration!=undefined?allDetails.data.suddenAcceleration.aggressive.totalSudAcceleration:0}}</th>
										</tr>
										<tr ng-if="allDetails.data.suddenAcceleration.aggressive.totalSudAcceleration!=undefined" style="text-align:center">
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.place'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.speed'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.slow'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.time_date'); ?></th>
										</tr>
										<tr ng-repeat="info in allDetails.data.suddenAcceleration.aggressive.historySuddenAcceleration" style="text-align:center" class="active">
											<td><a href="https://www.google.com/maps?q=loc:{{split(info,0)}},{{split(info,1)}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
											<td>{{split(info,3)}}</td>
											<td>{{split(info,2)}}</td>
											<td>{{split(info,4) | date:"dd/MM/yyyy 'at' h:mma"}}</td>
										</tr>
										<tr style="text-align:center">
											<th style="text-align:center;" colspan="4"><?php echo Lang::get('content.harsh'); ?> : {{allDetails.data.suddenAcceleration.harsh.totalSudAcceleration!=undefined?allDetails.data.suddenAcceleration.harsh.totalSudAcceleration:0}}</th>
										</tr>
										<tr ng-if="allDetails.data.suddenAcceleration.harsh.totalSudAcceleration!=undefined" style="text-align:center">
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.place'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.speed'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.slow'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.time_date'); ?></th>
										</tr>
										<tr ng-repeat="info in allDetails.data.suddenAcceleration.harsh.historySuddenAcceleration" style="text-align:center" class="active">
											<td><a href="https://www.google.com/maps?q=loc:{{split(info,0)}},{{split(info,1)}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
											<td>{{split(info,3)}}</td>
											<td>{{split(info,2)}}</td>
											<td>{{split(info,4) | date:"dd/MM/yyyy 'at' h:mma"}}</td>
										</tr>
										<tr style="text-align:center">
											<th style="text-align:center;" colspan="4"><?php echo Lang::get('content.very_harsh'); ?> : {{allDetails.data.suddenAcceleration.veryharsh.subTotalSuddenBreak!=undefined?allDetails.data.suddenAcceleration.veryharsh.subTotalSuddenBreak:0}}</th>
										</tr>
										<tr ng-if="allDetails.data.suddenAcceleration.veryharsh.subTotalSuddenBreak!=undefined" style="text-align:center">
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.place'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.speed'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.slow'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.time_date'); ?></th>
										</tr>
										<tr ng-repeat="info in allDetails. data.suddenAcceleration.veryharsh.subTotalSuddenBreak" style="text-align:center" class="active">
											<td><a href="https://www.google.com/maps?q=loc:{{split(info,0)}},{{split(info,1)}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
											<td>{{split(info,3)}}</td>
											<td>{{split(info,2)}}</td>
											<td>{{split(info,4) | date:"dd/MM/yyyy 'at' h:mma"}}</td>
										</tr>
										<tr style="text-align:center">
											<th style="text-align:center;" colspan="4"><?php echo Lang::get('content.worst'); ?> : {{allDetails.data.suddenAcceleration.worst.subTotalSuddenBreak!=undefined?allDetails.data.suddenAcceleration.worst.subTotalSuddenBreak:0}}</th>
										</tr>
										<tr ng-if="allDetails.data.suddenAcceleration.worst.subTotalSuddenBreak!=undefined" style="text-align:center">
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.place'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.speed'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.slow'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.time_date'); ?></th>
										</tr>
										<tr ng-repeat="info in allDetails.data.suddenAcceleration.worst.historySuddenBrk" style="text-align:center" class="active">
											<td><a href="https://www.google.com/maps?q=loc:{{split(info,0)}},{{split(info,1)}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
											<td>{{split(info,3)}}</td>
											<td>{{split(info,2)}}</td>
											<td>{{split(info,4) | date:"dd/MM/yyyy 'at' h:mma"}}</td>
										</tr>
									</table>
									<table ng-if="allDetails.data.weightedShockAlarmAnalysis" class="table table-striped table-bordered table-condensed table-hover">
										<!-- <tr><th style="text-align:center;" colspan="4">Vehicle Name : {{id}}</th></tr> -->
										<tr style="text-align:center">
											<th style="text-align:center;" colspan="4"><?php echo Lang::get('content.shock_analysis'); ?> : {{allDetails.data.shortName}}</th>
										</tr>
										<tr style="text-align:center">
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.status'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.count'); ?></th>
											<th width="10%" style="text-align:center;"><?php echo Lang::get('content.speed'); ?></th>
										</tr>
										<tr style="text-align:center" class="active">
											<td><?php echo Lang::get('content.excellent'); ?></td>
											<td>{{allDetails.data.historyShockAlarm.Excellent!=undefined?split(allDetails.data.historyShockAlarm.Excellent,0):0}}</td>
											<td>{{allDetails.data.historyShockAlarm.Excellent!=undefined?split(allDetails.data.historyShockAlarm.Excellent,1):0}}</td>
										</tr>
										<tr style="text-align:center" class="active">
											<td><?php echo Lang::get('content.best'); ?></td>
											<td>{{allDetails.data.historyShockAlarm.Best!=undefined?split(allDetails.data.historyShockAlarm.Best,0):0}}</td>
											<td>{{allDetails.data.historyShockAlarm.Best!=undeifned?split(allDetails.data.historyShockAlarm.Best,1):0}}</td>
										</tr>
										<tr style="text-align:center" class="active">
											<td><?php echo Lang::get('content.average'); ?></td>
											<td>{{allDetails.data.historyShockAlarm.Average!=undefined?split(allDetails.data.historyShockAlarm.Average,0):0}}</td>
											<td>{{allDetails.data.historyShockAlarm.Average!=undefined?split(allDetails.data.historyShockAlarm.Average,1):0}}</td>
										</tr>
										<tr style="text-align:center" class="active">
											<td><?php echo Lang::get('content.aggressive'); ?></td>
											<td>{{allDetails.data.historyShockAlarm.Aggressive!=undefined?split(allDetails.data.historyShockAlarm.Aggressive,0):0}}</td>
											<td>{{allDetails.data.historyShockAlarm.Aggressive!=undefined?split(allDetails.data.historyShockAlarm.Aggressive,1):0}}</td>
										</tr>
										<tr style="text-align:center" class="active">
											<td><?php echo Lang::get('content.redliner'); ?></td>
											<td>{{allDetails.data.historyShockAlarm.RedLiner!=undefined?split(allDetails.data.historyShockAlarm.RedLiner,0):0}}</td>
											<td>{{allDetails.data.historyShockAlarm.RedLiner!=undefined?split(allDetails.data.historyShockAlarm.RedLiner,1):0}}</td>
										</tr>
									</table>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Lang::get('content.close'); ?></button>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

	</div>
</div>
<script src="assets/js/jquery-1.11.0.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script> 
<script src="assets/js/ui-bootstrap-tpls-0.13.3.js"></script>
<script data-require="angular-ui-bootstrap@0.11.0" data-semver="0.11.0" src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>
<script src="../resources/views/reports/customjs/moment.js"></script>
<script src="assets/js/angular-ui-bootstrap-modal.js"></script>
<script src="assets/js/angular-translate.js"></script>
<script src="../resources/views/reports/customjs/FileSaver.js"></script>
<script src="../resources/views/reports/customjs/html5csv.js"></script>
<script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
<script src="assets/js/infobubble.js" type="text/javascript"></script>
<script src="assets/js/naturalSortVersionDatesCaching.js"></script>
<!-- <script src="assets/js/naturalSortVersionDates.js"></script>-->
<script src="assets/js/static.js"></script>
<script src="assets/js/vamoApp.js"></script>
<script src="assets/js/highcharts-bar.js"></script>
<script src="assets/js/services.js"></script>
<script src="assets/js/performance.js?v=<?php echo Config::get('app.version');?>"></script>
<script type="text/javascript">
	// For demo to fit into DataTables site builder...
	$('#example')
	.removeClass( 'display' )
	.addClass('table table-striped table-bordered');
</script>
<script>
	$("#menu-toggle").click(function(e) {
		e.preventDefault();
		$("#wrapper").toggleClass("toggled");
	});

	var generatePDF = function() {
		kendo.drawing.drawDOM($("#formConfirmation"),{paperSize: "A4",multiPage: true,margin: {
			left   : "2mm",
			top    : "10mm",
			right  : "2mm",
			bottom : "5mm"
		} ,landscape: true }).then(function(group) {
			kendo.drawing.pdf.saveAs(group, "PerformanceReport.pdf");
		});
	}

	var generateAnalysisPdf = function() {
		kendo.drawing.drawDOM($("#PerformanceAnalysis"),{paperSize: "A4",multiPage: true,margin: {
			left   : "2mm",
			top    : "10mm",
			right  : "2mm",
			bottom : "5mm"
		} ,landscape: true }).then(function(group) {
			kendo.drawing.pdf.saveAs(group, "PerformanceAnalysis.pdf");
		});
	}


	$(function () {

	  //       $('#dateFrom').datetimepicker({
			// 	pickTime: false,
			// 	format: "MMM,YYYY",
			// 	viewMode: "months", 
			// 	minViewMode: "months"
			// });
			var date 	 = 	new Date();
			var lastMonth	 =	new Date(date.setMonth(date.getMonth()-1));
			$('#monthFrom').datetimepicker({
				minViewMode: 'months',
				viewMode: 'months',
				pickTime: false,
				useCurrent:true,
				format:'MM/YYYY',
				maxDate: new Date,
				minDate: new Date(2015, 12, 1),
				defaultDate: lastMonth,
			});
			var dateObj 	 = 	new Date();
			var fromNowTSS	 =	new Date(dateObj.setDate(dateObj.getDate()-1));
		  //console.log(getTodayDate(fromNowTSS));
		  var fromNowTimes =  getTodayDate(fromNowTSS);

		  $('#datef').datetimepicker({
		  	format:'DD-MM-YYYY',
		  	useCurrent:true,
		  	pickTime: false,
		  	defaultDate: fromNowTimes,
		  });

		/*	$('#timeFrom').datetimepicker({
				pickDate: false
			});
			$('#timeTo').datetimepicker({
				useCurrent:true,
				pickDate: false
			}); */
		});      
	</script>
	
</body>
</html>

