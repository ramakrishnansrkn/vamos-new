<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title><?php echo Lang::get('content.gps'); ?></title>

<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
<link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>

<style>
body {
      font-family: 'Lato', sans-serif;
   /* font-weight: bold; */  
   /* font-family: 'Lato', sans-serif;
      font-family: 'Roboto', sans-serif;
      font-family: 'Open Sans', sans-serif;
      font-family: 'Raleway', sans-serif;
      font-family: 'Faustina', serif;
      font-family: 'PT Sans', sans-serif;
      font-family: 'Ubuntu', sans-serif;
      font-family: 'Droid Sans', sans-serif;
      font-family: 'Source Sans Pro', sans-serif;
      */
  }
.empty{
    height: 1px; width: 1px; padding-right: 30px; float: left;
}
.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
   background-color: #ffffff;
}

</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2X3F316479');
</script>
</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
    <div ng-controller="mainCtrl" class="ng-cloak">
      <div id="wrapper">
     <?php include('sidebarList.php');?> 
        
        <div id="testLoad"></div>
        
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default">
                 
                </div>   
            </div>
        </div>
 


        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
                    <h3 class="box-title"><?php echo Lang::get('content.geofence'); ?></h3>
                </div>
        
                <div>

                    <div class="pull-right" style="margin-top: 10px;margin-right: 5px;">
                        <img style="cursor: pointer;" ng-click="exportData('GeofenceReport')"  src="../resources/views/reports/image/xls.png" />
                        <img style="cursor: pointer;" ng-click="exportDataCSV('GeofenceReport')"  src="../resources/views/reports/image/csv.jpeg" />
                        <img style="cursor: pointer;" onclick="generatePDF()"  src="../resources/views/reports/image/Adobe.png" />
                    </div>

                    
              
            <div class="box-body" id="GeofenceReport"  style="padding-top:20px;" > 

                <div class="empty" align="center"></div>
           

                        <div class="row" style="padding-top: 20px;"></div>

                        <div>
                          <div id="formConfirmation">


                          <div class="row">
                              <div class="col-md-3">
                                <button type="button" class="btn btn-warning" style="width: 70%;height: 50px;"><?php echo Lang::get('content.total'); ?> - {{totalVehicles}}</button>
                              </div>
                              <div class="col-md-3">
                                <button type="button" class="btn" style="color: white;background: #efc016ed;width: 70%;height: 50px;"><?php echo Lang::get('content.online'); ?> - {{vehicleOnline}} </button>
                              </div>
                              <div class="col-md-3">
                                <button type="button" class="btn btn-success" style="width: 70%;height: 50px;"><?php echo Lang::get('content.moving'); ?> - {{movingCount}}</button>
                              </div>
                              <div class="col-md-3">
                                <button type="button" class="btn btn-danger" style="width: 70%;height: 50px;"><?php echo Lang::get('content.nodata'); ?>   -  {{noDataCount}}</button>
                              </div>
                           </div>
                          <div class="panel-group" id="accordion" ng-repeat="singleData in getGeoFence track by $index"  ng-init="getCount($index)" style="margin-top: 10px;">
                                   <div class="row">   
                                      <div class="panel panel-default col-md-3" style="margin-left: 10px;padding-left: 0px;
                                         padding-right: 0px;top: 5px;" id="site{{$index}}">
                                          <div  class="panel-heading" style="text-overflow: ellipsis;
                                           overflow: hidden;background-color:white">
                                         {{singleData.geoFence}}   <span class="span6 pull-right"><b>{{singleData.vehiclesGeofence.length}}</b></span>
                                        </div>
                                      </div>
                                      <div class="panel panel-default col-md-8" style="margin-left: 10px" id="vehicle{{$index}}">
                                              <div  class="panel-heading" style="background-color:white">

                                                 
                                                  <h8 class="panel-title" style="font-size:14px;padding-right:14px;">   <b><?php echo Lang::get('content.vehicle'); ?>s : </b> </h8>
                                                  <h8 class="panel-title"  style="font-size:14px;padding-right:14px;" ng-repeat="vehicleData in singleData.vehiclesGeofence"> 
                                                    <b>{{ vehicleData.vehicleId }}</b>
                                                  </h8> 

                                              </div>

                                         </div>
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-8" style="margin-left: 20px;margin-top: 10px;">
                                            <table class="table" style="border:2px #f1f1f1 solid;">
                                                 <thead class="thead-inverse">
                                                   <tr>
                                                       <th style="text-align: left;"><?php echo Lang::get('content.count'); ?></th>
                                                       <th style="text-align: left;">{{vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></th>
                                                        <th style="text-align: left;"> <?php echo Lang::get('content.reported_time'); ?> </th>
                                                        <th style="text-align: left;"> <?php echo Lang::get('content.duration'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</th>
                                                        <th style="text-align: left;"> <?php echo Lang::get('content.plan_time'); ?> </th>
                                                        <th style="text-align: left;"> <?php echo Lang::get('content.gap'); ?> </th>
                                                    </tr>
                                                 </thead>
                                              <tbody>
                                              <tr ng-repeat="vehicleInfo in singleData.vehiclesGeofence">
                                                <th scope="row" style="text-align: left;">{{$index+1}}</th>
                                                <td style="text-align: left;">{{vehicleInfo.vehicleId}}</td>
                                                <td style="text-align: left;">{{vehicleInfo.reportedTime | date:'HH:mm a'}}</td>
                                                <td style="text-align: left;">{{msToTime(vehicleInfo.duration)}}</td>
                                                <td style="text-align: left;">{{msToTime(vehicleInfo.plantime)}}</td>
                                                <td style="text-align: left;">
                                                  <span ng-if="vehicleInfo.delayTime>0" style="color: red">{{msToTime(vehicleInfo.delayTime)}}</span>
                                                  <span ng-if="vehicleInfo.delayTime<=0" style="color: green">-</span>
                                                </td>
                                              </tr>
                       
                                              </tbody>
                                            </table>
                                        </div>
                                    </div>
                                 
                                 <!-- collapse End  -->
                         
                      </div>
                     </div>


                   </div>  
                  </div>
                  </div>

                
                  </div>
                </div>
            </div>

          </div>
        </div>


    <script src="assets/js/static.js"></script>
    <script src="assets/js/jquery-1.11.0.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
    <script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="assets/js/angular-translate.js"></script>
    <script src="../resources/views/reports/customjs/html5csv.js"></script>
    <script src="../resources/views/reports/customjs/moment.js"></script>
    <script src="../resources/views/reports/customjs/FileSaver.js"></script>
    <script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="../resources/views/reports/datatable/jquery.dataTables.js"></script>
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/services.js"></script>
    <script src="assets/js/geoFence.js?v=<?php echo Config::get('app.version');?>"></script>
    
    <script>

   
        $("#example1").dataTable();
          
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

 var generatePDF = function() {
  kendo.drawing.drawDOM($("#GeofenceReport"),{paperSize: "A4",multiPage: true,margin: '1mm',landscape: true }).then(function(group) {
    kendo.drawing.pdf.saveAs(group, "GeofenceReport.pdf");
  });
}

  </script>
    
</body>
</html>