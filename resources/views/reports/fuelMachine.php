<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title><?php echo Lang::get('content.gps'); ?></title>
  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
  <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
  <link href="assets/css/switchToggleBtn.css" rel="stylesheet">
  <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
  <style>
    .empty {
     height: 1px; width: 1px; padding-right: 30px; float: left;
   }
   .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
     background-color: #ffffff;
   }

   body {
    font-family: 'Lato', sans-serif;
  /*font-weight: bold;
  font-family: 'Lato', sans-serif;
  font-family: 'Roboto', sans-serif;
  font-family: 'Open Sans', sans-serif;
  font-family: 'Raleway', sans-serif;
  font-family: 'Faustina', serif;
  font-family: 'PT Sans', sans-serif;
  font-family: 'Ubuntu', sans-serif;
  font-family: 'Droid Sans', sans-serif;
  font-family: 'Source Sans Pro', sans-serif;
  */
} 

.col-md-12 {
 width: 98% !important;
 left: 15px !important;
 padding-left: 20px !important;
}

.tooltip-color + .tooltip > .tooltip-inner {background-color: white;border: 1px solid black;color: black;}

/*
.chart {
    min-width: 320px;
    max-width: 800px;
    height: 280px;
    margin: 0 auto;
}

#container {
    width: 80%;
    height: unset !important; 
    max-width: 80%;
    max-height: unset !important; 
    min-height: 500px !important;
    margin-top: 10px !important;
}
*/

</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2X3F316479');
</script>
</head>

<div id="preloader" >
  <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
  <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
  <div ng-controller="mainCtrl" class="ng-cloak">
    <div id="wrapper">
      <?php include('sidebarList.php');?> 
      <div id="testLoad"></div>

      <div id="page-content-wrapper">
        <div class="container-fluid">
          <div class="panel panel-default"></div>   
        </div>
      </div>

      <div class="col-md-12">
       <div class="box box-primary" style="padding-top: 5px;margin-top: -20px;">
        <!-- <div class="row"> -->
          <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
            <h3 class="box-title"><?php echo Lang::get('content.fuel_analytics'); ?>
          </h3>
        </div>
        <div class="row">
          <div class="col-md-2" align="center">
            <div class="form-group" ng-if="shortNam!=undefined || shortNam!=null">
              <h5 style="color: grey;">{{shortNam}}</h5>
            </div>

          </div>
          <?php include('dateTime.php');?>
          <div class="col-md-1" align="center">
            <button style="margin-left: -42%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
          </div>
        </div>
        <div class="col-md-9" align="right" style="
        margin-bottom: 10px;">
        <?php include('weekdays.php');?>
      </div>
      <div class="col-md-3">
       <?php include('helpVideos.php');?>
     </div>

     <!--  </div> -->
     <div class="row">
      <div class="col-md-1" align="center"></div>
      <div class="col-md-2" align="center">
        <div class="form-group"></div>
      </div>
    </div>

  </div>
</div>

<div class="col-md-12" style="border-color: unset!important;">
  <div class="box box-primary" style="min-height:570px;background-color:#fdfdfd;">
    <div>
      <div class="pull-left" ng-if="sensorCount>1"  ng-repeat="sensorNo in range(1,sensorCount)"  style="margin-top: 10px;margin-left: 5px;margin-bottom:10px;">
       <button type="button" class="btn btn-info" ng-click="sensorChange(sensorNo)" id="sensor{{sensorNo}}" ><?php echo Lang::get('content.sensor'); ?> {{sensorNo}}</button>
     </div> 
     <div class="pull-left" ng-if="sensorCount>1" style="margin-top: 10px;margin-left: 5px;margin-bottom:10px;">
       <button type="button" class="btn btn-info" ng-click="sensorChange('All')" id="sensorAll" ><?php echo Lang::get('content.all'); ?></button>
     </div> 
     <div class="pull-right" style="margin-top: 10px;margin-right: 5px;margin-bottom:10px;">
      <!-- <button type="button" class="btn btn-info" ng-click="durationFilter('month')" ng-disabled="monthDisabled"><?php echo Lang::get('content.month'); ?></button> -->

      <img style="cursor: pointer;" ng-click="exportData('fuelMachineReport')"  src="../resources/views/reports/image/xls.png" ng-hide="sensor=='All'"/>
      <img style="cursor: pointer;" ng-click="exportDataCSV('fuelMachineReport')"  src="../resources/views/reports/image/csv.jpeg" ng-hide="sensor=='All'"/>
      <img style="cursor: pointer;" onclick="generatePDF()"  src="../resources/views/reports/image/Adobe.png" ng-hide="sensor=='All'"/>
    </div>
    <div class="box-body" >

      <div style="margin-top: 20px;" id="fuelMachineReport1">
       <div id="formConfirmation">
         <table class="table table-bordered table-striped table-condensed table-hover" ng-if="!errMsg.includes('expired')" >
          <thead>
            <tr style="text-align:center;">
              <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.group'); ?></th>
              <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(gName)}}</th>
              <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total_distance'); ?></th>
              <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(fuelMachineData.sensorMode == 'DG') ? '-' : fuelMachineData.tdistance}}</th>
            </tr>
            <tr style="text-align:center;" ng-hide="fuelMachineData.error && sensor=='All'">
             <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.start_fuel_level'); ?></th>
             <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelMachineData.sFuelLevel}}</th>
             <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.end_fuel_level'); ?></th>       
             <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelMachineData.eFuelLevel}}</th>   
           </tr> 
           <tr style="text-align:center;" ng-hide="fuelMachineData.error && sensor=='All'">
            <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.tot_fuel_fill'); ?></th>
            <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"> {{(sensor=='All')? fuelMachineData.totalFuelFillforAll :roundOffDecimal(fuelMachineData.totalFuelFills)}}</th>
            <th colspan="2"  ng-hide="fuelMachineData.sensorMode=='Dispenser'" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_theft'); ?></th>

            <th colspan="2" ng-hide="fuelMachineData.sensorMode=='Dispenser'" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(sensor=='All')? fuelMachineData.totalFuelDropforAll :roundOffDecimal(fuelMachineData.totalFuelDrops)}}</th>   
            <th colspan="2"  ng-show="fuelMachineData.sensorMode=='Dispenser'"style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_dispensed'); ?></th>
         <th colspan="2"  ng-show="fuelMachineData.sensorMode=='Dispenser'" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(fuelMachineData.sensorMode=='Dispenser')? roundOffDecimal(fuelMachineData.fuelDispenser) : "-"}}</th> 
          </tr>
          <tr style="text-align:center;" ng-hide="fuelMachineData.error && sensor=='All'">
           <th colspan="2"  style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.tot_fuel_consume'); ?></th>
           <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{roundOffDecimal(fuelMachineData.tFuelCon)}}</th>
           <th ng-hide="fuelMachineData.sensorMode == 'DG' || fuelMachineData.sensorMode == 'Machinery'" colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.kmpl'); ?></th>
           <th ng-hide="fuelMachineData.sensorMode == 'DG' || fuelMachineData.sensorMode == 'Machinery'"colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelMachineData.kmpl}}</th> 
         <th ng-show="fuelMachineData.sensorMode == 'DG' || fuelMachineData.sensorMode == 'Machinery'" colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.LTPH'); ?></th>
        <th ng-show="fuelMachineData.sensorMode == 'DG' || fuelMachineData.sensorMode == 'Machinery'" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(fuelMachineData.sensorMode =='DG' || fuelMachineData.sensorMode == 'Machinery')?fuelMachineData.lph : "-"}}</th>  
        </tr>
         <tr align="center" ng-if="!errMsg">
          <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 7 : 8 }}" class="err" ng-show="fuelMachineData.error && sensor=='All'" style="text-align: center;">
           <h5>{{fuelMachineData.error}}</h5>
         </td>
       </tr>

     </thead>
   </table>
   <div class="panel panel-default" style="margin-top: 20px;margin-bottom: 5px;" ng-show="sensor!='All' && theftBeta">
    <div class="panel-body">
      <span style="font-size: 14px;" ng-show="sensor!='All' && theftBeta && (fuelMachineData.sensorMode != 'DG'&&fuelMachineData.sensorMode != 'Machinery')">
        <span style="color: #2f6581;"> <?php echo Lang::get('content.mileage'); ?> : </span>
        <input ng-model="millage" ng-change="applyFilter()" onkeydown="validateval(event)" maxlength="1" style="padding:3px 3px 3px 3px; font-size: 12px;width:100px;margin-left: 20px;" />
      </span>
      <span style="font-size: 14px;" ng-show="sensor!='All' && theftBeta && (fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery')">
        <span style="color: #2f6581;"> <?php echo Lang::get('content.LTPH'); ?> : </span>
        <select ng-model="ltphValue" ng-change="applyFilter()" style="padding:3px 3px 3px 3px; font-size: 12px;width:100px;margin-left: 20px;"  >
          <option value=""><?php echo Lang::get('content.all'); ?></option>
          <option value="3">  > 3 </option>
          <option value="4">  > 4 </option>
          <option value="5">  > 5 </option>
          <option value="6">  > 6  </option>
          <option value="7">  > 7  </option>
          <option value="8">  > 8 </option>
          <option value="9">  > 9 </option>
          <option value="10">  > 10 </option>
        </select>
      </span>
      <span style="font-size: 14px;margin-left: 20px;" ng-show="sensor!='All' && theftBeta">
        <span style="color: #2f6581;"> <?php echo Lang::get('content.ign'); ?> : </span>
        <select ng-model="ignitionOnPer" ng-change="applyFilter()" style="padding:3px 3px 3px 3px; font-size: 12px;width:100px;margin-left: 5px;"  >
          <option value=""><?php echo Lang::get('content.all'); ?></option>
          <option value="10">  <= 10 %</option>
          <option value="20">  <= 20 %</option>
          <option value="30">  <= 30 %</option>
          <option value="40">  <= 40 %</option>
          <option value="50">  <= 50 %</option>
          <option value="60">  <= 60 %</option>
          <option value="70">  <= 70 %</option>
          <option value="80">  <= 80 %</option>
          <option value="90">  <= 90 %</option>
          <option value="100">  <= 100 %</option>
        </select>
      </span>
      <span style="font-size: 14px;margin-left: 20px;" ng-show="sensor!='All' && theftBeta " >
        <span style="color: #2f6581;"> <?php echo Lang::get('content.excess_consumption'); ?> Filter : </span>
                        <!-- <label class="rate-hit" style="margin-left: 5px;">
                           <input type="radio" ng-model="showFueltheftFilter" value="ON" ng-checked="showFueltheftFilter=='ON'">
                           ON
                       </label>
                       &nbsp;&nbsp;
                       <label class="rate-miss">
                           <input type="radio" ng-model="showFueltheftFilter" value="OFF" ng-checked="showFueltheftFilter=='OFF'">
                           OFF 
                         </label> -->
                         <label class="switch">
                          <input type="checkbox" ng-click="handleFuelFilter('btnClick')" ng-checked="showFueltheftFilter">
                          <span class="slider round"></span>
                        </label>

                      </span>
                      <span style="font-size: 14px;margin-left: 20px;" ng-show="showFueltheftFilter && sensor!='All' && theftBeta" >
                        <select ng-model="minFuelTheft" ng-change="applyFilter()" style="padding:3px 3px 3px 3px; font-size: 12px;width:100px;margin-left: 5px;">
                          <option value=""><?php echo Lang::get('content.all'); ?> </option>
                          <option value="5">  >= 5 </option>
                          <option value="10">  >= 10  </option>
                          <option value="20">  >= 20  </option>
                          <option value="30">  >= 30 </option>
                          <option value="40">  >= 40 </option>
                          <option value="50">  >= 50 </option>
                          <option value="others"> Others </option>
                        </select>
                      </span>
                      <span style="font-size: 14px;margin-left: 20px;" ng-show="showFueltheftFilter && minFuelTheft=='others' && sensor!='All' && theftBeta" >
                        <input type="number" ng-model="customMinFuelTheftFilterValue" ng-change="applyFilter()" placeholder="Enter Fuel in ltrs">
                      </span>
                    </div>
                  </div>
                  <div style="padding-top: 15px;" ng-hide="sensor=='All'"> 

                    <tabset class="nav-tabs-custom">

                      <tab heading="<?php echo Lang::get('content.fuel_fill'); ?>" active="true" ng-click="handleFuelTheftBeta('no')">

                        <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top: 10px;"> 
                          <thead class='header' style=" z-index: 1;">
                            <tr style="text-align:center">
                              <th class="id" custom-sort order="'time'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.from'); ?> <?php echo Lang::get('content.time'); ?></th>
                              <th class="id" custom-sort order="'time'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.to'); ?> <?php echo Lang::get('content.time'); ?></th>
                              <th class="id" custom-sort order="'pFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.pre_fuel'); ?></th>
                              <th class="id" custom-sort order="'cFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.cur_fuel'); ?></th>
                              <th  width="25%" ng-show="fuelMachineData.sensorMode=='Moving Vehicle' && fuelMachineData.consumption=='yes'"  class="id" custom-sort order="'fuelConsumption'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><span><?php echo Lang::get('content.consumption_during_fuel_fill'); ?></span>
                                <span><i style="margin-left: 10px;color:#2f6581;font-size: 15px;" class="fa fa-info-circle tooltip-color" aria-hidden="true" data-toggle="tooltip" title="
                                  <?php echo Lang::get('content.consumption_during_fuel_fill'); ?> = <?php echo Lang::get('content.distance_covered'); ?> / <?php echo Lang::get('content.fuel'); ?> <?php echo Lang::get('content.average'); ?>"></i></span>
                                </th>
                                <th class="id" custom-sort order="'lt'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.fuel_fill'); ?></th>
                                <th class="id" custom-sort order="'odo'" sort="sort" ng-hide="fuelMachineData.sensorMode == 'DG'" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.odo'); ?></th>
                                <th  custom-sort order="'address'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;" class="addr"><?php echo Lang::get('content.address'); ?></th>      
                                <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;" class="loc"><?php echo Lang::get('content.gmap'); ?></th>                    
                              </tr>
                            </thead>
                            <tbody ng-repeat="fuel in fuelMachineData.fuelFills | orderBy:natural(sort.sortingOrder):sort.reverse">
                              <tr ng-if="!errMsg.includes('expired')">
                                <td>{{fuel.startTime | date:'HH:mm:ss'}}   {{fuel.startTime | date:'dd-MM-yyyy'}}</td>
                                <td>{{fuel.time | date:'HH:mm:ss'}}   {{fuel.time | date:'dd-MM-yyyy'}}</td>
                                <td>{{fuel.pFuel}}</td>                    
                                <td>{{fuel.cFuel}}</td>
                                <td width="25%" ng-show="fuelMachineData.sensorMode=='Moving Vehicle' && fuelMachineData.consumption=='yes'">{{roundOffDecimal(fuel.fuelConsumption)}}</td>
                                <td>{{roundOffDecimal(fuel.lt)}}</td>
                                <td ng-hide="fuelMachineData.sensorMode == 'DG'">{{fuel.odo}}</td>     
                                <td class="addr" ng-if="fuel.address!=null">{{fuel.address}}</td>   
                                <td class="addr" ng-if="fuel.address==null" style="text-decoration: underline;"><a href="https://www.google.com/maps?q=loc:{{fuel.lat}},{{fuel.lng}}" target="_blank">{{fuel.lat}},{{fuel.lng}}</a></td>   
                                <td><a href="https://www.google.com/maps?q=loc:{{fuel.lat}},{{fuel.lng}}" target="_blank" class="loc">Link</a></td>                  
                              </tr> 
                            </tbody>
                            <tr align="center" ng-if="!errMsg">
                              <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 10 : fuelMachineData.sensorMode=='Moving Vehicle' && fuelMachineData.consumption=='yes'?12:11 }}" ng-if="(fuelMachineData.fuelFills==null||fuelMachineData.fuelFills.length==0)&&!fuelMachineData.error" class="err" style="text-align: center;">
                               <h5><?php echo Lang::get('content.no_records_avaiable'); ?></h5>
                             </td>
                             <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 10 : fuelMachineData.sensorMode=='Moving Vehicle' && fuelMachineData.consumption=='yes'?12:11 }}" ng-if="(fuelMachineData.fuelFills==null||fuelMachineData.fuelFills.length==0)&&fuelMachineData.error" class="err" style="text-align: center;">
                               <h5>{{fuelMachineData.error}}</h5>
                             </td>
                           </tr>
                           <tr align="center" ng-if="errMsg">
                            <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 9 : fuelMachineData.sensorMode=='Moving Vehicle' && fuelMachineData.consumption=='yes'?11:10 }}" class="err" ><h5>{{errMsg}}</h5></td>
                          </tr>
                        </table> 

                      </tab>

                      <tab heading="<?php echo Lang::get('content.fuel_theft'); ?>" active="false" ng-hide="fuelMachineData.sensorMode=='Dispenser'" ng-click="handleFuelTheftBeta('no')">


                        <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top: 10px;"> 
                          <thead class='header' style=" z-index: 1;">
                            <tr style="text-align:center">
                              <th class="id" custom-sort order="'startTime'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.from'); ?> <?php echo Lang::get('content.time'); ?></th>
                              <th class="id" custom-sort order="'time'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.to'); ?> <?php echo Lang::get('content.time'); ?></th>
                              <th class="id" custom-sort order="'pFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.pre_fuel'); ?></th>
                              <th class="id" custom-sort order="'cFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.cur_fuel'); ?></th>  <th class="id" custom-sort order="'lt'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.fuel_theft'); ?></th>
                              <th class="id" custom-sort order="'odo'" sort="sort" ng-hide="fuelMachineData.sensorMode == 'DG'" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.odo'); ?></th>
                              <th custom-sort order="'address'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;" class="addr"><?php echo Lang::get('content.address'); ?></th>          
                              <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;" class="loc"> <?php echo Lang::get('content.gmap'); ?></th>                    
                            </tr>
                          </thead>
                          <tbody ng-repeat="fuel in fuelMachineData.fuelDrops | orderBy:natural(sort.sortingOrder):sort.reverse">
                            <tr ng-if="!errMsg.includes('expired')">
                              <td>{{fuel.startTime | date:'HH:mm:ss'}}   {{fuel.startTime | date:'dd-MM-yyyy'}}</td>
                              <td>{{fuel.time | date:'HH:mm:ss'}}   {{fuel.time | date:'dd-MM-yyyy'}}</td>
                              <td>{{fuel.pFuel}}</td>
                              <td>{{fuel.cFuel}}</td>                   
                              <td>{{roundOffDecimal(fuel.lt)}}</td>
                              <td ng-hide="fuelMachineData.sensorMode == 'DG'">{{fuel.odo}}</td>  
                              <td class="addr" ng-if="fuel.address!=null">{{fuel.address}}</td>   
                              <td class="addr" ng-if="fuel.address==null" style="text-decoration: underline;"><a href="https://www.google.com/maps?q=loc:{{fuel.lat}},{{fuel.lng}}" target="_blank">{{fuel.lat}},{{fuel.lng}}</a></td>       
                              <td class="loc"><a href="https://www.google.com/maps?q=loc:{{fuel.lat}},{{fuel.lng}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>                  
                            </tr> 
                          </tbody>
                          <tr align="center" ng-if="!errMsg">
                            <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 9 : 10 }}" ng-if="(fuelMachineData.fuelDrops==null||fuelMachineData.fuelDrops.length==0)&&!fuelMachineData.error" class="err" style="text-align: center;">
                             <h5><?php echo Lang::get('content.no_records_avaiable'); ?></h5>
                           </td>
                           <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 9 : 10 }}" ng-if="(fuelMachineData.fuelDrops==null||fuelMachineData.fuelDrops.length==0)&&fuelMachineData.error" class="err" style="text-align: center;">
                             <h5>{{fuelMachineData.error}}</h5>
                           </td>
                         </tr>
                         <tr align="center" ng-if="errMsg">
                          <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 9 : 10 }}" class="err" ><h5>{{errMsg}}</h5></td>
                        </tr>
                      </table> 


                    </tab>
           <!--  <tab heading="<?php echo Lang::get('content.fuel_fill'); ?> <?php echo Lang::get('content.beta'); ?>" active="true" ng-click="handleFuelTheftBeta('no')">

              <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top: 10px;"> 
                <thead class='header' style=" z-index: 1;">
                <tr style="text-align:center">
                  <th class="id" custom-sort order="'time'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.from'); ?> <?php echo Lang::get('content.time'); ?></th>
                  <th class="id" custom-sort order="'time'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.to'); ?> <?php echo Lang::get('content.time'); ?></th>
                  <th class="id" custom-sort order="'pFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.pre_fuel'); ?></th>
                  <th class="id" custom-sort order="'cFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.cur_fuel'); ?></th>
                  <th class="id" custom-sort order="'lt'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.fuel_fill'); ?> <?php echo Lang::get('content.beta'); ?></th>
                  <th class="id" custom-sort order="'odo'" sort="sort" ng-hide="fuelMachineData.sensorMode == 'DG'" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.odo'); ?></th>    
                  <th  custom-sort order="'address'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;" class="addr"><?php echo Lang::get('content.address'); ?></th>      
                  <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;" class="loc"><?php echo Lang::get('content.gmap'); ?></th>                    
                </tr>
                </thead>
                <tbody ng-repeat="fuel in fuelMachineData.fuelFillBeta | orderBy:natural(sort.sortingOrder):sort.reverse">
                  <tr>
                    <td>{{fuel.startTime | date:'HH:mm:ss'}}   {{fuel.startTime | date:'dd-MM-yyyy'}}</td>
                    <td>{{fuel.time | date:'HH:mm:ss'}}   {{fuel.time | date:'dd-MM-yyyy'}}</td>
                    <td>{{fuel.pFuel}}</td>                    
                    <td>{{fuel.cFuel}}</td>
                    <td>{{roundOffDecimal(fuel.lt)}}</td>
                    <td ng-hide="fuelMachineData.sensorMode == 'DG'">{{fuel.odo}}</td>   
                    <td class="addr">{{fuel.address}}</td>           
                    <td><a href="https://www.google.com/maps?q=loc:{{fuel.lat}},{{fuel.lng}}" target="_blank" class="loc">Link</a></td>                  
                  </tr> 
                </tbody>
                  <tr align="center" ng-if="!errMsg">
                      <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 7 : 8 }}" ng-if="(fuelMachineData.fuelFillBeta==null||fuelMachineData.fuelFillBeta.length==0)&&!fuelMachineData.error" class="err" style="text-align: center;">
                           <h5><?php echo Lang::get('content.no_records_avaiable'); ?></h5>
                      </td>
                      <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 7 : 8 }}" ng-if="(fuelMachineData.fuelFillBeta==null||fuelMachineData.fuelFillBeta.length==0)&&fuelMachineData.error" class="err" style="text-align: center;">
                           <h5>{{fuelMachineData.error}}</h5>
                      </td>
                  </tr>
                  <tr align="center" ng-if="errMsg">
                      <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 7 : 8 }}" class="err" ><h5>{{errMsg}}</h5></td>
                  </tr>
              </table> 

            </tab> -->

            <tab heading="<?php echo Lang::get('content.consumption'); ?> <?php echo Lang::get('content.rate'); ?>" active="false" ng-hide="fuelMachineData.sensorMode=='Dispenser'" ng-click="handleFuelTheftBeta('yes')">


              <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top: 10px;"> 
                <thead class='header' style=" z-index: 1;">
                  <tr style="text-align:center">
                    <th class="id" custom-sort order="'startTime'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.from'); ?> <?php echo Lang::get('content.time'); ?></th>
                    <th class="id" custom-sort order="'endTime'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.to'); ?> <?php echo Lang::get('content.time'); ?></th>
                    <th class="id" custom-sort order="'ignitionOnPer'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.ignition'); ?> <?php echo Lang::get('content.on'); ?> %</th>
                    <th ng-hide="fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery'" class="id" custom-sort order="'distance'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.distance'); ?> (Kms)</th> 
                    <th class="id" custom-sort order="'startFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.pre_fuel'); ?> (Ltrs)</th>
                    <th class="id" custom-sort order="'endFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.cur_fuel'); ?> (Ltrs)</th> 
                    <th class="id" custom-sort order="'totalConsumption'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.actual_consumption'); ?> (Ltrs)</th>
                  </th> 
                  <th custom-sort order="'fuelMileage'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;">
                    <span ng-if="fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery'"><?php echo Lang::get('content.ltrs_per_hrs'); ?> </span>
                    <span ng-hide="fuelMachineData.sensorMode == 'DG' || fuelMachineData.sensorMode == 'Machinery'"><?php echo Lang::get('content.kmpl'); ?> </span>
                    <th class="id" custom-sort order="'fuelConsume'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.expected_consumption'); ?> (Ltrs)</th>
                    <th class="id" custom-sort order="'fuelTheftBeta'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.excess_consumption'); ?> (Ltrs)</th>
                    <th class="id" custom-sort order="'startAddress'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.start_loca'); ?></th>
                    <th class="id" custom-sort order="'endAddress'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.end_loca'); ?></th>
                  </tr>
                </thead>
                <tbody ng-repeat="fuel in fuelRunningTheft | orderBy:natural(sort.sortingOrder):sort.reverse">
                  <tr ng-if="!errMsg.includes('expired')">
                    <td>{{fuel.startTime | date:'HH:mm:ss'}}   {{fuel.startTime | date:'dd-MM-yyyy'}}</td>
                    <td>{{fuel.endTime | date:'HH:mm:ss'}}   {{fuel.endTime | date:'dd-MM-yyyy'}}</td>
                    <td>{{fuel.ignitionOnPer}}</td>
                    <td ng-hide="fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery'"> {{fuel.distance }} </td> 
                    <td>{{fuel.startFuel}}</td>
                    <td>{{fuel.endFuel}}</td> 
                    <td> {{fuel.totalConsumption }} </td>
                    <td> 
                      <span ng-if="fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery'">{{fuel.ltrsphr}}</span>
                      <span ng-if="fuelMachineData.sensorMode != 'DG' ||  fuelMachineData.sensorMode != 'Machinery'">{{fuel.fuelMileage}}</span>
                    </td>                 
                    <td>{{roundOffDecimal(fuel.fuelConsume)}}</td>
                    <td>{{roundOffDecimal(fuel.fuelTheftBeta)}}</td>
                    <td ng-if="fuel.startAddress!=null && fuel.startAddress!='-'">{{fuel.startAddress}}</td>
                    <td style="text-decoration: underline;" ng-if="fuel.startAddress==null || fuel.startAddress=='-'"><a href="https://www.google.com/maps?q=loc:{{fuel.startLat}},{{fuel.startLng}}" target="_blank">{{fuel.startLat}},{{fuel.startLng}}</a></td> 
                    <td ng-if="fuel.endAddress!=null && fuel.endAddress!='-'">{{fuel.endAddress}}</td>
                    <td style="text-decoration: underline;" ng-if="fuel.endAddress==null || fuel.endAddress=='-'"><a href="https://www.google.com/maps?q=loc:{{fuel.endLat}},{{fuel.endLng}}" target="_blank">{{fuel.endLat}},{{fuel.endLng}}</a></td> 
                  </tr> 

                </tbody>
                <tr ng-show="fuelRunningTheft.length!=0">
                  <td colspan="{{(fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery')?10:11}}" style="text-align: right;"> <b><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.excess_consumption'); ?> (Ltrs) </b> </td>
                  <td colspan="1"><b>{{roundOffDecimal(totalFuelTheftBeta)}}</b></td>
                </tr>
                <tr align="center" ng-if="!errMsg">
                  <td colspan="{{(fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery')?13:14}}" ng-if="(fuelRunningTheft==null||fuelRunningTheft.length==0)&&!fuelMachineData.error" class="err" style="text-align: center;">
                   <h5><?php echo Lang::get('content.no_records_avaiable'); ?></h5>
                 </td>
                 <td colspan="{{(fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery')?13:14}}" ng-if="(fuelRunningTheft==null||fuelRunningTheft.length==0)&&fuelMachineData.error" class="err" style="text-align: center;">
                   <h5>{{fuelMachineData.error}}</h5>
                 </td>
               </tr>
               <tr align="center" ng-if="errMsg">
                <td colspan="{{(fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery')?13:14}}" class="err" ><h5>{{errMsg}}</h5></td>
              </tr>
            </table> 


          </tab>


        </tabset>
      </div>
    </div>

  </div>

</div>

<div class="box-body" ng-hide="true">

  <div style="margin-top: 20px;" id="fuelMachineReport" >
    <div id="formConfirmation1">
      <table class="table table-bordered table-striped table-condensed table-hover" ng-if="!errMsg.includes('expired')"  >
        <thead>
          <tr style="text-align:center;">
            <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.group'); ?></th>
            <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(gName)}}</th>  
            <th  ng-hide="fuelMachineData.sensorMode =='DG' || fuelMachineData.sensorMode == 'Machinery'"colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.kmpl'); ?></th>
           <th  ng-hide="fuelMachineData.sensorMode =='DG' || fuelMachineData.sensorMode == 'Machinery'"colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelMachineData.kmpl}}</th> 
           <th  ng-show="fuelMachineData.sensorMode =='DG' || fuelMachineData.sensorMode == 'Machinery'" colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.LTPH'); ?></th>
           <th ng-show="fuelMachineData.sensorMode =='DG' || fuelMachineData.sensorMode == 'Machinery'"colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelMachineData.lph}}</th>  
         </tr>  
          </tr>
          <tr style="text-align:center;">
            <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.tot_fuel_consume'); ?></th>
            <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelMachineData.tFuelCon}}</th>
            <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total_distance'); ?></th>
            <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{(fuelMachineData.vehicleMode=='DG')? "-" :fuelMachineData.tdistance}}</th>   
          </tr> 
          <tr style="text-align:center;">
            <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.start_fuel_level'); ?></th>
            <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelMachineData.sFuelLevel}}</th>
            <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.end_fuel_level'); ?></th>       
            <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelMachineData.eFuelLevel}}</th>   
          </tr>
          <tr style="text-align:center;">
            <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.tot_fuel_fill'); ?></th>
            <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{roundOffDecimal(fuelMachineData.totalFuelFills)}}</th>
            <th ng-hide="fuelMachineData.sensorMode=='Dispenser'"colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_theft'); ?></th>
            <th ng-hide="fuelMachineData.sensorMode=='Dispenser'"colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{roundOffDecimal(fuelMachineData.totalFuelDrops)}}</th>   
         <th ng-show="fuelMachineData.sensorMode=='Dispenser'"colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_dispensed'); ?></th>
         <th ng-show="fuelMachineData.sensorMode=='Dispenser'"colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{roundOffDecimal(fuelMachineData.fuelDispenser)}}</th> 
        </tr>
        </thead>
      </table>

      <div style="padding-top: 15px;"> 

        <tabset class="nav-tabs-custom">

          <tab heading="<?php echo Lang::get('content.fuel_fill'); ?>" active="true">

            <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top: 10px;"> 
              <thead>
                <tr style="text-align:center">
                  <th class="id" custom-sort order="'time'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.from'); ?> <?php echo Lang::get('content.time'); ?></th>
                  <th class="id" custom-sort order="'time'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.to'); ?> <?php echo Lang::get('content.time'); ?></th>
                  <th class="id" custom-sort order="'pFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.pre_fuel'); ?></th>
                  <th class="id" custom-sort order="'cFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.cur_fuel'); ?></th>
                  <th class="id" custom-sort order="'lt'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.fuel_fill'); ?></th>
                  <th class="id" custom-sort order="'odo'" sort="sort" ng-hide="fuelMachineData.sensorMode == 'DG'" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.odo'); ?></th>  
                  <th class="id" custom-sort order="'address'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;" ><?php echo Lang::get('content.address'); ?></th>      
                  <!-- <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.gmap'); ?></th>  -->                   
                </tr>
              </thead>
              <tbody ng-repeat="fuel in fuelMachineData.fuelFills | orderBy:natural(sort.sortingOrder):sort.reverse">
                <tr ng-if="!errMsg.includes('expired')">
                  <td>{{fuel.startTime | date:'HH:mm:ss'}}   {{fuel.startTime | date:'dd-MM-yyyy'}}</td>
                  <td>{{fuel.time | date:'HH:mm:ss'}}   {{fuel.time | date:'dd-MM-yyyy'}}</td>
                  <td>{{fuel.pFuel}}</td>                    
                  <td>{{fuel.cFuel}}</td>
                  <td>{{roundOffDecimal(fuel.lt)}}</td>  
                  <td ng-hide="fuelMachineData.sensorMode == 'DG'">{{fuel.odo}}</td>   
                  <td ng-if="fuel.address!=null">{{fuel.address}}</td>   
                  <td ng-if="fuel.address==null"><a href="https://www.google.com/maps?q=loc:{{fuel.lat}},{{fuel.lng}}" target="_blank">{{fuel.lat}},{{fuel.lng}}</a></td>          
                  <!-- <td><a href="https://www.google.com/maps?q=loc:{{fuel.lat}},{{fuel.lng}}" target="_blank">Link</a></td>     -->              
                </tr> 
              </tbody>
              <tr align="center" ng-if="!errMsg">
                <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 7 : 8 }}" ng-if="fuelMachineData.fuelFills==null&&!fuelMachineData.error" class="err" style="text-align: center;">
                 <h5><?php echo Lang::get('content.no_records_avaiable'); ?></h5>
               </td>
               <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 7 : 8 }}" ng-if="fuelMachineData.fuelFills==null&&fuelMachineData.error" class="err" style="text-align: center;">
                 <h5>{{fuelMachineData.error}}</h5>
               </td>
             </tr>
             <tr align="center" ng-if="errMsg">
              <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 7 : 8 }}" class="err" ><h5>{{errMsg}}</h5></td>
            </tr>
          </table> 

        </tab>

        <tab heading="<?php echo Lang::get('content.fuel_theft'); ?>" active="false">


          <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top: 10px;"> 
            <thead>
              <tr style="text-align:center">
                <th class="id" custom-sort order="'startTime'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.from'); ?> <?php echo Lang::get('content.time'); ?></th>
                <th class="id" custom-sort order="'time'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.to'); ?> <?php echo Lang::get('content.time'); ?></th>
                <th class="id" custom-sort order="'pFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.pre_fuel'); ?></th>
                <th class="id" custom-sort order="'cFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.cur_fuel'); ?></th>  <th class="id" custom-sort order="'lt'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.fuel_theft'); ?></th>
                <th class="id" custom-sort order="'odo'" sort="sort" ng-hide="fuelMachineData.sensorMode == 'DG'" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.odo'); ?></th>
                <th class="id" custom-sort order="'address'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;" ><?php echo Lang::get('content.address'); ?></th>          
                <!--  <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.gmap'); ?></th>   -->                  
              </tr>
            </thead>
            <tbody ng-repeat="fuel in fuelMachineData.fuelDrops | orderBy:natural(sort.sortingOrder):sort.reverse">
              <tr>
                <td>{{fuel.startTime | date:'HH:mm:ss'}}   {{fuel.startTime | date:'dd-MM-yyyy'}}</td>
                <td>{{fuel.time | date:'HH:mm:ss'}}   {{fuel.time | date:'dd-MM-yyyy'}}</td>
                <td>{{fuel.pFuel}}</td>
                <td>{{fuel.cFuel}}</td>                   
                <td>{{roundOffDecimal(fuel.lt)}}</td>
                <td ng-hide="fuelMachineData.sensorMode == 'DG'">{{fuel.odo}}</td>   
                <td ng-if="fuel.address!=null">{{fuel.address}}</td>   
                <td ng-if="fuel.address==null"><a href="https://www.google.com/maps?q=loc:{{fuel.lat}},{{fuel.lng}}" target="_blank">{{fuel.lat}},{{fuel.lng}}</a></td>     
                <!--  <td><a href="https://www.google.com/maps?q=loc:{{fuel.lat}},{{fuel.lng}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>  -->                 
              </tr> 
            </tbody>
            <tr align="center" ng-if="!errMsg">
              <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 7 : 8 }}" ng-if="fuelMachineData.fuelDrops==null&&!fuelMachineData.error" class="err" style="text-align: center;">
               <h5><?php echo Lang::get('content.no_records_avaiable'); ?></h5>
             </td>
             <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 7 : 8 }}" ng-if="fuelMachineData.fuelDrops==null&&fuelMachineData.error" class="err" style="text-align: center;">
               <h5>{{fuelMachineData.error}}</h5>
             </td>
           </tr>
           <tr align="center" ng-if="errMsg">
            <td colspan="{{ fuelMachineData.sensorMode == 'DG'? 7 : 8 }}" class="err" ><h5>{{errMsg}}</h5></td>
          </tr>
        </table> 


      </tab>
      <tab heading="<?php echo Lang::get('content.consumption'); ?> <?php echo Lang::get('content.rate'); ?>" active="false" ng-hide="fuelMachineData.sensorMode=='Dispenser'" ng-click="handleFuelTheftBeta('yes')">


        <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top: 10px;"> 
          <thead class='header' style=" z-index: 1;">
            <tr style="text-align:center">
              <th class="id" custom-sort order="'startTime'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.from'); ?> <?php echo Lang::get('content.time'); ?></th>
              <th class="id" custom-sort order="'endTime'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.to'); ?> <?php echo Lang::get('content.time'); ?></th>
              <th class="id" custom-sort order="'ignitionOnPer'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.ignition'); ?> <?php echo Lang::get('content.on'); ?> %</th>
              <th ng-hide="fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery'" class="id" custom-sort order="'distance'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.distance'); ?> (Kms)</th> 
              <th class="id" custom-sort order="'startFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.pre_fuel'); ?> (Ltrs)</th>
              <th class="id" custom-sort order="'endFuel'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.cur_fuel'); ?> (Ltrs)</th> 
              <th class="id" custom-sort order="'totalConsumption'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.actual_consumption'); ?> (Ltrs)</th>
            </th> 
            <th custom-sort order="'fuelMileage'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;">
              <span ng-if="fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery'"><?php echo Lang::get('content.ltrs_per_hrs'); ?> </span>
              <span ng-if="fuelMachineData.sensorMode != 'DG'||fuelMachineData.sensorMode != 'Machinery'"><?php echo Lang::get('content.kmpl'); ?> </span>
              <th class="id" custom-sort order="'fuelConsume'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.expected_consumption'); ?> (Ltrs)</th>
              <th class="id" custom-sort order="'fuelTheftBeta'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.excess_consumption'); ?> (Ltrs)</th>
              <th class="id" custom-sort order="'startAddress'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.start_loca'); ?></th>
              <th class="id" custom-sort order="'endAddress'" sort="sort" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.end_loca'); ?></th>
            </tr>
          </thead>
          <tbody ng-repeat="fuel in fuelRunningTheft | orderBy:natural(sort.sortingOrder):sort.reverse">
            <tr ng-if="!errMsg.includes('expired')">
              <td>{{fuel.startTime | date:'HH:mm:ss'}}   {{fuel.startTime | date:'dd-MM-yyyy'}}</td>
              <td>{{fuel.endTime | date:'HH:mm:ss'}}   {{fuel.endTime | date:'dd-MM-yyyy'}}</td>
              <td>{{fuel.ignitionOnPer}}</td>
              <td ng-hide="fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery'"> {{fuel.distance }} </td> 
              <td>{{fuel.startFuel}}</td>
              <td>{{fuel.endFuel}}</td> 
              <td> {{fuel.totalConsumption }} </td>
              <td> 
                <span ng-if="fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery'">{{fuel.ltrsphr}}</span>
                <span ng-if="fuelMachineData.sensorMode != 'DG' || fuelMachineData.sensorMode != 'Machinery'">{{fuel.fuelMileage}}</span>
              </td>                 
              <td>{{roundOffDecimal(fuel.fuelConsume)}}</td>
              <td>{{roundOffDecimal(fuel.fuelTheftBeta)}}</td>
              <td ng-if="fuel.startAddress!=null && fuel.startAddress!='-'">{{fuel.startAddress}}</td>
              <td style="text-decoration: underline;" ng-if="fuel.startAddress==null || fuel.startAddress=='-'"><a href="https://www.google.com/maps?q=loc:{{fuel.startLat}},{{fuel.startLng}}" target="_blank">{{fuel.startLat}},{{fuel.startLng}}</a></td> 
              <td ng-if="fuel.endAddress!=null && fuel.endAddress!='-'">{{fuel.endAddress}}</td>
              <td style="text-decoration: underline;" ng-if="fuel.endAddress==null || fuel.endAddress=='-'"><a href="https://www.google.com/maps?q=loc:{{fuel.endLat}},{{fuel.endLng}}" target="_blank">{{fuel.endLat}},{{fuel.endLng}}</a></td> 
            </tr> 

          </tbody>
          <tr ng-show="fuelRunningTheft.length!=0">
            <td colspan="{{(fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery')?10:11}}" style="text-align: right;"> <b><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.excess_consumption'); ?> (Ltrs) </b> </td>
            <td colspan="1"><b>{{roundOffDecimal(totalFuelTheftBeta)}}</b></td>
          </tr>
          <tr align="center" ng-if="!errMsg">
            <td colspan="{{(fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery')?13:14}}" ng-if="(fuelRunningTheft==null||fuelRunningTheft.length==0)&&!fuelMachineData.error" class="err" style="text-align: center;">
             <h5><?php echo Lang::get('content.no_records_avaiable'); ?></h5>
           </td>
           <td colspan="{{(fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery')?13:14}}" ng-if="(fuelRunningTheft==null||fuelRunningTheft.length==0)&&fuelMachineData.error" class="err" style="text-align: center;">
             <h5>{{fuelMachineData.error}}</h5>
           </td>
         </tr>
         <tr align="center" ng-if="errMsg">
          <td colspan="{{(fuelMachineData.sensorMode == 'DG'||fuelMachineData.sensorMode == 'Machinery')?13:14}}" class="err" ><h5>{{errMsg}}</h5></td>
        </tr>
      </table> 


    </tab>

  </tabset>
</div>
</div>

</div>

</div>
</div>

</div>
</div>

</div>
</div>

<script src="assets/js/static.js"></script>
<!--<script src="assets/js/jquery-1.11.0.js"></script>-->
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<!--<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>-->
<!--<script src="assets/js/bootstrap.min.js"></script>-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script data-require="angular-ui-bootstrap@0.11.0" data-semver="0.11.0" src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script>
<script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
<script src="assets/js/angular-translate.js"></script>
<script src="../resources/views/reports/customjs/html5csv.js"></script>   
<script src="../resources/views/reports/customjs/moment.js"></script>
<script src="../resources/views/reports/customjs/FileSaver.js"></script>
<script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
<script src="../resources/views/reports/datatable/jquery.dataTables.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<!--<script src="https://code.highcharts.com/highcharts.js"></script>
  <script src="https://code.highcharts.com/modules/exporting.js"></script>-->
  <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
  <!--<script src="assets/js/naturalSortVersionDates.js"></script>-->
  <script src="assets/js/vamoApp.js"></script>
  <script src="assets/js/services.js"></script>
  <script src="assets/js/fuelMachine.js?v=<?php echo Config::get('app.version');?>"></script>

  <script>
    $(document).ready(function(){
      $('[data-toggle="tooltip"]').tooltip();
    });
    $("#example1").dataTable();
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    }); 

    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });


    var generatePDF = function() {
      // $('.addr').show();
      $('.loc').hide();
      kendo.drawing.drawDOM($("#formConfirmation"),{paperSize: "A4",multiPage: true,margin: {
        left   : "1mm",
        top    : "5mm",
        right  : "1mm",
        bottom : "5mm"
      } ,landscape: true }).then(function(group) {
        kendo.drawing.pdf.saveAs(group, "FuelAnalyticsReport.pdf");
        $('.loc').show();
        // $('.addr').hide();
      });
    }

  </script>

</body>
</html>
