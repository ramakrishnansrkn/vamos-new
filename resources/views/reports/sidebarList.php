<html>
<head>
  <style type="text/css">
    .custom-btn {
      width: 130px;
      height: 40px;
      color: #fff;
      border-radius: 5px;
      padding: 10px 25px;
      font-family: 'Lato', sans-serif;
      font-weight: 500;
      background: transparent;
      cursor: pointer;
      transition: all 0.3s ease;
      position: relative;
      display: inline-block;
      box-shadow:inset 2px 2px 2px 0px rgba(255,255,255,.5),
      7px 7px 20px 0px rgba(0,0,0,.1),
      4px 4px 5px 0px rgba(0,0,0,.1);
      outline: none;
    }

    .btn-12{
      position: relative;
      right: 26px;
      bottom: 20px;
      border:none;
      box-shadow: none;
      width: 130px;
      height: 15px;
      line-height: 42px;
      -webkit-perspective: 230px;
      perspective: 230px;
    }
    .btn-12 span {
      background: rgb(0,172,238);
      background: linear-gradient(0deg, rgba(0,172,238,1) 0%, rgba(2,126,251,1) 100%);
      display: block;
      position: absolute;
      width: 130px;
      height: 40px;
      box-shadow:inset 2px 2px 2px 0px rgba(255,255,255,.5),
      7px 7px 20px 0px rgba(0,0,0,.1),
      4px 4px 5px 0px rgba(0,0,0,.1);
      border-radius: 5px;
      margin:0;
      text-align: center;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
      -webkit-transition: all .3s;
      transition: all .3s;
    }
    .btn-12 span:nth-child(1) {
      color: #fff;
      box-shadow:
      -7px -7px 20px 0px #fff9,
      -4px -4px 5px 0px #fff9,
      7px 7px 20px 0px #0002,
      4px 4px 5px 0px #0001;
      -webkit-transform: rotateX(90deg);
      -moz-transform: rotateX(90deg);
      transform: rotateX(90deg);
      -webkit-transform-origin: 50% 50% -20px;
      -moz-transform-origin: 50% 50% -20px;
      transform-origin: 50% 50% -20px;
    }
    .btn-12 span:nth-child(2) {
      color: #fff;
      -webkit-transform: rotateX(0deg);
      -moz-transform: rotateX(0deg);
      transform: rotateX(0deg);
      -webkit-transform-origin: 50% 50% -20px;
      -moz-transform-origin: 50% 50% -20px;
      transform-origin: 50% 50% -20px;
    }
    .btn-12:hover span:nth-child(1) {
      box-shadow:inset 2px 2px 2px 0px rgba(255,255,255,.5),
      7px 7px 20px 0px rgba(0,0,0,.1),
      4px 4px 5px 0px rgba(0,0,0,.1);
      -webkit-transform: rotateX(0deg);
      -moz-transform: rotateX(0deg);
      transform: rotateX(0deg);
    }
    .btn-12:hover span:nth-child(2) {
      box-shadow:inset 2px 2px 2px 0px rgba(255,255,255,.5),
      7px 7px 20px 0px rgba(0,0,0,.1),
      4px 4px 5px 0px rgba(0,0,0,.1);
      color: transparent;
      -webkit-transform: rotateX(-90deg);
      -moz-transform: rotateX(-90deg);
      transform: rotateX(-90deg);
    }

    @keyframes shine {
      0% {
        transform: translateX(-30px) rotate(-25deg);
      }
      
      100% {
        transform: translateX(100px) rotate(-25deg);
      }
    }

    .shine {
      margin: 1rem;
      color: #FFFFFF;
      
      cursor: pointer;
      border-radius: 5px;
      position: relative;
      /*overflow: hidden;*/
      /*transition: all 100ms linear;*/
    }

    .shine:hover {
      /* transform: scale(1.05) rotate(-2.5deg); */
    }

    .shine::after {
      content: '';
      display: block;
      width: 150%;
      height: 40px;
      background: rgb(255,255,255);
      background: linear-gradient(90deg, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 25%, rgba(255,255,255,1) 50%, rgba(255,255,255,1) 75%, rgba(255,255,255,0) 100%);
      opacity: 0.2;
      position: absolute;
      top: 0px;
      left: 0;
      animation: shine 2s linear infinite;
      transform: translateX(100px) rotate(-25deg);
    }

    .sidebar-subnav >li:nth-child(2){
      padding-top: 0px;
    }


  </style>
  <title>jQuery Function</title>
  <script src = "https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

  <script>
    $(document).ready(function() {
      var dealerName=localStorage.getItem('dealerName');
      if(dealerName=='TECHBITS'){
        document.getElementById("fleet").innerHTML ='';
      }
    });
  </script>
</head>
<body>
 <div id="sidebar-wrapper">
  <ul class="sidebar-nav">
    <li class="sidebar-brand"><a href="javascript:void(0);"><img id="imagesrc" src=""/></i></a></li>
    <li class="track"><a href="../public/track"><div></div><label><?php echo Lang::get('content.track'); ?></label></a></li>
    <!-- <li class="history"><a href="../public/track?maps=replay"><div></div><label>History</label></a></li> -->
    <li class="alert01" ng-if="!reports"><a href="../public/reports" ><div></div><label><?php echo Lang::get('content.reports'); ?></label></a></li>
    <li class="alert01" ng-if="reports"><a href="javascript:void(0);" class="active"><div></div><label><?php echo Lang::get('content.reports'); ?></label></a></li>
    <li class="stastics" ng-if="!stastics"><a href="../public/statistics?ind=1"><div></div><label><?php echo Lang::get('content.statistics'); ?></label></a></li>
    <li class="stastics" ng-if="stastics"><a href="javascript:void(0);" class="active"><div></div><label><?php echo Lang::get('content.statistics'); ?></label></a></li>
    <li class="admin"><a href="../public/settings"><div></div><label><?php echo Lang::get('content.scheduled'); ?></label></a></li>
    <li id="fleet" class="fms" ng-if="!fmsactive"><a href="../public/fleetManagement?fms=FleetManagement" id="fms"><div></div><label><?php echo Lang::get('content.FMS'); ?></label></a></li>
    <li id="fleet" class="fms" ng-if="fmsactive"><a href="../public/fleetManagement?fms=FleetManagement" class="active"><div></div><label><?php echo Lang::get('content.FMS'); ?></label></a></li>
    <li class="ReleaseNotes"><a ng-href="pdfView"><div></div><label style="line-height: 25px;"><?php echo Lang::get('content.release'); ?></br><?php echo Lang::get('content.notes'); ?></label></a></li>
    <li class="logout"><a href="../public/logout"><img src="assets/imgs/logout.png"/></a></li>
  </ul>

  <ul class="sidebar-subnav" style="overflow-y: auto;width: 170px;">
  <!-- <li style="padding-left:25px;">
     <div class="right-inner-addon" align="center"> 
          <select ng-model="multiLang" id="lang" class="form-control" >
                <option value="en">English</option>
                <option value="hi">Hindi</option>
         </select>
    </div>
  </li> -->
  <div style="font-size: 15px;font-weight: bold;color: #fff" class="right-inner-addon proButton" align="center"  >
    <a href="https://gpsvtspro.vamosys.com" class="custom-btn btn-12 shine" target="_blank">
      <span style="font-size: 17px;"><i style="color: #fff;" class="glyphicon glyphicon-send"></i>Click!</span><span style="font-size: 17px;">GPSVTS PRO</span>
    </a>
  </div>
  <li style="padding-left:25px;">
    <div class="right-inner-addon" align="center"> 
      <i class="fa fa-search"></i>
      <input type="search" class="form-control" placeholder="Search" ng-model="searchbox" name="search" />
    </div>
  </li>

  <li ng-repeat="location in vehicle_list track by $index | orderBy:natural('group')" class="active"><a href="javascript:void(0);" ng-click="groupSelection(location.group, location.rowId)">{{trimColon(location.group)}} <span ng-if="location.vehicleLocations.length">({{location.vehicleLocations.length}})</span></a>
    <ul class="nav" style="max-height:400px;overflow-x:hidden;overflow-y:scroll;">
      <li ng-repeat="loc in location.vehicleLocations | orderBy:natural('shortName') | filter:searchbox" data-trigger="hover" ng-class="{active:vehiname ==loc.vehicleId}">
        <a href="javascript:void(0);"  ng-class="{red:loc.status=='OFF'}" ng-click="genericFunction(loc.vehicleId,$index,loc.shortName,loc.position,loc.address,loc.groupName,loc.licenceExpiry,loc.serial1,loc.defaultMileage)" ng-cloak>
         <img ng-if="trvShow==true" ng-src="assets/imgs/trvSideMarker/{{(loc.vehicleType=='Pickup')?'Car':(loc.vehicleType=='PassengerVan')?'Van':loc.vehicleType=='Carrier/Cement' || loc.vehicleType=='Bulker'?'Cement-Mixer':loc.vehicleType}}_{{loc.color}}.png" fall-back-src="assets/imgs/Car.png" width="16" height="16"/>
         <img ng-if="trvShow!=true&&location.fcode!='ETHIOPIA'" ng-hide="vehiImage" ng-src="assets/imgs/sideMarker/{{(loc.vehicleType=='Pickup')?'Car':(loc.vehicleType=='PassengerVan')?'Van':loc.vehicleType=='Carrier/Cement' || loc.vehicleType=='Bulker'?'Cement-Mixer':loc.vehicleType=='Tractor'?'HeavyVehicle':loc.vehicleType}}_{{loc.color}}.png" fall-back-src="assets/imgs/Car.png" width="16" height="16"/>
         <img ng-if="trvShow!=true" ng-show="vehiImage" ng-src="assets/imgs/asset-marker/as_{{loc.color}}.png" fall-back-src="assets/imgs/assetImage.png" width="16" height="16"/>
         <img ng-if="trvShow!='true'" ng-show="vehiImage" ng-src="assets/imgs/asset-marker/as_{{loc.color}}.png" fall-back-src="assets/imgs/assetImage.png" width="16" height="16"/>
         <span  tooltips tooltips-position="right"> {{loc.shortName}} </span>
       </a> 
     </li>
   </ul>
 </li>
</ul>

</div>
</body>
</html>