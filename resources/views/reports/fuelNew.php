<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title><?php echo Lang::get('content.gps'); ?></title>
<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
<link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">

<style>
.empty{
    height: 1px; width: 1px; padding-right: 30px; float: left;
}
.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
background-color: #ffffff;
}

body{
font-family: 'Lato', sans-serif;
/*font-weight: bold;
font-family: 'Lato', sans-serif;
font-family: 'Roboto', sans-serif;
font-family: 'Open Sans', sans-serif;
font-family: 'Raleway', sans-serif;
font-family: 'Faustina', serif;
font-family: 'PT Sans', sans-serif;
font-family: 'Ubuntu', sans-serif;
font-family: 'Droid Sans', sans-serif;
font-family: 'Source Sans Pro', sans-serif;
*/
} 

.col-md-12 {
    width: 98% !important;
    left: 15px !important;
    padding-left: 20px !important;
}

/*
.chart {
    min-width: 320px;
    max-width: 800px;
    height: 280px;
    margin: 0 auto;
}

#container {
    width: 80%;
    height: unset !important; 
    max-width: 80%;
    max-height: unset !important; 
    min-height: 500px !important;
    margin-top: 10px !important;
}
*/
</style>

</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
    <div ng-controller="mainCtrl" class="ng-cloak">
      <div id="wrapper">

        <?php include('sidebarList.php');?> 

        <div id="testLoad"></div>
        
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default"></div>   
            </div>
        </div>
 
    <div class="col-md-12">
       <div class="box box-primary" style="padding-top: 5px;margin-top: -20px;">
        <!-- <div class="row"> -->
                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
                    <h3 class="box-title"><?php echo Lang::get('content.fuel_speed_report'); ?></h3>
                </div>
               <div class="row">
                    <div class="col-md-2" align="center">
                        <div class="form-group" ng-if="shortNam!=undefined || shortNam!=null">
                          <h5 style="color: grey;">{{shortNam}}</h5>
                        </div>
                        <div class="form-group" ng-if="shortNam==undefined || shortNam==null">
                          <h5 style="color: red;"><?php echo Lang::get('content.vehicle_not_found'); ?></h5>
                        </div>
                  </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.fromdate" class="form-control placholdercolor" id="dateFrom"  placeholder="From date">
                                <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.fromtime" class="form-control placholdercolor" id="timeFrom" placeholder="From time">
                                <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.todate" class="form-control placholdercolor" id="dateTo" placeholder="From date">
                                <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.totime" class="form-control placholdercolor" id="timeTo" placeholder="From time">
                                <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                            </div>
                        </div>
                    </div>
       
                    <div class="col-md-1" align="center">
                          <div class="form-group">
                              <div class="input-group datecomp">
                                <select class="input-sm form-control" ng-model="interval">
                                   <option value=""><?php echo Lang::get('content.interval'); ?></option>
                                   <option label="1 mins">1</option>
                                   <option label="2 mins">2</option>
                                   <option label="5 mins">5</option>
                                   <option label="10 mins">10</option>
                                   <option label="15 mins">15</option>
                                   <option label="30 mins">30</option>
                                </select>
                            </div>
                          </div>
                     </div>
                     <div class="col-md-1" align="center">
                        <button style="margin-left: -42%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
                    </div>
                </div>

              <!--  </div> -->
              <div class="row">
              <div class="col-md-1" align="center"></div>
                <div class="col-md-2" align="center">
                  <div class="form-group"></div>
                </div>
              </div>

            </div>
        </div>

        <div class="col-md-12" style="border-color: unset!important;">
            <div class="box box-primary" style="min-height:570px;background-color:#fdfdfd;">
                <div>
                    <div class="pull-right" style="margin-top: 10px;margin-right: 5px;margin-bottom:10px;">
                        <img style="cursor: pointer;" ng-click="exportData('fuelReportNew')"  src="../resources/views/reports/image/xls.png" />
                        <img style="cursor: pointer;" ng-click="exportDataCSV('fuelReportNew')"  src="../resources/views/reports/image/csv.jpeg" />
                    </div>
           
            <div class="box-body">
              
            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>

              <div style="margin-top: 20px;" id="fuelReportNew">
                <table class="table table-bordered table-striped table-condensed table-hover" >
                  <thead>
                    <tr style="text-align:center;">
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.group'); ?></th>
                      <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{trimColon(gName)}}</th>
                      <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.id'); ?></th>
                      <th ng-if="fuelData.vehicleId!=null" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelData.vehicleId}}</th>   
                      <th ng-if="fuelData.vehicleId==null" colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{vehIds}}</th>   
                    </tr>

                <tr style="text-align:center;">
                  <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.from'); ?> <?php echo Lang::get('content.date_time'); ?></th>
                  <th ng-if="fuelData.fromDateTimeUTC!==0" colspan="2" style="background-color:#ecf7fb;font-weight:unset;font-size:12px;">{{fuelData.fromDateTimeUTC | date:'yyyy-MM-dd'}}&nbsp;&nbsp;&nbsp;{{ fuelData.fromDateTimeUTC | date:'HH:mm:ss'}}</th>
                  <th ng-if="fuelData.fromDateTimeUTC==0" colspan="2" style="background-color:#ecf7fb;font-weight:unset;font-size:12px;">{{uiDate.fromdate}}&nbsp;&nbsp;{{uiDate.fromtimes}}</th>
                  <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.to'); ?> <?php echo Lang::get('content.date_time'); ?></th>
                  <th ng-if="fuelData.fromDateTimeUTC!==0" colspan="2" style="background-color:#ecf7fb;font-weight: unset;font-size:12px;">{{fuelData.toDateTimeUTC | date:'yyyy-MM-dd'}}&nbsp;&nbsp;&nbsp;{{fuelData.toDateTimeUTC | date:'HH:mm:ss'}}</th>
                  <th ng-if="fuelData.fromDateTimeUTC==0" colspan="2" style="background-color:#ecf7fb;font-weight: unset;font-size:12px;">{{uiDate.todate}}&nbsp;&nbsp;{{uiDate.totimes}}</th>
                </tr>
                <tr style="text-align:center;">
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#f9f9f9;"><?php echo Lang::get('content.tot_fuel_fill'); ?></th>
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{fuelData.totalFuelFill}}</th>
                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"><?php echo Lang::get('content.tot_fuel_consume'); ?></th>
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">{{fuelData.totalFuelConsume}}</th>
                </tr>
                <tr style="text-align:center;">                   
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.opening_odoreading'); ?></th>
                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelData.openingOdoReading}}</th>
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.closing_odoreading'); ?></th>
                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelData.closingOdoReading}}</th>
                </tr>
                <tr style="text-align:center;">
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.start_fuel'); ?></th>
                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelData.startFuel}}</th>
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.end_fuel'); ?></th>
                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelData.endFuel}}</th>
                </tr>

                <tr style="text-align:center;">
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.odo'); ?> <?php echo Lang::get('content.distance'); ?></th>
                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelData.odoDistance}}</th>
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.tot_trip_distance'); ?> <?php echo Lang::get('content.KMS'); ?></th>
                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{fuelData.tripDistance}}</th>
                </tr>

                  </thead>
              </table>

              <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top: 10px;"> 
                <thead>
                  <tr style="text-align:center">
                    <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.date_time'); ?></th>
                    <th width="10%" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.fuel'); ?> <?php echo Lang::get('content.ltrs'); ?></th>
                    <th width="8%" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.speed'); ?> <?php echo Lang::get('content.Kmph'); ?></th>
                    <th width="8%" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.ign'); ?></th>
                    <th style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.address'); ?></th>
                    <th width="12%" style="text-align:center;background-color:#d3e0f1;font-weight:unset;font-size:13px;"><?php echo Lang::get('content.odo'); ?></th>
                  </tr>
                  </thead>
                  <tbody ng-repeat="fuel in fuelData.history4Mobile" >
                  <tr ng-if="fuel.fuelLitr!='0' && fuel.fuelLitr!='0.0'">
                    <td>{{fuel.dt | date:'HH:mm:ss'}}&nbsp;&nbsp;&nbsp;{{fuel.dt | date:'dd-MM-yyyy'}}</td>
                    <td>{{fuel.fuelLitr}}</td>
                    <td>{{fuel.sp}}</td>
                    <td>{{fuel.ignitionStatus}}</td>
                    <td><p ng-if="fuel.address!='P'">{{fuel.address}}</p><p ng-if="fuel.address=='P'" style="font-weight: bold;font-size: 16px;font-family: 'Ubuntu', sans-serif; ">"</p></td>
                    <td>{{fuel.odoMeterReading}}</td>
                  </tr> 
                </tbody>
                  <tr align="center">
                      <td colspan="6" ng-if="fuelDataMsg == null"  class="err" style="text-align: center;">
                           <h5><?php echo Lang::get('content.zero_records'); ?></h5>
                      </td>
                  </tr>
              </table>   
            </div>

            </div>
           </div>
          
          </div>
        </div>

      </div>
   </div>

    <script src="assets/js/static.js"></script>
<!--<script src="assets/js/jquery-1.11.0.js"></script>-->

   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <!--  <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>-->
<!--<script src="assets/js/bootstrap.min.js"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="assets/js/angular-translate.js"></script>
    <script src="../resources/views/reports/customjs/html5csv.js"></script>
    <script src="../resources/views/reports/customjs/moment.js"></script>
    <script src="../resources/views/reports/customjs/FileSaver.js"></script>
    <script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="../resources/views/reports/datatable/jquery.dataTables.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
<!--<script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>-->

    <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
<!--<script src="assets/js/naturalSortVersionDates.js"></script>-->
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/services.js"></script>
    <script src="../resources/views/reports/customjs/fuelNew.js"></script>
    
    <script>

        $("#example1").dataTable();
          
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        
        $(function () {
                $('#dateFrom, #dateTo').datetimepicker({
                    format:'YYYY-MM-DD',
                    useCurrent:true,
                    pickTime: false,
                    maxDate: new Date,
                    minDate: new Date(2015, 12, 1)
                });
               
                $('#timeFrom').datetimepicker({
                    pickDate: false,
                    
                });
                $('#timeTo').datetimepicker({
                    pickDate: false,
                    
                });
        });      
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });

 </script>
    
 </body>
</html>
