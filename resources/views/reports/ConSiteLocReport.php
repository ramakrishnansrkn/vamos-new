<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title><?php echo Lang::get('content.gps'); ?></title>
  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
  <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
  <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
  <style>

    body{
     font-family: 'Lato', sans-serif;
     /*font-weight: bold;*/ 

 /*font-family: 'Lato', sans-serif;
   font-family: 'Roboto', sans-serif;
   font-family: 'Open Sans', sans-serif;
   font-family: 'Raleway', sans-serif;
   font-family: 'Faustina', serif;
   font-family: 'PT Sans', sans-serif;
   font-family: 'Ubuntu', sans-serif;
   font-family: 'Droid Sans', sans-serif;
   font-family: 'Source Sans Pro', sans-serif;
   */
 } 

 .empty{
  height: 1px; width: 1px; padding-right: 30px; float: left;
}

.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
  background-color: #ffffff;
}

.bgColor{

  background-color: #999999 !important;
  color: dimgrey !important;
  border: solid 0.2px #999999 !important;
  height: 0.2px !important;
}

.bxTop{
  border-top: 2px solid #eceaea;
  margin-top: -20px;
  margin-bottom: 60px;
}

.bxTopss{
  border-top: 2px solid #eceaea;
  margin-top: -8px;
}

.btn {

  padding: 3.5px 8px;
  /* 
    display: inline-block;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: normal;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;*/
  }

  .selCls{
    text-align: center;
    min-width: 98px;
    height: 27px;
    font-size: 12px;
    font-weight: bold;
    border: 1px solid #ccc;
    border-radius:4px;
  }
  td{
    border: 1px solid blue;
  }

</style>
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2X3F316479');
</script>
</head>
<div id="preloader" >
  <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
  <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
  <div ng-controller="mainCtrl" class="ng-cloak">
    <div id="wrapper">

      <?php include('sidebarList.php');?> 

      <div id="testLoad"></div>

       <!-- <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default">
                 
                </div>   
            </div>
          </div>-->

          <div id="page-content-wrapper">

            <div class="col-md-12" style="padding-top: 5px;margin-top: 60px;">
             <div class="box box-primary" >
              <!-- <div class="row"> -->
                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
                  <h3 class="box-title"><span><?php echo Lang::get('content.cons_site_report'); ?></span>
                    <span><?php include('OutOfOrderDataInfo.php');?></span>  
                  </h3>
                </div>
                <div class="row">
                  <div class="col-md-1" align="center"></div>
                  <?php include('dateTime.php');?>

              <!--  <div class="col-md-1" align="center">
                        <div class="form-group">
                            
                                <select class="input-sm form-control" ng-model="interval">
                                     <option value="">Interval</option>
                                     <option label="1 mins">1</option>
                                     <option label="2 mins">2</option>
                                     <option label="5 mins">5</option>
                                     <option label="10 mins">10</option>
                                     <option label="15 mins">15</option>
                                     <option label="30 mins">30</option>
                                </select>
                        </div>
                      </div>  -->

                      <div class="col-md-1" align="center"></div>
                      <div class="col-md-1" align="center">
                        <button style="margin-left: -100%; padding : 5px" ng-click="submitFunction('date')"><?php echo Lang::get('content.submit'); ?></button>
                      </div>
                    </div>
                    <div class="col-md-12" align="right">
                     <?php include('days.php');?>
                   </div>

                   <div class="row">
                    <div class="col-md-1" align="center"></div>

                    <div class="col-md-2" align="center">
                      <div class="form-group">

                      </div>
                    </div>
                  </div>

                  <!--  </div> -->


                </div>
              </div>

              <div class="col-md-12">
                <div class="box bxTop" >

                  <div class="pull-right" style="margin-top: 18px;margin-right: 5px;">

                    <!-- <button type="button" class="btn btn-info" ng-click="durationFilter('month')" ng-disabled="monthDisabled">Month</button>  -->
                    <img style="cursor: pointer;" ng-click="exportData('Consolidated_Site_Report')"  src="../resources/views/reports/image/xls.png" />
                    <img style="cursor: pointer;" ng-click="exportDataCSV('Consolidated_Site_Report')"  src="../resources/views/reports/image/csv.jpeg" />
                    <img style="cursor: pointer;" onclick="generatePDF()"  src="../resources/views/reports/image/Adobe.png" />
                  </div>

                  <div> 
                    <select ng-model="orgIds" ng-change="submitFunction('org')" class="pull-right selCls" style="margin-top: 18px;margin-right: 90px;">
                      <option ng-repeat="route in organsIds" value="{{route}}">{{route}}</option>
                    </select>

                    <span class="pull-right" style="margin-top: 21px;margin-right: 30px;"><?php echo Lang::get('content.organisation_name'); ?> : </span>

                  </div>

                  <div class="form-group pull-right" style="z-index:2;padding-right:15%;margin-top:18px;width:20px;height:6px;cursor:pointer;" ng-dropdown-multiselect="" options="selectVehicleList" selected-model="selectVehicleModel" ng-click="selectVehicle(selectVehicleModel)"  extra-settings="example14settings" translation-texts="example14textss"></div> 

                  <div class="form-group pull-right" style="z-index:2;padding-right:15%;margin-top:18px;width:20px;height:6px;cursor:pointer;" ng-dropdown-multiselect="" options="selectSiteList" selected-model="selectSiteModel" ng-click="selectSite(selectSiteModel)"  extra-settings="example14settings" translation-texts="example14texts"></div> 



                </div>
              </div>

              <div class="row">
                <div class="col-md-1" align="center"></div>
                <div class="col-md-2" align="center">
                 <div class="form-group"></div>
               </div>
             </div>

             <div class="col-md-12">
              <div class="box bxTopss" style="min-height:570px;">
                <div id="Consolidated_Site_Report" style="overflow-x: auto; padding-top: 5px;">
                  <div class="box-body">
                    <div class="empty" align="center"></div>
                    <div class="row" style="padding-top: 20px;"></div>
                    <div id="formConfirmation">
                      <table class="table table-striped table-bordered table-condensed table-hover" style="padding:10px 0 10px 0;">

                        <thead>
                          <col>
                          <colgroup span="2"></colgroup>
                          <colgroup span="2"></colgroup>
                        </col>

                        <tr>
                          <td rowspan="2" ng-show="vehiShow"  style="text-align:center;padding-top:25px;">{{vehiLabel}} <?php echo Lang::get('content.id'); ?></td>
                          <!-- <th colspan="2" scope="colgroup" ng-repeat="master in conSiteLocData.siteList" style="background-color:#d2dff7;">{{master}}</th> -->
                          <th scope="colgroup" colspan="2" ng-repeat="master in conSiteLocData.siteList" style="background-color:#d2dff7;">
                            {{master}}
                          </th>

                        </tr>

                        <tr>

                          <th ng-repeat="i in count" style="background-color:#ecf7fb;font-weight:unset;color:gray;">
                            <div ng-if="$even">
                             <?php echo Lang::get('content.entry'); ?></td>
                           </div>
                           <div ng-if="$odd">
                            <?php echo Lang::get('content.exit'); ?></td>
                          </div></th>


                        </tr>

                      </thead> 

                      <tbody>

                        <tr ng-if="!conSiteError" ng-repeat="data in conSiteLocData.getDetails" >

                          <td style="text-align:center;border-right: 1px solid darkgray;">
                            {{data.vehicleId}}
                          </td>


                           <!-- <td  ng-repeat-start="(key,value) in data.hist" >
                                <div ng-repeat="dat in value" style="padding-bottom:0px !important;padding-right: 10px;">
                                   <span ng-if="dat.startTime!=0" class="fontStyle garage-title" style="color:#557fa8;">{{dat.startTime | date:'dd-MMM-yyyy'}} </span> 
                                   <span ng-if="dat.startTime!=0" class="fontStyle garage-title" style="color:#557fa8;">{{dat.startTime | date:'HH:mm:ss'}}<hr> </span> 
                                
                                   <span ng-if="dat.startTime==0" style="text-align: center;" class="err"> Entry Time </span> 
                                   <span ng-if="dat.startTime==0" style="text-align: center;" class="err"> not found <hr> </span> 

                                   <span class="fontStyle garage-title" style="color:#e18e32;">{{dat.siteEntryTemp}}<hr class="bgColor"> </span> 
                            
                                </div>   
                              </td> -->

                              <td  colspan="2"  ng-repeat="master in conSiteLocData.siteList" style="padding-bottom:0px !important;padding-right: 10px;">
                                <div ng-repeat="(key,value) in data.hist">
                                  <div ng-repeat="dat in value" style="display: flex; justify-content: center;" >
                                    <div style="width: 60%;padding:5px">  
                                      <span ng-if="dat.siteName==master"class="fontStyle garage-title">
                                        <p ng-if="dat.startTime!=0 " style="color:#557fa8;">{{dat.startTime | date:'HH:mm:ss dd-MM-yyyy'}}</p>
                                        <p ng-if="dat.startTime==0" class="err" > <?php echo Lang::get('content.entry'); ?> <?php echo Lang::get('content.time'); ?> <br><?php echo Lang::get('content.not_found'); ?> </p> 
                                        <p style="color:#e18e32;" ng-if="temperature !='no'">{{dat.siteEntryTemp}}&#176;C</p>
                                        <hr ng-if="!$last" class="bgColor">
                                      </span>
                                    </div>
                                    <div ng-if="dat.siteName==master" style="width: 40%;padding: 5px">
                                      <span class="fontStyle garage-title">
                                        <p ng-if="dat.endTime!=0 " style="color:#557fa8;">{{dat.endTime | date:'HH:mm:ss dd-MM-yyyy'}}</p>
                                        <p ng-if="dat.endTime==0 " class="err"> <?php echo Lang::get('content.exit'); ?> <?php echo Lang::get('content.time'); ?><br> <?php echo Lang::get('content.not_found'); ?> </p> 
                                        <p style="color:#e18e32;" ng-if="temperature !='no'">{{dat.siteExitTemp}}&#176;C</p>
                                        <hr ng-if="!$last" class="bgColor">
                                      </span>
                                    </div>
                                  </div>
                                  
                                </div>   
                              </td>

                                <!-- <td ng-repeat="i in count" >
                                   
                                        <div ng-if="$even">
                                            <div ng-repeat="(key,value) in data.hist">
                                                <div ng-repeat="dat in value" style="padding-bottom:0px !important;padding-right: 10px;">
                                                    <div >
                                                     <span ng-if="dat.startTime!=0" class="fontStyle garage-title" style="color:#557fa8;">{{dat.startTime | date:'HH:mm:ss'}} </span> 
                                                     <span ng-if="dat.startTime!=0" class="fontStyle garage-title" style="color:#557fa8;">{{dat.startTime | date:'dd-MM-yyyy'}}<hr> </span> 
                                                     <span ng-if="dat.startTime==0" style="text-align: center;" class="err"> <?php echo Lang::get('content.entry'); ?> <?php echo Lang::get('content.time'); ?> </span> 
                                                     <span ng-if="dat.startTime==0" style="text-align: center;" class="err"> <?php echo Lang::get('content.not_found'); ?> <hr> </span> 

                                                     <span class="fontStyle garage-title" style="color:#e18e32;">{{dat.siteEntryTemp}}<hr class="bgColor"> </span> 
                                                 </div>

                                                 </div>   
                                            </div>
                                        </div>
                                        <div ng-if="$odd">
                                            kkkk
                                        </div>
                                   
                                      </td> -->

                            <!-- <td  ng-repeat="(key,value) in data.hist" >

                                <div ng-repeat="dat in value" style="padding-bottom:0px !important;padding-right: 10px;">
                                 <span ng-if="dat.startTime!=0" class="fontStyle garage-title" style="color:#557fa8;">{{dat.startTime | date:'HH:mm:ss'}} </span> 
                                 <span ng-if="dat.startTime!=0" class="fontStyle garage-title" style="color:#557fa8;">{{dat.startTime | date:'dd-MM-yyyy'}}<hr> </span> 
                                 <span ng-if="dat.startTime==0" style="text-align: center;" class="err"> <?php echo Lang::get('content.entry'); ?> <?php echo Lang::get('content.time'); ?> </span> 
                                 <span ng-if="dat.startTime==0" style="text-align: center;" class="err"> <?php echo Lang::get('content.not_found'); ?> <hr> </span> 

                                 <span class="fontStyle garage-title" style="color:#e18e32;">{{dat.siteEntryTemp}}<hr class="bgColor"> </span> 

                             </div>   
                           </td> -->



                            <!--  <td  ng-repeat-end="(key,value) in data.hist" style="border-right: 1px solid darkgray;">
                                <div ng-repeat="dat in value" style="padding-bottom:0px !important;padding-right: 10px;">
                                  <span ng-if="dat.endTime!=0" class="fontStyle garage-title" style="color:#557fa8;">{{dat.endTime | date:'dd-MMM-yyyy'}} </span> 
                                  <span ng-if="dat.endTime!=0" class="fontStyle garage-title" style="color:#557fa8;">{{dat.endTime | date:'HH:mm:ss'}} <hr> </span> 
                                  
                                  <span ng-if="dat.endTime==0" style="text-align: center;" class="err"> Exit Time </span> 
                                  <span ng-if="dat.endTime==0" style="text-align: center;" class="err"> not found <hr> </span> 

                                  <span class="fontStyle garage-title" style="color:#e18e32;"> {{dat.siteExitTemp}}<hr class="bgColor"> </span> 
                         
                                </div>  
                              </td> -->
                            </tr>


                          </tbody>

                          <tr ng-if="!conSiteError" ng-hide="vehiShow">
                           <td colspan="35" ng-if="conSiteLocData.getDetails==null || conSiteLocData.getDetails.length==0 || conSiteLocData.getDetails==''" style="padding: 25px;margin-top:20px;font-size:14px;" class="err"><?php echo Lang::get('content.no_data_found'); ?></td>
                         </tr>
                         <tr ng-if="conSiteError" class="ng-scope" style="text-align: center">
                          <td colspan="22" class="err"><h5>{{conSiteError}}</h5></td>
                        </tr>

                      </table>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

      <script src="assets/js/static.js"></script>
      <script src="assets/js/jquery-1.11.0.js"></script>
      <script src="assets/js/bootstrap.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
      <script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
      <script src="assets/js/angular-translate.js"></script>
      <script src="../resources/views/reports/customjs/html5csv.js"></script>
      <script src="../resources/views/reports/customjs/moment.js"></script>
      <script src="../resources/views/reports/customjs/FileSaver.js"></script>
      <script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>

      <script src="assets/js/underscore.js"></script>
      <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
      <!-- <script src="assets/js/naturalSortVersionDates.js"></script> -->
      <script src="assets/js/vamoApp.js"></script>
      <script src="assets/js/services.js"></script>
      <script src="assets/js/ConSiteLocReport.js?v=<?php echo Config::get('app.version');?>"></script>

      <script>


        $("#menu-toggle").click(function(e) {
          e.preventDefault();
          $("#wrapper").toggleClass("toggled");
        });

      //  $("#datepicker").datepicker({ maxDate: new Date, minDate: new Date(2007, 6, 12) });
      $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
      });
      var generatePDF = function() {
        kendo.drawing.drawDOM($("#formConfirmation"),{paperSize: "A4",multiPage: true,landscape: true }).then(function(group) {
          kendo.drawing.pdf.saveAs(group, "ConsolidateSiteReport.pdf");
        });
      }
    </script>

  </body>
  </html>
