var getIP = globalIP;
//var app = angular.module('hist',['ui.bootstrap']);

app.controller('histCtrl',['$scope', '$http', '$filter', '_global','$translate', function($scope, $http, $filter, GLOBAL,$translate) {
  var language=localStorage.getItem('lang');
  $scope.multiLang=language;
  $translate.use(language);
  var translate = $filter('translate');
  var assLabel          =  localStorage.getItem('isAssetUser');
  $scope.vehiAssetView  =  true;
  $scope.trvShow        =  localStorage.getItem('trackNovateView');
  $scope.dealerName     =  localStorage.getItem('dealerName');
  $scope.filterDuration=240000;
  $scope.dealerFilter   =  false;  
  $scope.tapchanged=true;
  $scope.vehicleMode = "";
  var expiryDays="";
  if($scope.dealerName == 'FUELVIEW') {
   $scope.dealerFilter = true;  
 }
 $scope.tn       =  getParameterByName('tn'); 
 $scope.vehicleId =getParameterByName('vid');
 var licenceExpiry="";
 var strExpired='expired';
 $scope.todaymillisec=new Date().getTime();
 if(localStorage.getItem('licenceExpiry'))licenceExpiry=localStorage.getItem('licenceExpiry');
//alert( localStorage.getItem('dealerName') );
console.log('Dealer : '+$scope.dealerName);

if( assLabel == "true" ) {
  $scope.vehiLabel      =  "Asset";
  $scope.vehiImage      =  true;
  $scope.vehiAssetView  =  false;
} else if( assLabel == "false" ) {
  $scope.vehiLabel      =  "Vehicle";
  $scope.vehiImage      =  false;
  $scope.vehiAssetView  =  true;
} else {
  $scope.vehiLabel      =  "Vehicle";
  $scope.vehiImage      =  false;
  $scope.vehiAssetView  =  true;
}

$scope.ignDurTot      =  "0h : 0m : 0s"; 
$scope.reportBanShow  =  false;
$scope.reportUrl      =  GLOBAL.DOMAIN_NAME+'/getReportsList';


$scope.$watch("reportUrl", function (val) {

  $http.get($scope.reportUrl).success(function(data) {

   var tabShow = data;
      //console.log(tabShow);
      if(tabShow != "" && tabShow != null)  {     
     // console.log('not Empty getReportList API ...');
     angular.forEach(tabShow,function(val, key){

      var newReportName = Object.getOwnPropertyNames(val).sort();

      if(newReportName == 'Analytics_IndivdiualReportsList'){
        if(val.Analytics_IndivdiualReportsList[0].IDLE==false&&val.Analytics_IndivdiualReportsList[0].PARKED==false&&val.Analytics_IndivdiualReportsList[0].STOPPAGE==false&&val.Analytics_IndivdiualReportsList[0].MOVEMENT==false){

          $scope.reportBanShow = true;
          $(window).load(function(){
            $('#allReport').modal('show');
          });

          stopLoading();

        } else {

          var reportSubName = Object.getOwnPropertyNames(val.Analytics_IndivdiualReportsList[0]).sort();
                  //console.log(reportSubName);

                  angular.forEach(reportSubName,function(value, keys) {
                //console.log(value);
                switch(value){
                    /* case 'AC':
                        //if(val.Analytics_IndivdiualReportsList[0].AC==false){$scope.tabAcreportShow = false; }
                        $scope.tabAcreportShow = val.Analytics_IndivdiualReportsList[0].AC;
                        break; */
                        case 'OVERSPEED':
                        //if(val.Analytics_IndivdiualReportsList[0].OVERSPEED==false){ $scope.tabOverspeedShow = false;  }
                        $scope.tabOverspeedShow = val.Analytics_IndivdiualReportsList[0].OVERSPEED;
                        break;
                       // case 'FUEL':
                       //  //if(val.Analytics_IndivdiualReportsList[0].FUEL==false){ $scope.tabFuelShow   = false;  }
                       //  $scope.tabFuelShow = val.Analytics_IndivdiualReportsList[0].FUEL;
                       // break;
                       case 'IDLE':
                        //if(val.Analytics_IndivdiualReportsList[0].IDLE==false){ $scope.tabIdleShow  = false; }
                        $scope.tabIdleShow  = val.Analytics_IndivdiualReportsList[0].IDLE;
                        break;
                        case 'PARKED':
                        //if(val.Analytics_IndivdiualReportsList[0].PARKED==false){$scope.tabParkedShow  = false; }
                        $scope.tabParkedShow = val.Analytics_IndivdiualReportsList[0].PARKED; 
                        break;
                        case 'STOPPAGE':
                        //if(val.Analytics_IndivdiualReportsList[0].STOPPAGE==false){ $scope.tabStopShow = false; }
                        $scope.tabStopShow = val.Analytics_IndivdiualReportsList[0].STOPPAGE;
                        break;
                        case 'MOVEMENT':
                        //if(val.Analytics_IndivdiualReportsList[0].MOVEMENT==false){ $scope.tabMovementShow = false;}
                        $scope.tabMovementShow = val.Analytics_IndivdiualReportsList[0].MOVEMENT;
                        break;
                        case 'IGNITION':
                        //if(val.Analytics_IndivdiualReportsList[0].IGNITION==false){ $scope.tabIgnitionShow = false; }
                        $scope.tabIgnitionShow = val.Analytics_IndivdiualReportsList[0].IGNITION;
                        break;
                      }
                    });
                }
              }
            });

   } else {
    console.log('Empty getReportList API ...');

    $scope.tabStopShow        =  true;
    $scope.tabMovementShow    =  true;
    $scope.tabNoDataShow      =  true;
    //$scope.tabAcreportShow    =  true;
    $scope.tabIgnitionShow    =  true;
      //$scope.tabFuelShow        =  true;
      $scope.tabIdleShow        =  true;
      $scope.tabParkedShow      =  true;
      $scope.tabOverspeedShow   =  true; 
    }
  })
  .error(function(data, status) {
    console.error('Repos error', status, data);
    console.log('Empty getReportList API ...');

    $scope.tabStopShow        =  true;
    $scope.tabMovementShow    =  true;
    $scope.tabNoDataShow      =  true;
    //$scope.tabAcreportShow    =  true;
    $scope.tabIgnitionShow    =  true;
      //$scope.tabFuelShow        =  true;
      $scope.tabIdleShow        =  true;
      $scope.tabParkedShow      =  true;
      $scope.tabOverspeedShow   =  true; 
    });

});


$scope.overallEnable     =   true;
$scope.tabactive         =   true;
$scope.cardData          =   false;
$scope.showErrMsg        =   false;
$scope.oaddress          =   [];
$scope.maddress          =   [];
$scope.maddress1         =   [];
$scope.saddress          =   [];
$scope.addressIdle       =   [];
$scope.addressEvent      =   [];
$scope.saddressStop      =   [];
$scope.eventReportData   =   [];
$scope.addressLoad       =   [];
$scope.addressFuel       =   [];
//$scope.location          =   [];
$scope.ltrs              =   [];
$scope.fuelDate          =   [];
$scope.gIndex            =   0;

$scope.interval          =   getParameterByName('interval')?getParameterByName('interval'):"";
$scope.sort              =   sortByDate('date');

//$scope.itemsPerPage      =   5;
//$scope.currentPage       =   0;
//$scope.items             =   [];

$scope.filteredTodos     =   [];
$scope.itemsPerPage      =   10;
$scope.currentPage       =   1;

/*$scope.gap               =   5;
  $scope.itemsPerPage      =   5;
  $scope.currentPage       =   0;*/
  
  $scope.time_stop         =   'All';
  $scope.time_park         =   'All';

//$scope.timeDrops=[{'timeId' :'Above 5 mins' },{'timeId' :'Above 10 mins' },{'timeId' :'Above 15 mins' },{'timeId' :'Above 30 mins' },{'timeId' :'Above 1 hrs' }];
//console.log($scope.timeChange);

function sessionValue(vid, gname,licenceExpiry){
  localStorage.setItem('user', JSON.stringify(vid+','+gname));
  localStorage.setItem('licenceExpiry', licenceExpiry);
  $("#testLoad").load("../public/menu");
}

$("#testLoad").load("../public/menu");
  // $scope.url = 'http://'+getIP+context+'/public//getVehicleLocations';
  $scope.url = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');

  $scope.$watch("url", function (val) {

    $http.get($scope.url).success(function(data) {

      //$scope.locations        =  data;
      $scope.vehicle_list     =  data;
      $scope.vehiname         =  getParameterByName('vid');
      $scope.uiGroup          =  $scope.trimColon(getParameterByName('vg'));
      $scope.gName            =  getParameterByName('vg');  

        //alert($scope.vehiname);

        if($scope.vehiname=='undefined' || $scope.vehiname==''){
          $scope.vehiname  =  data[$scope.gIndex].vehicleLocations[0].vehicleId; 
          licenceExpiry=data[$scope.gIndex].vehicleLocations[0].licenceExpiry;
          $scope.vehicleMode=data[$scope.gIndex].vehicleLocations[0].vehicleMode;
          expiryDays=data[$scope.gIndex].vehicleLocations[0].expiryDays;
          //alert($scope.vehiname );
        } 

        if($scope.gName=='undefined' || $scope.gName==''){
          $scope.gName  =  data[$scope.gIndex].group; 
        } 

        angular.forEach(data, function(val, key) {

          if($scope.gName == val.group) {

            $scope.gIndex = val.rowId;       

            angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys) {

              if($scope.vehiname == value.vehicleId) {

                if(value.position == "N" || value.position == "Z"){
                 alert('Vehicle '+value.vehicleId+'is not synced!');
               }

               $scope.shortNam = value.shortName;
               licenceExpiry=value.licenceExpiry;
               $scope.vehicleMode=value.vehicleMode;
               expiryDays=value.expiryDays;
             }

           });
          }

        });



        sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
        var notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
        $('#notncount').text(notncount);
        window.localStorage.setItem('totalNotifications',notncount);

      // if(data.length){
      //  $scope.vehiname   = data[0].vehicleLocations[0].vehicleId;
      //  $scope.gName    =   data[0].group; 
      //  // sessionValue($scope.vehiname, $scope.gName);
      //  angular.forEach(data, function(value, key) {
      //      if(value.totalVehicles) {
      //        $scope.data1    = data[key];
      //      }
      //  });   
      // }
      initial(prodId);
    }).error(function(){ /*alert('error'); */ });
  });


  $scope.downloadid    =  'movementreport';
  var prodId           =  getParameterByName('vid');
  var tabId            =  getParameterByName('tn');
  $scope.tab_val       =  getParameterByName('tn');
  $scope.vgroup        =  getParameterByName('vg');
  $scope.dvgroup       =  getParameterByName('dvg');
  $scope.vvid          =  getParameterByName('vvid');
  $scope.repId         =  getParameterByName('rid');
  $scope.fd            =  getParameterByName('fd');
  $scope.ft            =  getParameterByName('ft');
  $scope.td            =  getParameterByName('td');
  $scope.tt            =  getParameterByName('tt');
  var ignitionValue    =  [];
  $scope.todayhistory  =  [];

  $scope.trimColon = function(textVal){

    var splitVal =[];
    
    if(textVal!=undefined){

     splitVal = textVal.split(/[:]+/);

   }else{
     splitVal[0]='No Value';
   }
   return splitVal[0];
 }

 function formatAMPM(date) {
  var date = new Date(date);
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? 'PM' : 'AM';
  hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
      return strTime;
    }

    function changeTimeFormat(time_str) {
    //console.log(time_str);
    var str   = time_str.split(' ');
    var stradd  = str[0].concat(":00");
    var strAMPM = stradd.concat(' '+str[1]);
    var time = strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
    var hours = Number(time[1]);
    var minutes = Number(time[2]);
    var seconds = Number(time[2]);
    var meridian = time[4].toLowerCase();

    if (meridian == 'p' && hours < 12) {
      hours = hours + 12;
    }
    else if (meridian == 'a' && hours == 12) {
      hours = hours - 12;
    }     
    var marktimestr =''+hours+':'+minutes+':'+seconds;      
    return marktimestr;
  };


  $scope.convert_to_24hrs = function(time_str) {

   if(time_str != undefined){

    var str   = time_str.split(' ');
    var stradd  = str[0].concat(":00");
    var strAMPM = stradd.concat(' '+str[1]);
    var time = strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
    var hours = Number(time[1]);
    var minutes = Number(time[2]);
    var seconds = Number(time[2]);
    var meridian = time[4].toLowerCase();

    if (meridian == 'p' && hours < 12) {
      hours = hours + 12;
    }
    else if (meridian == 'a' && hours == 12) {
      hours = hours - 12;
    } 

    hours   = hours < 10 ? '0'+hours : hours;
    minutes = minutes < 10 ? '0'+minutes : minutes;  
    seconds = seconds < 10 ? '0'+seconds : seconds; 

    var marktimestr = ''+hours+':'+minutes+':'+seconds;   
  }

  return marktimestr;
};

function eventButton(eventdate)
{
  $scope.buttonClick = eventdate;
  serviceCallEvent();
}

function serviceCallEvent()
{
  var stoppage        =   document.getElementById ("stop").checked
  var idleEvent       =   document.getElementById ("idle").checked;
  var notReachable    =   document.getElementById ("notreach").checked;
  var overspeedEvent  =   document.getElementById ("overspeed").checked;
  var stopMints       =   document.getElementById ("stop1").value;
  var idleMints       =   document.getElementById ("idle1").value;
  var notReachMints   =   document.getElementById ("notreach1").value;
  var speedEvent      =   document.getElementById ("overspeed1").value
  var locEvent        =   document.getElementById ("location").checked;
  var siteEvent       =   document.getElementById ("site").checked;
  var urlEvent        =   GLOBAL.DOMAIN_NAME+"/getActionReport?vehicleId="+prodId+"&fromDate="+$scope.fromdate+"&fromTime="+convert_to_24h($scope.fromtime)+"&toDate="+$scope.todate+"&toTime="+convert_to_24h($scope.totime)+"&interval="+$scope.interval+"&stoppage="+stoppage+"&stopMints="+stopMints+"&idle="+idleEvent+"&idleMints="+idleMints+"&notReachable="+notReachable+"&notReachableMints="+notReachMints+"&overspeed="+overspeedEvent+"&speed="+speedEvent+"&location="+locEvent+"&site="+siteEvent;
      //console.log(' inside the method '+ urlEvent)

      $http.get(urlEvent).success(function(eventRes){
        $scope.eventReportData    = eventRes;
   //     $('#status').fadeOut(); 
      // $('#preloader').delay(350).fadeOut('slow');
      stopLoading();
      if($scope.buttonClick==true)
      {
        $scope.alertMe_click($scope.downloadid);
        
      }

    })
      
    }

  //for individual method for event report service call
  
  $scope.eventCall    =     function()
  {
    document.getElementById ("stop").checked        = true;
    document.getElementById ("idle").checked        = true;
    document.getElementById ("notreach").checked      = true;
    document.getElementById ("overspeed").checked     = true;
    document.getElementById ("stop1").defaultValue      = 10;
    document.getElementById ("idle1").defaultValue      = 10;
    document.getElementById ("notreach1").defaultValue    = 10;
    document.getElementById ("overspeed1").defaultValue   = 60;
    document.getElementById ("location").checked      = false;
    document.getElementById ("site").checked        = false;
      //buttonClick = false;
      serviceCallEvent();
      // return $scope.eventReportData;
    }

    //site web service
    $scope.siteCall       =     function()
    {
      var url =GLOBAL.DOMAIN_NAME+"/getSiteReport?vehicleId="+prodId+"&fromDate="+$scope.fromdate+"&fromTime="+convert_to_24h($scope.fromtime)+"&toDate="+$scope.todate+"&toTime="+convert_to_24h($scope.totime)+"&interval="+$scope.interval+"&site=true";
      // console.log(' asd '+url);
      $http.get(url).success(function(siteval){
        $scope.siteReport=[];
        $scope.siteReport = siteval;
   //     $('#status').fadeOut(); 
      // $('#preloader').delay(350).fadeOut('slow');
      stopLoading();
    });
    }

    
    $scope.$watch("tab_val", function (newval, oldval) {
    // $scope.$watch("tabId", function (val) {

   /* $scope.tabStopShow      = true;
      $scope.tabMovementShow  = true;
      $scope.tabAcreportShow  = true;
      $scope.tabIgnitionShow  = true;
      $scope.tabFuelShow      = true;
      $scope.tabIdleShow      = true;
      $scope.tabParkedShow    = true;
      $scope.tabOverspeedShow = true;*/
      $scope.tabAll         =  false;
      $scope.tabNodata      =  false;
      $scope.tabmovement    =  false;
      $scope.taboverspeed   =  false;
      $scope.tabparked      =  false;
      $scope.tabidle        =  false;
      $scope.tabevent       =  false;
      $scope.tabsite        =  false;
      $scope.tabload        =  false;
      $scope.tabfuel        =  false;
      $scope.tabignition    =  false;
   // $scope.tabac          =  false;
   $scope.tabStop        =  false;

   switch(tabId){
    case 'all':
    $scope.tabAll         = true; 
    break;
    case 'movement':
    $scope.tabmovement    = true; 
    break;
    case 'overspeed':
    $scope.taboverspeed   = true;
    break;
    case 'parked':
    $scope.tabparked      = true;
    break;
    case 'idle':
    $scope.tabidle        = true;
    break;
    case 'noData':
    $scope.tabNoData      = true;
    break;
    case 'event':
    $scope.tabevent       = true;
    break;
    case 'site':
    $scope.tabsite        = true;
    break;
    case 'load':
    $scope.tabload        = true;
    break;
    case 'fuel':
    $scope.tabfuel        = true;
    break;
    case 'ignition':
    $scope.tabignition     = true;
    break;
     /* case 'acreport':
          $scope.tabac           = true;
          break;*/
          case 'stoppageReport':
          $scope.tabStop        = true;
          break;
          default:
          $scope.tabmovement    = true; 
          break;
        }
      }, true);

    function getTodayDatess() {
      var date = new Date();
      return date.getFullYear()+'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+("0" + (date.getDate())).slice(-2);
    };
    
    function initial(){
      console.log(' new  '+prodId)
      startLoading();

      $scope.id = prodId;


      $scope.fromTimes = localStorage.getItem('fromTime');
      $scope.fromDates = localStorage.getItem('fromDate');
      $scope.toTimes       = localStorage.getItem('toTime');
      $scope.toDates       = localStorage.getItem('toDate');
      if(localStorage.getItem('timeTochange')!='yes'){
        updateToTime();
        $scope.toTimes   =   localStorage.getItem('toTime');
      }

      var ignurl =  GLOBAL.DOMAIN_NAME+'/getPrimaryEngineReport?vehicleId='+prodId+'&fromDateTime='+utcFormat($scope.fromDates,changeTimeFormat($scope.fromTimes))+'&toDateTime='+utcFormat($scope.toDates,changeTimeFormat($scope.toTimes));

      var histurl = GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+prodId+"&interval="+$scope.interval+'&fromDateUTC='+utcFormat($scope.fromDates,changeTimeFormat($scope.fromTimes))+'&toDateUTC='+utcFormat($scope.toDates,changeTimeFormat($scope.toTimes));



      // $scope.fromTimes = "00:00:00";
      // $scope.fromDates = getTodayDatess();

      //    var histurl = GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+prodId+"&interval="+$scope.interval+'&fromDateUTC='+utcFormat($scope.fromDates,$scope.fromTimes);
      // var histurl = GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+prodId+'&fromDateUTC='+utcFormat($scope.fromDates,$scope.fromTimes);
      // var histurl = GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+prodId+"&interval="+$scope.interval+'&fromDateUTC='+utcFormat($scope.fromDates,$scope.fromTimes);
      // var histurl = GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+getParameterByName('vid')+"&interval="+$scope.interval;
      $scope.registerNo='';
      $scope.loading  = true;
      $scope.historyError='';
      var expdate=moment().add(expiryDays,'days').format('DD-MM-YYYY'),
      convertedexpdate=utcFormat(expdate,changeTimeFormat('11:59 PM')),
      convertedtodate=utcFormat($scope.toDates,changeTimeFormat($scope.toTimes)),
      convertedfromdate=utcFormat($scope.fromDates,changeTimeFormat($scope.fromTimes));
      if(convertedtodate<=convertedexpdate && convertedfromdate<=convertedexpdate){
        $http.get(ignurl).success(function(data){

         if(data=='Failed'){
          $scope.historyError =  translate("curl_error");
          stopLoading();
        }
        else {
          $scope.dataignition       =  [];
          var ignitionVal   =  [];
          $scope.dataignition2      =  [];
          $scope.dataignition2   =  data;
          if(data.getEngineData!=null){
           ignitionVal    =  ($filter('filter')(data.getEngineData, {'ignitionStatus': "!null"}));
           $scope.dataignition    = filterIgnitionStatus(ignitionVal,$scope.toDates,$scope.toTimes);
         }
       } 
     });
      }
      else{
        $scope.errMsg=licenceExpiry;
        $scope.showErrMsg = true;
        stopLoading();
      } 

      try{
        $scope.historyError='';
        if(convertedtodate<=convertedexpdate && convertedfromdate<=convertedexpdate){

          $http.get(histurl).success(function(data){
           if(data=='Failed'){
            $scope.historyError =  translate("curl_error");
            stopLoading();
          }
        //console.log(data.error);
        else{
          $scope.registerNo=data.regNo;
          $scope.errMsg=data.error;
          $scope.showErrMsg = true;

          if(data.vehicleLocations != null) {

            $scope.loading        =  false;
            $scope.hist           =  data; 
            $scope.cardData       =  true;
            $scope.filterLoc      =  data.vehicleLocations;  
            $scope.topspeedtime   =  data.topSpeedTime;
        //$scope.tankSize       =  parseInt(data.tankValue);
        $scope.tankSize       =  data.tankValue;

        //$scope.dataGeofence(data.gfTrip);   
        var fromNow         = new Date(data.fromDateTime.replace('IST',''));
        var toNow           = new Date(data.toDateTime.replace('IST',''));
        $scope.fromNowTS    = data.fromDateTimeUTC;
        $scope.toNowTS      = data.toDateTimeUTC; 
        $scope.fromtime     = formatAMPM($scope.fromNowTS);
        $scope.totime       = formatAMPM($scope.toNowTS);
        $scope.fromdate     = getTodayDate($scope.fromNowTS);
        $scope.todate       = getTodayDate($scope.toNowTS);

        $scope.toTimeVal = utcFormat($scope.todate,convert_to_24h($scope.totime));

        $scope.dataArray(data.vehicleLocations);
        stopLoading();

      } else {

       //    var curDates        = new Date();   
       //    $scope.fromtime     = '12:00 AM';
       // // $scope.totime       = formatAMPM($scope.toNowTS);
       //    $scope.totime       = formatAMPM(curDates);
       //    $scope.fromdate     = getTodayDatess();
       //    $scope.todate       = getTodayDatess();
       $scope.fromtime     = localStorage.getItem('fromTime');
       $scope.totime       = localStorage.getItem('toTime');
       $scope.fromdate     = localStorage.getItem('fromDate');
       $scope.todate       = localStorage.getItem('toDate');
       if(localStorage.getItem('timeTochange')!='yes'){
        updateToTime();
        $scope.totime    =   localStorage.getItem('toTime');
      }

      $scope.toTimeVal = utcFormat($scope.todate,convert_to_24h($scope.totime));
      stopLoading();
    }
        // $scope.eventCall();
        // $scope.siteCall();
      }

    });
        }else{

          $scope.fromtime     = localStorage.getItem('fromTime');
          $scope.totime       = localStorage.getItem('toTime');
          $scope.fromdate     = localStorage.getItem('fromDate');
          $scope.todate       = localStorage.getItem('toDate');
          if(localStorage.getItem('timeTochange')!='yes'){
            updateToTime();
            $scope.totime    =   localStorage.getItem('toTime');
          }

          $scope.toTimeVal = utcFormat($scope.todate,convert_to_24h($scope.totime));
          $scope.errMsg=licenceExpiry;
          $scope.showErrMsg = true;
          stopLoading();
        } 
      }catch (errr){
      // $('#status').fadeOut(); 
      // $('#preloader').delay(350).fadeOut('slow');  
      console.log(' error '+errr);
      stopLoading();
    }  
  };

  function showIgnitionData(){

    startLoading();
    var ignitionurl =  GLOBAL.DOMAIN_NAME+'/getPrimaryEngineReport?vehicleId='+prodId+'&fromDateTime='+utcFormat($scope.fromdate,changeTimeFormat($scope.fromtime))+'&toDateTime='+utcFormat($scope.todate,changeTimeFormat($scope.totime));


    
    $scope.loading  = true;
    try{
      $scope.historyError='';
      $scope.dataignition       =  [];
      var ignitionVal   =  [];
      $scope.dataignition2      =  [];
      if(convertedtodate<=convertedexpdate && convertedfromdate<=convertedexpdate){

        $http.get(ignitionurl).success(function(data){

         if(data=='Failed'){
          $scope.historyError =  translate("curl_error");
          stopLoading();
        }
        else {
          $scope.dataignition2   =  data;
          if(data.getEngineData!=null){
           ignitionVal    =  ($filter('filter')(data.getEngineData, {'ignitionStatus': "!null"}));
           $scope.dataignition    = filterIgnitionStatus(ignitionVal,$scope.todate,$scope.totime);
         }

       }


     });
      }else{

        $scope.errMsg=licenceExpiry;
        $scope.showErrMsg = true;
        stopLoading();
      } 
    }catch (errr){
      // $('#status').fadeOut(); 
      // $('#preloader').delay(350).fadeOut('slow');  
      console.log(' error '+errr);
      stopLoading();
    }  
  }

  function filterIgnitionStatus(data,todate,totime) {

    console.log('pair filter..');
    var ign_On    = 0;
    var lastOnId;
    var remdata=false;
    var durVar     =  0;
    var durVarTot  =  0;
    var remdata=false;
    var ret_Arr       = [];
    var toDateTimeVal = utcFormat(todate,changeTimeFormat(totime));  
    ret_Arr =data;

      //   for(var i=0; i<data.length; i++) {

      //       if(data[i].ignitionStatus == "ON") {

      //      if(ign_On==0) {

      //            //console.log(i+' '+'ON');

      //             ret_Arr.push(data[i]);
      //            if(remdata==true&&ret_Arr.length>=2) {

      //            if((ret_Arr[ret_Arr.length-1].dateTime-ret_Arr[ret_Arr.length-2].dateTime)<=$scope.filterDuration){
      //                          ret_Arr.pop();
      //                          ret_Arr.pop();
      //                           }
      //                           else if(ret_Arr.length>=3){
      //                             ret_Arr.pop();
      //                             ret_Arr.pop();
      //                             ret_Arr.pop();
      //                             ret_Arr.push(data[i]);
      //                           }
      //          }

      //             if(ret_Arr.length>=2&&remdata==false){
      //                          if((ret_Arr[ret_Arr.length-1].dateTime-ret_Arr[ret_Arr.length-2].dateTime)<=$scope.filterDuration){
      //                          //alert('pop');
      //                          ret_Arr.pop();
      //                          ret_Arr.pop();

      //                          //remdata=true;
      //                           }
      //                   }  


      //            ign_On = 1;
      //          }

      //   } else if(data[i].ignitionStatus == "OFF") {

      //     if(ign_On==1) {

      //       ret_Arr.push(data[i]);
      //       remdata=false;
      //       if(ret_Arr.length>=2){
      //                          if((ret_Arr[ret_Arr.length-1].dateTime-ret_Arr[ret_Arr.length-2].dateTime)<=$scope.filterDuration){
      //                          if(data.length==(i+1)){
      //                           ret_Arr.pop();
      //                          }
      //                          remdata=true;
      //                           }
      //                   }  

      //              //console.log(i+' '+'OFF');
      //             ign_On=0;  
      //           }
      //   }

      //   }

      // if(ret_Arr.length){ 
      //   if(ret_Arr[ret_Arr.length-1].ignitionStatus=="OFF"){

      //    if(ret_Arr.length>=2){
      //                          if((ret_Arr[ret_Arr.length-1].dateTime-ret_Arr[ret_Arr.length-2].dateTime)<=$scope.filterDuration){

      //                           ret_Arr.pop();
      //                           ret_Arr.pop();

      //                           }
      //                   }  

      //   }
      // }

      var ignVar    = 0;
      var onInit    = 0;
      var priTol = 0;

      for(var i=0; i<ret_Arr.length; i++) {

       if(ret_Arr[i].ignitionStatus=="ON" ) {

        if(onInit==0) {
         ignVar=ignVar+ret_Arr[i].dateTime;
         onInit=1;
       }  


     } else if(ret_Arr[i].ignitionStatus=="OFF") {


       if(onInit==1) {
         priTol = priTol+(ret_Arr[i].dateTime-ignVar);
         ignVar    = 0;
         onInit    = 0;
       }
     }
   }
   durVarTot=priTol;
   if(onInit==1) {
    ret_Arr.push({ignitionStatus:"ONN",dateTime:toDateTimeVal});
    durVarTot=priTol+(toDateTimeVal-ret_Arr[ret_Arr.length-2].dateTime);
  }
  console.log(durVarTot);
             //$scope.durVarTot  =  $scope.msToTime(durVarTot);


             return ret_Arr; 
           }

           $scope.alertMe_click    = function(value){
            switch(value){
              case 'movementreport':
              $scope.recursive1($scope.movementdata,0);
              break;
        // case 'overspeedreport':
        //   $scope.recursive($scope.overspeeddata,0);
        //   break;
        case 'stoppedparkingreport':
        $scope.recursiveStop($scope.parkeddata,0);
        break;
        case 'idlereport':
        $scope.recursiveIdle($scope.idlereport,0);
        break;
        case 'eventReport':
        $scope.recursiveEvent($scope.eventReportData,0);
        break;
     /* case 'acreport':
          // $scope.recursiveLoad($scope.loadreport,0);
          break; */
          case 'fuelreport':
          $scope.fuelChart($scope.fuelValue);
          $scope.recursiveFuel($scope.fuelValue,0);
          break;
          default:
          break;
        }
      }

      function _pairFilter(_data, _yes, _no, _status) {

        var _checkStatus =_no, _pairList = [];

        angular.forEach(_data, function(value, key) {

          if( _pairList.length <= 0 ) {

            if(value[_status] == _yes) {
              _pairList.push(value)
            }

          } else if(_pairList.length >0 ) {

            if(value[_status] == _checkStatus) {

              _pairList.push(value);
              if($scope.dealerFilter){
                try{
                 if((_pairList[_pairList.length-1].ignitionStatus == 'OFF')){
                            //alert(_pairList[_pairList.length-1].ignitionStatus);
                            if(((_pairList[_pairList.length-1].date)-(_pairList[_pairList.length-2].date))<=$scope.filterDuration){

                              _pairList.pop();
                                  //remdata=true;
                                }
                              }
                            }catch(err){
                              console.log(err);
                            }
                          }
                          if(_pairList[_pairList.length-1][_status] == _yes) {
                            _checkStatus = _no;
                          } else {
                            _checkStatus = _yes
                          }
                        }
                      }

                    });


        if(_status=='ignitionStatus') {
         console.log('filter Duration');
         var ignVar    = 0;
         var onInit    = 0;
         var ignVarTot = 0;

         for(var i=0; i<_pairList.length; i++){
          if(_pairList[i].ignitionStatus=="ON"){
           if(onInit==0){
            ignVar=ignVar+_pairList[i].date;
            onInit=1;
          }  
        } else if(_pairList[i].ignitionStatus=="OFF"){

          if(onInit==1){
           ignVarTot =ignVarTot+(_pairList[i].date-ignVar);
           ignVar=0;
           onInit=0;
         }
       }
     } 
      //console.log($scope.msToTime2(ignVarTot));
      $scope.ignDurTot  = $scope.msToTime2(ignVarTot);
      $scope.ignDurTot2 = ignVarTot;
    }     

    console.log(_pairList);

    if(_pairList.length>1) {

      if(_pairList.length%2==0) {

       return _pairList;

       console.log(_pairList);

     } else {

       var lastVal = _pairList[_pairList.length-1];

       console.log( lastVal );

         //_pairList.pop();

         _pairList[_pairList.length] = { date:$scope.toTimeVal, ignitionStatus:"ONN", address:lastVal.address, latitude:lastVal.latitude, longitude:lastVal.longitude };

         console.log(_pairList); 

         if($scope.ignDurTot2!=undefined) {

          //console.log($scope.ignDurTot2);
          //console.log($scope.toTimeVal);
          console.log( _pairList[_pairList.length-2].date );

          var dur = $scope.toTimeVal - _pairList[_pairList.length-2].date;
          dur = $scope.ignDurTot2 + dur;

          $scope.ignDurTot = $scope.msToTime2(dur);

          console.log( $scope.ignDurTot );
        }

        return _pairList;
      }

    }  else {
           //_pairList.pop();

           console.log(_pairList);

          //return _pairList;
        }

      }

  /*   function filterNoData(data){

      //console.log(data);
        console.log(data.length);
        var retArr  = data;

          var toDateTimeVal = utcFormat($scope.todate,convert_to_24h($scope.totime)); 

            retArr.push({date:toDateTimeVal,position:"UU"});

              console.log(retArr);

      return retArr;
    }*/


    // function ignitionFilter(ignitionValue)
    // {
    //  $scope.ignitionData   =_pairFilter(ignitionValue, 'ON', 'OFF', 'ignitionStatus');
    // }
    // function acFilter(_acData){
    //  $scope.acReport   =_pairFilter(_acData, 'yes', 'no', 'vehicleBusy');
    // }

    function loadReportApi(url){
      //var loadUrl = "http://"+getIP+context+"/public/getLoadReport?vehicleId="+prodId;
      $http.get(url).success(function(loadresponse){
        $scope.loadreport     =    loadresponse;
        console.log(' log '+$scope.tabload)
      });
    }

  /*  function filter(obj){
      var _returnObj = [];
      if(obj)
        angular.forEach(obj,function(val, key){
          if(val.fuelLitre >0)
            _returnObj.push(val)
        })
      return _returnObj;
    }*/

    function filter(obj,name){
      var _returnObj = [];
      if(name=='fuel'){
        angular.forEach(obj,function(val, key){

          if(val.fuelLitre >0)
          {
            _returnObj.push(val)
          }
        })
      }
      else if(name=='stoppage'){

        angular.forEach(obj,function(val, key){

          if(val.stoppageTime >0)
          {
            _returnObj.push(val)
          }
        })
      }
      else if(name=='ovrspd'){
        return $scope.hist.overSpeedList;
        // angular.forEach(obj,function(val, key){
        //   if(val.isOverSpeed=="Y"){
        //     if(val.overSpeedTime >0){
        //       _returnObj.push(val)
        //     }else{
        //       val.overSpeedTime = 1000;
        //       _returnObj.push(val)
        //     }
        //   }
        // })
      }
      return _returnObj;
    }


    function _globalFilter(data) { 

      $scope.allData          = [];
      $scope.parkeddata       = [];
      $scope.overspeeddata    = [];
      $scope.movementdata     = [];
      $scope.idlereport       = [];
      $scope.temperatureData  = [];
      $scope.fuelValue        = [];
      $scope.ignitionData     = [];
    //$scope.acReport         = [];
    $scope.stopReport       = [];
    $scope.noData           = [];

    try {

      if(data || data.length > 0) {

          //$scope.ovrSpeedGraph($scope.hist.overSpeedList);
          $scope.ovrSpeedGraph(data);
          $scope.allData         =  data;
          $scope.parkeddata      =  ($filter('filter')(data, {'position':"P"}));
          //$scope.overspeeddata =  ($filter('filter')(data, {'isOverSpeed':"Y"}));
          $scope.overspeeddata   =  $scope.hist.overSpeedList;
          $scope.movementdata    =  ($filter('filter')(data, {'position':"M"}));
          $scope.idlereport      =  ($filter('filter')(data, {'position':"S"}));
          $scope.temperatureData =  ($filter('filter')(data, {'temperature': "0"}));
          $scope.fuelValue       =  filter(data,'fuel');
          ignitionValue          =  ($filter('filter')(data, {'ignitionStatus': "!undefined"}))
          $scope.ignitionData    =  _pairFilter(ignitionValue, 'ON', 'OFF', 'ignitionStatus');
        //$scope.acReport        =  _pairFilter(data, 'yes', 'no', 'vehicleBusy');
        $scope.stopReport      =  filter(data,'stoppage');
        $scope.noData          =  ($filter('filter')(data, {'position':"U"}));

          // ignitionFilter(ignitionValue);
          // acFilter(data)
        }
      } catch (error) {
        stopLoading();
      }
    }


    $scope.timeFilter_park=function(data,fVal) { 

      var filterValues=fVal;
      var ret_obj=[];

      if(data){

        angular.forEach(data,function(val, key){

          if(val.parkedTime >0 && filterValues==100){

            if(val.parkedTime>=3600000){

              ret_obj.push(val);
            }

          }else if(val.parkedTime >0 && filterValues==30){


            if(val.parkedTime>=1800000){

              ret_obj.push(val);
            }

          }else if(val.parkedTime >0 && filterValues==15){

            if(val.parkedTime>=900000){

              ret_obj.push(val);
            }

          }else if(val.parkedTime >0 && filterValues==10){

            if(val.parkedTime>=600000){

              ret_obj.push(val);
            }
          }else if(val.parkedTime >0 && filterValues==5){


            if(val.parkedTime>=300000){

              ret_obj.push(val);
            }
          }else if(val.parkedTime >0 && filterValues==2){


            if(val.parkedTime>=120000){

              ret_obj.push(val);
            }
          }
          else if(val.parkedTime >0 && filterValues==1){


            if(val.parkedTime>=60000){

              ret_obj.push(val);
            }
          }else if(val.parkedTime >0 && filterValues=='All'){

            ret_obj.push(val);
          }


        })
      }

      return ret_obj;
    }

    $scope.timeFilter_stop=function(data,fVal) {

      var filterValues=fVal;
      var ret_obj=[];

      if(data){

        angular.forEach(data,function(val, key){

          if(val.stoppageTime>0 && filterValues==100){

            if(val.stoppageTime>=3600000){

              ret_obj.push(val);
            }

          }else if(val.stoppageTime >0 && filterValues==30){


            if(val.stoppageTime>=1800000){

              ret_obj.push(val);
            }

          }else if(val.stoppageTime >0 && filterValues==15){

            if(val.stoppageTime>=900000){

              ret_obj.push(val);
            }

          }else if(val.stoppageTime >0 && filterValues==10){

            if(val.stoppageTime>=600000){

              ret_obj.push(val);
            }
          }else if(val.stoppageTime >0 && filterValues==5){

            if(val.stoppageTime>=300000){

              ret_obj.push(val);
            }

          }else if(val.stoppageTime >0 && filterValues==2){

            if(val.stoppageTime>=120000){

              ret_obj.push(val);
            }

          }
          else if(val.stoppageTime >0 && filterValues==1){

            if(val.stoppageTime>=60000){

              ret_obj.push(val);
            }

          }else if(val.stoppageTime >0 && filterValues=='All'){

            ret_obj.push(val);
          }


        })
      }

      return ret_obj;
    }

 /*  $scope.filtersTime= function(val,name){

  $scope.filterValue=val;

  if(name=='park'){
  $scope.parkeddata=[];

  console.log('park');
  $scope.parkeddata=$scope.timeFilter_park($scope.filterLoc, $scope.filterValue);

  }else if(name=='stop'){
   console.log('stop');
     $scope.stopReport=[];
     $scope.stopReport=$scope.timeFilter_stop($scope.filterLoc, $scope.filterValue);
   }

 }  */


 $scope.filtersTime= function(val,name) {

 //  $scope.filterValue=val;
 //  $scope.filterValueName=name;

 if(name=='park'){
     // console.log('park');
     $scope.filterValPark=val;
     $scope.parkeddata=[];
     $scope.parkeddata=$scope.timeFilter_park($scope.filterLoc, $scope.filterValPark);
   }else if(name=='stop'){
    // console.log('stop');
    $scope.filterValStop=val;
    $scope.stopReport=[];
    $scope.stopReport=$scope.timeFilter_stop($scope.filterLoc, $scope.filterValStop);
  }
}

    //for initial loading
    $scope.dataArray      = function(data) {

      _globalFilter(data);


      if(tabId == 'fuel')
      {
        $scope.fuelChart($scope.fuelValue);
        $scope.recursiveFuel($scope.fuelValue, 0);
      }

    // loadReportApi("http://"+getIP+context+"/public/getLoadReport?vehicleId="+prodId);
    $scope.recursive1($scope.movementdata,0);
    //console.log(' data----> '+$scope.downloadid)
  };

  /*  // for submit button click
    $scope.dataArray_click    = function(data) {
      
      _globalFilter(data)
      $scope.alertMe_click($scope.downloadid);
    
    };*/

// for submit button click
$scope.dataArray_click    = function(data) {

 $scope.time_stop='All';
 $scope.time_park='All'; 

 _globalFilter(data);

 if($scope.filterValPark!=undefined && $scope.filterValStop!=undefined){

  $scope.parkeddata=[];
  $scope.parkeddata=$scope.timeFilter_park($scope.filterLoc, $scope.filterValPark);

  $scope.stopReport=[];
  $scope.stopReport=$scope.timeFilter_stop($scope.filterLoc,$scope.filterValStop);
}
else if($scope.filterValPark!=undefined){
  $scope.parkeddata=[];
  $scope.parkeddata=$scope.timeFilter_park($scope.filterLoc, $scope.filterValPark); 
}else if($scope.filterValStop!=undefined){
 $scope.stopReport=[];
 $scope.stopReport=$scope.timeFilter_stop($scope.filterLoc,$scope.filterValStop);
}

$scope.alertMe_click($scope.downloadid);
};


$scope.recursive   = function(location_over,index){
  var indexs = 0;
  angular.forEach(location_over, function(value, primaryKey){
    indexs = primaryKey;
    if(location_over[indexs].address == undefined)
    {
          //console.log(' address over speed'+indexs)
          var latOv    =  location_over[indexs].latitude;
          var lonOv    =  location_over[indexs].longitude;
          var tempurlOv  =  "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latOv+','+lonOv+"&sensor=true";
          //console.log(' in overspeed '+indexs)
          delayed(3000, function (indexs) {
            return function () {
              google_api_call_Over(tempurlOv, indexs, latOv, lonOv);
            };
          }(indexs));
        }
      })
}

function google_api_call_Over(tempurlOv, indexs, latOv, lonOv){
  $http.get(tempurlOv).success(function(data){
    $scope.oaddress[indexs] = data.results[0].formatted_address;
      // var t = vamo_sysservice.geocodeToserver(latOv,lonOv,data.results[0].formatted_address);
    })
}


function google_api_call(tempurlMo, index1, latMo, lonMo) {
  $http.get(tempurlMo).success(function(data){
    $scope.maddress1[index1] = data.results[0].formatted_address;
      // var t = vamo_sysservice.geocodeToserver(latMo,lonMo,data.results[0].formatted_address);
    })
};
$scope.recursive1   =   function(locations, indes)
{
  var index1  =  0;
  angular.forEach(locations, function(value, primaryKey){
    index1 = primaryKey;
    if(locations[index1].address == undefined)
    {
          //console.log(' address movementreport'+index1)
          var latMo    =  locations[index1].latitude;
          var lonMo    =  locations[index1].longitude;
          var tempurlMo  =  "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latMo+','+lonMo+"&sensor=true";
          //console.log('  movement report  '+index1)
          delayed1(3000, function (index1) {
            return function () {
              google_api_call(tempurlMo, index1, latMo, lonMo);
            };
          }(index1));
        }
      })
}

var delayed = (function () {
  var queue = [];

  function processQueue() {
    if (queue.length > 0) {
      setTimeout(function () {
        queue.shift().cb();
        processQueue();
      }, queue[0].delay);
    }
  }

  return function delayed(delay, cb) {
    queue.push({ delay: delay, cb: cb });

    if (queue.length === 1) {
      processQueue();
    }
  };
}());
var delayed1 = (function () {
  var queue = [];

  function processQueue() {
    if (queue.length > 0) {
      setTimeout(function () {
        queue.shift().cb();
        processQueue();
      }, queue[0].delay);
    }
  }

  return function delayed(delay, cb) {
    queue.push({ delay: delay, cb: cb });

    if (queue.length === 1) {
      processQueue();
    }
  };
}());
var delayed2 = (function () {
  var queue = [];

  function processQueue() {
    if (queue.length > 0) {
      setTimeout(function () {
        queue.shift().cb();
        processQueue();
      }, queue[0].delay);
    }
  }

  return function delayed(delay, cb) {
    queue.push({ delay: delay, cb: cb });

    if (queue.length === 1) {
      processQueue();
    }
  };
}());
var delayed3 = (function () {
  var queue = [];

  function processQueue() {
    if (queue.length > 0) {
      setTimeout(function () {
        queue.shift().cb();
        processQueue();
      }, queue[0].delay);
    }
  }

  return function delayed(delay, cb) {
    queue.push({ delay: delay, cb: cb });

    if (queue.length === 1) {
      processQueue();
    }
  };
}());
var delayed4 = (function () {
  var queue = [];

  function processQueue() {
    if (queue.length > 0) {
      setTimeout(function () {
        queue.shift().cb();
        processQueue();
      }, queue[0].delay);
    }
  }

  return function delayed(delay, cb) {
    queue.push({ delay: delay, cb: cb });

    if (queue.length === 1) {
      processQueue();
    }
  };
}());

var delayed5 = (function () {
  var queue = [];

  function processQueue() {
    if (queue.length > 0) {
      setTimeout(function () {
        queue.shift().cb();
        processQueue();
      }, queue[0].delay);
    }
  }

  return function delayed(delay, cb) {
    queue.push({ delay: delay, cb: cb });

    if (queue.length === 1) {
      processQueue();
    }
  };
}());

var delayed6 = (function () {
  var queue = [];

  function processQueue() {
    if (queue.length > 0) {
      setTimeout(function () {
        queue.shift().cb();
        processQueue();
      }, queue[0].delay);
    }
  }

  return function delayed(delay, cb) {
    queue.push({ delay: delay, cb: cb });

    if (queue.length === 1) {
      processQueue();
    }
  };
}());

$scope.address_click = function(data, ind)
{
  var urlAddress    = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+data.latitude+','+data.longitude+"&sensor=true"
  $http.get(urlAddress).success(function(response)
  {
    data.address  = response.results[0].formatted_address;
      // var t      =   vamo_sysservice.geocodeToserver(data.latitude,data.longitude,response.results[0].formatted_address);
    });
}


function google_api_call_stop(tempurlStop, index2, latStop, lonStop) {
  $http.get(tempurlStop).success(function(data){
    $scope.saddressStop[index2] = data.results[0].formatted_address;
      // var t = vamo_sysservice.geocodeToserver(latStop,lonStop,data.results[0].formatted_address);
    })
};

$scope.recursiveStop   = function(locationStop,indexStop){
  var index2 = 0;
  angular.forEach(locationStop, function(value, primaryKey){
    index2 = primaryKey;
    if(locationStop[index2].address == undefined)
    {
          //console.log(' address stop'+index2)
          var latStop    =  locationStop[index2].latitude;
          var lonStop    =  locationStop[index2].longitude;
          var tempurlStop  =  "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latStop+','+lonStop+"&sensor=true";
          //console.log('  stopped or parked '+index2)
          delayed2(3000, function (index2) {
            return function () {
              google_api_call_stop(tempurlStop, index2, latStop, lonStop);
            };
          }(index2));
        }
      })
}
function google_api_call_Idle(tempurlIdle, index3, latIdle, lonIdle) {
  $http.get(tempurlIdle).success(function(data){
    $scope.addressIdle[index3] = data.results[0].formatted_address;
      // var t = vamo_sysservice.geocodeToserver(latIdle,lonIdle,data.results[0].formatted_address);
    })
};
function google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent) {
  $http.get(tempurlEvent).success(function(data){
    $scope.addressEvent[index4] = data.results[0].formatted_address;
    console.log(' address '+$scope.addressEvent[index4])
      // var t = vamo_sysservice.geocodeToserver(latEvent,lonEvent,data.results[0].formatted_address);
    })
};
$scope.recursiveIdle   = function(locationIdle,indexIdle){
  var index3 = 0;
  angular.forEach(locationIdle, function(value, primaryKey){
    index3 = primaryKey;
    if(locationIdle[index3].address == undefined)
    {
          //console.log(' address idle'+index3)
          var latIdle    =  locationIdle[index3].latitude;
          var lonIdle    =  locationIdle[index3].longitude;
          var tempurlIdle  =  "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latIdle+','+lonIdle+"&sensor=true";
          delayed3(3000, function (index3) {
            return function () {
              google_api_call_Idle(tempurlIdle, index3, latIdle, lonIdle);
            };
          }(index3));
        }
      })
}


$scope.recursiveEvent   =   function(locationEvent, indexEvent)
{
  var index4 = 0;
  angular.forEach(locationEvent, function(value ,primaryKey){
      //console.log(' primaryKey '+primaryKey)
      index4 = primaryKey;
      if(locationEvent[index4].address == undefined)
      {
        var latEvent     =  locationEvent[index4].latitude;
        var lonEvent     =  locationEvent[index4].longitude;
        var tempurlEvent =  "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latEvent+','+lonEvent+"&sensor=true";
        delayed4(2000, function (index4) {
          return function () {
            google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent);
          };
        }(index4));
      }
    })
}

function google_api_call_Load(tempurlLoad, index5, latLoad, lonLoad) {
  $http.get(tempurlLoad).success(function(data){
    $scope.addressLoad[index5] = data.results[0].formatted_address;

  })
};

$scope.recursiveLoad  =   function(locationLoad, indexLoad)
{
  var index5 = 0;
  angular.forEach(locationLoad, function(value ,primaryKey){
      //console.log(' primaryKey '+primaryKey)
      index5 = primaryKey;
      if(locationLoad[index5].address == undefined)
      {
        var latLoad    =  locationLoad[index5].latitude;
        var lonLoad    =  locationLoad[index5].longitude;
        var tempurlLoad = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latLoad+','+lonLoad+"&sensor=true";
        delayed5(2000, function (index5) {
          return function () {
            google_api_call_Load(tempurlLoad, index5, latLoad, lonLoad);
          };
        }(index5));
      }
    })
}

function google_api_call_Fuel(tempurlFuel, index6, latFuel, lonFuel) {
  $http.get(tempurlFuel).success(function(data){
    $scope.addressFuel[index6] = data.results[0].formatted_address;
  })
};

$scope.recursiveFuel  =   function(locationFuel, indexFuel)
{
  var index6 = 0;
  angular.forEach(locationFuel, function(value ,primaryKey){
      //console.log(' primaryKey '+primaryKey)
      index6 = primaryKey;
      if(locationFuel[index6].address == undefined)
      {
        var latFuel    =  locationFuel[index6].latitude;
        var lonFuel    =  locationFuel[index6].longitude;
        var tempurlFuel = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latFuel+','+lonFuel+"&sensor=true";
        delayed6(2000, function (index6) {
          return function () {
            google_api_call_Fuel(tempurlFuel, index6, latFuel, lonFuel);
          };
        }(index6));
      }
    })
}


$scope.getParkedCorrectHours  = function(data) {
  return $scope.msToTime(data);
}

function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
  results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function convert_to_24h(time_str) {
    //console.log(time_str);
    var str   = time_str.split(' ');
    var stradd  = str[0].concat(":00");
    var strAMPM = stradd.concat(' '+str[1]);
    var time = strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
    var hours = Number(time[1]);
    var minutes = Number(time[2]);
    var seconds = Number(time[3]);
    var meridian = time[4].toLowerCase();

    if (meridian == 'p' && hours < 12) {
      hours = hours + 12;
    } 
    else if (meridian == 'a' && hours == 12) {
      hours = hours - 12;
    }     
    var marktimestr = ''+hours+':'+minutes+':'+seconds;   

      //alert(marktimestr);

      return marktimestr;
    };

    $scope.genericFunction = function(vehid, index, shortname,position, address,groupName,licenceExp) {
      licenceExpiry=licenceExp;
      $scope.errMsg="";
      $scope.shortNam=shortname;
      $scope.vehiname=vehid;
      sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
      angular.forEach($scope.vehicle_list[$scope.gIndex].vehicleLocations, function(val, key){
        if(vehid == val.vehicleId){
          $scope.vehicleMode   = val.vehicleMode;
          expiryDays=val.expiryDays;
        }
      });
    // if((licenceExpiry!="-")&&(licenceExpiry.indexOf(strExpired)!= -1)){
    //   $scope.errMsg=licenceExpiry;
    //    $scope.showErrMsg = true;
    // }else{

      if(position === "N" || position === 'Z') {
       alert(address);
     } else {
       var licenceExpiry="";
       if(localStorage.getItem('licenceExpiry'))licenceExpiry=localStorage.getItem('licenceExpiry');(vehid, $scope.gName);
       var pageUrl='history?vid='+vehid+'&vg='+$scope.gName+'&tn='+$scope.tn;
       console.log(pageUrl);
       $(location).attr('href',pageUrl);
     }
      //}

    }

    $scope.groupSelection = function(groupname, groupid){
      licenceExpiry="";
      $scope.errMsg="";
      $scope.gIndex  =  groupid;
   //startLoading();
   var urlGroup = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+groupname;
   $http.get(urlGroup).success(function(data){
    $scope.vehiname   =  data[groupid].vehicleLocations[0].vehicleId;
    $scope.shortNam   =  data[groupid].vehicleLocations[0].shortName;
    licenceExpiry=data[groupid].vehicleLocations[0].licenceExpiry;
    $scope.vehicleMode=data[groupid].vehicleLocations[0].vehicleMode;
    expiryDays=data[groupid].vehicleLocations[0].expiryDays;
    $scope.gName      =  data[groupid].group; 
    //$scope.locations  =  data;
    $scope.vehicle_list =  data;
    sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
    if(convertedtodate>convertedexpdate){
     $scope.errMsg=licenceExpiry;
     $scope.showErrMsg = true;
   }else{
    var pageUrl='history?vid='+$scope.vehiname+'&vg='+$scope.gName+'';
    $(location).attr('href',pageUrl);
  }
   //stopLoading();
 });

 }

 $scope.getLocation  = function(lat,lon,ind) { 
    //alert(ind);
    switch($scope.downloadid) {
      // case 'overspeedreport':
      //   $scope.recursive($scope.overspeeddata,ind);
      // break;
      case 'movementreport':
      $scope.recursive($scope.movementdata,ind);
      break;
      case 'stoppedparkingreport':
      $scope.recursive($scope.parkeddata,ind);
      break;
      default:
      break;
    }
  };
  
  $scope.alertMe    = function(data) {  
    $scope.tapchanged=true;
   //console.log(data);
   switch(data) {
    case 'All':
    $scope.downloadid  =  'allreport';
    $scope.tn='all';
    $scope.videoLink="https://www.youtube.com/watch?v=hyBnVZ9waq0&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=27";

        //$scope.recursive1($scope.allData,0);
        break;
        case 'noData':
        $scope.tn='noData';
        $scope.downloadid  =  'nodatareport';
        $scope.videoLink="https://www.youtube.com/watch?v=2bbaMms4SEQ&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=9";
        //$scope.recursive1($scope.noData,0);
        break;
        case 'Overspeed':
        $scope.tn='overspeed';
        $scope.downloadid  =  'overspeedreport';
        $scope.overallEnable =  true;
        $scope.videoLink="https://www.youtube.com/watch?v=MOMQTbPKNIc&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=28";

        //$scope.recursive($scope.overspeeddata,0);
        break;
        case 'Movement':
        $scope.tn='movement';
        $scope.downloadid  =  'movementreport';
        $scope.overallEnable =  true;
        //clearTimeout(promis);
        $scope.recursive1($scope.movementdata,0);
        $scope.videoLink="https://www.youtube.com/watch?v=Gg-zvg9M5Zg&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=22";

        break;
        case 'Stopped/Parked':
        $scope.tn='parked';
        $scope.downloadid  =  'stoppedparkingreport';
        $scope.overallEnable =  true;
        $scope.recursiveStop($scope.parkeddata,0);
        $scope.videoLink="https://www.youtube.com/watch?v=fV9M_59FhMM&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=21";

        break;
        case 'Geo Fence':
        $scope.downloadid  =  'geofencereport';
        $scope.overallEnable =  false;
        break;
        case 'idlereport':
        $scope.tn='idle';
        $scope.downloadid    =  'idlereport';
        $scope.overallEnable =  true;
        $scope.recursiveIdle($scope.idlereport,0);
        $scope.videoLink="https://www.youtube.com/watch?v=TJb0Jhqvp54&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=26";

        break;
        case 'eventReport':

        $scope.downloadid    =  'eventReport';
        $scope.overallEnable =  true;
        $scope.recursiveEvent($scope.eventReportData,0);
        break;
        case 'sitereport':
        $scope.overallEnable =  true;
        $scope.downloadid    =  'sitereport';
        break;
        case 'loadreport':
        $scope.downloadid    =  'loadreport';
        $scope.overallEnable =  true;
      //$scope.recursiveLoad($scope.loadreport,0);
      break;
      case 'fuelreport':
      $scope.downloadid    =  'fuelreport';
      $scope.overallEnable =  true;
      $scope.fuelChart($scope.fuelValue);
      $scope.recursiveFuel($scope.fuelValue, 0);
      break;
      case 'ignitionreport':
      $scope.tn='ignition';
      $scope.downloadid   = 'ignitionreport';
      $scope.videoLink="https://www.youtube.com/watch?v=fsUvgODtDPs&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=25";

      break;
    /*case 'acreport':
        $scope.downloadid   = 'acreport';
        break; */
        case 'stoppageReport':
        $scope.tn='stoppageReport';
        $scope.downloadid   = 'stoppageReport';
        $scope.videoLink="https://www.youtube.com/watch?v=gBXpAE0HAW4&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=13";


        break;
        default:
        break;
      }
    };
    $scope.generatePDF = function(){
      var id=$scope.downloadid;
      kendo.drawing.drawDOM($("#"+id),{paperSize: "A4",multiPage: true,margin: {
        left   : "3mm",
        top    : "5mm",
        right  : "3mm",
        bottom : "5mm"
      } ,landscape: true }).then(function(group) {
        kendo.drawing.pdf.saveAs(group, id+"_HistoryReport.pdf"); 
      });

    }

    $scope.ovrSpeedGraph  =   function(data) {

      var grpDate   = [];
      var ovrSpd    = [];
  //console.log(data);

  try {

    if(data.length) {

      for (var i = 0; i < data.length; i++) {

        //if(data[i].overSpeedTime > 0 ) {
          if( data[i].speed > 0 ) {
            ovrSpd.push(data[i].speed);
            var dar = $filter('date')(data[i].date, "dd/MM/yyyy HH:mm:ss");
            grpDate.push(dar);
          }
        };
      }

    } catch (err) {
      console.log(err.message);
    }

    $(function () {
      $('#container3').highcharts({
        chart: {
          zoomType: 'x'
        },
        title: {
          text: translate('Speed')
        },
        credits: {
          enabled: false
        },
        xAxis: {
         categories: grpDate,
         title: {
          text: translate('Date & Time')
        }

      },

      yAxis: {
        title: {
          text: translate('Speed(Kms)')
        },
        plotLines: [{
          color: '#FF0000',
          width: 2,
                  value: $scope.hist.overSpeedLimit // Need to set this probably as a var.
                }]
              },
              legend: {
                enabled: false
              },
              plotOptions: {
                area: {
                  // fillColor: {
                  //   // linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                  //     stops: [
                  //        [0, Highcharts.getOptions().colors[0]],
                  //         [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0.1).get('rgba')]
                  //         ]
                  //     },
                  fillColor: {
                  linearGradient: {
                  x1: 0,
                  y1: 0,
                  x2: 0,
                  y2: 1
                },
              stops: [
                [0, Highcharts.getOptions().colors[0]],
                [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')] // Notice the change from Highcharts.Color to Highcharts.color
            ]
        },
                  marker: {
                    radius: 2
                  },
                  lineWidth: 1,
                  states: {
                    hover: {
                      lineWidth: 1
                    }
                  },
                  threshold: null
                }
              },

              series: [{
                type: 'area',
                name: 'Speed',
                data:  ovrSpd
              }]
            });

    });



  }


  $scope.fuelChart = function(data) {

    var ltrs         =  [];
    var fuelDate     =  [];
    var distCovered  =  [];
    var spdVals      =  [];
    var tankSize     =  parseFloat($scope.tankSize);
  //alert(tankSize);

  console.log(data);

  try {
    if(data.length) {
      for (var i = 0; i < data.length; i++) {
        if(data[i].fuelLitre !='0' || data[i].fuelLitre !='0.0') {
          //ltrs.push(data[i].fuelLitre);
          ltrs.push( { y:parseFloat(data[i].fuelLitre), odo:data[i].odoDistance, speed:data[i].speed, ignition:data[i].ignitionStatus } ); 
          var dar = $filter('date')(data[i].date, "dd/MM/yyyy HH:mm:ss");
          fuelDate.push(dar);
          distCovered.push(data[i].distanceCovered);
          spdVals.push(data[i].speed);
        }

      };
    }
  } catch (err) {
    console.log(err.message)
  }

    //console.log(ltrs);
    
    $(function () {

      $('#container').highcharts({
        chart: {
          zoomType: 'x'
        },
        title: {
          text: translate('Fuel')
        },
        credits: {
          enabled: false
        },
        xAxis: {
         categories: fuelDate,
         title: {
          text: translate('Date&Time')
        }
      },           
      yAxis: {
        title: {
          text: translate('Fuel(Ltrs)')
        },
        max: tankSize,
        min: 0,
        endOnTick: false 
      },
      tooltip: {
        formatter: function () {
          var s,a=0;
              //console.log(this.points);

              $.each(this.points, function () {
                        //a+=1;   
                        //if(a==2){ 
                          s =  '<b>'+this.x+'</b>'+'</b>'+'<br>'+' '+'</br>'+
                          '--------------------'+'<br>'+' '+'</br>'+
                          translate('Fuel')+'     : ' +'  '+'<b>' + this.point.y + '</b>'+' Ltrs'+'<br>'+' '+'</br>'+
                          translate('Speed')+'    : ' +'  '+'<b>' + this.point.speed + '</b>'+' Kmph'+'<br>'+' '+'</br>'+
                          translate('Ignition')+' : ' +'  '+'<b>' + this.point.ignition + '</b>'+'<br>'+' '+'</br>'+
                          translate('Odo')+'      : ' +'  '+'<b>' + this.point.odo + '</b>';
                         //}       
                       });
              return s;
            },
            shared: true
          },
          legend: {
            enabled: false
          },
          plotOptions: {
            area: {
              fillColor: {
                linearGradient: {
                  x1: 0,
                  y1: 0,
                  x2: 0,
                  y2: 1
                },
                stops: [
                [0, Highcharts.getOptions().colors[0]],
                [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                ]
              },
              marker: {
                radius: 2
              },
              lineWidth: 1,
              states: {
                hover: {
                  lineWidth: 1
                }
              },
              threshold: null,
              turboThreshold: 0
            }
          },

          series: [{
            type: 'area',
            name: 'Fuel Level',
            data: ltrs
          }]
        });


      $('#container2').highcharts({
        chart: {
          zoomType: 'x'
        },
        title: {
          text: translate('FuelDistance')
        },
        credits: {
         enabled: false
       },
       xAxis: {
        categories: distCovered,
        title: {
          text: translate('Distance(Kms)')
        }
      },

      yAxis: {
        title: {
          text: translate('Fuel(Ltrs)')
        },
        max: tankSize,
        min: 0,
        endOnTick: false 
      },
      tooltip: {
        formatter: function () {
          var s;
                    //console.log(this.points);
                    $.each(this.points, function () {                    
                      s  =  translate('Fuel')+'      : ' +'  '+'<b>'+this.y + '</b>'+' Ltrs'+'<br>'+' '+'</br>'+
                      translate('Distance')+'  : ' +'  '+'<b>'+this.x + '</b>'+' Kms'+'<br>';
                    });

                    return s;
                  },
                  shared: true
                },
                legend: {
                  enabled: false
                },
                plotOptions: {
                  area: {
                    fillColor: {
                      linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                      },
                      stops: [
                      [0, Highcharts.getOptions().colors[0]],
                      [1, Highcharts.color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                      ]
                    },
                    marker: {
                      radius: 2
                    },
                    lineWidth: 1,
                    states: {
                      hover: {
                        lineWidth: 1
                      }
                    },
                    threshold: null,
                    turboThreshold: 0
                  }
                },

                series: [{
                  type: 'area',
                  name: 'Fuel Level',
                  data: ltrs
                }]
              }); 


      Highcharts.chart('container4', {
        chart: {
          width:900,
          height: 300,
          type: 'spline',
          zoomType: 'xy'              
        },
        title: {
          text: translate('FuelSpeed')
        },
        legend: {
               /* layout: 'vertical',
                  align: 'left',
                  verticalAlign: 'top',
                  x: 150,
                  y: 100,*/
                  floating: true,
               /* borderWidth: 1,
               backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'*/
             },
             xAxis: {
              categories: fuelDate,
                  plotBands: [{ // visualize the weekend
                    from: 4.5,
                    to: 6.5,
                    color: 'rgba(68, 170, 213, .2)'
                  }]
                },
                yAxis: {
                  title: {
                    text: translate('Speed&Fuel')
                  },
                  max: tankSize,
                  min: 0,
                  endOnTick: false 
                },
                tooltip: {
                  shared: true,
                  valueSuffix: 'Kmph/Ltrs'
                },
                credits: {
                  enabled: false
                },
                plotOptions: {
                  areaspline: {
                    fillOpacity: 0.5
                  }
                },
                series: [{
                  name: translate('Speed'),
                  data: spdVals
                }, {
                  name: translate('Fuel'),
                  data: ltrs
                }]
              });

    });

}


// $scope.exportData = function (data) {
//     //console.log(data);
//     var blob = new Blob([document.getElementById(data).innerHTML], {
//       type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
//     });
//     saveAs(blob, data+".xls");
//   };
  $scope.exportData = function (data) {
    var blob = new Blob(["\ufeff", document.getElementById(data).innerHTML], {
        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, data + ".xls");
};

  // $scope.exportDataCSV = function (data) {
  //   //console.log(data);
  //   CSV.begin('#'+data).download(data+'.csv').go();
  // };
  $scope.exportDataCSV = function (data) {
    var table = document.getElementById(data); // Assuming data is the ID of the div containing the table
    var rows = table.querySelectorAll("table tr");
    var csvContent = "";

    // Loop through each row in the table
    for (var i = 0; i < rows.length; i++) {
        var row = rows[i];
        var cells = row.querySelectorAll("td, th");

        // Loop through each cell in the row
        for (var j = 0; j < cells.length; j++) {
            var cell = cells[j];
            var cellValue = cell.textContent.trim().replace(/"/g, '""'); // Escape double quotes
            csvContent += '"' + cellValue + '"' + (j < cells.length - 1 ? "," : ""); // Add cell value and comma if it's not the last cell
        }

        csvContent += "\n"; // Add a new line at the end of each row
    }

    // Create a Blob from the CSV content
    var blob = new Blob(["\uFEFF" + csvContent], {
        type: "text/csv;charset=utf-8;"
    });

    // Use FileSaver.js to save the Blob as a CSV file
    saveAs(blob, data + ".csv");
};


  $scope.msToTime   = function(ms) {
    days       = Math.floor(ms / (24*60*60*1000));
    daysms     = ms % (24*60*60*1000);
    hours      = Math.floor((ms)/(60*60*1000));
    hoursms    = ms % (60*60*1000);
    minutes    = Math.floor((hoursms)/(60*1000));
    minutesms  = ms % (60*1000);
    sec = Math.floor((minutesms)/(1000));
      // return days+"d : "+hours+"h : "+minutes+"m : "+sec+"s";
      if(hours=="NaN"||hours==Infinity)hours=0;
      if(minutes=="NaN"||minutes==Infinity)minutes=0;
      if(sec=="NaN"||sec==Infinity)sec=0;
      hours = (hours<10)?"0"+hours:hours;
      minutes = (minutes<10)?"0"+minutes:minutes;
      sec = (sec<10)?"0"+sec:sec;
      return hours+":"+minutes+":"+sec;
    }

    $scope.msToTime2   = function(ms) {
      days = Math.floor(ms / (24*60*60*1000));
      daysms=ms % (24*60*60*1000);
      hours = Math.floor((ms)/(60*60*1000));
      hoursms=ms % (60*60*1000);
      minutes = Math.floor((hoursms)/(60*1000));
      minutesms=ms % (60*1000);
      sec = Math.floor((minutesms)/(1000));

      return hours+"h : "+minutes+"m : "+sec+"s";

    /*  if(days>0) {
        return days+"d : "+hours+"h : "+minutes+"m : "+sec+"s";
      } else {
        return hours+"h : "+minutes+"m : "+sec+"s";
      }*/
      //return hours+":"+minutes+":"+sec;
    }
    function setBtnEnable(btnName){
     $scope.yesterdayDisabled = false;
     $scope.weekDisabled = false;
     $scope.monthDisabled = false;
     $scope.todayDisabled = false;
     $scope.lastmonthDisabled = false;
     $scope.thisweekDisabled = false;

     switch(btnName){

      case 'yesterday':
      $scope.yesterdayDisabled = true;
      break;
      case 'today':
      $scope.todayDisabled = true;
      break;
      case 'thisweek':
      $scope.thisweekDisabled = true;
      break;
      case 'lastweek':
      $scope.weekDisabled = true;
      break;
      case 'month':
      $scope.monthDisabled = true;
      break;
      case 'lastmonth':
      $scope.lastmonthDisabled = true;
      break;
    }


  }
  setBtnEnable("init");
  $scope.durationFilter    =   function(duration){
  //alert('inside function');
  startLoading();
  var now = new Date();
  $scope.todate       = getTodayDate(now.setDate(now.getDate() - 1));
  switch(duration){

    case 'yesterday':
    setBtnEnable('yesterday');
    var d = new Date();
    $scope.fromdate       = getTodayDate(d.setDate(d.getDate() - 1));
    $scope.totime   = '11:59 PM';
      // datechange();
      break;
      case 'thisweek':
      setBtnEnable('thisweek');
      var d=new Date();
      var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1);
      var firstday= new Date(d.getFullYear(), d.getMonth(), diff);
      $scope.fromdate     = getTodayDate(firstday.setDate(firstday.getDate() ));
      $scope.todate= getTodayDate(new Date().setDate(new Date().getDate() ));

      $scope.totime      = formatAMPM(d);
      //datechange();
      break;
      case 'lastweek':
      setBtnEnable('lastweek');
      var d = new Date();
      var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1)-7;
      var diff1=d.getDate() - day + (day == 0 ? -6:1)-1;
      var firstday= new Date(d.getFullYear(), d.getMonth(), diff);
      var lastday= new Date(d.getFullYear(), d.getMonth(), diff1);
      $scope.fromdate      = getTodayDate(firstday.setDate(firstday.getDate() ));
      $scope.todate       = getTodayDate(lastday.setDate(lastday.getDate() ));
      $scope.totime = '11:59 PM';
      //datechange();
      break;
      case 'month':
      setBtnEnable('month');
      var date = new Date();
      var firstdate = new Date(date.getFullYear(), date.getMonth(), 1);
      $scope.fromdate       = getTodayDate(firstdate.setDate(firstdate.getDate() ));
      $scope.todate       = getTodayDate(date.setDate(date.getDate() ));

      $scope.totime       = formatAMPM(date);
       //datechange();
       break;
       case 'today':
       setBtnEnable('today');
       var d = new Date();
       $scope.fromdate       = getTodayDate(d.setDate(d.getDate() ));
       $scope.todate       = getTodayDate(d.setDate(d.getDate() ));
       $scope.totime       = formatAMPM(d);
      //datechange();
      break;
      case 'lastmonth':
      setBtnEnable('lastmonth');
      var date = new Date();
      var firstdate = new Date(date.getFullYear(), date.getMonth()-1, 1);
      var lastdate = new Date(date.getFullYear(), date.getMonth(), 0);
      $scope.fromdate       = getTodayDate(firstdate.setDate(firstdate.getDate() ));
      $scope.todate       = getTodayDate(lastdate.setDate(lastdate.getDate() ));

      $scope.totime   = '11:59 PM';
      //datechange();
      break;
    }
    licenceExpiry=localStorage.getItem('licenceExpiry');
    expdate=moment().add(expiryDays,'days').format('DD-MM-YYYY');
    convertedexpdate=utcFormat(expdate,changeTimeFormat('11:59 PM'));
    convertedtodate=utcFormat($scope.todate,changeTimeFormat($scope.totime));
    convertedfromdate=utcFormat($scope.fromdate,changeTimeFormat($scope.fromtime));
    if(convertedtodate>convertedexpdate){

      $scope.errMsg=licenceExpiry;
      $scope.showErrMsg = true;
      stopLoading();
    }else{
     datechange();
   }
 }

 function datechange()
 {
  if((checkXssProtection($scope.fromdate) == true) && (checkXssProtection($scope.todate) == true) && (checkXssProtection($scope.fromtime) == true) && (checkXssProtection($scope.totime) == true))
  {
   var histurl   = GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+prodId+"&interval="+$scope.interval+'&fromDateUTC='+utcFormat($scope.fromdate,changeTimeFormat($scope.fromtime))+'&toDateUTC='+utcFormat($scope.todate,changeTimeFormat($scope.totime));
     // var histurl   = GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+prodId+'&fromDateUTC='+utcFormat($scope.fromdate,convert_to_24h($scope.fromtime))+'&toDateUTC='+utcFormat($scope.todate,convert_to_24h($scope.totime));
     // var histurl   = "http://"+getIP+context+"/public//getVehicleHistory?vehicleId="+prodId+"&fromDate="+$scope.fromdate+"&fromTime="+convert_to_24h($scope.fromtime)+"&toDate="+$scope.todate+"&toTime="+convert_to_24h($scope.totime)+"&interval="+$scope.interval+'&fromDateUTC='+utcFormat($scope.fromdate,convert_to_24h($scope.fromtime))+'&toDateUTC='+utcFormat($scope.todate,convert_to_24h($scope.totime));
     // var loadUrl   = "http://"+getIP+context+"/public//getLoadReport?vehicleId="+prodId+"&fromDate="+$scope.fromdate+"&fromTime="+convert_to_24h($scope.fromtime)+"&toDate="+$scope.todate+"&toTime="+convert_to_24h($scope.totime);
     showIgnitionData();

     try{
      $http.get(histurl).success(function(data){

        $scope.errMsg=data.error;
     // $scope.loading       = false;
     $scope.hist          =  data;
     $scope.showErrMsg    =  true;
     $scope.cardData      =  true;
     $scope.filterLoc     =  data.vehicleLocations;
     $scope.topspeedtime  =  data.topSpeedTime;
      //$scope.tankSize      =  parseInt(data.tankValue);
      $scope.tankSize      =  data.tankValue;
        // loadReportApi(loadUrl);
        $scope.dataArray_click(data.vehicleLocations);
        // $('#status').fadeOut(); 
        // $('#preloader').delay(350).fadeOut('slow');
        stopLoading();  
      });
    }
    catch (err){
      console.log(' err '+err);
      // $('#status').fadeOut(); 
      //  $('#preloader').delay(350).fadeOut('slow');
      stopLoading();  
    }
  } 
}

    //submit button click function
    $scope.buttonClick;
    $scope.plotHist     = function() {
      $scope.fromdate  =  $('#dateFrom').val();
      $scope.todate    =  $('#dateTo').val();
      $scope.fromtime  =  $('#timeFrom').val();
      $scope.totime    =  $('#timeTo').val();
      if(localStorage.getItem('timeTochange')!='yes'){
       updateToTime();
       $scope.totime    =   localStorage.getItem('toTime');
     }
     $scope.toTimeVal = utcFormat($scope.todate,convert_to_24h($scope.totime));
     expdate=moment().add(expiryDays,'days').format('DD-MM-YYYY');
     convertedexpdate=utcFormat(expdate,changeTimeFormat('11:59 PM'));
     convertedtodate=utcFormat($scope.todate,changeTimeFormat($scope.totime));
     convertedfromdate=utcFormat($scope.fromdate,changeTimeFormat($scope.fromtime));
     if(convertedtodate>convertedexpdate){
      $scope.errMsg=licenceExpiry;
      $scope.showErrMsg = true;
    }else{
      $scope.yesterdayDisabled = false;
      $scope.weekDisabled = false;
      $scope.monthDisabled = false;
      $scope.todayDisabled = false;
      $scope.lastmonthDisabled = false;
      $scope.thisweekDisabled = false;
      $scope.ignDurTot      =  "0h : 0m : 0s"; 
      if((checkXssProtection($scope.fromdate) == true) && (checkXssProtection($scope.todate) == true) && (checkXssProtection($scope.fromtime) == true) && (checkXssProtection($scope.totime) == true))
      {
        $scope.historyError='';
        startLoading();
        var valueas   = $('#txtv').val();

        // alert($scope.fromdate);
        // alert($scope.todate);
        // alert( convert_to_24h($scope.fromtime) );
        // alert( convert_to_24h($scope.totime) );
        showIgnitionData();

        var histurl   = GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+prodId+"&interval="+$scope.interval+'&fromDateUTC='+utcFormat($scope.fromdate,changeTimeFormat($scope.fromtime))+'&toDateUTC='+utcFormat($scope.todate,changeTimeFormat($scope.totime));
     // var histurl   = GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+prodId+'&fromDateUTC='+utcFormat($scope.fromdate,convert_to_24h($scope.fromtime))+'&toDateUTC='+utcFormat($scope.todate,convert_to_24h($scope.totime));
     // var histurl   = "http://"+getIP+context+"/public//getVehicleHistory?vehicleId="+prodId+"&fromDate="+$scope.fromdate+"&fromTime="+convert_to_24h($scope.fromtime)+"&toDate="+$scope.todate+"&toTime="+convert_to_24h($scope.totime)+"&interval="+$scope.interval+'&fromDateUTC='+utcFormat($scope.fromdate,convert_to_24h($scope.fromtime))+'&toDateUTC='+utcFormat($scope.todate,convert_to_24h($scope.totime));
     // var loadUrl   = "http://"+getIP+context+"/public//getLoadReport?vehicleId="+prodId+"&fromDate="+$scope.fromdate+"&fromTime="+convert_to_24h($scope.fromtime)+"&toDate="+$scope.todate+"&toTime="+convert_to_24h($scope.totime);
     try{
      $http.get(histurl).success(function(data){
        stopLoading();  
        if(data=='Failed'){
          $scope.historyError =  translate("curl_error");
        }
        else{

          $scope.errMsg=data.error;
     // $scope.loading       = false;
     $scope.hist          =  data;
     $scope.showErrMsg    =  true;
     $scope.cardData      =  true;
     $scope.filterLoc     =  data.vehicleLocations;
     $scope.topspeedtime  =  data.topSpeedTime;
      //$scope.tankSize      =  parseInt(data.tankValue);
      $scope.tankSize      =  data.tankValue;
        // loadReportApi(loadUrl);
        $scope.dataArray_click(data.vehicleLocations);
        // $('#status').fadeOut(); 
        // $('#preloader').delay(350).fadeOut('slow');
      }

    });
    }
    catch (err){
      console.log(' err '+err);
      // $('#status').fadeOut(); 
      //  $('#preloader').delay(350).fadeOut('slow');
      stopLoading();  
    }
  }
}
}

  //pdf method
  $scope.pdfHist     =   function() {  

   var histurl = GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+$scope.vvid+"&fromDate="+$scope.fd+"&fromTime="+convert_to_24h($scope.ft)+"&toDate="+$scope.td+"&toTime="+convert_to_24h($scope.tt)+"&interval="+$scope.interval;      
    // var histurl = GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+$scope.vvid+"&fromDate="+$scope.fd+"&fromTime="+convert_to_24h($scope.ft)+"&toDate="+$scope.td+"&toTime="+convert_to_24h($scope.tt);      
    // console.log(histurl);   

    //var toDateTimeVal = utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));

    $http.get(histurl).success(function(data){

      $scope.errMsg      =  data.error;
      $scope.hist        =  data;
      $scope.showErrMsg  =  true;
      $scope.cardData    =  true;
      $scope.filterLoc   =  data.vehicleLocations;
    //$scope.tankSize    =  parseInt(data.tankValue);
    $scope.tankSize    =  data.tankValue;

    $scope.dataArray(data.vehicleLocations);

    switch($scope.repId) {
      case 'movementreport':
      $scope.recursive1($scope.movementdata,0);
      break;
      case 'stoppedparkingreport':
      $scope.recursiveStop($scope.parkeddata,0);
      break;
      case 'idlereport':
      $scope.recursiveIdle($scope.idlereport,0);
      break; 
      default:
      break;
    }     
  });
    
  }

  function dateStringFormat(d) {
    var s     =     d.split(' ');
    var t     =     s[0].split('-');
    var ds    =     (t[2].concat('-'+t[1]).concat('-'+t[0])).concat(' '+s[1]);
    return new Date(ds).getTime();
  }
  
  $scope.figureOutTodosToDisplay = function() {
    var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
    var end = begin + $scope.itemsPerPage;
    $scope.filteredTodos = $scope.movementdata.slice(begin, end);
  };


  $scope.pageChanged = function() {
    $scope.figureOutTodosToDisplay();
  };

  $(window).scroll(function (event) {
    if($scope.tapchanged){
      $scope.tapchanged=false;
      console.log('scrolled');
      switch($scope.tn){
        case 'all':
                   // alert('all scroll');
                   $('.table-fixed-header1').fixedHeader();
                   break;
                   case 'movement':
                   $('.table-fixed-header8').fixedHeader();
                   break;
                   case 'overspeed':
                   $('.table-fixed-header2').fixedHeader();
                   break;
                   case 'parked':
                    //alert('parked scroll');
                    $('.table-fixed-header3').fixedHeader();
                    break;
                    case 'idle':
                    $('.table-fixed-header4').fixedHeader();
                    break;
                    case 'noData':
                    $('.table-fixed-header7').fixedHeader();
                    break;
                    case 'ignition':
                    $('.table-fixed-header5').fixedHeader();
                    break;
                    case 'stoppageReport':
                    $('.table-fixed-header6').fixedHeader();
                    break;
                    default:
                    $('.table-fixed-header8').fixedHeader();
                    break;
                  }

                }
              });

  
}]);
// app.factory('vamo_sysservice', function($http, $q){
//  return {
//    geocodeToserver: function (lat, lng, address) {
//      try { 
//        var reversegeourl = GLOBAL.DOMAIN_NAME+'/store?geoLocation='+lat+','+lng+'&geoAddress='+address;
//          return this.getDataCall(reversegeourl);
//      }
//      catch(err){ console.log(err); }

//    },
//         getDataCall: function(url){
//          var defdata = $q.defer();
//          $http.get(url).success(function(data){
//               defdata.resolve(data);
//      }).error(function() {
//                     defdata.reject("Failed to get data");
//             });
//      return defdata.promise;
//         }
//     }
// });

app.directive("getLocation", function () {
  return {
    restrict: "A",
    replace: true,    
    link: function (scope, element, attrs) {
      angular.element(element).on('click', function(){    //mouseenter
        var lat = attrs.lat;
        var lon = attrs.lon;
        var ind = attrs.index;
        console.log(ind);
        scope.getLocation(lat,lon,ind);
      });
    }
  };
});
