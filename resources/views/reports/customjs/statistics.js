app.controller('mainCtrl', ['$scope','$http' ,'$filter','vamoservice', '_global','$translate', function($scope, $http, $filter, vamoservice, GLOBAL,$translate){
  var language=localStorage.getItem('lang');
  console.log('statistic');
  $scope.multiLang=language;
  $translate.use(language);
  var licenceExpiry="";
  var strExpired='expired';
  if(localStorage.getItem('licenceExpiry'))licenceExpiry=localStorage.getItem('licenceExpiry');
  $scope.errMsg="";
  var translate = $filter('translate');
  $scope.stastics=true;
  $scope.reportBanShow     =  false;
  $scope.donut_new         =  0;
  $scope.donut             =  1;
  $scope.showMonTable      =  false;
  $scope.showMonFuelTable  =  false;
  $scope.showDriverChart=false;
  $scope.tapchanged=true;
  $scope.ReducedData=[];
  var assLabel  =  localStorage.getItem('isAssetUser');
  $scope.vehiAssetView  = true;
  $scope.fromdate   =   $('#dtFrom').val();
  $scope.todate   =   $('#dtTo').val();
  var monthNames      =   ["Jan", "Feb", "Mar", "Apr", "May", "Jun","Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

  if(assLabel=="true") {
    $scope.vehiLabel = "Asset";
    $scope.vehiImage = true;
    $scope.vehiAssetView  = false;
  } else if(assLabel=="false") {
    $scope.vehiLabel      = "Vehicle";
    $scope.vehiImage      = false;
    $scope.vehiAssetView  = true;
  } else {
    $scope.vehiLabel      = "Vehicle";
    $scope.vehiImage      = false;
    $scope.vehiAssetView  = true;
  }

  var getUrl          =  document.location.href;
  var tabId;
//var tabId           =  'executive';
//$scope.donut_new    =  false;
var index           =  getParameterByName("ind");
$scope.tab           =  getParameterByName("ind");

$scope.buttonShow=true;
if($scope.tab=="5"||$scope.tab=="6" || $scope.tab=="7"){
  $scope.buttonShow=false;
}

//$scope.fromMonthFuel='06/2017';
$scope.reportUrl  =  GLOBAL.DOMAIN_NAME+'/getReportsList';

$scope.$watch("reportUrl", function (val) {

  $http.get($scope.reportUrl).success(function(data){

   var tabShow = data;
       // console.log(tabShow);
       if(tabShow != "" && tabShow != null)  {     
      //console.log('not Empty getReportList API ...');
      angular.forEach(tabShow,function(val, key){

        var newReportName = Object.getOwnPropertyNames(val).sort();

        if(newReportName == 'Statistics_IndivdiualReportsList'){

          if(val.Statistics_IndivdiualReportsList[0].VEHICLE_PERFORMANCE==false&&val.Statistics_IndivdiualReportsList[0].CONSOLIDATED==false) {

            $scope.sort  = sortByDate('date');
            $scope.reportBanShow =true;
            $(window).load(function(){
              $('#allReport').modal('show');
            });
            stopLoading();

          } else {

            var reportSubName = Object.getOwnPropertyNames(val.Statistics_IndivdiualReportsList[0]).sort();
                  //console.log(reportSubName);

                  angular.forEach(reportSubName,function(value, keys){

                    switch(value){
                     case 'VEHICLE_PERFORMANCE':
                        //if(val.Statistics_IndivdiualReportsList[0].DAILY==false){ $scope.dailyTabShow=false; }
                        $scope.dailyTabShow=val.Statistics_IndivdiualReportsList[0].VEHICLE_PERFORMANCE;
                        break;
                       // case 'MONTHLY_DIST':
                       //  //if(val.Statistics_IndivdiualReportsList[0].MONTHLY_DIST==false){ $scope.distMonTabShow=false; }
                       //  $scope.distMonTabShow=val.Statistics_IndivdiualReportsList[0].MONTHLY_DIST;
                       // break;
                       // case 'POI':
                       //  //if(val.Statistics_IndivdiualReportsList[0].POI==false){ $scope.poiTabShow=false; }
                       //  $scope.poiTabShow=val.Statistics_IndivdiualReportsList[0].POI;
                       // break;
                       case 'CONSOLIDATED':
                        //if(val.Statistics_IndivdiualReportsList[0].CONSOLIDATED==false){ $scope.consolTabShow=false; }
                        $scope.consolTabShow=val.Statistics_IndivdiualReportsList[0].CONSOLIDATED;
                        break;
                      }
                    });

                  switch(index){

                    case '1':
                    tabId                = 'executive';
                    $scope.downloadid    = 'executive';
                    $scope.donut_new     = false;
                    $scope.donut         = false;
                    $scope.showDate      = true;
                    $scope.showMonth     = false;
                    $scope.showMonthFuel = false;
                    $scope.sort          = sortByDate('date');
                    break;
                    case '2':
                    tabId                = 'poi';
                    $scope.downloadid    = 'poi';
                    $scope.actTab        = true;
                    $scope.donut_new     = false;
                    $scope.donut         = true;
                    $scope.showDate      = true;
                    $scope.showMonth     = false;
                    $scope.showMonthFuel = false;
                    $scope.sort          = sortByDate('time');
                    break;
                    case '3':
                    tabId                 = 'executive';
                    $scope.donut_new      = false;
                    $scope.donut          = false;
                    $scope.downloadid     = 'consolidated';
                    $scope.actCons        = true;
                    $scope.showDate       = true;
                    $scope.showMonth      = false;
                    $scope.showMonthFuel  = false;
                    $scope.sort           = sortByDate('date');
                    break;
                    case '4':
                    $scope.donut_new      = true;
                    $scope.donut          = true;
                    tabId                 = 'execFuel';
                    $scope.downloadid     = 'execFuel';
                    $scope.showDate       = true;
                    $scope.showMonth      = false;
                    $scope.showMonthFuel  = false;
                    $scope.actFuel        = true;
                    $scope.sort           = sortByDate('date');
                    break;
                    case '5':
                    $scope.donut_new        = false;
                    $scope.donut            = true;
                    tabId                   = 'month';
                    $scope.downloadid       = 'month';
                    $scope.showDate         = false;
                    $scope.showMonth        = true;
                    $scope.showMonthFuel    = false;
                    $scope.actMonth         = true;
                    $scope.showMonTable     = false;
                    $scope.sort             = sortByDate('date');
                    break;
                    case '6':
                    $scope.donut_new        = false;
                    $scope.donut            = true;
                    tabId                   = 'monthFuel';
                    $scope.downloadid       = 'monthFuel';
                    $scope.showDate         = false;
                    $scope.showMonth        = false;
                    $scope.showMonthFuel    = true;
                    $scope.actMonthFuel     = true;
                    $scope.showMonFuelTable = false;
                    $scope.sort             = sortByDate('date');
                    break;
                    case '7':
                    $scope.donut_new        = false;
                    $scope.donut            = true;
                    tabId                   = 'freezekm';
                    $scope.downloadid       = 'FreezeKm';
                    $scope.showDate         = false;
                    $scope.showMonth        = false;
                    $scope.showMonthFuel    = false;
                    $scope.actMonth         = false;
                    $scope.showMonTable     = false;
                    $scope.sort             = sortByDate('date');
                    break;
                    default:
                    if($scope.dailyTabShow==true) {
                      tabId                = 'executive';
                      $scope.downloadid    = 'executive';
                      $scope.donut_new     = false;
                      $scope.donut         = false;
                      $scope.showDate      = true;
                      $scope.showMonth     = false;
                      $scope.showMonthFuel = false;
                      $scope.sort          = sortByDate('date');
                    } else if($scope.poiTabShow==true) {
                      tabId                = 'poi';
                      $scope.downloadid    = 'poi';
                      $scope.actTab        = true;
                      $scope.donut_new     = false;
                      $scope.donut         = true;
                      $scope.showDate      = true;
                      $scope.showMonth     = false;
                      $scope.showMonthFuel = false;
                      $scope.sort          = sortByDate('time');
                    } else if($scope.consolTabShow==true) {
                      tabId                 = 'executive';
                      $scope.donut_new      = false;
                      $scope.donut          = false;
                      $scope.downloadid     = 'consolidated';
                      $scope.actCons        = true;
                      $scope.showDate       = true;
                      $scope.showMonth      = false;
                      $scope.showMonthFuel  = false;
                      $scope.sort           = sortByDate('date');
                    } else if($scope.exFuelTabShow==true){
                      $scope.donut_new      = true;
                      $scope.donut          = true;
                      tabId                 = 'execFuel';
                      $scope.downloadid     = 'execFuel';
                      $scope.showDate       = true;
                      $scope.showMonth      = false;
                      $scope.showMonthFuel  = false;
                      $scope.actFuel        = true;
                      $scope.sort           = sortByDate('date');
                    } else if($scope.distMonTabShow==true) {
                      $scope.donut_new        = false;
                      $scope.donut            = true;
                      tabId                   = 'month';
                      $scope.downloadid       = 'month';
                      $scope.showDate         = false;
                      $scope.showMonth        = true;
                      $scope.showMonthFuel    = false;
                      $scope.actMonth         = true;
                      $scope.showMonTable     = false;
                      $scope.sort             = sortByDate('date');
                    } else if($scope.distMonFuelTabShow==true){
                      $scope.donut_new        =false;
                      $scope.donut            =true;
                      tabId                   = 'monthFuel';
                      $scope.downloadid       = 'monthFuel';
                      $scope.showDate         = false;
                      $scope.showMonth        = false;
                      $scope.showMonthFuel    = true;
                      $scope.actMonthFuel     = true;
                      $scope.showMonFuelTable = false;
                      $scope.sort             = sortByDate('date');
                    } else {
                      tabId                = 'executive';
                      $scope.downloadid    = 'executive';
                      $scope.donut_new     = false;
                      $scope.donut         = false;
                      $scope.showDate      = true;
                      $scope.showMonth     = false;
                      $scope.showMonthFuel = false;
                      $scope.sort          = sortByDate('date');
                    }
                    break;
                  }

                }

              }

            });

} else {

  $scope.exFuelTabShow=true;
  $scope.distMonFuelTabShow=true; 
  $scope.dailyTabShow=true;
  $scope.distMonTabShow=true;
  $scope.poiTabShow=true;
  $scope.consolTabShow=true;

  console.log('Empty getReportList API ...');

  switch(index){
    case '1':
    tabId                = 'executive';
    $scope.downloadid    = 'executive';
    $scope.donut_new     = false;
    $scope.donut         = false;
    $scope.showDate      = true;
    $scope.showMonth     = false;
    $scope.showMonthFuel = false;
    $scope.sort          = sortByDate('date');
    break;
    case '2':
    tabId                = 'poi';
    $scope.downloadid    = 'poi';
    $scope.actTab        = true;
    $scope.donut_new     = false;
    $scope.donut         = true;
    $scope.showDate      = true;
    $scope.showMonth     = false;
    $scope.showMonthFuel = false;
    $scope.sort          = sortByDate('time');
    break;
    case '3':
    tabId                 = 'executive';
    $scope.donut_new      = false;
    $scope.donut          = false;
    $scope.downloadid     = 'consolidated';
    $scope.actCons        = true;
    $scope.showDate       = true;
    $scope.showMonth      = false;
    $scope.showMonthFuel  = false;
    $scope.sort           = sortByDate('date');
    break;
    case '4':
    $scope.donut_new      = true;
    $scope.donut          = true;
    tabId                 = 'execFuel';
    $scope.downloadid     = 'execFuel';
    $scope.showDate       = true;
    $scope.showMonth      = false;
    $scope.showMonthFuel  = false;
    $scope.actFuel        = true;
    $scope.sort           = sortByDate('date');
    break;
    case '5':
    $scope.donut_new        = false;
    $scope.donut            = true;
    //console.log('index 4...');
    tabId                   = 'month';
    $scope.downloadid       = 'month';
    $scope.showDate         = false;
    $scope.showMonth        = true;
    $scope.showMonthFuel    = false;
    $scope.actMonth         = true;
    $scope.showMonTable     = false;
    $scope.sort             = sortByDate('date');
    break;
    case '6':
    $scope.donut_new        =false;
    $scope.donut            =true;
      //console.log('index 5...');
      tabId                   = 'monthFuel';
      $scope.downloadid       = 'monthFuel';
      $scope.showDate         = false;
      $scope.showMonth        = false;
      $scope.showMonthFuel    = true;
      $scope.actMonthFuel     = true;
      $scope.showMonFuelTable = false;
      $scope.sort             = sortByDate('date');
      break;
      case '7':
      $scope.donut_new        = false;
      $scope.donut            = true;
    //console.log('index 4...');
    tabId                   = 'freezekm';
    $scope.downloadid       = 'FreezeKm';
    $scope.showDate         = false;
    $scope.showMonth        = false;
    $scope.showMonthFuel    = false;
    $scope.actMonth         = false;
    $scope.showMonTable     = false;
    $scope.sort             = sortByDate('date');
    break;
    default:
    tabId                = 'executive';
    $scope.downloadid    = 'executive';
    $scope.donut_new     = false;
    $scope.donut         = false;
    $scope.showDate      = true;
    $scope.showMonth     = false;
    $scope.showMonthFuel = false;
    $scope.sort          = sortByDate('date');
    break;
  }
}

})
.error(function(data, status) {

  console.error('Repos error', status, data);
  console.log('Empty getReportList API ...');

  $scope.exFuelTabShow=true;
  $scope.distMonFuelTabShow=true; 
  $scope.dailyTabShow=true;
  $scope.distMonTabShow=true;
  $scope.poiTabShow=true;
  $scope.consolTabShow=true;

  switch(index){
    case '1':
    tabId                = 'executive';
    $scope.downloadid    = 'executive';
    $scope.donut_new     = false;
    $scope.donut         = false;
    $scope.showDate      = true;
    $scope.showMonth     = false;
    $scope.showMonthFuel = false;
    $scope.sort          = sortByDate('date');
    break;
    case '2':
    tabId                = 'poi';
    $scope.downloadid    = 'poi';
    $scope.actTab        = true;
    $scope.donut_new     = false;
    $scope.donut         = true;
    $scope.showDate      = true;
    $scope.showMonth     = false;
    $scope.showMonthFuel = false;
    $scope.sort          = sortByDate('time');
    break;
    case '3':
    tabId                 = 'executive';
    $scope.donut_new      = false;
    $scope.donut          = false;
    $scope.downloadid     = 'consolidated';
    $scope.actCons        = true;
    $scope.showDate       = true;
    $scope.showMonth      = false;
    $scope.showMonthFuel  = false;
    $scope.sort           = sortByDate('date');
    break;
    case '4':
    $scope.donut_new      = true;
    $scope.donut          = true;
    tabId                 = 'execFuel';
    $scope.downloadid     = 'execFuel';
    $scope.showDate       = true;
    $scope.showMonth      = false;
    $scope.showMonthFuel  = false;
    $scope.actFuel        = true;
    $scope.sort           = sortByDate('date');
    break;
    case '5':
    $scope.donut_new        = false;
    $scope.donut            = true;
    tabId                   = 'month';
    $scope.downloadid       = 'month';
    $scope.showDate         = false;
    $scope.showMonth        = true;
    $scope.showMonthFuel    = false;
    $scope.actMonth         = true;
    $scope.showMonTable     = false;
    $scope.sort             = sortByDate('date');
    break;
    case '6':
    $scope.donut_new        =false;
    $scope.donut            =true;
    tabId                   = 'monthFuel';
    $scope.downloadid       = 'monthFuel';
    $scope.showDate         = false;
    $scope.showMonth        = false;
    $scope.showMonthFuel    = true;
    $scope.actMonthFuel     = true;
    $scope.showMonFuelTable = false;
    $scope.sort             = sortByDate('date');
    break;
    case '7':
    $scope.donut_new        = false;
    $scope.donut            = true;
    tabId                   = 'freezekm';
    $scope.downloadid       = 'FreezeKm';
    $scope.showDate         = false;
    $scope.showMonth        = false;
    $scope.showMonthFuel    = false;
    $scope.actMonth         = false;
    $scope.showMonTable     = false;
    $scope.sort             = sortByDate('date');
    break;
    default:
    tabId                = 'executive';
    $scope.downloadid    = 'executive';
    $scope.donut_new     = false;
    $scope.donut         = false;
    $scope.showDate      = true;
    $scope.showMonth     = false;
    $scope.showMonthFuel = false;
    $scope.sort          = sortByDate('date');
    break;
  }

});

});

/*  //tab view
  if(index == 1) {
      tabId                = 'poi';
      $scope.downloadid    = 'poi';
      $scope.actTab        = true;
      $scope.donut_new     = false;
      $scope.donut         = true;
      $scope.showDate      = true;
      $scope.showMonth     = false;
      $scope.showMonthFuel = false;
      $scope.sort          = sortByDate('time');

  } else if(index == 2){
      tabId                 = 'executive';
      $scope.donut_new      = false;
      $scope.donut          = false;
      $scope.downloadid     = 'consolidated';
      $scope.actCons        = true;
      $scope.showDate       = true;
      $scope.showMonth      = false;
      $scope.showMonthFuel  = false;
      $scope.sort           = sortByDate('date');
  } 
    else if(index == 3){
      $scope.donut_new    = true;
      $scope.donut        = true;
      tabId               = 'fuel';
      $scope.downloadid     = 'fuel';
      $scope.showDate       = true;
      $scope.showMonth      = false;
      $scope.showMonthFuel  = false;
      $scope.actFuel        = true;
      $scope.sort           = sortByDate('date');

  } else if(index == 4) {
      $scope.donut_new=false;
      $scope.donut    =true;
    //console.log('index 4...');
      tabId         = 'month';
      $scope.downloadid   = 'month';
      $scope.showDate     = false;
      $scope.showMonth    = true;
      $scope.showMonthFuel = false;
      $scope.actMonth   = true;
      $scope.showMonTable = false;
      $scope.sort     = sortByDate('date');
  }
   else if(index == 5){
      $scope.donut_new=false;
      $scope.donut    =true;
      //console.log('index 5...');
      tabId         = 'monthFuel';
      $scope.downloadid   = 'monthFuel';
      $scope.showDate     = false;
      $scope.showMonth    = false;
      $scope.showMonthFuel = true;
      $scope.actMonthFuel    = true;
      $scope.showMonFuelTable = false;
      $scope.sort     = sortByDate('date');
  }
  else {
      tabId               = 'executive';
      $scope.downloadid   = 'executive';
      $scope.donut_new    = false;
      $scope.donut        =false;
      $scope.showDate     = true;
      $scope.showMonth    = false;
      $scope.showMonthFuel = false;
      $scope.sort     = sortByDate('date');
  }

  */

  $scope.trimHyphens = function(textVal){
   var splitValue = textVal.split(/[-]+/);
   return splitValue[2];
 }
// millesec to day, hours, min, sec
$scope.msToTime = function(ms) 
{
  days = Math.floor(ms / (24 * 60 * 60 * 1000));
  daysms = ms % (24 * 60 * 60 * 1000);
  hours = Math.floor((ms) / (60 * 60 * 1000));
  hoursms = ms % (60 * 60 * 1000);
  minutes = Math.floor((hoursms) / (60 * 1000));
  minutesms = ms % (60 * 1000);
  seconds = Math.floor((minutesms) / 1000);
      // if(days==0)
      //  return hours +" h "+minutes+" m "+seconds+" s ";
      // else
      hours = (hours<10)?"0"+hours:hours;
      minutes = (minutes<10)?"0"+minutes:minutes;
      seconds = (seconds<10)?"0"+seconds:seconds;
      return hours +":"+minutes+":"+seconds;
    }

    function daysInThisMonth() {
      var now = new Date();
 // console.log(now);
 var mm = now.getMonth()+1;
 if(mm==0){
  mm=12;
}
var nowNew = new Date(now.getFullYear(), mm, 0).getDate();

return nowNew;
}

$scope.submitMon=function(){
 // console.log($scope.fromMonthss);
 $scope.fromMonthss = document.getElementById("monthFrom").value;
 var newmonVals   = $scope.fromMonthss.split('/');
 $scope.monValss  = parseInt(newmonVals[0]);
 $scope.yearValss = newmonVals[1];

 $scope.lenMon   = getDaysInMonthFuel($scope.monValss,$scope.yearValss);

  //  console.log($scope.lenMon);

  $scope.colValss = $scope.lenMon+2;

  $scope.monValFrontss  = $scope.monthsValss[newmonVals[0]-1];
  $scope.curYearFrontss = " - "+newmonVals[1];

     //console.log(parseInt(newmonVal[0]));
       //console.log($scope.yearFuelVal);
       startLoading();
       $scope.showMonTable = false;
       serviceCall();
     }

     $scope.submitMonFuel=function(){

 // console.log($scope.fromMonthFuel);
 $scope.fromMonthFuel = document.getElementById("monthFroms").value;
 var newmonVal=$scope.fromMonthFuel.split('/');
 $scope.monFuelVal=parseInt(newmonVal[0]);
 $scope.yearFuelVal=newmonVal[1];

 $scope.lenMonss=getDaysInMonthFuel($scope.monFuelVal,$scope.yearFuelVal);
 $scope.colValss=$scope.lenMonss+2;

 $scope.monValFront  = $scope.monthsValss[newmonVal[0]-1];
 $scope.curYearFront = " - "+newmonVal[1];

     //console.log(parseInt(newmonVal[0]));
       //console.log($scope.yearFuelVal);
       startLoading();
       $scope.showMonFuelTable = false;
       serviceCall();
     }

     function getNewMonthFuel() {
      $scope.monthsValss=["January", "February","March","April","May","June","July","August","September","October","November","December"]; 

      var dates = new Date();
      var monsValss = dates.getMonth()+1;
      var yearValss = dates.getFullYear(); 

      if(monsValss == 0){
        monsValss=12;
        yearValss=dates.getFullYear()-1; 

        $scope.monFuelVal   = monsValss;
        $scope.monValss     = monsValss;
        $scope.yearFuelVal  = yearValss;
        $scope.yearValss    = yearValss;
        $scope.monValFront  = $scope.monthsValss[monsValss-1];
        $scope.curYearFront = " - "+yearValss;
        $scope.monValFrontss  = $scope.monthsValss[monsValss-1];
        $scope.curYearFrontss = " - "+yearValss;
      }else{

        $scope.monFuelVal   = monsValss;
        $scope.monValss     = monsValss;
        $scope.yearFuelVal  = yearValss;
        $scope.yearValss    = yearValss;
        $scope.monValFront  = $scope.monthsValss[monsValss-1];
        $scope.curYearFront = " - "+yearValss;
        $scope.monValFrontss  = $scope.monthsValss[monsValss-1];
        $scope.curYearFrontss = " - "+yearValss;

        if(monsValss<10){
         monsValss = "0"+monsValss; 
       }
     }

     return monsValss+'/'+yearValss;
   }
//console.log(getNewMonthFuel());
$scope.fromMonthFuel = getNewMonthFuel();
$scope.fromMonthss   = $scope.fromMonthFuel;
$scope.lenMon        = daysInThisMonth();
$scope.colVals       = $scope.lenMon+2;
$scope.lenMonss      = $scope.lenMon;
$scope.colValss      = $scope.lenMonss+2;

function getDaysInMonthFuel(month,year) {
  return new Date(year, month, 0).getDate();
}

function getDaysInMonth(month) {
  var now = new Date(); 
  return new Date(now.getFullYear(), month, 0).getDate();
}

function currentYear(){
  var yy = new Date();
  return yy.getFullYear();
}


function currentMonth(){

 var retVal;
 $scope.monthsVal=["January", "February","March","April","May","June","July","August","September","October","November","December"];
 var dd = new Date();
 //var mm = dd.getMonth()+1;
 var mm = dd.getMonth();
 var mmNew=mm;

 if(mm==0)
 {
  mm=12;
} 

if(mm<10) {
  mm='0'+mm;
}
$scope.monthNo=mm;

   //console.log($scope.monthNo);

   if(mmNew==0){
    retVal = $scope.monthsVal[11];
  }else{
    retVal = $scope.monthsVal[mmNew-1];
  }

  return retVal;
}

$scope.monArray=currentMonth();
// console.log($scope.monArray);
$scope.curYear=" - "+currentYear()+"";
$scope.monsVal=$scope.monArray;


$scope.monthssVal=function(val){

//  console.log(val);
switch(val)
{
  case 'January':
  $scope.monthNo="0"+1;
  break;
  case 'February':
  $scope.monthNo="0"+2;
  break;
  case 'March':
  $scope.monthNo="0"+3;
  break;
  case 'April':
  $scope.monthNo="0"+4;
  break;
  case 'May':
  $scope.monthNo="0"+5;
  break;
  case 'June':
  $scope.monthNo="0"+6;
  break;
  case 'July':
  $scope.monthNo="0"+7;
  break;
  case 'August':
  $scope.monthNo="0"+8;
  break;
  case 'September':
  $scope.monthNo="0"+9;
  break;
  case 'October':
  $scope.monthNo=10;
  break;
  case 'November':
  $scope.monthNo=11;
  break;
  case 'December':
  $scope.monthNo=12;
  break;
  default:
  console.log('not a month');
  break;
}

$scope.lenMon=getDaysInMonth($scope.monthNo);
$scope.colVals=$scope.lenMon+2;

startLoading();
$scope.showMonTable =  false;
serviceCall();
// console.log($scope.monthNo);
}

//Global Variable declaration 
$scope.url      =   GLOBAL.DOMAIN_NAME+'/getVehicleLocations';
var clickStatus   =   'groupButton';
//$scope.donut    =   true;
//$scope.donut_new    =   false;
$scope.bar      = true;
$('#singleDiv').hide();
var avoidOnload   = false;

var vehicleSelected = '';


$scope.parseInts=function(data){
 return parseInt(data);
}

function utcFormatOnlyDate(d){

  return new Date(d).getTime();
}
$scope.getUpdateData = function(shortName,date){
    // alert("Hello");

    var usernameMater = window.localStorage.getItem("userMasterName");
    var sendUser = usernameMater.split(',');
      // alert(sendUser[1]);
      $http.get('http://128.199.159.130:9000/addExecutiveDetailsTableData?userId='+sendUser[1]+'&date='+date+'&vehicleId='+shortName+'');
    //window.location.reload();
   // console.log(shortName+date);
   var shifttime=$scope.orgTime=='12:00 - 23:59'?0:$scope.orgTime;
   if($scope.showOrgTime){
     var groupUrl    = GLOBAL.DOMAIN_NAME+'/getExecutiveReport?groupId='+$scope.viewGroup.group+'&fromDate='+$scope.fromdate+'&toDate='+$scope.todate+'&vehicleId='+$scope.trackVehID+'&shiftTime='+shifttime;}
     else {
      var groupUrl    = GLOBAL.DOMAIN_NAME+'/getExecutiveReport?groupId='+$scope.viewGroup.group+'&fromDate='+$scope.fromdate+'&toDate='+$scope.todate+'&vehicleId='+$scope.trackVehID;
    }
    vamoservice.getDataCall(groupUrl).then(function(responseGroup){
      var tagsCheck   = (responseGroup.error) ? true :  false;
        // console.log($scope.to_trusted($scope.fromdate));
        $scope.execGroupReportData    = [];
        if(tagsCheck == false)
          if(vehicleSelected){
          //$scope.donut  =   false;
          //$scope.donut_new=   false;
          $scope.donut  = 1;
          $scope.bar    = false;
          $('#singleDiv').show(500);
          $scope.execGroupReportData  = ($filter('filter')(responseGroup.execReportData, {'vehicleId':vehicleSelected},true));
          
          barLoad(vehicleSelected);
        } else {
          //$scope.donut  =   false;
          //$scope.donut_new=   false;
          $scope.bar    = true;
          $('#singleDiv').hide();
          $scope.execGroupReportData  = responseGroup.execReportData;
          donutLoad(responseGroup);
        }
        else
          barLoad(vehicleSelected),donutLoad(responseGroup);
        stopLoading();
      })
  }

  function formatAMPM(date) {
    var date = new Date(date);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
  hours = hours ? hours : 12; // the hour '0' should be '12'
  minutes = minutes < 10 ? '0'+minutes : minutes;
  var strTime = hours + ':' + minutes + ' ' + ampm;
  return strTime;
}


function sessionValue(vid, gname){
  localStorage.setItem('user', JSON.stringify(vid+','+gname));
  $("#testLoad").load("../public/menu");
}

function initialAPICall(val){
  vamoservice.getDataCall(val).then(function(data) {

  //$scope.vehicleList  = data;
  $scope.vehicle_list = data;
  //console.log($scope.vehicleList);
  $scope.fromNowTS    = new Date();
  $scope.toNowTS      = new Date().getTime() - 86400000;
  $scope.fromdate     = getTodayDate($scope.fromNowTS.setDate($scope.fromNowTS.getDate()-7));
  $scope.todate       = getTodayDate($scope.toNowTS);
  $scope.fromtime     = formatAMPM($scope.fromNowTS);
  $scope.totime     = formatAMPM($scope.toNowTS);
  $scope.trackVehID = data[0].vehicleLocations[0].vehicleId;
  $scope.groupname  = data[0].group;
  $scope.group = $scope.groupname.split(":")[0];
  sessionValue($scope.trackVehID, $scope.groupname);
  if($scope.showOrgTime){
    calculateDateTime();
  }


  angular.forEach(data, function(value, key){

   if($scope.groupname == value.group){

    $scope.gIndex = value.rowId;
    $scope.vehicleNames=[];

    angular.forEach(value.vehicleLocations, function(val, skey){
      $scope.vehicleNames.push(val.vehicleId);
          // console.log(val.vehicleId);
        })
  }  
  if(value.totalVehicles) {
    $scope.viewGroup =  value;
    serviceCall();
    avoidOnload   = true;
  }

})

  $scope.notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
  window.localStorage.setItem('totalNotifications',$scope.notncount);

})
}

//vehicle list
$scope.$watch("url", function(val){
  $scope.orgShiftTime=[];
  $scope.orgShiftName=[];
  var timeUrl    = GLOBAL.DOMAIN_NAME+'/getOrgShiftTimeList';
  vamoservice.getDataCall(timeUrl).then(function(response){

    if(response.data==0){
      $scope.showOrgTime=false;
      initialAPICall(val);
    }
    else {

      $scope.showOrgTime=true;
      for(i=1;i<response.data.length;i++){
        if(response.data[i]!="00:00 - 23:59"){

          var orgnametime=response.data[i].split(',');

          var t=orgnametime[0].split('-');
          var t1=t[0].split(':');
          var t2=t[1].split(':');
          if(t1[0]=='00'){
            $scope.orgShiftTime.push('12:'+t1[1]+'-'+t[1]);
          }
          else if(t2[0]=='00'){
            $scope.orgShiftTime.push(t[0]+'-'+'12:'+t2[1]);
          }
          else {
            $scope.orgShiftTime.push(orgnametime[0]);
          }

        }
        else {

          var t=response.data[i].split('-');
          var t1=t[0].split(':');
          var t2=t[1].split(':');
          if(t1[0]=='00'){
            $scope.orgShiftTime.push('12:'+t1[1]+'-'+t[1]);
          }
          else if(t2[0]=='00'){
            $scope.orgShiftTime.push(t[0]+'-'+'12:'+t2[1]);
          }


        }
        $scope.orgShiftName.push(response.data[i]);

      }
      $scope.organisation=response.data[1].split(',');

      $scope.organName=$scope.organisation[1];
      var res=response.data[0];
      if(res=='00:00 - 23:59'){
        var index=$scope.orgShiftTime.indexOf('12:00 - 23:59');
        $scope.orgTime=$scope.orgShiftTime[index];}
        else {
          var index=$scope.orgShiftTime.indexOf(response.data[0]);
          $scope.orgTime=$scope.orgShiftTime[index];
        }
        if($scope.orgTime=='12:00 - 23:59'){
          $scope.showOrganization=false;
          $scope.fromtotime=('00:00 - 23:59').split('-');
        }
        else {
          $scope.showOrganization=true;
          $scope.fromtotime=$scope.orgTime.split('-');
        }

        initialAPICall(val); 

      }


    });
  
})

$scope.changeTime=function(time){
  $scope.orgTime=time;
}

function calculateDateTime(){
  if($scope.orgTime=='12:00 - 23:59'){
    $scope.showOrganization=false;
    $scope.fromtotime=('00:00 - 23:59').split('-');
  }
  else {
    $scope.showOrganization=true;
    $scope.fromtotime=$scope.orgTime.split('-');

  }

  var index=$scope.orgShiftTime.indexOf($scope.orgTime);
  var name=$scope.orgShiftName[index].split(',');
  $scope.organName=name[1];
  var frmhrs=$scope.fromtotime[0].split(':');
  var tohrs=$scope.fromtotime[1].split(':');

  var dateString1 = moment($scope.fromdate,'DD-MM-YYYY').format('MM-DD-YYYY'); 
  var hrs=new Date().getHours();
  var date1 = new Date(dateString1);
  var dateString2 = moment($scope.todate,'DD-MM-YYYY').format('MM-DD-YYYY'); 
  yesterdaydate=getTodayDate(new Date().setDate(new Date().getDate()-1));
  todaydate=getTodayDate(new Date().setDate(new Date().getDate()));
  var startDate = moment($scope.fromdate, 'DD-MM-YYYY');
  var endDate = moment($scope.todate, 'DD-MM-YYYY');

  var dayDiff = endDate.diff(startDate, 'days');

  var date2 = new Date(dateString2);
  if($scope.fromdate==$scope.todate){
    if($scope.orgTime=='12:00 - 23:59'){
      $scope.fromdate=getTodayDate(date1.setDate(date1.getDate()));
      $scope.todate=getTodayDate(date2.setDate(date2.getDate()));
    }
    else if(parseInt(tohrs[0])>parseInt(frmhrs[0]) && parseInt(tohrs[0])<parseInt(hrs)){
      $scope.fromdate=getTodayDate(date1.setDate(date1.getDate()));
      $scope.todate=getTodayDate(date2.setDate(date2.getDate()));
    }

    else {
      $scope.fromdate=getTodayDate(date1.setDate(date1.getDate()));
      $scope.todate=getTodayDate(date1.setDate(date1.getDate()+1));
    }
  }
  else if((dayDiff==1)&&(getTodayDate(date2.setDate(date2.getDate()))!=yesterdaydate && getTodayDate(date2.setDate(date2.getDate()))!=todaydate)){

   if(parseInt(tohrs[0])>parseInt(frmhrs[0])&& parseInt(tohrs[0])<parseInt(hrs)){
    $scope.fromdate=getTodayDate(date1.setDate(date1.getDate()));
    $scope.todate=getTodayDate(date2.setDate(date2.getDate()));
  }
  else if($scope.orgTime=='12:00 - 23:59'){
   $scope.fromdate=getTodayDate(date1.setDate(date1.getDate()));
   $scope.todate=getTodayDate(date2.setDate(date2.getDate()));
 }
 else {
  $scope.fromdate=getTodayDate(date1.setDate(date1.getDate()));
  $scope.todate=getTodayDate(date2.setDate(date2.getDate()+1));}
}
else {   

  if(getTodayDate(date2.setDate(date2.getDate()))==yesterdaydate && $scope.orgTime!='12:00 - 23:59'){
    if(parseInt(tohrs[0])>parseInt(frmhrs[0])&& parseInt(tohrs[0])<parseInt(hrs)){
      $scope.fromdate=getTodayDate(date1.setDate(date1.getDate()));
      $scope.todate=getTodayDate(date2.setDate(date2.getDate()+1));
    }

    else {
      $scope.fromdate=getTodayDate(date1.setDate(date1.getDate()));
      $scope.todate=getTodayDate(date2.setDate(date2.getDate()+1));}
    }
    else if((getTodayDate(date2.setDate(date2.getDate()))==todaydate) && $scope.orgTime=='12:00 - 23:59'){
      $scope.fromdate=getTodayDate(date1.setDate(date1.getDate()));
      $scope.todate=getTodayDate(date2.setDate(date2.getDate()-1));
    }
    else {
      $scope.fromdate=getTodayDate(date1.setDate(date1.getDate()));
      $scope.todate=getTodayDate(date2.setDate(date2.getDate()));
    }

  }

}
$scope.getAMPM=function(time){
  var str       =  time.split(':');     
  var hours     =  Number(str[0]);
  var minutes   =  Number(str[1]);

  if(hours=='00'){
   return ('12'+':'+('0'+minutes).slice(-2)).concat(" ",'AM');
 }
 
 else if(hours==12){
   return ('12'+':'+('0'+minutes).slice(-2)).concat(" ",'PM');
 }
 else if(hours < 12) {
  return (hours+':'+('0'+minutes).slice(-2)).concat(" ",'AM');
}
else{
  hours=hours-12;
  return (hours+':'+('0'+minutes).slice(-2)).concat(" ",'PM');
}     


}

//group click
$scope.groupSelection   = function(groupName, groupId) {
 startLoading();
 if(tabId == "month" || tabId == "monthFuel"){
  undo();
}
$scope.gIndex=groupId;
$scope.showMonTable=false;
vehicleSelected    = '';
$scope.donut       = 0;

$scope.viewGroup.group   =   groupName;
var groupUrl       =   GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+groupName;

vamoservice.getDataCall(groupUrl).then(function(groupResponse){

  $scope.trackVehID       =   groupResponse[groupId].vehicleLocations[0].vehicleId;
  $scope.groupname    = groupName;
  $scope.group = $scope.groupname.split(":")[0];
  sessionValue($scope.trackVehID, $scope.groupname)
    //$scope.vehicleList  =   groupResponse;
    $scope.vehicle_list  =   groupResponse;


    angular.forEach(groupResponse, function(value, key){

     if($scope.groupname == value.group){
      $scope.vehicleNames=[]; 
      $scope.gIndex = value.rowId;

      angular.forEach(value.vehicleLocations, function(val, skey){

       $scope.vehicleNames.push(val.vehicleId);
               // console.log(val.vehicleId);
             })
    }
  })
    $scope.notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
    window.localStorage.setItem('totalNotifications',$scope.notncount);
    serviceCall();
   //stopLoading();
 })

}

//vehicleId click
$scope.genericFunction  =   function(vehicleID, index,shortname,position, address,groupName,licenceExp){
  if(tabId!='freezekm'){
    startLoading();
    licenceExpiry=licenceExp;
    $scope.errMsg="";
    vehicleSelected   =   vehicleID;
    sessionValue(vehicleSelected,$scope.groupname);
    if(getParameterByName("ind")==1){

    // if((licenceExpiry!="-")&&(licenceExpiry.indexOf(strExpired)!= -1)){
    //         stopLoading();
    //         $scope.errMsg=licenceExpiry;
    //   }else{
     serviceCall();
            //}
          }

          stopLoading();}
          else {

          }
        }

//loading start function
// var startLoading   = function () {
//  $('#status').show(); 
//  $('#preloader').show();
// };

//loading stop function
// var stopLoading    = function () {
//  $('#status').fadeOut(); 
//  $('#preloader').delay(350).fadeOut('slow');
//  $('body').delay(350).css({'overflow':'visible'});
// };


//trim colon function
$scope.trimColon = function(textVal) {
  if(textVal!=undefined){    
    return textVal.split(":")[0].trim();
  }
}


//vehicle level graphs

function barLoad(vehicleId) {
      //console.log(vehicleId);
      $scope.barArray1  = [];
      $scope.barArray2  = [];
      $scope.barArray1.push(["X", "Date vs Distance"]);
      $scope.barArray2.push(["X", "Date vs Overspeed"]);
      $scope.data     = ($filter('filter')($scope.execGroupReportData, {'vehicleId':vehicleId},true));
      angular.forEach($scope.data, function(value, key) {
        $scope.barArray1.push([value.date, value.distanceToday]);
        $scope.barArray2.push([value.date, value.topSpeed]);
      }); 
    //console.log($scope.barArray1);
    c3.generate({
      bindto: '#chart3',
      data: {
        x: 'X',
        columns: $scope.barArray1,
        type: 'bar'
      },
      axis: {
        x: {
          type : 'category',
          label:{                 
            text : 'Date',
            position: 'outer-right' 
          }
        },
        y: {
          label:{
            text :'Distance Today',
            position: 'outer-middle'  
          }
        }
      }
    });
    
    c3.generate({
      bindto: '#chart4',
      data: {
        x: 'X',
        columns: $scope.barArray2,
        type: 'bar'
      },
      axis: {
        x: {
          type : 'category',
          label:{                 
            text : 'Date',
            position: 'outer-right' 
          }
        },
        y: {
          label:{
            text : 'Topspeed',
            position: 'outer-middle'  
          }
        }
      }
    });       
  };
  

//group level graph 
function donutLoad(data) {

  $scope.barArray = [];
  var vehiName='';
  var vehiId='';
  if(data != '')
    angular.forEach(JSON.parse(data.distanceCoveredAnalytics), function(value, key) {
      angular.forEach($scope.vehicle_list[$scope.gIndex].vehicleLocations, function(val, index){
        if(key == val.vehicleId){
          if(val.vehicleMode!='DG'){
           $scope.barArray.push([val.shortName, value, val.vehicleId]);
         }
       }
     });
    }); 
  $('#container').highcharts({
    chart: {
      type: 'column'
    },
    title: {
      text: translate('Total_Distance')
    },
    xAxis: {
      type: 'category',
      labels: {
        rotation: -45,
        style: {
          fontSize: '8px',
          fontFamily: 'Verdana, sans-serif'
        }
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: translate('Distance_Travelled')
      }
    },
    legend: {
      enabled: false
    },
        // tooltip: {
        //     pointFormat: 'Population in 2008: <b>{point.y:.1f} millions</b>'
        // },
        series: [{
          name: 'Distance',
          data: $scope.barArray,
          dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#003366',
            align: 'right',
                format: '{point.y:.1f}', // one decimal
                y: 5, // 10 pixels down from the top
                style: {
                  fontSize: '9px',
                  fontFamily: 'Verdana, sans-serif'
                }
              }
            }]
          });
};


function chartFuel(data){

  $scope.chartVehic  =   [];
  $scope.chartDist   =   [];
  $scope.chartFuels  =   [];
  
  var distVal=0;
  var fuelVal=0;
  var preVehiVal="";
  var curVehiVal="";
  var elseVal=0;


  if(data)
  {
    angular.forEach(data, function(val, key){

      curVehiVal=val.vehicleName;

      if(key==0){

       distVal=distVal+val.distanceToday;
       fuelVal=fuelVal+val.fuelConsume;

       preVehiVal=curVehiVal;
     }

     if(key>0){

       if(preVehiVal==curVehiVal){

         distVal=distVal+val.distanceToday;
         fuelVal=fuelVal+val.fuelConsume;

         preVehiVal=curVehiVal;
       }else{

        $scope.chartDist.push(parseInt(distVal));
        $scope.chartFuels.push(parseInt(fuelVal));
        $scope.chartVehic.push(preVehiVal);

            //  console.log(''+preVehiVal+'  '+distVal+'  '+fuelVal);
            distVal=0;
            fuelVal=0;


            distVal=distVal+val.distanceToday;
            fuelVal=fuelVal+val.fuelConsume;

            preVehiVal=curVehiVal;
            elseVal=1;
          }
        }
        if(key==data.length-1){

          $scope.chartDist.push(parseInt(distVal));
          $scope.chartFuels.push(parseInt(fuelVal));
          $scope.chartVehic.push(preVehiVal);
        }

      });

    if(elseVal==0){

      $scope.chartDist.push(parseInt(distVal));
      $scope.chartFuels.push(parseInt(fuelVal));
      $scope.chartVehic.push(preVehiVal);
    }

  /*  console.log($scope.chartVehic);
        console.log($scope.chartDist);  
        console.log($scope.chartFuels); */
      }


//Highcharts.chart('container', {
  $('#container_new').highcharts({  
    chart: {
      type: 'column'
    },
    title: {
      text: translate('Efficiency_Optimization')
    },
    xAxis: {
      categories: $scope.chartVehic,
    },
    yAxis: [{
      min: 0,
      title: {
        text: translate('Distance(Kms)')
      }
    }, {
      title: {
        text: translate('Fuel(Ltrs)')
      },
      opposite: true
    }],
    legend: {
      shadow: false
    },
    tooltip: {
      shared: true
    },
    plotOptions: {
      column: {
        grouping: false,
        shadow: false,
        borderWidth: 0
      }
    },
    series: [{
      name: translate('Distance_Covered'),
      color: 'rgba(165,170,217,1)',
      data: $scope.chartDist,
      pointPadding: 0.3,
      pointPlacement: -0.2
    }, /*{
        name: 'Employees Optimized',
        color: 'rgba(126,86,134,.9)',
        data: [140, 90, 40],
        pointPadding: 0.4,
        pointPlacement: -0.2
      },*/ {
        name: translate('Fuel_Consumed'),
        color: 'rgba(248,161,63,1)',
        data: $scope.chartFuels,
        tooltip: {
           // valuePrefix: '',
           valueSuffix: ' Ltrs'
         },
         pointPadding: 0.3,
         pointPlacement: 0.2,
         yAxis: 1
    } /*, {
        name: 'Profit Optimized',
        color: 'rgba(186,60,61,.9)',
        data: [203.6, 198.8, 208.5],
        tooltip: {
            valuePrefix: '$',
            valueSuffix: ' M'
        },
        pointPadding: 0.4,
        pointPlacement: 0.2,
        yAxis: 1
      }*/ ]
    });

}
$scope.convert = function(tDistance) 
{
  //alert('hhhh');
  var dist= parseFloat(tDistance).toFixed(2);
  return dist;
}

$scope.msToTime = function(ms) 
{
  days = Math.floor(ms / (24 * 60 * 60 * 1000));
  daysms = ms % (24 * 60 * 60 * 1000);
  hours = Math.floor((daysms) / (60 * 60 * 1000));
  hoursms = ms % (60 * 60 * 1000);
  minutes = Math.floor((hoursms) / (60 * 1000));
  minutesms = ms % (60 * 1000);
  seconds = Math.floor((minutesms) / 1000);
  hours = (hours<10)?"0"+hours:hours;
  minutes = (minutes<10)?"0"+minutes:minutes;
  seconds = (seconds<10)?"0"+seconds:seconds;
  if(days>1){
   return days+" days "+hours+":"+minutes+":"+seconds;
 } 
 else if(days==1){
   return days+" day "+hours+":"+minutes+":"+seconds;
 }
 else if(days==0){
  return hours +":"+minutes+":"+seconds;
}
}



function distanceMonth(data){

  var ret_obj = [];
  var i=0;
  angular.forEach(data.vehicleDistanceDatas, function(val, key){

    if(val!=null && val.vehicleMode!='DG'){

      ret_obj.push({vehiId:val.vehicleId,vehiName:val.shortName,totDist:val.totalDistance,vehicleMode : val.vehicleMode,position:val.position});
      ret_obj[i].distsTodays=[];

      angular.forEach(val.distances, function(sval, skey){
       ret_obj[i].distsTodays.push({distanceToday:sval});
           // console.log(sval);
         });
      i=i+1;
    }
  });

  return ret_obj;
}

function distanceFuelMonth(data){

  var ret_obj=[];

  angular.forEach(data.monthlyDistAndFuelDetails, function(val, key){
    if(val.totalDistance){
     val.totalDistance=parseFloat(val.totalDistance).toFixed(2);
   }

   ret_obj.push({vehiId:val.vehicleId,vehiName:val.shortName,totDist:val.totalDistance,totFuel:val.totalFuelConsume,odoStart:val.startOdoMeterReading,odoEnd:val.endOdoMeterReading,vehicleMode : val.vehicleMode,position:val.position,totKmpl:val.totalKmpl,
    totFuelFill:val.totalFuelFill,totLitperHr:val.totalLph});
   ret_obj[key].distsTodays=[];
   ret_obj[key].fuelTodays=[];
   ret_obj[key].kmplTodays=[];
   ret_obj[key].fuelFillTodays=[];
   ret_obj[key].lphTodays=[];

   angular.forEach(val.getVehicleDistanceData, function(sval, skey){
     ret_obj[key].distsTodays.push({distanceToday:sval});
           // console.log(sval);
         }); 

   angular.forEach(val.getVehicleFuelData, function(tval, tkey){
     ret_obj[key].fuelTodays.push({fuelsToday:tval});
           // console.log(tval);
         }); 
   angular.forEach(val.getVehicleKmplData, function(kval, tkey){
     ret_obj[key].kmplTodays.push({kmplsToday:kval});
           // console.log(tval);
         });
   angular.forEach(val.getVehicleFuelFillData, function(kval, tkey){
     ret_obj[key].fuelFillTodays.push({fuelFillToday:kval});
           // console.log(tval);
         });
         angular.forEach(val.getVehicleLphData, function(kval, tkey){
          ret_obj[key].lphTodays.push({lphToday:kval});
                   // console.log(tval);
           });

 });   

  return ret_obj;
}


 /*function distanceMonth(data){

  var ret_obj  = [] ;
  var preVal   = "" ;
    var curVal   = "" ;
    var firstVal =  0 ;
    var distVehi =  0 ;
    $scope.totDistY =  [] ;
      
    angular.forEach(data.executiveReportsForDistances, function(val, key){

        curVal=val.vehicleId.toString();

         if(key==0){

           ret_obj.push({VehiId:val.vehicleId});
             ret_obj[firstVal].dateDist=[];
             ret_obj[firstVal].dateDist.push({date:val.date,distanceToday:val.distanceToday});
             distVehi=distVehi+val.distanceToday;
             preVal=val.vehicleId.toString();
           
          }
      
      if(key>0){
             if(preVal==curVal){
      
                 ret_obj[firstVal].dateDist.push({date:val.date,distanceToday:val.distanceToday});
                 distVehi=distVehi+val.distanceToday;
                 preVal=val.vehicleId.toString();

             }else{

                 firstVal++;
                 $scope.totDistY.push({totDist:distVehi});
                 ret_obj.push({VehiId:val.vehicleId});
                 ret_obj[firstVal].dateDist=[];
                 preVal=val.vehicleId.toString();

              }
           }

    }) 

  $scope.totDistY.push({totDist:distVehi});
    
return ret_obj;
}

*/
function setBtnEnable(btnName){
 $scope.yesterdayDisabled = false;
 $scope.weekDisabled = false;
 $scope.monthDisabled = false;
 $scope.todayDisabled = false;
 $scope.lastmonthDisabled = false;
 $scope.thisweekDisabled = false;

 switch(btnName){

  case 'yesterday':
  $scope.yesterdayDisabled = true;
  break;
  case 'today':
  $scope.todayDisabled = true;
  break;
  case 'thisweek':
  $scope.thisweekDisabled = true;
  break;
  case 'lastweek':
  $scope.weekDisabled = true;
  break;
  case 'month':
  $scope.monthDisabled = true;
  break;
  case 'lastmonth':
  $scope.lastmonthDisabled = true;
  break;
}


}
setBtnEnable("init");
$scope.durationFilter    =   function(duration){
  //alert('inside function');
  startLoading();
  var now = new Date();
  $scope.todate       = getTodayDate(now.setDate(now.getDate() - 1));
  switch(duration){

    case 'yesterday':
    setBtnEnable('yesterday');
    var d = new Date();
    if($scope.tab=="1" || $scope.tab=="4"){
      if($scope.showOrgTime){
        $scope.fromdate    = getTodayDate(d.setDate(d.getDate() - 1));
        $scope.todate    = getTodayDate(d.setDate(d.getDate()));
        calculateDateTime();}
        else {
          $scope.fromdate= getTodayDate(d.setDate(d.getDate() - 1));
        }
      }
      else {
        $scope.fromdate= getTodayDate(d.setDate(d.getDate() - 1));
      }
      $scope.totime   = '11:59 PM';
      // datechange();
      break;
      case 'thisweek':
      setBtnEnable('thisweek');
      var d=new Date();
      var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1);
      var firstday= new Date(d.getFullYear(), d.getMonth(), diff);
      
      if($scope.tab=="1" || $scope.tab=="4"){
        if($scope.showOrgTime){
          $scope.fromdate    = getTodayDate(firstday.setDate(firstday.getDate() - 1));
          $scope.todate    = getTodayDate(new Date().setDate(new Date().getDate()-1));
          calculateDateTime();
        }
        else {
          $scope.fromdate    = getTodayDate(firstday.setDate(firstday.getDate()-1 ));
          $scope.todate= getTodayDate(new Date().setDate(new Date().getDate()-1 ));}
        }
        else {
          $scope.fromdate    = getTodayDate(firstday.setDate(firstday.getDate() ));
          $scope.todate= getTodayDate(new Date().setDate(new Date().getDate() ));
        }

        $scope.totime      = formatAMPM(d);
      //datechange();
      break;
      case 'lastweek':
      setBtnEnable('lastweek');
      var d = new Date();
      var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1)-7;
      var diff1=d.getDate() - day + (day == 0 ? -6:1)-1;
      var firstday= new Date(d.getFullYear(), d.getMonth(), diff);
      var lastday= new Date(d.getFullYear(), d.getMonth(), diff1);

      $scope.fromdate    = getTodayDate(firstday.setDate(firstday.getDate() ));
      $scope.todate      = getTodayDate(lastday.setDate(lastday.getDate() ));
      $scope.totime = '11:59 PM';
      //datechange();
      break;
      case 'month':
      setBtnEnable('month');
      var date = new Date();
      var firstdate = new Date(date.getFullYear(), date.getMonth(), 1);
      if($scope.tab=="1" || $scope.tab=="4"){
        if($scope.showOrgTime ){
          $scope.fromdate    = getTodayDate(firstday.setDate(firstdate.getDate() - 1));
          $scope.todate    = getTodayDate(new Date().setDate(new Date().getDate()-1));
          calculateDateTime();
        }
        else {
          $scope.fromdate    = getTodayDate(firstdate.setDate(firstdate.getDate() ));
          $scope.todate= getTodayDate(new Date().setDate(new Date().getDate()-1 ));}
        }
        else {
          $scope.fromdate       = getTodayDate(firstdate.setDate(firstdate.getDate() ));
          $scope.todate       = getTodayDate(date.setDate(date.getDate() ));}

          $scope.totime       = formatAMPM(date);
       //datechange();
       break;
       case 'today':
       setBtnEnable('today');
       var d = new Date();
       if($scope.tab=="1" || $scope.tab=="4"){
        if($scope.showOrgTime){
          $scope.fromdate       = getTodayDate(d.setDate(d.getDate()-1 ));
          $scope.todate       = getTodayDate(d.setDate(d.getDate() ));
        }
        else {
          $scope.fromdate       = getTodayDate(d.setDate(d.getDate() ));
          $scope.todate       = getTodayDate(d.setDate(d.getDate() ));}
        }
        else {
         $scope.fromdate       = getTodayDate(d.setDate(d.getDate() ));
         $scope.todate       = getTodayDate(d.setDate(d.getDate() ));}
         $scope.totime       = formatAMPM(d);
      //datechange();
      break;
      case 'lastmonth':
      setBtnEnable('lastmonth');
      var date = new Date();
      var firstdate = new Date(date.getFullYear(), date.getMonth()-1, 1);
      var lastdate = new Date(date.getFullYear(), date.getMonth(), 0);
      
      $scope.fromdate       = getTodayDate(firstdate.setDate(firstdate.getDate() ));
      $scope.todate       = getTodayDate(lastdate.setDate(lastdate.getDate() ));

      $scope.totime   = '11:59 PM';
      //datechange();
      break;
    }
    serviceCall();
  }

  $scope.formatDate=function(date){
    return moment(date,'YYYY-MM-DD').format('DD-MM-YYYY');
  }

  function convert_to_24h(time_str) {
    //console.log(time_str);
    var str   = time_str.split(' ');
    var stradd  = str[0].concat(":00");
    var strAMPM = stradd.concat(' '+str[1]);
    var time = strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
    var hours = Number(time[1]);
    var minutes = Number(time[2]);
    var seconds = Number(time[2]);
    var meridian = time[4].toLowerCase();

    if (meridian == 'p' && hours < 12) {
      hours = hours + 12;
    }
    else if (meridian == 'a' && hours == 12) {
      hours = hours - 12;
    }     
    var marktimestr = ''+hours+':'+minutes+':'+seconds;     
    return marktimestr;
  };

  function serviceCall(shortName,date){

  //console.log("service....");

  var fromdate=moment($scope.fromdate,'DD-MM-YYYY').format('YYYY-MM-DD');
  var todate=moment($scope.todate,'DD-MM-YYYY').format('YYYY-MM-DD');


  if( tabId == 'executive' || tabId == 'poi' || tabId == 'execFuel'){
    if((checkXssProtection($scope.fromdate) == true) && (checkXssProtection($scope.todate) == true)){
      if(tabId == 'executive' || $scope.actCons==true ){
        $scope.fromTime     =   '12:00 AM';
        $scope.totime         =   '11:59 PM';
        var shifttime=$scope.orgTime=='12:00 - 23:59'?0:$scope.orgTime;
        if($scope.showOrgTime){
          var groupUrl    = GLOBAL.DOMAIN_NAME+'/getExecutiveReport?groupId='+$scope.viewGroup.group+'&fromDate='+fromdate+'&toDate='+todate+'&vehicleId='+$scope.trackVehID+'&shiftTime='+shifttime;}
          else {
            var groupUrl    = GLOBAL.DOMAIN_NAME+'/getExecutiveReport?groupId='+$scope.viewGroup.group+'&fromDate='+fromdate+'&toDate='+todate+'&vehicleId='+$scope.trackVehID;
          }
          var webServiceUrl     =   GLOBAL.DOMAIN_NAME+'/getDailyDriverPerformance?groupId='+$scope.viewGroup.group+'&fromUtcTime='+utcFormat($scope.fromdate,convert_to_24h($scope.fromTime))+'&toUtcTime='+utcFormat($scope.todate,convert_to_24h($scope.totime));
          vamoservice.getDataCall(groupUrl).then(function(responseGroup){
            var tagsCheck   = (responseGroup.error) ? true :  false;
        // console.log($scope.to_trusted($scope.fromdate));
        $scope.execGroupReportData    = [];
        $scope.ReducedData=[];
        $scope.allData = [];
        var totalDistance=0.0;
        var totalRunningTime=0;
        var totalParkedTime=0;
        var totalNoDataTime=0;
        var totalIdleTime=0;
        var parkingCount=0;
        var overSpeedInstances=0;
        var i=0;
        if(tagsCheck == false)
          if(vehicleSelected){
          //$scope.donut  =   false;
          //$scope.donut_new=   false;
          $scope.donut  = 1;
          $scope.bar    = false;
          $('#singleDiv').show(500);
          $scope.execGroupReportData  = ($filter('filter')(responseGroup.execReportData, {'vehicleId':vehicleSelected},true));
          startLoading();
          $scope.ReducedData.push({'vehicleId':vehicleSelected});
          totalDistance=0.0;
          totalRunningTime=0;
          totalParkedTime=0;
          totalNoDataTime=0;
          totalIdleTime=0;
          parkingCount=0;
          overSpeedInstances=0;
          var key = 0;
          $scope.allData  = ($filter('filter')(responseGroup.execReportData, {'vehicleId':vehicleSelected},true)).sort(function(a, b){
            var dateA=new Date(a.date), dateB=new Date(b.date)
                                          return dateA-dateB //sort by date ascending
                                        });
          angular.forEach($scope.allData,function(vehicleInfo, index){
            $scope.ReducedData[key].vehicleMode=vehicleInfo.vehicleMode;
            if(index==0){
              $scope.ReducedData[key].allData=[];
              $scope.ReducedData[key].shortName=vehicleInfo.shortName;
              $scope.ReducedData[key].fDate=vehicleInfo.date;
              $scope.ReducedData[key].sLocation=vehicleInfo.startLocation;
              $scope.ReducedData[key].tDate=$scope.allData[$scope.allData.length-1].date;
              $scope.ReducedData[key].eLocation=$scope.allData[$scope.allData.length-1].endLocation;
              $scope.ReducedData[key].power=vehicleInfo.batteryPowerStatus;
              $scope.ReducedData[key].position=vehicleInfo.position;
              $scope.ReducedData[key].allData=$scope.allData;
              totalDistance=totalDistance+vehicleInfo.distanceToday;
              totalRunningTime=totalRunningTime+vehicleInfo.totalRunningTime;
              totalParkedTime=totalParkedTime+vehicleInfo.totalParkedTime;        
              totalNoDataTime=totalNoDataTime+vehicleInfo.totalNoDataTime;
              totalIdleTime= totalIdleTime+vehicleInfo.totalIdleTime;
              parkingCount=parkingCount+vehicleInfo.parkingCount;
              overSpeedInstances=overSpeedInstances+vehicleInfo.overSpeedInstances;
            }else{
              totalDistance=totalDistance+vehicleInfo.distanceToday;
              totalRunningTime=totalRunningTime+vehicleInfo.totalRunningTime;
              totalParkedTime=totalParkedTime+vehicleInfo.totalParkedTime;        
              totalNoDataTime=totalNoDataTime+vehicleInfo.totalNoDataTime;
              totalIdleTime= totalIdleTime+vehicleInfo.totalIdleTime;
              parkingCount=parkingCount+vehicleInfo.parkingCount;
              overSpeedInstances=overSpeedInstances+vehicleInfo.overSpeedInstances;

            }
            if(index==$scope.allData.length-1){
              $scope.ReducedData[key].tDistance=totalDistance;
              $scope.ReducedData[key].tRunningTime=totalRunningTime;
              $scope.ReducedData[key].tParkedTime=totalParkedTime;
              $scope.ReducedData[key].tNoDataTime=totalNoDataTime;
              $scope.ReducedData[key].tIdleTime=totalIdleTime;
              $scope.ReducedData[key].parkCount=parkingCount;
              $scope.ReducedData[key].overSpeed=overSpeedInstances;

            }



          })

          

          barLoad(vehicleSelected);
          stopLoading();
        } else {
          //$scope.donut  =   false;
          //$scope.donut_new=   false;
          $scope.bar    = true;
          $('#singleDiv').hide();
          $scope.execGroupReportData  = responseGroup.execReportData;
          // added for single vehicle / single row consolidated report
          startLoading();
          $scope.ReducedData = [];
          angular.forEach($scope.vehicle_list[$scope.gIndex].vehicleLocations,function(val, key){
            //alert($scope.execGroupReportData.filter(item=> item.vehicleId == val.vehicleId).length);
            if($scope.execGroupReportData.filter(item=> item.vehicleId == val.vehicleId).length){
              $scope.ReducedData.push({'vehicleId':val.vehicleId , 'shortName' : val.shortName });
              totalDistance=0.0;
              totalRunningTime=0;
              totalParkedTime=0;
              totalNoDataTime=0;
              totalIdleTime=0;
              parkingCount=0;
              overSpeedInstances=0;
              $scope.allData  = ($filter('filter')($scope.execGroupReportData, {'vehicleId':val.vehicleId},true)).sort(function(a, b){
                var dateA=new Date(a.date), dateB=new Date(b.date)
                                          return dateA-dateB //sort by date ascending
                                        });
              angular.forEach($scope.allData,function(vehicleInfo, index){
                $scope.ReducedData[key].vehicleMode=vehicleInfo.vehicleMode;
                if(index==0){
                  $scope.ReducedData[key].allData=[];
                  $scope.ReducedData[key].fDate=vehicleInfo.date;
                  $scope.ReducedData[key].sLocation=vehicleInfo.startLocation;
                  $scope.ReducedData[key].tDate=$scope.allData[$scope.allData.length-1].date;
                  $scope.ReducedData[key].eLocation=$scope.allData[$scope.allData.length-1].endLocation;
                  $scope.ReducedData[key].power=vehicleInfo.batteryPowerStatus;
                  $scope.ReducedData[key].position=vehicleInfo.position;
                  $scope.ReducedData[key].allData=$scope.allData;
                  totalDistance=totalDistance+vehicleInfo.distanceToday;
                  totalRunningTime=totalRunningTime+vehicleInfo.totalRunningTime;
                  totalParkedTime=totalParkedTime+vehicleInfo.totalParkedTime;        
                  totalNoDataTime=totalNoDataTime+vehicleInfo.totalNoDataTime;
                  totalIdleTime= totalIdleTime+vehicleInfo.totalIdleTime;
                  parkingCount=parkingCount+vehicleInfo.parkingCount;
                  overSpeedInstances=overSpeedInstances+vehicleInfo.overSpeedInstances;
                }else{
                  totalDistance=totalDistance+vehicleInfo.distanceToday;
                  totalRunningTime=totalRunningTime+vehicleInfo.totalRunningTime;
                  totalParkedTime=totalParkedTime+vehicleInfo.totalParkedTime;        
                  totalNoDataTime=totalNoDataTime+vehicleInfo.totalNoDataTime;
                  totalIdleTime= totalIdleTime+vehicleInfo.totalIdleTime;
                  parkingCount=parkingCount+vehicleInfo.parkingCount;
                  overSpeedInstances=overSpeedInstances+vehicleInfo.overSpeedInstances;

                }
                if(index==$scope.allData.length-1){
                  $scope.ReducedData[key].tDistance=totalDistance;
                  $scope.ReducedData[key].tRunningTime=totalRunningTime;
                  $scope.ReducedData[key].tParkedTime=totalParkedTime;
                  $scope.ReducedData[key].tNoDataTime=totalNoDataTime;
                  $scope.ReducedData[key].tIdleTime=totalIdleTime;
                  $scope.ReducedData[key].parkCount=parkingCount;
                  $scope.ReducedData[key].overSpeed=overSpeedInstances;

                }



              })
            }
          })


          donutLoad(responseGroup);
          OverallDriverPerformance(webServiceUrl);
          startLoading();
          
        }
        else
          barLoad(vehicleSelected),donutLoad(responseGroup);
        stopLoading();
      })
} else if(tabId == 'poi' || $scope.actTab == true){
  $scope.donut    =   true;
  $scope.donut_new    =   false;
  $scope.bar      =   true;
  $('#singleDiv').hide();
  var poiUrl      = GLOBAL.DOMAIN_NAME+'/getPoiHistory?groupId='+$scope.viewGroup.group+'&fromDate='+fromdate+'&toDate='+todate;
  vamoservice.getDataCall(poiUrl).then(function(responsePoi){
    $scope.geofencedata     =   [];
    if(responsePoi.history !=null)
      if(responsePoi.history.length>0)
        $scope.geofencedata   =     responsePoi.history;
      stopLoading();
    })
} else if(tabId == 'execFuel' || $scope.actFuel == true){
  $scope.donut    =   true;
  $scope.donut_new    =   true;
  $scope.bar      =   true;
  $('#singleDiv').hide();
  var shifttime=$scope.orgTime=='12:00 - 23:59'?0:$scope.orgTime;
  if($scope.showOrgTime){
    var fuelUrl =GLOBAL.DOMAIN_NAME+'/getExecutiveFuelReport?groupId='+$scope.viewGroup.group+'&fromDate='+fromdate+'&toDate='+todate+'&shiftTime='+shifttime;}
    else {
      var fuelUrl =GLOBAL.DOMAIN_NAME+'/getExecutiveFuelReport?groupId='+$scope.viewGroup.group+'&fromDate='+fromdate+'&toDate='+todate;
    }
      // console.log(fuelUrl);
      $scope.execFuelData   = [];
      $http.get(fuelUrl).success(function(data){
        stopLoading();
        if(data=='Failed'){
          $scope.execFuelData = [{ error : translate("curl_error") }];
        }else{
          $scope.execFuelData  = data;
          chartFuel($scope.execFuelData);
        }
      });
    }

  }else {

    $scope.barArray1      = [];
    $scope.barArray2      = [];
    $scope.execGroupReportData  = [];
    $scope.geofencedata     = [];
    barLoad(vehicleSelected);
    donutLoad('');
    chartFuel('');
    stopLoading();
  }

}

else if(tabId == 'freezekm') {

 $scope.donut    =   true;
 $scope.donut_new    =   false;
 $scope.bar      =   true;
 $('#singleDiv').hide();
 $scope.curlError='';
 

 var fuelUrl =GLOBAL.DOMAIN_NAME+'/getOrgShiftVehiclesReport?groupId='+$scope.viewGroup.group+'&fromDateUtc='+utcFormatOnlyDate(fromdate)+'&toDateUtc='+utcFormatOnlyDate(todate);

      // console.log(fuelUrl);
      $scope.freezeKmData   = [];
      $http.get(fuelUrl).success(function(data){
        stopLoading();
        if(data=='Failed'){
          $scope.curlError  = translate("curl_error");
        }else{
          $scope.freezeKmData  = data;
          
        }
      });


    }

    else if(tabId == 'month' || $scope.actMonth == true) {

      $scope.donut    =   true;
      $scope.donut_new    =   false;
      $scope.bar      =   true;
      $('#singleDiv').hide();

      var monthUrl =GLOBAL.DOMAIN_NAME+'/getExecutiveReportVehicleDistance?groupId='+$scope.viewGroup.group+'&month='+$scope.monValss+'&year='+$scope.yearValss;
     // console.log(monthUrl);

     $scope.monthData  = [];
     monFuelScroll=0;
     $http.get(monthUrl).success(function(data){
      monFuelScroll=1;
      $scope.monthData = data;
        //  console.log($scope.monthData);

        $scope.monthDates=[];
        for(var i=0;i<$scope.lenMon;i++){
          $scope.monthDates.push(i+1);
        }

        $scope.distMonData=[];
        $scope.distMonData=distanceMonth($scope.monthData);

        $scope.totDistVehic=[];
        if(data.comulativeDistance!=null){

         for(var i=0;i<data.comulativeDistance.length;i++){
           const sum = $scope.distMonData.reduce(function (result, item) {
            return parseFloat(result) + parseFloat(item.distsTodays[i].distanceToday);
          }, 0);
           $scope.totDistVehic.push(sum); 
         }
       }
             //console.log($scope.distMonData);
             //console.log(daysInThisMonth());
             $scope.showMonTable=true;
             setTimeout(function(){ stopLoading(); enhance(); }, 100);

           });

   } else if(tabId == 'monthFuel' || $scope.actMonthFuel == true) {

    $scope.donut      =   true;
    $scope.donut_new  =   false;
    $scope.bar        =   true;
    $('#singleDiv').hide();
    monthExeFuelScroll=0;
    var monthFuelUrl = GLOBAL.DOMAIN_NAME+'/getMonthlyExecutiveDistanceAndFuel?groupId='+$scope.viewGroup.group+'&month='+$scope.monFuelVal+'&year='+$scope.yearFuelVal;
         //console.log(monthFuelUrl);
         monthExeFuelScroll=1;
         $scope.monthFuelData  = [];
         $http.get(monthFuelUrl).success(function(data){
          $scope.showMonFuelTable=true;
          if(data=='Failed'){
            $scope.monthFuelData = { 'error' : translate("curl_error") };
          }else{
            $scope.monthFuelData = data;
               // console.log($scope.monthFuelData);

               $scope.monthFuelDates=[];
               for(var i=0;i<$scope.lenMonss;i++){
                $scope.monthFuelDates.push(i+1);
              }

              $scope.distMonFuelData=[];
              $scope.distMonFuelData=distanceFuelMonth($scope.monthFuelData);

              $scope.totMonDistVehic=[];
              if(data.cumulativeVehicleDistanceData!=null){
                for(var i=0;i<data.cumulativeVehicleDistanceData.length;i++){
                 const sum = $scope.distMonFuelData.reduce(function (result, item) {
                  if(item.vehicleMode!='DG'){
                   return parseFloat(result) + parseFloat(item.distsTodays[i].distanceToday);
                 }else{
                  return parseFloat(result);
                }

              }, 0);
                 $scope.totMonDistVehic.push(sum); 
               }
             }

             $scope.totMonFuelVehic=[];
             if(data.cumulativeVehicleFuelData!=null){

               for(var i=0;i<data.cumulativeVehicleFuelData.length;i++){
                 $scope.totMonFuelVehic.push(data.cumulativeVehicleFuelData[i]); 
               }
             }
             $scope.totMonLph=[];
             if(data.cumulativeVehicleLphData!=null){

               for(var i=0;i<data.cumulativeVehicleLphData.length;i++){
                 $scope.totMonLph.push(data.cumulativeVehicleLphData[i]); 
               }
             }



             $scope.totMonKmplVehic=[];
             if(data.cumulativeVehicleKmplData!=null){

               for(var i=0;i<data.cumulativeVehicleKmplData.length;i++){
                 $scope.totMonKmplVehic.push(data.cumulativeVehicleKmplData[i]); 
               }
             }

             $scope.totMonFuelFill=[];
             if(data.cumulativeVehicleFuelFillData!=null){

               for(var i=0;i<data.cumulativeVehicleFuelFillData.length;i++){
                 $scope.totMonFuelFill.push(data.cumulativeVehicleFuelFillData[i]); 
               }
             }
                 //console.log($scope.distMonFuelData);
                 //console.log(daysInThisMonth());



               }
               setTimeout(function(){ stopLoading(); enhance(); }, 100);
             });
       } 
     }

     $scope.plotHist   = function()
     {
       $scope.yesterdayDisabled = false;
       $scope.weekDisabled = false;
       $scope.monthDisabled = false;
       $scope.todayDisabled = false;
       $scope.lastmonthDisabled = false;
       $scope.thisweekDisabled = false;
       startLoading();
       $scope.fromdate   =   $('#dtFrom').val();
       $scope.todate   =   $('#dtTo').val();
       if($scope.showOrgTime){
        calculateDateTime();}
        serviceCall();
  // stopLoading();
}

//tab click method
$scope.alertMe    =   function(tabClick)
{ 
  monFuelScroll=0;
  monthExeFuelScroll=0;
  $scope.tapchanged=true;
  if(avoidOnload == true)
    switch (tabClick){
      case 'executive':
      startLoading();
      tabId               = 'executive';
      $scope.sort         = sortByDate('date');
      $scope.downloadid   = 'executive';
      $scope.tabActive    = true;
      $scope.actTab       = false;
      $scope.actFuel      = false;
      $scope.actMonth     = false;
      $scope.actMonthFuel = false;
      $scope.actCons      = false;
      $scope.showDate     = true;
      $scope.showMonth    = false;
      $scope.showMonthFuel = false;
      serviceCall();
      $scope.donut        = false;
      $scope.donut_new    = false;
      $scope.buttonShow=true;
      break;
      case 'poi':
      startLoading();
      $scope.sort         = sortByDate('time');
      tabId               = 'poi';
      $scope.downloadid   = 'poi';
      $scope.tabActive    = false;
      $scope.actTab       = true;
      $scope.actFuel      = false;
      $scope.actMonth     = false;
      $scope.actMonthFuel = false;
      $scope.actCons      = false;
      $scope.showDate     = true;
      $scope.showMonth    = false;
      $scope.showMonthFuel = false;
      $scope.buttonShow=true;
      serviceCall();
      break;
      case 'consolidated' :
      startLoading();
      $scope.sort         = sortByDate('date');
      tabId               = 'executive';
      $scope.downloadid   = 'consolidated';
      $scope.tabActive    = false;
      $scope.actTab       = false;
      $scope.actFuel      = false;
      $scope.actMonth     = false;
      $scope.actMonthFuel = false;
      $scope.actCons      = true;
      $scope.showDate     = true;
      $scope.showMonth    = false;
      $scope.showMonthFuel = false;
      serviceCall();
      $scope.donut        = false;
      $scope.donut_new    = false;
      $scope.buttonShow=true;
      break;
      case 'fuel' :
      startLoading();
      $scope.sort         = sortByDate('date');
      tabId               = 'execFuel';
      $scope.downloadid   = 'execFuel';
      $scope.tabActive    = false;
      $scope.actTab       = false;
      $scope.actFuel      = true;
      $scope.actMonth     = false;
      $scope.actMonthFuel = false;
      $scope.actCons      = false;
      $scope.showDate     = true;
      $scope.showMonth    = false;
      $scope.showMonthFuel = false;
      $scope.buttonShow=true;
      serviceCall();
      break;
      case 'distMonth' :
      startLoading();
      $scope.sort         = sortByDate('date');
      $scope.tabActive    = false;
      $scope.actTab       = false;
      $scope.actFuel      = false;
      $scope.actMonth     = true;
      $scope.actMonthFuel = false;
      $scope.actCons      = false;
      $scope.showMonTable = false;
      tabId               = 'month';
      $scope.downloadid   = 'month';
      $scope.showDate     = false;
      $scope.showMonthFuel = false;
      $scope.showMonth    = true;
      $scope.buttonShow=false;
      serviceCall();
      break;
      case 'distMonthFuel' :
      startLoading();
      tabId  = 'monthFuel';
      $scope.sort         = sortByDate('date');
      $scope.tabActive    = false;
      $scope.actTab       = false;
      $scope.actFuel      = false;
      $scope.actMonth     = false;
      $scope.actCons      = false;
      $scope.actMonthFuel = true;
      $scope.showMonFuelTable = false;
      $scope.downloadid   = 'monthFuel';
      $scope.showDate     = false;
      $scope.showMonth    = false;
      $scope.showMonthFuel = true;
      $scope.buttonShow=false;
      serviceCall();
      break;    
      default :
      break;
    }
  } 

  var OverallDriverPerformance = function(url)
  {
    startLoading();
    var totalsuddenBreak=[];
    var SuddenAcc=[];
    var OverSpeed=[];
    var sparkAlarm=[];
    var vehiclename=[];
    var kiloMeter=[];
    var i=0;
    //console.log('inside the url')
    
    $http.get(url).success(function(data)
    { 
      // console.log(arrangeMonth());
      // if($scope.groupVeh == false)
      if(data==""){
        $scope.showDriverChart=false;
      }
      else {  
        $scope.showDriverChart=true;
        $scope.tableValue=[];
        dataTableList=[];
        for(i; i<data.length; i++)
        {
          if(!data[i].error || data[i].error == null){
            vehiclename.push(data[i].shortName);
            totalsuddenBreak.push(data[i].weightedBreakAnalysis);
            SuddenAcc.push(data[i].weightedAccelAnalysis);
            OverSpeed.push(data[i].weightedSpeedAnalysis);
            sparkAlarm.push(data[i].weightedShockAlarmAnalysis);
            kiloMeter.push(data[i].distance);
            dataTableList.push({'month':data[i].shortName,'data': data[i]});  
          } 

        }
        $scope.tableValue=dataTableList;
        stopLoading();
      //group value charts
      $('#driverChart').highcharts({
        chart: {
          height: vehiclename.length>100?(32 / 16 * 100) + '%':(16 / 16 * 100) + '%',
          width:900,
           // marginLeft: 125,
           type: 'bar',
         },
         title: {
          text: translate('Drivers_Performance_chart')
        },
        subtitle: {
          align: 'right',
          x: 10,
          verticalAlign: 'top',
          y: 30,
          text: translate('Total_Distance'),
          style: {
            color: Highcharts.getOptions().colors[2]
          }
          
        },
        xAxis: [{
          categories: vehiclename,
          reversed: false,
          labels: {
            step: 1
          }
          }, { // mirror axis on right side
            opposite: true,
            reversed: false,
            categories: kiloMeter,
            linkedTo: 0,
            labels: {
              step: 1,
             //format: vehiclename, 
           },
         }],
         yAxis: {
          min: 0,
          
          /*title: {
            text: vehicleno
          }*/
        },
        tooltip: {
          shared: true,
        },

        legend: {
          reversed: true
        },
        /*plotOptions: {

          series: {
            stacking: 'normal'
          }
        },*/
        plotOptions: {
          series: {
             // pointWidth: 7,
             stacking: 'normal',
             cursor: 'pointer',
           }
         },
         series: [{
          name: translate('Brake_Analysis'),
          data: totalsuddenBreak
        }, {
          name: translate('Speed_Analysis'),
          data: OverSpeed
        }, {
          name: translate('Shock_Analysis'),
          data: sparkAlarm
        }, {
          name: translate('Acceleration_Analysis'),
          data: SuddenAcc
        }]

      });
    }
  })

  }

  function arrangeMonth(){

    var monthIndex=0;
    arrangeMonthList =[];
    monthIndex=monthNames.indexOf($scope.month);
    for (var j=0; j<12; j++) 
    {
      arrangeMonthList.push(monthNames[monthIndex]);
      monthIndex--; 
      if (monthIndex < 0) 
      {
        monthIndex = 11;
      }
    }
    return arrangeMonthList;
  };

  $scope.roundOffDecimal = function(val) {
    return Number.parseFloat(val).toFixed(2);
  }
  $scope.exportData = function (data) {
      //console.log(data);
      var blob = new Blob([document.getElementById(data).innerHTML], {
        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
      });
      saveAs(blob, data+".xls");
      if(tabId == "month" || tabId == "monthFuel"){
        enhance();
      }
    };
    
    $scope.exportDataCSV = function (data) {
    //console.log(data);
    CSV.begin('#'+data).download(data+'.csv').go();
    if(tabId == "month"){
      enhance();
    }else if(tabId == "monthFuel"){
      setTimeout(function(){ enhance(); }, 50);
    }
  };
  $scope.exportPDF = function (rep_name) {
    let tableName,id;
    switch(rep_name){

      case 'poi':
      tableName="Place Of Interest"
      id="poiId"
      break;

      case 'month':
      tableName='Monthly_Report'+'-'+$scope.monValFrontss+$scope.curYearFrontss;
      id='month'
      break;

      case 'monthFuel':
      tableName='Vehicle_Distance_and_Fuel';
      id="MonthFuel"
      break;

      case 'executive':
      tableName='Vehiclewise Performance Report';
      id='Executive';
      break;

      case 'consolidated':
      tableName='Consolidated'
      id='Consolidated'
      break;

      case 'execFuel':
      tableName='Executive Fuel Report'
      id='ExecFuel'
      break;  
    }
    var doc = new jsPDF({
     orientation: 'l',
     unit: 'mm',
     format: 'a3',
   });
    var elem = document.getElementById(id);
    var data = doc.autoTableHtmlToJson(elem);
    doc.setFontSize(12);
    doc.text(`${tableName.toUpperCase()}`,130,10,'center');
    if(id==='Executive'){
     data.columns.shift();
     data.rows.map(v=>{
      v.shift();
    })
   }
      //data.rows.shift();
      doc.autoTable(data.columns, data.rows, {
        margin: [15,5,10,5],
        tableLineColor: [189, 195, 199],
        styles: {
          fontSize: 8,
          overflow: 'linebreak',
          lineWidth: 0.01
        }
      });
      doc.save(tableName+'.pdf');

    }
    
 // $scope.showAllData = function(vehid, index, click) {
 //            var id='allData'+vehid;
 //           if($("#id").hasClass("out")) {
 //            $("#collapseme").addClass("in");
 //            $("#collapseme").removeClass("out");
 //        } else {
 //            $("#collapseme").addClass("out");
 //            $("#collapseme").removeClass("in");
 //        }
 //    }

 $scope.expandAll = function(expanded) {
        // $scope is required here, hence the injection above, even though we're using "controller as" syntax
        $scope.$broadcast('onExpandAll', {
          expanded: expanded
        });
      };

      $(window).scroll(function (event) {
        if($scope.tapchanged){
          $scope.tapchanged=false;
          console.log('scrolled');
          switch(tabId){
            case 'executive':
                   // alert('all scroll');
                     // if($scope.downloadid=='executive'){
                     //     $('.table-fixed-header1').fixedHeader();
                     // }else 
                     if($scope.downloadid=='consolidated'){
                       $('.table-fixed-header3').fixedHeader();
                     }
                     break;
                     case 'poi':
                     $('.table-fixed-header2').fixedHeader();
                     break;
                     case 'execFuel':
                     $('.table-fixed-header4').fixedHeader();
                     break;
                  // case 'month':
                  //   if(monFuelScroll){
                  //      $('.table-fixed-header5').fixedHeader();
                  //   }
                  // break;
                  // case 'monthFuel':
                  //   if(monthExeFuelScroll){
                  //     $('.table-fixed-header6').fixedHeader();
                  //   }
                  // break;
                }

              }
            });

  // $scope.roundOffDecimal = function(val) {
  //        if(val){
  //          return parseFloat(val).toFixed(2);
  //        }else{
  //          return val;
  //        }

  // }  
  //from date changes
//   if($scope.tab=="1"){
//   $(function () {
//   $('#dateFrom, #dateTo').datetimepicker({
//     format:'DD-MM-YYYY',
//     useCurrent:true,
//     pickTime: false,
//     maxDate: moment().subtract(1, 'days'),
//     minDate: new Date(2015, 12, 1),
//   });

// });
// }



}]);