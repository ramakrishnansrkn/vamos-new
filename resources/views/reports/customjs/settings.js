app.controller('mainCtrl',['$scope','vamoservice','$timeout','_global','$http','$templateCache','$translate','$filter',function($scope, vamoservice, $timeout, GLOBAL, $http, $templateCache,$translate,$filter) {
 $scope.videoLink="https://www.youtube.com/watch?v=yjJbDfqyRT8&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=3";
 var language=localStorage.getItem('lang');
 $scope.multiLang=language;
 $translate.use(language);
 $scope.tapchanged=true;
 var translate = $filter('translate');
 $("#testLoad").load("../public/menu");
 $scope.tapchanged=true;
 $scope.isLoading=true;
// global variable

//var url                 =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations';
var menuValue           =  JSON.parse(localStorage.getItem('userIdName'));
var sp                  =  menuValue.split(",");
var usrName             =  menuValue.split(",");
var assLabel            =  localStorage.getItem('isAssetUser');

$scope.vehiView         =  true;
$scope.assetView        =  false;

if(assLabel=="true") {

  $scope.vehiLabel     = "Asset";
  $scope.vehiView      =  false;
  $scope.assetView     =  true;

} else if(assLabel=="false") {

  $scope.vehiLabel    = "Vehicle";
  $scope.vehiView     =  true;
  $scope.assetView    =  false;

} else {

  $scope.vehiLabel    = "Vehicle";
  $scope.vehiView     =  true;
  $scope.assetView    =  false;

}

//$scope.schReportShow        = false;
$scope.reportBanShow        = false;
$scope.delMonVal            = false;
$scope.reportDaily          = false;
$scope.reportMonthly        = false;
$scope.reportDailyAct       = false;
$scope.reportMonthlyAct     = false;

$scope.vehicles             = [];
$scope.groupList            = [];
$scope.checkingValue        = {};
$scope.checkingValue2       = {};
$scope.hourBasedValue       = {};
$scope.vehiId               = [];
$scope.grpId                = [];

$scope.from                 = "";
$scope.to                   = "";
$scope.from2                = "";
$scope.to2                  = "";

$scope.hoursFrom              = ['0:00','1:00','2:00','3:00','4:00','5:00','6:00','7:00','8:00','9:00','10:00', '11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00']
$scope.hoursTo                = ['0:59','1:59','2:59','3:59','4:59','5:59','6:59','7:59','8:59','9:59','10:59','11:59','12:59','13:59','14:59','15:59','16:59','17:59','18:59','19:59','20:59','21:59','22:59','23:59'];
$scope.hoursBasedHourDrop              = ['1:00','2:00','3:00','4:00','5:00','6:00','7:00','8:00','9:00','10:00', '11:00','12:00','13:00','14:00','15:00','16:00','17:00','18:00','19:00','20:00','21:00','22:00','23:00','24:00']
//$scope.reports                = ['Movement (M)','OverSpeed (O)','Site (S)', 'POI (PI)','Fuel (F)','Temperature (T)'];
$scope.monReport              = ['Consolidated Stoppage (CS)'];
$scope.monReportSel           = 'Consolidated Stoppage (CS)';
$scope.grpMailId              = "";
$scope.mailId2                = ""; 
$scope.grpReport              = ['Consolidated Reports (CR)','Fuel Consolidate (FC)','Travel Summary (TS)','No Data (ND)','Executive Fuel Report (EF)','Last Transmission Report (LTR)','Vehicle Wise Performance (VP)','Monthly Distance Report (MD)' ];
$scope.hourBasedReport              = ['Fuel Consolidate (FC)','Executive Fuel Report (EF)' ];
$scope.checkingValue.move     = [];
$scope.checkingValue.over     = [];
$scope.checkingValue.site     = [];
$scope.checkingValue.geo     = [];
$scope.checkingValue.poi      = [];
$scope.checkingValue.fuel     = [];
$scope.checkingValue.fuelRaw  = [];
//$scope.checkingValue.temp     = [];
$scope.checkingValue.cstop    = [];
$scope.checkingValue2.consr   = [];
$scope.checkingValue2.fuelCon = [];
$scope.checkingValue2.travSum = [];
$scope.checkingValue2.nodata = [];
$scope.checkingValue2.lastTran = [];
$scope.checkingValue2.exceFuel  = [];
$scope.checkingValue2.vehWisePer = [];
$scope.hourBasedValue.conFuel  = [];
$scope.hourBasedValue.exceFuelReport  = [];
$scope.checkingValue2.monthlyDistance = [];
$scope.reportUrl              = GLOBAL.DOMAIN_NAME+'/getReportsList';

$scope.$watch("reportUrl", function (val) {

  vamoservice.getDataCall($scope.reportUrl).then(function(data){
   var tabShow = data;
   console.log(tabShow); 
   if(tabShow != "" && tabShow != null)  {     
       // console.log('not Empty getReportList API ...');
       angular.forEach(tabShow,function(val, key){

        var newReportName = Object.getOwnPropertyNames(val).sort();

        if(newReportName == 'Scheduled_IndivdiualReportsList') {

          var reportSubName = Object.getOwnPropertyNames(val.Scheduled_IndivdiualReportsList[0]).sort();
          console.log(reportSubName);

          angular.forEach(reportSubName,function(value, keys) {
            switch(value) {
              case 'SCHEDULED_REPORTS':
              $scope.reportDaily = val.Scheduled_IndivdiualReportsList[0].SCHEDULED_REPORTS;
              if($scope.reportDaily==true) {
                $scope.reportDailyAct    = true;
                $scope.reportMonthlyAct  = false;
              }
              break;
              case 'SCHEDULED_REPORTS_MONTHLY':
              $scope.reportMonthly = val.Scheduled_IndivdiualReportsList[0].SCHEDULED_REPORTS_MONTHLY;
              if($scope.reportDaily==false&&$scope.reportMonthly==true) {
                $scope.reportDailyAct   = false;
                $scope.reportMonthlyAct = true;
              }
              break;    
            }
          });

          if(val.Scheduled_IndivdiualReportsList[0].SCHEDULED_REPORTS == false && val.Scheduled_IndivdiualReportsList[0].SCHEDULED_REPORTS_MONTHLY == false){

            $scope.reportDailyAct      = false;
            $scope.reportMonthlyAct    = false;
            $scope.reportBanShow       = true;

          } else{
            $scope.url = GLOBAL.DOMAIN_NAME+'/getVehicleLocations';
            initFunc();
          }

          //stopLoading();

        }

      });

     } else{
      console.log('Empty getReportList API ...');

      $scope.reportDaily      = true;
      $scope.reportMonthly    = true;
      $scope.reportBanShow    = false;

      $scope.url  = GLOBAL.DOMAIN_NAME+'/getVehicleLocations';
      initFunc();
    }

  });

});


$scope.getReportNames = function() {
  $.ajax({
    async: false,
    method: 'GET', 
    url: "ScheduledController/getRepName",
    //data: value,
    success: function (response) {
      if(assLabel=="true") {
       $scope.repNames = [];
       console.log(response);

       for(var i=0;i<response.length;i++){
        if(response[i]=="Movement(M)"){
         $scope.repNames.push(response[i]);
       }
     }
   } else {
     console.log(response);
     $scope.repNames = [];
           //$scope.repNames = response;
           for(var i=0;i<response.length;i++){

            if(response[i]!="Fuel(F)" && response[i]!="FuelRaw(FR)" && response[i]!="POI(P)"){
             $scope.repNames.push(response[i]);
           }
         }
       }
     }

   });

  return $scope.repNames;  
}

$scope.reports  = $scope.getReportNames();


//onload function for value already present

function fetchController(group) {
  $scope.error = "";
  console.log('fetchController');
  $http.get("getScheduledReportDetails?group="+group)
  .success(function (response) {
    console.log("success");

    stopLoading();
     $scope.isLoading=false;
    if(response.response == "Success"){
      var initVal=0;

      console.log(response);
      let scheduleData = response.data;
      if(scheduleData.length){

        angular.forEach($scope.vehicles, function(value, key){
          $scope.vehiId[key]                  = false;
          $scope.checkingValue.move[key]      = false;
          $scope.checkingValue.over[key]      = false;
          $scope.checkingValue.site[key]      = false;
          $scope.checkingValue.geo[key]      = false;
          $scope.checkingValue.poi[key]       = false;
          $scope.checkingValue.fuel[key]      = false;
          $scope.checkingValue.fuelRaw[key]   = false;
                // $scope.checkingValue.temp[key]   = false;
                angular.forEach(scheduleData, function(innerValue, innerKey){
                  if(value.vehicleId == innerValue.vehicleId){


                    if(initVal==0){

                      $scope.mailId           =  scheduleData[innerKey].emailIds;
                      $scope.from             =  scheduleData[innerKey].fromTime;
                      $scope.to               =  scheduleData[innerKey].toTime;
                      $scope.groupSelected    =  scheduleData[innerKey].group;

                      initVal++;
                    }


                    $scope.vehiId[key] = true;
                    // console.log(innerValue.reports)
                    var reportsValue  =  innerValue.reports.split(',');
                    try{

                      angular.forEach(reportsValue, function(rep, ind){

                        switch (rep){
                          case 'movement':
                          $scope.checkingValue.move[key]     =  true;
                          break;
                          case 'overspeed':
                          $scope.checkingValue.over[key]     =  true;
                          break;
                          case 'site':
                          $scope.checkingValue.site[key]     =  true;
                          break;
                          case 'geofence_fuel':
                          $scope.checkingValue.geo[key]     =  true;
                          break;
                          case 'poi':
                          $scope.checkingValue.poi[key]      =  true;
                          break;
                          case 'fuel':
                          $scope.checkingValue.fuel[key]     =  true;
                          break;
                          case 'fuelRaw':
                          $scope.checkingValue.fuelRaw[key]  =  true;
                          break;  
                        }

                      });

                    } catch(err){

                      console.log(err)

                    }
                    
                  }
                });

              });
      }
    }

  })
  .error(function (response) {
    console.log("fail");
  });
}

$scope.handleTapChange  =  function(){
  $scope.error = "";
}
$scope.fetchController2  =  function(){
  $scope.error = "";
  console.log('fetchController2');
  $http.get("getScheduledReportDetails?type=group")
  .success(function (response) {
    console.log("success");
    var initVal=0;

    console.log(response);

    let groupScheduleData = response.data;
    if(groupScheduleData.length){

                //angular.forEach($scope.groupList, function(value, key){



                  angular.forEach(groupScheduleData, function(innerValue,key){
                    let filteredGroup = $scope.locations02.find(grpInfo => grpInfo.group == innerValue.vehicleId);
                    let innerKey = filteredGroup.rowId;
                    $scope.grpId[innerKey]                  = false;
                    $scope.checkingValue2.consr[innerKey]   = false;
                    $scope.checkingValue2.fuelCon[innerKey] = false;
                    $scope.checkingValue2.travSum[innerKey]   = false;
                    $scope.checkingValue2.nodata[innerKey]   = false;
                    $scope.checkingValue2.lastTran[innerKey]   = false;
                    $scope.checkingValue2.exceFuel[innerKey]  = false;
                    $scope.checkingValue2.vehWisePer[innerKey]  = false;
                    $scope.checkingValue2.monthlyDistance[innerKey]  = false;

                    //if(value == innerValue.vehicleId){

                      if(initVal==0){

                        console.log(groupScheduleData[key].emailIds);

                        $scope.mailId2   =  groupScheduleData[key].emailIds;
                        $scope.from2     =  groupScheduleData[key].fromTime;
                        $scope.to2       =  groupScheduleData[key].toTime;

                        initVal++;

                      }  

                      $scope.grpId[innerKey] = true;
                      // console.log(innerValue.reports)
                      var reportsValue  =  innerValue.reports.split(',');
                      try{

                        angular.forEach(reportsValue, function(rep, ind){

                          switch (rep){
                            case 'VEHICLES_CONSOLIDATED':
                            $scope.checkingValue2.consr[innerKey]   = true;
                            break;
                            case 'FUEL_CONSOLIDATED':
                            $scope.checkingValue2.fuelCon[innerKey]   = true;
                            break;
                            case 'TRAVELSUMMARY':
                            $scope.checkingValue2.travSum[innerKey]   = true;
                            break;
                            case 'LAST_TRANSMISSION_REPORT':
                            $scope.checkingValue2.lastTran[innerKey]   = true;
                            break;
                            case 'NODATA_REPORT':
                            $scope.checkingValue2.nodata[innerKey]   = true;
                            break;
                            case 'EXECUTIVEFUEL_REPORT':
                            $scope.checkingValue2.exceFuel[innerKey]  = true;
                            break;
                            case 'EXECUTIVE_REPORT':
                            $scope.checkingValue2.vehWisePer[innerKey]  = true;
                            break;
                            case 'MONTHLY_DISTANCE_REPORT':
                            $scope.checkingValue2.monthlyDistance[innerKey]  = true;
                            break;

                          }

                        });

                      }catch(err){

                        console.log(err)

                      }
                      
                    //}
                  });

              //});
            }

          })
  .error(function (response) {
    console.log("fail");
  });
  
}
$scope.fetchHourBasedSchedule  =  function(){

  console.log('getHourBasedScheduleInfo');

  $.ajax({
    async: false,
    method: 'GET', 
    url: "ScheduledController/getHourBasedScheduleInfo",
    data: {'userName':sp[1]},
    success: function (response) {

      var initVal=0;

      console.log(response);


      if(response.length){

        angular.forEach($scope.groupList, function(value, key){

          $scope.grpId[key]                  = false;
          $scope.hourBasedValue.conFuel[key]   = false;
          $scope.hourBasedValue.exceFuelReport[key] = false;
          angular.forEach(response, function(innerValue, innerKey){

            if(value == innerValue.groupName){

              if(initVal==0){

                console.log(response[innerKey].email);

                $scope.mailIdHour   =  response[innerKey].email;
                $scope.hours     =  response[innerKey].fromTime;
                $scope.time       =  response[innerKey].toTime;

                initVal++;

              }  

              $scope.grpId[key] = true;
                      // console.log(innerValue.reports)
                      var reportsValue  =  innerValue.reports.split(',');
                      try{

                        angular.forEach(reportsValue, function(rep, ind){

                          switch (rep){
                            case 'FUEL_CONSOLIDATED':
                            $scope.hourBasedValue.conFuel[key]   = true;
                            break;
                            case 'EXECUTIVEFUEL_REPORT':
                            $scope.hourBasedValue.exceFuelReport[key]   = true;
                            break;
                          }

                        });

                      }catch(err){

                        console.log(err)

                      }
                      
                    }
                  });

        });
      }

    }
  });
}


// onload function for value already present
$scope.fetchMonController =  function(){

   //console.log('fetchMonController');

   $http({
     method: 'POST', 
     url: 'ScheduledController/getMonValue', 
     data: { 'userName':usrName[1] },
     cache: $templateCache
   }).
   then(function(response) {

     $scope.status = response.status;
     $scope.data   = response.data;

            // console.log($scope.status);
            //   console.log($scope.data);


            if(response.data.length){


              $scope.grpMailId   = response.data[0].email;

               // console.log($scope.groupList);

               angular.forEach($scope.groupList, function(value, key){

                //  console.log('groupList');

                $scope.grpId[key]                   = false;
                $scope.checkingValue.cstop[key]     = false;

                angular.forEach(response.data, function(innerValue, innerKey){

                 //   console.log('response for....');


                 if(value == innerValue.groupName){

                    //  console.log(innerValue); 

                    $scope.grpId[key] = true;
                    $scope.checkingValue.cstop[key]  = true;
                  }

                  
                });

              });


             }else{

              if($scope.delMonVal == true){


                angular.forEach($scope.groupList, function(value, key){

                  $scope.grpId[key] = false;
                  $scope.checkingValue.cstop[key]  = false;
                });

                $scope.delMonVal = false;

              }

            }



          }, function(response) {

           $scope.data   = response.data || 'Request failed';
           $scope.status = response.status;
         });



 /*   $.ajax({
          async: false,
          method: 'GET', 
          url: "ScheduledController/getValue",
          data: { 'userName':sp[1] },
          success: function (response) {
              if(response.length){
                $scope.mailId   = response[0].email;
                $scope.from     = response[0].fromTime;
                $scope.to       = response[0].toTime;
                $scope.groupSelected    = response[0].groupName;
                angular.forEach($scope.vehicles, function(value, key){
                  $scope.vehiId[key] = false;
                  $scope.checkingValue.move[key]   = false;
                  $scope.checkingValue.over[key]   = false;
                  $scope.checkingValue.site[key]   = false;
                  $scope.checkingValue.poi[key]    = false;
                  $scope.checkingValue.fuel[key]   = false;
                  // $scope.checkingValue.temp[key]   = false;
                  angular.forEach(response, function(innerValue, innerKey){
                    if(value.vehicleId == innerValue.vehicleId){
                      $scope.vehiId[key] = true;
                      // console.log(innerValue.reports)
                      var reportsValue  =  innerValue.reports.split(',');
                      try{

                        angular.forEach(reportsValue, function(rep, ind){

                          switch (rep){
                            case 'movement':
                              $scope.checkingValue.move[key]   = true;
                              break;
                            case 'overspeed':
                              $scope.checkingValue.over[key]   = true;
                              break;
                            case 'site':
                              $scope.checkingValue.site[key]   = true;
                              break;
                            case 'poi':
                              $scope.checkingValue.poi[key]    = true;
                              break;
                            case 'fuel':
                              $scope.checkingValue.fuel[key]   = true;
                              break;
                          }

                        });

                      }catch(err){

                        console.log(err)

                      }
                      
                    }
                });

              });
            }
            
          }
        });  */
      }



//add vehicle in single list
function addVehi(vehi){
  $scope.starterError='';
  $scope.vehicles = [];
  let isAllStarter = vehi.every(function(v){
    return v.licenceType == 'Starter';
  })
  if(isAllStarter){
   $scope.starterError = 'This feature is not privileged for selected group';
 }else{
  for (var i = 0; i < vehi.length; i++) 
  {
    if(vehi[i].licenceType!='Starter'){
      $scope.vehicles.push(vehi[i]);}

    }
  }
}


//init function

// (function (){ 

  function initFunc() {

    startLoading();

    vamoservice.getDataCall($scope.url).then(function(data){

  //  console.log(data);

  $scope.groupList=[];

  angular.forEach(data, function(val, key){
    //    console.log(val.group);
    $scope.groupList.push(val.group);
  });

  addVehi(data[0].vehicleLocations);
  $scope.locations02 = data;
  $scope.notncount =getParkedIgnitionAlert($scope.locations02[0].vehicleLocations)[3];
  $('#notncount').text($scope.notncount);
  window.localStorage.setItem('totalNotifications',$scope.notncount);
    // $scope.groupSelected.group  = data[0];
    


    if(data.length){

     fetchController(data[0].group);   

   } else {
    fetchController();
  }

});

  //stopLoading();
//}());
}

$scope.trimColon = function(textVal){
  return textVal.split(":")[0].trim();
}

$scope.vehiSelect   = function(vehid){
  $scope.vehiId  = [];
  if(vehid == true)
    angular.forEach($scope.vehicles, function(data, id){

      $scope.vehiId[id] = true;
    })
}

$scope.grpSelect   = function(val){

  $scope.grpId  = [];

  if(val == true)
    angular.forEach($scope.groupList, function(data, id){
      $scope.grpId[id] = true;
    });
}

$scope.statusFilter = function(obj, param){
  var out = [];
  if(param=='ALL'){
    out= obj;
    return out;  
  }else if(param=='ON' || param=='OFF'){
    for(var i=0; i<obj.length; i++){
      if(obj[i].status == param){
        out.push(obj[i]);
      }
    }
    return out;  
  }else{
    if(param=='O'){
      for(var i=0; i<obj.length; i++){
        if(obj[i].isOverSpeed == 'Y'){
          out.push(obj[i]);
        }
      }
      return out;
    }else{
      for(var i=0; i<obj.length; i++){
        if(obj[i].position == param){
          out.push(obj[i]);
        }
      }
      return out;
    }
  }
}

function checkUndefined(value){
  try{
    if(value)
      return true
  }
  catch (e){ 
    return false
  }
}


$scope.storeMonValues  =  function(){

  startLoading();

  $scope.monSchReports=[];
  $scope.grpMailIds = $('#groupMailId').val();

  angular.forEach($scope.grpId,function(val,id) {

    var reportVal='';

                 if($scope.monReport.length == 1){

                   reportVal = $scope.checkingValue.cstop[id] == true ? 'Consolidated_Stoppage' :  '';

                 } else if($scope.monReport.length > 1){

                   reportVal += $scope.checkingValue.cstop[id] == true ? 'Consolidated_Stoppage,' :  '';
                 }
          if($scope.grpId[id] == true)
          {

            $scope.monSchReports.push([usrName[1],reportVal,$scope.groupList[id], $scope.grpMailIds]);
          }

        });

    //  console.log('MonSchdRpt:'+$scope.monSchReports);

    $http({
     method: 'POST', 
     url: 'ScheduledController/monthReportScheduling', 
     data: { 'reportList': $scope.monSchReports,'userName':usrName[1] },
     cache: $templateCache
   }).
    then(function(response) {

     $scope.status = response.status;
     $scope.data   = response.data;
          }, function(response) {

           $scope.data   = response.data || 'Request failed';
           $scope.status = response.status;
         });

    /*  $.ajax({
          async: false,
          method: 'POST', 
          url: "ScheduledController/monthReportScheduling",
          data: {'reportList': reportsList,'userName':sp[1],'groupName': $scope.groupSelected},// a JSON object to send back
            
            success: function (response) {
              stopLoading();
              location.reload();
            }
        });
        */

        stopLoading();
      }


      $scope.storeValue   = function(){

 startLoading();

 $scope.mailId = $('#mailIdDaily').val();




 if($scope.mailId && $('#fromTime').val() && $('#toTime').val()){
  var fromss   =  parseInt($('#fromTime').val().split(':'));
  var toss     =  parseInt($('#toTime').val().split(':'));

  $scope.from   = $scope.hoursFrom[fromss];
  $scope.to     = $scope.hoursTo[toss];
  var reportsList = [];
  angular.forEach($scope.vehiId,function(val,id){

    var reports = '';

    if($scope.vehiId[id] == true && $scope.from != undefined && $scope.to != undefined && $scope.vehicles[id].vehicleId != null && $scope.vehicles[id].vehicleId != ''){
      try{ reports += $scope.checkingValue.move[id] == true ?  'movement,' : ''; }catch (e){}
      try{ reports += $scope.checkingValue.over[id] == true ?  'overspeed,' : ''; }catch (e){}
      try{ reports += $scope.checkingValue.site[id] == true ?  'site,' : ''; }catch (e){}
      try{ reports += $scope.checkingValue.geo[id] == true ?  'geofence_fuel,' : ''; }catch (e){}
      try{ reports += $scope.checkingValue.poi[id]  == true ?  'poi,' : ''; }catch (e){}
      try{ reports += $scope.checkingValue.fuel[id] == true ?  'fuel,' : ''; }catch (e){}
      try{ reports += $scope.checkingValue.fuelRaw[id] == true ?  'fuelRaw,' : ''; }catch (e){}
          // try{ reports += $scope.checkingValue.temp[id] == true ?  'temperature,' : ''; } 
          // catch (e){}
          let reportInfo = {
            "userId":sp[1],
            "reports":reports,
            "vehicleId": $scope.vehicles[id].vehicleId,
            "fromTime": $scope.from,
            "toTime": $scope.to,
            "emailIds": $scope.mailId,
            "group": $scope.groupSelected
          };
          reportsList.push(reportInfo);
        }

      })

    // $.ajax({
    //         async: false,
    //         method: 'POST', 
    //         url: "ScheduledController/reportScheduling",
    //         data: {'reportList': reportsList,'userName':sp[1],'groupName': $scope.groupSelected},//{'userName':sp[1],'vehicle': $scope.vehicles[id].vehicleId,'report': reportsList,'fromt': $scope.from,'tot': $scope.to,'mail':$scope.mailId,'groupName': $scope.groupSelected.group}, // a JSON object to send back
    //         success: function (response) {
    //           stopLoading();
    //           //location.reload();
    //           //alert(response);
    //           fetchController($scope.groupSelected);
    //         }
    // });
    if($scope.groupSelected!=undefined && $scope.groupSelected!=''){
      console.log("selectgrps",$scope.groupSelected)
      $http.post("saveScheduledReport", {'reportList': reportsList,'userName':sp[1],'groupName': $scope.groupSelected})
      .success(function (response) {
        console.log("success");
        stopLoading();
              //location.reload();
              fetchController($scope.groupSelected);
            })
      .error(function (response) {
        console.log("fail");
      });
    }else{
      stopLoading();
      $scope.error = "*"+translate("Please select the group");
    }

  } else{

    stopLoading();
    if(!$scope.mailId ){
      $scope.error = "*"+translate("Please fill all field & Enter valid email id");
    }else if (!$('#fromTime').val() || !$('#toTime').val()) {
      $scope.error = (!$('#fromTime').val())? "** Please select from time" : "** Please select to time";
    }

    var countUp = function() {
      $scope.error = '';
    }

    $timeout(countUp, 5000);

    stopLoading();
  }
}

$scope.storeValue2   = function(){
 $scope.error = "";
 console.log('storeValue');

 startLoading();

 $scope.mailIdss = $('#mailIdDaily2').val();

 console.log( $scope.mailIdss );

 var fromss     =  parseInt($('#fromTime2').val().split(':'));
 var toss       =  parseInt($('#toTime2').val().split(':'));

 $scope.from2   =  $scope.hoursFrom[fromss];
 $scope.to2     =  $scope.hoursTo[toss];


 if($scope.mailIdss && $('#fromTime2').val() && $('#toTime2').val()){
  var reportsList2 = [];

  console.log("grp",$scope.grpId);

  angular.forEach($scope.grpId,function(val,id){

    var reports = '';

    if(val == true){

      //  if($scope.vehiId[id] == true && $scope.from != undefined && $scope.to != undefined && $scope.vehicles[id].vehicleId != null && $scope.vehicles[id].vehicleId != ''){
        try{ reports += $scope.checkingValue2.consr[id] == true ?  'VEHICLES_CONSOLIDATED,' : ''; }catch (e){}
        try{ reports += $scope.checkingValue2.fuelCon[id] == true ?  'FUEL_CONSOLIDATED,' : ''; }catch (e){}
        try{ reports += $scope.checkingValue2.travSum[id] == true ?  'TRAVELSUMMARY,' : ''; }catch (e){}
        try{ reports += $scope.checkingValue2.lastTran[id] == true ?  'LAST_TRANSMISSION_REPORT,' : ''; }catch (e){}
        try{ reports += $scope.checkingValue2.nodata[id] == true ?  'NODATA_REPORT,' : ''; }catch (e){}
        try{ reports += $scope.checkingValue2.exceFuel[id] == true ?  'EXECUTIVEFUEL_REPORT,' : ''; }catch (e){}
        try{ reports += $scope.checkingValue2.vehWisePer[id] == true ?  'EXECUTIVE_REPORT,' : ''; }catch (e){}
         try{ reports += $scope.checkingValue2.monthlyDistance[id] == true ?  'MONTHLY_DISTANCE_REPORT,' : ''; }catch (e){}
        let grpReportInfo = {
          "userId":sp[1],
          "reports":reports,
          "vehicleId": $scope.groupList[id],
          "fromTime": $scope.from2,
          "toTime": $scope.to2,
          "emailIds": $scope.mailIdss,
          "group": $scope.groupList[id]
        };
        reportsList2.push(grpReportInfo);

      }
      //  }

    });

  console.log("report",reportsList2);

  stopLoading();

  $http.post("saveScheduledReport", {'reportList': reportsList2,'groupName':''})
  .success(function (response) {
    console.log("success");
    stopLoading();
    if(response.response == 'failure'){
     $scope.error = "*"+"Request failed!...";
   }

 })
  .error(function (response) {
    console.log("fail");
  });

  
} else{

  stopLoading();
  if(!$scope.mailIdss ){
    $scope.error = "*"+translate("Please fill all field & Enter valid email id");
  }else if (!$('#fromTime2').val() || !$('#toTime2').val()) {
    $scope.error = (!$('#fromTime2').val())? "** Please select from time" : "** Please select to time";
  }

  var countUp = function() {
    $scope.error = '';
  }

  $timeout(countUp, 5000);

  stopLoading();
}
}

$scope.storeHourbasedValue   = function(){

 console.log('storeHourbasedValue');

 startLoading();

 $scope.mailIdHour = $('#mailIdHour').val();

 if($scope.mailIdHour&&$('#hours').val()&&$('#time').val()){
      //var fromss     =  parseInt($('#hours').val().split(':'));

      $scope.hours   =   $('#hours').val();
      $scope.time     =  $scope.hoursTo[$('#time').val()];
      var hourBasedReportList = [];

      console.log($scope.grpId);

      angular.forEach($scope.grpId,function(val,id){

        var reports = '';

        if(val == true){
      //  if($scope.vehiId[id] == true && $scope.from != undefined && $scope.to != undefined && $scope.vehicles[id].vehicleId != null && $scope.vehicles[id].vehicleId != ''){
        try{ reports += $scope.hourBasedValue.conFuel[id] == true ?  'FUEL_CONSOLIDATED,' : ''; }catch (e){}
        try{ reports += $scope.hourBasedValue.exceFuelReport[id] == true ?  'EXECUTIVEFUEL_REPORT,' : ''; }catch (e){}

          // catch (e){}
          hourBasedReportList.push([ sp[1],reports, $scope.groupList[id], $scope.hours,$scope.time, $scope.mailIdHour ]);

        }
      //  }

    });

      console.log(hourBasedReportList);

      stopLoading();

      $http.post("ScheduledController/hourScheduling", {'reportList': hourBasedReportList,'userName':sp[1]})
      .success(function (response) {
        console.log("success");
        stopLoading();
      })
      .error(function (response) {
        console.log("fail");
      });


    } else{

      stopLoading();
      if($scope.mailIdHour){
        $scope.error = "* Please fill all field"
      }else{
       $scope.error = translate("Please fill all field & Enter valid email id");
     }
     var countUp = function() {
      $scope.error = '';
    }

    $timeout(countUp, 5000);

    stopLoading();
  }
}


function forNull(){
  $scope.selectId               = false;
  $scope.checkingValue.move     = [];
  $scope.checkingValue.over     = [];
  $scope.checkingValue.site     = [];
  $scope.checkingValue.geo     = [];
  $scope.checkingValue.poi      = [];
  $scope.checkingValue.fuel     = [];
  $scope.checkingValue.fuelRaw  = [];
}

function forNull2(){

  $scope.checkingValue2.consr = [];
  $scope.checkingValue2.fuelCon = [];
  $scope.checkingValue2.travSum = [];
  $scope.checkingValue2.lastTran = [];
  $scope.checkingValue2.exceFuel = [];
  $scope.checkingValue2.vehWisePer = [];
  $scope.checkingValue2.monthlyDistance = [];

}
function forNull3(){

  $scope.hourBasedValue.conFuel  = [];
  $scope.hourBasedValue.exceFuelReport  = [];

}


$scope.changeValue =  function(reportName){

  forNull();
  if(reportName.length)
    angular.forEach(reportName, function(name, id){
      angular.forEach($scope.vehicles, function(key, value){
        switch (name){
          case 'Movement(M)':
          $scope.checkingValue.move[value]     =  true;
          break;
          case 'Overspeed(O)':
          $scope.checkingValue.over[value]     =  true;
          break;
          case 'Site(S)':
          $scope.checkingValue.site[value]     =  true;
          break;
          case 'Geofence Fuel(GF)':
          $scope.checkingValue.geo[value]     =  true;
          break;
          case 'POI(P)':
          $scope.checkingValue.poi[value]      =  true;
          break;
          case 'Fuel(F)':
          $scope.checkingValue.fuel[value]     =  true;
          break; 
          case 'FuelRaw(FR)':
          $scope.checkingValue.fuelRaw[value]  =  true;
          break;   
       /* case 'Temperature(T)':
            $scope.checkingValue.temp[value]     =  true;
            break;*/
          }
        })
    })
}

$scope.changeValue2 =  function(reportName){

  forNull2();
  if(reportName.length)
    angular.forEach(reportName, function(name, id){
      angular.forEach($scope.groupList, function(key, value){
        switch (name){
          case 'Consolidated Reports (CR)':
          $scope.checkingValue2.consr[value]  = true;
          break;
          case 'Fuel Consolidate (FC)':
          $scope.checkingValue2.fuelCon[value]  = true;
          break;
          case 'Travel Summary (TS)':
          $scope.checkingValue2.travSum[value]  = true;
          break;
          case 'Last Transmission Report (LTR)':
          $scope.checkingValue2.lastTran[value]  = true;
          break;
          case 'No Data (ND)':
          $scope.checkingValue2.nodata[value]  = true;
          break;
          case 'Executive Fuel Report (EF)':
          $scope.checkingValue2.exceFuel[value]  = true;
          break;
          case 'Vehicle Wise Performance (VP)':
          $scope.checkingValue2.vehWisePer[value]  = true;
          break;
          case 'Monthly Distance Report (MD)':
          $scope.checkingValue2.monthlyDistance[value]  = true;
          break;

        }
      })
    })
}
$scope.changeHourBasedValue =  function(reportName){

  forNull3();
  if(reportName.length)
    angular.forEach(reportName, function(name, id){
      angular.forEach($scope.groupList, function(key, value){
        switch (name){
          case 'Fuel Consolidate (FC)':
          $scope.hourBasedValue.conFuel[value]  = true;
          break;
          case 'Executive Fuel Report (EF)':
          $scope.hourBasedValue.exceFuelReport[value]  = true;
          break;
        }
      })
    })
}

$scope.changeMonValue =  function(reportName){

//  console.log(reportName);

if(reportName.length>0){
  angular.forEach($scope.groupList, function(key, value){
   $scope.checkingValue.cstop[value]  = true;
 });
} else if(reportName) {
  $scope.checkingValue.cstop = [];
}
}


$scope.groupChange  = function(groupName){
  startLoading();
  $scope.error =  "";
  forNull();
  $scope.groupSelected=groupName;

  // $scope.checkingValue.temp = [];
  $scope.vehiId             = [];
  $scope.grpId              = [];
  $scope.from               = '';
  $scope.to                 = '';
  
  vamoservice.getDataCall($scope.url+'?group='+$scope.groupSelected).then(function(response){

   angular.forEach(response, function(value, key){
     ($scope.groupSelected == value.group) ? addVehi(response[key].vehicleLocations) : console.log(' No groupName ');
     if($scope.groupSelected == value.group){
      $scope.notncount =getParkedIgnitionAlert(response[key].vehicleLocations)[3];
      $('#notncount').text($scope.notncount);
      window.localStorage.setItem('totalNotifications',$scope.notncount);
    }

  });

       // addVehi(res[key].vehicleLocations);
       fetchController($scope.groupSelected);
       // fetchController($scope.groupSelected.group);

       stopLoading();

     });
}

/*
  FOR DELETING DAILY SCHEDULE REPORT 
  */

  $scope.deleteFn   = function(_data, index) {

    console.log(_data);

    startLoading();
    $scope.error    = '';
    var value       = {};
    value.userName  = '';
    value.groupName = '';
    value.vehiId    = '';
    value.userName  = sp[1];
    value.groupName = $scope.groupSelected;
    value.vehiId    = (!index == undefined || index >= 0) ? _data.vehicleId : null;

    if(value.userName != undefined && value.groupName != undefined)
      $http.post("deleteScheduledReport", {
        "reportList": [
        {
          "vehicleId":value.vehiId,
          "group":value.groupName
        }
        ],
        "userId": value.userName,
        "groupName": value.groupName,
        "removeGroup": false,
        "removeVehicles" : (!index == undefined || index >= 0) ? false : true
      })
    .success(function (res) {
      $scope.error = (res.response == 'Success') ? translate('Deleted Successfully') : translate('Not Updated Successfully');
      stopLoading();
      location.reload();
    })
    .error(function (res) {
      $scope.error = 'Not Updated Successfully';
      console.log("fail");
    });
    else
      $scope.error = '*'+translate('Enter all fields');
  }

  $scope.deleteFn2   = function(_data, index) {

    console.log(_data);

    startLoading();
    $scope.error    = '';
    var value       = {};
    value.userName  = '';
    value.groupName = '';
    value.vehiId    = '';
    value.userName  = sp[1]; 
    value.vehiId    = (!index == undefined || index >= 0) ? _data : null;
    value.groupName = (!index == undefined || index >= 0) ? _data : null;

    if(value.userName != undefined){

      console.log(value);
      $http.post("deleteScheduledReport", {
        "reportList": [
        {
          "vehicleId":value.vehiId,
          "group":value.groupName
        }
        ],
        "userId": value.userName,
        "groupName": value.groupName,
        "removeGroup": (!index == undefined || index >= 0) ? false : true,
        "removeVehicles" : false
      })
      .success(function (res) {
        $scope.error = (res.response == 'Success') ? translate('Deleted Successfully') : translate('Not Updated Successfully');
        $scope.grpId[index]                  = false;
        $scope.checkingValue2.consr[index]   = false;
        $scope.checkingValue2.fuelCon[index] = false;
        $scope.checkingValue2.travSum[index]   = false;
        $scope.checkingValue2.nodata[index]   = false;
        $scope.checkingValue2.lastTran[index]   = false;
        $scope.checkingValue2.exceFuel[index]  = false;
        $scope.checkingValue2.vehWisePer[index]  = false;
        $scope.checkingValue2.monthlyDistance[index]  = false;
        let isAllnotnDel = $scope.grpId.every(value=> value==false );
        if(isAllnotnDel){
          $scope.mailId2   =  "";
          $scope.from2     =  "";
          $scope.to2       =  ""; 
          document.getElementById("mailIdDaily2").value = "";
          document.getElementById("fromTime2").value = "";
          document.getElementById("toTime2").value = "";
        }
        stopLoading();
      //location.reload();
    })
      .error(function (res) {
        $scope.error = 'Not Updated Successfully';
        console.log("fail");
      });

    } else {
      $scope.error = '*'+translate('Enter all fields');
    }

  }

  $scope.deleteHourBasedReport   = function(_data, index) {

    console.log(_data);

    startLoading();
    $scope.error    = '';
    var value       = {};
    value.userName  = '';
    value.groupName = '';
    value.userName  = sp[1]; 
    value.groupName = (!index == undefined || index >= 0) ? _data : null;

    if(value.userName != undefined){

      console.log(value);

      $.ajax({
        async: false,
        method: 'GET', 
        url: "ScheduledController/deleteHourBasedReport",
        data: value,
        success: function (response) {
          $scope.error = (response == 'correct') ? translate('Successfully Updated') : translate('Not Updated Successfully');
          
          console.log($scope.error);

          $scope.fetchHourBasedSchedule();

          stopLoading();
        //location.reload();

      }
    });

    } else {
      $scope.error = '*'+translate('Enter all fields');
    }

  }



/*
  FOR DELETING MONTHLY SCHEDULE REPORT 
  */

  $scope.deleteMonFunc = function(index) {

   console.log('deleteMonFunc....');

   startLoading();

 // console.log(index);
 
 $scope.error     = '';

 var value        = {};
 value.userName   = sp[1];
 value.groupName  = (!index == undefined || index >= 0) ? $scope.groupList[index] : null;


 // console.log(value.groupName);

  //if( value.userName != undefined && value.groupName != undefined ) {

    $http({
     method: 'POST', 
     url: 'ScheduledController/reportMonDelete', 
     data: value,
     cache: $templateCache
   }).
    then(function(response) {

     $scope.status = response.status;
     $scope.data   = response.data;

     stopLoading();     

     if( index == undefined ) {

      $scope.selectGrpId[0]  = false;

      angular.forEach($scope.groupList, function(value, key){

       $scope.grpId[key]                   = false;
       $scope.checkingValue.cstop[key]     = false;

     });

    } else {

      $scope.fetchMonController();

      $scope.delMonVal=true;

    }

           //  console.log(response.status);

         }, function(response) {

           $scope.data   = response.data || 'Request failed';
           $scope.status = response.status;
         });


  /*  $.ajax({
      async: false,
      method: 'GET', 
      url: "ScheduledController/reportDelete",
      data: value,
      success: function (response) {
        $scope.error = (response == 'correct') ? 'Successfully Updated' : 'Not Updated Successfully';
        stopLoading();
        location.reload();
      }
    });*/

 /* } else {

     $scope.error = '*Enter all fields';
   } */
 }
 $(window).scroll(function (event) {
  if($scope.tapchanged){
    $scope.tapchanged=false;
    console.log('scrolled');
    $('.table-fixed-header1').fixedHeader();
  }
});

}]);
