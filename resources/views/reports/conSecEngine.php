<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title><?php echo Lang::get('content.gps'); ?></title>

  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
  <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
  <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
  <!-- Optional theme -->
  <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
  <style>
    body {
      font-family: 'Lato', sans-serif;
      /* font-weight: bold; */  
   /* font-family: 'Lato', sans-serif;
      font-family: 'Roboto', sans-serif;
      font-family: 'Open Sans', sans-serif;
      font-family: 'Raleway', sans-serif;
      font-family: 'Faustina', serif;
      font-family: 'PT Sans', sans-serif;
      font-family: 'Ubuntu', sans-serif;
      font-family: 'Droid Sans', sans-serif;
      font-family: 'Source Sans Pro', sans-serif;
      */
    }
    .empty{
      height: 1px; width: 1px; padding-right: 30px; float: left;
    }
    .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
      background-color: #ffffff;
    }
    .table-bordered {
      border:1px solid black
    }
/* td{
  border:1px solid black
  }*/
</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2X3F316479');
</script>
</head>
<div id="preloader" >
  <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
  <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
  <div ng-controller="mainCtrl" class="ng-cloak">
    <div id="wrapper">
     <?php include('sidebarList.php');?> 
     
     <div id="testLoad"></div>
     
     <div id="page-content-wrapper">
      <div class="container-fluid">
        <div class="panel panel-default">
         
        </div>   
      </div>
    </div>
    


    <div class="col-md-12">
     <div class="box box-primary" style="padding-top: 5px;margin-top: 10px;">
      <!-- <div class="row"> -->
        <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
          <h3 class="box-title"><?php echo Lang::get('content.consolidated'); ?> <?php echo Lang::get('content.sec_eng_on'); ?> <?php echo Lang::get('content.report'); ?>
        </h3>
        <div class="box-tools pull-right">
         <?php include('helpVideos.php');?>
       </div>
     </div>
     <div class="row">
      <div class="col-md-2" align="center"></div>
      <div class="col-md-3" align="center">
        <div class="form-group">
          <div class="input-group datecomp">
            <input type="text" ng-model="uiDate.fromdate" class="form-control placholdercolor" id="dateFrom"  placeholder="<?php echo Lang::get('content.from_date'); ?>">
            <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
          </div>
        </div>
      </div> 
      <div class="col-md-3" align="center" style="margin-left: -40px;">
        <div class="form-group">
          <div class="input-group datecomp">
            <input type="text" ng-model="uiDate.todate" class="form-control placholdercolor" id="dateTo" placeholder="<?php echo Lang::get('content.to_date'); ?>" >
            <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
          </div>
        </div>
      </div>
      <div class="col-md-1" align="center">
        <button style="margin-left: -100%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
      </div>
    </div>
    <div class="col-md-12" align="right" style="padding-left: 10%;">
      <?php include('days.php');?>
    </div>
    

    <!--  </div> -->
    <div class="row">
      <div class="col-md-1" align="center"></div>

      <div class="col-md-2" align="center">
        <div class="form-group">

        </div>
      </div>
    </div>

  </div>
</div>

<div class="col-md-12">
  <div class="box box-primary" style="min-height:570px;">
    
    <div>

      <div class="pull-right" style="margin-top: 10px;margin-right: 5px;">
        
        <!-- <button type="button" class="btn btn-info" ng-click="durationFilter('month')" ng-disabled="monthDisabled">Month</button> -->
        <img style="cursor: pointer;" ng-click="exportData('ConSecEngineON')"  src="../resources/views/reports/image/xls.png" />
        <img style="cursor: pointer;" ng-click="exportDataCSV('ConSecEngineON')"  src="../resources/views/reports/image/csv.jpeg" />
        <img style="cursor: pointer;" onclick="generatePDF()"  src="../resources/views/reports/image/Adobe.png" />

        <!-- <button class="btn btn-primary" onclick="generatePDF()"><i class="fa fa-save"></i> Save as PDF</button> -->
      </div>

      
      <div id="content1">
        <div class="box-body" id="ConSecEngineON">
          <div class="empty" align="center"></div>
          

          <div class="row" style="padding-top: 20px;"></div>
          <div id="formConfirmation">
            <table class="table table-striped table-bordered table-condensed table-hover"   style="padding:10px 0 10px 0;">
              
              <tbody ng-repeat="data in execReportData"  >
                <tr style="text-align:center;height:30px;" ng-if="execReportData[$index].vehicleId!=execReportData[$index-1].vehicleId">
                  <td style="font-size:13px;background-color:#C2D2F2;"><b><?php echo Lang::get('content.veh_name'); ?> : {{data.shortName}}</b></td>
                  <td style="font-size:13px;background-color:#C2D2F2;"><b><?php echo Lang::get('content.start_time'); ?> : {{uiDate.fromdate | date:'dd-MM-yyyy'}}</b></td>
                  <td style="font-size:13px;background-color:#C2D2F2;"><b><?php echo Lang::get('content.end_time'); ?> : {{uiDate.todate | date:'dd-MM-yyyy'}}</b></td>
                </tr>
                <!-- <tr><td colspan="4" bgcolor="grey"></td><tr> -->
                  <tr ng-if="execReportData[$index].vehicleId!=execReportData[$index-1].vehicleId">
                    <td  style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.date_time'); ?></b></td>
                    <td  style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.hours'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</b></td>
                    <td  style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.utilized'); ?> <?php echo Lang::get('content.hours'); ?> (%)</b></td>
                    <!-- <td width="10%" style="font-size:12px;background-color:#ecf7fb;"><b>Geofence</b></td> -->
                  </tr>
                  
                  <tr style="padding-bottom:20px;">
                   <td>{{data.date|date:'dd-MM-yyyy'}}</td>
                   <td>{{msToTime(data.totalSecondaryEngineTime) }}</td>
                   <td>{{data.totalSecondaryEngineTimePersent}}</td>
                 </tr>
                 <tr ng-if="execReportData[$index].vehicleId!=execReportData[$index+1].vehicleId" ng-show="!$last"><td colspan="3" bgcolor="grey"></td><tr>
                 </tbody>
                                    <!-- <tr ng-if="execReportData.length==0" style="text-align: center">
                                        <td colspan="3" class="err"><h5><?php echo Lang::get('content.no_date_time'); ?></h5></td>
                                      </tr> -->
                                      <tr ng-if="conSecEngData.error" style="text-align: center">
                                        <td colspan="3" class="err"><h5>{{conSecEngData.error}}</h5></td>
                                      </tr>
                                    </table> 
                                  </div>                       
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>

                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.min.js"></script>


   <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.5/jspdf.plugin.autotable.min.js"></script>
-->
<script src="assets/js/static.js"></script>
<script src="assets/js/jquery-1.11.0.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
<script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script> -->
<script src="assets/js/angular-translate.js"></script>
<script src="../resources/views/reports/customjs/html5csv.js"></script>
<script src="../resources/views/reports/customjs/moment.js"></script>
<script src="../resources/views/reports/customjs/FileSaver.js"></script>
<script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
<script src="../resources/views/reports/datatable/jquery.dataTables.js"></script>
<script src="assets/js/vamoApp.js"></script>
<script src="assets/js/services.js"></script>
<script src="assets/js/conSecEngine.js?v=<?php echo Config::get('app.version');?>"></script>

<script>

 
  $("#example1").dataTable();
  
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });
  var generatePDF = function() {
    kendo.drawing.drawDOM($("#formConfirmation")).then(function(group) {
      kendo.drawing.pdf.saveAs(group, "ConSecEngineON.pdf");
    });
  }


</script>

</body>
</html>
