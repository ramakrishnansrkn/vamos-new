
<link href="../resources/views/assets/css/datebutton.css" rel="stylesheet" type="text/css" />
 <div>
     <div class="col-md-2"></div>
      <button class="btnDate" ng-click="durationFilter('today')" ng-disabled="todayDisabled" id='today' onclick="setToTime('today')"><?php echo Lang::get('content.today'); ?></button>

	
      <button class="btnDate" ng-click="durationFilter('yesterday')" ng-disabled="yesterdayDisabled" id='yesterday' onclick="setToTime('yesterday')"><?php echo Lang::get('content.yesterday'); ?></button>
  
 		
      <button class="btnDate" ng-click="durationFilter('thisweek')" ng-disabled="thisweekDisabled" id='thisweek' onclick="setToTime('thisweek')"><?php echo Lang::get('content.thisweek'); ?></button>
  
 		
      <button class="btnDate" ng-click="durationFilter('lastweek')" ng-disabled="weekDisabled" id='lastweek' onclick="setToTime('lastweek')"><?php echo Lang::get('content.lastweek'); ?></button>
  
  	
      <button class="btnDate" ng-click="durationFilter('month')" ng-disabled="monthDisabled" id='thismonth' onclick="setToTime('thismonth')"><?php echo Lang::get('content.thismonth'); ?></button>
  
		
      <button class="btnDate" ng-click="durationFilter('lastmonth')" ng-disabled="lastmonthDisabled"  id='lastmonth' onclick="setToTime('lastmonth')"><?php echo Lang::get('content.lastmonth'); ?></button>
                    
</div>
