<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title>GPS</title>

<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
<link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">

<style>
.empty{
    height: 1px; width: 1px; padding-right: 30px; float: left;
}
.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
background-color: #ffffff;
}

</style>

</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
    <div ng-controller="mainCtrl" class="ng-cloak">
      <div id="wrapper">
        <?php include('sidebarList.php');?>
        
        <div id="testLoad"></div>
        
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default">
                 
                </div>   
            </div>
        </div>
 


    <div class="col-md-12">
       <div class="box box-primary">
        <!-- <div class="row"> -->
                <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                    <h3 class="box-title">Scheduled Stoppage Report</h3>
                </div>
               <div class="row">
                    <div class="col-md-1" align="center"></div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.fromdate" class="form-control placholdercolor" id="dateFrom"  placeholder="From date">
                                <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.fromtime" class="form-control placholdercolor" id="timeFrom" placeholder="From time">
                                <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.todate" class="form-control placholdercolor" id="dateTo" placeholder="From date">
                                <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2" align="center">
                        <div class="form-group">
                            <div class="input-group datecomp">
                                <input type="text" ng-model="uiDate.totime" class="form-control placholdercolor" id="timeTo" placeholder="From time">
                                <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                            </div>
                        </div>
                    </div>
                    <!--
                    <div class="col-md-1" align="center">
                        <div class="form-group">
                            
                                <select class="input-sm form-control" ng-model="interval">
                                     <option value="">Interval</option>
                                     <option label="1 mins">1</option>
                                     <option label="2 mins">2</option>
                                     <option label="5 mins">5</option>
                                     <option label="10 mins">10</option>
                                     <option label="15 mins">15</option>
                                     <option label="30 mins">30</option>
                                </select>
                           
                        </div>
                    </div>
                    -->
                    <div class="col-md-1" align="center"></div>
                     <div class="col-md-1" align="center">
                        <button style="margin-left: -100%; padding : 5px" ng-click="submitFunction()">Submit</button>
                    </div>
                </div>

              <!--  </div> -->
            </div>
            
          
        </div>

        <div class="col-md-12">
            <div class="box box-primary">
        
                <div>
                    <div class="pull-right">
                        <img style="cursor: pointer;" ng-click="exportData('tripreport')"  src="../resources/views/reports/image/xls.png" />
                        <img style="cursor: pointer;" ng-click="exportDataCSV('tripreport')"  src="../resources/views/reports/image/csv.jpeg" />
                        
                    </div>
                    <div class="box-body" id="tripreport">
                <div class="empty" align="center"></div>

                    <table>
                      <tbody> 
                        <tr> 
                          <td style="font-size:14px;font-weight:bold;padding:0 15px 0 5px"> Vehicle Group :</td>
                           <td style="padding:0 0 0 5px;font-size:12px;">
                            <select ng-model="gName" style="background-color:#f9f9f9;padding:3px 3px 3px 3px;min-width:150px;max-width:150px;height:25px;">
                                <option ng-repeat="vehi in vehicle_group" value="{{vehi.vgName}}">{{trimColon(vehi.vgName)}}</option>
                               </select>
                             </td>
                         </tr>
                       </tbody> 
                      </table>  

                       <div class="row" style="padding-top: 20px;"></div>

                        <table class="table table-striped table-bordered table-condensed table-hover" style="padding:10px 0 10px 0;">
                                          
                                <tbody ng-repeat="data in stopData" >
                                     <tr style="text-align:center;height:30px;">
                                        <td colspan="2" style="font-size:13px;background-color:#C2D2F2;"><b>Vehicle Name : {{data.shortName}}</b></td>
                                        <td style="font-size:13px;background-color:#C2D2F2;"><b>Start Time : {{data.startTime | date:'yyyy-MM-dd HH:mm:ss'}}</b></td>
                                        <td style="font-size:13px;background-color:#C2D2F2;"><b>End Time : {{data.endTime | date:'yyyy-MM-dd HH:mm:ss'}}</b></td>
                                      </tr>
                                           
                                      <tr><td colspan="4" bgcolor="grey"></td><tr>
                                      <tr>
                                        <td width="20%" style="font-size:12px;background-color:#ecf7fb;"><b>Start Time</b></td>
                                        <td width="20%" style="font-size:12px;background-color:#ecf7fb;"><b>End Time</b></td>
                                        <td width="40%" style="font-size:12px;background-color:#ecf7fb;"><b>Address</b></td>
                                        <td width="20%" style="font-size:12px;background-color:#ecf7fb;"><b>Stoppage Duration (h:m:s)</b></td>
                                      </tr>
                                     
                                      <tr ng-repeat="subdata in data.history" style="padding-bottom:20px;" >
                                         <td width="20%">{{subdata.startTime | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                                         <td width="20%">{{subdata.endTime | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                                         <td width="40%">{{subdata.address}}</td>
                                         <td width="20%">{{msToTime(subdata.stoppageTime)}}</td>
                                      </tr>

                                      <tr ng-if="data.history==null" style="text-align: center">
                                          <td colspan="4" class="err"><h5>No Data Found! Choose some other date</h5></td>
                                      </tr>
                                             
                                      <tr ng-show="data.history!==null"><td colspan="4" bgcolor="white" style="border-style:none;"></td><tr>
                                      <tr ng-show="data.history!==null"><td colspan="4" bgcolor="white" style="border-style:none;"></td><tr>
                                      <tr ng-show="data.history!==null"><td colspan="4" bgcolor="white" style="border-style:none;"></td><tr>
                                      <tr ng-show="data.history!==null"><td colspan="4" bgcolor="white" style="border-style:none;"></td><tr>
                                </tbody>
                                
                            </table>                        
                        </div>

                    </div>
                </div>
            </div>

          </div>
        </div>





    <script src="assets/js/static.js"></script>
    <script src="assets/js/jquery-1.11.0.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
    <script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="assets/js/angular-translate.js"></script>
    <script src="../resources/views/reports/customjs/html5csv.js"></script>
    <script src="../resources/views/reports/customjs/moment.js"></script>
    <script src="../resources/views/reports/customjs/FileSaver.js"></script>
    <script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="../resources/views/reports/datatable/jquery.dataTables.js"></script>
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/services.js"></script>
    <script src="assets/js/stoppageReport.js"></script>
    
    <script>

   
        $("#example1").dataTable();
          
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
        });
        
        $(function () {
                $('#dateFrom, #dateTo').datetimepicker({
                    format:'YYYY-MM-DD',
                    useCurrent:true,
                    pickTime: false
                });
                $('#timeFrom').datetimepicker({
                    pickDate: false,
                    
                });
                $('#timeTo').datetimepicker({
                    pickDate: false,
                    
                });
        });      
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });


  </script>
    
</body>
</html>
