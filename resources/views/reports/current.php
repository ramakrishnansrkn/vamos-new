<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="Satheesh">
  <title>
    <?php echo Lang::get('content.gps'); ?></title>
    <link rel="shortcut icon" href="assets/imgs/tab.ico">
    <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/popup.bootstrap.min.css">
    <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="../resources/views/reports/datepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css">
    <link href="assets/css/jVanilla.css" rel="stylesheet">
    <link href="assets/css/simple-sidebar.css" rel="stylesheet">
    <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
    <!-- <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css"> -->
    <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style type="text/css">.gm-style{font-family:Roboto,Arial,sans-serif;font-size:11px;font-weight:400;text-decoration:none}</style>
  <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">

  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>

  <!-- pdfgen -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
  <script src="https://unpkg.com/jspdf-autotable@2.3.2/dist/jspdf.plugin.autotable.js"></script>

  <style type="text/css">


    .tot{
      font-stretch: expanded; 
      text-decoration: underline;
      font-size: 15px;
    }

    #table_address {
      padding-top: 35px;
      float: left;
    }

    .tot:hover{
      transition: 0.8s;
      color: yellow;
    }

    div#butt:active { 
      background-color: yellow;
    }

    div#butt:focus { 
      background-color: yellow;
    }


    body{
     font-family: 'Lato', sans-serif;
     /*font-weight: bold;*/ 

 /*font-family: 'Lato', sans-serif;
   font-family: 'Roboto', sans-serif;
   font-family: 'Open Sans', sans-serif;
   font-family: 'Raleway', sans-serif;
   font-family: 'Faustina', serif;
   font-family: 'PT Sans', sans-serif;
   font-family: 'Ubuntu', sans-serif;
   font-family: 'Droid Sans', sans-serif;
   font-family: 'Source Sans Pro', sans-serif;
   */
 } 

 #map_canvas { 
  width:100%; height:100%;
}

.drop-menu {
  display: block;
  margin-right: auto;
  margin-left: auto;
  text-align: left;
  padding: 10px 10px;
  font-size: 22px;
  height: 25px;
  max-height: 25px;
  width: 400px;
  background: rgba(51, 51, 51, 0);
  cursor: pointer;
  border: 1px solid black;
}

.table-striped > tbody > tr:nth-child(even) > td, 
.table-striped > tbody > tr:nth-child(even) > th {
 background-color: #ffffff;
}

.butList:hover {

}

</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2X3F316479');
</script>
</head>
<div id="preloader" >

  <div id="status">&nbsp;</div>
</div>
<!-- <div id="preloader02" >
  <div id="status02">&nbsp; -->
    <!-- <p id="preloader"> asdasdß</p> -->
 <!-- </div>
 </div> -->

 <body ng-app="mapApp" class="ng-cloak">
  <div id="wrapper" ng-controller="mainCtrl">

    <?php include('sidebarList.php');?>

    <div id="testLoad"></div>
    <div id="page-content-wrapper">
     <div class="container-fluid">
       <div class="panel panel-default">
       </div>   
     </div>
     <!-- /#page-content-wrapper -->
   </div>

   <!-- AdminLTE css box-->

   <div ng-show="reportBanShow" class="modal fade" id="allReport" role="dialog" style="top: 100px">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <p class="err" style="text-align: center;"><?php echo Lang::get('content.premium'); ?></p>
        </div>
      </div>
    </div>
  </div> 

  <div ng-show="reportBanShow" class="col-md-10" >
    <div class="box box-primary" style="height:90px; padding-top:30px; margin-top:5%; margin-left:8%;">
      <p ><h5 class="err" style="text-align: center;"> <?php echo Lang::get('content.no_report_found'); ?> </h5></p>
    </div>
  </div>

  <div class="col-md-12" ng-hide="reportBanShow">

    <tabset class="nav-tabs-custom">
      <div class="form-group pull-right" style="margin-top: 10px;margin-right : 40px; " ng-hide="firstTab">
        <!-- <button type="button" class="btn btn-success" ng-click="durationFilter('today')" ng-disabled="todayDisabled" ng-hide="siteTab"><?php echo Lang::get('content.today'); ?></button> -->
        <button type="button" class="btn btn-primary" ng-click="durationFilter('yesterday')" ng-disabled="yesterdayDisabled"><?php echo Lang::get('content.yesterday'); ?></button>
      </div>
      <tab ng-show="curStatShow" heading="<?php echo Lang::get('content.dashboard'); ?>" id="tabs" active="firstTab" ng-click="startTime('shortName', 'reload')">
        <div class="box box-primary1" ng-show="curStatShow">

          <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >

           <h5 class="box-title"><?php echo Lang::get('content.dashboard'); ?>
         </h5>

         <!-- language -->
         <!-- <hr style="margin-bottom:0px;"> -->
               <!--  <div class="box-tools pull-right">
                       <img style="cursor: pointer;" ng-click="exportData('statusreport')"  src="../resources/views/reports/image/xls.png" />
                     <img width=30 height=30 style="cursor: pointer;" ng-click="exportDataCSV('statusreport')"  src="../resources/views/reports/image/csv.jpeg" />
                   </div> -->

                   <div class="span6 pull-right" style="padding:10px;" >
                    <button type="button" ng-hide= "verifyNoDataCount == 0" class="btn btn-primary" data-toggle="modal" data-target="#NoDataDetail" ng-click="getNoDataDetails()"><?php echo Lang::get('content.nodata'); ?></button>
                    <button type="button"  class="btn btn-primary" ng-click="getVehicleStatus()"><?php echo Lang::get('content.lastTrans'); ?></button>
                    <span class="pull-right" style="padding-left: 10px;">
                      <?php include('helpVideos.php');?>
                    </span>

                    
                    <button type="button" ng-hide= "verifyGeoCount == 0" class="btn btn-primary" onclick="window.location.href='geofence'"><?php echo Lang::get('content.geofence'); ?></button>
                    <button type="button" ng-show="showGeoRoid" class="btn btn-primary" ><a style="color:inherit;" href="http://georoidcamera.com" target="_blank"><?php echo Lang::get('content.georoid_camera'); ?></a></button>
                    <span style="background-color: red;
                    color: white;
                    padding: 2px 15px;margin-left: 5px;"></span>
                    <span style="padding-left: 5px;margin-right: 10px;font-weight: bold;">No Data Vehicles</span>
                    <!-- for no data modal start-->

                    <div class="modal fade" id="NoDataDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                     <div class="modal-dialog" role="document">
                      <div class="modal-dialog" role="document">
                        <div class="modal-content"  style="overflow: auto;">

                          <div class="modal-header" >

                            <h5 class="modal-title" id="exampleModalLabel" ><?php echo Lang::get('content.nodata'); ?></h5>
                            <span class="close" data-dismiss="modal" style="margin-top: -17px;">X</span>
                          </div>

                          <div class="modal-body">
                            <div class="panel-group" id="accordion" ng-repeat="noData in noDataDetails.vehicleDetails track by $index"  ng-init="getCount($index)">
                             <div class="panel panel-default" >
                              <div  class="panel-heading" >

                                <a data-toggle="collapse"   href="#nodatacollapse{{$index}}"  data-parent="#accordion0" style="text-decoration: none;">
                                  <table style="width: 100%;">
                                    <th><td width="20%">
                                      <h8 class="panel-title" style="font-size:14px;padding-right:14px;">

                                       <?php echo Lang::get('content.vehicle_name'); ?> :  </td><td><td width="20%">
                                       {{ noData.vehicleName }}  </h8>
                                     </td>
                                     <td width="65%">
                                       <h8 class="panel-title" style="font-size:14px;text-align:left;"><?php echo Lang::get('content.reason'); ?>  :  &nbsp;  <span style="text-align: center;">{{noData.alertType}}</span>
                                       </h8>
                                     </td>
                                   </th>
                                 </table>
                               </a>
                             </div>

                             <div id="nodatacollapse{{$index}}" class="panel-collapse collapse">
                               <div class="panel-body">
                                 <table class="table">
                                   <thead class="thead-inverse">
                                     <tr>
                                       <th style="text-align: left;">{{vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></th>
                                       <th style="text-align: left;"> <?php echo Lang::get('content.Last_Comm'); ?> </th>
                                       <th style="text-align: left;"> <?php echo Lang::get('content.duration'); ?> (<?php echo Lang::get('content.h:m:s'); ?>) </th>
                                       <th style="text-align: left;"> <?php echo Lang::get('content.odo'); ?> </th>
                                       <th  style="text-align: left;"><?php echo Lang::get('content.gmap'); ?></th>
                                     </tr>
                                   </thead>
                                   <tbody>
                                    <tr >
                                      <th scope="row" style="text-align: left;">{{noData.vehicleId}}</th>
                                      <td style="text-align: left;">{{noData.lastCommunicationTime | date:'yyyy-MM-dd HH:mm:ss' }}</td>
                                      <td style="text-align: left;"> {{msToTime(noData.noDataTime)}}</td>
                                      <td style="text-align: left;">{{noData.odometerReading}}</td>
                                      <td><a href="https://www.google.com/maps?q=loc:{{noData.latitude}},{{noData.longitude}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>


                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>

                          </div>

                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo Lang::get('content.close'); ?></button>

                        </div>
                      </div>
                    </div>   

                  </div>
                </div>



                <!--  no data model end -->
                <div class="modal fade " id="GeoFenceDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                 <div class="modal-dialog modal-lg modal-dialog-centered" role="document">

                   <div class="modal-content" style="width:980px;">

                     <div class="modal-header" >

                      <h5 class="modal-title" id="exampleModalLabel" ><?php echo Lang::get('content.geofence'); ?></h5>
                      <span class="close" data-dismiss="modal" style="margin-top: -17px;">X</span>
                    </div>

                    <div class="modal-body">
                      <div class="row">
                        <div class="col-md-3">
                          <button type="button" class="btn btn-warning" style="width: 100%;"><?php echo Lang::get('content.total'); ?> - {{data2.totalVehicles}}</button>
                        </div>
                        <div class="col-md-3">
                          <button type="button" class="btn" style="color: white;background: #efc016ed;width: 100%;"><?php echo Lang::get('content.online'); ?> - {{data2.online}} </button>
                        </div>
                        <div class="col-md-3">
                          <button type="button" class="btn btn-success" style="width: 100%;"><?php echo Lang::get('content.moving'); ?> - {{data2.totalMovingVehicles}}</button>
                        </div>
                        <div class="col-md-3">
                          <button type="button" class="btn btn-danger" style="width: 100%;"><?php echo Lang::get('content.nodata'); ?>   -  {{data2.totalNoDataVehicles}}</button>
                        </div>
                      </div>
                      <div class="panel-group" id="accordion" ng-repeat="itemss in getGeoFence track by $index"  ng-init="getCount($index)" style="margin-top: 10px;">
                       <div class="row">   
                        <div class="panel panel-default col-md-3" style="margin-left: 10px;padding-left: 0px;
                        padding-right: 0px;top: 5px;" id="site{{$index}}">
                        <div  class="panel-heading" style="width: 243px;text-overflow: ellipsis;
                        overflow: hidden;background-color:{{($index%2)?'white !important':''}}">
                        {{itemss.geoFence}}   <span class="span6 pull-right"><b>{{getGeoFence[$index].getVehicles.length}}</b></span>
                      </div>
                    </div>
                    <div class="panel panel-default col-md-8" style="margin-left: 10px;padding-left: 0px;
                    padding-right: 0px;" id="vehicle{{$index}}">
                    <div  class="panel-heading" style="background-color:{{($index%2)?'white !important':''}}">

                     <a data-toggle="collapse"   href="#collapse{{$index}}"  data-parent="#accordion0" >
                      <h8 class="panel-title" style="font-size:14px;padding-right:14px;">   <b><?php echo Lang::get('content.vehicle'); ?>s : </b> </h8>

                      <h8 class="panel-title"  style="font-size:14px;padding-right:14px;" ng-repeat="item in itemss.getVehicles"> 
                       {{siteSplitName(item,1)}} {{(itemss.getVehicles.length-1==$index)?'':' ,'}}
                     </h8> 
                   </a>

                 </div>

                 <!-- collapse Start -->
                 <div id="collapse{{$index}}" class="panel-collapse collapse">
                  <div class="panel-body">
                   <table class="table">
                     <thead class="thead-inverse">
                       <tr>
                         <th style="text-align: left;"><?php echo Lang::get('content.count'); ?></th>
                         <th style="text-align: left;">{{vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></th>
                         <th style="text-align: left;"> <?php echo Lang::get('content.status'); ?> </th>
                         <th style="text-align: left;"> <?php echo Lang::get('content.position'); ?> </th>
                         <th style="text-align: left;"> <?php echo Lang::get('content.duration'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</th>
                       </tr>
                     </thead>
                     <tbody>
                      <tr ng-repeat="item in itemss.getVehicles">
                        <th scope="row" style="text-align: left;">{{$index+1}}</th>
                        <td style="text-align: left;">{{siteSplitName(item,1)}}</td>
                        <td style="text-align: left;">{{siteSplitName(item,2)}}</td>
                        <!-- <td style="text-align: left;">{{siteSplitName(item,3)}}</td> -->

                        <td ng-if="trvShow!='true'" style="text-align: left;" ng-switch on="siteSplitName(item,3)">
                          <span ng-switch-when="S" style="padding-left:10px;"><img title="Vehicle Standing" src="assets/imgs{{markerPath}}/orange.png"/></span>
                          <span ng-switch-when="M" style="padding-left:5px;">
                            <span ng-if="siteSplitName(item,5) == 'R'" style="padding-left:5px;"><img src="assets/imgs/red.png"></span>
                            <span ng-if="siteSplitName(item,5) == 'G'" style="padding-left:5px;"><img src="assets/imgs/green.png"></span>
                          </span>
                          <span ng-switch-when="P" style="padding-left:10px;"><img title="Vehicle Parked" src="assets/imgs{{markerPath}}/flag.png"/></span>
                          <span ng-switch-when="U" style="padding-left:10px;"><img title="Vehicle NoData" src="assets/imgs{{markerPath}}/gray.png"/></span>
                        </td>

                        <td ng-if="trvShow=='true'" style="text-align: left;" ng-switch on="siteSplitName(item,3)">
                          <span ng-switch-when="S" style="padding-left:10px;"><img title="Vehicle Standing" src="assets/imgs/trvMarker2/yellow.png"/></span>
                          <span ng-switch-when="M" style="padding-left:5px;">
                            <span ng-if="siteSplitName(item,5) == 'R'" style="padding-left:5px;"><img src="assets/imgs/trvMarker2/over.png"></span>
                            <span ng-if="siteSplitName(item,5) == 'G'" style="padding-left:5px;"><img src="assets/imgs/trvMarker2/green.png"></span>
                          </span>
                          <span ng-switch-when="P" style="padding-left:10px;"><img title="Vehicle Parked" src="assets/imgs/trvMarker2/red.png"/></span>
                          <span ng-switch-when="U" style="padding-left:10px;"><img title="Vehicle NoData" src="assets/imgs/trvMarker2/gray.png"/></span>
                        </td>

                        <td style="text-align: left;">{{msToTime(siteSplitName(item,4))}}</td>
                      </tr>

                    </tbody>
                  </table>
                </div>
              </div>
              <!-- collapse End  -->
            </div>
          </div>
        </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo Lang::get('content.close'); ?></button>

      </div>
    </div>
  </div>   

</div>
</div>

</div>
<hr style="margin: 0px">
<div class="box-body">
  <table class="table">
    <thead style="font-weight: bold;">
      <th><b><?php echo Lang::get('content.company'); ?> :</b>&nbsp;&nbsp;{{data2.compName}}</th>
      <th><b>{{vehiLabel | translate }} <?php echo Lang::get('content.group'); ?> :</b> &nbsp;&nbsp;{{trimColon(data2.group)}}</th>
      <th><b><?php echo Lang::get('content.report_date'); ?> :<b> &nbsp;&nbsp;{{today| date:'dd-MM-yyyy'}}</th>                   
        <th><div id="google_translate_element"></div></th>
        <td>
          <div class="pull-right">
            <img style="cursor: pointer;" ng-click="exportData('statusreport')"  src="../resources/views/reports/image/xls.png" />
            <img style="cursor: pointer;" ng-click="exportDataCSV('statusreport')"  src="../resources/views/reports/image/csv.jpeg" />
            <img style="cursor: pointer;" onclick="generatePDF()"  src="../resources/views/reports/image/Adobe.png" />

          </div>
        </td>                   
      </thead>
      <thead>

      </thead>            
    </table>
    <!-- <input class="btn btn-xs btn-danger pull-right" type="button" value="More" ng-click="toggle = !toggle" > -->
    <hr style="margin:0px 0px 10px 0px;">

    <div class="col-md-3" ng-show="cardMsgShow" ng-click="filtDatas('tot')" style="cursor: pointer;">
      <div class="small-box bg-maroon">
       <div class="inner">

         <div id="totClick"><p><!--<i class="fa fa-taxi pull-right"></i>--><a class="butList" href="" ng-click="filtDatas('tot')" style="color: inherit;font-weight: bold;"><span class="tot"><?php echo Lang::get('content.total'); ?></span></a> <span style="float: right;padding-right:18px;"><b>{{data2.totalVehicles}}</b></span></p></div> 
       </div>
     </div>
   </div>

   <div class="col-md-3" ng-show="cardMsgShow" ng-click="filtDatas('vehiOn')" style="cursor: pointer;">
     <div class="small-box bg-purple">
      <div class="inner">
        <div id="vehiOnClick"> <p><!--<i class="fa fa-taxi pull-right"></i>--><a  class="butList" href="" ng-click="filtDatas('vehiOn')"  style="color: inherit;font-weight: bold;"><span class="tot"><?php echo Lang::get('content.online'); ?></span></a> <span style="float: right;padding-right:18px;"><b> {{data2.online}}</b></span></p></div> 
      </div>
    </div>
  </div>

  <div class="col-md-3" ng-show="cardMsgShow" ng-click="filtDatas('move')" style="cursor: pointer;">
    <div class="small-box bg-green">
      <div class="inner">

       <div id="moveClick"> <p><!--<i class="fa fa-taxi pull-right" ></i>--><a class="butList" href="" ng-click="filtDatas('move')" style="color: inherit;font-weight: bold;"><span class="tot"><?php echo Lang::get('content.running'); ?></span></a> <span style="float: right;padding-right:18px;"><b> {{data2.totalMovingVehicles}}</b></span></p> </div> 
     </div>

   </div>
 </div>


 <div  class="col-md-3" ng-show="cardMsgShow" ng-click="filtDatas('idle')" style="cursor: pointer;">
  <div class="small-box bg-yellow">
    <div class="inner">
     <!-- <h3></h3> -->
     <div id="idleClick"><p><!--<i class="fa fa-taxi pull-right"></i>--><a class="butList" href="" ng-click="filtDatas('idle')" style="color: inherit;font-weight: bold;"><span class="tot"><?php echo Lang::get('content.idle'); ?></span></a> <span style="float: right;padding-right:18px;"><b> {{data2.totalIdleVehicles}}</b></span></p>  </div>

   </div>
 </div>
</div>   


<div  class="col-md-3" ng-show="cardMsgShow" ng-click="filtDatas('park')" style="cursor: pointer;">
  <div class="small-box bg-dark-gray">
    <div class="inner">

     <div id="parkClick" > <p><!--<i class="fa fa-taxi pull-right"></i>--><a class="butList" href="" ng-click="filtDatas('park')"  style="color: inherit;font-weight: bold;"><span class="tot"><?php echo Lang::get('content.parking'); ?></span> </a><span style="float: right;padding-right:18px;"><b>{{data2.totalParkedVehicles}}</b></span></p>   </div>
   </div>

 </div>
</div>

<div class="col-md-3" ng-show="cardMsgShow" ng-click="filtDatas('noDat')" style="cursor: pointer;" >
  <div  class="small-box bg-red active" >
   <div class="inner">

     <div id="noDatClick"> <p><!--<i class="fa fa-taxi pull-right"></i>--><a class="butList" href="" ng-click="filtDatas('noDat')" style="color: inherit;font-weight: bold;"><span class="tot"><?php echo Lang::get('content.nodata'); ?></span ></a> <span style="float: right;padding-right:18px;"><b> {{data2.totalNoDataVehicles}}</b></span></p> </div> 


   </div>

 </div>
</div>


<div class="col-md-3" ng-show="cardMsgShow">
  <div class="small-box bg-teal">
    <div class="inner">
      <!-- <h3></h3> -->
      <div ng-if="data2.distance > 1" > <p><!--<i class="fa fa-tachometer pull-right"></i>--><span style="text-decoration: none;font-weight: bold;" class="tot"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.Kms'); ?> </span> <span style="float: right;padding-right:18px;"><b>{{data2.distance|number:0 }} </b></span> </p></div>

      <div ng-if="data2.distance <= 1" > <p><!--<i class="fa fa-tachometer pull-right"></i>--><span style="text-decoration: none;font-weight: bold;" class="tot"><?php echo Lang::get('content.totalkm'); ?> </span> <span style="float: right;padding-right:18px;"><b>{{data2.distance|number:0}} </b></span> </p></div>      
    </div>
  </div>
</div>

<div  class="col-md-3" ng-show="cardMsgShow" style="cursor: pointer;">
  <div class="small-box bg-primary">
    <div class="inner">
     <div id="notSynClick"> <p><!--<i class="fa fa-taxi pull-right"></i>--> <a class="butList" href="" ng-click="filtDatas('notSyn')" style="color: inherit;font-weight: bold;"><span class="tot"><?php echo Lang::get('content.notsync'); ?></span> </a>
       <span style="float: right;padding-right:18px;"><b>{{data2.totalNonSyncedVehicles}}</b></span></p> </div>
     </div>
   </div>
 </div>

               <!-- <div class="col-md-3" ng-show="cardMsgShow">
                <div class="small-box bg-maroon">
                          <div class="inner">
                           <h3></h3> -->
                 <!--          <div ng-if="data2.alerts > 1"><p>--><!--<i class="fa fa-bell pull-right"></i>--><!--<span style="text-decoration: none;font-weight: bold;" class="tot">Alarms / Attentions </span> <span style="float: right;padding-right:18px;"><b>{{data2.alerts}}</b></span></p></div> 
                            <div ng-if="data2.alerts <= 1"><p>--><!--<i class="fa fa-bell pull-right"></i>--><!--<span style="text-decoration: none;font-weight: bold;" class="tot">Alarm / Attention </span> <span style="float: right;padding-right:18px;"><b>{{data2.alerts}}</b></span></p></div>                                  
                          </div>
                      
                       </div>
                     </div> -->

                   </div>

                   <div class="box-body" style="overflow-y:auto;">
                    <div id="formConfirmation">
                     <table class="table table-striped table-bordered table-condensed table-hover table-fixed-header1">
                        <!--  <tr ng-hide ="true" style="font-weight: bold;">
                      <td>Vehicle Group : {{data1.group}}</td>            
                      <td style="text-align: right;">Report Date : {{today}}</td>       
                    </tr> -->
                    <!-- <tr ng-hide ="true" style="text-align:center; font-weight: bold;">
                      <td colspan="4"> Vehical Group</td>
                      <td >{{data1.group}}</td>
                      <td colspan="2">Total Vehicle</td>
                      <td >{{data1.totalVehicles}}</td>
                      <td colspan="3">Total Km - today</td>
                      <td>{{data1.distance}}</td>           
                    </tr>
                    <tr ng-hide ="true" style="text-align:center;font-weight: bold;">
                      <td colspan="4">Alarms/Atentions</td>
                        <td>{{data1.alerts}}</td>
                        <td colspan="2">Vehicles Online</td>
                        <td>{{data1.online}}</td>
                        <td colspan="3">Report Date</td>
                        <td></td>
                      </tr> -->
                      <thead class='header' style=" z-index: 1;">
                        <tr style="text-align:center">
                          <th width="10%" class="id"  style="text-align:center;background-color:#d2dff7;">{{vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></th>
                          <!-- <th ng-hide="true" style="text-align:center;">Vehicle Name</th> -->
                          <th width="5%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.reg_no'); ?></th>
                          <th width="10%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.last_seen'); ?></th>
                          <th width="10%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.Last_Comm'); ?></th>
                          <th ng-if="vehiAssetView" width="5%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.Driver'); ?></th>
                          <th ng-if="vehiAssetView" width="5%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.Driver'); ?> <?php echo Lang::get('content.mobile'); ?></th>
                          <!-- <th ng-hide="true">Mobile Number</th> -->
                          <th width="5%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.kms'); ?></th>
                          <th width="5%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.speed'); ?></th>
                          <th width="5%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.position'); ?></th>
                          <th width="5%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.duration'); ?> <?php echo Lang::get('content.h:m:s'); ?></th>
                          <!-- <th width="5%" class="id" custom-sort order="'status'" sort="sort" style="text-align:center;">GPS</th> -->
                          <!-- <th width="5%" class="id" custom-sort order="'gsmLevel'" sort="sort" style="text-align:center;background-color:#d2dff7;">Sat</th> -->
                          <!-- <th width="5%" class="id" custom-sort order="'loadTruck'" sort="sort" style="text-align:center;background-color:#d2dff7;">Load</th> -->
                          <th width="8%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.powerstatus'); ?></th>
                          <th width="5%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.A/C'); ?></th>
                          <th width="5%" class="id"  style="text-align:center;background-color:#d2dff7;" ng-if="groupFuel.length"><?php echo Lang::get('content.fuel'); ?> <?php echo Lang::get('content.ltrs'); ?></th>
                          <th width="5%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.celsius'); ?></th>
                          <th width="27%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.Nearest_Loc'); ?></th>
                          <th width="5%" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.gmap'); ?></th>
                        </tr>
                      </thead>
                      <tr ng-repeat="user in data1.vehicleLocations" class="active" ng-style="{ 'color':user.position=='U'?'red':'black' }" style="text-align:center; font-size: 11px">
                        <td><a ng-style="{ 'color':user.position=='U'?'red':'blue' }" href="history?vid={{user.vehicleId}}&vg={{data1.group}}">{{user.shortName}}</a></td>
                        <!-- <td ng-hide="true"></td> -->
                        <td>{{user.regNo}}</td>
                        <td>{{user.date | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                        <td>{{user.lastComunicationTime | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                    <!-- <td ng-if="vehiAssetView == 'true' && (user.driverName == 'nill' || user.driverName == '')">--</td>
                      <td ng-if="vehiAssetView == 'true' && (user.driverName != 'nill' || user.driverName != '')">{{user.driverName}}</td> -->
                    <!-- <td ng-if="vehiAssetView">
                      <span ng-show='user.driverName == "nill" || user.driverName == ""'>nill</span>
                      <span ng-show='user.driverName != "nill" && user.driverName != ""'>{{user.driverName}}</span>
                    </td> -->
                    <td ng-if='vehiAssetView&&(user.driverName == "nill" || user.driverName == "")'>
                      -
                    </td>
                    <td ng-if='vehiAssetView&&(user.driverName != "nill" && user.driverName != "")'>
                     {{user.driverName}}
                   </td>
                   <!-- <td ng-hide="true">{{user.mobileNo}}</td> -->
                   <td ng-if="vehiAssetView">{{user.driverMobile?user.driverMobile:'-'}}</td>
                   <td>{{(user.vehicleMode=='DG')? '-' : user.distanceCovered}}</td>
                   <td>{{user.speed}}</td>

                  <!-- <td ng-switch on="user.position">
                                      <span ng-switch-when="S"><img title="Vehicle Standing" src="assets/imgs/orange.png"/></span>
                                      <span ng-switch-when="M">
                                        <span ng-if="user.color == 'R'"><img src="assets/imgs/red.png"></span>
                                        <span ng-if="user.color == 'G'"><img src="assets/imgs/green.png"></span>
                                      </span>
                                      <span ng-switch-when="P"><img title="Vehicle Parked" src="assets/imgs/flag.png"/></span>
                                      <span ng-switch-when="U"><img title="Vehicle NoData" src="assets/imgs/gray.png"/></span>
                                    </td> -->

                                    <td ng-if="trvShow!='true'" ng-switch on="user.position">
                                     <span ng-switch-when="S"><img title="Vehicle Standing" src="assets/imgs{{markerPath}}/orange.png"/></span>
                                     <span ng-switch-when="M">
                                      <span ng-if="user.color == 'R'"><img title="Running" src="assets/imgs/red.png"></span>
                                      <span ng-if="user.color == 'G'"><img title="Running" src="assets/imgs/green.png"></span>
                                    </span>
                                    <span ng-switch-when="P"><img title="Vehicle Parked" src="assets/imgs{{markerPath}}/flag.png"/></span>
                                    <span ng-switch-when="U"><img title="Vehicle NoData" src="assets/imgs{{markerPath}}/gray.png"/></span>
                                  </td>

                                  <td ng-if="trvShow=='true'" ng-switch on="user.position">
                                    <span ng-switch-when="S"><img title="Vehicle Standing" src="assets/imgs/trvMarker2/yellow.png"/></span>
                                    <span ng-switch-when="M">
                                     <span ng-if="user.color == 'R'"><img title="Running" src="assets/imgs/trvMarker2/over.png"></span>
                                     <span ng-if="user.color == 'G'"><img title="Running" src="assets/imgs/trvMarker2/green.png"></span>
                                   </span>
                                   <span ng-switch-when="P"><img title="Vehicle Parked" src="assets/imgs/trvMarker2/red.png"/></span>
                                   <span ng-switch-when="U"><img title="Vehicle NoData" src="assets/imgs/trvMarker2/gray.png"/></span>
                                 </td>

                                 <td ng-switch on="user.position">
                                  <span ng-switch-when="S">{{msToTime(user.idleTime)}}</span>
                                  <span ng-switch-when="M">{{msToTime(user.movingTime)}}</span>
                                  <span ng-switch-when="P">{{msToTime(user.parkedTime)}}</span>
                                  <span ng-switch-when="U">{{msToTime(user.noDataTime)}}</span>
                                </td>
                                <!-- <td>{{user.gsmLevel}}</td> -->
                             <!-- <td ng-switch on="user.status">
                                      <span ng-switch-when="OFF"><img title="GPS OFF" src="assets/imgs/gof.png"/></span>
                                      <span ng-switch-when="ON"><img title="GPS ON" src="assets/imgs/gon.png"/></span>
                                  </td>
                                 <td ng-switch on="user.ignitionStatus">
                                      <span ng-switch-when="OFF"><img src="assets/imgs/no.png"/></span>
                                      <span ng-switch-when="ON"><img src="assets/imgs/yes.png"/></span>
                                    </td> -->
                                    <!--<td ng-if="user.loadTruck=='nill'">--</td>-->
                                    <!--<td ng-if="user.loadTruck!='nill'">{{user.loadTruck}}</td>-->
                                    <!--<td>{{user.powerStatus}}</td>-->
                                    <td ng-switch on="user.powerStatus">
                                     <span>{{user.powerStatus}}</span>
                                   </td>

                                   <td ng-switch on="user.vehicleBusy">
                                    <span ng-switch-when="yes" style="color: #ff0045"><?php echo Lang::get('content.on'); ?></span>
                                    <span ng-switch-when="no" style="color: #00ce2f"><?php echo Lang::get('content.off'); ?></span>
                                  </td>
                                  <td ng-switch on="user.fuel" ng-if="groupFuel.length">
                                   <span ng-switch-when="yes">{{user.fuelLitre}}</span>
                                   <span ng-switch-when="no"> - </span>
                                 </td>              
                                 <td>{{user.temperature}}</td>
                                 <td ng-if="user.address!=null" address={{user.address}}>{{user.address}}</td>
                                 <td style="cursor: pointer;" get-location lat={{user.latitude}} lon={{user.longitude}} index={{$index}} ng-if="user.address==null && mainlist[$index]==null"><?php echo Lang::get('content.clickme'); ?></td>
                                 <td style="cursor: pointer;" ng-if="user.address==null && mainlist[$index]!=null">{{mainlist[$index]}}</td>
                                 <td><a href="https://www.google.com/maps?q=loc:{{user.latitude}},{{user.longitude}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>                            
                               </tr>
                               <tr ng-if="data1.vehicleLocations==null || data1.vehicleLocations.length==0">
                                <td colspan="16" class="err"><h5><?php echo Lang::get('content.no_data_found'); ?></h5></td>
                              </tr>
                            </table>   
                          </div> 
                        </div>
                        <div class="box-body" id="statusreport" ng-show="false" style="overflow-y:auto;">
                          <div id="formConfirmation">
                            <table class="table table-striped table-bordered table-condensed table-hover" id="dashboard_table">
                        <!--  <tr ng-hide ="true" style="font-weight: bold;">
                      <td>Vehicle Group : {{data1.group}}</td>            
                      <td style="text-align: right;">Report Date : {{today}}</td>       
                    </tr> -->
                    <!-- <tr ng-hide ="true" style="text-align:center; font-weight: bold;">
                      <td colspan="4"> Vehical Group</td>
                      <td >{{data1.group}}</td>
                      <td colspan="2">Total Vehicle</td>
                      <td >{{data1.totalVehicles}}</td>
                      <td colspan="3">Total Km - today</td>
                      <td>{{data1.distance}}</td>           
                    </tr>
                    <tr ng-hide ="true" style="text-align:center;font-weight: bold;">
                      <td colspan="4">Alarms/Atentions</td>
                        <td>{{data1.alerts}}</td>
                        <td colspan="2">Vehicles Online</td>
                        <td>{{data1.online}}</td>
                        <td colspan="3">Report Date</td>
                        <td></td>
                      </tr> -->
                      <thead class='header' style=" z-index: 1;">
                        <tr style="text-align:center">
                          <th width="10%" class="id"  style="text-align:center;background-color:#d2dff7;">{{vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></th>
                          <!-- <th ng-hide="true" style="text-align:center;">Vehicle Name</th> -->
                          <th width="5%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.reg_no'); ?></th>
                          <th width="10%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.last_seen'); ?></th>
                          <th width="10%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.Last_Comm'); ?></th>
                          <th ng-if="vehiAssetView" width="5%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.Driver'); ?></th>
                          <th ng-if="vehiAssetView" width="5%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.Driver'); ?> <?php echo Lang::get('content.mobile'); ?></th>
                          <!-- <th ng-hide="true">Mobile Number</th> -->
                          <th width="5%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.kms'); ?></th>
                          <th width="5%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.speed'); ?></th>
                          <th width="5%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.position'); ?></th>
                          <th width="5%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.duration'); ?> <?php echo Lang::get('content.h:m:s'); ?></th>
                          <!-- <th width="5%" class="id" custom-sort order="'status'" sort="sort" style="text-align:center;">GPS</th> -->
                          <!-- <th width="5%" class="id" custom-sort order="'gsmLevel'" sort="sort" style="text-align:center;background-color:#d2dff7;">Sat</th> -->
                          <!-- <th width="5%" class="id" custom-sort order="'loadTruck'" sort="sort" style="text-align:center;background-color:#d2dff7;">Load</th> -->
                          <th width="8%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.powerstatus'); ?></th>
                          <th width="5%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.A/C'); ?></th>
                          <th width="5%" class="id"  style="text-align:center;background-color:#d2dff7;" ng-if="groupFuel.length"><?php echo Lang::get('content.fuel'); ?> <?php echo Lang::get('content.ltrs'); ?></th>
                          <th width="5%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.celsius'); ?></th>
                          <th width="27%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.Nearest_Loc'); ?></th>
                          <th width="5%" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.gmap'); ?></th>
                        </tr>
                      </thead>
                      <tr ng-repeat="user in data1.vehicleLocations" class="active" ng-style="{ 'color':user.position=='U'?'red':'black' }" style="text-align:center; font-size: 11px">
                        <td><a ng-style="{ 'color':user.position=='U'?'red':'blue' }" href="history?vid={{user.vehicleId}}&vg={{data1.group}}">{{user.shortName}}</a></td>
                        <!-- <td ng-hide="true"></td> -->
                        <td>{{user.regNo}}</td>
                        <td>{{user.date | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                        <td>{{user.lastComunicationTime | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                    <!-- <td ng-if="vehiAssetView == 'true' && (user.driverName == 'nill' || user.driverName == '')">--</td>
                      <td ng-if="vehiAssetView == 'true' && (user.driverName != 'nill' || user.driverName != '')">{{user.driverName}}</td> -->
                    <!-- <td ng-if="vehiAssetView">
                      <span ng-show='user.driverName == "nill" || user.driverName == ""'>nill</span>
                      <span ng-show='user.driverName != "nill" && user.driverName != ""'>{{user.driverName}}</span>
                    </td> -->
                    <td ng-if='vehiAssetView&&(user.driverName == "nill" || user.driverName == "")'>
                      -
                    </td>
                    <td ng-if='vehiAssetView&&(user.driverName != "nill" && user.driverName != "")'>
                     {{user.driverName}}
                   </td>
                   <!-- <td ng-hide="true">{{user.mobileNo}}</td> -->
                   <td ng-if="vehiAssetView">{{user.driverMobile?user.driverMobile:'-'}}</td>
                   <td>{{(user.vehicleMode=='DG')? '-' : user.distanceCovered}}</td>
                   <td>{{user.speed}}</td>

                  <!-- <td ng-switch on="user.position">
                                      <span ng-switch-when="S"><img title="Vehicle Standing" src="assets/imgs/orange.png"/></span>
                                      <span ng-switch-when="M">
                                        <span ng-if="user.color == 'R'"><img src="assets/imgs/red.png"></span>
                                        <span ng-if="user.color == 'G'"><img src="assets/imgs/green.png"></span>
                                      </span>
                                      <span ng-switch-when="P"><img title="Vehicle Parked" src="assets/imgs/flag.png"/></span>
                                      <span ng-switch-when="U"><img title="Vehicle NoData" src="assets/imgs/gray.png"/></span>
                                    </td> -->

                                    <td ng-if="trvShow!='true'" ng-switch on="user.position">
                                     <span ng-switch-when="S">Idle</span>
                                     <span ng-switch-when="M">
                                       Moving
                                     </span>
                                     <span ng-switch-when="P">Parked</span>
                                     <span ng-switch-when="U">No Data</span>
                                   </td>

                                   <td ng-if="trvShow=='true'" ng-switch on="user.position">
                                    <span ng-switch-when="S">Idle</span>
                                    <span ng-switch-when="M">
                                     Moving
                                   </span>
                                   <span ng-switch-when="P">Parked</span>
                                   <span ng-switch-when="U">No Data</span>
                                 </td>

                                 <td ng-switch on="user.position">
                                  <span ng-switch-when="S">{{msToTime(user.idleTime)}}</span>
                                  <span ng-switch-when="M">{{msToTime(user.movingTime)}}</span>
                                  <span ng-switch-when="P">{{msToTime(user.parkedTime)}}</span>
                                  <span ng-switch-when="U">{{msToTime(user.noDataTime)}}</span>
                                </td>
                                <!-- <td>{{user.gsmLevel}}</td> -->
                             <!-- <td ng-switch on="user.status">
                                      <span ng-switch-when="OFF"><img title="GPS OFF" src="assets/imgs/gof.png"/></span>
                                      <span ng-switch-when="ON"><img title="GPS ON" src="assets/imgs/gon.png"/></span>
                                  </td>
                                 <td ng-switch on="user.ignitionStatus">
                                      <span ng-switch-when="OFF"><img src="assets/imgs/no.png"/></span>
                                      <span ng-switch-when="ON"><img src="assets/imgs/yes.png"/></span>
                                    </td> -->
                                    <!--<td ng-if="user.loadTruck=='nill'">--</td>-->
                                    <!--<td ng-if="user.loadTruck!='nill'">{{user.loadTruck}}</td>-->
                                    <!--<td>{{user.powerStatus}}</td>-->
                                    <td ng-switch on="user.powerStatus">
                                     <span>{{user.powerStatus}}</span>
                                   </td>

                                   <td ng-switch on="user.vehicleBusy">
                                    <span ng-switch-when="yes" style="color: #ff0045"><?php echo Lang::get('content.on'); ?></span>
                                    <span ng-switch-when="no" style="color: #00ce2f"><?php echo Lang::get('content.off'); ?></span>
                                  </td>
                                  <td ng-switch on="user.fuel" ng-if="groupFuel.length">
                                   <span ng-switch-when="yes">{{user.fuelLitre}}</span>
                                   <span ng-switch-when="no"> - </span>
                                 </td>              
                                 <td>{{user.temperature}}</td>
                                 <td ng-if="user.address!=null" address={{user.address}}>{{user.address}}</td>
                                 <td style="cursor: pointer;" get-location lat={{user.latitude}} lon={{user.longitude}} index={{$index}} ng-if="user.address==null && mainlist[$index]==null"><?php echo Lang::get('content.clickme'); ?></td>
                                 <td style="cursor: pointer;" ng-if="user.address==null && mainlist[$index]!=null">{{mainlist[$index]}}</td>
                                 <td><a href="https://www.google.com/maps?q=loc:{{user.latitude}},{{user.longitude}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>                            
                               </tr>
                               <tr ng-if="data1.vehicleLocations==null || data1.vehicleLocations.length==0">
                                <td colspan="16" class="err"><h5><?php echo Lang::get('content.no_data_found'); ?></h5></td>
                              </tr>
                            </table>   
                          </div> 
                        </div>
                      </div>  
                    </tab>
                    <tab ng-show="consolTabShow" heading="<?php echo Lang::get('content.cons_report'); ?>" ng-click="dialogBox()" active="actTab" id="consoldate" data-toggle="modal" data-target="#myModal"> 

                      <div class="box box-primary1" ng-show="consolTabShow">

                       <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">

                        <h5 class="box-title"><span><?php echo Lang::get('content.cons_report'); ?></span>
                          <span><?php include('OutOfOrderDataInfo.php');?></span>                           
                        </h5>
                      <!-- <div class="form-group " style="margin-top: 10px;margin-right : 5px; ">
                              
                      </div> -->
                      <div class="box-tools pull-right">
                       <?php include('helpVideos.php');?>
                     </div>
                   </div>

                   <!-- modal box -->
                   <div class="modal fade" id="myModal" role="dialog" style="top: 100px">
                    <div class="modal-dialog">

                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title"><?php echo Lang::get('content.cons_report'); ?></h4>
                        </div>
                        <div class="modal-body">
                          <p><?php echo Lang::get('content.report_5-10_sec'); ?></p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal" ng-click="consoldate()"><?php echo Lang::get('content.ok'); ?></button>
                          <button type="button" class="btn btn-default" data-dismiss="modal" ng-click="dateFunction()"><?php echo Lang::get('content.close'); ?></button>
                        </div>
                      </div>

                    </div>
                  </div>

                  <!-- modal box -->
                  <div class="modal fade" id="connSlow" role="dialog" style="top: 100px">
                    <div class="modal-dialog">

                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title"><?php echo Lang::get('content.cons_report'); ?></h4>
                        </div>
                        <div class="modal-body" align="center">
                          <h4>{{connSlow}}</h4>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Lang::get('content.close'); ?></button>
                        </div>
                      </div>

                    </div>
                  </div>


                  <div class="row">
                   <div class="col-md-1"></div>
                   <div class="col-md-2" align="center">
                    <div class="form-group">
                      <div class="input-group datecomp">
                        <input type="text" ng-model="fromdate1" class="form-control placholdercolor" id="dateFrom" placeholder="<?php echo Lang::get('content.fromdate'); ?>">
                        <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2" align="center">
                    <div class="form-group">
                      <div class="input-group datecomp" id="timeFromh">
                        <input type="text" class="form-control placholdercolor" ng-model="fromTime" id="timeFrom" placeholder="<?php echo Lang::get('content.from_time'); ?>">
                        <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                      </div>
                    </div>

                  </div>
                  <div class="col-md-2" align="center">
                    <div class="form-group">
                      <div class="input-group datecomp">
                        <input type="text" ng-model="todate1" class="form-control placholdercolor" id="dateTo" placeholder="<?php echo Lang::get('content.todate'); ?>">
                        <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2" align="center">
                    <div class="form-group">
                      <div class="input-group datecomp">
                        <input type="text" ng-model="totime" class="form-control placholdercolor" id="timeTo" placeholder="<?php echo Lang::get('content.to_time'); ?>">
                        <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2" align="center" style="margin-top: 5px;">
                    <div class="form-group">
                      <div class="input-group datecomp">
                       <button ng-click="consoldate1()" style="margin-left: -100%;" id="but"><?php echo Lang::get('content.submit'); ?></button>

                     </div>
                   </div>
                 </div>

               </div>
               <div class="row">
                <div class="box-tools pull-right" style="margin-right: 30px;">

                  <img style="cursor: pointer;" ng-click="exportData('conreport')"  src="../resources/views/reports/image/xls.png" />
                  <img width=30 height=30 style="cursor: pointer;" ng-click="exportDataCSV('conreport')"  src="../resources/views/reports/image/csv.jpeg" />
                  <img style="cursor: pointer;" onclick="generatePDF3()" src="../resources/views/reports/image/Adobe.png" />
                </div>
              </div>

              <div id="conreport" >

                <div class="box-body">
                 <div id="formConfirmation3">
                  <table id="table" class="table table-striped table-bordered table-condensed table-hover">
                    <tbody ng-repeat="user in consoldateData track by $index" >
                      <tr style="text-align:center">
                        <td colspan="2" style="font-size:13px;background-color:#C2D2F2;"><b>{{vehiLabel}} <?php echo Lang::get('content.name'); ?> : &nbsp;{{user.vehicleName}}</b></td>
                        <td colspan="4" style="font-size:13px;background-color:#C2D2F2;"><b><?php echo Lang::get('content.from'); ?> :  {{fromTime}}  {{fromdate1|date:'dd-MM-yyyy'}}   -   <?php echo Lang::get('content.to'); ?> :  {{totime}}  {{todate1|date:'dd-MM-yyyy'}}</b></td>
                        <td colspan="3" style="font-size:13px;background-color:#C2D2F2;"><b><?php echo Lang::get('content.trip_distance'); ?> : &nbsp;{{ (user.vehicleMode=='DG')? '-' :  user.totalTripLength}} {{ (user.vehicleMode=='DG')? '' : Km }} </b></td>
                      </tr>
                      <tr style="text-align:center">
                        <td colspan="2" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.moving'); ?> :&nbsp; {{msToTime(user.totalMovingTime)}}</b></td>
                        <td colspan="2" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.parked'); ?> :&nbsp; {{msToTime(user.totalParkedTime)}}</b></td>
                        <td style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.idle'); ?> :&nbsp; {{msToTime(user.totalIdleTime)}}</b></td>
                        <td colspan="1" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.speed_hike'); ?> : &nbsp;{{user.topSpeed}} Km</b></td>
                        <td colspan="1" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.avg_speed'); ?> : &nbsp;{{user.averageSpeed}} Km</b></td>
                      </tr>
                      <tr><td colspan="7" bgcolor="black"></td><tr>
                        <tr class="active" style="text-align:center">
                          <td rowspan="2"><b><?php echo Lang::get('content.status'); ?></b></td>
                          <td rowspan="2"><b><?php echo Lang::get('content.start'); ?></b></td>
                          <td rowspan="2"><b><?php echo Lang::get('content.end'); ?></b></td>
                          <td rowspan="2"><b><?php echo Lang::get('content.duration'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</b></td>
                          <td width="40%" colspan="3"><b><?php echo Lang::get('content.location'); ?> <?php echo Lang::get('content.details'); ?></b></td>
                        </tr>
                        <tr class="active" style="text-align:center">
                          <td><b><?php echo Lang::get('content.length'); ?></b></td>
                          <td><b><?php echo Lang::get('content.speed_hike'); ?></b></td>
                          <td><b><?php echo Lang::get('content.speed_avg'); ?></b></td>
                        </tr>
                        <tr ng-if="response=='Success'" ng-repeat="subdata in user.historyConsilated track by $index" ng-style="{ 'color':user.position=='U'?'red':'black' }"  style="text-align:center">
                          <td ng-switch on="subdata.state">
                            <span ng-switch-when="S"><?php echo Lang::get('content.idle'); ?></span>
                            <span ng-switch-when="M"><?php echo Lang::get('content.moving'); ?></span>
                            <span ng-switch-when="P"><?php echo Lang::get('content.parked'); ?></span>
                            <span ng-switch-when="U"><?php echo Lang::get('content.nodata'); ?></span>
                          </td>
                          <td>{{subdata.startTime | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                          <td>{{subdata.endTime | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                          <td>{{msToTime(subdata.duration)}}</td>
                          <td ng-if="subdata.state !='M'" colspan="3">
                            <p> {{subdata.address}} </p>
                            <p ng-if="subdata.address == null"><img src="assets/imgs/loader.gif" align="middle"></p>
                          </td>
                          <td ng-if="subdata.state =='M'">{{subdata.tripDistance}} <?php echo Lang::get('content.km'); ?></td>
                          <td ng-if="subdata.state =='M'">{{subdata.topSpeed}} <?php echo Lang::get('content.km'); ?></td>
                          <td ng-if="subdata.state =='M'">{{subdata.averageSpeed}} <?php echo Lang::get('content.km'); ?></td>
                        </tr>
                        
                      </tbody>
                     <!--  <tr ng-if="consoldateData.length==0 && consoldateData1.length==0&&(!totimeErr)" style="text-align: center">
                        <td colspan="9" class="err"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
                      </tr> -->
                      <!-- <tr ng-show="totimeErr" style="text-align: center">
                        <td colspan="9" class="err"><h5><?php echo Lang::get('content.totimeErrorMsg'); ?></h5></td>
                      </tr>
                    -->                      <tr ng-if="response=='failure'" style="text-align: center">
                      <td colspan="9" class="err"><h5>{{error}}</h5></td>
                    </tr>

                  </table>
                </div>
                <!-- 
                <div id="hid">
                  <div style="display: flex; width: 100%">
                    <div style="float: center">
                      Its  data intesive report. Its will take 20-30 seconds to display. Please click OK to continue.
                    </div>
                    <div id="ok">
                      <span  ng-click="consoldate()">OK</span>
                    </div>
                    <div id="ok">
                      <span>Cancel</span>
                    </div>
                  </div>
                </div> -->
                
              </div>
            </div>
          </div>
          <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">

              </div>
            </div>
          </div>
        </tab>
        <tab ng-show="conSitTabShow" heading="<?php echo Lang::get('content.cons_sites'); ?>" active="siteTab" ng-click="consoldateTrip('trip')" id="tripTab">
          <div class="box box-primary" ng-show="conSitTabShow">
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
              <h5 class="box-title"><span><?php echo Lang::get('content.cons_sites'); ?></span>
                <span><?php include('OutOfOrderDataInfo.php');?></span> 
              </h5>
              <div class="box-tools pull-right">

               <?php include('helpVideos.php');?>

             </div>

           </div> 
           <div class="row">
            <div class="col-xs-1" align="center"></div>
            
            <div class="col-md-2" align="center" >
              <div class="form-group">
                <div class="input-group datecomp">
                  <input type="text" ng-model="fromDateSite" class="form-control placholdercolor" id="tripDatefrom" placeholder="<?php echo Lang::get('content.fromdate'); ?>">
                  <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div></input>/ -->
                </div>
              </div>
            </div>
            <div class="col-md-2" align="center">
              <div class="form-group">
                <div class="input-group datecomp" >
                  <input type="text" ng-model="fromTimeSite" class="form-control placholdercolor"  id="tripTimeFrom" placeholder="<?php echo Lang::get('content.from_time'); ?>">
                  <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                </div>
              </div>

            </div>

            <div class="col-md-2" align="center">
              <div class="form-group">
                <div class="input-group datecomp">
                  <input type="text" ng-model="toDateSite" class="form-control placholdercolor" id="tripDateTo" placeholder="<?php echo Lang::get('content.todate'); ?>">
                  <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                </div>
              </div>
            </div>

            <div class="col-md-2" align="center">
              <div class="form-group">
                <div class="input-group datecomp">
                  <input type="text" ng-model="toTimeSite" class="form-control placholdercolor" id="tripTimeTo" placeholder="<?php echo Lang::get('content.to_time'); ?>">
                  <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                </div>
              </div>
            </div>

          </div>

          <div class="row">
            <div class="col-md-1" align="center"></div>

            <div class="col-md-2" align="center">
              <div class="form-group">

              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-1" align="center"></div>

            <div class="col-md-2" align="center">
              <div class="form-group">
              </div>
            </div>
          </div>
          <div class="row">

           <div class="col-xs-1" align="center"></div>
           <div class="col-xs-1" align="center"></div>

           <div class="col-md-2" align="center">

            <div class="form-group">
              <label><input type="checkbox" ng-model="checkBox.site"><span style="font-size: 12px;padding:0 0 0 10px;font-weight: bold; "><?php echo Lang::get('content.site'); ?></span></label></div>

            </div>
            <div class="col-md-2" align="center">
              <div class="form-group">
                <label><input type="checkbox" ng-model="checkBox.loc"><span style="font-size: 12px;padding:0 0 0 10px;font-weight: bold; "><?php echo Lang::get('content.loc'); ?></span></label>
              </div>
            </div>
            <div class="col-md-2" align="center">
              <div class="form-group">
                <div class="input-group datecomp">
                 <button style="margin-left: -100%;" ng-click="consoldateTrip('tripButon')"><?php echo Lang::get('content.submit'); ?></button>
               </div>
             </div>
           </div>

           <div class="box-body" id="consoledateTrip">

            <div class="col-md-12">
              <hr>
              <div class="row">
                <div class="box-tools pull-right" style="margin-right: 30px;margin-bottom: 10px;">

                  <img style="cursor: pointer;" ng-click="exportData('consoledateTrip')"  src="../resources/views/reports/image/xls.png" />
                  <img width=30 height=30 style="cursor: pointer;" ng-click="exportDataCSV('consoledateTrip')"  src="../resources/views/reports/image/csv.jpeg" />
                  <img style="cursor: pointer;" onclick="generatePDF1()"  src="../resources/views/reports/image/Adobe.png" />
                </div>
              </div>
              <div id="formConfirmation1">
                <table class="table table-striped table-bordered table-condensed table-hover table-fixed-header3" id='tableCLS'>
                  <thead class='header' style=" z-index: 1;">
                    <tr style="text-align:center">
                      <th style="text-align:center;background-color:#C2D2F2;" custom-sort order="'startTime'" sort="sort"><?php echo Lang::get('content.date_time'); ?></th>
                      <th style="text-align:center;background-color:#C2D2F2;" custom-sort order="'vehicleName'" sort="sort">{{vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></th>
                      <th style="text-align:center;background-color:#C2D2F2;" custom-sort order="'state'" sort="sort"><?php echo Lang::get('content.state'); ?></th>
                      <th style="text-align:center;background-color:#C2D2F2;" custom-sort order="'address'" sort="sort"><?php echo Lang::get('content.loc'); ?></th>

                    <!-- <th style="text-align:center;">Fuel Consume</th>
                    <th style="text-align:center;">Temp Fuel</th>
                    <th style="text-align:center;">Fuel From</th>
                    <th style="text-align:center;">Fuel To</th>
                    <th style="text-align:center;">Duration</th> -->
                  </tr>
                </thead>
                <tr ng-repeat="tripVal in tripData.mulitple | orderBy:natural(sort.sortingOrder):sort.reverse" >
                  <td style="text-align:center;">{{tripVal.startTime | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                  <td style="text-align:center;">{{tripVal.vehicleName}}</td>
                  <td style="text-align:center;">{{tripVal.state}}</td>
                  <td style="text-align:center;">{{tripVal.address}}</td>
                    <!-- <td>{{tripVal.fuelConsume}}</td>
                    <td>{{tripVal.tempFuelLitre}}</td>
                    <td>{{tripVal.fuelFrom}}</td>
                    <td>{{tripVal.fuelTo}}</td>
                    <td>
                      <div ng-if="tripVal.tduration==undefined">--</div>
                      <div ng-if="tripVal.tduration!=undefined">{{msToTime(tripVal.tduration)}}</div>
                    </td> -->
                  </tr>
                  <!-- <tr ng-if="tripData.mulitple.length==0 || tripData.mulitple==' ' || tripData.mulitple==null" align="center" >
                      <td ng-show="msgShow" colspan="4" class="err"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
                    <td ng-hide="msgShow" colspan="4" class="err"><h5>{{tripDataErr[0].error}}</h5></td>

                  </tr> -->
                  <tr ng-if="tripDataErr"align="center" >
                    <td ng-show="tripDataErr" colspan="4" class="err"><h5>{{tripDataErr}}</h5></td>
                  </tr>
                </table>
              </div>
            </div>

          </div>
        </div>
      </div>
    </tab>

<!--      <tab heading="Site Location" active="newSiteTab" ng-click="siteLocFunc('newSiteTab')">
          <div class="box box-primary">
            
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                <h5 class="box-title">Site Location</h5>
                  <div class="box-tools pull-right">
                   <!-- <a href="" target="_blank"><img src="../resources/views/reports/image/Adobe.png" /></a> -->
<!--                  <img style="cursor: pointer;" ng-click="exportData('newSite')"  src="../resources/views/reports/image/xls.png" />
                    <img width=30 height=30 style="cursor: pointer;" ng-click="exportDataCSV('newSite')"  src="../resources/views/reports/image/csv.jpeg" />
                  </div>
              </div> 
            
            <div class="row">

                  <div class="col-xs-1" align="center"></div>

              <div class="col-md-2" align="center" >
                        <div class="form-group">
                            <div class="input-group datecomp">
                                  <input type="text" ng-model="fromDateSite" class="form-control placholdercolor" id="tripDateFrom" placeholder="From date">
                                  <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div></input>/ -->
<!--                        </div>
                          </div>
                      </div>

                    <div class="col-md-2" align="center">
                          <div class="form-group">
                              <div class="input-group datecomp" >
                                          <input type="text" ng-model="fromTimeSite" class="form-control placholdercolor"  id="tripTimeFrom" placeholder="From time">
                                          <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
<!--                                    </div>
                            </div>
                            
                      </div>

                      <div class="col-md-2" align="center">
                          <div class="form-group">
                              <div class="input-group datecomp">
                                    <input type="text" ng-model="toDateSite" class="form-control placholdercolor" id="tripDateTo" placeholder="To date">
                                    <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
<!--                            </div>
                            </div>
                       </div>

                       <div class="col-md-2" align="center">
                          <div class="form-group">
                              <div class="input-group datecomp">
                                    <input type="text" ng-model="toTimeSite" class="form-control placholdercolor" id="tripTimeTo" placeholder="To time">
                                    <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
<!--                              </div>
                            </div>
                        </div>
                     
               
                    <!--  <div class="col-md-2" align="center">                       
                          <div class="form-group">
                          <label><input type="checkbox" ng-model="checkBox.site">Site</label></div>
                      </div>

                      <div class="col-md-2" align="center">
                        <div class="form-group">
                          <label><input type="checkbox" ng-model="checkBox.loc">Location</label>
                        </div>
                      </div>  -->

<!--                     <div class="col-md-1" align="center">
                        <div class="form-group">
                          <div class="input-group datecomp">
                              <button style="margin-left: -100%;" ng-click="siteLocFunc('newSiteTab')">Submit</button>
                            </div>
                        </div>
                    </div>

                    <div class="box-body" id="newSite">
                      
                      <div class="col-md-12">
                        <hr>
                         <table class="table table-striped table-bordered table-condensed table-hover" >
                                          
                                    <tbody ng-repeat="data in tripSiteLocData" >
                                      <tr style="text-align:center;height:30px;">
                                        <td colspan="3" style="font-size:13px;background-color:#C2D2F2;" ><b>Vehicle Name :{{data.vehicleName}}</b></td>
                                      </tr>
                                      <!-- <tr><td colspan="4" bgcolor="grey"></td><tr> -->
<!--                                      <tr>
                                        <td width="20%" style="font-size:12px;background-color:#ecf7fb;"><b>Date&Time</b></td>
                                        <td width="40%" style="font-size:12px;background-color:#ecf7fb;"><b>Site Name</b></td>
                                        <td width="20%" style="font-size:12px;background-color:#ecf7fb;"><b>Duration</b></td>
                                       </tr>
                                     
                                      <tr ng-repeat="subdata in data.mulitple" style="padding-bottom:20px;">
                                         <td width="20%" >{{subdata.deviceTime | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                                         <td width="40%" >{{subdata.siteName}}</td>
                                         <td width="20%" >{{msToTime(subdata.duration)}}</td>
                                      </tr>

                                      <tr ng-if="data.mulitple.length == 0 || data.mulitple==' ' || data.mulitple == null" style="text-align: center">
                                        <td colspan="3" class="err"><h5>No Data Found! Choose some other date</h5></td>
                                      </tr>

                                      <tr><td colspan="3" bgcolor="grey"></td><tr>
                                </tbody>
                            </table> 

                      </div>
                    </div>
                </div>
            </div>
          </tab>  -->

          <tab ng-show="overTabShow" heading="<?php echo Lang::get('content.cons_overspeed'); ?>" active="overTab" ng-click="consOverspeed('ovr')">
            <div class="box box-primary1" ng-show="overTabShow">
              <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                <h5 class="box-title"><span><?php echo Lang::get('content.cons_overspeed'); ?></span>
                  <span><?php include('OutOfOrderDataInfo.php');?></span> 
                </h5>
                <div class="box-tools pull-right">
                  <!-- <button type="button" class="btn btn-primary" ng-click="durationFilter('yesterday')" ng-disabled="yesterdayDisabled">Yesterday</button>
                    <button type="button" class="btn btn-success" ng-click="durationFilter('lastweek')" ng-disabled="weekDisabled">Lastweek</button>
                    <button type="button" class="btn btn-info" ng-click="durationFilter('month')" ng-disabled="monthDisabled">Month</button> -->
                    <!-- <a href="" target="_blank"><img src="../resources/views/reports/image/Adobe.png" /></a> -->
                    <img style="cursor: pointer;" ng-click="exportData('consoledateOvrSpeed')"  src="../resources/views/reports/image/xls.png" />
                    <img width=30 height=30 style="cursor: pointer;" ng-click="exportDataCSV('consoledateOvrSpeed')"  src="../resources/views/reports/image/csv.jpeg" />
                    <img style="cursor: pointer;" onclick="generatePDF2()"  src="../resources/views/reports/image/Adobe.png" />
                  </div>

                </div> 
                <div class="row">
                  <div class="col-md-1" align="center"></div>
                  <div class="col-md-2" align="center">
                    <div class="form-group">
                      <div class="input-group datecomp">
                        <input type="text" ng-model="fromdate2" class="form-control placholdercolor" id="ovrFrom" placeholder="<?php echo Lang::get('content.fromdate'); ?>">
                        <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2" align="center">
                    <div class="form-group">
                      <div class="input-group datecomp" >
                        <input type="text" class="form-control placholdercolor" ng-model="fromTime2" id="ovrTimeFrom" placeholder="<?php echo Lang::get('content.from_time'); ?>">
                        <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                      </div>
                    </div>

                  </div>
                  <div class="col-md-2" align="center">
                    <div class="form-group">
                      <div class="input-group datecomp">
                        <input type="text" ng-model="todate2" class="form-control placholdercolor" id="ovrTo" placeholder="<?php echo Lang::get('content.todate'); ?>">
                        <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2" align="center">
                    <div class="form-group">
                      <div class="input-group datecomp">
                        <input type="text" ng-model="toTime2" class="form-control placholdercolor" id="ovrTimeTo" placeholder="<?php echo Lang::get('content.to_time'); ?>">
                        <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2" align="center">
                    <div class="form-group">
                      <div class="input-group datecomp">
                       <button ng-click="consOverspeed('ovrButon')" style="margin-left: -100%;" ><?php echo Lang::get('content.submit'); ?></button>


                     </div>
                   </div>
                 </div>



                 <div class="box-body" id="consoledateOvrSpeed">
                  <div id="formConfirmation2">
                    <div class="col-md-12">
                      <hr>
                      <table class="table table-striped table-bordered table-condensed table-hover" >

                        <tbody ng-repeat="data in ovrData track by $index">
                         <tr >
                          <td colspan="2" style="font-size:13px;background-color:#C2D2F2;"><b>{{vehiLabel | translate }} <?php echo Lang::get('content.name'); ?> : {{data.shortName}}</b></td>
                          <td  colspan="1" style="font-size:13px;background-color:#C2D2F2;"><b><?php echo Lang::get('content.speed_limit'); ?> : {{data.speedLimit}}</b></td>
                          <td colspan="2" style="font-size:13px;background-color:#C2D2F2;"><b><?php echo Lang::get('content.start_time'); ?> : {{data.startTime | date:'hh:mm a dd-MM-yyyy'}}</b></td>
                          <td  colspan="2" style="font-size:13px;background-color:#C2D2F2;"><b><?php echo Lang::get('content.end_time'); ?> : {{data.endTime | date:'hh:mm a dd-MM-yyyy'}}</b></td>

                        </tr>
                        <!-- <tr><td colspan="4" bgcolor="grey"></td><tr> -->
                          <tr>
                            <td width="20%" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.date_time'); ?></b></td>
                            <td width="10%" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.overspeed'); ?></b></td>
                            <td width="10%" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.overspeed_duration'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</b></td>
                            <td  width="15%" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.dri_name'); ?></b></td>
                            <td  width="15%" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.dri_mob'); ?></b></td>
                            <td width="35%" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.loc'); ?></b></td>
                            <td width="15%" style="font-size:12px;background-color:#ecf7fb;"><b><?php echo Lang::get('content.distance_covered'); ?><?php echo Lang::get('content.KMS'); ?></b></td>

                          </tr>

                          <tr ng-repeat="subdata in data.history" style="padding-bottom:20px;">
                            <td width="20%" >{{subdata.time | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                            <td width="10%" >{{subdata.overSpeed}}</td>
                            <td width="20%" >{{msToTime(subdata.overSpeedDuration)}}</td>
                            <td   width="15%" >{{ (data.driverName)? data.driverName : '-'}}</td>
                            <td   width="15%" >{{ (data.driverContactNo)? data.driverContactNo: '-'}}</td>
                            <td ng-show="subdata.location!=null" width="35%" >{{subdata.location}}</td>
                            <td ng-show="subdata.location==null"><a href="https://www.google.com/maps?q=loc:{{subdata.latitude}},{{subdata.langitude}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
                            <td   width="15%" >{{ (data.vehicleMode=='DG')? '-' : subdata.distance}}</td>

                          </tr>

                          <tr ng-if="data.history==null" style="text-align: center">
                            <td colspan="7" class="err"><h5>{{data.error}}</h5></td>
                          </tr>
                          <tr><td colspan="7" bgcolor="grey"></td><tr>
                          </tbody>

                          <tr ng-if="ovrData.length==0" style="text-align: center">
                            <td colspan="7" class="err"><h5><?php echo Lang::get('content.no_data_found'); ?></h5></td>
                          </tr> 

                        </table>                   
                      </div>
                    </div>


                  </div>  
                </div>


              </div>
            </tab>
          </tabset>
        </div>
        <!--  added for last transmission Report -->
        <div id="LastTransmission" ng-show="false">
         <table class="table table-striped table-bordered table-condensed table-hover" id="table_address">

          <tr style="text-align:center">
            <th width="10%" class="id" style="text-align:center;background-color:#d2dff7;"> <?php echo Lang::get('content.asset'); ?> <?php echo Lang::get('content.id'); ?></th>
            <th width="10%" class="id" style="text-align:center;background-color:#d2dff7;">{{vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></th>

            <!-- <th ng-hide="true" style="text-align:center;">Vehicle Name</th> -->
            <th width="5%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.deviceId'); ?></th>
            <th width="5%" class="id" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.orgName'); ?></th>
            <th width="5%" class="id"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.reg_no'); ?></th>
            <th width="5%" class="id" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.groups'); ?> </th>
            <th width="5%" class="id" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.position'); ?></th>
            <th width="8%" class="id" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.ign'); ?></th>
            <th width="10%" class="id" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.Last_Comm'); ?></th>
            <th width="10%" class="id"style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.last_loc'); ?></th>
            <th width="27%" class="id" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.address'); ?></th>
            <th width="5%" class="id" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.speed'); ?></th>


          </tr>
          <tr ng-repeat="user in VehicleStatus | orderBy:natural(sort.sortingOrder):sort.reverse" class="active" style="text-align:center; font-size: 11px">
            <td  style="white-space: nowrap;">{{user.vehicleId}}</td>
            <td  style="white-space: nowrap;">{{user.vehicleName}}</td>

            <td>{{user.deviceId}}</td>
            <td>{{user.orgId}}</td>
            <td>{{user.regNo}}</td>
            <td>{{user.groups}}</td>

            <td ng-switch on="user.position">
              <span ng-switch-when="S"><?php echo Lang::get('content.idle'); ?></span>
              <span ng-switch-when="M"><?php echo Lang::get('content.moving'); ?>
            </span>
            <span ng-switch-when="N"><?php echo Lang::get('content.notsync'); ?></span>
            <span ng-switch-when="P"><?php echo Lang::get('content.parked'); ?></span>
            <span ng-switch-when="U"><?php echo Lang::get('content.nodata'); ?></span>
          </td>

          <td ng-switch on="user.ignition">
            <span ng-switch-when="ON" style="color: #ff0045"><?php echo Lang::get('content.on'); ?></span>
            <span ng-switch-when="OFF" style="color: #00ce2f"><?php echo Lang::get('content.off'); ?></span>
          </td>
          <td  style="white-space: nowrap;">{{user.lastComm | date:'yyyy-MM-dd HH:mm:ss'}}</td>    
          <td  style="white-space: nowrap;">{{user.lastLoc | date:'yyyy-MM-dd HH:mm:ss'}}</td>    
          <td>{{user.address}}</td>
          <td>{{user.speed}}</td>
        </tr>
        <tr ng-if="VehicleStatus==null || VehicleStatus.length==0">
          <td colspan="15" class="err"><h5><?php echo Lang::get('content.no_data_found'); ?></h5></td>
        </tr>
      </table>   
    </div> 
    <div id="SMPLastTran" ng-show="false">
      <table class="table table-striped table-bordered table-condensed table-hover" id="table_address">

        <tr style="text-align:center">
          <th width="5%" class="id" style="text-align:center;background-color:#d2dff7;">{{vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></th>
          <th width="5%" class="id" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.deviceId'); ?></th>
          <th width="5%" class="id" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.orgName'); ?></th>
          <th width="5%" class="id" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.position'); ?></th>
          <th width="8%" class="id" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.ign'); ?></th>
          <th width="10%" class="id" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.Last_Comm'); ?></th>
          <th width="10%" class="id"style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.last_loc'); ?></th>
          <th width="27%" class="id" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.address'); ?></th>
          <th width="5%" class="id" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.speed'); ?></th>


        </tr>
        <tr ng-repeat="user in VehicleStatus" class="active" style="text-align:center; font-size: 11px">
          <td  style="white-space: nowrap;">{{user.vehicleName}}</td>
          <td  style="white-space: nowrap;">{{user.deviceId}}</td>

          <td>{{user.orgId}}</td>

          <td ng-switch on="user.position">
            <span ng-switch-when="S"><?php echo Lang::get('content.idle'); ?></span>
            <span ng-switch-when="M"><?php echo Lang::get('content.moving'); ?>
          </span>
          <span ng-switch-when="N"><?php echo Lang::get('content.notsync'); ?></span>
          <span ng-switch-when="P"><?php echo Lang::get('content.parked'); ?></span>
          <span ng-switch-when="U"><?php echo Lang::get('content.nodata'); ?></span>
        </td>

        <td ng-switch on="user.ignition">
          <span ng-switch-when="ON" style="color: #ff0045"><?php echo Lang::get('content.on'); ?></span>
          <span ng-switch-when="OFF" style="color: #00ce2f"><?php echo Lang::get('content.off'); ?></span>
        </td>
        <td  style="white-space: nowrap;">{{user.lastComm | date:'yyyy-MM-dd HH:mm:ss'}}</td>    
        <td  style="white-space: nowrap;">{{user.lastLoc | date:'yyyy-MM-dd HH:mm:ss'}}</td>    
        <td>{{user.address}}</td>
        <td>{{user.speed}}</td>
      </tr>
      <tr ng-if="VehicleStatus==null || VehicleStatus.length==0">
        <td colspan="15" class="err"><h5><?php echo Lang::get('content.no_data_found'); ?></h5></td>
      </tr>
    </table>   
  </div>
  <script src="assets/js/static.js"></script>
  <script src="assets/js/jquery-1.11.0.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script> 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  <script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <!--<script src="assets/js/bootstrap.min.js"></script> -->
  <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
  <script src="assets/js/angular-translate.js"></script>
  <script src="../resources/views/reports/customjs/html5csv.js"></script>
  <script src="../resources/views/reports/customjs/moment.js"></script>
  <script src="../resources/views/reports/customjs/FileSaver.js"></script>
  <script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
  <script src="../resources/views/reports/datatable/jquery.dataTables.js"></script>
  <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
  <!--<script src="assets/js/naturalSortVersionDates.js"></script> -->
  <script src="assets/js/vamoApp.js"></script>
  <script src="../resources/views/reports/customjs/reports.js?v=<?php echo Config::get('app.version');?>"></script>
  
  <!-- script src="assets/js/custom.js"></script -->   
  <script>

    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
    }

 // $("#testLoad").load("../public/menu");
 // var logo =document.location.host;
 // var imgName= '/vamo/public/assets/imgs/'+logo+'.small.png';
 // $('#imagesrc').attr('src', imgName);
    // $("#example1").dataTable();

    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
    
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });
    var generatePDF = function() {
      generateAutoPDF('Dashboard','dashboard_table');
    }

    var generatePDF1 = function() {
      generateAutoPDF('ConsolidateLocation&siteReport','tableCLS');
    }

    var generatePDF2 = function() {
      kendo.drawing.drawDOM($("#formConfirmation2"),{paperSize: "A4",multiPage: true,landscape: true }).then(function(group) {
        kendo.drawing.pdf.saveAs(group, "ConsolidateOverspeedReport.pdf");
      });
    }

    var generatePDF3 = function() {
      kendo.drawing.drawDOM($("#formConfirmation3"),{paperSize: "A4",multiPage: true,landscape: true }).then(function(group) {
        kendo.drawing.pdf.saveAs(group, "ConsolidateReport.pdf");
      });
    }

    $("body").on("click", "#downloadPDF", function () {
      html2canvas($('#table'), {
        onrendered: function (canvas) {
          var data = canvas.toDataURL();
          var docDefinition = {
            content: [{
              image: data,
              width: 500
            }]
          };
          pdfMake.createPdf(docDefinition).download("ConsolidateReport.pdf");
        }
      });
    });
  </script>
  <script src='assets/js/table-fixed-header.js'></script>

</body>
</html>
