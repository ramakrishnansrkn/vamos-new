<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title><?php echo Lang::get('content.gps'); ?></title>
<link href="../resources/views/assets/css/datebutton.css" rel="stylesheet" type="text/css" />
</head>
<body>
 <div>


 	                   <div class="col-md-3"></div>
                        <button class="btnDate" ng-click="durationFilter('today')" ng-disabled="todayDisabled" id='today' onclick="setToTime('today')"><?php echo Lang::get('content.today'); ?></button>

                		
                        <button class="btnDate" ng-click="durationFilter('yesterday')" ng-disabled="yesterdayDisabled" id='yesterday' onclick="setToTime('yesterday')"><?php echo Lang::get('content.yesterday'); ?></button>
                    
                   		
                        <button class="btnDate" ng-click="durationFilter('thisweek')" ng-disabled="thisweekDisabled" id='thisweek' onclick="setToTime('thisweek')"><?php echo Lang::get('content.thisweek'); ?></button>
                    
                   		
                        <button class="btnDate" ng-click="durationFilter('lastweek')" ng-disabled="weekDisabled" id='lastweek' onclick="setToTime('lastweek')"><?php echo Lang::get('content.lastweek'); ?></button>
                    
                    	
                    

                   
                    
</div>
</body>
 </html>