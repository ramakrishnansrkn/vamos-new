<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<title><?php echo Lang::get('content.gps'); ?></title>
<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
<link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
  <!-- pdfgen -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
    <script src="https://unpkg.com/jspdf-autotable@2.3.2/dist/jspdf.plugin.autotable.js"></script>
<style type="text/css">
    body{
   font-family: 'Lato', sans-serif;
 /*font-weight: bold;
   font-family: 'Lato', sans-serif;
   font-family: 'Roboto', sans-serif;
   font-family: 'Open Sans', sans-serif;
   font-family: 'Raleway', sans-serif;
   font-family: 'Faustina', serif;
   font-family: 'PT Sans', sans-serif;
   font-family: 'Ubuntu', sans-serif;
   font-family: 'Droid Sans', sans-serif;
   font-family: 'Source Sans Pro', sans-serif;
  */
} 
</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2X3F316479');
</script>

</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
<div id="wrapper" ng-controller="mainCtrl" class="ng-cloak">
    
    <?php include('sidebarList.php');?>
        
    <div id="testLoad"></div>
     <div id="page-content-wrapper">
        <div class="container-fluid">
            <div class="panel panel-default">
             
            </div>   
        </div>
    </div>
 
    <!-- AdminLTE css box-->

    <div class="col-md-12">
       <div class="box box-primary">
        <!-- <div class="row"> -->
            <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
                <h3 class="box-title"><?php echo Lang::get('content.alarm_report'); ?></h3>
            </div>
            <div class="row">
                <div class="col-md-2" align="center">
                        <div class="form-group" ng-if="shortNam!=undefined || shortNam!=null">
                          <h5 style="color: grey;">{{shortNam}}</h5>
                        </div>
                     
                  </div>
                <?php include('dateTime.php');?>
                <div class="col-md-1" align="center"></div>
                 <div class="col-md-1" align="center">
                    <button style="margin-left: -100%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
                </div>
                <div class="col-md-12" align="right" style="margin-bottom: 10px;">
                                   <?php include('DateButtonWithWeekMonth.php');?>
                                </div>
            </div>

              <!--  </div> -->
        </div>
    </div>
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="pull-right">
                
                <img style="cursor: pointer;" ng-click="exportData('alarmreport')"  src="../resources/views/reports/image/xls.png" />
                <img style="cursor: pointer;" ng-click="exportDataCSV('alarmreport')"  src="../resources/views/reports/image/csv.jpeg" />
                <img style="cursor: pointer;" onclick="generateAutoPDF('Alarm_Report','table')"  src="../resources/views/reports/image/Adobe.png" />

            </div>
        <div class="col-md-12">
            <div class="box-body table-responsive" id="alarmreport">
                <p style="margin:0" class="page-header"><?php echo Lang::get('content.alarm_report'); ?> <span style="float: right;"><?php echo Lang::get('content.from'); ?> : {{uiDate.fromtime}} {{uiDate.fromdate | date:'dd-MM-yyyy'}} - <?php echo Lang::get('content.to'); ?> : {{uiDate.totime}} {{uiDate.todate | date:'dd-MM-yyyy'}}</span></p>
                         <div id="formConfirmation">   
                    <table class="table table-bordered table-striped table-condensed table-hover table-fixed-header" id='table'>
                        <thead>
                            <tr style="text-align:center; font-weight: bold;">
                                <td style="background-color:#ecf7fb;">{{ vehiLabel | translate }} <?php echo Lang::get('content.group'); ?></td>
                                <td>{{uiGroup}}</td>
                                <td style="background-color:#ecf7fb;">{{ vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></td>
                                <td>{{shortNam}}</td>   
                            </tr>
                            <tr><td colspan="4"></td></tr>
                        </thead>
                        <thead class='header' style=" z-index: 1;">
                            <tr style="text-align:center;font-weight: bold;">
                                <th class="id" custom-sort order="'alarmTime'" sort="sort" style="text-align:center;background-color:#d3e0f1;" width="25%"><?php echo Lang::get('content.date_time'); ?></th>
                                <th class="id" custom-sort order="'alarmType'" sort="sort" style="text-align:center;background-color:#d3e0f1;" width="25%"><?php echo Lang::get('content.alarm_type'); ?></th>
                                
                                <th class="id" custom-sort order="'address'" sort="sort" style="text-align: center;background-color:#d3e0f1;" width="35%"><?php echo Lang::get('content.Nearest_Loc'); ?></th>
                                <th class="id" style="text-align: center;background-color:#d3e0f1;" width="15%"><?php echo Lang::get('content.gmap'); ?></th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr class="active" style="text-align:center" ng-repeat="alarm in siteData.alarmList | orderBy:sort.sortingOrder:sort.reverse">
                                <td>{{alarm.alarmTime | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                                <!-- <td>{{alarm.toTime | date:'yyyy-MM-dd HH:mm:ss'}}</td> -->
                                <td>
                                    <span ng-if="alarm.alarmType == 'LBA'">Low Battery</span>
                                    <span ng-if="alarm.alarmType == 'PCA'">Battery Disconnected</span>
                                    <span ng-if="alarm.alarmType == 'SHO'">Shock</span>
                                    <span ng-if="alarm.alarmType == 'TPA'">Tampered</span>
                                    <span ng-if="alarm.alarmType == 'A/C yes'">AC ON</span>
                                    <span ng-if="alarm.alarmType == 'SOS'">SOS</span>
                                    <span ng-if="alarm.alarmType == 'A/C no'">AC OFF</span>
                                    <span ng-if="alarm.alarmType != 'LBA'&&alarm.alarmType != 'PCA'&&alarm.alarmType 1= 'SHO'
                                    &&alarm.alarmType != 'TPA'&&alarm.alarmType != 'A/C yes'&&alarm.alarmType != 'SOS'&&alarm.alarmType != 'A/C no'"> {{alarm.alarmType}} </span>
                                </td>
                                <td>
                                
                                    <p ng-if="alarm.address!=null">{{alarm.address}}</p>
                                    <p ng-if="alarm.address==null && addressFuel[$index]!=null">{{addressFuel[$index]}}</p>
                                    <p ng-if="alarm.address==null && addressFuel[$index]==null"><img src="assets/imgs/loader.gif" align="middle"></p>
                                </td> 
                                <td><a href="https://www.google.com/maps?q=loc:{{alarm.lat}},{{alarm.lng}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
                            </tr>
                            <tr ng-if="siteData.alarmList =='' || siteData.alarmList.length==0 || siteData.alarmList == null" align="center">
                                <td colspan="4" class="err" ng-if="!errMsg"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
                                <td colspan="4" class="err" ng-if="errMsg"><h5>{{errMsg}}</h5></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                </div>
              </div>
        </div>
    </div>
</div>
    <script src="assets/js/static.js"></script>
    <script src="assets/js/jquery-1.11.0.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
    <script src="assets/js/ui-bootstrap-0.6.0.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="assets/js/angular-translate.js"></script>
    <script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="../resources/views/reports/customjs/html5csv.js"></script>
    <script src="../resources/views/reports/customjs/FileSaver.js"></script>
    <script src="../resources/views/reports/customjs/moment.js"></script>
    <script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/services.js"></script>
    <script src="assets/js/siteReport.js?v=<?php echo Config::get('app.version');?>"></script>
    <script>

   
        // $("#example1").dataTable();
          
        // $("#menu-toggle").click(function(e) {
        //     e.preventDefault();
        //     $("#wrapper").toggleClass("toggled");
        // });
        
        $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });


  </script>
</body>
</html>