<!DOCTYPE html>
<html lang="en" style="overflow: hidden !important;">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title><?php echo Lang::get('content.gps'); ?></title>

  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
  <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
  <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
  <link href="assets/css/web.css" rel="stylesheet" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="assets/js/gridviewscroll.js"></script>

  <!--  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script> -->

  <style>
    body {
      font-family: 'Lato', sans-serif;
      /* font-weight: bold; */  
   /* font-family: 'Lato', sans-serif;
      font-family: 'Roboto', sans-serif;
      font-family: 'Open Sans', sans-serif;
      font-family: 'Raleway', sans-serif;
      font-family: 'Faustina', serif;
      font-family: 'PT Sans', sans-serif;
      font-family: 'Ubuntu', sans-serif;
      font-family: 'Droid Sans', sans-serif;
      font-family: 'Source Sans Pro', sans-serif;
      */
    }
    .empty{
      height: 1px; width: 1px; padding-right: 30px; float: left;
    }
    .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
      background-color: #ffffff;
    }

  </style>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-2X3F316479');
  </script>
</head>
<div id="preloader" >
  <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
  <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
  <div ng-controller="mainCtrl" class="ng-cloak">
    <div id="wrapper">
      <?php include('sidebarList.php');?> 
      
      <div id="testLoad"></div>
      
      <div id="page-content-wrapper">
        <div class="container-fluid">
          <div class="panel panel-default">

          </div>   
        </div>
      </div>
      
      <div class="col-md-12">
       <div class="box box-primary" style="padding-top: 5px;margin-top: 10px;">
        <!-- <div class="row"> -->
          <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
            <h3 class="box-title"><?php echo Lang::get('content.daywise_truck_pos'); ?>
          </h3>
        </div>
        
        <div class="row" style="margin-top: 20px;">
          <div class="col-md-2" align="center"></div>
          
          <div class="col-md-2 form-group" style="right:0px;"><?php echo Lang::get('content.month_year'); ?>:</div>
          
          <div class="col-md-3" align="center">
           <div class="input-group datecomp" style="padding-bottom: 20px;">
            <input type="text" ng-model="fromMonth" ng-change="changeMonth()" class="form-control placholdercolor" id="monthFrom"  name="date" placeholder="Month">
          </div>
        </div>
        <div class="col-md-1" align="center">
         <div class="form-group">
           <button ng-click="changeMonth()" onclick="undo()" style="margin-left: 0%; margin-top: 2px;"><?php echo Lang::get('content.submit'); ?></button>
         </div>
       </div>
     </div>
     

     <!--  </div> -->
     <div class="row">
      <div class="col-md-1" align="center"></div>

      <div class="col-md-2" align="center">
        <div class="form-group">

        </div>
      </div>
    </div>

  </div>
</div>

<div class="col-md-12">
  <div class="box box-primary" style="min-height:570px;">

    <div>
      <div class="pull-right" style="margin-top: 10px;margin-right: 5px;">

        <img style="cursor: pointer;" ng-click="exportData('DayWise_Truck_Positions')"  src="../resources/views/reports/image/xls.png" />
        <img style="cursor: pointer;" ng-click="exportDataCSV('DayWise_Truck_Positions')"  src="../resources/views/reports/image/csv.jpeg" />
        <!-- <img style="cursor: pointer;" onclick="generatePDF()"  src="../resources/views/reports/image/Adobe.png" /> -->

      </div>

      
      
      <div class="box-body" id="DayWise_Truck_Positions">
        <div class="empty" align="center"></div>
        <div class="row" style="padding-top: 20px;"></div>
        <div id="formConfirmation" style="overflow-x: hidden;">
          <table class="table table-bordered table-striped table-condensed table-hover" style="margin-top: 7px;">
            <thead>
              <tr style="text-align:center;">
                <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.group'); ?></th>
                <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{uiGroup}}</th>
                <th style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.month'); ?> & <?php echo Lang::get('content.year'); ?> </th>
                <th colspan="3" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"> {{selMonthYear}} </th>         
              </tr>
            </thead>
          </table>   

          
          <table cellspacing="0" id="gvMain" style="width: 100%; border-collapse: collapse;" >

            <!-- <thead class='header' style=" z-index: 1;"> -->

              <!-- <tr><td colspan="4" bgcolor="grey"></td><tr> -->
                <tr class="GridViewScrollHeader" style="padding-bottom:20px;">
                 <th class="id" custom-sort order="'vehicleName'" sort="sort" style="font-size:12px;background-color:#ecf7fb;white-space: nowrap;text-align: center;"><?php echo Lang::get('content.veh_name'); ?> </th>
                 <th class="id" custom-sort order="'vehicleId'" sort="sort" style="font-size:12px;background-color:#ecf7fb;white-space: nowrap;text-align: center;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.id'); ?></th>
                 <th  ng-repeat="day in dayArr" style="font-size:12px;background-color:#ecf7fb;white-space: nowrap;text-align: center;"><b>{{day}}</b></th>
                 <th custom-sort order="'numOfCycle'" sort="sort" style="font-size:12px;background-color:#ecf7fb;white-space: nowrap;text-align: center;"><?php echo Lang::get('content.DESP'); ?></th>
                 <th custom-sort order="'totalNumUH'" sort="sort"style="font-size:12px;background-color:#ecf7fb;white-space: nowrap;text-align: center;"><?php echo Lang::get('content.UH'); ?></th>
                 <th custom-sort order="'totalNumFC'" sort="sort" style="font-size:12px;background-color:#ecf7fb;white-space: nowrap;text-align: center;"><?php echo Lang::get('content.UM/FC'); ?></th>
               </tr>
               <!-- </thead> -->
               <!-- <tbody> -->
                <tr class="GridViewScrollItem" ng-if="!errMsg.includes('expired')" style="padding-bottom:20px;" ng-repeat-start="sdata in allData | orderBy:natural(sort.sortingOrder):sort.reverse">
                 <td rowspan="1" style="white-space: nowrap;">{{ sdata.vehicleName }}</td>
                 <td rowspan="1" style="white-space: nowrap;">{{ sdata.vehicleId }}</td>

                 <td ng-repeat="mgmData in sdata.mgmData" style="white-space: nowrap;" ng-if=(sdata.mgmData.length!=0&&($index+1)<=dayArr.length)>
                  <span ng-if="mgmData.status=='NoData'||mgmData.status=='-'"> - </span>
                  <span ng-if="mgmData.status!='NoData'&&mgmData.status!='-'">{{ mgmData.status  }} - {{mgmData.location}}</span>
                </td>
                <td rowspan="2" ng-repeat="date in dayArr" style="white-space: nowrap;" ng-if=(sdata.mgmData.length==0&&($index+1)<=dayArr.length)>
                  <span> - </span>
                </td>
                <td rowspan="2" style="white-space: nowrap;">{{ sdata.numOfCycle }}</td>
                <td rowspan="2" style="white-space: nowrap;">{{ sdata.totalNumUH }}</td>
                <td rowspan="2" style="white-space: nowrap;">-</td>
              </tr>
              <tr ng-if="!errMsg.includes('expired')" ng-repeat-end class="GridViewScrollItem">
               <td rowspan="1" style="white-space: nowrap;">{{ sdata.vehicleName }}</td>
               <td rowspan="1" style="white-space: nowrap;">{{ sdata.vehicleId }}</td>
               <td  ng-repeat="mgmData in sdata.mgmData" style="white-space: nowrap;" ng-if=(sdata.mgmData.length!=0&&($index+1)<=dayArr.length)>
                <span ng-if="mgmData.route=='null'||mgmData.route=='-'"> - </span>
                <span ng-if="mgmData.route!='null'&&mgmData.route!='-'"> {{ mgmData.route  }} </span>
              </td>
            </tr>
            <tr ng-if="allData.length==0 && !errMsg" style="text-align: center" class="GridViewScrollItem">
              <td colspan={{dayArr.length+5}} class="err" ng-if="!errMsg"><h5><?php echo Lang::get('content.no_date_time'); ?></h5></td>
            </tr>
            <tr ng-if="errMsg" style="text-align: center" class="GridViewScrollItem">
              <td colspan="4" class="err" ><h5>{{errMsg}}</h5></td>
            </tr>
            <!-- </tbody> -->
            
          </table>  
          
        </div>                     
      </div>
    </div>
  </div>
</div>
</div>
</div>


<script src="assets/js/static.js"></script>
<script src="assets/js/jquery-1.11.0.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
<script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="assets/js/angular-translate.js"></script>
<script src="../resources/views/reports/customjs/html5csv.js"></script>
<script src="../resources/views/reports/customjs/moment.js"></script>
<script src="../resources/views/reports/customjs/FileSaver.js"></script>
<script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>

<script src="assets/js/vamoApp.js"></script>
<script src="assets/js/services.js"></script>
<script src="assets/js/mgmReport.js?v=<?php echo Config::get('app.version');?>"></script>

<script>



  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });
  
  $(function () {

   $('#monthFrom').datetimepicker({
    minViewMode: 'months',
    viewMode: 'months',
    pickTime: false,
    useCurrent:true,
    format:'MM/YYYY',
    maxDate: new Date,
    minDate: new Date(2015, 12, 1)
  });
 });      
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });

//  var generatePDF = function() {
//   kendo.drawing.drawDOM($("#formConfirmation")).then(function(group) {
//     kendo.drawing.pdf.saveAs(group, "DayWise_Truck_Positions.pdf");
//   });
// }

</script>
<script type="text/javascript">
  var gridViewScroll = null;
  window.onload = function () {

    gridViewScroll = new GridViewScroll({
      elementID: "gvMain",
      width: window.screen.width-330,
      height: window.screen.height-450,
      freezeColumn: true,
                //freezeFooter: true,
                freezeColumnCssClass: "GridViewScrollItemFreeze",
                //freezeFooterCssClass: "GridViewScrollFooterFreeze",
                freezeHeaderRowCount: 1,
                freezeColumnCount: 2,
                onscroll: function (scrollTop, scrollLeft) {
                    // console.log(scrollTop + " - " + scrollLeft);
                  }
                });
    gridViewScroll.enhance();
  }
  function enhance() {
    gridViewScroll.enhance();
  }
  function undo() {
    gridViewScroll.undo();
  }
  
</script>

</body>
</html>
