<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title><?php echo Lang::get('content.gps'); ?></title>
  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
  <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
  <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
  <style>
    .empty{
      height: 1px; width: 1px; padding-right: 30px; float: left;
    }
    .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
      background-color: #ffffff;
    }

    body{
      font-family: 'Lato', sans-serif;
    } 

    .col-md-12 {
      width: 98% !important;
      left: 15px !important;
      padding-left: 20px !important;
    }

    .feeds li .col1>.cont {
      float: left;
      margin-right: 75px;
      overflow: hidden;
    }
    .feeds li .col1, .feeds li .col1>.cont>.cont-col2 {
      width: 98%;
      float: left;
    }
    .feeds li .col2 {
      float: left;
      width: 75px;
      margin-left: -105px;
    }
    .feeds li .col3 {
      float: left;
    }
    .feeds li .col1 {
      clear: both;
    }
/*.feeds li .col1 {
    color: #262829;
    }*/
    .feeds li .col2>.date {
      padding: 4px 9px 5px 4px;
      text-align: right;
      font-style: italic;
      color: #c1cbd0;
      white-space: nowrap;
    }
    .ml-30{
      margin-left: -25px;
    }

  </style>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-2X3F316479');
  </script>
</head>
<div id="preloader" >
  <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
  <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
  <div ng-controller="mainCtrl" class="ng-cloak">
    <div id="wrapper">

      <?php include('sidebarList.php');?>

      <div id="testLoad"></div>

      <div id="page-content-wrapper">
        <div class="container-fluid">
          <div class="panel panel-default"></div>   
        </div>
      </div>

      <div class="col-md-12">
       <div class="box box-primary" style="padding-top: 5px;margin-top: -20px;">
        <!-- <div class="row"> -->
          <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
            <h3 class="box-title"><?php echo Lang::get('content.fuel_notification'); ?> <?php echo Lang::get('content.report'); ?>
          </h3>
        </div>
        <div class="row">
          <div class="col-md-2" align="center">
            <div class="form-group" ng-if="shortNam!=undefined || shortNam!=null">
              <h5 style="color: grey;">{{shortNam}}</h5>
            </div>

          </div>
          <div>
            <?php include('dateTime.php');?>
          </div>

          <div class="col-md-1" align="center">
            <button style="margin-left: -42%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
          </div>
          <div class="col-md-12" align="right" style="margin-bottom: 10px;">
           <?php include('DateButtonWithWeekMonth.php');?>
         </div>

       </div>

       <div class="row">
        <div class="col-md-1" align="center"></div>
        <div class="col-md-2" align="center">
          <div class="form-group"></div>
        </div>
      </div>
    </div>

  </div>

  <div class="col-md-12" style="border-color: unset!important;" ng-show="showPage">
    <div class="box box-primary" style="min-height:570px;border: none;">
      <h5 ng-show="notiError && !errMsg.includes('expired')" class="err text-center" style="padding-top: 30px;">{{notiError}}</h5>
      <h5 ng-show="!notiError && errMsg.includes('expired')" class="err text-center" style="padding-top: 30px;">{{errMsg}}</h5>
      <div class="nav-tabs-custom" style="margin-top: 10px;" ng-hide="notiError">
        <ul class="nav nav-tabs" style="border-bottom-color: white;">
          <li ng-class="{'active': activeTap=='FILL'}" ng-click="changeTabs('fill')">
            <a href="#tab_1_1" data-toggle="tab"><?php echo Lang::get('content.fuel_fill'); ?>
            <span class="badge badge-default" ng-style="{ 'background-color' : (fill_list.length) ? '#196481' : '#777'}">{{fill_list.length}}</span></a>
          </li>
          <li ng-class="{'active': activeTap=='DROP'}" ng-click="changeTabs('drop')">
            <a href="#tab_1_2" data-toggle="tab"><?php echo Lang::get('content.fuel_drop'); ?>
            <span class="badge badge-default" ng-style="{ 'background-color' : (drop_list.length) ? '#196481' : '#777'}">
            {{drop_list.length}} </span></a>
          </li>
          <li ng-class="{'active': activeTap=='EXCESS'}" ng-click="changeTabs('excess')">
            <a href="#tab_1_3" data-toggle="tab"> <?php echo Lang::get('content.excess_fuel_consumption'); ?>  
            <span class="badge badge-default" ng-style="{ 'background-color' : (excess_consumption_list.length) ? '#196481' : '#777'}">
            {{excess_consumption_list.length}} </span></a>
          </li>
                            <!-- <li ng-class="{'active': activeTap=='DROPPED'}">
                                <a href="#tab_1_4" data-toggle="tab" ><?php echo Lang::get('content.fuel_dropped'); ?> 
                                    <span class="badge badge-default" ng-style="{ 'background-color' : (dropped_list.length) ? '#196481' : '#777'}">
                                    {{dropped_list.length}} </span></a>
                                  </li> -->
                                </ul>
                              </div>
                              <div class="box-body" ng-hide="notiError">
                                <!--BEGIN TABS-->
                                <div class="tab-content" style="margin-top: -10px;">

                                  <div ng-class="activeTap=='FILL'?'tab-pane active':'tab-pane'" id="tab_1_1">
                                    <span ng-if="fill_list.length==0 && !notificationError" class="err"><?php echo Lang::get('content.no_notification_change_date_try'); ?></span>
                                    <span ng-if="notificationError" class="err">{{ notificationError }}</span> 

                                    <div ng-if="fill_list.length!=0 && !notificationError"  class="nav-tabs-custom" style="margin-top: 10px;" >
                                      <ul  class="nav nav-tabs" style="border-bottom-color: white;">


                                       <li>
                                        <a href="#tab_ongoing" data-toggle="tab">Ongoing
                                          <span class="badge badge-default" ng-style="{ 'background-color' : (fill_list.length) ? '#196481' : '#777'}">{{ongoing_list.length}}</span></a>
                                        </li>
                                        <li >
                                          <a href="#tab_detection" data-toggle="tab">Detected
                                            <span class="badge badge-default" ng-style="{ 'background-color' : (detection_list.length) ? '#196481' : '#777'}">
                                            {{detection_list.length}} </span></a>
                                          </li>
                                          <li>
                                            <a href="#tab_finalised" data-toggle="tab"> Finalised  
                                              <span class="badge badge-default" ng-style="{ 'background-color' : (finalised_list.length) ? '#196481' : '#777'}">
                                              {{finalised_list.length}} </span></a>
                                            </li>

                                          </ul>
                                        </div>
                                        <div class="tab-content" style="margin-top: -10px;">

                                          <div class="tab-pane"  id="tab_ongoing">
                                            <span ng-if="ongoing_list.length==0" class="err"><?php echo Lang::get('content.no_notification_change_date_try'); ?></span>
                                            <ul  class="feeds" style="list-style-type: none;">
                                              <li ng-if="ongoing_list.length==0" class="err"><?php echo Lang::get('content.no_notification_change_date_try'); ?></li>
                                              <li ng-if="ongoing_list.length!=0" ng-repeat="fuelInfo in ongoing_list  | filter:notnsearch" style="margin-top: 5px;">
                                                <div class="col1">
                                                  <img  class="ml-30" src="assets/imgs/fuel-notification.svg" width="20px" height="20px">
                                                  Fuel Fill is ongoing for 
                                                  {{fuelInfo.shortName}} 
                                                  Quantity : <b>{{ fuelInfo.litres }}</b> Liters 
                                                  <a href="https://www.google.com/maps?q=loc:{{fuelInfo.lat}},{{fuelInfo.lng}}" target="_blank"><span class="glyphicon glyphicon-map-marker"></span><?php echo Lang::get('content.location'); ?> </a>
                                                </div>
                                                <div class="col2">
                                                  <div class="date"> {{fuelInfo.date | date:'HH:mm:ss dd-MM-yyyy' }}</div>
                                                </div>
                                              </li>
                                            </ul>
                                          </div>
                                          <div class="tab-pane"  id="tab_detection">
                                            <ul class="feeds" style="list-style-type: none;">
                                              <li ng-if="detection_list.length==0" class="err"><?php echo Lang::get('content.no_notification_change_date_try'); ?></li>
                                              <li ng-if="detection_list.length!=0" ng-repeat="fuelInfo in detection_list  | filter:notnsearch" style="margin-top: 5px;">
                                                <div class="col1">
                                                  <img  class="ml-30" src="assets/imgs/fuel-notification.svg" width="20px" height="20px">
                                                  Fuel Fill is detected for 
                                                  {{fuelInfo.shortName}} 
                                                  Quantity : <b>{{ fuelInfo.litres }}</b> Liters 
                                                  <a href="https://www.google.com/maps?q=loc:{{fuelInfo.lat}},{{fuelInfo.lng}}" target="_blank"><span class="glyphicon glyphicon-map-marker"></span><?php echo Lang::get('content.location'); ?> </a>
                                                </div>
                                                <div class="col2">
                                                  <div class="date"> {{fuelInfo.date | date:'HH:mm:ss dd-MM-yyyy' }}</div>
                                                </div>
                                              </li>
                                            </ul>
                                          </div>
                                          <div class="tab-pane"  id="tab_finalised">
                                            <ul class="feeds" style="list-style-type: none;">
                                              <li ng-if="finalised_list.length==0" class="err"><?php echo Lang::get('content.no_notification_change_date_try'); ?></li>
                                              <li ng-if="finalised_list.length!=0" ng-repeat="fuelInfo in finalised_list  | filter:notnsearch" style="margin-top: 5px;">
                                                <div class="col1">
                                                  <img  class="ml-30" src="assets/imgs/fuel-notification.svg" width="20px" height="20px">
                                                  Fuel Fill is finalised for 
                                                  {{fuelInfo.shortName}} 
                                                  Quantity : <b>{{ fuelInfo.litres }}</b> Liters 
                                                  <a href="https://www.google.com/maps?q=loc:{{fuelInfo.lat}},{{fuelInfo.lng}}" target="_blank"><span class="glyphicon glyphicon-map-marker"></span><?php echo Lang::get('content.location'); ?> </a>
                                                </div>
                                                <div class="col2">
                                                  <div class="date"> {{fuelInfo.date | date:'HH:mm:ss dd-MM-yyyy' }}</div>
                                                </div>
                                              </li>
                                            </ul>
                                          </div>
                                        </div>
                                      </div>
                                      <div ng-class="activeTap=='DROP'?'tab-pane active':'tab-pane'" id="tab_1_2">
                                       <span ng-if="drop_list.length==0 && !notificationError" class="err"><?php echo Lang::get('content.no_notification_change_date_try'); ?></span>
                                       <span ng-if="notificationError" class="err">{{ notificationError }}</span> 
                                       <div ng-if="drop_list.length!=0 && !notificationError" class="nav-tabs-custom" style="margin-top: 10px;">
                                        <ul class="nav nav-tabs" style="border-bottom-color: white;">
                                          <li>
                                            <a href="#tab_dropongoing" data-toggle="tab">Ongoing
                                              <span class="badge badge-default" ng-style="{ 'background-color' : (fill_list.length) ? '#196481' : '#777'}">{{ongoing_list.length}}</span></a>
                                            </li>
                                            <li >
                                              <a href="#tab_dropdetection" data-toggle="tab">Detected
                                                <span class="badge badge-default" ng-style="{ 'background-color' : (detection_list.length) ? '#196481' : '#777'}">
                                                {{detection_list.length}} </span></a>
                                              </li>
                                              <li>
                                                <a href="#tab_dropfinalised" data-toggle="tab"> Finalised  
                                                  <span class="badge badge-default" ng-style="{ 'background-color' : (finalised_list.length) ? '#196481' : '#777'}">
                                                  {{finalised_list.length}} </span></a>
                                                </li>

                                              </ul>
                                            </div>
                                            <div class="tab-content" style="margin-top: -10px;">

                                              <div class="tab-pane"  id="tab_dropongoing">
                                                <ul class="feeds" style="list-style-type: none;">
                                                  <li ng-if="ongoing_list.length==0" class="err"><?php echo Lang::get('content.no_notification_change_date_try'); ?></li>
                                                  <li ng-if="ongoing_list.length!=0" ng-repeat="fuelInfo in ongoing_list  | filter:notnsearch" style="margin-top: 5px;">
                                                    <div class="col1">
                                                      <img  class="ml-30" src="assets/imgs/fuel-notification.svg" width="20px" height="20px">
                                                      Fuel Drop is ongoing for 
                                                      {{fuelInfo.shortName}} 
                                                      Quantity : <b>{{ fuelInfo.litres }}</b> Liters 
                                                      <a href="https://www.google.com/maps?q=loc:{{fuelInfo.lat}},{{fuelInfo.lng}}" target="_blank"><span class="glyphicon glyphicon-map-marker"></span><?php echo Lang::get('content.location'); ?> </a>
                                                    </div>
                                                    <div class="col2">
                                                      <div class="date"> {{fuelInfo.date | date:'HH:mm:ss dd-MM-yyyy' }}</div>
                                                    </div>
                                                  </li>
                                                </ul>
                                              </div>
                                              <div class="tab-pane"  id="tab_dropdetection">
                                                <ul class="feeds" style="list-style-type: none;">
                                                  <li ng-if="detection_list.length==0" class="err"><?php echo Lang::get('content.no_notification_change_date_try'); ?></li>
                                                  <li ng-if="detection_list.length!=0" ng-repeat="fuelInfo in detection_list  | filter:notnsearch" style="margin-top: 5px;">
                                                    <div class="col1">
                                                      <img  class="ml-30" src="assets/imgs/fuel-notification.svg" width="20px" height="20px">
                                                      Fuel Drop is detected for 
                                                      {{fuelInfo.shortName}} 
                                                      Quantity : <b>{{ fuelInfo.litres }}</b> Liters 
                                                      <a href="https://www.google.com/maps?q=loc:{{fuelInfo.lat}},{{fuelInfo.lng}}" target="_blank"><span class="glyphicon glyphicon-map-marker"></span><?php echo Lang::get('content.location'); ?> </a>
                                                    </div>
                                                    <div class="col2">
                                                      <div class="date"> {{fuelInfo.date | date:'HH:mm:ss dd-MM-yyyy' }}</div>
                                                    </div>
                                                  </li>
                                                </ul>
                                              </div>
                                              <div class="tab-pane"  id="tab_dropfinalised">
                                                <ul class="feeds" style="list-style-type: none;">
                                                  <li ng-if="finalised_list.length==0" class="err"><?php echo Lang::get('content.no_notification_change_date_try'); ?></li>
                                                  <li ng-if="finalised_list.length!=0" ng-repeat="fuelInfo in finalised_list  | filter:notnsearch" style="margin-top: 5px;">
                                                    <div class="col1">
                                                      <img  class="ml-30" src="assets/imgs/fuel-notification.svg" width="20px" height="20px">
                                                      Fuel Drop is finalised for 
                                                      {{fuelInfo.shortName}} 
                                                      Quantity : <b>{{ fuelInfo.litres }}</b> Liters 
                                                      <a href="https://www.google.com/maps?q=loc:{{fuelInfo.lat}},{{fuelInfo.lng}}" target="_blank"><span class="glyphicon glyphicon-map-marker"></span><?php echo Lang::get('content.location'); ?> </a>
                                                    </div>
                                                    <div class="col2">
                                                      <div class="date"> {{fuelInfo.date | date:'HH:mm:ss dd-MM-yyyy' }}</div>
                                                    </div>
                                                  </li>
                                                </ul>
                                              </div>
                                            </div>
                                          </div>
                                          <div ng-class="activeTap=='EXCESS'?'tab-pane active':'tab-pane'" id="tab_1_3">

                                            <span ng-if="excess_consumption_list.length==0 && !notificationError" class="err"><?php echo Lang::get('content.no_notification_change_date_try'); ?></span>
                                            <span ng-if="notificationError" class="err">{{ notificationError }}</span> 

                                            
                                            <ul class="feeds" style="list-style-type: none;">
                                              
                                              <li ng-if="excess_consumption_list.length!=0" ng-repeat="fuelInfo in excess_consumption_list  | filter:notnsearch" style="margin-top: 5px;">
                                                <div class="col1">
                                                  <img  class="ml-30" src="assets/imgs/fuel-notification.svg" width="20px" height="20px">
                                                  Excess fuel consumption detected for 
                                                  {{fuelInfo.shortName}} 
                                                  <a href="https://www.google.com/maps?q=loc:{{fuelInfo.lat}},{{fuelInfo.lng}}" target="_blank"><span class="glyphicon glyphicon-map-marker"></span><?php echo Lang::get('content.location'); ?> </a>
                                                </div>
                                                <div class="col2">
                                                  <div class="date"> {{fuelInfo.date | date:'HH:mm:ss dd-MM-yyyy' }}</div>
                                                </div>
                                              </li>
                                            </ul>
                                            
                                          </div>
                            <!-- <div ng-class="activeTap=='DROPPED'?'tab-pane active':'tab-pane'" id="tab_1_4">
                                <ul class="feeds" style="list-style-type: none;">
                                     <span ng-if="dropped_list.length==0 && !notificationError" class="err"><?php echo Lang::get('content.no_notification_change_date_try'); ?></span>
                                     <span ng-if="notificationError" class="err">{{ notificationError }}</span>
                                    <li ng-if="dropped_list.length!=0" ng-repeat="fuelInfo in dropped_list  | filter:notnsearch" style="margin-top: 5px;">
                                        <div class="col1">
                                          <span class="glyphicon glyphicon-exclamation-sign ml-30" style="color: #dc9419"></span> 
                                          Fuel Dropped Detected 
                                           {{fuelInfo.shortName}} 
                                           Quantity : <b>{{ fuelInfo.litres }}</b> Liters 
                                          <a href="https://www.google.com/maps?q=loc:{{fuelInfo.lat}},{{fuelInfo.lng}}" target="_blank"><span class="glyphicon glyphicon-map-marker"></span><?php echo Lang::get('content.location'); ?>
                                          </a>
                                        </div>
                                        <div class="col2">
                                            <div class="date"> {{fuelInfo.date | date:'HH:mm:ss dd-MM-yyyy' }}</div>
                                        </div>
                                         
                                    </li>
                                </ul>
                              </div> -->
                            </div>
                            <!--END TABS-->
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>

                  <script src="assets/js/static.js"></script>
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
                  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
                  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
                  <script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
                  <script src="assets/js/angular-translate.js"></script>
                  <script src="../resources/views/reports/customjs/moment.js"></script>
                  <script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
                  <script src="assets/js/vamoApp.js"></script>
                  <script src="assets/js/services.js"></script>
                  <script src="assets/js/fuelNotification.js?v=<?php echo Config::get('app.version');?>"></script>

                  <script>


                    $("#menu-toggle").click(function(e) {
                      e.preventDefault();
                      $("#wrapper").toggleClass("toggled");
                    });  
                    $("#menu-toggle").click(function(e) {
                      e.preventDefault();
                      $("#wrapper").toggleClass("toggled");
                    });

                  </script>

                </body>
                </html>
