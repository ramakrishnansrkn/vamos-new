<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title><?php echo Lang::get('content.gps'); ?></title>

  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
  <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
  <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
  <style>

    body{
     font-family: 'Lato', sans-serif;
     /*font-weight: bold;*/  
   /*font-family: 'Lato', sans-serif;
     font-family: 'Roboto', sans-serif;
     font-family: 'Open Sans', sans-serif;
     font-family: 'Raleway', sans-serif;
     font-family: 'Faustina', serif;
     font-family: 'PT Sans', sans-serif;
     font-family: 'Ubuntu', sans-serif;
     font-family: 'Droid Sans', sans-serif;
     font-family: 'Source Sans Pro', sans-serif;*/
   }

   .empty{
    height: 1px; width: 1px; padding-right: 30px; float: left;
  }
  .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
    background-color: #ffffff;
  }
</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2X3F316479');
</script>
</head>
<div id="preloader" >
  <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
  <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
  <div id="wrapper" ng-controller="mainCtrl" class="ng-cloak">
   <?php include('sidebarList.php');?>
   
   <div id="testLoad"></div>
   
   <div id="page-content-wrapper">
    <div class="container-fluid">
      <div class="panel panel-default">
       
      </div>   
    </div>
  </div>
  
  <!-- AdminLTE css box-->

  <div class="col-md-12">
   <div class="box box-primary">
    <!-- <div class="row"> -->
      <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
        <h3 class="box-title"><?php echo Lang::get('content.radio_frequency'); ?>
      </h3>
    </div>
    <div class="row">
      <div class="col-md-2" align="center">
        <div class="form-group" ng-if="shortNam!=undefined || shortNam!=null">
          <h5 style="color: grey;">{{shortNam}}</h5>
        </div>
        
      </div>
      <?php include('dateTime.php');?>
      <div class="col-md-1" align="center"></div>
      <div class="col-md-1" align="center">
        <button style="margin-left: -100%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
      </div>
      <div class="col-md-12" align="right" style="
      margin-bottom: 10px;
      ">
      <?php include('weekdays.php');?>
    </div>
  </div>

  <!--  </div> -->
</div>


</div>

<div class="col-md-12">
  <div class="box box-primary">
    <div>
      <div class="pull-right" style="margin-top: 1%">
        
        <img style="cursor: pointer;" ng-click="exportData('rfidreport')"  src="../resources/views/reports/image/xls.png" />
        <img style="cursor: pointer;" ng-click="exportDataCSV('rfidreport')"  src="../resources/views/reports/image/csv.jpeg" />
        <img style="cursor: pointer;" onclick="generatePDF()"  src="../resources/views/reports/image/Adobe.png" />

        <!-- <img style="cursor: pointer;" onClick ="$('#tableID').tableExport({type:'pdf',escape:'false'});"  src="assets/imgs/red.png" /> -->
        
      </div>
      <div class="col-md-12">
        <div class="box-body" id="rfidreport">
          <div class="empty" align="center"></div> <p style="margin:0;font-size:18px;" ><?php echo Lang::get('content.rfid_tag'); ?> <span style="float: right;font-size:15px;padding-right: 30px;"><b><?php echo Lang::get('content.from'); ?></b> :  {{(uiDate.fromtime)}}  {{uiDate.fromdate | date:'dd-MM-yyyy'}}    -    <b><?php echo Lang::get('content.to'); ?></b> :  {{(uiDate.totime)}}  {{uiDate.todate | date:'dd-MM-yyyy'}}</span></p> 
          
          <div class="row">
           <div class="col-md-1" align="center"></div>

           <div class="col-md-2" align="center">
             <div class="form-group">

             </div>
           </div>
         </div>   
         <div id="formConfirmation">
          <table class="table table-bordered table-striped table-condensed table-hover table-fixed-header" >
            <thead>
              <tr style="text-align:center; font-weight: bold; ">
                <td style="background-color:#ecf7fb;"><?php echo Lang::get('content.veh_name'); ?></td>
                <td style="background-color:#f9f9f9;">{{shortNam}}</td> 
                <td style="background-color:#ecf7fb;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.group'); ?></td>
                <td style="background-color:#f9f9f9;">{{uiGroup}}</td>
              </tr>

              <tr><td colspan="4"></td></tr>
            </thead>
            <thead class='header' style=" z-index: 1;">
              <tr style="text-align:center;font-weight: bold;" >
                <th class="id" custom-sort order="'fromTime'" sort="sort" style="text-align:center;background-color:#d2dff7;" width="25%"><?php echo Lang::get('content.fromdate'); ?></th>
                <th class="id" custom-sort order="'toTime'" sort="sort" style="text-align:center;background-color:#d2dff7;" width="25%"><?php echo Lang::get('content.todate'); ?></th>
                <!-- <th class="id" custom-sort order="'cumDistance'" sort="sort" style="text-align:center;" width="15%">cumulative</th> -->
                <!--   <th class="id" custom-sort order="'odoDistance'" sort="sort" style="text-align:center;" width="15%">Odo Meter</th> -->
                <th class="id" custom-sort order="'address'" sort="sort" style="text-align: center;background-color:#d2dff7;" width="40%"><?php echo Lang::get('content.Nearest_Loc'); ?></th>
                <th class="id" style="text-align: center;background-color:#d2dff7;" width="10%"><?php echo Lang::get('content.gmap'); ?></th>
              </tr>
            </thead>
            <tbody ng-repeat="rfidReport in siteData.gd | orderBy:sort.sortingOrder:sort.reverse">
              <tr class="active" style="text-align:center" ng-if="!errMsg.includes('expired')">
                <td>{{rfidReport.fromTime | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                <td>{{rfidReport.toTime | date:'HH:mm:ss dd-MM-yyyy'}}</td>
                                        <!-- <td>{{rfidReport.distanse}}</td>
                                        <td>{{rfidReport.cumDistance}}</td>
                                        <td>{{rfidReport.odoDistance}}</td> -->
                                        <td>{{rfidReport.address}} 
                                            <!-- <p ng-if="ignitionignitionignition.address!=null">{{ignition.address}}</p>
                                            <p ng-if="ignitionignition.address==null && addressFuel[$index]!=null">{{addressFuel[$index]}}</p>
                                            <p ng-if="ignition.address==null && addressFuel[$index]==null"><img src="assets/imgs/loader.gif" align="middle"></p> -->
                                          </td>
                                          <!-- <td>{{rfidReport.tagDet}}</td>  -->
                                          <td><a href="https://www.google.com/maps?q=loc:{{rfidReport.lat}},{{rfidReport.lng}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
                                        </tr>
                                        <tr ng-repeat="rfidtage in rfidReport.tagDet track by $index">
                                          <td colspan="2" style="text-align: center;">{{rfidSplit(rfidtage, 'one')}}</td>
                                          <td colspan="2" style="text-align: center;">{{rfidSplit(rfidtage, 'two')}}</td>
                                       <!--  <td>{{rfidtage.tagId}}</td>
                                        <td>{{rfidtage.tagName}}</td> -->
                                      </tr>
                                      
                                    </tbody>
                                    <tr ng-if="siteData.gd == null || siteData.vehicleId == null"  align="center">
                                      <td colspan="4" class="err" ng-if="!error&&!errMsg"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
                                      <td colspan="4"class="err" ng-if="error&&!errMsg"><h5>{{error}}</h5></td>
                                      <td colspan="4" class="err" ng-if="errMsg"><h5>{{errMsg}}</h5></td>
                                    </tr>
                                  </table>
                                </div>
                              </div>

                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <script src="assets/js/static.js"></script>
                    <script src="assets/js/jquery-1.11.0.js"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
                    <script src="assets/js/ui-bootstrap-0.6.0.min.js"></script>
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
                    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places" type="text/javascript"></script>
                    <script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
                    <script src="assets/js/angular-translate.js"></script>
                    <script src="../resources/views/reports/customjs/html5csv.js"></script>
                    <script src="../resources/views/reports/customjs/FileSaver.js"></script>
                    <script src="../resources/views/reports/customjs/moment.js"></script>
                    <script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
                    <script src="../resources/views/reports/datatable/jquery.dataTables.js"></script>
                    <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
                    <!--<script src="assets/js/naturalSortVersionDates.js"></script> -->
                    <script src="assets/js/vamoApp.js"></script>
                    <script src="assets/js/services.js"></script>
                    <script src="assets/js/siteReport.js?v=<?php echo Config::get('app.version');?>"></script>
                    <script>

        // $("#example1").dataTable();
        // $("#menu-toggle").click(function(e) {
        //     e.preventDefault();
        //     $("#wrapper").toggleClass("toggled");
        // });
        
        $("#menu-toggle").click(function(e) {
          e.preventDefault();
          $("#wrapper").toggleClass("toggled");
        });

        var generatePDF = function() {
          kendo.drawing.drawDOM($("#formConfirmation"),{paperSize: "A4",multiPage: true,margin: {
            left   : "4mm",
            top    : "10mm",
            right  : "4mm",
            bottom : "10mm"
          } ,landscape: true }).then(function(group) {
            kendo.drawing.pdf.saveAs(group, "RfidReport.pdf");
          });
        }

      </script>
      
    </body>
    </html>


