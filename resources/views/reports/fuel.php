<!DOCTYPE>
<html lang="en">
<head>
  <title><?php echo Lang::get('content.gps'); ?></title>
  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="../resources/views/reports/datepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css">
  <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
  
  <style>
  
  body{
     font-family: 'Lato', sans-serif;
   /*font-weight: bold; 
     font-family: 'Lato', sans-serif;
     font-family: 'Roboto', sans-serif;
     font-family: 'Open Sans', sans-serif;
     font-family: 'Raleway', sans-serif;
     font-family: 'Faustina', serif;
     font-family: 'PT Sans', sans-serif;
     font-family: 'Ubuntu', sans-serif;
     font-family: 'Droid Sans', sans-serif;
     font-family: 'Source Sans Pro', sans-serif;*/
  }

  .squaredFour {
    width: 20px;
    position: relative;
    /*margin: 20px auto;*/
  }
  .squaredFour label {
    width: 15px;
    height: 15px;
    cursor: pointer;
    position: absolute;
    top: 0;
    left: 0;
    background: #fcfff4;
    background: -webkit-linear-gradient(top, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
    background: linear-gradient(to bottom, #fcfff4 0%, #dfe5d7 40%, #b3bead 100%);
    border-radius: 4px;
    box-shadow: inset 0px 1px 1px white, 0px 1px 3px rgba(0, 0, 0, 0.5);
  }
  .squaredFour label:after {
    content: '';
    width: 12px;
    height: 5px;
    position: absolute;
    top: 4px;
    left: 2px;
    border: 3px solid #333;
    border-top: none;
    border-right: none;
    background: transparent;
    opacity: 0;
    -webkit-transform: rotate(-45deg);
    transform: rotate(-45deg);
  }
  .squaredFour label:hover::after {
    opacity: 0.5;
  }
  .squaredFour input[type=checkbox] {
    visibility: hidden;
  }
  .squaredFour input[type=checkbox]:checked + label:after {
    opacity: 1;
  }


  #plotin{height: 25px;z-index: 999; width:100px; background-color:transparent;  font-size:11px; border:1px; color: #080808}
  #plotin:hover{
    background-color:;z-index: 999;border:; color:; font-size:12px; -moz-transform: scale(1); -webkit-transform: scale(1); transform: scale(1);
  }
  /*#perloader{
    z-index: 1020;
    background: transparent url('assets/imgs/loader.gif') 50% 50% no-repeat;*/
  }
}
</style>
</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>
<body ng-app="mapApp">
  <div id="wrapper" ng-controller="mainFuel" class="ng-cloak">
  <?php include('sidebarList.php');?> 

  <div id="page-content-wrapper">
    <div class="container-fluid">
        <div class="panel panel-default"></div>       
      </div>
  </div>
  <div id="testLoad"></div>
  <div class="col-md-12">
         <div class="box box-primary">
          <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
            <h4 class="box-title"><?php echo Lang::get('content.fuel_analytics'); ?></h4>
          </div>
          <div class="row">
           <div class="col-md-2" align="center">
              <div class="form-group" ng-if="shortNam!=undefined || shortNam!=null">
                  <h5 style="color: grey;">{{shortNam}}</h5>
              </div>
                <div class="form-group" ng-if="shortNam==undefined || shortNam==null">
                  <h5 style="color: red;"><?php echo Lang::get('content.vehicle_not_found'); ?></h5>
                </div>
            </div>
           <div class="col-md-2" align="center">
             <div class="form-group">
               <div class="input-group datecomp">
                <input type="text" ng-model="fromdate" class="form-control placholdercolor" id="dateFrom" placeholder="From date">
                <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
              </div>
            </div>
          </div>
          <div class="col-md-2" align="center">
           <div class="form-group">
             <div class="input-group datecomp">
              <input type="text" ng-model="fromtime" class="form-control placholdercolor" id="timeFrom" placeholder="From time">
              <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
            </div>
          </div>
        </div>
        <div class="col-md-2" align="center">
         <div class="form-group">
           <div class="input-group datecomp">
             <input type="text" ng-model="todate" class="form-control placholdercolor" id="dateTo" placeholder="To date">
             <!-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div> -->
           </div>
         </div>
       </div>
       <div class="col-md-2" align="center">
         <div class="form-group">
           <div class="input-group datecomp">
             <input type="text" ng-model="totime" class="form-control placholdercolor" id="timeTo" placeholder="To time">
             <!-- <div class="input-group-addon"><i class="glyphicon glyphicon-time"></i></div> -->
           </div>
         </div>
       </div>
       <div class="col-md-1" align="center">
         <div class="form-group">
           <div class="input-group datecomp">
            <select class="input-sm form-control" id="fuelValue">
             <!-- <option value="0">Select</option> -->
             <option value="fill"><?php echo Lang::get('content.fuel_fill'); ?></option>
             <option value="drops"><?php echo Lang::get('content.fuel_theft'); ?></option>
           </select>
         </div>
       </div>
     </div>
  
      <div class="col-md-1" align="center">
        <div class="form-group">
          <button ng-click="plotHist()" style="margin-left: -30%; margin-top: 2px;"><?php echo Lang::get('content.submit'); ?></button>
        </div>
      </div>

  </div>

<div class="col-md-12" style="margin-top:20px;">
         <div class="box box-primary">
          <div id="page-content-wrapper" style="background-color:">
            <div class="container-fluid">
              <div class="panel panel-default">
                <div class="col-md-12">
                  <div class="box-body" id="eventReport" style="width: 100%; height: 100%; ">
                    <div style="display: flex; -webkit-flex; -ms-flex: 1; background-color: #f9f9f9; padding-left: 30px" class="box box-info">
                      <div class="squaredFour" style="width: 30%"><h5><?php echo Lang::get('content.group'); ?> <?php echo Lang::get('content.name'); ?> : {{groupName}}</h5></div>
                      <div style="float: left; width: 45%"><h5><?php echo Lang::get('content.veh_name'); ?> : {{shortName}}</h5></div> 
                    </div>

                    <div style="display: flex; -webkit-flex; -ms-flex: 1; background-color:#f9f9f9; padding-left: 30px" class="box box-info">
                      <div style="width: 95%"><h5><?php echo Lang::get('content.dis_vs_fuel'); ?></h5></div>
                      <div style="width: 5%; padding: 5px"><span id="minus"><img src="assets/imgs/minus.png"></span></div> 
                    </div>

                   <!--  <div class="box box-info">
                    <div style="padding: 5 px; width: 100%; border-radius: 3px" ><h5 style="float: left">Trip Vs Fuel Consume</h5>
                        <span id="minus"><img src="assets/imgs/minus.png"></span>
                      </div>
                    </div> -->
                    <div style="height: 310px;" id="chart1">
                      <div style="min-width:800px; max-width:800px; height: 300px;" id="container"></div>
                    </div>
                    <div style="display: flex; -webkit-flex; -ms-flex: 1; background-color: #f9f9f9; padding-left: 30px" class="box box-info">
                      <div style="width: 95%"><h5><?php echo Lang::get('content.dur_vs_fuel'); ?></h5></div>
                      <div style="width: 5%; padding: 5px"><span id="minus1"><img src="assets/imgs/minus.png"></span></div> 
                    </div>

                    <div style="height: 310px;" id="chart2">
                      <div style="min-width:800px; max-width:800px; height: 300px" id="container1"></div> 
                    </div>

                    <!-- </b></th> -->
                      <div style="width: 50%; float: left; padding-right: 2%; text-align: center" id="fuelCons">
                        <table class="table table-striped table-condensed table-hover" style="font-size: 14px">
                          <thead style="background-color: #f9f9f9; color: #323232">
                            <th colspan="3"><?php echo Lang::get('content.distance_fuel'); ?></th>
                            <th>
                              <div class="pull-right">
                                <img src="../resources/views/reports/image/xls.png" ng-click="exportData('fuelCons')"/>
                                <img src="../resources/views/reports/image/csv.png" ng-click="exportDataCSV('fuelCons')"/>
                              </div>
                            </th>
                            <tr>
                              <th colspan="2"><?php echo Lang::get('content.vehiclename'); ?></th>
                              <th colspan="2">{{shortName}}</th>
                            </tr>
                            <tr>
                             <th><?php echo Lang::get('content.fuel_distance'); ?></th>
                             <th colspan="2">
                              <div style="display: flex; -webkit-flex; -ms-flex: 1;">
                                <div style="width: 20%">
                                  <input type="checkbox" value="None" id="stop" name="check"/>
                                  <!-- <label for="stop"></label> -->
                                </div>
                                <div style="float: left; width: 45%"><?php echo Lang::get('content.distance'); ?></div> 
                              </div>
                            </th>
                            <th  colspan="1" style="text-align: center">
                              <select title="Suggested Stops" id="plotin" class="kms">
                                <option value="25">25 (kms)</option>
                                <option value="50">50 (kms)</option>
                                <option value="75">75 (kms)</option>
                                <option value="100">100 (kms)</option>
                              </select>
                              <!-- <input type="textbox" id="plotin" style="height: 20px; width:40px; border-radius: 3px;" value="25" class="kms" />(km)</th> -->
                          </tr>
                          <tr>
                            <th colspan="2"><?php echo Lang::get('content.date_time'); ?></th>
                            <th><?php echo Lang::get('content.distance'); ?><?php echo Lang::get('content.Km'); ?></th>
                            <th><?php echo Lang::get('content.consume'); ?><?php echo Lang::get('content.Ltr'); ?>)</th>
                          </tr>
                          </tr>
                        </thead>
                        <tbody>
                          <tr ng-repeat="rep in fuelTotal[0].distanceHistory">
                            <td colspan="2">{{rep.startTime | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                            <td>{{rep.tripDistance}}</td>
                            <td>{{rep.fuelConsume}}</td>
                          </tr>
                          <tr ng-if="fuelTotal[0].distanceHistory==null" align="center">
                            <td colspan="4" class="err"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <div style="width: 50%; float: left; text-align: center" id="fueltime">
                      <table class="table table-striped table-condensed table-hover" style="font-size: 14px">
                        <thead style="background-color: #f9f9f9; color: #323232">
                          <th colspan="2"><?php echo Lang::get('content.duration_fuel'); ?></th>
                            <th>
                              <div class="pull-right">
                                <img style="cursor: pointer;" src="../resources/views/reports/image/xls.png" ng-click="exportData('fueltime')"/>
                                <img width=30 height=30 style="cursor: pointer;" src="../resources/views/reports/image/csv.png" ng-click="exportDataCSV('fueltime')"/>
                              </div>
                            </th>
                            <tr>
                              <th colspan="2"><?php echo Lang::get('content.vehiclename'); ?></th>
                              <th colspan="2">{{shortName}}</th>
                            </tr>
                         <tr>
                            <th><?php echo Lang::get('content.fuel_time'); ?></th>
                            <th colspan="1" style="text-align: center">
                              <div style="display: flex; -webkit-flex; -ms-flex: 1;">
                                <div style="width: 15%;">
                                  <input type="checkbox" value="None" id="idlecheck" name="check"/>
                                  <!-- <label for="idle"></label> -->
                                </div>
                                <div style="float: left; width: 30%"><?php echo Lang::get('content.interval'); ?></div> 
                              </div>
                            </th>

                            <th style="text-align: center"><!-- <input type="textbox" id="plotin" style="height: 20px; width:40px; border-radius: 3px" value="2" class="hrs"/>(hrs) -->
                              <select title="Suggested Stops" id="plotin" class="hrs">
                                <option value="2">2 (hrs)</option>
                                <option value="3">3 (hrs)</option>
                                <option value="4">4 (hrs)</option>
                                <option value="5">5 (hrs)</option>
                              </select>
                            </th>
                          <tr>
                            <th><?php echo Lang::get('content.start_time'); ?></th>
                            <th><?php echo Lang::get('content.end_time'); ?></th>
                            <!-- <th>Duration</th> -->
                            <th><?php echo Lang::get('content.consume'); ?> <?php echo Lang::get('content.Ltr'); ?></th> 
                          </tr>
                        </thead>
                        <tbody>
                          <tr ng-repeat="val in fuelTotal[0].timeHistory">
                            <td>{{val.startTime | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                            <td>{{val.endTime | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                            <!-- <td>{{msToTime(val.duration)}}</td> -->
                            <td>{{val.fuelConsume}}</td> 
                          </tr>
                          <tr ng-if="fuelTotal[0].timeHistory==null" align="center">
                            <td colspan="3" class="err"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>


              <div id="fill">

                <div class="box-body" id="eventReport" style="width: 100%; height: 100%;">

                    <div style="display: flex; -webkit-flex; -ms-flex: 1; background-color: #f4f4f4; padding-left: 30px" class="box box-info" ng-show="hide">
                      <div class="squaredFour" style="width: 30%"><h5><?php echo Lang::get('content.group'); ?> <?php echo Lang::get('content.name'); ?> : {{groupName}}</h5></div>
                      <div style="float: left; width: 45%"><h5><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.id'); ?> : {{shortName}}</h5></div> 
                    </div>

                    <div style="width: 50%; float: left; padding-right: 2%; margin-top: 5px; text-align: center" ng-show="hide">

                        <table class="table table-striped table-condensed table-hover" style="font-size: 14px" >
                          <thead style="background-color: #1affc2; color: #323232">
                          <tr>
                              <th colspan="2"><?php echo Lang::get('content.vehiclename'); ?></th>
                              <th colspan="2">{{shortName}}</th>
                            </tr>
                            <tr>
                             <th><?php echo Lang::get('content.fuel_drop'); ?></th>
                             <th>
                              <div style="display: flex; -webkit-flex; -ms-flex: 1;">
                                <div  style="width: 20%">
                                  <input type="checkbox" value="None" id="fillfuel" name="check" checked/>
                                </div>
                                <div style="float: left; width: 60%"><?php echo Lang::get('content.drop'); ?></div> 
                              </div>
                            </th>
                            <th><?php echo Lang::get('content.drop_total'); ?> :</th>
                            <th>{{fuelTotal[0].totalFuel}} (ltr)</th>
                          </tr>
                          <tr>
                            <th><?php echo Lang::get('content.loc'); ?></th>
                            <th><?php echo Lang::get('content.trip'); ?><?php echo Lang::get('content.Km'); ?></th>
                            <th><?php echo Lang::get('content.start'); ?><?php echo Lang::get('content.Ltr'); ?></th>
                            <th><?php echo Lang::get('content.end'); ?><?php echo Lang::get('content.Ltr'); ?></th>
                          </tr>
                          </tr>
                        </thead>
                        <tbody>
                          <tr ng-repeat="rep in fuelTotal[0].fuelDrop">
                            <td>{{rep.latitude}},{{rep.longitude}}</td>
                            <td>{{rep.fuelConsume}}</td>
                            <td>{{rep.fuelFrom}}</td>
                            <td>{{rep.fuelTo}}</td>
                          </tr> 
                          <tr ng-if="fuelTotal[0].fuelDrop==null" align="center">
                            <td colspan="4" class="err"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
                          </tr>
                        </tbody>
                      </table>

                  </div>

                    <div style="width: 50%; float: left;  margin-top: 5px; text-align: center" id="fuelfill">
                      <table class="table table-striped table-condensed table-hover" style="font-size: 14px">
                        <thead style="background-color:#f6f7fa; color: #323232">
                          <th colspan="2"></th>
                          <th colspan="2"><?php echo Lang::get('content.fuel_fill_analytics'); ?></th>
                            <th colspan="2">
                              <div class="pull-right">
                                <img style="cursor: pointer;" src="../resources/views/reports/image/xls.png" ng-click="exportData('fuelfill')"/>
                                <img width=30 height=30 style="cursor: pointer;" src="../resources/views/reports/image/csv.png" ng-click="exportDataCSV('fuelfill')"/>
                              </div>
                            </th>
                            <tr>
                              <th colspan="1"></th>
                              <th colspan="2"><?php echo Lang::get('content.vehiclename'); ?></th>
                              <th colspan="3">{{shortName}}</th>
                            </tr>
                         <tr>
                            
                            <th><?php echo Lang::get('content.fuel_fill'); ?></th>
                            <th>
                              <div style="display: flex;">
                                <div style="width: 50%">
                                  <input type="checkbox" value="None" id="drop" name="check" checked/>
                                  
                                </div>
                               <!--  <div style="float: left; width: 45%">Fill</div>  -->
                              </div>
                            </th>
                            <th colspan="1"></th>
                            <th colspan="2"><?php echo Lang::get('content.cumulative_fuel'); ?> :</th>
                            <th>{{fuelTotal[1].totalFuel}} (ltr)</th>
                          <tr>
                            <th><?php echo Lang::get('content.start_time'); ?></th>
                            <th><?php echo Lang::get('content.end_time'); ?></th>
                            <th><?php echo Lang::get('content.glink'); ?></th>
                            <th><?php echo Lang::get('content.filling'); ?> <?php echo Lang::get('content.Ltr'); ?></th>
                            <th><?php echo Lang::get('content.begining'); ?> <?php echo Lang::get('content.Ltr'); ?></th>
                            <th><?php echo Lang::get('content.ending'); ?> <?php echo Lang::get('content.Ltr'); ?></th> 
                          </tr>
                        </thead>
                        <tbody>
                          <tr ng-repeat="rep in fuelTotal[1].fuelFill">
                            <td>{{rep.startTime | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                            <td>{{rep.endTime | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                            <td><a href="https://www.google.com/maps?q=loc:{{rep.latitude}},{{rep.longitude}}" target="_blank">Link</td>
                            <td>{{rep.fuelConsume}}</td>
                            <td>{{rep.fuelFrom}}</td>
                            <td>{{rep.fuelTo}}</td>
                          </tr>
                          <tr ng-if="fuelTotal[1].fuelFill==null" align="center">
                            <td colspan="6" class="err"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
                          </tr>
                        </tbody>
                      </table>
                    </div>

                </div>

              </div>




               <div id="theft">

                <div class="box-body" id="theftReport" style="width: 100%; height: 100%;">

                    <div style="display: flex; -webkit-flex; -ms-flex: 1; background-color: #f4f4f4; padding-left: 30px" class="box box-info" ng-show="hide">
                      <div class="squaredFour" style="width: 30%"><h5><?php echo Lang::get('content.group'); ?> <?php echo Lang::get('content.name'); ?> : {{groupName}}</h5></div>
                      <div style="float: left; width: 45%"><h5><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.id'); ?> : {{shortName}}</h5></div> 
                    </div>

                    <div style="width: 50%; float: left; padding-right: 2%; margin-top: 5px; text-align: center" ng-show="hide">

                        <table class="table table-striped table-condensed table-hover" style="font-size: 14px" >
                          <thead style="background-color: #1affc2; color: #323232">
                          <tr>
                              <th colspan="2"><?php echo Lang::get('content.vehiclename'); ?></th>
                              <th colspan="2">{{shortName}}</th>
                            </tr>
                            <tr>
                             <th><?php echo Lang::get('content.fuel_drop'); ?></th>
                             <th>
                              <div style="display: flex; -webkit-flex; -ms-flex: 1;">
                                <div  style="width: 20%">
                                  <input type="checkbox" value="None" id="fillfuel" name="check" checked/>
                                </div>
                                <div style="float: left; width: 60%"><?php echo Lang::get('content.drop'); ?></div> 
                              </div>
                            </th>
                            <th><?php echo Lang::get('content.drop_total'); ?> :</th>
                            <th>{{fuelTotal[0].totalFuel}} (ltr)</th>
                          </tr>
                          <tr>
                            <th><?php echo Lang::get('content.loc'); ?></th>
                            <th><?php echo Lang::get('content.trip'); ?><?php echo Lang::get('content.Km'); ?></th>
                            <th><?php echo Lang::get('content.start'); ?><?php echo Lang::get('content.Ltr'); ?></th>
                            <th><?php echo Lang::get('content.end'); ?><?php echo Lang::get('content.Ltr'); ?></th>
                          </tr>
                          </tr>
                        </thead>
                        <tbody>
                          <tr ng-repeat="rep in fuelTotal[0].fuelDrop">
                            <td>{{rep.latitude}},{{rep.longitude}}</td>
                            <td>{{rep.fuelConsume}}</td>
                            <td>{{rep.fuelFrom}}</td>
                            <td>{{rep.fuelTo}}</td>
                          </tr> 
                          <tr ng-if="fuelTotal[0].fuelDrop==null" align="center">
                            <td colspan="4" class="err"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
                          </tr>
                        </tbody>
                      </table>

                  </div>

                  <div style="width: 50%; float: left;  margin-top: 5px; text-align: center" id="fueltheft">
                      <table class="table table-striped table-condensed table-hover" style="font-size: 14px">
                        <thead style="background-color:#f6f7fa; color: #323232">
                          <th colspan="2"></th>
                          <th colspan="2"><?php echo Lang::get('content.fuel_fill_analytics'); ?></th>
                            <th colspan="2">
                              <div class="pull-right">
                                <img style="cursor: pointer;" src="../resources/views/reports/image/xls.png" ng-click="exportData('fueltheft')"/>
                                <img width=30 height=30 style="cursor: pointer;" src="../resources/views/reports/image/csv.png" ng-click="exportDataCSV('fueltheft')"/>
                              </div>
                            </th>
                            <tr>
                              <th colspan="1"></th>
                              <th colspan="2"><?php echo Lang::get('content.vehiclename'); ?></th>
                              <th colspan="3">{{shortName}}</th>
                            </tr>
                         <tr>
                            <th><?php echo Lang::get('content.fuel_theft'); ?></th>
                            <th>
                              <div style="display: flex;">
                                <div style="width: 50%">
                                  <input type="checkbox" value="None" id="drop" name="check" checked/>
                                  
                                </div>
                               <!--  <div style="float: left; width: 45%">Fill</div>  -->
                              </div>
                            </th>
                            <th colspan="1"></th>
                            <th colspan="2"><?php echo Lang::get('content.cumulative_fuel'); ?> :</th>
                            <th>{{fuelTheft.totalTheftFuel}} (ltr)</th>
                          <tr>
                            <th><?php echo Lang::get('content.start_time'); ?></th>
                            <th><?php echo Lang::get('content.end_time'); ?></th>
                            <th><?php echo Lang::get('content.glink'); ?></th>
                            <th><?php echo Lang::get('content.filling'); ?> <?php echo Lang::get('content.Ltr'); ?></th>
                            <th><?php echo Lang::get('content.begining'); ?> <?php echo Lang::get('content.Ltr'); ?></th>
                            <th><?php echo Lang::get('content.ending'); ?> <?php echo Lang::get('content.Ltr'); ?></th> 
                          </tr>
                        </thead>
                        <tbody>
                          <tr ng-repeat="rep in fuelTheft.getData">
                            <td>{{rep.startDate | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                            <td>{{rep.date | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                            <td><a href="https://www.google.com/maps?q=loc:{{rep.latitude}},{{rep.longitude}}" target="_blank">Link</td>
                            <td>{{rep.theftLitr}}</td>
                            <td>{{rep.prevLitr}}</td>
                            <td>{{rep.currLitr}}</td>
                          </tr>
                          <tr ng-if="fuelTheft.getData==null" align="center">
                            <td colspan="6" class="err"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
                          </tr>
                        </tbody>
                      </table>
                  </div>

                </div>

              </div>


                  </div>
                  </div>
                </div>
              </div>
            </div>      
          </div>
        </div>
      </div>
    </div>

    <script src="assets/js/static.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
    <script src="assets/js/ui-bootstrap-0.6.0.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="assets/js/angular-translate.js"></script>
    <script src="../resources/views/reports/customjs/FileSaver.js"></script>
    <script src="../resources/views/reports/customjs/html5csv.js"></script>
    <script src="assets/js/highcharts.js"></script>
    <script src="../resources/views/reports/customjs/moment.js"></script>
    <script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
<!--<script src="assets/js/naturalSortVersionDates.js"></script> -->
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/fuelreport.js"></script>
    
    <script>

    function googleTranslateElementInit() {
       new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
    } 
     
    $(function () {
        $('#dateFrom, #dateTo').datetimepicker({
          format:'YYYY-MM-DD',
          useCurrent:true,
          pickTime: false,
          maxDate: new Date,
          minDate: new Date(2015, 12, 1)
        });
        
        $('#timeFrom').datetimepicker({
          pickDate: false
        });
        
        $('#timeTo').datetimepicker({
          useCurrent:true,
          pickDate: false
        });
    });

    </script>

  </body>
  </html>


