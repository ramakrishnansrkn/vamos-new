<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title><?php echo Lang::get('content.gps'); ?></title>
  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
  <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
  <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
  <style>
    <style>
    body{
      font-family: 'Lato', sans-serif;
      /*font-weight: bold;*/  
/* font-family: 'Lato', sans-serif;
font-family: 'Roboto', sans-serif;
font-family: 'Open Sans', sans-serif;
font-family: 'Raleway', sans-serif;
font-family: 'Faustina', serif;
font-family: 'PT Sans', sans-serif;
font-family: 'Ubuntu', sans-serif;
font-family: 'Droid Sans', sans-serif;
font-family: 'Source Sans Pro', sans-serif;
*/
}
.empty{
  height: 1px; width: 1px; padding-right: 30px; float: left;
}
.table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
 background-color: #ffffff;
}
</style>
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2X3F316479');
</script>
</head>
<div id="preloader" >
  <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
  <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
  <div id="wrapper" ng-controller="mainCtrl" class="ng-cloak">
    <?php include('sidebarList.php');?> 
    
    <div id="testLoad"></div>
    
    <div id="page-content-wrapper">
      <div class="container-fluid">
        <div class="panel panel-default">
         
        </div>   
      </div>
    </div>
    
    <!-- AdminLTE css box-->

    <div class="col-md-12">
     <div class="box box-primary">
      <!-- <div class="row"> -->
        <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
          <h3 class="box-title"><?php echo Lang::get('content.route_deviation_report'); ?>
        </h3>
      </div>
      <div class="row">
        <div class="col-md-2" align="center">
          <div class="form-group" ng-if="shortNam!=undefined || shortNam!=null">
            <h5 style="color: grey;">{{shortNam}}</h5>
          </div>
          
        </div>
        <?php include('dateTime.php');?>
        <div class="col-md-1" align="center">
          <button style="margin-left: 0%; padding : 5px" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
        </div>
        <div class="col-md-9" align="right" style="margin-bottom: 10px;padding-left: 5%;">
         <?php include('DateButtonWithWeekMonth.php');?>
       </div>
       <div class="col-md-3" style="padding-bottom: 10px;">
         <?php include('helpVideos.php');?>
       </div>
     </div>

     <!--  </div> -->
   </div>
   
 </div>

 <div class="col-md-12">
  <div class="box box-primary">
    <div>
      <div class="pull-right" style="margin-top: 1%;">
        
        <img style="cursor: pointer;" ng-click="exportData('routeDeviation')"  src="../resources/views/reports/image/xls.png" />
        <img style="cursor: pointer;" ng-click="exportDataCSV('routeDeviation')"  src="../resources/views/reports/image/csv.jpeg" />
        <img style="cursor: pointer;" onclick="generatePDF()"  src="../resources/views/reports/image/Adobe.png" />
      </div>
      <div class="box-body" id="routeDeviation">
        <div class="empty" align="center"></div><!--  <p style="margin:0;font-size:18px;"><?php echo Lang::get('content.trip_time_report'); ?> <span style="float: right;font-size:15px;padding-right: 30px;"><b><?php echo Lang::get('content.from'); ?></b> : &nbsp;{{uiDate.fromdate}} &nbsp;{{convert_to_24hrs(uiDate.fromtime)}} &nbsp;&nbsp; - &nbsp;&nbsp; <b><?php echo Lang::get('content.to'); ?></b> :&nbsp; {{uiDate.todate}} &nbsp;{{convert_to_24hrs(uiDate.totime)}}</span> </p>  -->
        
        <div class="row">
         <div class="col-md-1" align="center"></div>

         <div class="col-md-2" align="center">
           <div class="form-group">

           </div>
         </div>
       </div> 
       <div id="formConfirmation">
        <table class="table table-bordered table-striped table-condensed table-hover table-fixed-header" style="    margin-top: 10px;">
          <thead>
            <tr style="text-align:center; font-weight: bold;">
              <td style="background-color:#ecf7fb;">{{ vehiLabel | translate }} <?php echo Lang::get('content.group'); ?></td>
              <td  colspan="1" style="background-color:#f9f9f9;">{{uiGroup}}</td>
              <td style="background-color:#ecf7fb;">{{ vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></td>
              <td style="background-color:#f9f9f9;">{{shortNam}}</td>   
            </tr>
            <tr><td colspan="4"></td></tr>
          </thead>
          <thead class='header' style=" z-index: 1;">
            <tr style="text-align:center;font-weight: bold;">
              <th class="id" custom-sort order="'deviceTime'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.date_time'); ?></th>
              <th class="id" custom-sort order="'routeName'" sort="sort" style="text-align:center;background-color:#d2dff7;" ><?php echo Lang::get('content.route_name'); ?></th>
              <th class="id" custom-sort order="'distance'" sort="sort" style="text-align:center;background-color:#d2dff7;" ><?php echo Lang::get('content.distance'); ?></th>
              <!-- <th class="id" custom-sort order="'routeDeviation'" sort="sort" style="text-align:center;background-color:#d2dff7;" width="15%"><?php echo Lang::get('content.route_deviation'); ?></th> -->
              
              <th class="id" style="text-align: center;background-color:#d2dff7;">G-Map</th>
            </tr>
          </thead>
          <tbody>
           <tr class="active" style="text-align:center" ng-repeat="routeDev in routeDevData | orderBy:sort.sortingOrder:sort.reverse"  ng-if="(routeDevData[0].error == '-')&&!errMsg.includes('expired')">
            <td>{{routeDev.deviceTime | date:'yyyy-MM-dd HH:mm:ss'}}</td>
            <td>{{routeDev.routeName}}</td>
            <td>{{roundOff(routeDev.distance)}}</td>
            <!-- <td>{{routeDev.routeDeviation}}</td> -->
            <td><a href="https://www.google.com/maps?q=loc:{{routeDev.latitude}},{{routeDev.longitude}}" target="_blank">Link</a></td>
          </tr>
          <tr  align="center">
            <td colspan="4"class="err" ng-if="(routeDevData[0].error != '-')&&!errMsg"><h5>{{routeDevData[0].error}}</h5></td>
            <td colspan="4" class="err" ng-if="errMsg"><h5>{{errMsg}}</h5></td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>

</div>
</div>
</div>
</div>

<script src="assets/js/static.js"></script>
<script src="assets/js/jquery-1.11.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
<script src="assets/js/ui-bootstrap-0.6.0.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places" type="text/javascript"></script>
<script src="assets/js/angular-translate.js"></script>
<script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
<script src="../resources/views/reports/customjs/html5csv.js"></script>
<script src="../resources/views/reports/customjs/FileSaver.js"></script>
<script src="../resources/views/reports/customjs/moment.js"></script>
<script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
<script src="../resources/views/reports/datatable/jquery.dataTables.js"></script>
<script src="assets/js/naturalSortVersionDatesCaching.js"></script> 
<!-- <script src="assets/js/naturalSortVersionDates.js"></script> -->
<script src="assets/js/vamoApp.js"></script>
<script src="assets/js/services.js"></script>
<script src="assets/js/routeDeviation.js?v=<?php echo Config::get('app.version');?>"></script>
<script>

        // $("#example1").dataTable();
         // $("#menu-toggle").click(function(e) {
         //    e.preventDefault();
         //    $("#wrapper").toggleClass("toggled");
         // });
         
         $("#menu-toggle").click(function(e) {
          e.preventDefault();
          $("#wrapper").toggleClass("toggled");
        });

         var generatePDF = function() {
          kendo.drawing.drawDOM($("#formConfirmation")).then(function(group) {
            kendo.drawing.pdf.saveAs(group, "RouteDeviationReport.pdf");
          });
        }

      </script>
      
    </body>
    </html>
