<!DOCTYPE html>
<html lang="en" style="overflow: hidden !important;">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title><?php echo Lang::get('content.gps'); ?></title>

  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
  <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
  <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
  <link href="assets/css/web.css" rel="stylesheet" /> <!-- added for table freezing -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="assets/js/gridviewscroll.js"></script> <!-- added for table freezing -->
  <!-- pdfgen -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
  <script src="https://unpkg.com/jspdf-autotable@2.3.2/dist/jspdf.plugin.autotable.js"></script>

  <!--  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script> -->

  <style>
    body {
      font-family: 'Lato', sans-serif;
      /* font-weight: bold; */  
   /* font-family: 'Lato', sans-serif;
      font-family: 'Roboto', sans-serif;
      font-family: 'Open Sans', sans-serif;
      font-family: 'Raleway', sans-serif;
      font-family: 'Faustina', serif;
      font-family: 'PT Sans', sans-serif;
      font-family: 'Ubuntu', sans-serif;
      font-family: 'Droid Sans', sans-serif;
      font-family: 'Source Sans Pro', sans-serif;
      */
    }
    .empty{
      height: 1px; width: 1px; padding-right: 30px; float: left;
    }
    .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
      background-color: #ffffff;
    }

  </style>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-2X3F316479');
  </script>
</head>
<div id="preloader" >
  <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
  <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
  <div ng-controller="mainCtrl" class="ng-cloak">
    <div id="wrapper">
      <?php include('sidebarList.php');?> 
      
      <div id="testLoad"></div>
      
      <div id="page-content-wrapper">
        <div class="container-fluid">
          <div class="panel panel-default">

          </div>   
        </div>
      </div>
      
      <div class="col-md-12">
       <div class="box box-primary" style="padding-top: 5px;margin-top: -10px;">
        <!-- <div class="row"> -->
          <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
            <h3 class="box-title"><span><?php echo Lang::get('content.distance_fuel'); ?></span>
              <span><?php include('OutOfOrderDataInfo.php');?></span> 
            </h3>
          </div>
          
          <div class="row" style="margin-top: 20px;">
            <div class="col-md-2" align="center"></div>
            
            <div class="col-md-2 form-group" style="right:0px;"><?php echo Lang::get('content.month_year'); ?>:</div>
            <div class="col-md-3" align="center">
              <div class="input-group datecomp" style="padding-bottom: 20px;">
                <input type="text" ng-model="fromMonthFuel" class="form-control placholdercolor" id="monthFroms" placeholder="<?php echo Lang::get('content.month'); ?>">
              </div>
            </div>
            <div class="col-md-1" align="center">
             <div class="form-group">
               <button ng-click="submitMonFuel()" onclick="undo()" style="margin-left: -100%; margin-top: 2px;"><?php echo Lang::get('content.submit'); ?></button>
             </div>
           </div>
           
         </div>
         <div class="row" style="text-align: right;padding-right: 10px;">
          <span style="background-color: red;
          color: white;
          padding: 2px 15px;"></span>
          <span style="padding-left: 5px;margin-right: 10px;font-weight: bold;">No Data Vehicles</span>
        </div>


        <!--  </div> -->
        <div class="row">
          <div class="col-md-1" align="center"></div>

          <div class="col-md-2" align="center">
            <div class="form-group">

            </div>
          </div>
        </div>

      </div>
    </div>

    <div class="col-md-12">
      <div class="box box-primary" style="min-height:570px;">

        <div>
          <div class="pull-right" style="margin-top: 10px;margin-right: 5px;">

            <img style="cursor: pointer;" ng-click="exportData('monthDistFuel1')" onclick="undo()"  src="../resources/views/reports/image/xls.png" />
            <img width=30 height=30 style="cursor: pointer;" onclick="undo()" ng-click="exportDataCSV('monthDistFuel1')" src="../resources/views/reports/image/csv.jpeg" />
            <img style="cursor: pointer;"  ng-click="exportPDF(downloadid)"   src="../resources/views/reports/image/Adobe.png" />

          </div>

          
          
          <div class="box-body" id="DayWise_Truck_Positions">
            <div class="empty" align="center"></div>
            <div class="row" style="padding-top: 20px;"></div>
            <h5 ng-show="monthFuelData.error" class="err text-center" style="padding-top: 30px;">{{monthFuelData.error}}</h5>
            <div id="monthDistFuel" ng-hide="monthFuelData.error">
              <table class="table table-bordered table-striped table-condensed table-hover" style="width:  -webkit-fill-available;margin-top: 10px;">
                <thead>
                  <tr style="text-align:center;">
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.group'); ?></th>
                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{group}}</th>
                    <th style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.month'); ?> & <?php echo Lang::get('content.year'); ?> </th>
                    <th colspan="3" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"> {{monValFront+curYearFront}} </th>         
                  </tr>
                </thead>
              </table>   

              <table cellspacing="0" id="monthlydistfuel" style="width: 100%; border-collapse: collapse;" >

                <tr class="GridViewScrollHeader" style="padding-bottom:20px;" ng-if="monthFuelData.cumulativeVehicleDistanceData!=null || monthFuelData.cumulativeVehicleFuelData!=null || monthFuelData.cumulativeVehicleKmplData!=null">

                 <th style="background-color:#f9f9f9;" > {{ vehiLabel | translate }} <?php echo Lang::get('content.name'); ?> </th>
                 <th style="background-color:#f9f9f9;"> <?php echo Lang::get('content.day'); ?> </th>
                 <th ng-repeat="dat in monthFuelDates" style="background-color:#ecf7fb;font-weight: bold;">{{dat}}</th>
                 <th style="background-color:#f9f9f9;"> <?php echo Lang::get('content.total'); ?> </th>
               </tr>
               
               <tr class="GridViewScrollItem" ng-repeat-start="values in distMonFuelData track by $index">
                 <td  ng-if="$odd" style="background-color:#f9f9f9;font-weight: bold;padding-top: 15px;border-bottom-width: 0px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{values.vehiName}}</td>
                 <td  ng-if="$even" style="background-color:#ecf7fb;font-weight: bold;padding-top: 15px;border-bottom-width: 0px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{values.vehiName}}</td>         


                 <td ng-style="{ 'color':values.position=='U'?'red':'black' }"><?php echo Lang::get('content.distance'); ?> (<?php echo Lang::get('content.kms'); ?>)</td>
                 <td ng-repeat="val in values.distsTodays track by $index" style="color:#557fa8;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{ (values.vehicleMode == 'DG')? "-" :val.distanceToday}}</td>
                 <td style="color:#557fa8;font-weight: bold; " ng-style="{ 'color':values.position=='U'?'red':'black' }">{{ (values.vehicleMode == 'DG')? "-" :values.totDist}}</td>
               </tr>
               <tr class="GridViewScrollItem"> 
                 <td  ng-if="$odd" style="background-color:#f9f9f9;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }"> </td>
                 <td  ng-if="$even" style="background-color:#ecf7fb;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }"> </td>

                 <td ng-style="{ 'color':values.position=='U'?'red':'black' }"><?php echo Lang::get('content.fuel_fill'); ?> <?php echo Lang::get('content.ltrs'); ?></td>
                 <td ng-repeat="val in values.fuelFillTodays track by $index" style="color:#e18e32;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{val.fuelFillToday}}</td>
                 <td style="color:#e18e32;font-weight: bold;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{roundOffDecimal(values.totFuelFill)}}</td>
               </tr>
              <!--edit--> <tr class="GridViewScrollItem" ng-if="values.vehicleMode =='Moving Vehicle' || values.vehicleMode=='Dispenser'"> 
                 <td  ng-if="$odd" style="background-color:#f9f9f9;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }"> </td>
                 <td  ng-if="$even" style="background-color:#ecf7fb;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }"> </td>

                 <td  ng-style="{ 'color':values.position=='U'?'red':'black' }"><?php echo Lang::get('content.kmpl'); ?></td>
                 <td   ng-repeat="val in values.kmplTodays track by $index" style="color:#e18e32;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{val.kmplsToday}}</td>
                 <td   style="color:#e18e32;font-weight: bold;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{roundOffDecimal(values.totKmpl)}}</td>
                </tr>
               <tr class="GridViewScrollItem" ng-if="values.vehicleMode =='DG' || values.vehicleMode=='Machinery'"> 
                 <td  ng-if="$odd" style="background-color:#f9f9f9;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }"> </td>
                 <td  ng-if="$even" style="background-color:#ecf7fb;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }"> </td>

                 <td   ng-style="{ 'color':values.position=='U'?'red':'black' }"><?php echo Lang::get('content.ltrs_per_hrs'); ?></td>
                 <td   ng-repeat="val in values.lphTodays track by $index" style="color:#e18e32;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{val.lphToday}}</td>
                 <td   style="color:#e18e32;font-weight: bold;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{roundOffDecimal(values.totLitperHr)}}</td>
                </tr>
               <tr ng-repeat-end class="GridViewScrollItem"> 
                 <td  ng-if="$odd" style="background-color:#f9f9f9;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }"> </td>
                 <td  ng-if="$even" style="background-color:#ecf7fb;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }"> </td>

                 <td ng-style="{ 'color':values.position=='U'?'red':'black' }"><?php echo Lang::get('content.fuel_cons'); ?> <?php echo Lang::get('content.ltrs'); ?></td>
                 <td ng-repeat="val in values.fuelTodays track by $index" style="color:#e18e32;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{val.fuelsToday}}</td>
                 <td style="color:#e18e32;font-weight: bold;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{roundOffDecimal(values.totFuel)}}</td>
               </tr>
               <tr ng-repeat="n in [].constructor(1) track by $index" ng-if="monthFuelData.cumulativeVehicleDistanceData!=null" class="GridViewScrollItem">
                <td colspan="2" style="background-color:#ecf7fb;padding-top: 15px;"><?php echo Lang::get('content.total_distance'); ?> (<?php echo Lang::get('content.kms'); ?>)</td>
                
                <td ng-repeat="val in totMonDistVehic track by $index" style="color:#557fa8;font-weight: bold;">{{roundOffDecimal(val)}}</td>
              </tr>
              <tr ng-if="monthFuelData.cumulativeVehicleFuelFillData!=null" class="GridViewScrollItem">  
                <td colspan="2" style="background-color:#ecf7fb;padding-top: 15px;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_fill'); ?> <?php echo Lang::get('content.ltrs'); ?></td>
                <td ng-repeat="val in totMonFuelFill track by $index" style="color:#e18e32;font-weight: bold;">{{val}}</td>
              </tr>
              <tr ng-if="monthFuelData.cumulativeVehicleLphData!=null" class="GridViewScrollItem">  
                <td colspan="2" style="background-color:#ecf7fb;padding-top: 15px;"> <?php echo Lang::get('content.ltrs_per_hrs'); ?></td>
                <td ng-repeat="val in totMonLph track by $index" style="color:#557fa8;font-weight: bold;">{{val}}</td>
              </tr>
              <tr ng-if="monthFuelData.cumulativeVehicleKmplData!=null" class="GridViewScrollItem">  
                <td colspan="2" style="background-color:#ecf7fb;padding-top: 15px;"><?php echo Lang::get('content.kmpl'); ?></td>
                <td ng-repeat="val in totMonKmplVehic track by $index" style="color:#e18e32;font-weight: bold;">{{val}}</td>
              </tr>
              <tr ng-repeat-end ng-if="monthFuelData.cumulativeVehicleFuelData!=null" class="GridViewScrollItem">  
                <td colspan="2" style="background-color:#ecf7fb;padding-top: 15px;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_cons'); ?> <?php echo Lang::get('content.ltrs'); ?></td>
                <td ng-repeat="val in totMonFuelVehic track by $index" style="color:#557fa8;font-weight: bold;">{{val}}</td>
              </tr>
              <tr ng-if="monthFuelData.cumulativeVehicleDistanceData==null || monthFuelData.cumulativeVehicleFuelData==null || monthFuelData.cumulativeVehicleKmplData==null || monthFuelData.cumulativeVehicleFuelFillData==null || monthFuelData.cumulativeVehicleLphData==null" align="center" class="GridViewScrollItem"> 
                <td colspan="36" style="border-left: 1px solid #e6e6e6;"><h5 class="err"><?php echo Lang::get('content.no_data_found'); ?></h5></td>
              </tr>
            </table>
            <div id="monthDistFuel1" ng-hide="monthFuelData.error" ng-show="false">
              <table class="table table-bordered table-striped table-condensed table-hover" style="width:  -webkit-fill-available;margin-top: 10px;" ng-show="false">
                <thead>
                  <tr style="text-align:center;">
                    <th colspan="2" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.group'); ?></th>
                    <th colspan="2" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;">{{group}}</th>
                    <th style="font-weight:unset;font-size:12px;background-color:#ecf7fb;"><?php echo Lang::get('content.month'); ?> & <?php echo Lang::get('content.year'); ?> </th>
                    <th colspan="3" style="background-color:#f9f9f9;font-weight: unset;font-size:12px;"> {{monValFront+curYearFront}} </th>         
                  </tr>
                </thead>
              </table>  
            <table cellspacing="0" id="monthlydistfuel1" style="width: 100%; border-collapse: collapse;" ng-show="false">

<tr class="GridViewScrollHeader" style="padding-bottom:20px;" ng-if="monthFuelData.cumulativeVehicleDistanceData!=null || monthFuelData.cumulativeVehicleFuelData!=null || monthFuelData.cumulativeVehicleKmplData!=null">

 <th style="background-color:#f9f9f9;" > {{ vehiLabel | translate }} <?php echo Lang::get('content.name'); ?> </th>
 <th style="background-color:#f9f9f9;"> <?php echo Lang::get('content.day'); ?> </th>
 <th ng-repeat="dat in monthFuelDates" style="background-color:#ecf7fb;font-weight: bold;">{{dat}}</th>
 <th style="background-color:#f9f9f9;"> <?php echo Lang::get('content.total'); ?> </th>
</tr>

<tr class="GridViewScrollItem" ng-repeat-start="values in distMonFuelData track by $index">
 <td  ng-if="$odd" style="background-color:#f9f9f9;font-weight: bold;padding-top: 15px;border-bottom-width: 0px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{values.vehiName}}</td>
 <td  ng-if="$even" style="background-color:#ecf7fb;font-weight: bold;padding-top: 15px;border-bottom-width: 0px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{values.vehiName}}</td>         


 <td ng-style="{ 'color':values.position=='U'?'red':'black' }"><?php echo Lang::get('content.distance'); ?> (<?php echo Lang::get('content.kms'); ?>)</td>
 <td ng-repeat="val in values.distsTodays track by $index" style="color:#557fa8;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{ (values.vehicleMode == 'DG')? "-" :val.distanceToday}}</td>
 <td style="color:#557fa8;font-weight: bold; " ng-style="{ 'color':values.position=='U'?'red':'black' }">{{ (values.vehicleMode == 'DG')? "-" :values.totDist}}</td>
</tr>
<tr class="GridViewScrollItem"> 
 <td  ng-if="$odd" style="background-color:#f9f9f9;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }"> {{values.vehiName}}</td>
 <td  ng-if="$even" style="background-color:#ecf7fb;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }"> {{values.vehiName}}</td>

 <td ng-style="{ 'color':values.position=='U'?'red':'black' }"><?php echo Lang::get('content.fuel_fill'); ?> <?php echo Lang::get('content.ltrs'); ?></td>
 <td ng-repeat="val in values.fuelFillTodays track by $index" style="color:#e18e32;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{val.fuelFillToday}}</td>
 <td style="color:#e18e32;font-weight: bold;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{roundOffDecimal(values.totFuelFill)}}</td>
</tr>
<!--edit--> <tr class="GridViewScrollItem" ng-if="values.vehicleMode =='Moving Vehicle' || values.vehicleMode=='Dispenser'"> 
 <td  ng-if="$odd" style="background-color:#f9f9f9;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{values.vehiName}} </td>
 <td  ng-if="$even" style="background-color:#ecf7fb;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{values.vehiName}} </td>

 <td  ng-style="{ 'color':values.position=='U'?'red':'black' }"><?php echo Lang::get('content.kmpl'); ?></td>
 <td   ng-repeat="val in values.kmplTodays track by $index" style="color:#e18e32;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{val.kmplsToday}}</td>
 <td   style="color:#e18e32;font-weight: bold;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{roundOffDecimal(values.totKmpl)}}</td>
</tr>
<tr class="GridViewScrollItem" ng-if="values.vehicleMode =='DG' || values.vehicleMode=='Machinery'"> 
 <td  ng-if="$odd" style="background-color:#f9f9f9;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{values.vehiName}}</td>
 <td  ng-if="$even" style="background-color:#ecf7fb;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{values.vehiName}}</td>

 <td   ng-style="{ 'color':values.position=='U'?'red':'black' }"><?php echo Lang::get('content.ltrs_per_hrs'); ?></td>
 <td   ng-repeat="val in values.lphTodays track by $index" style="color:#e18e32;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{val.lphToday}}</td>
 <td   style="color:#e18e32;font-weight: bold;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{roundOffDecimal(values.totLitperHr)}}</td>
</tr>
<tr ng-repeat-end class="GridViewScrollItem"> 
 <td  ng-if="$odd" style="background-color:#f9f9f9;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{values.vehiName}}</td>
 <td  ng-if="$even" style="background-color:#ecf7fb;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{values.vehiName}}</td>

 <td ng-style="{ 'color':values.position=='U'?'red':'black' }"><?php echo Lang::get('content.fuel_cons'); ?> <?php echo Lang::get('content.ltrs'); ?></td>
 <td ng-repeat="val in values.fuelTodays track by $index" style="color:#e18e32;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{val.fuelsToday}}</td>
 <td style="color:#e18e32;font-weight: bold;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{roundOffDecimal(values.totFuel)}}</td>
</tr>
<tr ng-repeat="n in [].constructor(1) track by $index" ng-if="monthFuelData.cumulativeVehicleDistanceData!=null" class="GridViewScrollItem">
<td colspan="2" style="background-color:#ecf7fb;padding-top: 15px;"><?php echo Lang::get('content.total_distance'); ?> (<?php echo Lang::get('content.kms'); ?>)</td>

<td ng-repeat="val in totMonDistVehic track by $index" style="color:#557fa8;font-weight: bold;">{{roundOffDecimal(val)}}</td>
</tr>
<tr ng-if="monthFuelData.cumulativeVehicleFuelFillData!=null" class="GridViewScrollItem">  
<td colspan="2" style="background-color:#ecf7fb;padding-top: 15px;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_fill'); ?> <?php echo Lang::get('content.ltrs'); ?></td>
<td ng-repeat="val in totMonFuelFill track by $index" style="color:#e18e32;font-weight: bold;">{{val}}</td>
</tr>
<tr ng-if="monthFuelData.cumulativeVehicleLphData!=null" class="GridViewScrollItem">  
<td colspan="2" style="background-color:#ecf7fb;padding-top: 15px;"> <?php echo Lang::get('content.ltrs_per_hrs'); ?></td>
<td ng-repeat="val in totMonLph track by $index" style="color:#557fa8;font-weight: bold;">{{val}}</td>
</tr>
<tr ng-if="monthFuelData.cumulativeVehicleKmplData!=null" class="GridViewScrollItem">  
<td colspan="2" style="background-color:#ecf7fb;padding-top: 15px;"><?php echo Lang::get('content.kmpl'); ?></td>
<td ng-repeat="val in totMonKmplVehic track by $index" style="color:#e18e32;font-weight: bold;">{{val}}</td>
</tr>
<tr ng-repeat-end ng-if="monthFuelData.cumulativeVehicleFuelData!=null" class="GridViewScrollItem">  
<td colspan="2" style="background-color:#ecf7fb;padding-top: 15px;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_cons'); ?> <?php echo Lang::get('content.ltrs'); ?></td>
<td ng-repeat="val in totMonFuelVehic track by $index" style="color:#557fa8;font-weight: bold;">{{val}}</td>
</tr>
<tr ng-if="monthFuelData.cumulativeVehicleDistanceData==null || monthFuelData.cumulativeVehicleFuelData==null || monthFuelData.cumulativeVehicleKmplData==null || monthFuelData.cumulativeVehicleFuelFillData==null || monthFuelData.cumulativeVehicleLphData==null" align="center" class="GridViewScrollItem"> 
<td colspan="36" style="border-left: 1px solid #e6e6e6;"><h5 class="err"><?php echo Lang::get('content.no_data_found'); ?></h5></td>
</tr>
</table>
  </div>

            <table cellspacing="0" id="MonthFuel" style="width: 100%; border-collapse: collapse;" ng-show="false">

              <tr class="GridViewScrollHeader" style="padding-bottom:20px;" ng-if="monthFuelData.cumulativeVehicleDistanceData!=null || monthFuelData.cumulativeVehicleFuelData!=null || monthFuelData.cumulativeVehicleKmplData!=null">

               <th style="background-color:#f9f9f9;" > {{ vehiLabel | translate }} <?php echo Lang::get('content.name'); ?> </th>
               <th style="background-color:#f9f9f9;"> <?php echo Lang::get('content.day'); ?> </th>
               <th ng-repeat="dat in monthFuelDates" style="background-color:#ecf7fb;font-weight: bold;">{{dat}}</th>
               <th style="background-color:#f9f9f9;"> <?php echo Lang::get('content.total'); ?> </th>

             </tr>
             
             <tr class="GridViewScrollItem" ng-repeat-start="values in distMonFuelData track by $index">
               <td  ng-if="$odd" style="background-color:#f9f9f9;font-weight: bold;padding-top: 15px;border-bottom-width: 0px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{values.vehiName}}</td>
               <td  ng-if="$even" style="background-color:#ecf7fb;font-weight: bold;padding-top: 15px;border-bottom-width: 0px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{values.vehiName}}</td>         


               <td ng-style="{ 'color':values.position=='U'?'red':'black' }"><?php echo Lang::get('content.distance'); ?> (<?php echo Lang::get('content.kms'); ?>)</td>
               <td ng-repeat="val in values.distsTodays track by $index" style="color:#557fa8;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{(values.vehicleMode=='DG')? '-' : val.distanceToday}}</td>
               <td style="color:#557fa8;font-weight: bold; " ng-style="{ 'color':values.position=='U'?'red':'black' }">{{(values.vehicleMode=='DG')? '-' :  values.totDist}}</td>
             </tr>
             <tr class="GridViewScrollItem"> 
               <td  ng-if="$odd" style="background-color:#f9f9f9;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }"> </td>
               <td  ng-if="$even" style="background-color:#ecf7fb;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }"> </td>

               <td ng-style="{ 'color':values.position=='U'?'red':'black' }"><?php echo Lang::get('content.fuel_fill'); ?> <?php echo Lang::get('content.ltrs'); ?></td>
               <td ng-repeat="val in values.fuelFillTodays track by $index" style="color:#e18e32;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{val.fuelFillToday}}</td>
               <td style="color:#e18e32;font-weight: bold;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{roundOffDecimal(values.totFuelFill)}}</td>
             </tr>
             <tr class="GridViewScrollItem" ng-if="values.vehicleMode =='Moving Vehicle' || values.vehicleMode=='Dispenser'"> 
                 <td  ng-if="$odd" style="background-color:#f9f9f9;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }"> </td>
                 <td  ng-if="$even" style="background-color:#ecf7fb;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }"> </td>

                 <td  ng-style="{ 'color':values.position=='U'?'red':'black' }"><?php echo Lang::get('content.kmpl'); ?></td>
                 <td   ng-repeat="val in values.kmplTodays track by $index" style="color:#e18e32;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{val.kmplsToday}}</td>
                 <td   style="color:#e18e32;font-weight: bold;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{roundOffDecimal(values.totKmpl)}}</td>
                </tr>
               <tr class="GridViewScrollItem" ng-if="values.vehicleMode =='DG' || values.vehicleMode=='Machinery'"> 
                 <td  ng-if="$odd" style="background-color:#f9f9f9;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }"> </td>
                 <td  ng-if="$even" style="background-color:#ecf7fb;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }"> </td>

                 <td   ng-style="{ 'color':values.position=='U'?'red':'black' }"><?php echo Lang::get('content.ltrs_per_hrs'); ?></td>
                 <td   ng-repeat="val in values.lphTodays track by $index" style="color:#e18e32;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{val.lphToday}}</td>
                 <td   style="color:#e18e32;font-weight: bold;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{roundOffDecimal(values.totLitperHr)}}</td>
                </tr>
               <tr ng-repeat-end class="GridViewScrollItem"> 
                 <td  ng-if="$odd" style="background-color:#f9f9f9;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }"> </td>
                 <td  ng-if="$even" style="background-color:#ecf7fb;font-weight: bold;padding-top: 15px;border-bottom: none;" ng-style="{ 'color':values.position=='U'?'red':'black' }"> </td>

                 <td ng-style="{ 'color':values.position=='U'?'red':'black' }"><?php echo Lang::get('content.fuel_cons'); ?> <?php echo Lang::get('content.ltrs'); ?></td>
                 <td ng-repeat="val in values.fuelTodays track by $index" style="color:#e18e32;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{val.fuelsToday}}</td>
                 <td style="color:#e18e32;font-weight: bold;" ng-style="{ 'color':values.position=='U'?'red':'black' }">{{roundOffDecimal(values.totFuel)}}</td>
               </tr>
             <tr ng-repeat="n in [].constructor(1) track by $index" ng-if="monthFuelData.cumulativeVehicleDistanceData!=null" class="GridViewScrollItem">
              <td style="background-color:#ecf7fb;padding-top: 15px;"><?php echo Lang::get('content.total_distance'); ?> (<?php echo Lang::get('content.kms'); ?>)</td>
              <td>-</td>
              <td ng-repeat="val in totMonDistVehic track by $index" style="color:#557fa8;font-weight: bold;">{{roundOffDecimal(val)}}</td>
            </tr>
            <tr ng-if="monthFuelData.cumulativeVehicleFuelFillData!=null" class="GridViewScrollItem">  
              <td  style="background-color:#ecf7fb;padding-top: 15px;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_fill'); ?> <?php echo Lang::get('content.ltrs'); ?></td>
              <td>-</td>
              <td ng-repeat="val in totMonFuelFill track by $index" style="color:#e18e32;font-weight: bold;">{{val}}</td>
            </tr>
            <tr ng-if="monthFuelData.cumulativeVehicleLphData!=null" class="GridViewScrollItem">  
                <td colspan="2" style="background-color:#ecf7fb;padding-top: 15px;"> <?php echo Lang::get('content.ltrs_per_hrs'); ?></td>
                <td>-</td>
                <td ng-repeat="val in totMonLph track by $index" style="color:#557fa8;font-weight: bold;">{{val}}</td>
              </tr>
            <tr ng-if="monthFuelData.cumulativeVehicleFuelData!=null" class="GridViewScrollItem">  
              <td  style="background-color:#ecf7fb;padding-top: 15px;"><?php echo Lang::get('content.total'); ?> <?php echo Lang::get('content.fuel_cons'); ?> <?php echo Lang::get('content.ltrs'); ?></td>
              <td>-</td>
              <td ng-repeat="val in totMonFuelVehic track by $index" style="color:#557fa8;font-weight: bold;">{{val}}</td>
            </tr>
            <tr ng-repeat-end ng-if="monthFuelData.cumulativeVehicleKmplData!=null" class="GridViewScrollItem">  
              <td  style="background-color:#ecf7fb;padding-top: 15px;"> <?php echo Lang::get('content.kmpl'); ?></td>
              <td>-</td>
              <td ng-repeat="val in totMonKmplVehic track by $index" style="color:#e18e32;font-weight: bold;">{{val}}</td>
            </tr>
            <tr ng-if="monthFuelData.cumulativeVehicleDistanceData==null || monthFuelData.cumulativeVehicleFuelData==null || monthFuelData.cumulativeVehicleKmplData==null || monthFuelData.cumulativeVehicleFuelFillData==null || monthFuelData.cumulativeVehicleLphData==null" align="center" class="GridViewScrollItem"> 
              <td colspan="36" style="border-left: 1px solid #e6e6e6;"><h5 class="err"><?php echo Lang::get('content.no_data_found'); ?></h5></td>
            </tr>

          </table>

        </div>                     
      </div>
    </div>
  </div>
</div>
</div>
</div>


<script src="assets/js/static.js"></script>
<script src="assets/js/jquery-1.11.0.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
<script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="assets/js/angular-translate.js"></script>
<script src="../resources/views/reports/customjs/html5csv.js"></script>
<script src="../resources/views/reports/customjs/moment.js"></script>
<script src="../resources/views/reports/customjs/FileSaver.js"></script>
<script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
<script src="assets/js/vamoApp.js"></script>
<script src="assets/js/services.js"></script>
<script src="../resources/views/reports/customjs/statistics.js?v=<?php echo Config::get('app.version');?>"></script>

<script>



  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });

  $(function () {

    $('#monthFrom, #monthFroms').datetimepicker({
      minViewMode: 'months',
      viewMode: 'months',
      pickTime: false,
      useCurrent:true,
      format:'MM/YYYY',
      maxDate: new Date,
      minDate: new Date(2015, 12, 1)
    });
  });  
  $("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
  });

//  var generatePDF = function() {
//   kendo.drawing.drawDOM($("#formConfirmation")).then(function(group) {
//     kendo.drawing.pdf.saveAs(group, "DayWise_Truck_Positions.pdf");
//   });
// }

</script>
<script type="text/javascript">
  var gridViewScroll = null;
  window.onload = function () {

    gridViewScroll = new GridViewScroll({
      elementID: "monthlydistfuel",
      width: window.screen.width-280,
      height: window.screen.height-440,
      freezeColumn: true,
                //freezeFooter: true,
                freezeColumnCssClass: "GridViewScrollItemFreeze",
                //freezeFooterCssClass: "GridViewScrollFooterFreeze",
                freezeHeaderRowCount: 1,
                freezeColumnCount: 2,
                onscroll: function (scrollTop, scrollLeft) {
                    // console.log(scrollTop + " - " + scrollLeft);
                  }
                });
    gridViewScroll.enhance();
  }
  function enhance() {
    gridViewScroll.enhance();
  }
  function undo() {
    gridViewScroll.undo();
  }
  
</script>

</body>
</html>

