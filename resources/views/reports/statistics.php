<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="Satheesh">
  <title><?php echo Lang::get('content.gps'); ?></title>
  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="../resources/views/reports/datepicker/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
  <link href="../resources/views/assets/css/datebutton.css" rel="stylesheet" type="text/css" />
  <!-- <link href="//code.ionicframework.com/ionicons/1.5.2/css/ionicons.min.css" rel="stylesheet" type="text/css"> -->
  <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
  <link href="../resources/views/reports/c3chart/c3.css" rel="stylesheet" type="text/css">
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
  <!-- pdfgen -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
  <script src="https://unpkg.com/jspdf-autotable@2.3.2/dist/jspdf.plugin.autotable.js"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-2X3F316479');
  </script>
</head>
<style>
   /* .box > .loading-img {
      z-index: 1020;
      background: transparent url('assets/imgs/status.gif') 50% 50% no-repeat;
      }*/
      text.highcharts-credits {
        display: none;
      }
      .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
        background-color: #ffffff;
      }
      .striped {
        color:white;
        background-color:green;
      }

      body{
        font-family: 'Lato', sans-serif;
        /*font-weight: bold;*/  
/* font-family: 'Lato', sans-serif;
font-family: 'Roboto', sans-serif;
font-family: 'Open Sans', sans-serif;
font-family: 'Raleway', sans-serif;
font-family: 'Faustina', serif;
font-family: 'PT Sans', sans-serif;
font-family: 'Ubuntu', sans-serif;
font-family: 'Droid Sans', sans-serif;
font-family: 'Source Sans Pro', sans-serif;
*/
}

/*#driverChart {
    height:100%;
    width:100%;
    position:relative;
    }*/
  </style>
  <div id="preloader" >
    <div id="status">&nbsp;</div>
  </div>
  <div id="preloader02" >
    <div id="status02">&nbsp;</div>
  </div>
  <body ng-app="mapApp" style="overflow-x:auto;">
    <div id="wrapper" ng-controller="mainCtrl" class="ng-cloak"> 
      <?php include('sidebarList.php');?>
      <div id="page-content-wrapper">
        <div class="container-fluid">
          <div class="panel panel-default">
          </div>          
        </div>
      </div>
      <div id="testLoad"></div>

      <div ng-show="reportBanShow" class="modal fade" id="allReport" role="dialog" style="top: 100px">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-body">
              <p class="err" style="text-align: center;"> <?php echo Lang::get('content.premium_user'); ?> </p>
            </div>
          </div>
        </div>
      </div> 

      <div ng-show="reportBanShow" class="col-md-10" >
        <div class="box box-primary" style="height:90px; padding-top:30px; margin-top:5%; margin-left:8%;">
          <p ><h5 class="err" style="text-align: center;"> <?php echo Lang::get('content.no_report_found'); ?> </h5></p>
        </div>
      </div>


      <div ng-hide="reportBanShow" class="col-md-12">
        <div class="box box-primary">
          <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip">
            <h3 class="box-title"><span><?php echo Lang::get('content.executive_summary'); ?></span>
              <span><?php include('OutOfOrderDataInfo.php');?></span>
            </h3>
            <div class="box-tools pull-right">
              <img style="cursor: pointer;" ng-click="exportData(downloadid)"  src="../resources/views/reports/image/xls.png" />
              <img width=30 height=30 style="cursor: pointer;" ng-click="exportDataCSV(downloadid)"  src="../resources/views/reports/image/csv.jpeg" />
              <img style="cursor: pointer;"  ng-click="exportPDF(downloadid)"  src="../resources/views/reports/image/Adobe.png" />
            </div>
          </div>
          <div class="row" ng-show="showDate" style="margin-top: 20px;">
            <div class="col-md-2" align="center"></div>
            <div class="col-md-2" align="center">
              <div class="form-group">
                <div class="input-group datecomp">
                  <input type="text" ng-model="fromdate" class="form-control placholdercolor" id="dtFrom" placeholder="<?php echo Lang::get('content.fromdate'); ?>">
                </div>
              </div>
            </div>

            <div  class="col-md-2" align="center" >
              <div class="form-group">
                <div class="input-group datecomp">
                  <input type="text" ng-model="todate" class="form-control placholdercolor" id="dtTo" placeholder="<?php echo Lang::get('content.todate'); ?>">
                </div>
              </div>
            </div>
            <div class="col-md-1" align="center">
              <select style="
              height: 35px;
              width: 115px;"  ng-model="orgTime" ng-show="showOrgTime" ng-change="changeTime(orgTime)" >

              <option style="width: 100%;height: 100%"  ng-repeat="time in orgShiftTime track by $index | orderBy:time"    >{{time}}</option>

            </select>
          </div>
          <div class="col-md-2" align="center">
            <button ng-click="plotHist()"><?php echo Lang::get('content.submit'); ?></button>
          </div>
          <div class="col-md-1" align="center"></div>
          <div class="col-md-12"  align="right" style="margin-bottom: 10px;margin-left: 200px;">
            <!-- <button type="button" class="btnDate" ng-click="durationFilter('today')" ng-disabled="todayDisabled"><?php echo Lang::get('content.today'); ?></button> -->
            <button type="button" class="btnDate" ng-click="durationFilter('yesterday')" ng-disabled="yesterdayDisabled" onclick="setToTime('yesterday')"><?php echo Lang::get('content.yesterday'); ?></button>
            <button type="button" class="btnDate" ng-click="durationFilter('thisweek')" ng-disabled="thisweekDisabled" onclick="setToTime('thisweek')"><?php echo Lang::get('content.thisweek'); ?></button>
            <button type="button" class="btnDate" ng-click="durationFilter('lastweek')" ng-disabled="weekDisabled" onclick="setToTime('lastweek')"><?php echo Lang::get('content.lastweek'); ?></button>
            <button type="button" class="btnDate" ng-click="durationFilter('month')" ng-disabled="monthDisabled" onclick="setToTime('thismonth')"><?php echo Lang::get('content.thismonth'); ?></button>
            <button type="button" class="btnDate" ng-click="durationFilter('lastmonth')" ng-disabled="lastmonthDisabled" onclick="setToTime('lastmonth')"><?php echo Lang::get('content.lastmonth'); ?></button>                              
          </div>

        </div>
        <div class="row" style="text-align: right;margin-right: 10px;">
          
          <span style="background-color: red;
          color: white;
          padding: 2px 15px;"></span>
          <span style="padding-left: 5px;margin-right: 10px;font-weight: bold;">No Data Vehicles</span>
        </div>

      </div>

      <hr>
      <div class="col-md-12" ng-hide="donut">
       <div id="container" style="min-width:800px; max-width:800px; height: 350px;"></div>
     </div>

     <div class="col-md-12" ng-show="donut_new">
       <div id="container_new" style="min-width:800px; max-width:800px; height: 350px;"></div>
     </div>


     <div id="singleDiv" class="row">     
      <div class="col-md-5">
        <div style="min-width:400px; max-width:300px; height: 350px;" id="chart3" ></div>
      </div>
      <div class="col-md-5">
        <div style="min-width:400px; max-width:300px; height: 350px;" id="chart4"></div>     
      </div>
    </div>
    <div ng-show="showDriverChart">
     <div id="driverChart"  ng-hide="donut"></div>
   </div>

   <div class="box-body" ng-class="overallEnable?'col-md-9':'col-md-12'" id="statusreport">
    <div id="formConfirmation">
     <tabset class="nav-tabs-custom">
      <div class="form-group pull-right" style="margin-top: 10px;margin-right : 40px; " ng-show="buttonShow">


       <!--  <a style="cursor: pointer; margin-right: 15px;" ><?php echo Lang::get('content.view_summary'); ?></a> -->
       <!--  <img title="Download Summary" style="cursor: pointer;margin-right: 10px;" ng-click="exportData('executiveViewSummary')" src="../resources/views/reports/image/summary.png"  />  -->

       <span style="top: 15px;cursor: pointer;right: 15px;" class="fa fa-download fa-2x form-control-feedback"  title="Download Summary"  ng-click="exportData('executiveViewSummary')" ng-show="tabActive"></span>
     </div>


     <tab ng-show="dailyTabShow" select="alertMe('executive')" heading="<?php echo Lang::get('content.vehiclewise_performance'); ?>"  active="tabActive">

                         <!-- <div class="form-group pull-right">
                          
                              <input type="search" class="form-control input-sm" placeholder="<?php echo Lang::get('content.search'); ?>" ng-model="searchboxExe" ng-onkeyup="keyup()" name="search" />
                            </div> -->
                            <div class="form-group pull-right">
                              <p ng-if="showOrgTime" style="margin-left: 30px;margin-top: 5px;" ><span style="margin-left: 40px;"><b><?php echo Lang::get('content.date_time'); ?></b> : &nbsp;{{fromdate}}&nbsp;{{getAMPM(fromtotime[0])}} - {{todate}}&nbsp;{{getAMPM(fromtotime[1])}}</span> </p>
                              <p ng-if="!showOrgTime" style="margin-left: 30px;margin-top: 5px;" ><span style="margin-left: 40px;"><b><?php echo Lang::get('content.date_time'); ?></b> : &nbsp;{{fromdate}}&nbsp;- {{todate}}&nbsp;</span> </p>
                            </div>


                               <!--  <div class="form-group pull-right">
                                 <div class="dropdown">
                                      <button class="btn btn-info dropdown-toggle" type="button" data-toggle="dropdown">Select Vehicle
                                      <span class="caret"></span></button>
                                      <ul class="dropdown-menu">
                                        <li ng-repeat="loc in vehicle_list[gIndex].vehicleLocations | orderBy:natural('shortName') | filter:searchbox" data-trigger="hover" ng-class="{active:vehiname ==loc.vehicleId}">
                                              <a href="javascript:void(0);"  ng-class="{red:loc.status=='OFF'}" ng-click="genericFunction(loc.vehicleId, $index,loc.shortName,loc.position, loc.address,loc.groupName)" ng-cloak>
                                              <img ng-if="trvShow=='true'" ng-src="assets/imgs/trvSideMarker/{{loc.vehicleType}}_{{loc.color}}.png" fall-back-src="assets/imgs/Car.png" width="16" height="16"/>
                                              <img ng-if="trvShow!='true'" ng-hide="vehiImage" ng-src="assets/imgs/sideMarker/{{loc.vehicleType}}_{{loc.color}}.png" fall-back-src="assets/imgs/Car.png" 
                                              width="16" height="16"/>
                                              <img ng-if="trvShow!='true'" ng-show="vehiImage" ng-src="assets/imgs/assetImage.png" fall-back-src="assets/imgs/assetImage.png" width="10" height="10"/>
                                                        <span style=" font-size: 12px;"> {{loc.shortName}} </span>
                                                      </a> 
                                                   </li>
                                      </ul>
                                    </div>
                                </div>
                                <div class="form-group pull-right">
                                    <div class="dropdown" style="right: 5px;">
                                      <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Select Group
                                      <span class="caret"></span></button>
                                      <ul class="dropdown-menu">
                                        <li ng-repeat="location in vehicle_list track by $index | orderBy:natural('group')"><a href="javascript:void(0);" ng-click="groupSelection(location.group, location.rowId)">{{trimColon(location.group)}}</a></li>
                                      </ul>
                                    </div>
                                  </div> -->

                                  <h4 style="margin:0" class="page-header"><?php echo Lang::get('content.vehiclewise_performance_report'); ?></h4>
                                  <div id='executive' style="width: 100%;overflow-y:auto;">

                                    <table class="table table-bordered table-condensed table-hover"  id='Executive'>
                                      <thead class='header' style=" z-index: 1;">

                                        <tr style="text-align:center; font-weight: bold;">
                                       <!--  <td>
                    <button type="button" ng-click="expandAll(allExpanded = !allExpanded)">
                        <span ng-bind="allExpanded ? '-' : '+'"></span>
                    </button>
                  </td> -->               <td width="1%" style="text-align:center;background-color:#d2dff7;"></td>
                  <td ng-if="showOrgTime && showOrganization"  width="6%" class="id" custom-sort order="'organName'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.organization'); ?> <?php echo Lang::get('content.name'); ?></td>
                  <td width="6%" class="id" custom-sort order="'shortName'" sort="sort" style="text-align:center;background-color:#d2dff7;">{{ vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></td>
                  <td width="6%" class="id" custom-sort order="'vehicleId'" sort="sort" style="text-align:center;background-color:#d2dff7;">{{ vehiLabel | translate }} <?php echo Lang::get('content.id'); ?></td>
                  <td width="9%" class="id" custom-sort order="'fdate'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.fromdate'); ?></td>
                  <td width="9%" class="id" custom-sort order="'tdate'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.todate'); ?></td>
                  <td width="7%" class="id" custom-sort order="'tDistance'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.kms'); ?></td>
                  <td width="7%" class="id" custom-sort order="'parkCount'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.park_count'); ?></td>

                  <td ng-if="vehiAssetView" width="7%" class="id" custom-sort order="'overSpeed'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.over_speed_count'); ?></td>
                                       <!--  <td ng-if="vehiAssetView" width="7%" class="id" custom-sort order="'odoOpeningReading'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.odo_start'); ?></td>
                                        <td ng-if="vehiAssetView" width="7%" class="id" custom-sort order="'odoClosingReading'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.odo_end'); ?></td> -->
                                        <td width="7%" class="id" custom-sort order="'tRunningTime'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.moving'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</td>
                                        <td width="7%" class="id" custom-sort order="'tParkedTime'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.parked'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</td>
                                        <td width="6%" class="id" custom-sort order="'tNoDataTime'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.n_data'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</td>
                                        <td width="6%" class="id" custom-sort order="'tIdleTime'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.idle'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</td>
                                        <td width="2%" class="id" custom-sort order="'sLocation'" sort="sort"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.start_loc'); ?></td>
                                        <td width="2%" class="id" custom-sort order="'eLocation'" sort="sort"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.end_loc'); ?></td>
                                        <td width="2%" class="id" custom-sort order="'power'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.power'); ?></td>
                                      </tr>
                                    </thead>
                                    <!-- <tr ng-repeat="user in execGroupReportData | orderBy:natural(sort.sortingOrder):sort.reverse | filter:searchboxExe" class="active" style="text-align:center; font-size: 12px"> -->
                                      <tr ng-repeat-start="user in ReducedData | orderBy:sort.sortingOrder:sort.reverse | filter:searchboxExe" ng-style="{ 'color':user.position=='U'?'red':'black' }" class="active" style="text-align:center; font-size: 12px;"  ng-click="expanded = !expanded" expand >
                                        <td ng-if="$odd" style="background-color:#ecf7fb;">
                                          <button style="background-color:#ecf7fb;">
                                            <span ng-bind="expanded ? '-' : '+'"></span>
                                          </button>
                                        </td>

                                        <td ng-if="$even" style="background-color:#f9f9f9;">
                                          <button style="background-color:#f9f9f9;">
                                            <span ng-bind="expanded ? '-' : '+'"></span>
                                          </button>
                                        </td>
                                        <td ng-if="$odd && (showOrgTime && showOrganization)" style="background-color:#f9f9f9;">{{organName}}</td>
                                        <td ng-if="$odd" style="background-color:#ecf7fb;">{{user.shortName}}</td>
                                        <td ng-if="$odd" style="background-color:#ecf7fb;">{{user.vehicleId}}</td>
                                        <td ng-if="$odd" style="background-color:#ecf7fb;">{{user.fDate | date:'dd-MM-yyyy'}}</td>
                                        <td ng-if="$odd" style="background-color:#ecf7fb;">{{user.tDate | date:'dd-MM-yyyy'}}</td>
                                        <td ng-if="$odd" style="background-color:#ecf7fb;">{{ (user.vehicleMode == 'DG')? '-' : convert(user.tDistance)}}</td>
                                        <td ng-if="$odd" style="background-color:#ecf7fb;">{{user.parkCount}}</td>
                                        <td ng-if="vehiAssetView && $odd" style="background-color:#ecf7fb;">{{user.overSpeed}}</td>
                                        <!-- <td ng-if="vehiAssetView" >{{user.odoOpeningReading}}</td>
                                          <td ng-if="vehiAssetView" >{{user.odoClosingReading}}</td> -->
                                          <td ng-if="$odd" style="background-color:#ecf7fb;">{{msToTime(user.tRunningTime)}}</td>
                                          <td ng-if="$odd" style="background-color:#ecf7fb;">{{msToTime(user.tParkedTime)}}</td>
                                          <td ng-if="$odd" style="background-color:#ecf7fb;">{{msToTime(user.tNoDataTime)}}</td>
                                          <td ng-if="$odd" style="background-color:#ecf7fb;">{{msToTime(user.tIdleTime)}}</td>
                                          <td ng-if="$odd" style="background-color:#ecf7fb;">
                                            <span ng-if="user.startLocation!==null"> {{user.sLocation}}</span>
                                            <span ng-if="user.startLocation===null">-</span>
                                          </td>
                                          <td ng-if="$odd" style="background-color:#ecf7fb;">
                                            <span ng-if="user.endLocation!==null">{{user.eLocation}}</span>
                                            <span ng-if="user.endLocation===null">-</span>
                                          </td>
                                          <td ng-if="$odd" style="background-color:#f9f9f9;">{{user.power}}</td>
                                          <td ng-if="$even && (showOrgTime && showOrganization)" style="background-color:#f9f9f9;">{{organName}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{user.shortName}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{user.vehicleId}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{user.fDate | date:'dd-MM-yyyy'}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{user.tDate | date:'dd-MM-yyyy'}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{ (user.vehicleMode == 'DG')? '-' :  convert(user.tDistance)}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{user.parkCount}}</td>
                                          <td ng-if="vehiAssetView && $even" style="background-color:#f9f9f9;">{{user.overSpeed}}</td>
                                        <!-- <td ng-if="vehiAssetView" >{{user.odoOpeningReading}}</td>
                                          <td ng-if="vehiAssetView" >{{user.odoClosingReading}}</td> -->
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{msToTime(user.tRunningTime)}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{msToTime(user.tParkedTime)}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{msToTime(user.tNoDataTime)}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{msToTime(user.tIdleTime)}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">
                                            <span ng-if="user.startLocation!==null"> {{user.sLocation}}</span>
                                            <span ng-if="user.startLocation===null">-</span>
                                          </td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">
                                            <span ng-if="user.endLocation!==null">{{user.eLocation}}</span>
                                            <span ng-if="user.endLocation===null">-</span>
                                          </td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{user.power}}</td>
                                        </tr>
                                        <tr  ng-repeat-end ng-show="expanded">
                                          <!-- added for all regards -->
                                         <!-- <td ng-if="user.endLocation!==null">{{user.endLocation}}</td>
                                           <td>{{user.allData[0].date}}</td> -->
                                           <td colspan="20">
                                            <table class="table table-bordered table-condensed table-hover table-striped">
                                              <tr style="text-align:center; font-weight: bold;">
                                                <td width="6%" style="text-align:center;background-color:#d2dff7;">{{ vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></td>
                                                <td width="6%" style="text-align:center;background-color:#d2dff7;">{{ vehiLabel | translate }} <?php echo Lang::get('content.id'); ?></td>
                                                <td width="9%" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.date'); ?></td>
                                                <td width="7%" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.kms'); ?></td>
                                                <td width="7%" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.park_count'); ?></td>
                                                <td ng-if="vehiAssetView" width="7%" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.over_speed_count'); ?></td>
                                                <td ng-if="vehiAssetView" width="7%" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.odo_start'); ?></td>
                                                <td ng-if="vehiAssetView" width="7%"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.odo_end'); ?></td>
                                                <td width="7%"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.moving'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</td>
                                                <td width="7%" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.parked'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</td>
                                                <td width="6%"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.n_data'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</td>
                                                <td width="6%" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.idle'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</td>
                                                <td width="18%" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.start_loc'); ?></td>
                                                <td  width="18%" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.end_loc'); ?></td>
                                                <td width="7%" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.power'); ?></td>
                                              </tr>

                                              <tr ng-repeat="singleData in user.allData | orderBy:parkingCount" ng-style="{ 'color':user.position=='U'?'red':'black' }" class="active" style="text-align:center; font-size: 12px">
                                                <td>{{singleData.shortName}}</td>
                                                <td>{{singleData.vehicleId}}</td>
                                                <td>{{singleData.date | date:'dd-MM-yyyy'}}</td>
                                                <td>{{ (singleData.vehicleMode == 'DG')? '-' : singleData.distanceToday}}</td>
                                                <td>{{singleData.parkingCount}}</td>
                                                <td ng-if="vehiAssetView">{{singleData.overSpeedInstances}}</td>
                                                <td ng-if="vehiAssetView">{{ (singleData.vehicleMode == 'DG')? '-' :  singleData.odoOpeningReading}}</td>
                                                <td ng-if="vehiAssetView">{{(singleData.vehicleMode == 'DG')? '-' : singleData.odoClosingReading}}</td>
                                                <td>{{msToTime(singleData.totalRunningTime)}}</td>
                                                <td>{{msToTime(singleData.totalParkedTime)}}</td>
                                                <td>{{msToTime(singleData.totalNoDataTime)}}</td>
                                                <td>{{msToTime(singleData.totalIdleTime)}}</td>
                                                <td  ng-if="singleData.startLocation!==null">{{singleData.startLocation}}</td>
                                                <td  ng-if="singleData.startLocation===null">-</td>
                                                <td  ng-if="singleData.endLocation!==null">{{singleData.endLocation}}</td>
                                                <td  ng-if="singleData.endLocation===null">-</td>
                                                <td>{{singleData.batteryPowerStatus}}</td>
                                              </tr>

                                              <tr ng-if="user.allData=='' || user.allData==null || user.allData.length==0" align="center">
                                                <td colspan="20" class="err"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
                                              </tr>

                                            </table>
                                          </td>

                                        </tr>

                                        <tr ng-if="(execGroupReportData=='' || execGroupReportData==null || execGroupReportData.length==0)&&(!errMsg)" align="center">
                                          <td colspan="20" class="err"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
                                        </tr>
                                        <tr ng-if="errMsg" style="text-align: center">
                                          <td colspan="20" class="err"><h5>{{errMsg}}</h5></td>
                                        </tr>




                                      </table>

                                    </div>
                                    <div id='executiveViewSummary' style="overflow-y:auto; width: -webkit-fill-available;" ng-hide='true'>

                                      <table class="table table-bordered table-condensed table-hover">


                                        <tr style="text-align:center; font-weight: bold;">

                                          <td width="6%" class="id" custom-sort order="'shortName'" sort="sort" style="text-align:center;background-color:#d2dff7;">{{ vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></td>
                                          <td width="6%" class="id" custom-sort order="'vehicleId'" sort="sort" style="text-align:center;background-color:#d2dff7;">{{ vehiLabel | translate }} <?php echo Lang::get('content.id'); ?></td>
                                          <td width="9%" class="id" custom-sort order="'fdate'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.fromdate'); ?></td>
                                          <td width="9%" class="id" custom-sort order="'tdate'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.todate'); ?></td>
                                          <td width="7%" class="id" custom-sort order="'tDistance'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.kms'); ?></td>
                                          <td width="7%" class="id" custom-sort order="'parkCount'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.park_count'); ?></td>

                                          <td ng-if="vehiAssetView" width="7%" class="id" custom-sort order="'overSpeed'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.over_speed_count'); ?></td>
                                          <td width="7%" class="id" custom-sort order="'tRunningTime'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.moving'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</td>
                                          <td width="7%" class="id" custom-sort order="'tParkedTime'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.parked'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</td>
                                          <td width="6%" class="id" custom-sort order="'tNoDataTime'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.n_data'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</td>
                                          <td width="6%" class="id" custom-sort order="'tIdleTime'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.idle'); ?> (<?php echo Lang::get('content.h:m:s'); ?>)</td>
                                          <td width="2%" class="id" custom-sort order="'sLocation'" sort="sort"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.start_loc'); ?></td>
                                          <td width="2%" class="id" custom-sort order="'eLocation'" sort="sort"  style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.end_loc'); ?></td>
                                          <td width="2%" class="id" custom-sort order="'power'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.power'); ?></td>
                                        </tr>
                                        <tr ng-repeat="user in ReducedData | orderBy:sort.sortingOrder:sort.reverse | filter:searchboxExe" class="active" style="text-align:center; font-size: 12px;" >
                                          <td ng-if="$odd" style="background-color:#ecf7fb;">{{user.shortName}}</td>
                                          <td ng-if="$odd" style="background-color:#ecf7fb;">{{user.vehicleId}}</td>
                                          <td ng-if="$odd" style="background-color:#ecf7fb;">{{user.fDate}}</td>
                                          <td ng-if="$odd" style="background-color:#ecf7fb;">{{user.tDate}}</td>
                                          <td ng-if="$odd" style="background-color:#ecf7fb;">{{ (user.vehicleMode == 'DG')? '-' :  convert(user.tDistance)}}</td>
                                          <td ng-if="$odd" style="background-color:#ecf7fb;">{{user.parkCount}}</td>
                                          <td ng-if="vehiAssetView && $odd" style="background-color:#ecf7fb;">{{user.overSpeed}}</td>
                                        <!-- <td ng-if="vehiAssetView" >{{user.odoOpeningReading}}</td>
                                          <td ng-if="vehiAssetView" >{{user.odoClosingReading}}</td> -->
                                          <td ng-if="$odd" style="background-color:#ecf7fb;">{{msToTime(user.tRunningTime)}}</td>
                                          <td ng-if="$odd" style="background-color:#ecf7fb;">{{msToTime(user.tParkedTime)}}</td>
                                          <td ng-if="$odd" style="background-color:#ecf7fb;">{{msToTime(user.tNoDataTime)}}</td>
                                          <td ng-if="$odd" style="background-color:#ecf7fb;">{{msToTime(user.tIdleTime)}}</td>
                                          <td ng-if="$odd" style="background-color:#ecf7fb;">
                                            <span ng-if="user.startLocation!==null"> {{user.sLocation}}</span>
                                            <span ng-if="user.startLocation===null">-</span>
                                          </td>
                                          <td ng-if="$odd" style="background-color:#ecf7fb;">
                                            <span ng-if="user.endLocation!==null">{{user.eLocation}}</span>
                                            <span ng-if="user.endLocation===null">-</span>
                                          </td>
                                          <td ng-if="$odd" style="background-color:#f9f9f9;">{{user.power}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{user.shortName}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{user.vehicleId}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{user.fDate}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{user.tDate}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{ (user.vehicleMode == 'DG')? '-' :  convert(user.tDistance)}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{user.parkCount}}</td>
                                          <td ng-if="vehiAssetView && $even" style="background-color:#f9f9f9;">{{user.overSpeed}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{msToTime(user.tRunningTime)}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{msToTime(user.tParkedTime)}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{msToTime(user.tNoDataTime)}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{msToTime(user.tIdleTime)}}</td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">
                                            <span ng-if="user.startLocation!==null"> {{user.sLocation}}</span>
                                            <span ng-if="user.startLocation===null">-</span>
                                          </td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">
                                            <span ng-if="user.endLocation!==null">{{user.eLocation}}</span>
                                            <span ng-if="user.endLocation===null">-</span>
                                          </td>
                                          <td ng-if="$even" style="background-color:#f9f9f9;">{{user.power}}</td>
                                        </tr>
                                        <tr ng-if="(ReducedData=='' || ReducedData==null || ReducedData.length==0)&&(!errMsg)" align="center">
                                          <td colspan="20" class="err"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
                                        </tr>
                                        <tr ng-if="errMsg" style="text-align: center">
                                          <td colspan="20" class="err"><h5>{{errMsg}}</h5></td>
                                        </tr>
                                      </table>
                                    </div>
                                  </tab>
                        <!-- <tab ng-show="poiTabShow" select="alertMe('poi')" heading="<?php echo Lang::get('content.poi'); ?>" active="actTab"> 
                            <div class="form-group pull-right">
                                <input type="search" class="form-control input-sm" placeholder="{{vehiLabel | translate }} <?php echo Lang::get('content.name/poi'); ?>" id="searchbox" name="search" />
                            </div>
                            <h4 style="margin:0" class="page-header"><?php echo Lang::get('content.place_of_interest'); ?></h4>

                            <div id='poi'>
                            <table class="table table-bordered table-striped table-condensed table-hover table-striped table-fixed-header2" id='poiId'>
                                 <thead class='header' style=" z-index: 1;">
                                    <tr style="text-align:center" >
                                        <th class="id" custom-sort order="'shortName'" sort="sort" style="text-align:center;background-color:#d2dff7;">{{ vehiLabel | translate }} <?php echo Lang::get('content.name'); ?> </th>
                                        <th class="id" custom-sort order="'vehicleId'" sort="sort" style="text-align:center;background-color:#d2dff7;">{{ vehiLabel | translate }} <?php echo Lang::get('content.id'); ?> </th>
                                        <th class="id" custom-sort order="'time'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.date'); ?></th>
                                        <th class="id" custom-sort order="'time'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.time'); ?></th>
                                        <th class="id" custom-sort order="'place'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.place'); ?></th>
                                        <th class="id" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.gmap'); ?></th>
                                    </tr></thead>
                                    <tbody>
                                    <tr ng-repeat="user in geofencedata | orderBy:sort.sortingOrder:sort.reverse" class="active" style="text-align:center">
                                        <td>{{user.shortName}}</td>
                                        <td>{{user.vehicleId}}</td>
                                        <td>{{user.time | date:'dd-MM-yyyy'}}</td>
                                        <td>{{user.time | date:'HH:mm:ss'}}</td>
                                        <td>{{user.place}}</td>
                                        <td><a href="https://www.google.com/maps?q=loc:{{user.latandlng}}" target="_blank">Link</a></td>
                                    </tr>
                                    </tbody>
                                    <tr ng-if="geofencedata==null || geofencedata== '' || geofencedata.length==0" align="center">
                                        <td colspan="6" class="err"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
                                    </tr>           
                            </table>
                            </div>
                          </tab> -->

                          <tab ng-show="consolTabShow" select="alertMe('consolidated')" heading="<?php echo Lang::get('content.consolidated'); ?>" active="actCons"> 
                            <div class="form-group pull-right">
                              <p ng-if="showOrgTime" style="margin-left: 30px;margin-top: 5px;" ><span style="margin-left: 40px;"><b><?php echo Lang::get('content.date_time'); ?></b> : &nbsp;{{fromdate}}&nbsp;{{getAMPM(fromtotime[0])}} - {{todate}}&nbsp;{{getAMPM(fromtotime[1])}}</span> </p>
                              <p ng-if="!showOrgTime" style="margin-left: 30px;margin-top: 5px;" ><span style="margin-left: 40px;"><b><?php echo Lang::get('content.date_time'); ?></b> : &nbsp;{{fromdate}}&nbsp;- {{todate}}&nbsp;</span> </p>
                            </div>

                            <div class="form-group pull-right">
                              <input type="search" class="form-control input-sm" placeholder="<?php echo Lang::get('content.search'); ?>" ng-model="searchboxExe1" name="search" />
                            </div>
                            <h4 style="margin:0" class="page-header"><?php echo Lang::get('content.consolidated'); ?></h4>
                            <div id='consolidated'>
                              <!-- <div id="formConfirmation"> -->
                                <table class="table table-bordered table-condensed table-hover table-striped table-fixed-header3" id='Consolidated'>
                                  <thead class='header' style=" z-index: 1;">

                                    <tr style="text-align:center; font-weight: bold;">
                                      <th ng-if="showOrgTime && showOrganization" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.organization'); ?><?php echo Lang::get('content.name'); ?></th>
                                      <th style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.start_date'); ?></th>
                                      <th style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.end_date'); ?></th>
                                      <th custom-sort order="'user[0]'" sort="sort" style="text-align:center;background-color:#d2dff7;">{{ vehiLabel | translate }} <?php echo Lang::get('content.name'); ?></th>
                                      <th custom-sort order="'user[2]'" sort="sort" style="text-align:center;background-color:#d2dff7;">{{ vehiLabel | translate }} <?php echo Lang::get('content.id'); ?></th>
                                      <th custom-sort order="'user[1]'" sort="sort" style="text-align:center;background-color:#d2dff7;"><?php echo Lang::get('content.distance'); ?> <?php echo Lang::get('content.KMS'); ?></th>
                                        <!-- <th width="15%" class="id" custom-sort order="'distanceToday'" sort="sort" style="text-align:center;">Distance (Kms)</th>
                                          <th width="55%" class="id" custom-sort order="'address'" sort="sort" style="text-align:center;">Distance (Kms)</th> -->
                                        <!-- <th width="7%" class="id" custom-sort order="'parkingCount'" sort="sort" style="text-align:center;">ParkCount</th>
                                        <th width="7%" class="id" custom-sort order="'overSpeedInstances'" sort="sort" style="text-align:center;">OverSpeed Count</th>
                                        <th width="7%" class="id" custom-sort order="'odoOpeningReading'" sort="sort" style="text-align:center;">Odo Start</th>
                                        <th width="7%" class="id" custom-sort order="'odoClosingReading'" sort="sort" style="text-align:center;">Odo End</th>
                                        <th width="7%" class="id" custom-sort order="'totalRunningTime'" sort="sort" style="text-align:center;">Moving (<?php echo Lang::get('content.h:m:s'); ?>)</th>
                                        <th width="7%" class="id" custom-sort order="'totalIdleTime'" sort="sort" style="text-align:center;">Idle (<?php echo Lang::get('content.h:m:s'); ?>)</th> -->
                                      </tr>
                                    </thead>
                                    <tr ng-repeat="user in barArray | orderBy:sort.sortingOrder:sort.reverse | filter:searchboxExe1" ng-if ="user.distanceMonth !=0" style="text-align:center; font-size: 12px">
                                      <td ng-if="showOrgTime && showOrganization">{{organName}}</td>
                                      <td  >{{fromdate}}</td>
                                      <td  >{{todate}}</td>
                                      <td  >{{user[0]}}</td>
                                      <td  >{{user[2]}}</td>
                                      <td  >{{user[1]}}</td>
                                        <!-- <td>{{user.distanceMonth}}</td>
                                          <td>{{user.address}}</td> -->
                                        <!-- <td>{{user.parkingCount}}</td>
                                        <td>{{user.overSpeedInstances}}</td>
                                        <td>{{user.odoOpeningReading}}</td>
                                        <td>{{user.odoClosingReading}}</td>
                                        <td>{{msToTime(user.totalRunningTime)}}</td>
                                        <td>{{msToTime(user.totalIdleTime)}}</td> -->
                                      </tr>
                                      <tr ng-if="execGroupReportData=='' || execGroupReportData==null || execGroupReportData.length==0" align="center">
                                        <td colspan="10" class="err"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
                                      </tr>
                                    </table>
                                    <!-- </div> -->
                                  </div>
                                </tab>
                              </tabset>
                            </div>
                          </div>
                        </div>  
                      </div>
                    </div>
                    <script src="assets/js/static.js"></script>   
                    <script src="assets/js/jquery-1.11.0.js"></script>
                    <!--  <script src="assets/js/jquery-3.1.1.min.js"></script> -->
                    <script src="assets/js/bootstrap.min.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script> 
      <script data-require="angular-ui-bootstrap@0.11.0" data-semver="0.11.0" src="http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-0.11.0.js"></script> -->
      <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>

      <script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
      <!--  <script src="assets/js/highcharts-bar.js"></script> -->
      <script src="assets/js/highcharts_new.js"></script>
      <script src="assets/js/highcharts_exporting.js"></script>
      <script src="assets/js/highcharts_statistics.js"></script>

      <script src="../resources/views/reports/customjs/FileSaver.js"></script>
      <script src="../resources/views/reports/customjs/moment.js"></script>
      <script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
      <script src="../resources/views/reports/datatable/jquery.dataTables.js"></script>
      <script src="assets/js/angular-translate.js"></script>
      <script src="../resources/views/reports/customjs/html5csv.js"></script>
      <script src="../resources/views/reports/c3chart/d3.js"></script>
      <script src="../resources/views/reports/c3chart/c3.min.js"></script>
      <script src="assets/js/naturalSortVersionDatesCaching.js"></script>
      <script src='assets/js/table-fixed-header.js'></script>
      <!--<script src="assets/js/naturalSortVersionDates.js"></script> -->
      <script src="assets/js/vamoApp.js"></script>
      <script src="assets/js/services.js"></script>
      <script src="../resources/views/reports/customjs/statistics.js?v=<?php echo Config::get('app.version');?>"></script>
      <script>

    // $("#testLoad").load("../public/menu");
    // var logo =document.location.host;
    // var imgName= '/vamo/public/assets/imgs/'+logo+'.small.png';
    // $('#imagesrc').attr('src', imgName);
    $("#menu-toggle").click(function(e) {
      e.preventDefault();
      $("#wrapper").toggleClass("toggled");
    });


    
    $(function () {

      $('#monthFrom, #monthFroms').datetimepicker({
        minViewMode: 'months',
        viewMode: 'months',
        pickTime: false,
        useCurrent:true,
        format:'MM/YYYY',
        maxDate: new Date,
        minDate: new Date(2015, 12, 1)
      });
    });  
    $(document).ready(function(){
      $("#searchbox").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#poiId tbody tr").filter(function() {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });
    
  </script>

</body>
</html>

