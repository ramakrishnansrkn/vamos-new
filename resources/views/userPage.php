<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="assets/js/loaders.css.js"></script>
<div style="box-shadow:0px 8px 17px rgba(0, 0, 0, 0.2)  !important;" >     
<nav class="menus" style="visibility:hidden;">
    <ul>
        <li class="cl-effect-5">
            
            <!-- <a><span  data-hover="Tracking">Tracking</span></a><span class="glyphicons glyphicons-road"></span> -->
            <a><span  data-hover="<?php echo Lang::get('content.tracking'); ?>"><div class="glyphicon glyphicon-map-marker"></div>&nbsp;<?php echo Lang::get('content.tracking'); ?></span></a>
            <ul>
                <li class="removeli" id="historyRl"><a id="history" href="" target="_blank"><?php echo Lang::get('content.history'); ?></a></li>
                <li class="removeli" id="singleTrackRl"><a id="singleTrack" href=""><?php echo Lang::get('content.single_track'); ?></a></li>
                <li class="removeli" id="multiTrackRl"><a id="multiTrack" href=""><?php echo Lang::get('content.multi_track'); ?></a></li>
                <li class="removeli removeli_2" id="routesRl"><a href="../public/track?rt=routes&maps=replay" target="_blank"><?php echo Lang::get('content.routes'); ?></a></li>
                <li class="removeli removeli_2" id="addsiteRl"><!-- <a id="addsites">Admin</a>
                    <div  style="padding-left: 10px"> -->
                       <a href="../public/password_check" id="addsites"><?php echo Lang::get('content.addsite_geofence'); ?></a>
               <!-- </div> -->
                </li>
              <!-- <li class="removeli" id="singleTrackRls"><a id="landNew" href="" target="_blank">New Landing Page</a></li> -->
              <!--  <li class="removeli"><a id="dashNew" href="../public/dashNew" target="_blank">New DashBoard</a></li> -->
                <!-- <li id="removeli"><a href="../public/groupEdit">Edit Group</a></li> -->
            </ul>
        </li>
        <li class="cl-effect-5">
            <!-- <a><span data-hover="Reports">Reports</span></a> -->
            <a><span  data-hover="<?php echo Lang::get('content.reports'); ?>"><div class="glyphicon glyphicon-list-alt"></div>&nbsp;<?php echo Lang::get('content.reports'); ?></span></a>
            <ul>
                <li class="removeli" id="curStatRl"><a href="../public/reports?ind=1"><?php echo Lang::get('content.dashboard'); ?></a></li>
                <li class="removeli" id="vehConsRl"><a id="assetLabel" href="../public/reports?ind=2"><?php echo Lang::get('content.vehicles_consolidated'); ?> </a></li>
                <li class="removeli" id="conSiteRl"><a href="../public/reports?ind=3"> <?php echo Lang::get('content.cons_location'); ?></a></li>
           <!-- <li class="removeli"><a href="../public/reports?ind=4"> Site Location</a></li> -->
                <li class="removeli" id="conOvrRl"><a href="../public/reports?ind=4"> <?php echo Lang::get('content.cons_overspeed'); ?></a></li>
                <li class="removeli" id="stopReportRl"><a id="stopReport" href=""><?php echo Lang::get('content.cons_stoppage'); ?></a></li>
                <li class="removeli" id="conSiteLocRl"><a id="ConSiteLocReport" href=""><?php echo Lang::get('content.cons_site_report'); ?></a></li>
                <li class="removeli" id="tollReportRl"><a id="tollReport" href=""><?php echo Lang::get('content.tollgate_report'); ?></a></li>
                <li class="removeli" id="nonMovingReportRl"><a id="nonMovingReport" href=""><?php echo Lang::get('content.non_moving_report'); ?></a></li>
                <li class="removeli" id="travelSumReportRl"><a id="travelSumReport" href=""><?php echo Lang::get('content.travell_summary'); ?></a></li>
                <li class="removeli" id="fuelConReportRl"><a id="fuelConReport" href=""><?php echo Lang::get('content.cons_fuel_report'); ?></a></li>
                <li class="removeli" id="conPriEngReportRl"><a id="conPriEngReport" href=""><?php echo Lang::get('content.cons_pri_report'); ?></a></li>
                <li class="removeli" id="empAtnReportRl"><a id="empAtnReport" href=""><?php echo Lang::get('content.employee_attendance'); ?></a></li>
                <li class="removeli" id="SchoolSmsReportRl"><a id="SchoolSmsReport" href=""><?php echo Lang::get('content.school_sms_report'); ?></a></li>
                <li class="removeli" id="siteAlertReportRl"><a id="siteAlertReport" href=""><?php echo Lang::get('content.site_alert'); ?> </a></li>
                <li class="removeli" id="conRfidReportRl"><a id="conRfidReport" href=""> <?php echo Lang::get('content.cons_rfid_report'); ?></a></li>
                <li class="removeli" id="conAlarmReportRl"><a id="conAlarmReport" href=""> <?php echo Lang::get('content.cons_alarm_report'); ?></a></li>

                <!-- <li> <a href="#"> Advanced &nbsp; </a>
                     <div style="padding-left: 10px">
                        <table style="width: 300px; height: 100px;">
                            <tr>
                                <td><a id="movement" href="" class="lowcase"> Movement </a></td>
                                <td><a id="overspeed" href="" class="lowcase"> OverSpeed </a></td>
                                <td><a id="parked" href="" class="lowcase"> Parked </a></td>
                            </tr>
                            <tr>
                                <td><a id="idle" href="" class="lowcase"> Idle </a></td>
                                <td><a id="event" href="" class="lowcase"> Event </a></td>
                                <td><a id="site" href="" class="lowcase"> Site </a></td>
                            </tr>
                            <tr>
                                <td><a id="load" href="" class="lowcase"> Load </a></td>
                                <td><a id="fuel" href="" class="lowcase"> Fuel </a></td>
                                <td><a id="ignition" href="" class="lowcase"> Ignition </a></td>
                            </tr>
                            <tr>
                                <td><a id="trip" href="" class="lowcase"> Trip Time </a></td>
                                <td><a id="tripkms" href="" class="lowcase"> Trip Summary</a></td>
                                <td><a id="temp" href="" class="lowcase"> Temperature</a></td>
                            </tr>
                            <tr>
                                <td><a id="alarm" href="" class="lowcase">Alarm</a></td>
                            </tr>
                        </table>

                    </div>
                </li> -->
            </ul>
        </li>
            <li class="cl-effect-5">
                <!-- <a><span data-hover="analytics">analytics</span></a> -->
                <a><span  data-hover="<?php echo Lang::get('content.analytics'); ?>"><div class="glyphicon glyphicon-screenshot"></div>&nbsp;<?php echo Lang::get('content.analytics'); ?></span></a>
                <ul>
                    <li class="removeli" id="movementRl"><a id="movement" href="" > <?php echo Lang::get('content.movement'); ?></a></li>
                    <li class="removeli" id="overspeedRl"><a id="overspeed" href=""> <?php echo Lang::get('content.overspeed'); ?> </a></li>
                    <li class="removeli" id="parkedRl"><a id="parked" href=""> <?php echo Lang::get('content.parked'); ?> </a></li>
                    <li class="removeli" id="idleRl"><a id="idle" href=""> <?php echo Lang::get('content.idle'); ?> </a></li>
                    <li class="removeli" id="ignitionRl"><a id="ignition" href=""> <?php echo Lang::get('content.ign'); ?> </a></li>
                    <li class="removeli" id="fuelRl"><a id="fuel" href=""> <?php echo Lang::get('content.fuel'); ?> </a></li>
               <!-- <li class="removeli" id="eventRl"><a id="event" href=""> Event </a></li>
                    <li class="removeli" id="siteRl"><a id="site" href=""> Site </a></li>
                    <li class="removeli" id="multiSiteRl"><a id="multiSite" href=""> Site (Mulitple Site)</a></li> -->
                    <li class="removeli" id="tripSiteRl"><a id="tripSite" href=""> <?php echo Lang::get('content.site_trip'); ?></a></li>
                    <li class="removeli" id="tripRl"><a id="trip" href=""> <?php echo Lang::get('content.trip_time'); ?> </a></li>
                    <li class="removeli" id="tripkmsRl"><a id="tripkms" href=""> <?php echo Lang::get('content.trip_summary'); ?></a></li>
                    <li class="removeli" id="alarmRl"><a id="alarm" href=""><?php echo Lang::get('content.alarm'); ?></a></li>
                    <li class="removeli" id="stoppageRl"><a id="stoppage" href=""><?php echo Lang::get('content.stoppage'); ?></a></li>
                    <li class="removeli" id="noDataRl"><a id="noData" href=""><?php echo Lang::get('content.n_data'); ?></a></li>
                </ul>
            </li>
        <li class="cl-effect-5">
            <!-- <a><span data-hover="Statistics">Statistics</span></a> -->
            <a><span  data-hover="<?php echo Lang::get('content.statistics'); ?>"><div class="glyphicon glyphicon-stats"></div>&nbsp;<?php echo Lang::get('content.statistics'); ?></span></a>
            <ul>
                <li class="removeli" id="dailyRl"><a href="../public/statistics?ind=1"><?php echo Lang::get('content.daily'); ?></a></li>
                <!-- <li class="removeli" id="poiRl"><a href="../public/statistics?ind=2"><?php echo Lang::get('content.poi'); ?></a></li> -->
                <li class="removeli" id="consolRl"><a href="../public/statistics?ind=3"><?php echo Lang::get('content.consolidated'); ?></a></li>
                <li class="removeli" id="exfuelRl"><a href="../public/statistics?ind=4"><?php echo Lang::get('content.exe_fuel'); ?></a></li>
                <li class="removeli" id="monDistRl"><a href="../public/statistics?ind=5"><?php echo Lang::get('content.monthly_dist'); ?></a></li> 
                <li class="removeli" id="monDistFuelRl"><a href="../public/statistics?ind=6"><?php echo Lang::get('content.monthly_dist_fuel'); ?></a></li> 
            </ul>
        </li>
        <li class="cl-effect-5" id="sensMenu">
        <a><span  data-hover="<?php echo Lang::get('content.sensor'); ?>"><div class="glyphicon glyphicon-list"></div>&nbsp;<?php echo Lang::get('content.sensor'); ?></span></a>
            <!-- <a><span data-hover="sensor">sensor</span></a> -->
            <ul>
                    <li class="removeli" id="acRl"><a id="ac" href=""> <?php echo Lang::get('content.ac'); ?> </a></li>
                    <li class="removeli" id="idleWasteRl"><a id="idleWaste" href=""> <?php echo Lang::get('content.idle_wastage'); ?> </a></li>
                    <li class="removeli" id="prmEngineOnRl"><a id="prmEngineOn" href=""><?php echo Lang::get('content.pri_eng_on'); ?> </a></li>
                    <li class="removeli" id="doorSensorRl"><a id="doorSensor" href="" style="left: 7px;"><img src="assets/menu/secondary_Engine_On.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px">
                    <li class="removeli" id="engineOnRl"><a id="engineOn" href=""><?php echo Lang::get('content.sec_eng_on'); ?> </a></li>
                <!--<li class="removeli" id="loadRl"><a id="load" href=""> Load </a></li>-->
                    <li class="removeli" id="tempRl"><a id="temp" href=""> <?php echo Lang::get('content.temperature'); ?></a></li>
                    <li class="removeli" id="tempdevRl"><a id="tempdev" href=""> <?php echo Lang::get('content.temp_deviation'); ?></a></li>
                <!--<li class="removeli" id="distTimeRl"><a href="../public/fuel">Distance &amp; Time</a></li>-->
                    <li class="removeli" id="fuelNewRl"><a id="fuelNew" href=""><?php echo Lang::get('content.fuel_speed'); ?></a></li>
                <!--<li class="removeli" id="fuelFillRl"><a href="../public/fuel?tab=fuelfill">Fuel Fill</a></li>-->
                    <li class="removeli" id="fuelFillRl"><a id="fuelFill" href=""><?php echo Lang::get('content.fuel_fill'); ?></a></li>
                    <li class="removeli" id="fuelMachineRl"><a id="fuelMachine" href=""><?php echo Lang::get('content.fuel_analytics'); ?></a></li>
                    <li class="removeli" id="fuelTheftRl"><a  id="fuelTheft" href="" ><?php echo Lang::get('content.fuel_theft'); ?></a></li>
                    <li class="removeli" id="fuelProtocolRl"><a  id="fuelProtocol" href="" ><?php echo Lang::get('content.protocol'); ?> <?php echo Lang::get('content.report'); ?></a></li>
                    <li class="removeli" id="fuelMileageRl"><a  id="fuelMileage" href="" ><?php echo Lang::get('content.fuel_mileage'); ?></a></li>
                    <li class="removeli" id="fuelRawRl"><a  id="fuelRaw" href="" ><?php echo Lang::get('content.fuel_raw_data'); ?></a></li>
                    <li class="removeli" id="fuelConVehiRl"><a  id="fuelConVehicle" href=""> <?php echo Lang::get('content.vehicle_wise_report'); ?></a></li>
                    <li class="removeli" id="fuelConTimeRl"><a  id="fuelConTime" href="" style="left: 7px;"><?php echo Lang::get('content.vehicle_wise_intraday'); ?> </a></li>
                    <li class="removeli" id="fuelConTimeRl"><a  id="fuelConTime" href="" style="left: 7px;">
                        <?php echo Lang::get('content.vehicle_wise_intraday'); ?> </a></li>
                    <li class="removeli" id="rfidRl"><a id="rfid" href=""><?php echo Lang::get('content.rfid'); ?></a></li>
                    <li class="removeli" id="rfidNewRl"><a id="rfidNew" href=""><?php echo Lang::get('content.rfid_tag'); ?></a></li>
                    <li class="removeli" id="cameraRl"><a id="camera" href=""><?php echo Lang::get('content.camera'); ?></a></li> 
            </ul>
        </li>
        <!-- <li class="cl-effect-5">
           
            <a><span  data-hover="Fuel"><div class="glyphicon glyphicon-tint"></div>&nbsp;Fuel</span></a>
            <ul>
                <li><a href="../public/fuel">Distance &amp; Time</a></li>
                <li><a href="../public/fuel?tab=fuelfill">Fuel Fill</a></li>
            </ul>
        </li> -->
        <li class="cl-effect-5">
            <a><span  data-hover="<?php echo Lang::get('content.performance'); ?>"><div class="glyphicon glyphicon-charts"></div>&nbsp;<?php echo Lang::get('content.performance'); ?></span></a>
            <!-- <a><span  data-hover="Performance">Performance</span></a> -->
            <ul>
                <li class="removeli" id="dailyPerfRl"><a href="../public/performance?tab=daily"><?php echo Lang::get('content.daily_per'); ?></a></li>
                <li class="removeli" id="monPerfRl"><a href="../public/performance"><?php echo Lang::get('content.monthly_per'); ?></a></li>
            </ul>
        </li>
        
        <li id="userId">
            <span class="glyphicon glyphicon-user"><span id="valueUser" style="font-size:12px; font-weight: bold; font-family: Helvetica"></span></span>  
            <ul>           
                <li id="payDetRl"><a href="../public/payDetails"><?php echo Lang::get('content.payment_details'); ?></a></li>
                <li class="removeli removeli_2" id="editGrpRl"><a href="../public/groupEdit"><?php echo Lang::get('content.edit_group'); ?></a></li>
                <li class="removeli removeli_2" id="editNotRl"><a href="../public/passWd?userlevel=notify"><?php echo Lang::get('content.edit_notification'); ?></a></li>
                <li class="removeli removeli_2" id="editUsNotRl"><a href="../public/userNotify"><?php echo Lang::get('content.edit_user_notification'); ?></a></li>
                <li class="removeli removeli_2" id="resetRl"><a href="../public/passWd?userlevel=reset"><?php echo Lang::get('content.reset_password'); ?></a></li>
                <li class="removeli removeli_2" id="editBusRl"><a href="../public/passWd?userlevel=busStop"><?php echo Lang::get('content.edit_busstop'); ?></a></li>
                <li class="removeli removeli_2" id="getApiRl"><a href="../public/passWd?userlevel=apiKeys"><?php echo Lang::get('content.get_apikey'); ?></a></li>
            </ul>
        </li>
        
        <li>
            <a href="../public/logout" class="glyphicon glyphicon-log-out" style="padding: 10px 15px;color: #fff; text-align: center;"></a>
        </li>
    </ul>
</nav>
</div>

<script type="text/javascript">

// $('#pwdModal').appendTo("body") 
    
    // var menuValue = JSON.parse(localStorage.getItem('userIdName'));
    // var sp = [];
    // try{

    //     sp = menuValue.split(",");
        
    // }catch (err){
    //     $.ajax({

    //         async: false,
    //         method: 'GET', 
    //         url: "aUthName",
    //         // data: {"orgId":$scope.orgIds},
    //         success: function (response) {

    //             console.log(response)
    //             sp[1] = response[1];
    //         }
    //     })
    //     // sp[1] = 'DEMO';
    //     // console.log(document.location)
    // }


        var globalIP = document.location.host;
        var contextMenu = '/'+document.location.pathname.split('/')[1];
        var labelVal;
        var menuValue = JSON.parse(localStorage.getItem('userIdName'));

        window.localStorage.setItem("userMasterName",menuValue);
        // sp1 = menuValue.split(",");
        // $('#valueUser').text("  "+sp1[1]);

        // console.log(sp1[1]);

        // var obj = JSON.parse(localStorage.getItem('user'));
      //var menuObj = JSON.parse(window.localStorage.getItem('MenuData'));
      //console.log(menuObj);

        // var sp     =  obj.split(',');
        var vid    =  "<?php echo Session::get('vehicle'); ?>";
        //alert(vid)
        var gname  =  "<?php echo Session::get('group'); ?>";
        //alert(gname)
        
            //$("#movement").click(function() {
              //  window.localStorage.removeItem("reload");
               $("#movement").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=movement' );
           // });

           // $("#noData").click(function() {
                $("#noData").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=noData' );
           // });

          //  $("#overspeed").click(function() {
                $("#overspeed").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=overspeed')
          //  });
          //  $("#parked").click(function() {
                $("#parked").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=parked')
          //  });
         //   $("#idle").click(function() {
                $("#idle").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=idle')
          //  });
         //   $("#event").click(function() {
                $("#event").attr("href", 'event?vid='+vid+'&vg='+gname+'&tn=event')
         //   });
         //   $("#site").click(function() {
                $("#site").attr("href", 'siteReport?vid='+vid+'&vg='+gname+'&tn=site')
         //   }); 
          //  $("#multiSite").click(function() {
                $("#multiSite").attr("href", 'multiSite?vid='+vid+'&vg='+gname+'&tn=multiSite')
          //  });
          //  $("#tripSite").click(function() {
                $("#tripSite").attr("href", 'tripSite?vid='+vid+'&vg='+gname+'&tn=tripSite')
          //  });
          //  $("#load").click(function() {
                $("#load").attr("href", 'loadDetails?vid='+vid+'&vg='+gname+'&tn=load')
          //  });
          //  $("#fuel").click(function() {
                $("#fuel").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=fuel')
          //  });
             // $("#fuelFill").click(function() {
                $("#fuelFill").attr("href", 'fuel?vid='+vid+'&vg='+gname+'&tab=fuelfill')
             // });
          //  $("#fuelTheft").click(function() {
                $("#fuelTheft").attr("href", 'fuelTheft?vid='+vid+'&vg='+gname+'&tn=fuelTheft')
         //   });
          //  $("#fuelProtocol").click(function() {
                $("#fuelProtocol").attr("href", 'fuelProtocol?vid='+vid+'&vg='+gname+'&tn=fuelProtocol')
         //   });
         //  $("#fuelProtocol").click(function() {
                $("#fuelMachine").attr("href", 'fuelMachine?vid='+vid+'&vg='+gname+'&tn=fuelMachine')
         //   });
         //   $("#fuelMileage").click(function() {
                $("#fuelMileage").attr("href", 'fuelMileage?vid='+vid+'&vg='+gname+'&tn=fuelMileage')
         //   });
         //   $("#fuelConReport").click(function() {
                $("#fuelConReport").attr("href", 'fuelConReport?vid='+vid+'&vg='+gname+'&tn=fuelConReport')
         //   });
         //   $("#fuelRaw").click(function() {
                $("#fuelRaw").attr("href", 'fuelRaw?vid='+vid+'&vg='+gname+'&tn=fuelRaw')
                $("#fuelConVehicle").attr("href", 'fuelConsolidVehicle?vid='+vid+'&vg='+gname+'&tn=fuelConVehicle')
         //   });
         //   $("#fuelConVehicle").click(function() {
                $("#fuelConTime").attr("href", 'fuelConsolidTime?vid='+vid+'&vg='+gname+'&tn=fuelConTime')
         //   });
         //   }); 
         //   });
         //   $("#conAlarmReport").click(function() {
                $("#conAlarmReport").attr("href", 'conAlarmReport?vid='+vid+'&vg='+gname+'&tn=conAlarmReport')
         //   });
         //   $("#conRfidReport").click(function() {
                $("#conRfidReport").attr("href", 'conRfidReport?vid='+vid+'&vg='+gname+'&tn=conRfidReport')
         //   });
        //    $("#conPriEngReport").click(function() {
                $("#conPriEngReport").attr("href", 'conPriEngReport?vid='+vid+'&vg='+gname+'&tn=conPriEngReport')
        //    });
         //   $("#fuelNew").click(function() {
                $("#fuelNew").attr("href", 'fuelNew?vid='+vid+'&vg='+gname+'&tn=fuelNew')
         //   });
         //   $("#ignition").click(function() {
                $("#ignition").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=ignition')
         //   });
         //   $('#trip').click(function() {
                $('#trip').attr("href", 'trip?vid='+vid+'&vg='+gname+'&tn=trip')
         //   });
         //   $('#tripkms').click(function() {
                // console.log(' clck ')
                $('#tripkms').attr("href", 'track?vid='+vid+'&vg='+gname+'&tn=tripkms&maps=tripkms')
         //   });
         //   $('#temp').click(function() {
                // console.log(' clck ')
                $('#temp').attr("href", 'temperature?vid='+vid+'&vg='+gname+'&tn=temperature')
          //  });
          //  $('#tempdev').click(function() {
                // console.log(' clck ')
                $('#tempdev').attr("href", 'temperature?vid='+vid+'&vg='+gname+'&tn=temperatureDev')
         //   });
         //   $('#alarm').click(function(){
                $('#alarm').attr("href", 'alarm?vid='+vid+'&vg='+gname+'&tn=alarm')
          //  })

        /*  $('#ac').click(function(){
                $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=acreport')
            }) */

         //   $('#stoppage').click(function(){
                $('#stoppage').attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=stoppageReport')
         //   })

        //    $('#rfid').click(function(){
                $('#rfid').attr("href", 'rfidTag?vid='+vid+'&vg='+gname+'&tn=rfid')
         //   })

         //   $('#rfidNew').click(function(){
                $('#rfidNew').attr("href", 'rfidTagNew?vid='+vid+'&vg='+gname+'&tn=rfidNew')
         //   })

         //   $('#ac').click(function(){
                $('#ac').attr("href", 'ac?vid='+vid+'&vg='+gname+'&tn=ac')
         //   })

         //    $('#idleWaste').click(function(){
                $('#idleWaste').attr("href", 'idleWaste?vid='+vid+'&vg='+gname+'&tn=ac')
         //   })

         //   $('#engineOn').click(function(){
                $('#engineOn').attr("href", 'ac?vid='+vid+'&vg='+gname+'&tn=engine')
                $('#doorSensor').attr("href", 'ac?vid='+vid+'&vg='+gname+'&tn=drSensor')
         //   })

         //   $('#prmEngineOn').click(function(){
                $('#prmEngineOn').attr("href", 'ac?vid='+vid+'&vg='+gname+'&tn=prmEngine')
         //   })
            
         //   $('#camera').click(function(){
                $('#camera').attr("href", 'camera?vid='+vid+'&vg='+gname+'&tn=camera')
          //  })

         //   $('#stopReport').click(function(){
                $('#stopReport').attr("href", 'stopReport?vid='+vid+'&vg='+gname+'&tn=stopReport')
        //    })

         //   $('#ConSiteLocReport').click(function(){
                $('#ConSiteLocReport').attr("href", 'ConSiteLocReport?vid='+vid+'&vg='+gname+'&tn=ConSiteLocReport')
         //   })

          //  $('#tollReport').click(function(){
                $('#tollReport').attr("href", 'tollReport?vid='+vid+'&vg='+gname+'&tn=tollReport')
          //  })

         //   $('#nonMovingReport').click(function(){
                $('#nonMovingReport').attr("href", 'nonMovingReport?tn=nonMovingReport')
         //   })

          //  $('#travelSumReport').click(function(){
                $('#travelSumReport').attr("href", 'travelSummary?vid='+vid+'&vg='+gname+'&tn=travelSumReport')
         //   })

          //  $('#empAtnReport').click(function(){
                $('#empAtnReport').attr("href", 'empAtnReport?vid='+vid+'&vg='+gname+'&tn=empAtnReport')
         //   })

          //  $('#SchoolSmsReport').click(function(){
                $('#SchoolSmsReport').attr("href", 'SchoolSmsReport?vid='+vid+'&vg='+gname+'&tn=SchoolSmsReport')
         //   })
            
         //   $('#siteAlertReport').click(function(){
                $('#siteAlertReport').attr("href", 'siteAlertReport?vid='+vid+'&vg='+gname+'&tn=siteAlertReport')
          //  })

          //  $('#multiTrack').click(function(){
                $('#multiTrack').attr("href", 'track?vehicleId='+vid+'&track=multiTrack&maps=mulitple');
                $('#multiTrack').attr("target", '_blank');
         //   });
         //   $('#singleTrack').click(function(){
                $('#singleTrack').attr("href", 'track?vehicleId='+vid+'&track=single&maps=single');
                $('#singleTrack').attr("target", '_blank');
         //   });

         //   $('#history').click(function () {
                $('#history').attr("href", "track?vehicleId="+vid+'&gid='+gname+'&maps=replay');
         //   })


        $.get('//'+globalIP+contextMenu+'/public/getReportsList', function(response) {

        if(response){
              var menuObj=JSON.parse(response);
               //console.log(menuObj);

           if( menuObj != "" && menuObj!=null ){ 

             for(var i=0;i<menuObj.length;i++){

            // console.log( Object.getOwnPropertyNames(menuObj[i]).sort() );
                  var newReportName = Object.getOwnPropertyNames(menuObj[i]).sort();

                if(newReportName == "Analytics_IndivdiualReportsList"){

                  //  console.log( "Analytics_Reports");
                  //  console.log(menuObj[i].Analytics_IndivdiualReportsList.length);
               

                       var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Analytics_IndivdiualReportsList[0]).sort();
                     //  console.log(newReportNamesss.length);

                        
                        for(var k=0;k<newReportNamesss.length;k++){

                            switch(newReportNamesss[k]){

                                case 'ALARM':
                                $('#alarmRl').addClass('ALARM');
                                break;
                                case 'IDLE':
                                $('#idleRl').addClass('IDLE');
                                break;
                                case 'MOVEMENT':
                                $('#movementRl').addClass('MOVEMENT');
                                break;
                                case 'OVERSPEED':
                                $('#overspeedRl').addClass('OVERSPEED');
                                break;
                                case 'PARKED':
                                $('#parkedRl').addClass('PARKED');
                                break;
                                case 'TRIP_TIME':
                                $('#tripRl').addClass('TRIP_TIME');
                                break;
                                case 'TRIP_SUMMARY':
                                $('#tripkmsRl').addClass('TRIP_SUMMARY');
                                break;
                            /*  case 'SITE':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].SITE==false){   document.getElementById('siteRl').innerHTML = '';}
                                break; */
                                case 'SITE_TRIP':
                                $('#tripSiteRl').addClass('SITE_TRIP');
                                break;
                             /* case 'EVENT':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].EVENT==false){   document.getElementById('eventRl').innerHTML = '';}
                                break; */
                                case 'STOPPAGE':
                                $('#stoppageRl').addClass('STOPPAGE');
                                break;
                            /*  case 'MULTIPLE_SITE':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].MULTIPLE_SITE==false){   document.getElementById('multiSiteRl').innerHTML = '';}
                                break; */
                                case 'FUEL':
                                $('#fuelMachineRl').addClass('FUEL_ANALYTICS');
                                break;
                                case 'IGNITION':
                                $('#ignitionRl').addClass('IGNITION');
                                $('#prmEngineOnRl').addClass('PRI_ENGINE_ON');
                                break;
                            }
 
                        }

                } else if(newReportName == "Consolidatedvehicles_IndivdiualReportsList"){

                   // console.log( "Consolidatedvehicles_Reports");
                   // console.log(menuObj[i].Consolidatedvehicles_IndivdiualReportsList.length);
               
                     var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0]).sort();
                    // console.log(newReportNamesss.length);

                        for(var k=0;k<newReportNamesss.length;k++){

                            switch(newReportNamesss[k]){

                                case 'CONSOLIDATED_SITE_LOCATION':
                                $('#conSiteRl').addClass('CONSOLIDATED_SITE_LOCATION');
                                break;
                                case 'SCHOOL_SMS_REPORT':
                                $('#SchoolSmsReportRl').addClass('SCHOOL_SMS_REPORT');
                                break;
                                case 'CONSOLIDATED_STOPPAGE':
                                $('#stopReportRl').addClass('CONSOLIDATED_STOPPAGE');
                                break;
                                case 'SITESTOPPAGE_ALERT':
                                $('#siteAlertReportRl').addClass('SITESTOPPAGE_ALERT');
                                break;
                                case 'CURRENT_STATUS':
                                $('#curStatRl').addClass('CURRENT_STATUS');
                                break;
                                case 'TOLLGATE_REPORT':
                                $('#tollReportRl').addClass('TOLLGATE_REPORT');
                                break;
                                case 'NON_MOVING_REPORT':
                                $('#nonMovingReportRl').addClass('NON_MOVING_REPORT');
                                break; 
                                case 'TRAVEL_SUMMARY_REPORT':
                                $('#travelSumReportRl').addClass('TRAVEL_SUMMARY_REPORT');
                                break; 
                                
                                case 'PRIMARY_ENGINE_CONSOLIDATE_REPORT':
                                $('#conPriEngReportRl').addClass('PRIMARY_ENGINE_CONSOLIDATE_REPORT');
                                break; 
                                case 'EMPATTN_REPORT':
                                $('#empAtnReportRl').addClass('EMPATTN_REPORT');
                                break;
                                case 'CONSOLIDATED_OVERSPEED':
                                $('#conOvrRl').addClass('CONSOLIDATED_OVERSPEED');
                                break;
                                case 'VEHICLES_CONSOLIDATED':
                                $('#vehConsRl').addClass('VEHICLES_CONSOLIDATED');
                                break;
                                case 'CONSOLIDATED_SITE':
                                $('#conSiteLocRl').addClass('CONSOLIDATED_SITE');
                                break;
                                case 'CONSOLIDATE_ALARM_REPORT':
                                $('#conAlarmReportRl').addClass('CONSOLIDATE_ALARM_REPORT');
                                break; 
                                case 'CONSOLIDATE_RFID_REPORT':
                                $('#conRfidReportRl').addClass('CONSOLIDATE_RFID_REPORT');
                                break;
                            }
                         }

                } else if(newReportName == "Performance_IndivdiualReportsList"){

                  //  console.log( "Performance_Reports");
                   // console.log(menuObj[i].Performance_IndivdiualReportsList.length);
               
                       var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Performance_IndivdiualReportsList[0]).sort();
                     //  console.log(newReportNamesss.length);

                        for(var k=0;k<newReportNamesss.length;k++){

                            switch(newReportNamesss[k]){

                                case 'DAILY_PERFORMANCE':
                                $('#dailyPerfRl').addClass('DAILY_PERFORMANCE');
                                break;
                                case 'MONTHLY_PERFORMANCE':
                                $('#monPerfRl').addClass('MONTHLY_PERFORMANCE');
                                break;
                            }
                         }

                } else if(newReportName == "Sensor_IndivdiualReportsList") {

                  if(labelVal!="true"){

                // console.log( "Sensor_Reports");
                   // console.log(menuObj[i].Sensor_IndivdiualReportsList.length);
               
                       var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Sensor_IndivdiualReportsList[0]).sort();
                      // console.log(newReportNamesss.length);

                        for(var k=0;k<newReportNamesss.length;k++){

                            switch(newReportNamesss[k]){

                                case 'AC':
                                $('#acRl').addClass('AC');
                                break;
                                case 'DOOR_SENSOR':
                                $('#acRl').addClass('DOOR_SENSOR');
                                break;
                             /* case 'DISTANCE_TIME':
                                  if( menuObj[i].Sensor_IndivdiualReportsList[0].DISTANCE_TIME==false){ document.getElementById('distTimeRl').innerHTML ='';}
                                break; */
                                case 'TEMPERATURE_DEVIATION':
                                $('#tempdevRl').addClass('TEMPERATURE_DEVIATION');
                                break;
                                case 'TEMPERATURE':
                                $('#tempRl').addClass('TEMPERATURE');
                                break;
                             /* case 'LOAD':
                                  if( menuObj[i].Sensor_IndivdiualReportsList[0].LOAD==false){  document.getElementById('loadRl').innerHTML = '';}
                                break; 
                                case 'FUEL_NEW':
                                  if( menuObj[i].Sensor_IndivdiualReportsList[0].FUEL_NEW==false){  document.getElementById('fuelNewRl').innerHTML = '';}
                                break; */
                                case 'FUEL_FILL':
                                $('#fuelFillRl').addClass('FUEL_FILL');
                                break;
                                case 'FUEL_MACHINE':
                                $('#fuelMachineRl').addClass('FUEL_MACHINE');
                                break;
                                
                                case 'FUEL_THEFT':
                                $('#fuelTheftRl').addClass('FUEL_THEFT');
                                break;
                                case 'FUEL_PROTOCOL':
                                $('#fuelProtocolRl').addClass('FUEL_PROTOCOL');
                                break;
                                case 'FUEL_MILEAGE':
                                $('#fuelMileageRl').addClass('FUEL_MILEAGE');
                                break;
                                case 'RFID':
                                $('#rfidRl').addClass('RFID');
                                $('#rfidNewRl').addClass('RFID_NEW');
                                break;
                                case 'CAMERA':
                                $('#cameraRl').addClass('CAMERA');
                                break;
                            }
                         } 
                    }
                        
                } else if(newReportName == "Fuel_IndivdiualReportsList"){

                   // console.log( "Statistics_Reports");
                   // console.log(menuObj[i].Statistics_IndivdiualReportsList.length);
               
                       var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Fuel_IndivdiualReportsList[0]).sort();
                     //  console.log(newReportNamesss.length);

                        for(var k=0;k<newReportNamesss.length;k++){

                            switch(newReportNamesss[k]){
                                case 'FUEL_CONSOLIDATE_REPORT':
                                $('#fuelConReportRl').addClass('FUEL_CONSOLIDATE_REPORT');
                                $('#fuelConVehiRl').addClass('VEHICLE_WISE_FUEL');
                                $('#fuelConTimeRl').addClass('VEHICLE_WISE_INTRA_FUEL');
                                break; 
                                case 'EXECUTIVE_FUEL':
                                $('#exfuelRl').addClass('EXECUTIVE_FUEL');
                                break;
                                case 'MONTHLY_DIST_AND_FUEL':
                                $('#monDistFuelRl').addClass('MONTHLY_DIST_AND_FUEL');
                                break;
                                case 'FUEL_RAW':
                                $('#fuelRawRl').addClass('FUEL_RAW');
                                break;
                            }
 
                        }

                } else if(newReportName == "Statistics_IndivdiualReportsList"){

                   // console.log( "Statistics_Reports");
                   // console.log(menuObj[i].Statistics_IndivdiualReportsList.length);
               
                       var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Statistics_IndivdiualReportsList[0]).sort();
                     //  console.log(newReportNamesss.length);

                        for(var k=0;k<newReportNamesss.length;k++){

                            switch(newReportNamesss[k]){

                                
                                case 'DAILY':
                                $('#dailyRl').addClass('DAILY');
                                break;
                                case 'MONTHLY_DIST':
                                $('#monDistRl').addClass('MONTHLY_DIST');
                                break;
                                // case 'POI':
                                // $('#poiRl').addClass('POI');
                                break;
                                case 'CONSOLIDATED':
                                $('#consolRl').addClass('CONSOLIDATED');
                                break;
                            }
 
                        }

                } else if(newReportName == "Tracking_IndivdiualReportsList"){

                   // console.log( "Tracking_Reports");
                   // console.log(menuObj[i].Tracking_IndivdiualReportsList.length);
               
                       var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Tracking_IndivdiualReportsList[0]).sort();
                     //  console.log(newReportNamesss.length);

                        
                        for(var k=0;k<newReportNamesss.length;k++){

                            switch(newReportNamesss[k]){

                                case 'SINGLE_TRACK':
                                $('#singleTrackRl').addClass('SINGLE_TRACK');
                                break;
                                case 'ROUTES':
                                $('#routesRl').addClass('ROUTES');
                                break;
                                case 'ADDSITES_GEOFENCE':
                                $('#addsiteRl').addClass('ADDSITES_GEOFENCE');
                                break;
                                case 'MULTITRACK':
                                $('#multiTrackRl').addClass('MULTITRACK');
                                break;
                                case 'HISTORY':
                                $('#historyRl').addClass('HISTORY');
                                break;
                            }
                         }

                } else if(newReportName == "Useradmin_IndivdiualReportsList"){

                   // console.log( "Useradmin_Reports");
                   // console.log(menuObj[i].Useradmin_IndivdiualReportsList.length);
               
                       var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Useradmin_IndivdiualReportsList[0]).sort();
                    //   console.log(newReportNamesss.length);

                        for(var k=0;k<newReportNamesss.length;k++){

                            switch(newReportNamesss[k]){

                                case 'EDIT_NOTIFICATION':
                                $('#editNotRl').addClass('EDIT_NOTIFICATION');
                                break;
                                case 'PAYMENT_REPORTS':
                                $('#payDetRl').addClass('PAYMENT_REPORTS');
                                break;
                                case 'GET_APIKEY':
                                $('#getApiRl').addClass('GET_APIKEY');
                                break;
                                case 'EDIT_BUSSTOP':
                                $('#editBusRl').addClass('EDIT_BUSSTOP');
                                break;
                                case 'RESET_PASSWORD':
                                $('#resetRl').addClass('RESET_PASSWORD');
                                break;
                                case 'EDIT_GROUP':
                                $('#editGrpRl').addClass('EDIT_GROUP');
                                break;
                            }
                        }

                } else if(newReportName == "TRACK_PAGE"){

                     if( menuObj[i].TRACK_PAGE[0].Normal_View==true ) {

                        localStorage.setItem('trackNovateView', false);

                         
                     } else {

                        localStorage.setItem('trackNovateView', true); 

                     }
           
                   
                }
             }
             
             
                    var pageName="<?php echo Session::get('userPrePage'); ?>";
                    if(pageName){
                        var userpage='.'+pageName.split(":")[0];
                        //alert(userpage);
                        var userhref=$('a',$(userpage)).attr('href');
                        //alert(userhref);

                        setDateTimeSessionValues();
                        //alert(userhref);

                        var checkHB=pageName.split(":")[0];
                        if(userhref==undefined){
                            window.location.href ='track';
                        }
                        else{
                                if ((checkHB== 'HISTORY') || (checkHB == 'ROUTES')) {
                                    window.location.href = userhref;
                                }
                                else if (checkHB == 'SINGLE_TRACK') {
                                     window.location.href = userhref;
                                }
                                else if (checkHB == 'MULTITRACK') {
                                     window.location.href = userhref;
                                }
                               else{
                                window.location.href = userhref;
                            }
                        }

                    }else{
                       window.location.href ='track';
                    }
                    
             
        } else{
            console.log('Empty getReportsList API ....');
        }

   }

    }).error(function(){

        console.log('error getReportsList');
     });

           

function getTodayDate() {
  var date = new Date();
  return ("0" + (date.getDate())).slice(-2)+'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+date.getFullYear();
};
function setDateTimeSessionValues(){

        var dateObj    =  new Date();
        var fromNowTS  =  new Date(dateObj.setDate(dateObj.getDate()));
        var fromDate   =  getTodayDate();
        var fromTime   =  '12:00 AM';
        var toTime     =  formatAMPM(fromNowTS.getTime());
        localStorage.setItem('fromDate', fromDate);
        localStorage.setItem('fromTime', fromTime);
        localStorage.setItem('toDate', fromDate);
        localStorage.setItem('toTime', toTime);
        //alert(localStorage.getItem('toTime'));

}
function formatAMPM(date) {
      var date = new Date(date);
      var hours = date.getHours();
      var minutes = date.getMinutes();
      var ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
      return strTime;
  }

</script>