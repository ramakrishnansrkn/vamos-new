<head>
</head>
<table>
  <tr>
    <th>No</th>
    <th>Dealer Name</th>
    <th>Mobile Number</th>
    <th>WebSite</th>
    <th>E-Mail Id</th>
    <th>Mobile App Key</th>
    <th>Map Key / API Key</th>
    <th>Address Key</th>
    <th>Notification Key</th>
</tr>
<tbody>
@foreach($dealerlist as $key => $value)
<tr>
    <td >{{++$key }}</td>
    <td >{{ $value }}</td>    
    <td>{{ array_get($userGroupsArr, $value) }}</td>
    <td>{{ array_get($dealerWeb, $value) }}</td>
    <td>{{ array_get($mailid, $value) }}</td>
    <td>{{ array_get($mobilekey, $value) }}</td>
    <td>{{ array_get($mapkey, $value) }}</td>
    <td>{{ array_get($addressKey, $value) }}</td>
    <td>{{ array_get($notikey, $value) }}</td>    
</tr>

@endforeach
</tbody>
</table>

