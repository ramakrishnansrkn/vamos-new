<!-- app/views/nerds/index.blade.php -->

<!DOCTYPE html>
<html>
<head>
	<title>VAMO Systems</title>
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
	<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
	<style type="text/css">
		.navbar {
   			 border: 1px solid #080808;
		}
		.nav > li > a:hover{
		    background-color:#005db4;
		}
		.bg-primary {
 		   color: #fff;
   		 background-color: #1476ca;
		}
		.containers{
			padding-top: 3px;
		    padding-right: 1px;
		    padding-left: 1px;
		    margin-right: auto;
		    margin-left: auto;
		}
		@media (min-width: 768px){
			.containers {
    			width: 750px;
			}
		}
		@media (min-width: 992px){
			.containers {
			    width: 970px;
			}
		}
		@media (min-width: 1250px){
			.containers {
			    width: 1240px;
			}
		}

	</style>
</head>
<body>
	
<div class="containers">
<div>
<nav class="navbar navbar-dark bg-primary" role="navigation">
	<div class="navbar-header">
		<a class="navbar-brand" href="{{ URL::to('vdmFranchises') }}" style="color: white;
">VAMO SYSTEMS</a>
		

	</div>
	<div class="pull-right" style=" margin-top: 12px;margin-right: 55px;">
		<a href="{{ URL::to('logout/') }}" style="color: white;text-decoration: none;"> {{Auth::user ()->username }} <span class="glyphicon glyphicon-log-out" style="cursor: pointer;"></span></a>
	</div>	

	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('vdmFranchises/fransearch') }}" style="color: white;
">Switch Login</a></li>
		<li><a href="{{ URL::to('vdmFranchises/users') }}" style="color: white;
">Switch Login users</a></li>
		<li><a href="{{ URL::to('vdmFranchises') }}" style="color: white;
">View All Franchises</a></li>
		<li><a href="{{ URL::to('vdmFranchises/create') }}" style="color: white;
">Add a Franchise</a>
		<li><a href="{{ URL::to('ipAddressManager') }}" style="color: white;
">IPAddressManager</a>
		<li><a href="{{ URL::to('vdmFranchises/buyAddress') }}" style="color: white;
">Address control</a>
 <li><a href="{{ URL::to('fransOnboard') }}" style="color: white;
">Frans Onboard</a></li>
<li><a href="{{ URL::to('audit','AuditFrans') }}" style="color: white;
">Audit Trial - Franchises</a></li>
<!--<li><a href="{{ URL::to('vdmFranchises/expiry') }}" style="color: white;
">UpdateExpiryDate</a></li>-->
<li><a  href="{{ URL::to('showLnSugg') }}" style="color: white;
">Language Suggestion</a></li>
<li data-toggle="modal" data-target="#favoritesModal" style="color: white;cursor: pointer;">
  <a style="color: white;cursor: pointer;">Upload Release Notes</a>
</li>

<!--Timezone and apn updates -->
	<div class="pull-right" style=" margin-top: 12px;margin-right: 35px;">
   @if($timezoneKey != 'True')
		<a href="{{ URL::to('vdmFranchise/updatetimezone') }}" style="color: white;"> Update Timezone </a>	&nbsp;
    @endif	
    @if($apnKey != 'True') 
   <a href="{{ URL::to('vdmFranchise/updateapn') }}" style="color: white;"> Update APN </a>
   @endif	 
	</div>


   <!-- for audit -->
		<!-- <li style="padding-top: 10px;">
		   <div class="dropdown">
		  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Audit
		  <span class="caret"></span></button>
		  <ul class="dropdown-menu">
		    <li ><a href="{{ URL::to('audit','AuditFrans') }}" >Franchies</a></li>
		    <li><a href="{{ URL::to('audit','AuditDealer') }}">Dealer</a></li>
		    <li><a href="{{ URL::to('audit','AuditUser') }}">User</a></li>
		    <li><a href="{{ URL::to('audit','AuditVehicle') }}">Vehicle</a></li>
		    <li><a href="{{ URL::to('audit','RenewalDetails') }}">Renewal Details</a></li>
		  </ul>
		</div>
	 </li>	 -->
	 
<!-- <li><a href="{{ URL::to('vdmFranchises/licenceType/convertion') }}" style="color: white;
">Licence-Type</a></li> -->
		<!-- <li><a href="{{ URL::to('logout/') }}" style="color: white;
">Logout User: {{Auth::user ()->username }}</a></li>	 -->
		<!-- <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form> -->

	</ul>

</nav>
<div class="modal fade" id="favoritesModal" 
     tabindex="-1" role="dialog" 
     aria-labelledby="favoritesModalLabel">
  <div class="modal-dialog">
    <div class="modal-content" style="width: 350px !important;margin-left: -550 !important;margin-top: 100 !important;">
      <div class="modal-header">
        <button type="button" class="close" 
          data-dismiss="modal" 
          aria-label="Close">
          <span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" 
        id="favoritesModalLabel" style="color: black;">Select PDF File to Upload</h4>
      </div>
      <div class="modal-body" style="padding: 50px !important;">
        <body>
  {{ Form::open(array('url'=>'form-submit','files'=>true)) }}
  
 <!--  {{ Form::label('file','File',array('id'=>'','class'=>'')) }} -->
  <!-- {{ Form::file('file','',array('id'=>'','class'=>'')) }} -->
  <input type="file" name="file" id="file" class="form-control" accept=".pdf">
  <br/>
  <!-- submit buttons -->
  <span class="pull-right">
  	<button type="submit" class="btn btn-primary">
            Save
          </button> 
  <!-- {{ Form::submit('Save') }} -->
  <button type="Reset" class="btn btn-warning">
            Reset
          </button> 
          <button type="button" 
           class="btn btn-default" 
           data-dismiss="modal">Close</button>
  <!-- reset buttons -->
  <!-- {{ Form::reset('Reset') }} -->
</span>
  
  {{ Form::close() }}
 </body>
      </div>
      <!-- <div class="modal-footer">
        
        <span class="pull-right">
          <button type="button" class="btn btn-primary">
            save
          </button> 
          <button type="button" class="btn btn-primary">
            Add to Favorites
          </button>
        </span>
      </div> -->
    </div>
  </div>
</div>
@if (Session::has('message'))
	<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
@if (Session::has('errormessage'))
	<div class="alert alert-danger">{{ Session::get('errormessage') }}</div>
@endif
</div>


@yield('mainContent')

<footer class="row">
		@include('includes.footer')
	</footer>
</div>
</body>
</html>