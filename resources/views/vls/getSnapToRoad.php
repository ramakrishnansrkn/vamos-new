<?php
Log::info("Snap to road....");

if (! Auth::check ()) {
	return Redirect::to ( 'login' );
}
   $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=-35.27801,149.12958&sensor=true';
  $url=htmlspecialchars_decode($url);
   log::info( 'Routing to backed  :' . $url );


  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
    // Include header in result? (0 = yes, 1 = no)
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_TIMEOUT, 20);
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
  $response = curl_exec($ch);
	 $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE); 
  curl_close($ch);
log::info( 'curl status  :' .$httpCode  );

echo $response;
?>


