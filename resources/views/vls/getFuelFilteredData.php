<?php
Log::info("Get fuel filtered data....");

$input = Input::all();

$redis = Redis::connection ();
$ipaddress = $redis->get('ipaddress');
//$ipaddress ='188.166.244.126';

if (! Auth::check ()) {
  return Redirect::to ( 'login' );
}

$username = Auth::user ()->username;

//TODO - this hardcoding should be removed

foreach ($input as $key => $value) {
    if($key=='deviceTime'){
      $deviceTime = $value;
    }
   
}
$userId       = Input::get('userId');
$vehicleId       = Input::get('vehicleId');
$recordStatus       = Input::get('recordStatus');
$fromTimeUtc       = Input::get('fromTimeUtc');
$toTimeUtc       = Input::get('toTimeUtc');


 // $parameters="{$parameters}";
  log::info('ip :' . $ipaddress);

    $url = 'http://' .$ipaddress .':9000/getFuelFilteredData';
    $url=htmlspecialchars_decode($url);
    

    // Make Post Fields Array
    $data = [
        "userId"=>$userId,
        "vehicleId"=>$vehicleId,
        "deviceTime" => $deviceTime,
        "recordStatus" => $recordStatus,
        "fromTimeUtc" => $fromTimeUtc,
        "toTimeUtc" => $toTimeUtc
    ];

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30000,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($data),
        CURLOPT_HTTPHEADER => array(
            // Set here requred headers
            "accept: */*",
            "accept-language: en-US,en;q=0.8",
            "content-type: application/json",
        ),
    ));
    log::info( 'Routing to backed  :' . $url );

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        echo $response;
    }

?>


