<?php
Log::info("postVehicleData....");

$input = Input::all();

$redis = Redis::connection ();
$ipaddress = $redis->get('ipaddress');
//$ipaddress ='188.166.244.126';

if (! Auth::check ()) {
  return Redirect::to ( 'login' );
}

$username = Auth::user ()->username;

//TODO - this hardcoding should be removed

foreach ($input as $key => $value) {
    if($key=='shortName'){
      $shortName = $value;
    }else if($key=='odoDistance'){
      $odoDistance = $value;
    }
    else if($key=='userId'){
      $userId = $value;
    }
    else if($key=='overSpeedLimit'){
      $overSpeedLimit = $value;
    }
     else if($key=='driverName'){
      $driverName = $value;
    }
    else if($key=='driverMobile'){
      $driverMobile = $value;
    }
    else if($key=='regNo'){
      $regNo = $value;
    }
    else if($key=='vehicleType'){
      $vehicleType = $value;
    }
    else if($key=='routeName'){
      $routeName = $value;
    }
     else if($key=='vehicleModel'){
      $vehicleModel = $value;
    }
  
    else if($key=='secondaryEngineHours'){
      $secondaryEngineHours = $value;
    }
    else if($key=='freezedKm'){
      $freezedKm = $value;
    }
    else if($key=='freezedKmDate'){
      $freezedKmDate = $value;
    }
    else if($key=='shiftNames'){
      $shiftNames = $value;
    }
    else if($key=='vehicleId'){
      $vehicleId = $value;
    }
    else if($key=='userIpAddress'){
      $userIpAddress = $value;
    }
    else {
      $shiftDate = $value;
    }
   
   
}

 // $parameters="{$parameters}";
  log::info('ip :' . $ipaddress);

    $url = 'http://' .$ipaddress .':9000/v2/postVehicleData';
    $url=htmlspecialchars_decode($url);
    log::info( 'Routing to backed  :' . $url );

    // Make Post Fields Array
    $data = [
    'userId'=>$userId,
    'vehicleId'=>$vehicleId,
    "shortName"=>$shortName,
    "odoDistance"=> $odoDistance,
    "overSpeedLimit"=>$overSpeedLimit,
    'driverName'=>$driverName,
    'driverMobile' => $driverMobile,
    'regNo'=> $regNo,
    'vehicleType'=> $vehicleType,
    'routeName'=>$routeName,
    'vehicleModel'=>$vehicleModel,
    'userIpAddress'=>$userIpAddress,
    'secondaryEngineHours'=>$secondaryEngineHours,
    'freezedKm'=>$freezedKm,
    'freezedKmDate'=>$freezedKmDate,
    'shiftNames'=>$shiftNames,
    'shiftDate'=>$shiftDate
    ];

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30000,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($data),
        CURLOPT_HTTPHEADER => array(
            // Set here requred headers
            "accept: */*",
            "accept-language: en-US,en;q=0.8",
            "content-type: application/json",
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        echo $response;
    }

?>


