<div style="box-shadow:0px 8px 17px rgba(0, 0, 0, 0.2)  !important;">    
    <nav class="menus" >
        <ul id="menuBarList">
            <li class="cl-effect-5" style="width: 210px;">

                <!-- <a><span  data-hover="Tracking">Tracking</span></a><span class="glyphicons glyphicons-road"></span> -->
                <a><span  data-hover="<?php echo Lang::get('content.tracking'); ?>">&nbsp;&nbsp;<div class="glyphicon glyphicon-map-marker"></div>&nbsp;<?php echo Lang::get('content.tracking'); ?></span></a>
                <ul id="trackList">
                    <li class="removeli" id="singleTrackRl"><a id="singleTrack" style="left: 7px;"><img src="assets/menu/single_track.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.single_track'); ?></div></a></li>
                    <li class="removeli" id="multiTrackRl"><a id="multiTrack" style="left: 7px;"><img src="assets/menu/multi_track.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.multi_track'); ?></div></a></li>
                    <li class="removeli" id="historyRl"><a id="history" href="" style="left: 7px;" target="_blank"><img src="assets/menu/history.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.history'); ?></div></a></li>
                    <!-- <li class="removeli" id="multiTrackRlNew"><a id="multiTrackNew" href=""><img src="assets/menu/multi_track.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px">MultiTrack New</div></a></li> -->
                    <li class="removeli removeli_2" id="routesRl"><a id="routes" href="../public/track?rt=routes&maps=newreplay" style="left: 7px;" target="_blank"><img src="assets/menu/history.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.routes'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('routes')"></div></a></li>
                <li class="removeli removeli_2" id="addsiteRl"><!-- <a id="addsites">Admin</a>
                    <div  style="padding-left: 10px"> -->
                        <a href="../public/password_check" id="addsites" style="left: 7px;"><img src="assets/menu/Addsite_Geofence.png" width="20px" height="20px" style="float:left;margin-right: 5px;" /><div style=" padding:4px"><?php echo Lang::get('content.addsite_geofence'); ?></div><div class="glyphicon glyphicon-new-window" style="margin-left: 15px;" onclick="openNewWindow('addsites')"></div></a>
                        <!-- </div> -->
                    </li>
                    <!-- <li class="removeli" id="singleTrackRls"><a id="landNew" href="" target="_blank">New Landing Page</a></li> -->
                    <!--  <li class="removeli"><a id="dashNew" href="../public/dashNew" target="_blank">New DashBoard</a></li> -->
                    <!-- <li id="removeli"><a href="../public/groupEdit">Edit Group</a></li> -->
                </ul>
            </li>
            <li class="cl-effect-5" style="margin-left: -60px;" id="reportMenu">
                <!-- <a><span data-hover="Reports">Reports</span></a> -->
                <a><span  data-hover="<?php echo Lang::get('content.reports'); ?>">&nbsp;&nbsp;<div class="glyphicon glyphicon-list-alt"></div>&nbsp;<?php echo Lang::get('content.reports'); ?></span></a>
                <ul id="reportList">
                    <li class="removeli" id="curStatRl"><a id="dashboard" href="../public/reports?ind=1" style="left: 7px;"><img src="assets/menu/Dashboard.png" width="20px" height="20px" style="float:left;" />    <div style="float:left; padding:4px"><?php echo Lang::get('content.dashboard'); ?></div> 
                        <div class="glyphicon glyphicon-new-window" onclick="openNewWindow('dashboard')"></div> </a></li>
                        <li class="removeli" id="vehConsRl"><a id="assetLabel" href="../public/reports?ind=2" style="left: 7px;"><img src="assets/menu/Vehicles_Consolidated.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.vehicles_consolidated'); ?> </div> <div class="glyphicon glyphicon-new-window" onclick="openNewWindow('assetLabel')"></div>
                        </a></li>
                        <li class="removeli" id="conSiteRl"><a id="conSiteLoc" href="../public/reports?ind=3" style="left: 7px;"><img src="assets/menu/Consolidated.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.cons_location'); ?></div> <div class="glyphicon glyphicon-new-window" onclick="openNewWindow('conSiteLoc')"></div></a></li>
                        <!-- <li class="removeli"><a href="../public/reports?ind=4"> <img src="assets/menu/Site.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px">Site Location</div></a></li> -->
                        <li class="removeli" id="conOvrRl"><a id="conOvr" href="../public/reports?ind=4" style="left: 7px;"><img src="assets/menu/Consolidated_OverSpeed.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.cons_overspeed'); ?></div> <div class="glyphicon glyphicon-new-window" onclick="openNewWindow('conOvr')"></div> </a></li>
                        <li class="removeli" id="stopReportRl"><a id="stopReport" href="" style="left: 7px;"><img src="assets/menu/Consolidated_Stoppage.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.cons_stoppage'); ?></div>
                            <div class="glyphicon glyphicon-new-window" onclick="openNewWindow('stopReport')"></div></a></li>
                            <li class="removeli" id="conSiteLocRl"><a id="ConSiteLocReport" href="" style="left: 7px;"><img src="assets/menu/Consolidated_Site_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.cons_site_report'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('ConSiteLocReport')"></div></a></li>
                            <li class="removeli" id="tollReportRl"><a id="tollReport" href="" style="left: 7px;"><img src="assets/menu/Tollgate_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.tollgate_report'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('tollReport')"></div></a></li>
                            <li class="removeli" id="nonMovingReportRl"><a id="nonMovingReport" href="" style="left: 7px;"><img src="assets/menu/Non_Moving_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.non_moving_vehicles'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('nonMovingReport')"></div></a></li>
                            <li class="removeli" id="travelSumReportRl"><a id="travelSumReport" href="" style="left: 7px;"><img src="assets/menu/Travel_Summary_list.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.travell_summary'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('travelSumReport')"></div></a></li>
                            
                            <li class="removeli" id="conPriEngReportRl"><a id="conPriEngReport" href="" style="left: 7px;"><img src="assets/menu/Consolidated_Primary_Engine_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.cons_pri_report'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('conPriEngReport')"></div></a></li>
                            <li class="removeli" id="empAtnReportRl"><a id="empAtnReport" href="" style="left: 7px;"><img src="assets/menu/Employee_Attendance_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.employee_attendance'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('empAtnReport')"></div></a></li>
                            <li class="removeli" id="SchoolSmsReportRl"><a id="SchoolSmsReport" href="" style="left: 7px;"><img src="assets/menu/School_SMS_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.school_sms_report'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('SchoolSmsReport')"></div></a></li>
                            <li class="removeli" id="siteAlertReportRl"><a id="siteAlertReport" href="" style="left: 7px;"><img src="assets/menu/Site_Stpoppage_Alert_Rrport.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.site_alert'); ?> </div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('siteAlertReport')"></div></a></li>
                            <li class="removeli" id="conRfidReportRl"><a id="conRfidReport" href="" style="left: 7px;"><img src="assets/menu/Consolidated_Rfid_Tag_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.cons_rfid_report'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('conRfidReport')"></div></a></li>
                            <li class="removeli" id="conAlarmReportRl"><a id="conAlarmReport" href="" style="left: 7px;"><img src="assets/menu/Consolidated_Alarm_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.cons_alarm_report'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('conAlarmReport')"></div></a></li>
                            <li class="removeli" id="conSecEngRl"><a id="conSecEng" href="" style="left: 7px;"><img src="assets/menu/secondary_Engine_On.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.consolidated'); ?> <?php echo Lang::get('content.secondary_eng_on'); ?> </div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('conSecEng')"></div></a></li>


                <!-- <li> <a href="#"> Advanced &nbsp; </a>
                     <div style="padding-left: 10px">
                        <table style="width: 300px; height: 100px;">
                            <tr>
                                <td><a id="movement" href="" class="lowcase"> Movement </a></td>
                                <td><a id="overspeed" href="" class="lowcase"> OverSpeed </a></td>
                                <td><a id="parked" href="" class="lowcase"> Parked </a></td>
                            </tr>
                            <tr>
                                <td><a id="idle" href="" class="lowcase"> Idle </a></td>
                                <td><a id="event" href="" class="lowcase"> Event </a></td>
                                <td><a id="site" href="" class="lowcase"> Site </a></td>
                            </tr>
                            <tr>
                                <td><a id="load" href="" class="lowcase"> Load </a></td>
                                <td><a id="fuel" href="" class="lowcase"> Fuel </a></td>
                                <td><a id="ignition" href="" class="lowcase"> Ignition </a></td>
                            </tr>
                            <tr>
                                <td><a id="trip" href="" class="lowcase"> Trip Time </a></td>
                                <td><a id="tripkms" href="" class="lowcase"> Trip Summary</a></td>
                                <td><a id="temp" href="" class="lowcase"> Temperature</a></td>
                            </tr>
                            <tr>
                                <td><a id="alarm" href="" class="lowcase">Alarm</a></td>
                            </tr>
                        </table>

                    </div>
                </li> -->
            </ul>
        </li>
        <li class="cl-effect-5" id="analyticsMenu" style="margin-left: -60px;">
            <!-- <a><span data-hover="analytics">analytics</span></a> -->
            <a><span  data-hover="<?php echo Lang::get('content.analytics'); ?>">&nbsp;&nbsp;<div class="glyphicon glyphicon-screenshot"></div>&nbsp;<?php echo Lang::get('content.analytics'); ?></span></a>
            <ul id="analyticsList">
                <li class="removeli" id="movementRl"><a id="movement" href="" style="left: 7px;"><img src="assets/menu/Movement.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.movement'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('movement')"></div></a></li>
                <li class="removeli" id="overspeedRl"><a id="overspeed" href="" style="left: 7px;"> <img src="assets/menu/OverSpeed.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.overspeed'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('overspeed')"></div></a></li>
                <li class="removeli" id="parkedRl"><a id="parked" href="" style="left: 7px;"> <img src="assets/menu/Parked.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.parked'); ?></div> <div class="glyphicon glyphicon-new-window" onclick="openNewWindow('parked')"></div></a></li>
                <li class="removeli" id="idleRl"><a id="idle" href="" style="left: 7px;"> <img src="assets/menu/Idle.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.idle'); ?> </div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('idle')"></div></a></li>
                <li class="removeli" id="ignitionRl"><a id="ignition" href="" style="left: 7px;"><img src="assets/menu/Ignition.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.ign'); ?> </div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('ignition')"></div></a></li>
                <!-- <li class="removeli" id="fuelRl"><a id="fuel" href=""><img src="assets/menu/Fuel.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.fuel'); ?> </div></a></li> -->
               <!-- <li class="removeli" id="eventRl"><a id="event" href=""><img src="assets/menu/Event.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> Event </div></a></li>
                    <li class="removeli" id="siteRl"><a id="site" href=""><img src="assets/menu/Site.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> Site </div></a></li>
                    <li class="removeli" id="multiSiteRl"><a id="multiSite" href=""><img src="assets/menu/Site.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> Site (Mulitple Site)</div></a></li> -->
                    
                    <li class="removeli" id="tripSiteRl"><a id="tripSite" href="" style="left: 7px;"><img src="assets/menu/Site.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.site_trip'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('tripSite')"></div></a></li>
                    <!-- <li class="removeli" id="tripRl"><a id="trip" href="" style="left: 7px;"><img src="assets/menu/Trip_Time.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.trip_time'); ?></div> </a></li> -->
                    <li class="removeli" id="tripkmsRl"><a id="tripkms" href="" style="left: 7px;"><img src="assets/menu/Trip_Summary.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.trip_summary'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('tripkms')"></div></a></li>
                    <li class="removeli" id="alarmRl"><a id="alarm" href="" style="left: 7px;"><img src="assets/menu/Alarm.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.alarm'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('alarm')"></div></a></li>
                    <li class="removeli" id="stoppageRl"><a id="stoppage" href="" style="left: 7px;"><img src="assets/menu/Stoppage.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.stoppage'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('stoppage')"></div></a></li>
                    <li class="removeli" id="noDataRl"><a id="noData" href="" style="left: 7px;"><img src="assets/menu/No_Data.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.n_data'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('noData')"></div></a></li>
                    <li class="removeli" id="routeDeviationRl"><a id="routeDev" href="" style="left: 7px;"><img src="assets/menu/Route_Deviation.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.route_deviation'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('routeDev')"></div></a></li>
                    <li class="removeli" id="triplossRl"><a id="triploss" href=""><img src="assets/menu/Site.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> LOSS TIME / DAY  </div></a></li>
                </ul>
            </li>
            <li class="cl-effect-5" id="statisticsMenu" style="margin-left: -60px;">
                <!-- <a><span data-hover="Statistics">Statistics</span></a> -->
                <a><span  data-hover="<?php echo Lang::get('content.statistics'); ?>">&nbsp;&nbsp;<div class="glyphicon glyphicon-stats"></div>&nbsp;<?php echo Lang::get('content.statistics'); ?></span></a>
                <ul id="statisticList">
                    <li class="removeli" id="dailyRl"><a id="daily" href="../public/statistics?ind=1" style="left: 7px;"><img src="assets/menu/Vehiclewise_Performance.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.vehiclewise_performance'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('daily')"></div></a></li>
                    <!-- <li class="removeli" id="poiRl"><a id="pod" href="../public/statistics?ind=2" style="left: 7px;"><img src="assets/menu/POI.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.poi'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('pod')"></div></a></li> -->
                    <!-- <li class="removeli" id="consolRl"><a id="consol" href="../public/statistics?ind=3" style="left: 7px;"><img src="assets/menu/Consolidated.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.consolidated'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('consol')"></div></a></li> -->
                    
                    <li class="removeli" id="monDistRl"><a id="monDist" href="../public/statistics?ind=5" style="left: 7px;"><img src="assets/menu/Monthly_Dist.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.monthly_dist'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('monDist')"></div></a></li>
                    <li class="removeli" id="freezeKmRl"><a id="freezeKm" href="../public/statistics?ind=7" style="left: 7px;"><img src="assets/menu/Monthly_Dist.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.freezekm'); ?> <?php echo Lang::get('content.report'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('freezeKm')"></div></a></li>

                </ul>
            </li>
            <li class="cl-effect-5" id="sensMenu" style="margin-left: -60px;">
                <a><span  data-hover="<?php echo Lang::get('content.sensor'); ?>">&nbsp;&nbsp;<div class="glyphicon glyphicon-list"></div>&nbsp;<?php echo Lang::get('content.sensor'); ?></span></a>
                <!-- <a><span data-hover="sensor">sensor</span></a> -->
                <ul id="sensorList">
                    <li class="removeli" id="acRl"><a id="ac" href="" style="left: 7px;"> <img src="assets/menu/AC .png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.ac'); ?></div> <div class="glyphicon glyphicon-new-window" onclick="openNewWindow('ac')"></div></a></li>
                    <li class="removeli" id="idleWasteRl"><a id="idleWaste" href="" style="left: 7px;"><img src="assets/menu/Idle_Wastage .png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"> <?php echo Lang::get('content.idle_wastage'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('idleWaste')"></div></a></li>
                    <li class="removeli" id="prmEngineOnRl"><a id="prmEngineOn" href="" style="left: 7px;"><img src="assets/menu/Primary_Engine_On.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.engine_on'); ?> </div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('prmEngineOn')"></div></a></li>
                    <!-- <li class="removeli" id="doorSensorRl"><a id="doorSensor" href="" style="left: 7px;"><img src="assets/menu/secondary_Engine_On.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px">
                        <?php echo Lang::get('content.door_sensor'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('doorSensor')"></div> </a></li> -->
                        <li class="removeli" id="engineOnRl"><a id="engineOn" href="" style="left: 7px;"><img src="assets/menu/secondary_Engine_On.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.sec_eng_on'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('engineOn')"></div></a></li>
                        <!--<li class="removeli" id="loadRl"><a id="load" href=""> Load </a></li>-->
                        <li class="removeli" id="tempRl"><a id="temp" href="" style="left: 7px;"> <img src="assets/menu/Temperature.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.temperature'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('temp')"></div></a></li>
                        <li class="removeli" id="tempdevRl"><a id="tempdev" href="" style="left: 7px;"> <img src="assets/menu/Temperature_Deviation.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.temp_deviation'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('tempdev')"></div></a></li>
                        <!--<li class="removeli" id="distTimeRl"><a href="../public/fuel">Distance &amp; Time</a></li>-->
                        <!--  <li class="removeli" id="fuelNewRl"><a id="fuelNew" href=""><img src="assets/menu/Fuel.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.fuel_speed'); ?></div></a></li> -->
                        <!--<li class="removeli" id="fuelFillRl"><a href="../public/fuel?tab=fuelfill">Fuel Fill</a></li>-->
                        <!-- <li class="removeli" id="fuelFillRl"><a id="fuelFill" href=""><img src="assets/menu/No_Data" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.fuel_fill'); ?></div></a></li> -->
                        
                        <!-- <li class="removeli" id="fuelTheftRl"><a  id="fuelTheft" href="" ><img src="assets/menu/No_Data" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.fuel_theft'); ?></div></a></li> -->
                        
                    <!-- <li class="removeli" id="fuelMileageRl"><a  id="fuelMileage" href="" ><img src="assets/menu/No_Data" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.fuel_mileage'); ?></div></a></li>
                        <li class="removeli" id="fuelMileageNewRl"><a  id="fuelMileageNew" href="" ><img src="assets/menu/Fuel.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.fuel_mileage_new'); ?></div></a></li> -->

                        <li class="removeli" id="rfidRl"><a id="rfid" href="" style="left: 7px;"><img src="assets/menu/Rfid.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.rfid'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('rfid')"></div></a></li>
                        <li class="removeli" id="rfidNewRl"><a id="rfidNew" href="" style="left: 7px;"><img src="assets/menu/Rfid_Tag.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.rfid_tag'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('rfidNew')"></div></a></li>
                        <li class="removeli" id="daywise_truck_posRl"><a id="daywise_truck_pos" href="" style="left: 7px;"><img src="assets/menu/Camera.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.daywise_truck_pos'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('daywise_truck_pos')"></div></a></li>
                        <div id="protocolMenu">
                            <li id="protocolli"><a href="#" style="left: 7px;">
                             <img src="assets/menu/Protocol_Report.png" width="20px" height="20px" style="float:left;" />
                             <div style="float:left; padding:4px"><?php echo Lang::get('content.protocol'); ?> <?php echo Lang::get('content.report'); ?></div></span><span></a>
                                <ul id="protocolList" style="background: none !important">
                                   <li class="removeli" id="fuelProtocolRl" onmouseover="newWindowShow(this,'fuelProNew')"><a  id="fuelProtocol" href="" style="left: 7px;"><img src="assets/menu/Protocol_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.device_data'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('fuelProtocol')" id="fuelProNew"></div></a></li>
                                   <li class="removeli" id="dataConsumptionRl" onmouseover="newWindowShow(this,'dataConNew')"><a  id="dataConsumption" href="" style="left: 7px;"><img src="assets/menu/Protocol_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.data_consumption'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('dataConsumption')" id="dataConNew"></div></a></li>
                               </ul>

                           </li>

                       </div>
                       <!-- <li class="removeli" id="cameraRl"><a id="camera" href="" style="left: 7px;"><img src="assets/menu/Camera.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.camera'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('camera')"></div></a></li> -->
                   </ul>
               </li>
        <!-- <li class="cl-effect-5">
           
            <a><span  data-hover="Fuel"><div class="glyphicon glyphicon-tint"></div>&nbsp;Fuel</span></a>
            <ul>
                <li><a href="../public/fuel">Distance &amp; Time</a></li>
                <li><a href="../public/fuel?tab=fuelfill">Fuel Fill</a></li>
            </ul>
        </li> -->
        <li class="cl-effect-5" style="margin-left: -60px;width: 210px;" id="fuelMenu">
            <a><span  data-hover="<?php echo Lang::get('content.fuel'); ?>">&nbsp;&nbsp;<img src="assets/imgs/gas-station.svg" width="15px" height="15px" style="float:left;" />&nbsp;<?php echo Lang::get('content.fuel'); ?></span></a>
            <!-- <a><span  data-hover="Performance">Performance</span></a> -->
            <ul id="fuelList">
                <li class="removeli" id="fuelConReportRl"><a id="fuelConReport" href="" style="left: 7px;"><img src="assets/menu/Consolidated_Fuel_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.cons_fuel_report'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('fuelConReport')"></div></a></li>
                <!-- <li class="removeli" id="fuelConTankRl"><a id="fuelConTank" href="" style="left: 7px;"><img src="assets/menu/Consolidated_Fuel_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.tank_based'); ?> <?php echo Lang::get('content.cons_fuel'); ?></div></a></li> -->
                <li class="removeli" id="conFuelTheftRl"><a id="conFuelTheft" href="" style="left: 7px;"><img src="assets/menu/Fuel.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.consolidated'); ?>  <?php echo Lang::get('content.fuel_theft'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('conFuelTheft')"></div></a></li>
                <li class="removeli" id="exfuelRl"><a id="exfuel" href="../public/statistics?ind=4" style="left: 7px;"><img src="assets/menu/Excutive_Fuel.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.exe_fuel'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('exfuel')"></div></a></li>
                <li class="removeli" id="monDistFuelRl"><a id="monDistFuel" href="../public/statistics?ind=6" style="left: 7px;"><img src="assets/menu/Monthly_Dist_and_Fuel.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.monthly_dist_fuel'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('monDistFuel')"></div></a></li>
                <li class="removeli" id="fuelMachineRl"><a id="fuelMachine" href="" style="left: 7px;"><img src="assets/menu/Fuel Analytics.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.fuel_analytics'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('fuelMachine')"></div></a></li>
                <li class="removeli" id="fuelDispenserRl"><a id="fuelDispenser" href="" style="left: 7px;"><img src="assets/menu/Fuel Analytics.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.dispenser_report'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('fuelDispenser')"></div></a></li>
                
                <li class="removeli" id="siteTripFuelRl"><a id="siteTripFuel" href="" style="left: 7px;"><img src="assets/menu/Fuel Analytics.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.geofence_fuel'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('siteTripFuel')"></div></a></li>
                <li class="removeli" id="fuelFillV2Rl"><a id="fuelFillV2" href="../public/fuelFillV2?tn=fuelfillV2" style="left: 7px;"><img src="assets/menu/Fuel Fill_BETA.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.fuelfill_BETA'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('fuelFillV2')"></div></a></li>
                <li class="removeli" id="fuelRawRl"><a  id="fuelRaw" href="" style="left: 7px;"><img src="assets/menu/Fuel Raw Data.png" width="20px" height="20px" style="float:left;margin-right: 5px;" /><div style="padding:4px"><?php echo Lang::get('content.fuel_raw_data'); ?> (<?php echo Lang::get('content.graph'); ?>)</div><div class="glyphicon glyphicon-new-window"
                 onclick="openNewWindow('fuelRaw')"></div></a></li>
                 <li class="removeli" id="fuelConVehiRl"><a  id="fuelConVehicle" href="" style="left: 7px;"> <img src="assets/menu/Vehicle_Wise_Fuel_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.vehicle_wise_report'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('fuelConVehicle')"></div></a></li>
                 <li class="removeli" id="fuelConTimeRl"><a  id="fuelConTime" href="" style="left: 7px;"><img src="assets/menu/Vehicle_Wise_Intraday_Fuel_Report.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.vehicle_wise_intraday'); ?> </div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('fuelConTime')"></div></a></li>
                 <!-- <li class="removeli" id="fuelPerformanceRl"><a  id="fuelPerfor" href="" style="left: 7px;"><img src="assets/menu/Fuel_Performance_Reports.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.fuel_performance'); ?></div></a></li> -->
                 <li class="removeli" id="fuelNotificationRl"><a  id="fuelNotification" href="" style="left: 7px;"><img src="assets/menu/Fuel_Performance_Reports.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.fuel_notification'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('fuelNotification')"></div></a></li>

             </ul>
         </li>
         <li class="cl-effect-5" id="performanceMenu" style="margin-left: -71px;">
            <a><span  data-hover="<?php echo Lang::get('content.performance'); ?>">&nbsp;&nbsp;<div class="glyphicon glyphicon-stats"></div>&nbsp;<?php echo Lang::get('content.performance'); ?></span></a>
            <!-- <a><span  data-hover="Performance">Performance</span></a> -->
            <ul id="performanceList">
                <li class="removeli" id="dailyPerfRl"><a id="dailyPer" href="../public/performance?tab=daily" style="left: 7px;"><img src="assets/menu/Daily_Performance.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.daily_per'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('dailyPer')"></div></a></li>
                <li class="removeli" id="monPerfRl"><a id="monPerf" href="../public/performance" style="left: 7px;"><img src="assets/menu/Monthly_Performance.png" width="20px" height="20px" style="float:left;" /><div style="float:left; padding:4px"><?php echo Lang::get('content.monthly_per'); ?></div><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('monPerf')"></div></a></li>
            </ul>
        </li>
        
        <li id="userId">
            <span class="glyphicon glyphicon-user"><span id="valueUser" style="font-size:12px; font-weight: bold; font-family: Helvetica"></span></span>  
            <ul id="userList">          
                <!-- <li class="removeli removeli_2" id="payDetRl"><a href="../public/payDetails" id="payDet"><?php echo Lang::get('content.payment_details'); ?> <div class="glyphicon glyphicon-new-window" onclick="openNewWindow('payDet')"></div> </a></li> -->
                <li class="removeli removeli_2" id="editGrpRl"><a href="../public/groupEdit" id="editGrp"><?php echo Lang::get('content.edit_group'); ?> <div class="glyphicon glyphicon-new-window" onclick="openNewWindow('editGrp')"></div>  </a></li>
                <li class="removeli removeli_2" id="editNotRl"><a id="editNot" href="../public/passWd?userlevel=notify"><?php echo Lang::get('content.edit_notification'); ?><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('editNot')"></div></a></li>
                <!-- <li class="removeli removeli_2" id="editUsNotRl"><a id="editUsNot" href="../public/userNotify"><?php echo Lang::get('content.edit_user_notification'); ?><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('editUsNot')"></div></a></li> -->
                <li class="removeli removeli_2" id="resetRl"><a id="reset" href="../public/passWd?userlevel=reset"><?php echo Lang::get('content.reset_password'); ?><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('reset')"></div></a></li>
                <li class="removeli removeli_2" id="editBusRl"><a id="editBus" href="../public/passWd?userlevel=busStop"><?php echo Lang::get('content.edit_busstop'); ?><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('editBus')"></div></a></li>
                <li class="removeli removeli_2" id="getUrlRl"><a id="getUrl" href="../public/geturl" id="getUrl"><?php echo Lang::get('content.get_url'); ?><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('getUrl')"></div></a></li>
                <li class="removeli removeli_2" id="getApiRl"><a id="getApi" href="../public/passWd?userlevel=apiKeys"><?php echo Lang::get('content.get_apikey'); ?><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('getApi')"></div></a></li>
                <li class="removeli removeli_2" id="geofenceShift"><a id="getApi" href="../public/geofenceshift"><?php echo Lang::get('content.geofence_shift'); ?> <?php echo Lang::get('content.report'); ?><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('geofenceShift')"></div></a></li>
                <li class="removeli removeli_2" id="gpsPerDay"><a id="getApi" href="../public/geofenceshift?tn=gpsperday"><?php echo Lang::get('content.gps_per_day'); ?><?php echo Lang::get('content.report'); ?><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('gpsPerDay')"></div></a></li>
                <li class="removeli removeli_2" id="homepage"><a id="customLandPage" href="../public/showUserPre"><?php echo Lang::get('content.customize_landing_page'); ?><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('customLandPage')"></div></a></li>
                <li class="removeli removeli_2" id="homepage"><a href="langSuggestion" id="langSuggestion"><?php echo Lang::get('content.language_suggestion'); ?><div class="glyphicon glyphicon-new-window" onclick="openNewWindow('langSuggestion')"></div></a></li>
                 <!-- <li class="removeli removeli_2" id="English"><a href="English">english</a></li>
                    <li class="removeli removeli_2" id="Hindi"><a href="Hindi">hindi</a></li> -->

                    <!-- language hided -->
                    
                    <li class="removeli removeli_2"  id="homepage" data-toggle="collapse" data-target="#demo" ng-app="mapApp" ng-controller="mainCtrl"><a href="#demo">
                        <span id="changedlanguage"></span><span class="glyphicon glyphicon-menu-down" style="margin-left: 10px;"></span>
                        <span class="langHelpVideo" data-href="https://www.youtube.com/watch?v=d8vT8RO-9d4&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=11" class="external-link" style="padding-left: 3px;color: #196481;text-decoration: none;font-weight: bold;"><i class="glyphicon glyphicon-question-sign" style="margin-left: 30px;font-size: 12px;margin-right: 3px;"></i><?php echo Lang::get('content.help'); ?></span>
                        <!-- <span><i class="glyphicon glyphicon-question-sign" onclick="window.location.href='https://www.youtube.com/watch?v=d8vT8RO-9d4&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=11'" target="_blank"></i></span> -->
                    </a></li>
                    <div id="demo" class="collapse" style="cursor: pointer;">
                   <!-- <li class="removeli removeli_2" id="English" onclick="location = 'lang/en'"><a>English</a></li>
                     <li class="removeli removeli_2" id="Hindi" onclick="location = 'lang/en'"><a>Hindi</a></li> -->
                     <li class="removeli removeli_2" id="English" onclick="location = 'lang/en'"><a>English</a></li>
                     <li class="removeli removeli_2" id="Hindi" onclick="location = 'lang/hi'"><a>Hindi</a></li>

                     <li class="removeli removeli_2" id="Spanish" onclick="location = 'lang/es'"><a>Spanish</a></li>
                     <li class="removeli removeli_2" id="French" onclick="location = 'lang/fr'"><a>French</a></li>
                     <li class="removeli removeli_2" id="Arabic" onclick="location = 'lang/ar'"><a>Arabic</a></li>
                     <li class="removeli removeli_2" id="Portuguese" onclick="location = 'lang/pt'"><a>Portuguese</a></li>
                     <li class="removeli removeli_2" id="Gujarati" onclick="location = 'lang/gu'"><a>Gujarati</a></li>
                 </div>
             </ul>
         </li>

         <li style="margin-left: 0px;margin-right: -50px;" id="NotnMenu">
            <a style="padding: 10px 15px;color: #fff; text-align: center;margin-left: -10px;" id="notn"><span class="glyphicon glyphicon-bell" style=" cursor: pointer;margin-right: 100px;top: 5px !important;" onclick="notification()"><span class="badge badge-default" id="notncount" style="position: absolute; left: 4px; top:-12px;background-color: #36c6d3;"></span></span></a>
        </li>
        <li style="margin-left: -60px;margin-right: 100px;" id="logoutMenu">
            <a style="padding: 10px 15px;color: #fff; text-align: center;"> <span class="glyphicon glyphicon-log-out" onclick="logout()" style="cursor: pointer;margin-left: -100px;"></span></a>
        </li>
    </ul>
</nav>
</div>
<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#starterModal" id="AlertForStarter" style="display: none;"></button>
<!-- Modal -->
<div id="starterModal" class="modal fade" role="dialog" data-backdrop="false" style="top:20%;left: 10%;">
  <div class="modal-dialog" style="z-index:9999;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background-color: #196481;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: white;"><?php echo Lang::get('content.multi_track'); ?></h4>
    </div>
    <div class="modal-body">
        <p style="color:red;">This feature is not privileged for selected vehicle </p>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div>

</div>
</div>
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#expiredModal" id="licenceexpired" style="display: none;"></button>
<!-- Modal -->
<div id="expiredModal" class="modal fade" role="dialog" data-backdrop="false" style="top:20%;left: 10%;">
  <div class="modal-dialog" style="z-index:9999;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background-color: #196481;">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: white;">Licence expired</h4>
    </div>
    <div class="modal-body">
        <p style="color:red;">Licence is expired. Please contact sales for renewing licence. </p>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    </div>
</div>

</div>
</div>

<script type="text/javascript">
    $(document).on('click','span.external-link',function(){
        var t               = $(this), 
        URL             = t.attr('data-href');        
        $('<a href="'+ URL +'" target="_blank">External Link</a>')[0].click();
        
    });
    function logout()
    {   
        localStorage.clear();
        window.location.href = 'logout';
    }
    var swUser="<?php echo Session::get('swUser'); ?>";
        //alert(swUser);
        // if(swUser){
        // localStorage.setItem('userIdName', JSON.stringify('username'+","+swUser));
        // }

// $('#pwdModal').appendTo("body")

    // var menuValue = JSON.parse(localStorage.getItem('userIdName'));
    // var sp = [];
    // try{

    //     sp = menuValue.split(",");
    
    // }catch (err){
    //     $.ajax({

    //         async: false,
    //         method: 'GET',
    //         url: "aUthName",
    //         // data: {"orgId":$scope.orgIds},
    //         success: function (response) {

    //             console.log(response)
    //             sp[1] = response[1];
    //         }
    //     })
    //     // sp[1] = 'DEMO';
    //     // console.log(document.location)
    // }


    var globalIP = document.location.host;
    var contextMenu = '/'+document.location.pathname.split('/')[1];
    var labelVal;
    var sp1 = ["",""];
    var menuValue = JSON.parse(localStorage.getItem('userIdName'));

    window.localStorage.setItem("userMasterName",menuValue);
        // if(menuValue!=null){
        //     sp1 = menuValue.split(",");
        // }
        var username    =  "<?php echo Auth::user()->username; ?>";
        
        $('#valueUser').text("  "+username);
        if(username!='MSS'&&username!='SALEM-MINES')document.getElementById('fuelFillV2Rl').innerHTML = '';
        if(username!='TPMS')document.getElementById('triplossRl').innerHTML = '';
        if(username=='DEMOTAFE1'){
         document.getElementById('fuelMenu').innerHTML = ''; 
         document.getElementById('sensMenu').innerHTML = '';
         document.getElementById('performanceMenu').innerHTML = '';
         document.getElementById("reportMenu").style.marginLeft = "-30px";
         document.getElementById("analyticsMenu").style.marginLeft = "-30px";
         document.getElementById("statisticsMenu").style.marginLeft = "-30px";
         document.getElementById("NotnMenu").style.marginRight = "0px";
     }
        // else if( username=='KRUPA' || username=='CATU' ){
        //     document.getElementById('siteTripFuelRl').innerHTML = ''; 
        // }
        //if(username!='DEMO')document.getElementById('multiTrackRlNew').innerHTML = '';
        console.log(username);

        var obj = JSON.parse(localStorage.getItem('user'));
      //var menuObj = JSON.parse(window.localStorage.getItem('MenuData'));
      //console.log(menuObj);
      var sp;
      var vid;
      var gname;
      if(obj!=null){
        sp     =  obj.split(',');
        vid    =  sp[0];
        gname  =  sp[1];
    }

    else{
        vid    =  "<?php echo Session::get('vehicle'); ?>";
            //alert(vid)
            gname  =  "<?php echo Session::get('group'); ?>";
        }
        if(vid=="null"){
            vid    =  "<?php echo Session::get('vehicle'); ?>";
            //alert(vid)
            gname  =  "<?php echo Session::get('group'); ?>";
        }
        if(localStorage.getItem('timeTochange')!='yes'){
          var dateObj           =   new Date();
          var fromNowTS        =   new Date(dateObj.setDate(dateObj.getDate()-1));
          var totime        =   formatAMPM(fromNowTS.getTime());
          localStorage.setItem('toTime', totime);
                  //alert(localStorage.getItem('toTime'));
              }
              function formatAMPM(date) {
                  var date = new Date(date);
                  var hours = date.getHours();
                  var minutes = date.getMinutes();
                  var ampm = hours >= 12 ? 'PM' : 'AM';
                  hours = hours % 12;
                  hours = hours ? hours : 12; // the hour '0' should be '12'
                  minutes = minutes < 10 ? '0'+minutes : minutes;
                  var strTime = hours + ':' + minutes + ' ' + ampm;
                  return strTime;
              }
              
            //$("#movement").click(function() {
              //  window.localStorage.removeItem("reload");
              $("#movement").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=movement' );
           // });
           // $("#noData").click(function() {
            $("#noData").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=noData' );
            $("#routeDev").attr("href", 'routeDeviation?vid='+vid+'&vg='+gname+'&tn=routeDeviation' );
           // });

          //  $("#overspeed").click(function() {
            $("#overspeed").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=overspeed')
          //  });
          //  $("#parked").click(function() {
            $("#parked").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=parked')
          //  });
         //   $("#idle").click(function() {
            $("#idle").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=idle')
          //  });
         //   $("#event").click(function() {
            $("#event").attr("href", 'event?vid='+vid+'&vg='+gname+'&tn=event')
         //   });
         //   $("#site").click(function() {
            $("#triploss").attr("href", 'triploss?vid='+vid+'&vg='+gname+'&tn=tripshift')
         //   });
          //  $("#multiSite").click(function() {
            $("#multiSite").attr("href", 'multiSite?vid='+vid+'&vg='+gname+'&tn=multiSite')
          //  });
          //  $("#tripSite").click(function() {
            $("#tripSite").attr("href", 'tripSite?vid='+vid+'&vg='+gname+'&tn=tripSite')
          //  });
          //  $("#load").click(function() {
            $("#load").attr("href", 'loadDetails?vid='+vid+'&vg='+gname+'&tn=load')
          //  });
          //  $("#fuel").click(function() {
            $("#fuel").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=fuel')
          //  });
             // $("#fuelFill").click(function() {
                // $("#fuelFill").attr("href", 'fuel?vid='+vid+'&vg='+gname+'&tab=fuelfill')
             // });
          //  $("#fuelTheft").click(function() {
                // $("#fuelTheft").attr("href", 'fuelTheft?vid='+vid+'&vg='+gname+'&tn=fuelTheft')
         //   });
          //  $("#fuelProtocol").click(function() {
            $("#fuelFillV2").attr("href", 'fuelFillV2?vid='+vid+'&vg='+gname+'&tn=fuelFillV2')
            $("#fuelProtocol").attr("href", 'fuelProtocol?vid='+vid+'&vg='+gname+'&tn=fuelProtocol')
         //   });
         //  $("#fuelProtocol").click(function() {
            $("#fuelMachine").attr("href", 'fuelMachine?vid='+vid+'&vg='+gname+'&tn=fuelMachine')
            $("#fuelDispenser").attr("href", 'fuelDispenser?vid='+vid+'&vg='+gname+'&tn=fuelDispenser')
            $("#siteTripFuel").attr("href", 'siteTripFuel?vid='+vid+'&vg='+gname+'&tn=siteTripFuel')
         //   });
         //   $("#fuelMileage").click(function() {
                // $("#fuelMileage").attr("href", 'fuelMileage?vid='+vid+'&vg='+gname+'&tn=fuelMileage')
                // $("#fuelMileage").attr("href", 'fuelMileageNew?vid='+vid+'&vg='+gname+'&tn=fuelMileage')
         //   });
         //   $("#fuelConReport").click(function() {
            $("#fuelConReport").attr("href", 'fuelConReport?vid='+vid+'&vg='+gname+'&tn=fuelConReport')
                // $("#fuelConTank").attr("href", 'fuelConBasedOnTank?vid='+vid+'&vg='+gname+'&tn=fuelConTank')
         //   });
         //   $("#fuelRaw").click(function() {
            $("#fuelRaw").attr("href", 'fuelRaw?vid='+vid+'&vg='+gname+'&tn=fuelRaw')
         //   });
         //   $("#fuelConVehicle").click(function() {
            $("#fuelConVehicle").attr("href", 'fuelConsolidVehicle?vid='+vid+'&vg='+gname+'&tn=fuelConVehicle')
         //   });
         //   $("#fuelConVehicle").click(function() {
            $("#fuelConTime").attr("href", 'track?vid='+vid+'&vg='+gname+'&tn=fuelConTime&maps=fuelConsolidTime')
         //   });

         //   $("#fuelConVehicle").click(function() {
               // $("#fuelPerfor").attr("href", 'monthlyfuelperformance?vid='+vid+'&vg='+gname+'&tn=fuelPerfor')
         //   });
         $("#fuelNotification").attr("href", 'fuelNotification?vid='+vid+'&vg='+gname+'&tn=fuelNotification')


         //   $("#conAlarmReport").click(function() {
            $("#conAlarmReport").attr("href", 'conAlarmReport?vid='+vid+'&vg='+gname+'&tn=conAlarmReport')

            $("#conSecEng").attr("href", 'conSecEng?vg='+gname+'&tn=conSecEng');

            $("#conFuelTheft").attr("href", 'conFuelTheft?vid='+vid+'&vg='+gname+'&tn=conFuelTheft')
         //   });
         //   $("#conRfidReport").click(function() {
            $("#conRfidReport").attr("href", 'conRfidReport?vid='+vid+'&vg='+gname+'&tn=conRfidReport')
         //   });
        //    $("#conPriEngReport").click(function() {
            $("#conPriEngReport").attr("href", 'conPriEngReport?vid='+vid+'&vg='+gname+'&tn=conPriEngReport')
        //    });
         //   $("#fuelNew").click(function() {
                // $("#fuelNew").attr("href", 'fuelNew?vid='+vid+'&vg='+gname+'&tn=fuelNew')
         //   });
         //   $("#ignition").click(function() {
            $("#ignition").attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=ignition')
         //   });
         //   $('#trip').click(function() {
                // $('#trip').attr("href", 'trip?vid='+vid+'&vg='+gname+'&tn=trip')
         //   });
         //   $('#tripkms').click(function() {
                // console.log(' clck ')
                $('#tripkms').attr("href", 'track?vid='+vid+'&vg='+gname+'&tn=tripkms&maps=tripkms')
         //   });
         //   $('#temp').click(function() {
                // console.log(' clck ')
                $('#temp').attr("href", 'temperature?vid='+vid+'&vg='+gname+'&tn=temperature')
          //  });
          //  $('#tempdev').click(function() {
                // console.log(' clck ')
                $('#tempdev').attr("href", 'temperature?vid='+vid+'&vg='+gname+'&tn=temperatureDev')
         //   });
         //   $('#alarm').click(function(){
            $('#alarm').attr("href", 'alarm?vid='+vid+'&vg='+gname+'&tn=alarm')
          //  })

        /*  $('#ac').click(function(){
                $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=acreport')
            }) */

         //   $('#stoppage').click(function(){
            $('#stoppage').attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=stoppageReport')
         //   })

        //    $('#rfid').click(function(){
            $('#rfid').attr("href", 'rfidTag?vid='+vid+'&vg='+gname+'&tn=rfid')
         //   })

         //   $('#rfidNew').click(function(){
            $('#rfidNew').attr("href", 'rfidTagNew?vid='+vid+'&vg='+gname+'&tn=rfidNew')
         //   })

         //   $('#ac').click(function(){
            $('#ac').attr("href", 'ac?vid='+vid+'&vg='+gname+'&tn=ac')
         //   })

         //    $('#idleWaste').click(function(){
            $('#idleWaste').attr("href", 'idleWaste?vid='+vid+'&vg='+gname+'&tn=ac')
         //   })

         //   $('#engineOn').click(function(){
            $('#engineOn').attr("href", 'ac?vid='+vid+'&vg='+gname+'&tn=engine')
                // $('#doorSensor').attr("href", 'ac?vid='+vid+'&vg='+gname+'&tn=drSensor')
         //   })

         //   $('#prmEngineOn').click(function(){
            $('#prmEngineOn').attr("href", 'ac?vid='+vid+'&vg='+gname+'&tn=prmEngine')
         //   })

         $('#daywise_truck_pos').attr("href", 'dwtpReport?vid='+vid+'&vg='+gname)
         
         //   $('#camera').click(function(){
            /*$('#camera').attr("href", 'camera?vid='+vid+'&vg='+gname+'&tn=camera')*/
          //  })

         //   $('#stopReport').click(function(){
            $('#stopReport').attr("href", 'stopReport?vid='+vid+'&vg='+gname+'&tn=stopReport')
        //    })

         //   $('#ConSiteLocReport').click(function(){
            $('#ConSiteLocReport').attr("href", 'ConSiteLocReport?vid='+vid+'&vg='+gname+'&tn=ConSiteLocReport')
         //   })

          //  $('#tollReport').click(function(){
            $('#tollReport').attr("href", 'tollReport?vid='+vid+'&vg='+gname+'&tn=tollReport')
          //  })

         //   $('#nonMovingReport').click(function(){
            $('#nonMovingReport').attr("href", 'nonMovingReport?tn=nonMovingReport')
         //   })

          //  $('#travelSumReport').click(function(){
            $('#travelSumReport').attr("href", 'travelSummary?vid='+vid+'&vg='+gname+'&tn=travelSumReport')
         //   })

          //  $('#empAtnReport').click(function(){
            $('#empAtnReport').attr("href", 'empAtnReport?vid='+vid+'&vg='+gname+'&tn=empAtnReport')
         //   })

          //  $('#SchoolSmsReport').click(function(){
            $('#SchoolSmsReport').attr("href", 'SchoolSmsReport?vid='+vid+'&vg='+gname+'&tn=SchoolSmsReport')
         //   })
         
         //   $('#siteAlertReport').click(function(){
            $('#siteAlertReport').attr("href", 'siteAlertReport?vid='+vid+'&vg='+gname+'&tn=siteAlertReport')
          //  })

          //  $('#multiTrack').click(function(){
                // $('#multiTrack').attr("href", 'track?vehicleId='+vid+'&vg='+gname+'&track=multiTrack&maps=mulitple');
                // $('#multiTrack').attr("target", '_blank');

                // $('#multiTrackNew').attr("href", 'track?vehicleId='+vid+'&vg='+gname+'&track=multiTrack&maps=mulitpleNew');
                // $('#multiTrackNew').attr("target", '_blank');
         //   });
         //   $('#singleTrack').click(function(){
            // $('#singleTrack').attr("href", 'track?vehicleId='+vid+'&track=single&maps=single');
            // $('#singleTrack').attr("target", '_blank');
         //   });

         //   $('#history').click(function () {
            $('#history').attr("href", "track?vehicleId="+vid+'&gid='+gname+'&maps=replay');
         //   })

         function notification() {
           $('#notn').attr("href", 'notn?vid='+vid+'&vg='+gname+'&tn=movement' );
       }

       $.get('//'+globalIP+contextMenu+'/public/isAssetUser', function(response) {
        var menuVal  = response.trim();
        labelVal = menuVal;
        if(menuVal=="true"){
         document.getElementById('assetLabel').innerHTML = 'Assets Consolidated';
                     //document.getElementById('singleTrackRls').innerHTML   = '';
                     document.getElementById('sensMenu').innerHTML = '';
                 }
                //localStorage.setItem('isAssetUser', JSON.stringify(menuVal));
            }).error(function(){
                console.log('error in isAssetUser...');
            });


            
            $.get('//'+globalIP+contextMenu+'/public/getReportsRestrictForMobile', function(res) {
                if(document.getElementById("search_box")){
                    document.getElementById("search_box").value="";
                }
                var Restrictmenu=JSON.parse(res);
                var Restrict_FUEL=Restrictmenu.FUEL;
                var Restrict_RFID=Restrictmenu.RFID;
                var Restrict_TEMPERATURE=Restrictmenu.TEMPERATURE;
                if(Restrict_FUEL==false && username!='MRPLUSER'){  
                    document.getElementById('fuelMenu').innerHTML = '';
                            //document.getElementById('fuelPerformanceRl').innerHTML = '';
                        }
                       //alert(Restrict_FUEL);
                       $.get('//'+globalIP+contextMenu+'/public/getReportsList', function(response) {
                           if(document.getElementById("search_box")){
                            document.getElementById("search_box").value="";
                        }
                        if(response){
                          var menuObj=JSON.parse(response);
               //console.log(menuObj);

               if( menuObj != "" && menuObj!=null ){

                   for(var i=0;i<menuObj.length;i++){

            // console.log( Object.getOwnPropertyNames(menuObj[i]).sort() );
            var newReportName = Object.getOwnPropertyNames(menuObj[i]).sort();

            if(newReportName == "Analytics_IndivdiualReportsList"){

                  //  console.log( "Analytics_Reports");
                  //  console.log(menuObj[i].Analytics_IndivdiualReportsList.length);
                  

                  var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Analytics_IndivdiualReportsList[0]).sort();
                     //  console.log(newReportNamesss.length);

                     
                     for(var k=0;k<newReportNamesss.length;k++){

                        switch(newReportNamesss[k]){

                            case 'ALARM':
                            if( menuObj[i].Analytics_IndivdiualReportsList[0].ALARM==false){ document.getElementById('alarmRl').innerHTML ='';}
                            break;
                            case 'IDLE':
                            if( menuObj[i].Analytics_IndivdiualReportsList[0].IDLE==false){   document.getElementById('idleRl').innerHTML = '';}
                            break;
                            case 'MOVEMENT':
                            if( menuObj[i].Analytics_IndivdiualReportsList[0].MOVEMENT==false){   document.getElementById('movementRl').innerHTML = ''; document.getElementById('noDataRl').innerHTML = ''; }
                            break;
                            case 'OVERSPEED':
                            if( menuObj[i].Analytics_IndivdiualReportsList[0].OVERSPEED==false){   document.getElementById('overspeedRl').innerHTML = '';}
                            break;
                            case 'PARKED':
                            if( menuObj[i].Analytics_IndivdiualReportsList[0].PARKED==false){   document.getElementById('parkedRl').innerHTML = '';}
                            break;
                                // case 'TRIP_TIME':
                                //   if( menuObj[i].Analytics_IndivdiualReportsList[0].TRIP_TIME==false){   document.getElementById('tripRl').innerHTML = '';}
                                // break;
                                case 'TRIP_SUMMARY':
                                if( menuObj[i].Analytics_IndivdiualReportsList[0].TRIP_SUMMARY==false){   document.getElementById('tripkmsRl').innerHTML = '';}
                                break;
                            /*  case 'SITE':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].SITE==false){   document.getElementById('siteRl').innerHTML = '';}
                                  break; */
                                  case 'SITE_TRIP':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].SITE_TRIP==false){   document.getElementById('tripSiteRl').innerHTML = '';}
                                  break;
                             /* case 'EVENT':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].EVENT==false){   document.getElementById('eventRl').innerHTML = '';}
                                  break; */
                                  case 'STOPPAGE':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].STOPPAGE==false){   document.getElementById('stoppageRl').innerHTML = '';}
                                  break;
                                  case 'ROUTE_DEVIATION':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].ROUTE_DEVIATION==false){   document.getElementById('routeDeviationRl').innerHTML = '';}
                                  break;
                            /*  case 'MULTIPLE_SITE':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].MULTIPLE_SITE==false){   document.getElementById('multiSiteRl').innerHTML = '';}
                                  break; */
                                  
                                  case 'IGNITION':
                                  if( menuObj[i].Analytics_IndivdiualReportsList[0].IGNITION==false){  document.getElementById('ignitionRl').innerHTML = '';document.getElementById('prmEngineOnRl').innerHTML ='';}
                                  break;
                              }
                              
                          }

                      } else if(newReportName == "Consolidatedvehicles_IndivdiualReportsList"){

                   // console.log( "Consolidatedvehicles_Reports");
                   // console.log(menuObj[i].Consolidatedvehicles_IndivdiualReportsList.length);
                   
                   var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0]).sort();
                    // console.log(newReportNamesss.length);

                    for(var k=0;k<newReportNamesss.length;k++){

                        switch(newReportNamesss[k]){

                            case 'CONSOLIDATED_SITE_LOCATION':
                            if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].CONSOLIDATED_SITE_LOCATION==false ){ document.getElementById('conSiteRl').innerHTML ='';}
                            break;
                            case 'SCHOOL_SMS_REPORT':
                            if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].SCHOOL_SMS_REPORT==false ){ document.getElementById('SchoolSmsReportRl').innerHTML = '';}
                            break;
                            case 'CONSOLIDATED_STOPPAGE':
                            if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].CONSOLIDATED_STOPPAGE==false ){ document.getElementById('stopReportRl').innerHTML = '';}
                            break;
                            case 'SITESTOPPAGE_ALERT':
                            if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].SITESTOPPAGE_ALERT==false ){ document.getElementById('siteAlertReportRl').innerHTML = '';}
                            break;
                            case 'CURRENT_STATUS':
                            if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].CURRENT_STATUS==false ){ document.getElementById('curStatRl').innerHTML = '';}
                            break;
                            case 'TOLLGATE_REPORT':
                            if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].TOLLGATE_REPORT==false ){ document.getElementById('tollReportRl').innerHTML = '';}
                            break;
                            case 'NON_MOVING_REPORT':
                            if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].NON_MOVING_REPORT==false ){ document.getElementById('nonMovingReportRl').innerHTML = '';}
                            break;
                            case 'TRAVEL_SUMMARY_REPORT':
                            if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].TRAVEL_SUMMARY_REPORT==false ){ document.getElementById('travelSumReportRl').innerHTML = '';}
                            break;
                            
                            case 'PRIMARY_ENGINE_CONSOLIDATE_REPORT':
                            if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].PRIMARY_ENGINE_CONSOLIDATE_REPORT==false ){ document.getElementById('conPriEngReportRl').innerHTML = '';}
                            break;
                            case 'EMPATTN_REPORT':
                            if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].EMPATTN_REPORT==false||Restrict_RFID==false){ document.getElementById('empAtnReportRl').innerHTML = '';}
                            break;
                            case 'CONSOLIDATED_OVERSPEED':
                            if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].CONSOLIDATED_OVERSPEED==false ){ document.getElementById('conOvrRl').innerHTML = '';}
                            break;
                            case 'VEHICLES_CONSOLIDATED':
                            if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].VEHICLES_CONSOLIDATED==false ){ document.getElementById('vehConsRl').innerHTML = '';}
                            break;
                            case 'CONSOLIDATED_SITE':
                            if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].CONSOLIDATED_SITE==false ){ document.getElementById('conSiteLocRl').innerHTML = '';}
                            break;
                            case 'CONSOLIDATE_ALARM_REPORT':
                            if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].CONSOLIDATE_ALARM_REPORT==false ){ document.getElementById('conAlarmReportRl').innerHTML = '';}
                            break;
                            case 'CONSOLIDATE_RFID_REPORT':
                            if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].CONSOLIDATE_RFID_REPORT==false ||Restrict_RFID==false){ document.getElementById('conRfidReportRl').innerHTML = '';}
                            break;
                            case 'CONSOLIDATED_SECONDARY_ENGINE':
                            if( menuObj[i].Consolidatedvehicles_IndivdiualReportsList[0].CONSOLIDATED_SECONDARY_ENGINE==false){ document.getElementById('conSecEngRl').innerHTML = '';}
                            break;
                        }
                    }

                } else if(newReportName == "Performance_IndivdiualReportsList"){

                  //  console.log( "Performance_Reports");
                   // console.log(menuObj[i].Performance_IndivdiualReportsList.length);
                   
                   var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Performance_IndivdiualReportsList[0]).sort();
                     //  console.log(newReportNamesss.length);

                     for(var k=0;k<newReportNamesss.length;k++){

                        switch(newReportNamesss[k]){

                            case 'DAILY_PERFORMANCE':
                            if( menuObj[i].Performance_IndivdiualReportsList[0].DAILY_PERFORMANCE==false){ document.getElementById('dailyPerfRl').innerHTML ='';}
                            break;
                            case 'MONTHLY_PERFORMANCE':
                            if( menuObj[i].Performance_IndivdiualReportsList[0].MONTHLY_PERFORMANCE==false){   document.getElementById('monPerfRl').innerHTML = '';}
                            break;
                        }
                    }

                } else if(newReportName == "Sensor_IndivdiualReportsList") {

                  if(labelVal!="true"){

                // console.log( "Sensor_Reports");
                   // console.log(menuObj[i].Sensor_IndivdiualReportsList.length);
                   
                   var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Sensor_IndivdiualReportsList[0]).sort();
                      // console.log(newReportNamesss.length);

                      for(var k=0;k<newReportNamesss.length;k++){

                        switch(newReportNamesss[k]){

                            case 'AC':
                            if( menuObj[i].Sensor_IndivdiualReportsList[0].AC==false){
                                document.getElementById('acRl').innerHTML ='';
                                document.getElementById('engineOnRl').innerHTML ='';
                            }
                                // case 'DOOR_SENSOR':
                                //   if( menuObj[i].Sensor_IndivdiualReportsList[0].DOOR_SENSOR==false){
                                //     document.getElementById('doorSensorRl').innerHTML ='';
                                //   }
                                // break;
                             /* case 'DISTANCE_TIME':
                                  if( menuObj[i].Sensor_IndivdiualReportsList[0].DISTANCE_TIME==false){ document.getElementById('distTimeRl').innerHTML ='';}
                                  break; */
                                  case 'TEMPERATURE_DEVIATION':
                                  if( menuObj[i].Sensor_IndivdiualReportsList[0].TEMPERATURE_DEVIATION==false||Restrict_TEMPERATURE==false){  document.getElementById('tempdevRl').innerHTML = '';}
                                  break;
                                  case 'TEMPERATURE':
                                  if( menuObj[i].Sensor_IndivdiualReportsList[0].TEMPERATURE==false||Restrict_TEMPERATURE==false){  document.getElementById('tempRl').innerHTML = '';}
                                  break;
                             /* case 'LOAD':
                                  if( menuObj[i].Sensor_IndivdiualReportsList[0].LOAD==false){  document.getElementById('loadRl').innerHTML = '';}
                                break;
                                case 'FUEL_NEW':
                                  if( menuObj[i].Sensor_IndivdiualReportsList[0].FUEL_NEW==false){  document.getElementById('fuelNewRl').innerHTML = '';}
                                  break; */
                                // case 'FUEL_FILL':
                                //   if( menuObj[i].Sensor_IndivdiualReportsList[0].FUEL_FILL==false){  document.getElementById('fuelFillRl').innerHTML = '';}
                                // break;
                                // case 'FUEL_MACHINE':
                                //   if( menuObj[i].Sensor_IndivdiualReportsList[0].FUEL_MACHINE==false){  document.getElementById('fuelMachineRl').innerHTML = '';}
                                // break;
                                
                                // case 'FUEL_THEFT':
                                //   if( menuObj[i].Sensor_IndivdiualReportsList[0].FUEL_THEFT==false){  document.getElementById('fuelTheftRl').innerHTML = '';}
                                // break;
                                case 'PROTOCOL_REPORT':
                                if( menuObj[i].Sensor_IndivdiualReportsList[0].PROTOCOL_REPORT==false){ 
                                  // document.getElementById('fuelProtocolRl').innerHTML = '';
                                  // document.getElementById('dataConsumptionRl').innerHTML = '';
                                  document.getElementById('protocolMenu').innerHTML = '';
                              }
                              break;
                              //   case 'FUEL_MILEAGE':
                              //     if( menuObj[i].Sensor_IndivdiualReportsList[0].FUEL_MILEAGE==false){  document.getElementById('fuelMileageRl').innerHTML = '';
                              //     document.getElementById('fuelMileageNewRl').innerHTML = '';
                              // }
                              
                              break;
                              case 'RFID':
                              if( menuObj[i].Sensor_IndivdiualReportsList[0].RFID==false||Restrict_RFID==false){  
                                document.getElementById('rfidRl').innerHTML = '';
                                document.getElementById('rfidNewRl').innerHTML = '';}
                                break;
                                // case 'CAMERA':
                                //   if( menuObj[i].Sensor_IndivdiualReportsList[0].CAMERA==false){  document.getElementById('cameraRl').innerHTML = '';}
                                // break;
                                case 'DAYWISE_TRUCKPOSITIONS':
                                if( menuObj[i].Sensor_IndivdiualReportsList[0].DAYWISE_TRUCKPOSITIONS==false){  document.getElementById('daywise_truck_posRl').innerHTML = '';}
                                break;
                                case 'IDLE_WASTAGE':
                                if( menuObj[i].Sensor_IndivdiualReportsList[0].IDLE_WASTAGE==false){  document.getElementById('idleWasteRl').innerHTML = '';}
                                break;
                                
                                
                            }
                        }
                    }
                    
                } else if(newReportName == "Fuel_IndivdiualReportsList"){

                   // console.log( "Statistics_Reports");
                   // console.log(menuObj[i].Statistics_IndivdiualReportsList.length);
                   
                   var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Fuel_IndivdiualReportsList[0]).sort();
                     //  console.log(newReportNamesss.length);
                     const fuelvalueArr = Object.values(menuObj[i].Fuel_IndivdiualReportsList[0]);
                     var isAllReportDisabled = (currentValue) => currentValue == false;
                    //  if(!Restrict_FUEL){
                    //     console.log(fuelvalueArr.every(isAllReportDisabled));
                    // }
                    if(Restrict_FUEL){
                     if(fuelvalueArr.every(isAllReportDisabled)){
                        document.getElementById('fuelMenu').innerHTML = ''; 
                    }else{
                        for(var k=0;k<newReportNamesss.length;k++){

                            switch(newReportNamesss[k]){
                              case 'FUEL_CONSOLIDATE_REPORT':
                              if( menuObj[i].Fuel_IndivdiualReportsList[0].FUEL_CONSOLIDATE_REPORT==false||Restrict_FUEL==false ){

                                if(username!='DEMOTAFE1') document.getElementById('fuelConReportRl').innerHTML = '';
                                if (document.getElementById('fuelConVehiRl') !=null)
                                    if(username!='DEMOTAFE1') document.getElementById('fuelConVehiRl').innerHTML = '';
                            }
                            break;
                            case 'INTRADAY_FUEL':

                            if( menuObj[i].Fuel_IndivdiualReportsList[0].INTRADAY_FUEL==false){  document.getElementById('fuelConTimeRl').innerHTML = '';}
                            break;

                            case 'DISPENSER_REPORT':
                            if( menuObj[i].Fuel_IndivdiualReportsList[0].DISPENSER_REPORT==false){  document.getElementById('fuelDispenserRl').innerHTML = '';}
                            break;
                            case 'EXECUTIVE_FUEL':
                            if( menuObj[i].Fuel_IndivdiualReportsList[0].EXECUTIVE_FUEL==false||Restrict_FUEL==false){ document.getElementById('exfuelRl').innerHTML ='';}
                            break;
                            case 'MONTHLY_DIST_AND_FUEL':
                            if( menuObj[i].Fuel_IndivdiualReportsList[0].MONTHLY_DIST_AND_FUEL==false||Restrict_FUEL==false){  document.getElementById('monDistFuelRl').innerHTML = '';}
                            break;
                            case 'FUEL':
                            if( menuObj[i].Fuel_IndivdiualReportsList[0].FUEL==false||Restrict_FUEL==false){  
                                      // document.getElementById('fuelRl').innerHTML = '';
                                      document.getElementById('fuelMachineRl').innerHTML = '';
                                        //document.getElementById('fuelFillV2Rl').innerHTML = '';

                                      // if(labelVal=="false") {
                                      //     document.getElementById('fuelNewRl').innerHTML = '';
                                      // }

                                  }
                                  break;
                                  case 'FUEL_RAW':
                                  if( menuObj[i].Fuel_IndivdiualReportsList[0].FUEL_RAW==false||Restrict_FUEL==false){  document.getElementById('fuelRawRl').innerHTML = '';}
                                  break;
                                  case 'CONSOLIDATED_THEFT':
                                  if( menuObj[i].Fuel_IndivdiualReportsList[0].CONSOLIDATED_THEFT==false||Restrict_FUEL==false){  
                                    if(username!='DEMOTAFE1') {document.getElementById('conFuelTheftRl').innerHTML = '';}
                                }
                                break;
                                case 'FUEL_NOTIFICATION':
                                if( menuObj[i].Fuel_IndivdiualReportsList[0].FUEL_NOTIFICATION==false){  document.getElementById('fuelNotificationRl').innerHTML = '';}
                                break;
                                case 'GEOFENCE_FUEL_REPORT':
                                if( menuObj[i].Fuel_IndivdiualReportsList[0].GEOFENCE_FUEL_REPORT==false){  document.getElementById('siteTripFuelRl').innerHTML = '';}
                                break;
                            }
                            
                        }
                    }
                }

            } 
            else if(newReportName == "Statistics_IndivdiualReportsList"){

                   // console.log( "Statistics_Reports");
                   // console.log(menuObj[i].Statistics_IndivdiualReportsList.length);
                   
                   var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Statistics_IndivdiualReportsList[0]).sort();
                     //  console.log(newReportNamesss.length);
                     
                     const statisticsValueArr = Object.values(menuObj[i].Statistics_IndivdiualReportsList[0])
                     const isAllReportDisabled = (currentValue) => currentValue == false;
                     if(statisticsValueArr.every(isAllReportDisabled)){
                        document.getElementById('statisticsMenu').innerHTML = ''; 
                    }else{
                        for(var k=0;k<newReportNamesss.length;k++){

                            switch(newReportNamesss[k]){
                                case 'VEHICLE_PERFORMANCE':
                                if( menuObj[i].Statistics_IndivdiualReportsList[0].VEHICLE_PERFORMANCE==false){  document.getElementById('dailyRl').innerHTML = '';}
                                break;
                                case 'MONTHLY_DIST':
                                if( menuObj[i].Statistics_IndivdiualReportsList[0].MONTHLY_DIST==false){  document.getElementById('monDistRl').innerHTML = '';}
                                break;
                                case 'FREEZE_KM_REPORT':
                                if( menuObj[i].Statistics_IndivdiualReportsList[0].FREEZE_KM_REPORT==false){  document.getElementById('freezeKmRl').innerHTML = '';}
                                break;

                                    // case 'POI':
                                    //   if( menuObj[i].Statistics_IndivdiualReportsList[0].POI==false){  document.getElementById('poiRl').innerHTML = '';}
                                    // break;
                                    // case 'CONSOLIDATED':
                                    //   if( menuObj[i].Statistics_IndivdiualReportsList[0].CONSOLIDATED==false){  document.getElementById('consolRl').innerHTML = '';}
                                    // break;
                                }
                                
                            }
                        }
                        

                    } else if(newReportName == "Tracking_IndivdiualReportsList"){

                   // console.log( "Tracking_Reports");
                   // console.log(menuObj[i].Tracking_IndivdiualReportsList.length);
                   
                   var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Tracking_IndivdiualReportsList[0]).sort();
                     //  console.log(newReportNamesss.length);

                     
                     for(var k=0;k<newReportNamesss.length;k++){

                        switch(newReportNamesss[k]){

                            case 'SINGLE_TRACK':
                            if( menuObj[i].Tracking_IndivdiualReportsList[0].SINGLE_TRACK==false){
                                document.getElementById('singleTrackRl').innerHTML ='';
                                    //document.getElementById('singleTrackRls').innerHTML ='';
                                }
                                break;
                                case 'ROUTES':
                                if( menuObj[i].Tracking_IndivdiualReportsList[0].ROUTES==false){ document.getElementById('routesRl').innerHTML = '';}
                                break;
                                case 'ADDSITES_GEOFENCE':
                                if( menuObj[i].Tracking_IndivdiualReportsList[0].ADDSITES_GEOFENCE==false){ document.getElementById('addsiteRl').innerHTML = '';}
                                break;
                                case 'MULTITRACK':
                                if( menuObj[i].Tracking_IndivdiualReportsList[0].MULTITRACK==false){ document.getElementById('multiTrackRl').innerHTML = '';}
                                break;
                                case 'HISTORY':
                                if( menuObj[i].Tracking_IndivdiualReportsList[0].HISTORY==false){ document.getElementById('historyRl').innerHTML = '';}
                                break;
                            }
                        }

                    } else if(newReportName == "Useradmin_IndivdiualReportsList"){

                   // console.log( "Useradmin_Reports");
                   // console.log(menuObj[i].Useradmin_IndivdiualReportsList.length);
                   
                   var newReportNamesss = Object.getOwnPropertyNames(menuObj[i].Useradmin_IndivdiualReportsList[0]).sort();
                    //   console.log(newReportNamesss.length);

                    for(var k=0;k<newReportNamesss.length;k++){

                        switch(newReportNamesss[k]){

                            case 'EDIT_NOTIFICATION':
                            if( menuObj[i].Useradmin_IndivdiualReportsList[0].EDIT_NOTIFICATION==false){ document.getElementById('editNotRl').innerHTML ='';}
                            break;
                                // case 'EDIT_USER_NOTIFICATION':
                                //   if( menuObj[i].Useradmin_IndivdiualReportsList[0].EDIT_USER_NOTIFICATION==false){  document.getElementById('editUsNotRl').innerHTML =''}
                                // break;
                                // case 'PAYMENT_REPORTS':
                                //   if( menuObj[i].Useradmin_IndivdiualReportsList[0].PAYMENT_REPORTS==false){  document.getElementById('payDetRl').innerHTML = '';}
                                // break;
                                case 'GET_APIKEY':
                                if( menuObj[i].Useradmin_IndivdiualReportsList[0].GET_APIKEY==false){  document.getElementById('getApiRl').innerHTML = '';}
                                break;
                                case 'EDIT_BUSSTOP':
                                if( menuObj[i].Useradmin_IndivdiualReportsList[0].EDIT_BUSSTOP==false){  document.getElementById('editBusRl').innerHTML = '';}
                                break;
                                case 'RESET_PASSWORD':
                                if( menuObj[i].Useradmin_IndivdiualReportsList[0].RESET_PASSWORD==false){  document.getElementById('resetRl').innerHTML = '';}
                                break;
                                case 'EDIT_GROUP':
                                if( menuObj[i].Useradmin_IndivdiualReportsList[0].EDIT_GROUP==false){  document.getElementById('editGrpRl').innerHTML = '';}
                                break;
                            }
                        }

                    } else if(newReportName == "TRACK_PAGE"){

                       if( menuObj[i].TRACK_PAGE[0].Normal_View==true ) {

                        localStorage.setItem('trackNovateView', false);

                        
                    } else {

                        localStorage.setItem('trackNovateView', true);

                    }
                    
                    
                }
            }
            
        } else{
            console.log('Empty getReportsList API ....');
        }
    }

}).error(function(){

    console.log('error getReportsList');
});

}).error(function(){

    console.log('error getReportsRestrictForMobile');
});





// //
           // added for global datetime for all reports - start
           $("#dateFrom,#ovrFrom,#tripDatefrom").on("change paste keyup", function() {
               var changedDate = moment($(this).val(), "DD-MM-YYYY").format("YYYY-MM-DD");
               var d1 = Date.parse(changedDate);
               var d2 = "";
               if (document.getElementById("dateTo")) {
                 d2 = Date.parse(moment($("#dateTo").val(), "DD-MM-YYYY").format("YYYY-MM-DD"));
                 if (d1 > d2) {
                  $("#dateTo").val($(this).val())
                  localStorage.setItem('toDate', $(this).val());
              }
          }
          if (document.getElementById("ovrTo")) {
             d2 = Date.parse(moment($("#ovrTo").val(), "DD-MM-YYYY").format("YYYY-MM-DD"));
             if (d1 > d2) {
              $("#ovrTo").val($(this).val())
              localStorage.setItem('toDate', $(this).val());
          }
      }
      if (document.getElementById("tripDateTo")) {
        d2 = Date.parse(moment($("#tripDateTo").val(), "DD-MM-YYYY").format("YYYY-MM-DD"));
        if (d1 > d2) {
          $("#tripDateTo").val($(this).val())
          localStorage.setItem('toDate', $(this).val());
      }
  }
  localStorage.setItem('fromDate', $(this).val());
  
});
           $("#timeFrom,#ovrTimeFrom,#tripTimeFrom").on("change paste keyup", function() {
               localStorage.setItem('fromTime', $(this).val());
           });
           $("#dateTo,#ovrTo,#tripDateTo").on("change paste keyup", function() {
               var changedDate = moment($(this).val(), "DD-MM-YYYY").format("YYYY-MM-DD");
               var d1 = Date.parse(changedDate);
               var d2 = "";
               if (document.getElementById("dateFrom")) {
                 d2 = Date.parse(moment($("#dateFrom").val(), "DD-MM-YYYY").format("YYYY-MM-DD"));
                 if (d1 < d2) {
                  $("#dateFrom").val($(this).val())
                  localStorage.setItem('fromDate', $(this).val());
              }
          }
          if (document.getElementById("ovrFrom")) {
             d2 = Date.parse(moment($("#ovrFrom").val(), "DD-MM-YYYY").format("YYYY-MM-DD"));
             if (d1 < d2) {
              $("#ovrFrom").val($(this).val());
              localStorage.setItem('fromDate', $(this).val());
          }
      }
      if (document.getElementById("tripDatefrom")) {
        d2 = Date.parse(moment($("#tripDatefrom").val(), "DD-MM-YYYY").format("YYYY-MM-DD"));
        if (d1 < d2) {
          $("#tripDatefrom").val($(this).val())
          localStorage.setItem('fromDate', $(this).val());
      }
  }
  localStorage.setItem('toDate', $(this).val());
});
           $("#timeTo,#ovrTimeTo,#tripTimeTo").on("change paste keyup", function() {
               localStorage.setItem('timeTochange', 'yes');
               localStorage.setItem('toTime', $(this).val());
           });
           
        //global datetime end

        $("#movement").click(function() {
            window.localStorage.removeItem("reload");
            $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=movement' );
        });

        $("#noData").click(function() {
            $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=noData' );
        });
        $("#routeDev").click(function() {
            $(this).attr("href", 'routeDeviation?vid='+vid+'&vg='+gname+'&tn=routeDeviation' );
        });
        $("#overspeed").click(function() {
            $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=overspeed')
        });
        $("#parked").click(function() {
            $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=parked')
        });
        $("#idle").click(function() {
            $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=idle')
        });
        $("#event").click(function() {
            $(this).attr("href", 'event?vid='+vid+'&vg='+gname+'&tn=event')
        });
        $("#triploss").click(function() {
            $(this).attr("href", 'triploss?vid='+vid+'&vg='+gname+'&tn=tripshift')
        });
        $("#multiSite").click(function() {
            $(this).attr("href", 'multiSite?vid='+vid+'&vg='+gname+'&tn=multiSite')
        });
        $("#tripSite").click(function() {
            $(this).attr("href", 'tripSite?vid='+vid+'&vg='+gname+'&tn=tripSite')
        });
        $("#load").click(function() {
            $(this).attr("href", 'loadDetails?vid='+vid+'&vg='+gname+'&tn=load')
        });
            // $("#fuel").click(function() {
            //     $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=fuel')
            // });
            // $("#fuelFill").click(function() {
            //     //alert(vid);
            //     $(this).attr("href", 'fuel?vid='+vid+'&vg='+gname+'&tab=fuelfill')
            // });
            // $("#fuelTheft").click(function() {
            //     $(this).attr("href", 'fuelTheft?vid='+vid+'&vg='+gname+'&tn=fuelTheft')
            // });
            $("#fuelFillV2").click(function() {
                $(this).attr("href", 'fuelFillV2?vid='+vid+'&vg='+gname+'&tn=fuelFillV2')
            });
            $("#fuelProtocol").click(function() {
                $(this).attr("href", 'fuelProtocol?vid='+vid+'&vg='+gname+'&tn=fuelProtocol')
            });
            $("#dataConsumption").click(function() {
                $(this).attr("href", 'dataConsumption?vid='+vid+'&vg='+gname+'&tn=dataConsumption')
            });
            // $("#fuelMileage").click(function() {
            //     $(this).attr("href", 'fuelMileage?vid='+vid+'&vg='+gname+'&tn=fuelMileage')
            // });
            // $("#fuelMileageNew").click(function() {
            //     $(this).attr("href", 'fuelMileageNew?vid='+vid+'&vg='+gname+'&tn=fuelMileage')
            // });
            $("#fuelConReport").click(function() {
                $(this).attr("href", 'fuelConReport?vid='+vid+'&vg='+gname+'&tn=fuelConReport')
            });
            // $("#fuelConTank").click(function() {
            //     $(this).attr("href", 'fuelConBasedOnTank?vid='+vid+'&vg='+gname+'&tn=fuelConTank')
            // });
            $("#fuelRaw").click(function() {
                $(this).attr("href", 'fuelRaw?vid='+vid+'&vg='+gname+'&tn=fuelRaw')
            });            
            $("#fuelMachine").click(function() {
                $(this).attr("href", 'fuelMachine?vid='+vid+'&vg='+gname+'&tn=fuelMachine')
            });
            $("#fuelDispenser").click(function() {
                $(this).attr("href", 'fuelDispenser?vid='+vid+'&vg='+gname+'&tn=fuelDispenser')
            });
            $("#siteTripFuel").click(function() {
                $(this).attr("href", 'siteTripFuel?vid='+vid+'&vg='+gname+'&tn=siteTripFuel')
            });
            $("#fuelConVehicle").click(function() {
                $(this).attr("href", 'fuelConsolidVehicle?vid='+vid+'&vg='+gname+'&tn=fuelConVehicle')
            });
            $("#fuelConTime").click(function() {
                $(this).attr("href", 'track?vid='+vid+'&vg='+gname+'&tn=fuelConTime&maps=fuelConsolidTime')
            });
            
            //  $("#fuelPerfor").click(function() {
            //     $(this).attr("href", 'monthlyfuelperformance?vid='+vid+'&vg='+gname+'&tn=fuelPerfor')
            // });
            $("#fuelNotification").click(function() {
                $(this).attr("href", 'fuelNotification?vid='+vid+'&vg='+gname+'&tn=fuelNotification')
            });
            $("#conAlarmReport").click(function() {
                $(this).attr("href", 'conAlarmReport?vid='+vid+'&vg='+gname+'&tn=conAlarmReport')
            });
            $("#conSecEng").click(function() {
               $("#conSecEng").attr("href", 'conSecEng?vg='+gname+'&tn=conSecEng');
           });
            $("#conRfidReport").click(function() {
                $(this).attr("href", 'conRfidReport?vid='+vid+'&vg='+gname+'&tn=conRfidReport')
            });
            $("#conPriEngReport").click(function() {
                $(this).attr("href", 'conPriEngReport?vid='+vid+'&vg='+gname+'&tn=conPriEngReport')
            });
            $("#conFuelTheft").click(function() {
                $(this).attr("href", 'conFuelTheft?vid='+vid+'&vg='+gname+'&tn=conFuelTheft')
            });

            
            // $("#fuelNew").click(function() {
            //     $(this).attr("href", 'fuelNew?vid='+vid+'&vg='+gname+'&tn=fuelNew')
            // });
            $("#ignition").click(function() {
                $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=ignition')
            });
            // $('#trip').click(function() {
            //     $(this).attr("href", 'trip?vid='+vid+'&vg='+gname+'&tn=trip')
            // });
            $('#tripkms').click(function() {
                // console.log(' clck ')
                $(this).attr("href", 'track?vid='+vid+'&vg='+gname+'&tn=tripkms&maps=tripkms')
            });
            $('#temp').click(function() {
                // console.log(' clck ')
                $(this).attr("href", 'temperature?vid='+vid+'&vg='+gname+'&tn=temperature')
            });
            $('#tempdev').click(function() {
                // console.log(' clck ')
                $(this).attr("href", 'temperature?vid='+vid+'&vg='+gname+'&tn=temperatureDev')
            });
            $('#alarm').click(function(){
                $(this).attr("href", 'alarm?vid='+vid+'&vg='+gname+'&tn=alarm')
            })

        /*  $('#ac').click(function(){
                $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=acreport')
            }) */

            $('#stoppage').click(function(){
                $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=stoppageReport')
            })

            $('#rfid').click(function(){
                $(this).attr("href", 'rfidTag?vid='+vid+'&vg='+gname+'&tn=rfid')
            })

            $('#rfidNew').click(function(){
                $(this).attr("href", 'rfidTagNew?vid='+vid+'&vg='+gname+'&tn=rfidNew')
            })

            $('#ac').click(function(){
                $(this).attr("href", 'ac?vid='+vid+'&vg='+gname+'&tn=ac')
            })

            $('#idleWaste').click(function(){
                $(this).attr("href", 'idleWaste?vid='+vid+'&vg='+gname+'&tn=ac')
            })

            $('#engineOn').click(function(){
                $(this).attr("href", 'ac?vid='+vid+'&vg='+gname+'&tn=engine')
            })
            // $('#doorSensor').click(function(){
            //     $(this).attr("href", 'ac?vid='+vid+'&vg='+gname+'&tn=drSensor')
            // })

            $('#prmEngineOn').click(function(){
                $(this).attr("href", 'ac?vid='+vid+'&vg='+gname+'&tn=prmEngine')
            })

            $('#daywise_truck_pos').click(function(){
                $(this).attr("href", 'dwtpReport?vid='+vid+'&vg='+gname)
            })

            // $('#camera').click(function(){
            //     $(this).attr("href", 'camera?vid='+vid+'&vg='+gname+'&tn=camera')
            // })

            $('#stopReport').click(function(){
                $(this).attr("href", 'stopReport?vid='+vid+'&vg='+gname+'&tn=stopReport')
            })

            $('#ConSiteLocReport').click(function(){
                $(this).attr("href", 'ConSiteLocReport?vid='+vid+'&vg='+gname+'&tn=ConSiteLocReport')
            })

            $('#tollReport').click(function(){
                $(this).attr("href", 'tollReport?vid='+vid+'&vg='+gname+'&tn=tollReport')
            })

            $('#nonMovingReport').click(function(){
                $(this).attr("href", 'nonMovingReport?tn=nonMovingReport')
            })

            $('#travelSumReport').click(function(){
                $(this).attr("href", 'travelSummary?vid='+vid+'&vg='+gname+'&tn=travelSumReport')
            })

            $('#empAtnReport').click(function(){
                $(this).attr("href", 'empAtnReport?vid='+vid+'&vg='+gname+'&tn=empAtnReport')
            })

            $('#SchoolSmsReport').click(function(){
                $(this).attr("href", 'SchoolSmsReport?vid='+vid+'&vg='+gname+'&tn=SchoolSmsReport')
            })
            
            $('#siteAlertReport').click(function(){
                $(this).attr("href", 'siteAlertReport?vid='+vid+'&vg='+gname+'&tn=siteAlertReport')
            })

            $('#multiTrack').click(function(){
                $.get('//'+globalIP+contextMenu+'/public/checkVehicleLicence?vehicleId='+vid, function(response) {
                    console.log(response);
                    var licenceType =response.trim();
                    if(licenceType=='Starter' || licenceType=='StarterPlus'){
                        document.getElementById("AlertForStarter").click();             
                    }                    
                    else {
                        $.get('//'+globalIP+contextMenu+'/public/getVehicleExpiryDate?vehicleId='+vid, function(response) {

                            if(parseInt(response)+300000>=new Date().getTime() && response!=0){
                                window.open('track?vehicleId='+vid+'&vg='+gname+'&track=multiTrack&maps=mulitple');
                            }
                            else{
                             if (window.location.href.indexOf("ac") != -1 || window.location.href.indexOf("temperature") != -1 || 
                                window.location.href.indexOf("rfid") != -1 || window.location.href.indexOf("dwtpReport") != -1) {
                                alert('Licence is expired. Please contact sales for renewing licence.');
                        }
                        else {
                            document.getElementById("licenceexpired").click();
                        }
                    }

                }).error(function(){
                    console.log('error in isVirtualUser');
                });
            }
        }).error(function(){
            console.log('error in isVirtualUser');
        });

    });

            $('#multiTrackNew').click(function(){
                $(this).attr("href", 'track?vehicleId='+vid+'&vg='+gname+'&track=multiTrack&maps=mulitpleNew');
                $(this).attr("target", '_blank');
            });
            
            $('#singleTrack').click(function(){
               $.get('//'+globalIP+contextMenu+'/public/getVehicleExpiryDate?vehicleId='+vid, function(response) {
                console.log(parseInt(response)+3000);
                console.log(new Date().getTime());
                if(parseInt(response)+300000>=new Date().getTime() && response!=0){
                    window.open('track?vehicleId='+vid+'&track=single&maps=single');
                }
                else{
                    if (window.location.href.indexOf("ac") != -1 || window.location.href.indexOf("temperature") != -1 || 
                        window.location.href.indexOf("rfid") != -1 || window.location.href.indexOf("dwtpReport") != -1 || window.location.href.indexOf("fuelProtocol") != -1) {
                        alert('Licence is expired. Please contact sales for renewing licence.');
                }
                else {
                    document.getElementById("licenceexpired").click();
                }
            }

        }).error(function(){
            console.log('error in isVirtualUser');
        });

    });

            $('#history').click(function () {
                $(this).attr("href", "track?vehicleId="+vid+'&gid='+gname+'&maps=replay');
            })
            //  $("#fms").click(function() {
            //     // alert(JSON.parse(localStorage.getItem('userIdName')).split(",")[1]);
            //     var userName=JSON.parse(localStorage.getItem('userIdName')).split(",")[1];
            //     url="http://gpsvts.net/fms/public/switchLogin/"+userName;
            //     $(this).attr("href", url)
            // });
            $('#getUrl').click(function () {
                $(this).attr("href", "geturl?vg="+gname);
            })
            

        /*  $('#vehiView').click(function () {
                $(this).attr("href", "track?maps=viewVehicles&userId="+username+'&group='+gname);
            }) */

        /*    $('#landNew').click(function () {
                $(this).attr("href", "track?maps=landNew");
            }) */

          /*   $('#dashNew').click(function () {
                $(this).attr("href", "dashNew");
            }) */

            //auto logout

            /*  
             function  logout() {      
              window.location ="./public/login";
             }

            setTimeout(function(){ logout() }, 3600000);  //logout after one hour

       
        /*  setInterval(function(){
                $.get( "http://localhost/vamo/public/getVehicleHistory?vehicleId=TN66M0451&fromDate=2017-09-04&fromTime=16:17:20&fromDateUTC=1504522040000&userId=MSS", function( data ) {
                      $( ".result" ).html( data );
                      console.log(JSON.parse(data));
                });
            }, 3000); */


             //on click menu function to refresh START
/*                $('.funMe').click(function(){

                    $("#wrapper").click(function() {
                        window.location='history?vid='+vid+'&vg='+gname+'&tn=movement';
                    });        
                })

           // on click menu function to refresh END


           // code for onload div selection START
 
                function callSelectedDiv(){
                    {        
                         $('#wrapper').click(function()
                         {
                            $("#wrapper").click(function()
                            {
                                $(this).attr("href", 'history?vid='+vid+'&vg='+gname+'&tn=movement' );
                                window.localStorage.setItem("reload",1);
                            });
                               
                            window.location="#"+vid;
                           });

                           $('#movement').trigger('click'); // trigger auto click
                           window.localStorage.setItem("reload",1);
                          }
                        }

            // code for onload div selection END


            // calling SelectedDiv Function START                

             $(document).ready(function(){
               
                callSelectedDiv();
             });

*/          //calling SelectedDiv Function END


$('#addsites').click(function(){
    if(localStorage.getItem('auth') == 'sitesVal')
        $(this).attr("href", 'track?maps=sites')
    else
        $(this).attr("href", 'password_check')
});


        //    var isVir = JSON.parse(localStorage.getItem('isVirtualUser'));
        //    if(isVir == null || isVir == undefined){


            $.get('//'+globalIP+contextMenu+'/public/isVirtualUser', function(response) {

                var menuVal =response.trim();
                localStorage.setItem('isVirtualUser', JSON.stringify(menuVal));

                if(menuVal == 'true') {
                    var myList = document.getElementsByClassName('removeli_2');
                    for (var i = 0; myList.length > i ; i++) {
                     myList[i].innerHTML = '';
                 }
             }

         }).error(function(){
            console.log('error in isVirtualUser');
        });





    // }
    
/*      function getMaxOfArray(numArray) {
           return Math.max.apply(null, numArray);
        }

        function trimDueDays(value){
           var splitValue=value.split(/[ ]+/);
           return  splitValue[2];
        }

        function zohoDayValue(value){
     
           if(value>=7){
           
             var myList = document.getElementsByClassName('removeli');
                for (var i = 0; myList.length > i ; i++) {
                    myList[i].innerHTML = '';
                }
               console.log('7 (or) more than 7 days...');
           }
           else{

            console.log('less than 7 days...');
           }
        }

        function zohoDataCall(data){

            var zohoDayss=[];
   
            angular.forEach(data.hist,function(val, key){
                zohoDayss.push(trimDueDays(val.dueDays));
            })
         //  console.log(getMaxOfArray(zohoDayss));

        zohoDayValue(getMaxOfArray(zohoDayss));
        }
   

        $.get('//'+globalIP+contextMenu+'/public/getZohoInvoice?', function(response) {
             
              var zohoRaw=[];
                  zohoRaw=JSON.parse(response);

                  zohoDataCall(zohoRaw);
               
        }).error(function(){
                    console.log('Error in Zoho Url(Nav Menu Page)');
                }); */

    //added for notificationCount
    if(window.localStorage.getItem('totalNotifications')){
        $('#notncount').text(window.localStorage.getItem('totalNotifications'));
    }else{
        $('#notncount').text(0);
    }
    
    //added for Language Trans
    var locale="<?php echo App::getLocale(); ?>";
    if(locale){
        localStorage.setItem('lang',locale);
    }
   // added for lang trans
   if(localStorage.getItem('lang')=='en')
     $('#changedlanguage').text('English');
 else if(localStorage.getItem('lang')=='hi')
     $('#changedlanguage').text('Hindi');
 else if(localStorage.getItem('lang')=='es')
     $('#changedlanguage').text('Spanish');
 else if(localStorage.getItem('lang')=='fr')
   $('#changedlanguage').text('French');
else if(localStorage.getItem('lang')=='ar')
   $('#changedlanguage').text('Arabic');
else if(localStorage.getItem('lang')=='pt')
   $('#changedlanguage').text('Portuguese');
else if(localStorage.getItem('lang')=='gu')
   $('#changedlanguage').text('Gujarati');
else
 $('#changedlanguage').text('English');

$("#English").click(function() {
    localStorage.setItem('lang','en');
    $('#changedlanguage').text('English');
});

$("#Hindi").click(function() {
    localStorage.setItem('lang','hi');
    $('#changedlanguage').text('Hindi');
});
$("#Spanish").click(function() {
    localStorage.setItem('lang','es');
    $('#changedlanguage').text('Spanish');
});
$("#French").click(function() {
 localStorage.setItem('lang','fr');
 $('#changedlanguage').text('French');
});
$("#Arabic").click(function() {
 localStorage.setItem('lang','ar');
 $('#changedlanguage').text('Arabic');
});
$("#Portuguese").click(function() {
 localStorage.setItem('lang','pt');
 $('#changedlanguage').text('Portuguese');
});
$("#Gujarati").click(function() {
 localStorage.setItem('lang','gu');
 $('#changedlanguage').text('Gujarati');
});
function openNewWindow(id) {
    $('#'+id).attr("target", '_blank');
}
$("glyphicon-new-window").css("display","none");

$('li').on('mouseover mouseout',function(){
   $(this).find('.glyphicon-new-window').toggle();
     //find the closest li and find its children with class editInLine and 
     //toggle its display using 'toggle()'
 });
$('#protocolMenu').on('mouseover',function(){
    if(document.getElementById('protocolMenu').clientHeight==42){
      $("#protocolList").css({ top: document.getElementById('protocolMenu').clientHeight-40 });
  }else{
      $("#protocolList").css({ top: document.getElementById('protocolMenu').clientHeight-84 });
  }
});
$("#protocolli").hover(
    function() {
        $(protocolli).css("background",themeColor);
        $(this).find('div').first().css("color","white");
    }, function() {
        $( this ).css("background","white");
        $(this).find('div').first().css("color","#196481");
    }
    );
</script>
<style type="text/css">
    .menus li ul li{
      width:272px !important;
  }

  #protocolMenu li ul li {
    display: none;
    border-color: #196481;
}


#protocolMenu li:hover ul li {
    display: block;
    left: 262px;
    width:200px !important;
    border:0.5px solid #196481;
}

/*#protocolMenu li ul li .glyphicon-new-window{
    display: block;
    left: 150px;
    top: 100px;
}
*/

#reportMenu ul li{
  width:310px !important;
}

#reportMenu .glyphicon-new-window{
    left: 270px;
}

#fuelMenu ul li{
  width:310px !important;
}
#sensMenu .glyphicon-new-window{
    left: 270px;
}

#fuelMenu .glyphicon-new-window{
    left: 270px;
}

.glyphicon-new-window {
    position: absolute;
    color: white;
    left: 225px;
    top: 15px;
    cursor: pointer !important;
}
#userId .glyphicon-new-window {
    position: absolute;
    color: white;
    left: 225px;
    top: 10px !important;
    cursor: pointer !important;
}

/*.scrollbox:hover {
  max-height: 600px;
  overflow-y: auto;
  overflow-x: hidden;
  }*/
</style>
<!-- added for fixed header -->
<script src='assets/js/table-fixed-header.js'></script>
<script type="text/javascript">

    $(document).ready(function(){
      $('.table-fixed-header').fixedHeader();
  });
    function newWindowShow(x,id) {
      //alert(id)
      document.getElementById(id).style.display = "block";
      document.getElementById(id).style.left = "150px";
      $(x).find('.glyphicon-new-window').toggle();
  }
  $(function () {
    if (document.getElementById("dateFrom")||document.getElementById("dateTo")) {
        $('#dateFrom, #dateTo').datetimepicker({
            format:'DD-MM-YYYY',
            useCurrent:true,
            pickTime: false,
            maxDate: new Date,
            minDate: new Date(2015, 12, 1),
            defaultDate : new Date,
        });
    }
    
    if (document.getElementById("dtFrom")||document.getElementById("dtTo")) {
        $('#dtFrom, #dtTo').datetimepicker({
            format:'DD-MM-YYYY',
            useCurrent:true,
            pickTime: false,
            maxDate: moment().subtract(1, 'days'),
            minDate: new Date(2015, 12, 1),
            defaultDate : new Date,
        });
    }
    if (document.getElementById("tripDatefrom")||document.getElementById("tripDateTo")) {
        $('#tripDatefrom, #tripDateTo').datetimepicker({
            format:'DD-MM-YYYY',
            useCurrent:true,
            pickTime: false,
            maxDate: new Date,
            minDate: new Date(2015, 12, 1),
            defaultDate : new Date,
                // maxDate:  new Date(new Date().setDate(new Date().getDate()-1)),
                // minDate: new Date(2015, 12, 1),
                // defaultDate : new Date(new Date().setDate(new Date().getDate()-1)),
            });
    }
    if (document.getElementById("tripfrom")||document.getElementById("tripto")||document.getElementById("ovrFrom")||document.getElementById("ovrTo")) {       
      $('#tripfrom, #tripto, #ovrFrom, #ovrTo').datetimepicker({
        format:'DD-MM-YYYY',
        useCurrent:true,
        pickTime: false,
        maxDate: new Date,
        minDate: new Date(2015, 12, 1)
    });
  }
  if (document.getElementById("timeFrom")||document.getElementById("ovrTimeFrom")||document.getElementById("timefromTrip")||document.getElementById("tripTimeFrom")) {     
      $('#timeFrom, #ovrTimeFrom, #timefromTrip, #tripTimeFrom').datetimepicker({
        pickDate: false,
    });
  }
  if (document.getElementById("timeTo")||document.getElementById("ovrTimeTo")||document.getElementById("timetoTrip")||document.getElementById("tripTimeTo")) {       
      $('#timeTo, #ovrTimeTo, #timetoTrip, #tripTimeTo').datetimepicker({
        pickDate: false,
    });
  }
  if (document.getElementById("timefromTrip")){
      $('#timefromTrip').datetimepicker({
        pickDate: false
    });
  }
  if (document.getElementById("timetoTrip")){
      $('#timetoTrip').datetimepicker({
        pickDate: false
    });
  }
});
  $(document).ready(function(){ 
      if(document.location.host!='gpsvts.vamosys.com'){
        $('.langHelpVideo').css('display','none');
    }
})   
    //added for changing theme color
    var themeColor = "#196481"; 
    if(localStorage.getItem('themeColor')!=undefined && localStorage.getItem('themeColor')!=null){
        themeColor = localStorage.getItem('themeColor')
    }   
    //menu bar
    $('#menuBarList').css('background-color',themeColor);
    $(".removeli").hover(
        function() {
            $(this).css("background",themeColor);
            //$(this).find('a').css("color","white");
        }, function() {
            $( this ).css("background","white");
            //$(this).find('a').css("color",themeColor);
        }
        );
    //sidebar
    var hoverColor = "#808080";
    if(themeColor=="#196481"){
        hoverColor = "#152b44";
    }
    var menuList = [ "track" , "alert01" , "stastics" , "admin" ,"fms" , "ReleaseNotes"  ,"logout" ];
    for(var i = 0; i < menuList.length; i++){
        $("."+menuList[i]).hover(
          function() {
           $( this ).css("background",hoverColor);
       }, function() {
           $( this ).css("background",themeColor);
       }
       );
    }
    //added for sidebar themecolor Changes end
    // var menuList = [ "trackList" , "reportList" , "analyticsList" , "statisticList" ,"sensorList" , "fuelList"  ,"performanceList" , "userList" ];
    // for(var i = 0; i < menuList.length; i++){
    //     var elements = document.getElementById(menuList[i]).querySelectorAll("a");
    //     for(var j = 0; j < elements.length; j++){
    //         elements[j].style.color = themeColor;
    //     }
    // }
    

</script>
