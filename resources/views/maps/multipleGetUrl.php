<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <title><?php echo Lang::get('content.gps'); ?></title>
    <link rel="shortcut icon" href="assets/imgs/tab.ico">
    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/simple-sidebar.css" rel="stylesheet">
    <link href="../app/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
    <link href="assets/css/jVanilla.css" rel="stylesheet">
    <link href="assets/css/bootstrap-select.css" rel="stylesheet">
    <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link href="../app/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/css/popup.bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
</head>
<div id="preloader" >
    <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
    <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
    <div ng-controller="mainCtrl" style="padding-left:0 !important;">
             <?php include('menubarlist.php');?>
           <div class="box box-primary" style="min-height: 1px;margin-top: 30px;">
           </div>
          <div class="box box-primary" style="min-height:570px;margin-top:30px !important;margin-left:7%;width: 93%;">

        <div class="box-header" style="top: 15px;">
              
            <h5 class="box-title"><?php echo Lang::get('content.geturl_mul'); ?></h5>
        </div>

        <div class="box-body" style=" padding: 90px; box-shadow: 3px 3px 2px #888888; border-top: 2px solid #3071a9;margin-top: 30px;">
            <!-- <div style="background-color: #d8d8d8; padding: 100px; box-shadow: 3px 3px 2px #888888; border-top: 3px solid #3071a9;margin-top: 100px"> -->
                <div class="alert alert-danger" ng-show='error!=""'>
                  <strong>{{error}}</strong>
                </div>
                <div class="alert alert-success" ng-show='msg!=""'>
                  <strong>{{msg}}</strong>
                </div>
                <div class="container">
                    <div class="col-md-3">
                        <select ng-options="groups for groups in sliceGroup" ng-model="groupSelected" ng-change="update()" class="form-control">
                            
                            <option style="display:none" value="" ng-if="vg">{{vg}}</option>
                            <option style="display:none" value="" ng-if="!vg"> <?php echo Lang::get('content.select_group'); ?></option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <select name="selectdata" ng-model="selectedVehi"  class="selectpicker" title="<?php echo Lang::get('content.select_vehicle'); ?>" ng-options="vehi.shortName for vehi in vehiclelist" multiple  data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true"></select>
                    </div>
                     <div class="col-md-1">
                     <select id="Days" ng-model="days"  class="form-control _button_select">
                                    <option value = ""><?php echo Lang::get('content.days'); ?></option>
                                    <option value = "1">1</option>
                                    <option value = "2">2</option>
                                    <option value = "3">3</option>
                                    <option value = "4">4</option>
                                    <option value = "5">5</option>
                                    <option value = "6">6</option>
                                    <option value = "7">7</option>
                                    <option value = "8">8</option>
                                    <option value = "9">9</option>
                                    <option value = "10">10</option>
                                    <option value = "30">1 <?php echo Lang::get('content.month'); ?></option>
                                    <option value = "90">3 <?php echo Lang::get('content.months'); ?></option>
                                    <option value = "365">1 <?php echo Lang::get('content.year'); ?></option>
                                </select>
                        </div>
                   <!--  <div class="col-md-3">
                        <select ng-options="vehi.shortName for vehi in vehicles" ng-model="vehi.vehicleId" class="form-control">
                            <option style="display:none" value="">@lang('content.select') @{{vehiLabel}}</option>
                        </select>
                    </div> -->

                    <div class="col-md-1" style="margin-left: 40px" >
                        <button type="button" class="btn btn-primary" ng-click="selectVehGroup('selectedVehi')" ><?php echo Lang::get('content.get_url'); ?></button>
                        <!-- <button type="button" class="btn green" ng-click="multiTracking(vehi.vehicleId)">Submit</button> -->
                    </div>
                    <div class="col-md-1">
                    </div>
                  
                    <div class="col-sm-3" >
                        <button type="button" class="btn btn-primary" ng-click="selectVehGroup('All')" style="margin-left: 35px;"><?php echo Lang::get('content.geturl_all'); ?></button>
                    </div> 

                    <div class="col-sm-1" ng-show="false">
                        <button type="hidden" class="btn btn-default" data-toggle="modal" data-target="#myModal" id="dialog"></button>
                    </div> 
                  <!--  <div class="col-sm-3" style=" margin-left: -160px;">
                        <button type="button" class="btn btn-primary" ng-click="initMethod()">Remove All</button>
                    </div> -->
                   
                </div>
            </div>
            

             <div class="modal fade" id="myModal" role="dialog" data-backdrop="true" style="top: 50px !important;">
                <div class="modal-dialog modal-md">
                  <div class="modal-content">
                    <div class="modal-header">
                     <div class="form-group">
                         <h4><?php echo Lang::get('content.get_url'); ?></h4>
                        </div>
                    </div>
                    <div class="modal-body">
                       <h5 style="color: red">{{modal_error}}</h5>
                         <label for="mail"><?php echo Lang::get('content.mailid'); ?>:</label>
                          <input type="email" class="form-control" id="mail" required>
                          <br>
                          <label for="phone"><?php echo Lang::get('content.mobile_no'); ?>:</label>
                          <input type="text" class="form-control" id="phone">
                          <span style="font-size: 10px"><?php echo Lang::get('content.usecomma_mobile'); ?></span>
                           <br>
                          <label for="phone"><?php echo Lang::get('content.comments'); ?>:</label>
                          <textarea class="form-control" id="comments"></textarea>
                    </div>
                    <div class="modal-footer">
                      <button type="button" ng-click="geturl()" class="btn btn-default" ><?php echo Lang::get('content.submit'); ?></button>
                       <button type="button" class="btn btn-default" data-dismiss="modal" id="closeBut"><?php echo Lang::get('content.close'); ?></button>
                    </div>
                  </div>
                </div>
              </div>
       </div>
   </div>

   <!--  <script src="assets/js/static.js"></script>
    <script src="assets/js/jquery-1.11.0.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
    <script src="assets/js/ui-bootstrap-0.6.0.min.js"></script>
    <script src="http://code.highcharts.com/highcharts.js"></script>
    <script src="http://code.highcharts.com/highcharts-more.js"></script>
    <script src="http://code.highcharts.com/modules/solid-gauge.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places" type="text/javascript"></script>
    <script src="assets/js/markerwithlabel.js"></script>
    <script src="assets/js/infobox.js"  type="text/javascript"></script>
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/services.js"></script>
    <script src="assets/js/multiTrack.js"></script> -->

    <script type="text/javascript">
     var apikey_url = JSON.parse(localStorage.getItem('apiKey'));
        var url = "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places";
          //var url = "https://maps.googleapis.com/maps/api/js?key=AIzaSyABtcdlhUVm5aKq7wAlMatI56DKanIKS6o&libraries=places"; 

      if(apikey_url != null || apikey_url != undefined)
          url = "https://maps.googleapis.com/maps/api/js?key="+apikey_url+"&libraries=places";

    function loadJsFilesSequentially(scriptsCollection, startIndex, librariesLoadedCallback) {
     if (scriptsCollection[startIndex]) {
        var fileref = document.createElement('script');
        fileref.setAttribute("type","text/javascript");
        fileref.setAttribute("src", scriptsCollection[startIndex]);
        fileref.onload = function(){
            startIndex = startIndex + 1;
            loadJsFilesSequentially(scriptsCollection, startIndex, librariesLoadedCallback)
        };
 
        document.getElementsByTagName("head")[0].appendChild(fileref)
    }
    else {
        librariesLoadedCallback();
        }
    }
 
    // An array of scripts you want to load in order
    var scriptLibrary = [];
   
    scriptLibrary.push("assets/js/static.js");
    scriptLibrary.push("assets/js/jquery-1.11.0.js");
    scriptLibrary.push("assets/js/bootstrap.min.js");
    
    //scriptLibrary.push("https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places");
    scriptLibrary.push("https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js");
    scriptLibrary.push("assets/js/ui-bootstrap-0.6.0.min.js");
     scriptLibrary.push("assets/js/bootstrap-select.js");
    scriptLibrary.push("https://cdn.rawgit.com/angular-translate/bower-angular-translate/2.6.0/angular-translate.js");
    
    
    scriptLibrary.push("assets/js/vamoApp.js");
    scriptLibrary.push("assets/js/services.js");
    scriptLibrary.push("assets/js/multipleGetUrl.js?v=<?php echo Config::get('app.version');?>");

    scriptLibrary.push("https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js");
   
  
   // scriptLibrary.push("http://code.highcharts.com/highcharts.js");
   // scriptLibrary.push("http://code.highcharts.com/highcharts-more.js");
   // scriptLibrary.push("http://code.highcharts.com/modules/solid-gauge.js");
   
   // scriptLibrary.push("assets/js/infobubble.js");
   // scriptLibrary.push("assets/js/infobox.js");
   // scriptLibrary.push("assets/js/vamoApp.js");
   // scriptLibrary.push("assets/js/services.js");
   // scriptLibrary.push("assets/js/custom.js");



 
   // Pass the array of scripts you want loaded in order and a callback function to invoke when its done
   loadJsFilesSequentially(scriptLibrary, 0, function(){
});
    
</script>

</body>
</html>
