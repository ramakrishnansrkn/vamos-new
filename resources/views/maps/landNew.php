<!DOCTYPE html>

<html lang="en" style="overflow: hidden;">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="pragma" content="no-cache">
  <meta name="description" content="">
  <meta name="author" content="Vamo">
  <title><?php echo Lang::get('content.gps'); ?></title>
  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link href="assets/css/MarkerCluster.css" rel="stylesheet">
  <link href="assets/css/MarkerCluster.Default.css" rel="stylesheet">
  <link href="assets/css/leaflet.css" rel="stylesheet">
  <link href="assets/css/leaflet.label.css" rel="stylesheet">
  <link href="assets/ui-drop/select.min.css" rel="stylesheet">
  <link href="assets/css/loaders.min.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link rel="stylesheet" href="assets/css/popup.bootstrap.min.css">
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/css/bootstrap-select.css" rel="stylesheet">
  <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

  <style>

   /* Optional: Makes the sample page fill the window. */
   html, body {
    height: 100%;
    margin: 0;
    padding: 0;
    font-family: 'Lato', sans-serif;
    /*font-weight: bold;  
      font-family: 'Lato', sans-serif;
      font-family: 'Roboto', sans-serif;
      font-family: 'Open Sans', sans-serif;
      font-family: 'Raleway', sans-serif;
      font-family: 'Faustina', serif;
      font-family: 'PT Sans', sans-serif;
      font-family: 'Ubuntu', sans-serif;
      font-family: 'Droid Sans', sans-serif;
      font-family: 'Source Sans Pro', sans-serif;*/
    }

    

    .modal {
      text-align: center;
    }

    @media screen and (min-width: 768px) { 
      .modal:before {
        display: inline-block;
        vertical-align: middle;
        content: " ";
        height: 100%;
      }
    }

    .modal-dialog {
      display: inline-block;
      text-align: left;
      vertical-align: middle;
    }
    

    .leaflet-top,
    .leaflet-bottom {
      position: absolute;
      z-index: 1 !important;
      pointer-events: none;
    }

    .right-inner-addon input {
      width: 127px;
      margin-left: 10px;
      color: #727e8a !important;
    }

    .sidebar-subnav >li:nth-child(2){
      padding-top: 0px;
    }

    .custom-btn {
      width: 130px;
      height: 40px;
      color: #fff;
      border-radius: 5px;
      padding: 10px 25px;
      font-family: 'Lato', sans-serif;
      font-weight: 500;
      background: transparent;
      cursor: pointer;
      transition: all 0.3s ease;
      position: relative;
      display: inline-block;
      box-shadow:inset 2px 2px 2px 0px rgba(255,255,255,.5),
      7px 7px 20px 0px rgba(0,0,0,.1),
      4px 4px 5px 0px rgba(0,0,0,.1);
      outline: none;
    }

    .btn-12{
      position: relative;
      right: 26px;
      bottom: 20px;
      border:none;
      box-shadow: none;
      width: 130px;
      height: 15px;
      line-height: 42px;
      -webkit-perspective: 230px;
      perspective: 230px;
    }
    .btn-12 span {
      background: rgb(0,172,238);
      background: linear-gradient(0deg, rgba(0,172,238,1) 0%, rgba(2,126,251,1) 100%);
      display: block;
      position: absolute;
      width: 130px;
      height: 40px;
      box-shadow:inset 2px 2px 2px 0px rgba(255,255,255,.5),
      7px 7px 20px 0px rgba(0,0,0,.1),
      4px 4px 5px 0px rgba(0,0,0,.1);
      border-radius: 5px;
      margin:0;
      text-align: center;
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
      -webkit-transition: all .3s;
      transition: all .3s;
    }
    .btn-12 span:nth-child(1) {
      color: #fff;
      box-shadow:
      -7px -7px 20px 0px #fff9,
      -4px -4px 5px 0px #fff9,
      7px 7px 20px 0px #0002,
      4px 4px 5px 0px #0001;
      -webkit-transform: rotateX(90deg);
      -moz-transform: rotateX(90deg);
      transform: rotateX(90deg);
      -webkit-transform-origin: 50% 50% -20px;
      -moz-transform-origin: 50% 50% -20px;
      transform-origin: 50% 50% -20px;
    }
    .btn-12 span:nth-child(2) {
      color: #fff;
      -webkit-transform: rotateX(0deg);
      -moz-transform: rotateX(0deg);
      transform: rotateX(0deg);
      -webkit-transform-origin: 50% 50% -20px;
      -moz-transform-origin: 50% 50% -20px;
      transform-origin: 50% 50% -20px;
    }
    .btn-12:hover span:nth-child(1) {
      box-shadow:inset 2px 2px 2px 0px rgba(255,255,255,.5),
      7px 7px 20px 0px rgba(0,0,0,.1),
      4px 4px 5px 0px rgba(0,0,0,.1);
      -webkit-transform: rotateX(0deg);
      -moz-transform: rotateX(0deg);
      transform: rotateX(0deg);
    }
    .btn-12:hover span:nth-child(2) {
      box-shadow:inset 2px 2px 2px 0px rgba(255,255,255,.5),
      7px 7px 20px 0px rgba(0,0,0,.1),
      4px 4px 5px 0px rgba(0,0,0,.1);
      color: transparent;
      -webkit-transform: rotateX(-90deg);
      -moz-transform: rotateX(-90deg);
      transform: rotateX(-90deg);
    }

    #menuView { top: 210px; }
    #minmax { z-index: 2; }
    #draggable { top: 90px; }
    /*#siteSearch{display:none;}*/

    #newLeaf input[type=text] {
      width: 10px;
      box-sizing: border-box;
      border: 0px solid #f1f1f1;
      border-radius: 1px;
      font-size: 16px;
      background-color:rgb(13, 77, 103);
      background-image: url(assets/imgs/searchBarNew3.png);
      background-size: 26px;
      background-position: 2px 4px;
      background-repeat: no-repeat;
      padding: 4px 3px 5px 29px;
      -webkit-transition: width 0.4s ease-in-out;
      transition: width 0.4s ease-in-out;
      z-index: 1;
      position: absolute;
      left: 9.5px;
      margin-top: 183px;
      opacity: 0.8;
    }

    #newLeaf input[type=text]:focus {
      width: 200px;
      border: 2px solid #f1f1f1;
      background-color: #ffffff;
      opacity:0.8;
    }

    #newLeafOsm input[type=text] {
      width: 10px;
      box-sizing: border-box;
      border: 0px solid #f1f1f1;
      border-radius: 1px;
      font-size: 16px;
      background-color:rgb(13, 77, 103);
      background-image: url(assets/imgs/searchBarNew3.png);
      background-size: 26px;
      background-position: 2px 4px;
      background-repeat: no-repeat;
      padding: 4px 3px 5px 29px;
      -webkit-transition: width 0.4s ease-in-out;
      transition: width 0.4s ease-in-out;
      z-index: 1;
      position: absolute;
      left: 9.5px;
      margin-top: 183px;
      opacity: 0.8;
    }

    .bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
      width: 100px;
      
    }

    

    #newLeafOsm input[type=text]:focus {
      width: 200px;
      border: 2px solid #f1f1f1;
      background-color: #ffffff;
      opacity:0.8;
    }

    div#searchOsm {
      background-color: white;
      position: absolute;
      top: 240px;
      left: 50px;
      width: 300px;
      height: 280px;
      padding: 10px;
      z-index: 1;
    }

    div#resultsOsm {
      font-style: sans-serif;
      color: black;
      font-size: 75%;
    }

    .ui-select-bootstrap>.ui-select-choices, .ui-select-bootstrap>.ui-select-no-choice {
      width: auto;
      height: auto;
      max-height: 233px;
      overflow-x: hidden;
      margin-top: -1px;
    }

    .ui-select-bootstrap .ui-select-toggle {
      position: absolute;
      padding-right: 19px;
    }

    .scroll {
      height: auto;
      max-height: 200px;
      overflow-x: hidden;
    }

    #pac-input2 {
      padding: 0 11px 0 13px;
      width: 400px; 
      z-index:1; 
      position:absolute; 
      left:95px; 
      font-size: 15px;
      font-weight: 300;
      text-overflow: ellipsis;
      margin-top:70px;
      margin-right:10px;
    }
    
    #dropdowns {
 /* box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.2); 
    top: 48px;
    left: 12%;
    position: absolute;
    z-index: 1;
    opacity: 0.9; */
    box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.2); 
    position: absolute;
    z-index: 1;
    top: 50px;
    left: 200px;
    cursor: pointer;
  }

  .polygon_osm{ 
    stroke: green;
    fill: blue;
    /* stroke-dasharray: 10,10; */
    stroke-width: 1;  
  }

  .polygonLabel { 
    color:red;
    background-color:'#f1f1f1';
    z-index: 1;
  }

  .polygonLabel:before,
  .polygonLabel:after {
    border-top: none;
    border-bottom:none;
    content: none;
    position:none;
    top: none;
  }

  .polygonLabel:before {
    border-right: none;
    border-right-color:none;
    left:none;
  }

  .polygonLabel:after {
    border-left: none;
    border-left-color: none;
    right:none;
  }

  @keyframes shine {
    0% {
      transform: translateX(-30px) rotate(-25deg);
    }

    100% {
      transform: translateX(100px) rotate(-25deg);
    }
  }

  .shine {
    margin: 1rem;
    color: #FFFFFF;

    cursor: pointer;
    border-radius: 5px;
    position: relative;
    /*overflow: hidden;*/
    /*transition: all 100ms linear;*/
  }

  .shine:hover {
    /* transform: scale(1.05) rotate(-2.5deg); */
  }

  .shine::after {
    content: '';
    display: block;
    width: 150%;
    height: 40px;
    background: rgb(255,255,255);
    background: linear-gradient(90deg, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 25%, rgba(255,255,255,1) 50%, rgba(255,255,255,1) 75%, rgba(255,255,255,0) 100%);
    opacity: 0.2;
    position: absolute;
    top: 0px;
    left: 0;
    animation: shine 2s linear infinite;
    transform: translateX(100px) rotate(-25deg);
  }

  .labelsP{
   color: red;
   background-color: #fff;
   font-family: "Lucida Grande", "Arial", sans-serif;
   font-size: 10px;
   padding: 3px 3px 3px 3px; 
   font-weight: bold;
   text-align: center;
   border: 0.5px solid black;
   border-radius: 12px;
   white-space: nowrap;
   opacity: 0.8;
 }

 .circleLabel{
  color:black;
  background-color:'#f1f1f1';
  z-index: 1;
}
#statusreport_dashboard
{
  position: absolute;
  left: 0;
  bottom: 3vh;
  z-index: 1000;
  background: #FFF;
  height: 30vh;
  overflow-y:scroll;
}
#handle {
  width: 100%;
  height: 5px;
  top: -6px;
  background-color: #b6bbbb;
  cursor:n-resize;
  position: -webkit-sticky;
  position: sticky;
}
#column_name{
  z-index: 9999999999;
  position: absolute;
  bottom: 0;
  left: -11%;    
}
#sidebar-wrapper {
  z-index:2000;
}

#graphsId{
 bottom: 30px;
 left:10vh;
} 
#table_address th
{
  position: -webkit-sticky;
  position: sticky;
  top: 0;
}
.highlightrow
{
  color:#FFF;
  backgroung-color:#999;
}
input[type=text]:disabled {
  background: white;
}
button[type=button]:disabled {
  background-color: #eee !important;
  color: gray !important;
}

#getUrlButton {
  background-color: #196481;
  color: white;
}
/*.scroll{
 width:430px;
 height:21px;
 overflow:hidden;
 position:relative;
 }*/

/*.wrapper span {
  position: absolute;
  margin: 0;
  line-height: 25px;
  white-space: nowrap;
  animation: marquee 15s linear infinite;
}

@keyframes marquee {
  0% { transform: translateX(100%); }
  100% { transform: translateX(-100%); }
}
.wrapper:hover span { animation-play-state: paused; }*/
</style>

<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script> -->
<!-- <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script> -->
<script type="text/javascript">

  var apiUrlKey = JSON.parse(localStorage.getItem('apiKey'));

  if(apiUrlKey==null){
    var aUthName = [];
    try{
      $.ajax({
        async: false,
        method: 'GET', 
        url: "aUthName",
        //data: {"orgId":$scope.orgIds},
        success: function (response) {
          aUthName = response;
            //alert(response)
            localStorage.setItem('apiKey', JSON.stringify(aUthName[0]));
            localStorage.setItem('userIdName', JSON.stringify('username'+","+aUthName[1]));
          }
        });

    } catch (err){
     console.log(err);
   }
 }

</script>
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2X3F316479');
</script>
</head>

<div class="loader-inner ball-spin-fade-loader" id="statusLoad"><div></div></div>
<body ng-app="mapApps">
  <div ng-controller="mainCtrls" class="ng-cloak">
    <div after-render="missionCompled"></div>
    <div id="wrapper">
      <div id="sidebar-wrapper">
        <ul class="sidebar-nav">
          <li class="sidebar-brand"><a href="javascript:void(0);"><img id="imagesrc" src=""/></i></a> </li>
          <li class="track"><a href="javascript:void(0);" class="active"><div></div><label><?php echo Lang::get('content.track'); ?></label></a></li>
          <!-- <li class="history"><a href="../public/track?maps=replay"><div></div><label>History</label></a></li> -->
          <!-- <li class="alert01"><a href="../public/reports"><div></div><label>Reports</label></a></li> -->
          <li class="alert01"><a ng-href="{{navReports}}"><div></div><label><?php echo Lang::get('content.reports'); ?></label></a></li>
          <!--    <li class="stastics"><a href="../public/statistics"><div></div><label>Statistics</label></a></li>-->
          <li class="stastics"><a ng-href="{{navStats}}"><div></div><label><?php echo Lang::get('content.statistics'); ?></label></a></li>
          <!--    <li class="admin"><a href="../public/settings"><div></div><label>Scheduled</label></a></li> -->
          <li class="admin"><a ng-href="{{navSched}}"><div></div><label><?php echo Lang::get('content.scheduled'); ?></label></a></li>
          <!--    <li class="fms"><a href="../public/fms"><div></div><label>FMS</label></a></li> -->
          <li class="fms" ng-hide="dealerName=='TECHBITS'"><a ng-href="{{navFms}}"><div></div><label><?php echo Lang::get('content.FMS'); ?></label></a></li>
          <li class="ReleaseNotes"><a ng-href="pdfView"><div></div><label style="line-height: 25px;" ><?php echo Lang::get('content.release'); ?> </br><?php echo Lang::get('content.notes'); ?></label></a></li>
          <li class="logout"><a href="../public/logout"><img src="assets/imgs/logout.png"/></a></li>         
        </ul>
        <ul class="sidebar-subnav" style="max-height: 100vh; overflow-y: auto;" ng-init="vehicleStatus='ALL'">
              <!-- <li style="padding-left:25px;">
                 <div class="right-inner-addon" align="center"> 
                      <select ng-model="multiLang" id="lang" class="form-control" >
                            <option value="en">English</option>
                            <option value="hi">Hindi</option>
                     </select>
                </div>
              </li> -->
              <!-- <li style="font-size: 15px;font-weight: bold;" class="right-inner-addon" align="center" id="blink" >
                Click here to view our pro application.
              </li>
               <li style="font-size: 15px;font-weight: bold;" class="right-inner-addon" align="center">
              <button class="btn">
                  navigate
              </button>
            </li> -->
            <div style="font-size: 15px;font-weight: bold;color: #fff" class="right-inner-addon" align="center" ng-show="showProButton" >
              <a href="https://gpsvtspro.vamosys.com" class="custom-btn btn-12 shine" target="_blank">
                <span style="font-size: 17px;"><i style="color: #fff;" class="glyphicon glyphicon-send"></i>Click!</span><span style="font-size: 17px;">GPSVTS PRO</span>
              </a>
            </div>
            <li style="margin-bottom: 15px;"><div class="right-inner-addon" align="center"><i class="fa fa-search"></i><form autocomplete="off"><input type="text" class="form-control" placeholder="Search" ng-model="searchVehi" name="search1" id="searchBox" autocomplete="off"  /></form></div>

              <select ng-model="vehicleStatus"  ng-change='onCategoryChange()' style="width:127px; margin-left: 10px;margin-top: 10px; font-size: 12px;padding:3px 3px 3px 3px;">
                <option value="ALL"><?php echo Lang::get('content.all'); ?> ({{totalVehicles}})</option>
                <option value="ON"><?php echo Lang::get('content.on'); ?> ({{vehicleOnline}})</option>
                <option value="OFF"><?php echo Lang::get('content.off'); ?> ({{attention}})</option>
                <option value="P"><?php echo Lang::get('content.parked'); ?> ({{parkedCount}})</option>
                <option value="M"><?php echo Lang::get('content.moving'); ?> ({{movingCount}})</option>
                <option value="S"><?php echo Lang::get('content.idle'); ?> ({{idleCount}})</option>
                <!-- <option value="O">Overspeed ({{overspeedCount}})</option> -->
              </select>

            </li> 

            <?php include('vehiclelist.php');?> 
          </ul>
        </div>

        <div id="testLoad"></div>
        <div id="page-content-wrapper">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-12">
                <div id="draggable">
                 <div class="legendlist">
                  <h3><b><?php echo Lang::get('content.vehicle'); ?> <?php echo Lang::get('content.status'); ?></b></h3>
                  <div ng-if="trvShow!=true">
                    <table cellpadding="0" cellspacing="0">
                      <tbody>
                        <tr>
                          <td><?php echo Lang::get('content.moving'); ?></td>
                          <td><img src="assets/imgs/green.png"/></td>
                          <td><?php echo Lang::get('content.standing'); ?></td>
                          <td><img src="assets/imgs{{markerPath}}/orange.png"/></td>
                        </tr>
                        <tr>
                          <td><?php echo Lang::get('content.parked'); ?></td>
                          <td><img src="assets/imgs{{markerPath}}/flag.png"/></td>
                          <td><?php echo Lang::get('content.geo_fence'); ?></td>
                          <td><img src="assets/imgs/blue.png"/></td>
                        </tr>
                        <tr>
                          <td><?php echo Lang::get('content.overspeed'); ?></td>
                          <td><img src="assets/imgs/red.png"/></td>
                          <td><?php echo Lang::get('content.n_data'); ?></td>
                          <td><img src="assets/imgs{{markerPath}}/gray.png"/></td>
                        </tr>
                      </table>
                    </div>
                    <div ng-if="trvShow==true">
                     <table cellpadding="0" cellspacing="0">
                      <tbody>
                        <tr>
                          <td><?php echo Lang::get('content.moving'); ?></td>
                          <td><img src="assets/imgs/trvMarker2/green.png"/></td>
                          <td><?php echo Lang::get('content.standing'); ?></td>
                          <td><img src="assets/imgs/trvMarker2/yellow.png"/></td>
                        </tr>
                        <tr>
                          <td><?php echo Lang::get('content.parked'); ?></td>
                          <td><img src="assets/imgs/trvMarker2/red.png"/></td>
                          <td><?php echo Lang::get('content.geo_fence'); ?></td>
                          <td><img src="assets/imgs/blue.png"/></td>
                        </tr>
                        <tr>
                          <td><?php echo Lang::get('content.overspeed'); ?></td>
                          <td><img src="assets/imgs/trvMarker2/over.png"/></td>
                          <td><?php echo Lang::get('content.n_data'); ?></td>
                          <td><img src="assets/imgs/trvMarker2/gray.png"/></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                </div> 
                <div id="minmax">
                  <img src="assets/imgs/add.png" />
                </div>    

                <div id="menuView">

                  <!-- Side Menu -->
                  <table>
                    <tr>
                      <td>
                        <div id="viewMap">
                         <span title="<?php echo Lang::get('content.list_map'); ?>" id="listImg" ng-click="mapView('listMap')"><img style="width: 20px; height: 20px" src="assets/imgs/lis.png" /></span>
                         <span id="homeImg" ng-click="mapView('tablefull')"><img style="width: 20px; height: 20px" src="assets/imgs/back.png" /></span>
                       </div>
                     </td>
                   </tr>
                               <!--  <tr>
                                    <td>
                                        <div title="Live Details" id="viewMap"><img id="minmax" src="assets/imgs/ply.png" /></div>
                                    </td>
                                  </tr> -->
                                  <tr>
                                    <td>
                                      <div title="<?php echo Lang::get('content.maps_marker_details'); ?>" id="viewMap"><img id="minmaxMarker" style="width: 20px; height: 20px" src="assets/imgs/i.png" /></div>
                                    </td>
                                  </tr>
                                <!-- <tr>
                                    <td>
                                        <div title="Near by Vehicles" id="viewMap" ng-click="nearBy();"><img src="assets/imgs/near.png" />
                                        
                                    </td>
                                  </tr> -->
                                  <tr>
                                    <td>
                                      <div id="viewMap">
                                        <span title="<?php echo Lang::get('content.cluster_view'); ?>" id="cluster" ng-click="mapView('cluster')"><img style="width: 20px; height: 20px" src="assets/imgs/group.png" /></span>
                                        <span title="<?php echo Lang::get('content.marker_view'); ?>" id="single" ng-click="mapView('single')"><img style="width: 20px; height: 20px" src="assets/imgs/single.png" /></span>
                                      </div>
                                    </td>
                                  </tr> 
                                  <tr>
                                    <td>
                                      <div id="viewMap" title="<?php echo Lang::get('content.full_screen'); ?>">
                                       <span id="fullscreen" ng-click="mapView('fscreen')"><img id="minmaxMarker" style="width: 20px; height: 20px" src="assets/imgs/full.png" /></span>
                                       <span title="<?php echo Lang::get('content.exit_fullScreen'); ?>" id="efullscreen" ng-click="mapView('escreen')"><img style="width: 20px; height: 20px" src="assets/imgs/exit_full.png" /></span>
                                     </div>
                                   </td>
                                 </tr>   
                                 <tr>
                                  <td>
                                    <div id="viewMap">
                                      <span ng-click="mapView('home')">
                                        <img style="width: 20px; height: 20px" src="assets/imgs/hom.png" />
                                      </span>

                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <div id="viewMap" title="<?php echo Lang::get('content.graphs'); ?>">
                                      <span ng-click="mapView('graphs')" id="graphsCharts">
                                        <img style="width: 20px; height: 20px" src="assets/imgs/graphs.png" />
                                      </span>

                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>
                                    <div id="viewMap" title="<?php echo Lang::get('content.change_marker'); ?>" class="cMarker">
                                            <!-- <span ng-click="mapView('markerChange')">
                                                <img style="width: 20px; height: 20px" src="assets/imgs/graphs.png" />
                                              </span> -->
                                              <span title="<?php echo Lang::get('content.change_marker'); ?>" id="carMarker" ng-click="mapView('markerChange')"><img style="width: 20px; height: 20px" src="assets/imgs/mMarker.png" /></span>
                                              <span title="<?php echo Lang::get('content.change_marker'); ?>" id="marker" ng-click="mapView('undefined')"><img style="width: 20px; height: 20px" src="assets/imgs/mCar.png" /></span>

                                            </div>
                                          </td>
                                        </tr>
                                        <tr>
                                          <td>
                                            <div id="viewMap">
                                              <span title="<?php echo Lang::get('content.enable_label'); ?>" id="enableLabel" ng-click="mapView('enableLabel')"><img style="width: 20px; height: 20px" src="assets/imgs/single.png" /></span>
                                              <span title="<?php echo Lang::get('content.disable_label'); ?>" id="disableLabel" ng-click="mapView('disableLabel')"><img style="width: 14px; height: 18px" src="assets/imgs/maps_icon.png" /></span>
                                            </div>
                                          </td>
                                        </tr> 

                            <!--    <tr>
                                    <td>
                                        <div id="viewMap">
                                            <span title="Tollgate Markers" id="tollYes" ng-click="mapView('tollYes')"><img style="width: 20px; height: 20px" src="assets/imgs/group.png" /></span>
                                            <span title="Remove Tollgates" id="tollNo" ng-click="mapView('tollNo')"><img style="width: 20px; height: 20px" src="assets/imgs/single.png" /></span>
                                        </div>
                                    </td>
                                  </tr> -->

                                </table>    

                              </div>

                              <div class="details_box_top" ng-show="dpdown=='Yes'">
                                <div class="alert alert-danger">
                                  <span style="margin-left: -22%;font-weight: bold;font-size: 16px;"><?php echo Lang::get('content.sorry_report'); ?></span>
                                </div>
                              </div>              
                              <div class="details_box_zoho"  ng-show="zohod" style="padding-top: 15px;"><span class="closeZoho" ng-click="ZohoCall()">&times;</span><a style="color:inherit;" target="_blank" ng-href="{{zohoLink}}">{{zohoDays}}</a></div>   


                              <div class="details_box_bottom"><?php echo Lang::get('content.support_details'); ?>: {{support}}
                       <!-- <div class="wrapper">
                         <span  style="margin-top: -47px;color: red;    background-color: floralwhite  ;font-size: large;" > All Reports data are under processing. Please check after a few hours.
                         </span>
                       </div> -->
                     </div>
                     <!--  Gps commands model start -->
                     <div id="gpsComModal" class="modal fade" role="dialog">
                      <div class="modal-dialog">

                        <!-- Modal-->
                        <div class="modal-content">
                          <div class="modal-header" style="min-height: 60px!important;">
                            <button type="button" class="close" data-dismiss="modal" id="closeSendCmdModal">&times;</button>
                            <h4 class="modal-title pull-right"> <h4> <span class="pull-left"><?php echo Lang::get('content.command'); ?></span> <span class="pull-right" style="margin-right: 50px;">{{_editValue._shortName}}</span></h4></h4>
                          </div>

                          <!-- <div class="alert alert-success" ng-show="Successmsg">{{ Successmsg }}</div> -->
                          <div class="modal-body">
                            <ul class="nav nav-tabs">
                              <li class="active"><a data-toggle="tab" href="#menu1"><?php echo Lang::get('content.send_command'); ?></a></li>
                              <li><a data-toggle="tab" href="#menu2" ng-click="getCommandHistory()"><?php echo Lang::get('content.command_history'); ?></a></li>
                            </ul>

                            <div class="tab-content">
                             <div id="menu1" class="tab-pane fade in active">


                              <!-- <div class="alert alert-danger" ng-show="pwdErr">{{ pwdErr }}</div> -->
                              <div class="row" >

                               <div class="col-lg-5 col-xs-12 col-sm-12" style="border-right: thin solid #dcd8d8;">
                                <div style="text-align: center;color: #2C3E50;"><h4><?php echo Lang::get('content.command_list'); ?></h4></div>
                              </div>
                            </div>
                            <div class="row" >

                             <div class="col-lg-5 col-xs-12 col-sm-12" style="border-right: thin solid #dcd8d8;">
                               <ul class="nav" style="" ng-if="showSecondCommandInfo">
                                <li data-trigger="hover" ng-class="{active:command=='RESET'}">
                                  <a href="javascript:void(0);"  ng-click="getCommand('RESET')" >
                                    <span> RESET   </span>
                                  </a> 
                                </li>
                                <li data-trigger="hover" ng-class="{active:command=='CUSTOM-COMMAND'}">
                                  <a href="javascript:void(0);"  ng-click="getCommand('CUSTOM-COMMAND')" >
                                    <span> CUSTOM COMMAND </span>
                                  </a>
                                </li>
                              </ul>
                              <ul class="nav" style="" ng-if="selectedVehDeviceModel=='GV05'">
                                <li data-trigger="hover" ng-class="{active:command=='SET-IP'}">
                                  <a href="javascript:void(0);"  ng-click="getCommand('SET-IP')" >
                                    <span> SET-IP  </span>
                                  </a> 
                                </li>
                                <li data-trigger="hover" ng-class="{active:command=='SET-APN'}">
                                  <a href="javascript:void(0);"  ng-click="getCommand('SET-APN')" >
                                    <span> SET-APN </span>
                                  </a>
                                </li>
                                <li data-trigger="hover" ng-class="{active:command=='SET-CENTERNUMBER'}">
                                  <a href="javascript:void(0);"  ng-click="getCommand('SET-CENTERNUMBER')" >
                                    <span> SET-CENTERNUMBER </span>
                                  </a>
                                </li>
                                <li data-trigger="hover" ng-class="{active:command=='SET-INTERVAL'}">
                                  <a href="javascript:void(0);"  ng-click="getCommand('SET-INTERVAL')" >
                                    <span> SET-INTERVAL </span>
                                  </a>
                                </li>
                                <li data-trigger="hover" ng-class="{active:command=='SET-RESTART'}">
                                  <a href="javascript:void(0);"  ng-click="getCommand('SET-RESTART')" >
                                   <span> SET-RESTART </span>
                                 </a>
                               </li>
                               <li data-trigger="hover" ng-class="{active:command=='SET-BASICDATA'}">
                                <a href="javascript:void(0);"  ng-click="getCommand('SET-BASICDATA')" >
                                 <span> SET-BASICDATA </span>
                               </a>
                             </li>
                             <li data-trigger="hover" ng-class="{active:command=='SET-HARDWAREDATA'}">
                              <a href="javascript:void(0);"  ng-click="getCommand('SET-HARDWAREDATA')" >
                                <span> SET-HARDWAREDATA </span>
                              </a>
                            </li>
                            <li data-trigger="hover" ng-class="{active:command=='SET-OTHERDATA'}">
                              <a href="javascript:void(0);"  ng-click="getCommand('SET-OTHERDATA')" >
                               <span> SET-OTHERDATA </span>
                             </a>
                           </li>
                                      <!--  <li data-trigger="hover" ng-class="{active:command=='remove'}">
                                        <a href="javascript:void(0);"  ng-click="getCommand('remove')" >
                                           <span> REMOVE </span>
                                        </a>
                                      </li> -->
                                    </ul>
                                  </div>

                                  <div class="col-lg-7 col-xs-12 col-sm-12" ng-if="command=='SET-IP'&&res==0">

                                   <div style="text-align: center;color: #2C3E50;"><b>SET-IP </b></div>
                                   <br>
                                   <div class="alert alert-danger" ng-show="inputEmp||pwdErr">
                                     <p ng-show="inputEmp">*{{inputEmp}} </p> 
                                     <p ng-show="pwdErr">*{{pwdErr}}</p>
                                   </div>
                                   <div class="form-group row" style="margin-bottom: 5px !important;">
                                    <div class="col-md-4" style="text-align: center;color: #2C3E50;">
                                      <?php echo Lang::get('content.ip_address'); ?>
                                    </div>
                                    <div class="col-md-8" style="text-align: center;color: #2C3E50;">
                                     <input type="text" id="ipaddress" placeholder="IP address" class="form-control" onkeypress="return (event.keyCode == 8 || event.keyCode == 0 || event.keyCode == 13) ? null : (event.keyCode >= 48 && event.keyCode <= 57)||event.keyCode ==46">
                                     <br>
                                   </div>
                                 </div>
                                 <div class="form-group row" style="margin-bottom: 5px !important;">
                                  <div class="col-md-4" style="text-align: center;color: #2C3E50;">
                                   *<?php echo Lang::get('content.password'); ?> 
                                 </div>
                                 <div class="col-md-8" style="text-align: center;color: #2C3E50;">
                                   <input type="password" id="password1" placeholder="Password" class="form-control" >
                                   <span><?php echo Lang::get('content.input_pwd'); ?></span><br>
                                 </div>
                               </div>
                               <div class="col-md-4" style="text-align: center;color: #2C3E50;">
                               </div>
                               <div style="text-align:center" class="col-md-4">  

                                <button type="button" class="btn btn-primary" ng-click="submitGPSCommand()"><?php echo Lang::get('content.send'); ?></button>
                              </div>




                            </div>
                            <div class="col-lg-7 col-xs-12 col-sm-12" ng-if="command=='SET-APN'&&res==0">
                             <div style="text-align: center;color: #2C3E50;"><b>SET-APN </b></div>
                             <br>
                             <div class="alert alert-danger" ng-show="inputEmp||pwdErr">
                               <p ng-show="inputEmp">*{{inputEmp}} </p> 
                               <p ng-show="pwdErr">*{{pwdErr}}</p>
                             </div>
                             <div class="form-group row" style="margin-bottom: 5px !important;">
                              <div class="col-md-4" style="text-align: center;color: #2C3E50;">
                                <?php echo Lang::get('content.apn_name'); ?> 
                              </div>  
                              <div class="col-md-8" style="text-align: center;color: #2C3E50;">
                               <input type="text" id="apnName" placeholder="APN name" class="form-control" ng-required="true"><br>
                             </div>
                           </div>
                           <div class="form-group row" style="margin-bottom: 5px !important;">
                            <div class="col-md-4" style="text-align: center;color: #2C3E50;">
                             *<?php echo Lang::get('content.password'); ?> 
                           </div>
                           <div class="col-md-8" style="text-align: center;color: #2C3E50;">
                             <input type="password" id="password2" placeholder="Password" class="form-control" >
                             <span><?php echo Lang::get('content.input_pwd'); ?></span><br>
                           </div>
                         </div>
                         <div class="col-md-4" style="text-align: center;color: #2C3E50;">
                         </div>
                         <div style="text-align:center" class="col-md-4">  

                          <button type="button" class="btn btn-primary" ng-click="submitGPSCommand()"><?php echo Lang::get('content.send'); ?></button>
                        </div>
                      </div>
                      <div class="col-lg-7 col-xs-12 col-sm-12" ng-if="command=='SET-CENTERNUMBER'&&res==0">
                       <div style="text-align: center;color: #2C3E50;"><b>SET-CENTERNUMBER </b></div>
                       <br>
                       <div class="alert alert-danger" ng-show="inputEmp||pwdErr">
                         <p ng-show="inputEmp">*{{inputEmp}} </p> 
                         <p ng-show="pwdErr">*{{pwdErr}}</p>
                       </div>
                       <div class="form-group row" style="margin-bottom: 5px !important;">
                        <div class="col-md-4" style="text-align: center;color: #2C3E50;">
                         <?php echo Lang::get('content.mobile_no'); ?>
                       </div>
                       <div class="col-md-8" style="text-align: center;color: #2C3E50;">
                         <input type="text" id="mobileNo" placeholder="Mobile No" class="form-control" onkeypress="return (event.keyCode == 8 || event.keyCode == 0 || event.keyCode == 13) ? null : event.keyCode >= 48 && event.keyCode <= 57"><br>
                       </div>
                     </div>
                     <div class="form-group row" style="margin-bottom: 5px !important;">
                      <div class="col-md-4" style="text-align: center;color: #2C3E50;">
                       *<?php echo Lang::get('content.password'); ?> 
                     </div>
                     <div class="col-md-8" style="text-align: center;color: #2C3E50;">
                       <input type="password" id="password3" placeholder="Password" class="form-control" >
                       <span><?php echo Lang::get('content.input_pwd'); ?></span><br>
                     </div>
                   </div>
                   <div class="col-md-4" style="text-align: center;color: #2C3E50;">
                   </div>
                   <div style="text-align:center" class="col-md-4">  

                    <button type="button" class="btn btn-primary" ng-click="submitGPSCommand()"><?php echo Lang::get('content.send'); ?></button>
                  </div>
                </div>
                <div class="col-lg-7 col-xs-12 col-sm-12" ng-if="command=='SET-INTERVAL'&&res==0">
                 <div style="text-align: center;color: #2C3E50;"><b>SET-INTERVAL </b></div>
                 <br>
                 <div class="alert alert-danger" ng-show="inputEmp||pwdErr">
                   <p ng-show="inputEmp">*{{inputEmp}} </p> 
                   <p ng-show="pwdErr">*{{pwdErr}}</p>
                 </div>
                 <div class="form-group row" style="margin-bottom: 5px !important;">
                  <div class="col-md-4" style="text-align: center;color: #2C3E50;">
                   <?php echo Lang::get('content.interval'); ?>
                 </div>
                 <div class="col-md-8" style="text-align: center;color: #2C3E50;">
                   <input type="text" id="interval" placeholder="Interval in minutes" class="form-control" onkeypress="return (event.keyCode == 8 || event.keyCode == 0 || event.keyCode == 13) ? null : event.keyCode >= 48 && event.keyCode <= 57">
                   <span style="font-size: 10px"><?php echo Lang::get('content.interval_30_mins'); ?></span><br>
                 </div>
               </div>
               <div class="form-group row" style="margin-bottom: 5px !important;">
                <div class="col-md-4" style="text-align: center;color: #2C3E50;">
                 *<?php echo Lang::get('content.password'); ?> 
               </div>
               <div class="col-md-8" style="text-align: center;color: #2C3E50;">
                 <input type="password" id="password4" placeholder="Password" class="form-control" >
                 <span><?php echo Lang::get('content.input_pwd'); ?></span><br>
               </div>
             </div>
             <div class="col-md-4" style="text-align: center;color: #2C3E50;">
             </div>
             <div style="text-align:center" class="col-md-4">  

              <button type="button" class="btn btn-primary" ng-click="submitGPSCommand()"><?php echo Lang::get('content.send'); ?></button>
            </div>
          </div>
          <div class="col-lg-7 col-xs-12 col-sm-12" ng-if="command=='SET-RESTART'&&res==0">
           <div style="text-align: center;color: #2C3E50;"><b>SET-RESTART </b></div>
           <br>
           <div class="alert alert-danger" ng-show="inputEmp||pwdErr">
             <p ng-show="pwdErr">*{{pwdErr}}</p>
           </div>
           <div class="form-group row" style="margin-bottom: 5px !important;">
            <div class="col-md-4" style="text-align: center;color: #2C3E50;">
             *<?php echo Lang::get('content.password'); ?> 
           </div>
           <div class="col-md-8" style="text-align: center;color: #2C3E50;">
             <input type="password" id="password5" placeholder="Password" class="form-control" >
             <span><?php echo Lang::get('content.input_pwd'); ?></span><br>
           </div>
         </div>
         <div class="col-md-4" style="text-align: center;color: #2C3E50;">
         </div>
         <div style="text-align:center" class="col-md-4">  

          <button type="button" class="btn btn-primary" ng-click="submitGPSCommand()"><?php echo Lang::get('content.send'); ?></button>
        </div>
      </div>
      <div class="col-lg-7 col-xs-12 col-sm-12" ng-if="command=='RESET'&&res==0" style="border-left: thin solid #dcd8d8;">
       <div style="text-align: center;color: #2C3E50;"><b>RESET </b></div>
       <br>
       <div class="alert alert-danger" ng-show="inputEmp||pwdErr">
         <p ng-show="pwdErr">*{{pwdErr}}</p>
       </div>
       <div class="form-group row" style="margin-bottom: 5px !important;">
        <div class="col-md-4" style="text-align: center;color: #2C3E50;">
         *<?php echo Lang::get('content.password'); ?> 
       </div>
       <div class="col-md-8" style="text-align: center;color: #2C3E50;">
         <input type="password" id="resetpwd" placeholder="Password" class="form-control" >
         <span><?php echo Lang::get('content.input_pwd'); ?></span><br>
       </div>
     </div>
     <div class="col-md-4" style="text-align: center;color: #2C3E50;">
     </div>
     <div style="text-align:center" class="col-md-4">  

      <button type="button" class="btn btn-primary" ng-click="submitGPSCommand()"><?php echo Lang::get('content.send'); ?></button>
    </div>
  </div>
  <div class="col-lg-7 col-xs-12 col-sm-12" ng-if="command=='CUSTOM-COMMAND'&&res==0" style="border-left: thin solid #dcd8d8;">
   <div style="text-align: center;color: #2C3E50;"><b>CUSTOM COMMAND </b></div>
   <br>
   <div class="alert alert-danger" ng-show="inputEmp||pwdErr">
     <p ng-show="inputEmp">*{{inputEmp}} </p> 
     <p ng-show="pwdErr">*{{pwdErr}}</p>
   </div>
   <div class="form-group row" style="margin-bottom: 5px !important;">
    <div class="col-md-4" style="text-align: center;color: #2C3E50;">
     Command
   </div>  
   <div class="col-md-8" style="text-align: center;color: #2C3E50;">
     <input type="text" id="customCmd" placeholder="Custom Command" class="form-control" ng-required="true"><br>
   </div>
 </div>
 <div class="form-group row" style="margin-bottom: 5px !important;">
  <div class="col-md-4" style="text-align: center;color: #2C3E50;">
   *<?php echo Lang::get('content.password'); ?> 
 </div>
 <div class="col-md-8" style="text-align: center;color: #2C3E50;">
   <input type="password" id="cusCmdPwd" placeholder="Password" class="form-control" >
   <span><?php echo Lang::get('content.input_pwd'); ?></span><br>
 </div>
</div>
<div class="col-md-4" style="text-align: center;color: #2C3E50;">
</div>
<div style="text-align:center" class="col-md-4">  

  <button type="button" class="btn btn-primary" ng-click="submitGPSCommand()"><?php echo Lang::get('content.send'); ?></button>
</div>
</div>
<div class="col-lg-7 col-xs-12 col-sm-12" ng-if="res==1">
 <div style="text-align: center;color: #2C3E50;"><b>{{ command }}
 </b></div>
 <br>
                                           <!--  <div class="form-group row" style="margin-bottom: 5px !important;">
                                                  <div> {{ GPSrespose }}</div>
                                                </div> -->
                                                <div class="form-group row" style="margin-bottom: 5px !important;">
                                                  <div class="col-md-12" style="text-align: center;color: #2C3E50;">
                                                   {{ GPSrespose }} 
                                                   <br>
                                                 </div>
                                               </div>
                                               <div style="text-align:center" class="col-md-4" ng-show="remove">  
                                                <button type="button" class="btn btn-danger" ng-click="removeCommand()" ><?php echo Lang::get('content.remove'); ?></button>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                        <div id="menu2" class="tab-pane fade">
                                          <h4><?php echo Lang::get('content.history'); ?></h4>
                                          <table class="table table-bordered table-striped table-condensed table-hover">
                                            <tbody>

                                              <tr style="font-weight: bold; background-color: #d1d1d1; text-align : center">
                                                <!-- <td custom-sort order="'vehicleName'" sort="sort"><?php echo Lang::get('content.vehicle_name'); ?></td> -->
                                                <td custom-sort order="'systemTime'" sort="sort"><?php echo Lang::get('content.date_time'); ?></td>
                                                <td custom-sort order="'deviceMsg'" sort="sort"><?php echo Lang::get('content.device_message'); ?></td>
                                                <td custom-sort order="'deviceMsg'" sort="sort"><?php echo Lang::get('content.input'); ?></td>
                                                <td custom-sort order="'communicatingPortNumber'" sort="sort"><?php echo Lang::get('content.port_no'); ?> </td>
                                              </tr>
                                              <tr ng-repeat="chistory in ComHistory | orderBy:sort.sortingOrder:sort.reverse" style="text-align : center;" ng-show="ComHistory[0].error==null">
                                                <!-- ng-click="getInput(tripsummary, siteData)" data-toggle="modal" data-target="#mapmodals" -->

                                                <!-- <td>{{chistory.vehicleName}}</td> -->
                                                <td>{{chistory.systemTime | date:'yyyy-MM-dd HH:mm:ss' }}</td>
                                                <td>{{chistory.deviceMsg.split(',')[0]}}</td>
                                                <td ng-hide="chistory.deviceMsg.split(',')[0]=='SET-BASICDATA'||chistory.deviceMsg.split(',')[0]=='SET-HARDWAREDATA'||chistory.deviceMsg.split(',')[0]=='SET-OTHERDATA'||chistory.deviceMsg.split(',')[0]=='SET-RESTART'||chistory.deviceMsg.split(',')[0]=='remove'">{{chistory.deviceMsg.split(',')[1]}}</td>
                                                <td ng-show="chistory.deviceMsg.split(',')[0]=='SET-BASICDATA'||chistory.deviceMsg.split(',')[0]=='SET-HARDWAREDATA'||chistory.deviceMsg.split(',')[0]=='SET-OTHERDATA'||chistory.deviceMsg.split(',')[0]=='SET-RESTART'||chistory.deviceMsg.split(',')[0]=='remove'">-</td>
                                                <td>{{chistory.communicatingPortNumber}}</td>
                                              </tr>
                                              <tr  ng-if="ComHistory.length== 0" align="center">
                                                <td colspan="11" class="err"><h5><?php echo Lang::get('content.no_data'); ?></h5></td>
                                              </tr>
                                              <tr  ng-if="ComHistory.length== 1&&ComHistory[0].error!=null" align="center">
                                                <td colspan="11" class="err"><h5>{{ ComHistory[0].error }}</h5></td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </div>
                                      </div>

                                    </div>

                                  </div>

                                </div>
                              </div>
                              <!-- Gps Commands model end -->
                              <!-- Modal  for username change-->
                              <div id="changeUserNameModal" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                  <!-- Modal-->
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title"><?php echo Lang::get('content.update_user_name'); ?></h4>
                                    </div>
                                    <div class="alert alert-danger" ng-show="error"> {{error}} </div>
                                    <div class="alert alert-success" ng-show="Successmsg">{{ Successmsg }}</div>
                                    <div class="modal-body">
                                     <div class="form-group row" style="margin-bottom: 5px !important;">
                                      <div class="col-md-5" style="text-align: center;color: #2C3E50;">
                                        <?php echo Lang::get('content.user_name'); ?>
                                      </div>
                                      <div class="col-md-1" style="text-align: center;color: #2C3E50;">
                                        :
                                      </div>
                                      <div class="col-md-5" style="text-align: center;">
                                        {{ userName }}
                                      </div>
                                    </div>
                                    <div class="form-group row" style="margin-bottom: 5px !important;">
                                      <div class="col-md-5" style="text-align: center;color: #2C3E50;">
                                        <?php echo Lang::get('content.new_name'); ?>
                                      </div>
                                      <div class="col-md-1" style="text-align: center;color: #2C3E50;">
                                        :
                                      </div>
                                      <div class="col-md-5" style="text-align: center;">
                                        {{ newName }}
                                      </div>
                                    </div>
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-info" ng-click="changeUserName('YES')"><?php echo Lang::get('content.accept'); ?></button>
                                    <!-- <button type="button" class="btn btn-danger" ng-click="changeUserName('NO')">Ignore</button> -->
                                    <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Lang::get('content.cancel'); ?></button>
                                  </div>
                                </div>

                              </div>
                            </div>
                            <div id="graphsId" ng-style="{ 'height': (fuelErrorMsg)?'165px':'180px' }">
                             <div style="display: grid;">
                              <div style="display: inline-flex;">
                                <div>
                                  <div><?php echo Lang::get('content.speed'); ?> - <label id="speed"></label>&nbsp;Km/h</div>
                                  <div id="container-speed"></div>
                                  <div ng-show="noOfTank==1"  style="background-color: white;padding: 10.5px;">&nbsp;</div>
                                  <div ng-show="noOfTank>1"  style="background-color: white;padding: 9px;">&nbsp;</div>
                                </div>

                                <div ng-show="vehiclFuel&&(noOfTank==1)">
                                  <div><?php echo Lang::get('content.tanksize'); ?> - <label id="fuel"></label>&nbsp;Ltr</div>
                                  <div id="container-fuel"></div>
                                  <div style="background-color: white;
                                  font-size: 12px;
                                  padding: 3px;"><div style="font-weight: bold;cursor: pointer;text-decoration: underline;" title="Distance is calculated based on mileage for Moving Vehicles - 4 kmpl, Machinery/DG -5 kmpl">Estimated 
                                  Distance</div>
                                  <div style="font-weight: normal;">{{expectedFuelMileage}} Km</div>
                                </div>
                              </div>



                              <div style="background-color: #f4f4f4;" id="fuelSensor1"  ng-show="vehiclFuel&&(noOfTank>1)" >
                                <div>Tank 1 - <label id="fuel1">{{ tankSizeArr[0] }} Ltr</label></div>
                                <div id="container-fuelSensor1"></div>
                                <div style="    background-color: white;
                                font-size: 12px;
                                padding:2px;"><div style="font-weight: bold;cursor: pointer;text-decoration: underline;" title="Distance is calculated based on mileage for Moving Vehicles - 4 kmpl, Machinery/DG -5 kmpl">Estimated 
                                Distance</div>
                                <div style="font-weight: normal;">{{expectedFuelMileage}} Km</div>
                              </div>
                            </div>
                            <div style="background-color: #f4f4f4;" id="fuelSensor2"  ng-show="vehiclFuel&&(noOfTank>1)">
                              <div>Tank 2 - <label id="fuel2">{{ tankSizeArr[1] }} Ltr</label></div>
                              <div id="container-fuelSensor2"></div>
                              <div  style="background-color: white;padding: 9px;">&nbsp;</div>
                            </div>
                            <div style="background-color: #f4f4f4;" id="fuelSensor3"  ng-show="vehiclFuel&&(noOfTank>2)">
                              <div>Tank 3 - <label id="fuel3"></label>{{ tankSizeArr[2] }} Ltr</div>
                              <div id="container-fuelSensor3"></div>
                              <div  style="background-color: white;padding: 9px;">&nbsp;</div>
                            </div>
                            <div style="background-color: #f4f4f4;" id="fuelSensor4"  ng-show="vehiclFuel&&(noOfTank>3)">
                              <div>Tank 4 - <label id="fuel4">{{ tankSizeArr[3] }} Ltr</label></div>
                              <div id="container-fuelSensor4"></div>
                              <div  style="background-color: white;padding: 9px;">&nbsp;</div>
                            </div>
                            <div style="background-color: #f4f4f4;" id="fuelSensor5"  ng-show="vehiclFuel&&(noOfTank>4)">
                              <div>Tank 5 - <label id="fuel5">{{ tankSizeArr[4] }} Ltr</label></div>
                              <div id="container-fuelSensor5"></div>
                              <div  style="background-color: white;padding:9px;">&nbsp;</div>
                            </div>
                          </div>
                          <p style="background: white;" 
                          class="err" ng-show="fuelErrorMsg">The vehicle has exceeded the sensor volt - Please check wiring of sensor</p>
                        </div>
                      </div>

                     <!--  <div id="graphsId">
                            <div>
                                <div><?php echo Lang::get('content.speed'); ?> - <label id="speed"></label>&nbsp;Km/h</div>
                                <div id="container-speed"></div>
                            </div>
                            <div ng-show="vehiclFuel">
                                <div><?php echo Lang::get('content.tanksize'); ?> - <label id="fuel"></label>&nbsp;Ltr</div>
                                <div id="container-fuel"></div>
                            </div> 
                          </div> -->


                          <style type="text/css" ng-if="hideMe">
                            .polygonLabel{ visibility: hidden !important; }
                            .circleLabel{ visibility: hidden !important; }
                          </style>
                          <style type="text/css" ng-if ="markLab">
                           .markerLabels{ visibility: hidden !important; }
                         </style>

                         <style type="text/css" ng-if ="markLabss">
                           .labels{ visibility: hidden !important; }
                         </style>

                         <style type="text/css" ng-if ="polyLabs">
                           .labelsP{ visibility: hidden !important; }
                         </style>


                         <div id="contentmin" class="rightsection">

                           <input type="button" id="traffic" ng-click="setsTraffic('traffic')" value="<?php echo Lang::get('content.traffic'); ?>" />
                           <input type="button" value="<?php echo Lang::get('content.measure'); ?>" id="pac-input02" ng-click="distance('pac-input02');"/>
                           <input type="text" id="distanceVal" value="0" disabled/> <span style="font-size:12px;"><?php echo Lang::get('content.kms'); ?></span>
                           <div class="_caption_rightSide" align="center"><?php echo Lang::get('content.live_tracking'); ?> <div id="google_translate_element"></div></div>

                           <div style="display: flex">

                            <div style="display: flex;flex: 3;">
                              <select class="selectpicker3" data-size="10" data-style="btn-default btn-sm" data-live-search="true"  ng-model="days1">
                                <option style="width: 150px;" value = "0">{{vehiLabel}} <?php echo Lang::get('content.id'); ?></option>
                                <option style="width: 150px;" ng-repeat="loc in locations04.vehicleLocations" value="{{loc.vehicleId}}">{{loc.shortName}}</option>
                              </select>
                              <select id="Days" ng-model="days"   style="width:65px;margin-left: 2px;">
                                <option value = "0"><?php echo Lang::get('content.days'); ?></option>
                                <option value = "1">1</option>
                                <option value = "2">2</option>
                                <option value = "3">3</option>
                                <option value = "4">4</option>
                                <option value = "5">5</option>
                                <option value = "6">6</option>
                                <option value = "7">7</option>
                                <option value = "8">8</option>
                                <option value = "9">9</option>
                                <option value = "10">10</option>
                                <option value = "30">1 <?php echo Lang::get('content.month'); ?></option>
                                <option value = "90">3 <?php echo Lang::get('content.months'); ?></option>
                                <option value = "365">1 <?php echo Lang::get('content.year'); ?></option>
                              </select>
                              <!-- <input type="button" id="traffic" data-toggle="modal" data-target="#myModal" value="<?php echo Lang::get('content.get_url'); ?>" /> -->
                              <button type="button" id="getUrlButton" style="margin-left: 2px";  ng-click="openModal()" ng-disabled="days1=='0' || days=='0'"><?php echo Lang::get('content.get_url'); ?></button>
                            </div>

                            <div class="modal fade" id="myModal" role="dialog" data-backdrop="false">
                              <div class="modal-dialog modal-md">
                                <div class="modal-content">
                                  <div class="modal-header">
                                   <div class="form-group">
                                     <h4 style="text-align: center;"><?php echo Lang::get('content.get_url'); ?></h4>
                                   </div>

                                 </div>
                                 <div ng-if="urlError" class="err" style="margin: 7px;">{{urlError}}</div>
                                 <div class="row" ng-if="showIcons">
                                  <div class="col-md-10">
                                    <label for="copyUrl" style="margin-top: 5px;"><?php echo Lang::get('content.copy_url'); ?>:</label>
                                    <input type="text" class="form-control" style="width: 100%;" id="copyUrl" ng-model="url"></div>
                                    <div class="col-md-2">
                                      <span>
                                       <a href="#" data-toggle="tooltip" data-placement="top" title="Copy Link"><i class="glyphicon glyphicon-copy" ng-click="copyThisUrl()"  style="color:#428bca;margin-top:33px;font-size: 20px;"></i></a>
                                     </span>
                                     <span>
                                       <a href="{{url}}" data-toggle="tooltip" data-placement="bottom" title="Open link in new tab" target="_blank"><i class="glyphicon glyphicon-new-window"  style="color:#428bca;margin-top:33px;font-size: 20px;left: 60px;top: 3px;"></i></a></span>
                                     </div>

                                   </div>
                                   <div class="modal-body">

                                     <label for="mail"><?php echo Lang::get('content.mailid'); ?>:</label>
                                     <input type="text" class="form-control" id="mail">

                                     <label for="phone" style="margin-top: 5px;"><?php echo Lang::get('content.phoneno'); ?>:</label>
                                     <input type="text" style="margin-top: 5px;" class="form-control" id="phone">
                                   </div>
                                   <div class="modal-footer">
                                    <button type="button" ng-click="getMailIdPhoneNo(days1, days)" class="btn btn-default" ><?php echo Lang::get('content.submit'); ?></button>
                                    <button type="button" class="btn btn-default" ng-click="closeModal()"><?php echo Lang::get('content.close'); ?></button>


                                  </div>
                                </div>
                              </div>
                            </div>

                            <!-- loadModal Start -->
                            <div class="modal fade" id="loadModal" role="dialog" data-backdrop="false">
                              <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content" style="top: 60px;">
                                  <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title"><?php echo Lang::get('content.load_details'); ?></h4>
                                  </div>
                                  <div class="modal-body">
                                    <div class="panel-body">
                                     <table class="table">
                                       <thead class="thead-inverse">
                                         <tr>
                                           <th style="text-align: center;">{{vehiLabel}} <?php echo Lang::get('content.id'); ?></th>
                                           <th style="text-align: center;"><?php echo Lang::get('content.invoice_no'); ?> </th>
                                           <th style="text-align: center;"><?php echo Lang::get('content.quantity'); ?></th>
                                           <th style="text-align: center;"> <?php echo Lang::get('content.contact_no'); ?></th>
                                           <th  style="text-align: center;"><?php echo Lang::get('content.location'); ?></th>
                                           <th  style="text-align: center;"><?php echo Lang::get('content.other_desc'); ?></th>
                                         </tr>
                                       </thead>
                                       <tbody>
                                         <tr ng-if=loadData.vehicleId!=null>
                                          <th scope="row" style="text-align: center;">{{loadData.vehicleId}}</th>
                                          <th scope="row" style="text-align: center;">{{loadData.invoiceNumber}}</th>
                                          <th scope="row" style="text-align: center;">{{loadData.quantity}}</th>
                                          <th scope="row" style="text-align: center;">{{loadData.customerContactNumber}}</th>
                                          <th scope="row" style="text-align: center;">{{loadData.location}}</th>
                                          <th scope="row" style="text-align: center;">{{loadData.otherDescription}}</th>
                                        </tr>
                                        <tr ng-if="loadData.error!=null && loadData.vehicleId==null">
                                          <td colspan="6" class="err"><h5><?php echo Lang::get('content.no_data_found'); ?></h5></td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </div>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Lang::get('content.close'); ?></button>
                                </div>
                              </div>

                            </div>
                          </div>
                          <!-- loadModal End -->


                          <!-- The Modal -->
                          <div id="poi" class="poi_Modal">
                                <!-- <div class="modal-header">
                                       <span>Modal Header</span>  
                                       <span class="close">&times;</span>
                                     </div> -->

                                     <!-- Modal content -->
                                     <div class="poi-content">
                                      <!-- <span class="close1">&times;</span> -->
                                      <!-- <h5>Add Site</h5> -->
                                      <div class="row">
                                       <div class="col-md-6"><input type="text" class="form-control" placeholder="<?php echo Lang::get('content.site_name'); ?>" ng-model="poiName">
                                       </div>
                                       <div class="col-md-6">
                                        <select style="width:100%; font-weight: bold; font-size: 12px; padding : 3px" ng-model="orgID" class="form-control">
                                          <option value="">Select Organisation</option>
                                          <option ng-repeat="org in organsIds">{{org}}</option>
                                        </select></div>
                                      </div>
                                      <br>
                                      <div class="row">
                                        <div class="col-md-6"></div>
                                        <div class="col-md-3"><button type="button" class="btn btn-info" ng-click="submitPoi(poiName)"><?php echo Lang::get('content.save_site'); ?></button></div>
                                        <div class="col-md-3"><button type="button" class="btn btn-default poi_close" ><?php echo Lang::get('content.close'); ?></button></div>

                                      </div>

                                    </div>
                                  </div>

                                </div>


                                <div id="rightAddress" style="padding-top:2px;">
                                 <div class="_caption_rightSide"><?php echo Lang::get('content.api_key'); ?></div>
                                 <div align="center" style="word-wrap:break-word; font-size:11px;color:#949494;padding:5px 0 2px 0;">{{apiKeys}}</div> 
                                 <!-- <div align="center" id="lastseen" style="word-wrap:break-word; font-size:11px;"></div> -->
                               </div>

                               <div class="dynamicvehicledetails">
                                <div id="viewable" ng-show="_editValue_con">


                                  <table ng-if="viewEditforAll" cellpadding="0" cellspacing="0" class="_alignLeft">
                                    <tbody>   
                                      <tr>
                                        <td ng-hide="VirtualUser" style="font-weight: bold;"><span>Vehicle Details</span></td>
                                        <td ng-hide="VirtualUser">
                                         <span><button class="btn btn-xs btn-warning" style="font-weight: bold;margin-left: 15px;border: 1px solid #f7c56a;color:#f7c56a;background-color: white;" value="<?php echo Lang::get('content.edit'); ?>" ng-modal="_editShow" ng-click="toggleEdit()" ng-hide="_editShow">Edit&nbsp;&nbsp;<span class="glyphicon glyphicon-edit"></span></button></span>
                                         <span>
                                          <button class="btn btn-xs btn-info" style="font-weight: bold;margin-left: 15px;border: 1px solid #3486ce;color: #3486ce;background-color: white;" value="<?php echo Lang::get('content.save'); ?>" ng-click="updateDetails(); " ng-show="_editShow"  ng-modal="_editShow">Save&nbsp;&nbsp;<span class="glyphicon glyphicon-saved"></span></span></button></span>
                                          <span class="pull-right helpVideo"><a href="https://www.youtube.com/watch?v=sVecpVZf6t4&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=33" target="_blank" style="padding-left: 3px;color: #196481;text-decoration: none;font-weight: bold;"><i class="glyphicon glyphicon-question-sign" style="margin-top:2px;font-size: 12px;margin-right: 3px;"></i><?php echo Lang::get('content.help'); ?></a></span>
                                        </td>
                                      </tr>                                   
                                      <tr>
                                        <td>
                                         <?php echo Lang::get('content.veh_name'); ?>
                                       </td>
                                       <td ng-show="_editShow"><input type="text" class="_input" ng-model="_editValue._shortName" name="_editValue._shortName" ></td>
                                       <td ng-hide="_editShow">{{_editValue._shortName}}</td>
                                     </tr>

                                     <tr >
                                      <td><?php echo Lang::get('content.reg_no'); ?></td>
                                      <!-- <td id="regNo"><span></span></td> -->
                                      <td ng-show="_editShow"><input type="text" class="_input" ng-model="_editValue._regNo" name="_editValue._regNo" ></td>
                                      <td ng-hide="_editShow"> {{_editValue._regNo}}</td>
                                    </tr>
                                    <tr ng-if="vehiAssetView">
                                      <td><?php echo Lang::get('content.veh_type'); ?></td>
                                      <!-- <td id="vehitype"><span></span></td> -->
                                      <td ng-show="_editShow">
                                        <select class="selectpicker1" data-size="8" data-style="btn-default btn-xs"  ng-options="vehi for vehi in _editValue._vehiTypeList | orderBy" ng-model="_editValue.vehType">
                                          <option  style="margin-right: 10px;" ><?php echo Lang::get('content.select'); ?> {{vehiLabel}}</option>
                                        </select>
                                        <!--     <input type="text" class="_input" ng-model="_editValue.vehType" name="_editValue.vehType" > -->
                                      </td>
                                      <td ng-hide="_editShow" id="vehitype"><span></span></td>
                                    </tr>
                                    <tr ng-if="vehiAssetView">
                                      <td><?php echo Lang::get('content.veh_model'); ?></td>
                                      <!-- <td id="vehitype"><span></span></td> -->
                                      <td ng-show="_editShow">
                                        <select class="selectpicker" ng-change="showOthersInput(_editValue.vehiModel)"  data-live-search="true" data-style="btn-default btn-xs" data-size="8" ng-model="_editValue.vehiModel" >

                                         <option style="width: 110px;"  ng-repeat="vehi in _editValue._vehiModelList track by $index" data-content="{{vehi}}"   >{{vehi}}</option>

                                       </select>
                                       <input type="text" class="_input" ng-model="_editValue._veModel" ng-show="showOthers" name="veModel"  style="margin-top: 5px;margin-bottom: 5px;width: 100px;" >
                                       <!--     <input type="text" class="_input" ng-model="_editValue.vehType" name="_editValue.vehType" > -->
                                     </td>
                                     <td ng-hide="_editShow">{{_editValue._vModel}}</td>
                                   </tr>
                                   <tr ng-if="vehiAssetView && !isDGVehicle">
                                    <td><?php echo Lang::get('content.odo_meter'); ?> </td>
                                    <td ng-show="_editShow"><input type="text" class="_input" ng-model="_editValue._odoDista" name="_editValue._odoDista" ></td>
                                    <td ng-hide="_editShow"> {{_editValue._odoDista}} kms</td>

                                  </tr>
                                  <tr ng-if="vehiAssetView">
                                    <td><?php echo Lang::get('content.max_speed'); ?></td>
                                    <!-- <td id="mobno"><span id="val"></span> km/h</td> -->
                                    <td ng-show="_editShow"><input type="text" class="_input" ng-model="_editValue._overSpeed" name="_editValue._overSpeed" ></td>
                                    <td ng-hide="_editShow"> {{_editValue._overSpeed}} km/h</td>

                                  </tr>
                                  <tr ng-if="vehiAssetView">
                                    <td><span></span> <?php echo Lang::get('content.dri_name'); ?></td>
                                    <!-- <td id="driverName"><span id="val"></span></td> -->
                                    <td ng-show="_editShow"><input type="text" class="_input" ng-model="_editValue._driverName" name="_editValue._driverName" ></td>
                                    <td ng-hide="_editShow"> {{_editValue._driverName}}</td>
                                  </tr>

                                            <!-- <tr ng-if="vehiAssetView">
                                                    <td id="positiontime"><span></span> Time</td>
                                                    <td id="regno"><span id="val"></span></td>
                                                  </tr> -->



                                                <!-- <tr ng-if="vehiAssetView">
                                                  <td><?php echo Lang::get('content.powerstatus'); ?></td>
                                                  <td id="vehBat"><span id="val"></span></td>
                                                </tr> -->
                                                <tr>
                                                  <td><?php echo Lang::get('content.Driver'); ?>&nbsp;<?php echo Lang::get('content.mobile'); ?></td>
                                                  <!-- <td id="mobNo"><span></span></td> -->
                                                  <td ng-show="_editShow"><input type="text" class="_input" ng-model="_editValue._driverMobile" name="_editValue._driverMobile" ></td>
                                                  <td ng-hide="_editShow"> {{_editValue._driverMobile}}</td>
                                                </tr>

                                                <tr ng-if="vehiAssetView" ng-hide="_editValue.licenceType=='Starter'">
                                                  <td><?php echo Lang::get('content.route'); ?></td>
                                                  <td ng-show="_editShow">
                                                    <select ng-options="vehi for vehi in _editValue._vehiRoutesList" ng-model="_editValue.routeName" class="_button_select_small">
                                                      <option style="display:none" value=""><?php echo Lang::get('content.select'); ?> <?php echo Lang::get('content.route'); ?></option>
                                                    </select>
                                                  </td>
                                                  <td ng-hide="_editShow" data-toggle="tooltip" data-placement="top" title="{{_editValue.routeName}}" >{{trimText(_editValue.routeName)}}</td>
                                                </tr> 

                                                <tr ng-if="_editValue._rigMode!=='Disable'">
                                                  <td><?php echo Lang::get('content.total'); ?><?php echo Lang::get('content.sec_eng_on'); ?>&nbsp;<?php echo Lang::get('content.hours'); ?> (h:m:s)</td>
                                                  <!-- <td id="mobNo"><span></span></td> -->
                                                  <td ng-show="_editShow"><input type="text" class="_input"  ng-model="_editValue._secondaryEngineOn" name="_editValue._secondaryEngineOn" onkeydown="validateval(event)" ></td>
                                                  <td ng-hide="_editShow"> {{_editValue._secondaryEngineOn}}</td>
                                                </tr>



                                                <tr ng-show="showSendCmd">
                                                  <td><?php echo Lang::get('content.gps_commands'); ?></td>

                                                  <td id="gpscommands"><span id="gpscommands"><button type="button"  class="btn btn-xs btn-default" data-toggle="modal" data-target="#gpsComModal" ng-click="commandbtnclicked()"><?php echo Lang::get('content.send_command'); ?></button></span></td>
                                                </tr>
                                                
                                                
                                              </tbody>
                                            </table>

                                            <table ng-if="viewEditforEthopia" cellpadding="0" cellspacing="0" class="_alignLeft">
                                              <tbody> 
                                                <tr>
                                                  <td><span></span> <?php echo "Gender"; ?></td>
                                                  <td ng-hide="_editShow" style="text-transform: capitalize;"> {{gender}}</td>
                                                </tr>  
                                                <tr>
                                                  <td><span></span> <?php echo "Driver's Name"; ?></td>
                                                  <td ng-hide="_editShow"> {{_editValue._driverName}}</td>
                                                </tr>
                                                <tr>
                                                  <td><span></span> <?php echo "Driver's Mobile Number"; ?></td>
                                                  <td ng-hide="_editShow"> {{_editValue._driverMobile}}</td>
                                                </tr>
                                                <tr>
                                                  <td><span></span> <?php echo "Vehicle Name"; ?></td>
                                                  <td ng-hide="_editShow"> {{_editValue._shortName}}</td>
                                                </tr>
                                                <tr>
                                                  <td><span></span> <?php echo "Made In"; ?></td>
                                                  <td ng-hide="_editShow"> {{_editValue.madeIn}}</td>
                                                </tr>
                                                <tr>
                                                  <td><span></span> <?php echo "Manufacturing Date "; ?></td>
                                                  <td ng-hide="_editShow"> {{_editValue.manufactDate}}</td>
                                                </tr>
                                                <tr>
                                                  <td><span></span> <?php echo "Motor/Vehicle Number"; ?></td>
                                                  <td ng-hide="_editShow"> {{_editValue.vehicleNumber}}</td>
                                                </tr>
                                                <tr>
                                                  <td><span></span> <?php echo "Chassis No"; ?></td>
                                                  <td ng-hide="_editShow"> {{_editValue.chassisNo}}</td>
                                                </tr>
                                                <tr>
                                                  <td><span></span> <?php echo "License Plate"; ?></td>
                                                  <td ng-hide="_editShow"> {{_editValue._regNo}}</td>
                                                </tr>
                                                <tr>
                                                  <td><span></span> <?php echo "GPS Model"; ?></td>
                                                  <td ng-hide="_editShow"> {{_editValue.devModel}}</td>
                                                </tr>

                                                <tr>
                                                  <td><span></span> <?php echo "IMEI Number"; ?></td>
                                                  <td ng-hide="_editShow"> {{_editValue.deviceId}}</td>
                                                </tr>

                                                <tr>
                                                  <td><span></span> <?php echo "SIM Card Number"; ?></td>
                                                  <td ng-hide="_editShow"> {{_editValue.gpsSimNo}}</td>
                                                </tr>
                                                <tr>
                                                  <td><span></span> <?php echo "SIM Card ICCID Number"; ?></td>
                                                  <td ng-hide="_editShow"> {{_editValue.gpsSimICCID}}</td>
                                                </tr>
                                                <tr>
                                                  <td><span></span> <?php echo "GPS System Name"; ?></td>
                                                  <td ng-hide="_editShow"> {{systemName}}</td>
                                                </tr>
                                                <tr>
                                                  <td><span></span> <?php echo "GPS Company Name"; ?></td>
                                                  <td ng-hide="_editShow"> {{companyName}}</td>
                                                </tr>
                                                <tr>
                                                  <td><span></span> <?php echo "Registration Date"; ?></td>
                                                  <td ng-hide="_editShow"> {{_editValue.onboardDate}}</td>
                                                </tr>

                                              </tbody>
                                            </table>      

                                            <div id="safeParkShow" >
                                              <div id="safEdits" class="dynamicvehicledetails">
                                                <table cellpadding="0" cellspacing="0" class="_alignLeft">
                                                  <tr ng-show="vehiAssetView">
                                                    <td style="width: 80px;"><?php echo Lang::get('content.safety_parking'); ?></td>
                                                    <td id="safePark" style="padding-left: 30px;"><span></span></td>
                                                    <td id="safEdit" style="text-align: right;vertical-align: right;" ng-hide="VirtualUser"><a class="btn btn-xs btn-default" ng-click="safEdit"><span class="glyphicon glyphicon-cog"></span> </a></td>
                                                  </tr> 
                                                </table>
                                              </div>

                                              <div id="safeUps" class="dynamicvehicledetails">
                                               <table cellpadding="0" cellspacing="0" class="_alignLeft">
                                                <tr ng-show="vehiAssetView">
                                                  <td><?php echo Lang::get('content.safe_park'); ?></td>
                                                  <td style="padding-left: 41px;"> 
                                                    <select ng-model="sparkType" class="_button_select_small">
                                                      <option value = "Yes"> <?php echo Lang::get('content.yes'); ?></option>
                                                      <option value = "No"><?php echo Lang::get('content.no'); ?></option>
                                                    </select>
                                                  </td>
                                                  <td id="safeUp" style="text-align:right;vertical-align:right;" ng-click="updateSafePark()">
                                                   <a class="btn btn-xs btn-default">
                                                     <span class="glyphicon glyphicon-ok"></span> 
                                                   </a>
                                                 </td>
                                               </tr>   
                                             </table>
                                           </div> 
                                         </div>
                                         <!--  Debug Days Start-->
                                         <div id="debugLog" ng-show="enableLogShow">
                                          <div id="debugEdit" class="dynamicvehicledetails">
                                            <table cellpadding="0" cellspacing="0" class="_alignLeft">
                                              <tr ng-show="vehiAssetView">
                                                <td style="width: 80px;"><?php echo Lang::get('content.debug'); ?></td>
                                                <td id="enablelog" style="padding-left: 30px;"><span></span></td>
                                                <!-- <td style="padding-left: 30px;"><span ng-bind="debugValue"></span><span ng-if="(debugValue!='Disable')">( </span><span>{{debugDays}}  
                                                  <span ng-if="(debugDays=='7')">1 week)</span>
                                                  <span ng-if="(debugDays=='1')">day)</span>
                                                  <span ng-if="(debugDays>'1'&&debugDays!='')">days)</span>
                                                </span></td> -->
                                                <td id="logEdit" style="text-align: right;vertical-align: right;"><a class="btn btn-xs btn-default" ng-click="logEdit"><span class="glyphicon glyphicon-cog"></span> </a></td>
                                              </tr> 
                                            </table>
                                          </div>
                                          <div id="debugUpdate" class="dynamicvehicledetails">
                                           <table cellpadding="0" cellspacing="0" class="_alignLeft">
                                             <!--  <tr ng-show="vehiAssetView"> -->
                                              <tr>
                                                <td>Debug</td>
                                                <td style="padding-left: 41px;"> 
                                                  <select ng-model="debugValue" class="_button_select_small">
                                                    <option value = "Enable">Enable(24 hrs)</option>
                                                    <option value = "Disable">Disable</option>
                                                  </select>
                                                </td>
                                                <td id="logUpdate" style="text-align:right;vertical-align:right;" ng-click="updateDebugDays()">
                                                 <a class="btn btn-xs btn-default">
                                                   <span class="glyphicon glyphicon-ok"></span> 
                                                 </a>
                                               </td>
                                             </tr>
                                                 <!-- <tr ng-show="debugValue=='Enable'">
                                                 <td>Days</td>
                                                 <td style="padding-left: 41px;">
                                                   <select ng-model="debugDays" class="_button_select_small">
                                                        <option value = "">Days</option>
                                                        <option value = "1">1 Day</option>
                                                        <option value = "2">2 Days</option>
                                                        <option value = "3">3 Days</option>
                                                        <option value = "4">4 Days</option>
                                                        <option value = "5">5 Days</option>
                                                        <option value = "6">6 Days</option>
                                                        <option value = "7">1 Week</option>
                                                    </select>
                                                 </td>
                                               </tr>    -->
                                             </table>
                                           </div> 
                                         </div>
                                         <!--  Debug Days End -->

                                          <!-- <div id="rightAddress">
                                           <div class="_caption_rightSide">{{vehiLabel}} <?php echo Lang::get('content.description'); ?>/<?php echo Lang::get('content.remarks'); ?></div>
                                           <div align="center" id="vehDesc" style="word-wrap:break-word; font-size:11px;"></div> 
                                         </div>  -->
                                         <table  cellpadding="0" cellspacing="0" class="_alignLeft">
                                          <tr style="background-color: white;">
                                            <td><span style="font-weight: bold;"><?php echo Lang::get('content.description'); ?></span></td>
                                            
                                            <td> <span ng-show="_editValue.isFreezedKm =='yes'" data-toggle="tooltip" data-placement="top" title="{{description}}" style="word-wrap:break-word;;">{{trimText(description)}}</span>
                                              <span ng-hide="_editValue.isFreezedKm =='yes'"  style="word-wrap:break-word;">{{description}}</span>
                                            </td>
                                          </tr>
                                        </table>                                         
                                        <table  cellpadding="0" cellspacing="0" class="_alignLeft">
                                          <tr ng-show="_editValue.isFreezedKm =='yes'">
                                            <td colspan="2">
                                              <div align="center">
                                                <span>
                                                  <a style="cursor: pointer;text-decoration: none;font-weight: bold;color: #2f6581;text-align: center;"  ng-click="changeCollapse()"><?php echo Lang::get('content.view_more'); ?></a>
                                                </span>
                                                <span ng-click="changeCollapse()">
                                                  <button class="pull-right" style="background-color:#ecf7fb;">
                                                    <span ng-bind="collapse ? '-' : '+'"></span>
                                                  </button>
                                                </span>
                                              </div>
                                            </td>
                                          </tr>
                                          <tr ng-show="vehiAssetView && collapse">
                                            <td><?php echo Lang::get('content.freezed_km'); ?></td>
                                            <td ng-show="_editShow"><input type="text" ng-change="changeFreezeShift('freeze')" onkeypress="return (event.charCode == 8 || event.charCode == 0 || event.charCode == 13) ? null : event.charCode >= 48 && event.charCode <= 57" class="_input"  ng-model="_editValue._freezedKm" name="_editValue._freezedKm"  ></td>
                                            <td ng-hide="_editShow"> {{_editValue._freezedKm!=null?_editValue._freezedKm:'-'}}</td>
                                          </tr>
                                          <tr ng-show="vehiAssetView && collapse">
                                            <td><?php echo Lang::get('content.date'); ?></td>
                                            <td ng-show="_editShow"><input type="text" class="_input"  ng-model="_editValue._freezekmdt" onkeydown="validateDate(event)"  id="freezedKmDate" name="_editValue._freezekmdt"  ></td>
                                            <td ng-hide="_editShow"> {{_editValue._freezedKmDate!=''?_editValue._freezedKmDate:'-'}}</td>
                                          </tr>
                                          <tr ng-show="vehiAssetView && collapse">
                                            <td><?php echo Lang::get('content.shift_name'); ?></td>
                                            <td ng-show="_editShow"><select name="selectshift" ng-model="_editValue._shiftNames" ng-change="changeFreezeShift('shift')" data-size="8" data-style="btn-default btn-xs"  class="selectpicker2 dropup"  ng-options="shift for shift in _editValue._shiftList" multiple ></select></td>
                                            <td ng-hide="_editShow" data-toggle="tooltip" data-placement="top" title="{{_editValue._shiftName}}"> {{trimText(_editValue._shiftName)}}</td>
                                          </tr>
                                          <tr ng-show="vehiAssetView && collapse">
                                            <td><?php echo Lang::get('content.date'); ?></td>
                                            <td ng-show="_editShow"><input type="text" class="_input"  ng-model="_editValue._shiftdt" onkeydown="validateDate(event)" id="shiftDate" name="_editValue._shiftdt"  ></td>
                                            <td ng-hide="_editShow"> {{_editValue._shiftDate!=''?_editValue._shiftDate:'-'}}</td>
                                          </tr>
                                        </table>
                                        <table  cellpadding="0" cellspacing="0" class="_alignLeft">
                                          <tr ng-style="{ 'background-color': (_editValue.isFreezedKm =='yes')?'white':'#ecf7fb' }">
                                            <td><span style="font-weight: bold;"><?php echo Lang::get('content.address'); ?></span></td>
                                            
                                            <td> <span ng-show="_editValue.isFreezedKm =='yes'" data-toggle="tooltip" data-placement="top" title="{{address}}" style="word-wrap:break-word;">{{trimText(address)}}</span>
                                              <span ng-hide="_editValue.isFreezedKm =='yes'"  style="word-wrap:break-word;">{{address}}</span>
                                            </td>
                                          </tr>
                                        </table>
                                        <!-- <div id="rightAddress">
                                         <div class="_caption_rightSide"><?php echo Lang::get('content.address'); ?></div>
                                         <div align="center" id="lastseen" style="word-wrap:break-word; font-size:11px;"></div> 
                                       </div> 
                                     -->
                                   </div>

                                 </div>

                               </div>



                               <button id="buttt" type="button" onclick="addr_search()"></button>

                               <div id="searchOsm" style="position: absolute;">
                                <div id="resultsOsm"></div>
                              </div>


                              <div ng-show="siteExec">

                                <ui-select ng-model="sName.selected"  ng-change="siteFindFunc(sName.selected)" theme="bootstrap" ng-disabled="disabled" style="width:200px;
                                font-size:11px !important;
                                opacity: 0.8 !important;
                                position: absolute;
                                top: 48px;
                                float: right;
                                left: 300px; 
                                z-index:1;margin-left: 50px;">
                                <!-- <ui-select-match style="font-size:11px !important;" placeholder="Search Site">{{$select.selected.sName}}</ui-select-match> -->
                                <ui-select-match style="font-size:11px !important;" placeholder="Search Site"><?php echo Lang::get('content.search_site'); ?></ui-select-match>
                                <ui-select-choices   repeat="sit in site_list | filter: $select.search" style="overflow-x:auto;font-size:12px !important;">
                                  <span ng-bind="sit.sName | highlight: $select.sName"></span>
                                </ui-select-choices>
                              </ui-select>

                            </div>
                            <div class="radioButts" style="top: 50px;left: 550px;z-index: 1 !important;">

                             <label><input type="checkbox" name="track" ng-change="getValueCheck(SiteCheckbox.value1)" ng-model="SiteCheckbox.value1"  ng-true-value="'YES'" ng-false-value="'NO'" >
                               <b style="font-size:11px !important;"> <?php echo Lang::get('content.site'); ?></b>
                             </label>
                           </div>

                           <div  id="dropdowns">
                            <select ng-model="mapsHist" ng-change="changeMap(mapsHist)" style="font-size:12px;height: 26px; width: 63px; background: rgba(255, 255, 255, 0.9);border: none !important;">
                              <option value="1"><?php echo Lang::get('content.osm'); ?></a></option>
                              <option value="0"><?php echo Lang::get('content.google'); ?></option>
                            </select>
                          </div>

                          <form id="newLeaf">
                           <input type="text" name="" id="pac-inputs" placeholder="Search Places.." ng-model="siteFind" ng-enter="">
                         </form>

                         <form id="newLeafOsm">
                           <input type="text" name="" id="inputOsm" placeholder="Search Places.." ng-model="siteFind" ng-enter="">
                         </form>

                         <!--  <input id="pac-input" style="margin-left: 180px" class="controls" type="text" placeholder="Enter a location"> -->
                         <!--  <input id="pac-input2" ng-model="siteFind" ng-enter="siteFindFunc(siteFind)" style="margin-left: 200px" class="controls" type="text" placeholder="Enter Site Name">-->

                         <div id="notifyMsg">

                          <div  id="notifyS" class="alert alert-success" style="z-index: 1;position: absolute;margin-top: 50px;left:35%;"><span></span>
                          </div>

                          <div  id="notifyF" class="alert alert-danger" style="z-index: 1;position: absolute;margin-top: 50px;left:35%;"><span></span>
                          </div>

                        </div>

                        <div id="map_osm" style="height:94vh; width:100%; margin-top:38px;"></div>
                        <div id="maploc" style="height:94vh; width:100%; margin-top:38px;"></div>
                        <table cellspacing="0" cellpadding="0" style="width: 75px; z-index: 0; position: absolute; right: 0px; top: 200px;" id="currenStatus">
                          <tbody>
                            <tr><td align="left" style="vertical-align: top;"><div class="gwt-Label" style="padding-top: 6px; background-color: white; font-weight: bold; width: 100%; height: 50px; text-align: center;"><?php echo Lang::get('content.current_status'); ?></div></td></tr>
                            <tr><td align="left" style="vertical-align: top;"><div class="gwt-Label" style="background-color: rgb(76, 76, 76); line-height: 30px; text-align: center; color: white;"><?php echo Lang::get('content.running'); ?></div></td></tr>
                            <tr><td align="left" style="vertical-align: top;"><div class="gwt-Label" style="background-color: rgb(76, 76, 76); text-align: center; color: rgb(120, 194, 59); font-size: 15px; font-weight: bold; width: 100%; height: 40px;" ng-click="onCategoryChange1('M')"><a class="title" href="javascript:;" style="text-decoration: none;color: green">{{movingCount}}</a></div></td></tr>
                            <tr><td align="left" style="vertical-align: top;"><div class="gwt-Label" style="background-color: rgb(76, 76, 76); line-height: 30px; text-align: center; color: white;"><?php echo Lang::get('content.idle'); ?></div></td></tr>
                            <tr><td align="left" style="vertical-align: top;"><div class="gwt-Label" style="background-color: rgb(76, 76, 76); text-align: center; color: rgb(240, 122, 0); font-size: 15px; font-weight: bold; width: 100%; height: 40px;" ng-click="onCategoryChange1('S')"><a class="title" href="javascript:;" style="text-decoration: none;color: rgb(240, 122, 0);">{{idleCount}}</a></div></td></tr>
                            <tr><td align="left" style="vertical-align: top;"><div class="gwt-Label" style="width: 100%; height: 30px; background-color: rgb(76, 76, 76); line-height: 30px; text-align: center; color: white;"><?php echo Lang::get('content.parked'); ?></div></td></tr>
                            <tr><td align="left" style="vertical-align: top;"><div class="gwt-Label" style="background-color: rgb(76, 76, 76); text-align: center; color: rgb(156, 152, 152); font-size: 15px; font-weight: bold; width: 100%; height: 40px;" ng-click="onCategoryChange1('P')"><a class="title" href="javascript:;" style="text-decoration: none;color: rgb(156, 152, 152);">{{parkedCount}}</a></div></td></tr>
                            <tr><td align="left" style="vertical-align: top;"><div class="gwt-Label" style="background-color: rgb(76, 76, 76); line-height: 30px; text-align: center; color: white;"><?php echo Lang::get('content.nodata'); ?></div></td></tr><tr><td align="left" style="vertical-align: top;"><div class="gwt-Label" style="background-color: rgb(76, 76, 76); text-align: center; color: rgb(194, 59, 59); font-size: 15px; font-weight: bold; width: 100%; height: 40px;" ng-click="onCategoryChange1('OFF')"><a class="title" href="javascript:;" style="text-decoration: none;color: rgb(194, 59, 59);">{{attention}}</a></div></td></tr>
                            <tr><td align="left" style="vertical-align: top;"><div class="gwt-Label" style="width: 100%; height: 30px; background-color: white; line-height: 30px; text-align: center; color: black; font-weight: bold;"><?php echo Lang::get('content.total'); ?></div></td></tr>
                            <tr><td align="left" style="vertical-align: top;"><div class="gwt-Label" style="background-color: white; text-align: center; color: rgb(65, 107, 181); font-size: 15px; font-weight: bold; width: 100%; height: 40px;" ng-click="onCategoryChange1('ALL')"><a class="title" href="javascript:;" style="text-decoration: none;color: rgb(65, 107, 181);">{{totalVehicles}}</a></div></td></tr>
                          </tbody>
                        </table>

                        <div ng-show="bottomTableShow" class="box-body" id="statusreport_dashboard">
                          <div id="handle" class="ui-resizable-handle ui-resizable-n" ></div>
                          <table class="table table-striped table-bordered table-condensed table-hover" id="table_address">

                            <tr style="text-align:center">
                              <th width="10%" class="id" custom-sort order="'shortName'" sort="sort" style="text-align:center;background-color:#d0ebef;">{{vehiLabel}} <?php echo Lang::get('content.name'); ?></th>
                              <!-- <th ng-hide="true" style="text-align:center;">Vehicle Name</th> -->
                              <th width="5%" class="id" custom-sort order="'regNo'" sort="sort" style="text-align:center;background-color:#d0ebef;"><?php echo Lang::get('content.reg_no'); ?></th>
                              <th width="10%" class="id" custom-sort order="'date'" sort="sort" style="text-align:center;background-color:#d0ebef;"><?php echo Lang::get('content.Last_Seen'); ?></th>
                              <th width="10%" class="id" custom-sort order="'date'" sort="sort" style="text-align:center;background-color:#d0ebef;"><?php echo Lang::get('content.Last_Comm'); ?></th>
                              <th ng-if="vehiAssetView" width="5%" class="id" custom-sort order="'driverName'" sort="sort" style="text-align:center;background-color:#d0ebef;"><?php echo Lang::get('content.Driver'); ?></th>
                              <!-- <th ng-hide="true">Mobile Number</th> -->
                              <th width="5%" class="id" custom-sort order="'distanceCovered'" sort="sort" style="text-align:center;background-color:#d0ebef;"><?php echo Lang::get('content.Kms'); ?></th>
                              <th width="5%" class="id" custom-sort order="'speed'" sort="sort" style="text-align:center;background-color:#d0ebef;"><?php echo Lang::get('content.speed'); ?></th>
                              <th width="5%" class="id" custom-sort order="'position'" sort="sort" style="text-align:center;background-color:#d0ebef;"><?php echo Lang::get('content.position'); ?></th>
                              <th width="5%" class="id" custom-sort order="'position'" sort="sort" style="text-align:center;background-color:#d0ebef;"><?php echo Lang::get('content.duration'); ?> H:M:S</th>
                              <!-- <th width="5%" class="id" custom-sort order="'status'" sort="sort" style="text-align:center;">GPS</th> -->
                              <!-- <th width="5%" class="id" custom-sort order="'gsmLevel'" sort="sort" style="text-align:center;background-color:#d2dff7;">Sat</th> -->
                              <!-- <th width="5%" class="id" custom-sort order="'loadTruck'" sort="sort" style="text-align:center;background-color:#d2dff7;">Load</th> -->
                              <th width="8%" class="id" custom-sort order="'powerStatus'" sort="sort" style="text-align:center;background-color:#d0ebef;"><?php echo Lang::get('content.powerstatus'); ?></th>
                              <th width="5%" class="id" custom-sort order="'vehicleBusy'" sort="sort" style="text-align:center;background-color:#d0ebef;"><?php echo Lang::get('content.A/C'); ?></th>
                              <th width="5%" class="id" custom-sort order="'temperature'" sort="sort" style="text-align:center;background-color:#d0ebef;"><?php echo Lang::get('content.celsius'); ?></th>
                              <th width="32%" class="id" custom-sort order="'address'" sort="sort" style="text-align:center;background-color:#d0ebef;"><?php echo Lang::get('content.Nearest_Loc'); ?></th>
                            </tr> 
                            <tr ng-repeat="user in locations04.vehicleLocations  | orderBy:natural('shortName') | filter:searchVehi" class=""  style="text-align:center; font-size: 11px" ng-click="genericFunction(user.shortName, user.rowId, 'manualClick')">
                              <td>{{user.shortName}}</td>
                              <!-- <td ng-hide="true"></td> -->
                              <td>{{user.regNo}}</td>
                              <td>{{user.date | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                              <td>{{user.lastComunicationTime | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                              <td ng-if="user.driverName == 'nill' || user.driverName == '' && vehiAssetView==true">--</td>
                              <td ng-if="user.driverName!= 'nill' && user.driverName != '' && vehiAssetView==true">{{user.driverName}}</td>
                              <!-- <td ng-hide="true">{{user.mobileNo}}</td> -->
                              <td>{{user.distanceCovered}}</td>
                              <td>{{user.speed}}</td>

                              <td ng-if="trvShow!=true" ng-switch on="user.position">
                                <span ng-switch-when="S"><img title="<?php echo Lang::get('content.vehicle_standing'); ?>" src="assets/imgs/orange.png"/></span>
                                <span ng-switch-when="M">
                                  <span ng-if="user.color == 'R'"><img src="assets/imgs/red.png"></span>
                                  <span ng-if="user.color == 'G'"><img src="assets/imgs/green.png"></span>
                                </span>
                                <span ng-switch-when="P"><img title="<?php echo Lang::get('content.vehicle_parked'); ?>" src="assets/imgs/flag.png"/></span>
                                <span ng-switch-when="U"><img title="<?php echo Lang::get('content.vehicle_noData'); ?>" src="assets/imgs/gray.png"/></span>
                              </td>

                              <td ng-if="trvShow==true" ng-switch on="user.position">
                                <span ng-switch-when="S"><img title="<?php echo Lang::get('content.vehicle_standing'); ?>" src="assets/imgs/trvMarker2/yellow.png"/></span>
                                <span ng-switch-when="M">
                                 <span ng-if="user.color == 'R'"><img src="assets/imgs/trvMarker2/over.png"></span>
                                 <span ng-if="user.color == 'G'"><img src="assets/imgs/trvMarker2/green.png"></span>
                               </span>
                               <span ng-switch-when="P"><img title="<?php echo Lang::get('content.vehicle_parked'); ?>" src="assets/imgs/trvMarker2/red.png"/></span>
                               <span ng-switch-when="U"><img title="<?php echo Lang::get('content.vehicle_noData'); ?>" src="assets/imgs/trvMarker2/gray.png"/></span>
                             </td>

                             <td ng-switch on="user.position">
                              <span ng-switch-when="S">{{msToTime(user.idleTime)}}</span>
                              <span ng-switch-when="M">{{msToTime(user.movingTime)}}</span>
                              <span ng-switch-when="P">{{msToTime(user.parkedTime)}}</span>
                              <span ng-switch-when="U">{{msToTime(user.noDataTime)}}</span>
                            </td>

                            <td ng-switch on="user.powerStatus">
                             <span ng-switch-when="1"><?php echo Lang::get('content.on'); ?></span>
                             <span ng-switch-when="1.0"><?php echo Lang::get('content.on'); ?></span>
                             <span ng-switch-when="0"><?php echo Lang::get('content.off'); ?></span>
                             <span ng-switch-when="0.0"><?php echo Lang::get('content.off'); ?></span>
                             <span ng-if="user.powerStatus != 0 && user.powerStatus != 1 && user.powerStatus != 0.0 && user.powerStatus != 1.0">{{user.powerStatus}}</span>
                           </td>

                           <td ng-switch on="user.vehicleBusy">
                            <span ng-switch-when="yes"><?php echo Lang::get('content.on'); ?></span>
                            <span ng-switch-when="no"><?php echo Lang::get('content.off'); ?></span>
                          </td>
                          <td>{{user.temperature}}</td>
                          <td ng-if="user.address!=null" address={{user.address}}>{{user.address}}</td>
                          <td style="cursor: pointer;" get-location lat={{user.latitude}} lon={{user.longitude}} index={{$index}} ng-if="user.address==null && mainlist[$index]==null"><?php echo Lang::get('content.click_me'); ?></td>
                          <td style="cursor: pointer;" ng-if="user.address==null && mainlist[$index]!=null">{{mainlist[$index]}}</td>

                        </tr>
                        <tr ng-if="locations04.vehicleLocations==null || locations04.vehicleLocations.length==0">
                          <td colspan="15" class="err"><h5><?php echo Lang::get('content.no_data_found'); ?></h5></td>
                        </tr>
                      </table>    
                    </div>

                  </div>
                </div>
              </div>  
            </div>  
            <div id="mapTable-mapList">
              <div id="flexcolor">
                <div style="float: left; width: 50%; ">
                  <div style=" width: 40px; height: 40px; background-color: #858585; cursor: pointer" ng-click="mapView('home')">
                    <img src="assets/imgs/arrowback.png"/>
                  </div>
                </div>
                <div style="text-align: center;"><h5><b><?php echo Lang::get('content.current'); ?> {{vehiLabel}}s <?php echo Lang::get('content.details'); ?></b></h5> </div>

              </div>
             <!--  <script type="text/javascript">
  
        var headID = document.getElementsByTagName("body")[0];         
        var newScript = document.createElement('script');
        newScript.type = 'text/javascript';
        newScript.id = 'myjQuery';
        newScript.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places';
        // newScript.async = true;
        newScript.defer = true;

        headID.appendChild(newScript);
        console.log(' value '+newScript)

      </script> -->

      <div id="scrollTable" ng-show="tabView">
       <table class="table table-striped table-bordered table-condensed table-hover" >
         <tr style="text-align:center">
          <th width="6%" style="text-align:center;" class="id" custom-sort order="'shortName'" sort="sort"><div style="width: 100px;">{{vehiLabel}} <?php echo Lang::get('content.name'); ?></div></th>
          <th width="10%" style="text-align:center;" class="id" custom-sort order="'address'" sort="sort"><div style="width: 300px;"><?php echo Lang::get('content.Nearest_Loc'); ?></div></th>
          <th width="6%" style="text-align:center;" class="id" custom-sort order="'position'" sort="sort"><div style="width: 70px;"><?php echo Lang::get('content.position'); ?></div></th>
          <!-- <th style="text-align:center;" class="id" custom-sort order="'status'" sort="sort"><div style="width: 70px;">GPS</div></th>-->
          <th width="5%" style="text-align:center;" class="id" custom-sort order="'gsmLevel'" sort="sort"><div style="width: 70px;"><?php echo Lang::get('content.Sat'); ?></div></th>
          <th width="6%" style="text-align:center;" class="id" custom-sort order="'ignitionStatus'" sort="sort"><div style="width: 70px;"><?php echo Lang::get('content.ign'); ?></div></th>
          <th width="6%" style="text-align:center;" class="id" custom-sort order="'lastSeen'" sort="sort"><div style="width: 100px;"><?php echo Lang::get('content.Last_Seen'); ?></div></th>
          <th width="5%" style="text-align:center;" class="id" custom-sort order="'distanceCovered'" sort="sort"><div style="width: 50px;"><?php echo Lang::get('content.km'); ?></div></th>
          <th width="6%" style="text-align:center;" class="id" custom-sort order="'speed'" sort="sort"><div style="width: 70px;"><?php echo Lang::get('content.speed'); ?></div></th>
          <th width="6%" style="text-align:center;"><div style="width: 70px;"><?php echo Lang::get('content.glink'); ?></div></th>
          <th width="6%" style="text-align:center;" class="id" custom-sort order="'powerStatus'" sort="sort"><div style="width: 100px;"><?php echo Lang::get('content.powerstatus'); ?></div></th>
          <th width="6%" style="text-align:center;" class="id" custom-sort order="'deviceStatus'" sort="sort"><div style="width: 100px;"><?php echo Lang::get('content.devbattery'); ?></div></th>
          <th width="6%" style="text-align:center;" class="id" custom-sort order="'regNo'" sort="sort"><div style="width: 100px;"><?php echo Lang::get('content.reg_no'); ?></div></th>
          <th width="6%" style="text-align:center;" class="id" custom-sort order="'driverName'" sort="sort"><div style="width: 100px;"><?php echo Lang::get('content.Driver'); ?> <?php echo Lang::get('content.name'); ?></div></th>
          <th width="6%" style="text-align:center;" class="id" custom-sort order="'totalTruck'" sort="sort"><div style="width: 100px;"><?php echo Lang::get('content.Load'); ?></div></th>
          <th width="6%" style="text-align:center;" class="id" custom-sort order="'fuelLitre'" sort="sort" ng-if="groupFuel.length"><div style="width: 100px;"><?php echo Lang::get('content.cur_fuel'); ?><?php echo Lang::get('content.ltrs'); ?></div></th>
          <th width="6%" style="text-align:center;" class="id" custom-sort order="'temperature'" sort="sort"><div style="width: 100px;"><?php echo Lang::get('content.temperature'); ?></div></th>
          <th width="6%" style="text-align:center;" class="id" custom-sort order="'vehicleBusy'" sort="sort"><div style="width: 100px;"><?php echo Lang::get('content.ac/hire'); ?></div></th>
        </tr>
        <tr ng-repeat="user in locations04.vehicleLocations track by $index | orderBy:natural(sort.sortingOrder):sort.reverse" style="text-align:center; cursor: pointer" ng-mouseover="mouseJump(user)" ng-click="tabletd(user)">
          <td>{{user.shortName}}</td>
          <td>
            <div>{{starSplit(user.address)[0]}}</div>
            <div>{{starSplit(user.address)[1]}}</div>
          </td>

          <td ng-if="trvShow!=true" ng-switch on="user.position">
            <span ng-switch-when="S"><img title="<?php echo Lang::get('content.vehicle_standing'); ?>" src="assets/imgs{{markerPath}}/orange.png"/></span>
            <span ng-switch-when="M">
              <span ng-if="user.color == 'R'"><img src="assets/imgs/red.png"></span>
              <span ng-if="user.color == 'G'"><img src="assets/imgs/green.png"></span>
            </span>
            <span ng-switch-when="P"><img title="<?php echo Lang::get('content.vehicle_parked'); ?>" src="assets/imgs{{markerPath}}/flag.png"/></span>
            <span ng-switch-when="U"><img title="<?php echo Lang::get('content.vehicle_noData'); ?>" src="assets/imgs{{markerPath}}/gray.png"/></span>
          </td>

          <td ng-if="trvShow==true" ng-switch on="user.position">
            <span ng-switch-when="S"><img title="<?php echo Lang::get('content.vehicle_standing'); ?>" src="assets/imgs/trvMarker2/yellow.png"/></span>
            <span ng-switch-when="M">
             <span ng-if="user.color == 'R'"><img src="assets/imgs/trvMarker2/over.png"></span>
             <span ng-if="user.color == 'G'"><img src="assets/imgs/trvMarker2/green.png"></span>
           </span>
           <span ng-switch-when="P"><img title="<?php echo Lang::get('content.vehicle_parked'); ?>" src="assets/imgs/trvMarker2/red.png"/></span>
           <span ng-switch-when="U"><img title="<?php echo Lang::get('content.vehicle_noData'); ?>" src="assets/imgs/trvMarker2/gray.png"/></span>
         </td>

                            <!-- <td ng-switch on="user.status">
                                <span ng-switch-when="OFF"><img title="GPS OFF" src="assets/imgs/gof.png"/></span>
                                <span ng-switch-when="ON"><img title="GPS ON" src="assets/imgs/gon.png"/></span>
                              </td> -->
                              <td>{{user.gsmLevel}}</td>
                              <td ng-switch on="user.ignitionStatus">
                                <span ng-switch-when="OFF"><img src="assets/imgs/no.png"/></span>
                                <span ng-switch-when="ON"><img src="assets/imgs/yes.png"/></span>

                              </td>
                              <td>{{user.date | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                              <td>{{user.distanceCovered}}</td>
                              <td>{{user.speed}}</td>
                              <td><a href="https://www.google.com/maps?q=loc:{{user.latitude}},{{user.longitude}}" target="_blank"><?php echo Lang::get('content.link'); ?></a></td>
                              <td>{{user.powerStatus}}
                                <!-- <span ng-switch-when="OFF"><img src="assets/imgs/wr.png"/></span>
                                  <span ng-switch-when="ON"><img src="assets/imgs/rt.png"/></span> -->
                                </td>
                                <td>{{user.deviceStatus}}%</td>
                                <td>{{user.regNo}}</td>
                                <td>{{user.driverName}}</td>
                                <td>{{valueCheck(user.totalTruck)}}</td>
                                <!--  <td>{{valueCheck(user.fuelLitre)}}</td> -->
                                <td ng-switch on="user.fuel" ng-if="groupFuel.length">
                                  <span ng-switch-when="yes">{{user.fuelLitre}}</span>
                                  <span ng-switch-when="no"> - </span>
                                </td>
                                <td>{{user.temperature}}</td>
                                <td ng-switch on="user.vehicleBusy">
                                  <span ng-switch-when="yes" style="color: #ff0045"><?php echo Lang::get('content.busy'); ?></span>
                                  <span ng-switch-when="no" style="color: #00ce2f"><?php echo Lang::get('content.available'); ?></span>
                                </td>

                            <!-- <td style="cursor: pointer;" get-location lat={{user.latitude}} lon={{user.longitude}} index={{$index}} ng-if="user.address==null && mainlist[$index]==null">Click Me</td>
                            <td style="cursor: pointer;" ng-if="user.address==null && mainlist[$index]!=null">{{mainlist[$index]}}</td>
                            <td><a href="https://www.google.com/maps?q=loc:{{user.latitude}},{{user.longitude}}" target="_blank">Link</a></td> -->                                                      
                          </tr>
                        </table>        
                      </div>
                    </div>
                  </div> 
                  <div id="expiryMsg" class="modal fade" role="dialog" data-backdrop="false" style="top:20%;left: 10%;">
                    <div class="modal-dialog" style="z-index:9999;">

                      <!-- Modal content-->
                      <div class="modal-content">
                        <div class="modal-header" style="background-color: #196481;">
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                          <h4 class="modal-title">Licence Expiry Message</h4>
                        </div>
                        <div class="modal-body">
                          <p style="color:red;">Licence is expired. Please contact sales for renewing licence.</p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                      </div>

                    </div>
                  </div>
                  <script type="text/javascript">
    // $('.selectpicker').selectpicker();

    var userName    =  "<?php echo Auth::user()->username; ?>";
    var userIpaddr = "<?php echo $_SERVER['REMOTE_ADDR'] ?>";
    localStorage.setItem('ipAddress',userIpaddr);
    localStorage.setItem('userIdName', JSON.stringify('username,'+userName));
    // var userNameVal       =  localStorage.getItem('userIdName').split(',');
    // var userVals          =  userNameVal[1].split('"');
    // var userName          =  userVals[0];
    var postValue = {
      'id': userName,
      '_token': '<?php echo csrf_token() ?>',
    };
    if(localStorage.getItem('apiKey')==null){
      $.post('<?php echo route("ajax.apiKeyAcess") ?>',postValue)
      .done(function(data) {


        localStorage.setItem('apiKey', JSON.stringify(data));
        var apikey_url = JSON.parse(data);
        console.log('Api_Key : '+apikey_url);
        var url = "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places";

        if(apikey_url != null || apikey_url != undefined) {
         url = "https://maps.googleapis.com/maps/api/js?key="+apikey_url+"&libraries=places"; 
       }

     }).fail(function() {
      console.log("fail");
    });}
     else {
      var apikey_url = JSON.parse(localStorage.getItem('apiKey'));
      console.log('Api_Key : '+apikey_url);
      var url = "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places";

      if(apikey_url != null || apikey_url != undefined) {
       url = "https://maps.googleapis.com/maps/api/js?key="+apikey_url+"&libraries=places"; 
       //url = "https://maps.googleapis.com/maps/api/js?key=AIzaSyABtcdlhUVm5aKq7wAlMatI56DKanIKS6o&libraries=places"; 
     }
   }
   if(localStorage.getItem('dealerName')==null){
    $.post('<?php echo route("ajax.dealerAcess") ?>',postValue)
    .done(function(data) {

        //alert(data);
        localStorage.setItem('dealerName', data);

      }).fail(function() {
        console.log("fail");
      }); 
    }
    if(localStorage.getItem('fCode')==null){
      var userIP=localStorage.getItem('myIP');
      var postVal = {
        'id':userName,
        'userIP':userIP,
        '_token': '<?php echo csrf_token() ?>',
      };


      $.post('<?php echo route("ajax.fcKeyAcess") ?>',postVal)
      .done(function(data) {

        localStorage.setItem('fCode',data);

      }).fail(function() {
        console.log("fcode fail..");
      });}
      //function for google language
      function googleTranslateElementInit() {
       new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
     }



     function loadJsFilesSequentially(scriptsCollection, startIndex, librariesLoadedCallback) {
       if (scriptsCollection[startIndex]) {
         var fileref = document.createElement('script');
         fileref.setAttribute("type","text/javascript");
         fileref.setAttribute("src", scriptsCollection[startIndex]);
         fileref.onload = function(){
           startIndex = startIndex + 1;
           loadJsFilesSequentially(scriptsCollection, startIndex, librariesLoadedCallback)
         };
         document.getElementsByTagName("head")[0].appendChild(fileref);
       } else {
         librariesLoadedCallback();
       }
     }


// An array of scripts to load in order
var scriptLibrary = [];
scriptLibrary.push("https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js");
   // scriptLibrary.push("https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js");
   scriptLibrary.push("https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.js");
   scriptLibrary.push("https://code.jquery.com/jquery-1.12.4.js");
   scriptLibrary.push("https://code.jquery.com/ui/1.12.1/jquery-ui.js");
   scriptLibrary.push("assets/js/bootstrap.min.js");
 //scriptLibrary.push("assets/js/ui-bootstrap-0.6.0.min.js");
 scriptLibrary.push("assets/ui-drop/select.min.js");
 scriptLibrary.push("assets/js/loaders.css.js");
 scriptLibrary.push("assets/js/moment.js");
   //scriptLibrary.push("assets/js/bootstrap-datetimepicker.js");
   scriptLibrary.push("../resources/views/reports/datepicker/bootstrap-datetimepicker.js");
   scriptLibrary.push(url);
  //scriptLibrary.push("https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places");
  scriptLibrary.push("https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js");
  scriptLibrary.push("assets/js/infobubble.js");
  // scriptLibrary.push("https://cdn.rawgit.com/googlemaps/v3-utility-library/master/infobox/src/infobox.js");
  scriptLibrary.push("assets/js/angular-translate.js");
  scriptLibrary.push("assets/js/markerwithlabel.js");
   /*
   scriptLibrary.push("assets/js/leaflet.js");
   scriptLibrary.push("assets/js/leaflet.label.js");
   scriptLibrary.push("assets/js/leaflet.markercluster.js");
   */
   scriptLibrary.push("assets/js/leaflet.js?no_cache=" + new Date().getTime());
   scriptLibrary.push("assets/js/leaflet.label.js?no_cache=" + new Date().getTime());
   scriptLibrary.push("assets/js/leaflet.markercluster.js?no_cache=" + new Date().getTime());
   scriptLibrary.push("assets/js/highcharts_new.js");
   scriptLibrary.push("assets/js/highcharts-more_new.js");
   scriptLibrary.push("assets/js/solid-gauge_new.js");
   scriptLibrary.push("assets/js/naturalSortVersionDatesCaching.js");
   scriptLibrary.push("assets/js/infobox.js");

 //scriptLibrary.push("assets/js/naturalSortVersionDates.js");
 scriptLibrary.push("assets/js/landNew.js?v=<?php echo Config::get('app.version');?>");
 scriptLibrary.push("assets/js/bootstrap-select.js");


// Pass the array of scripts you want loaded in order and a callback function to invoke when its done
loadJsFilesSequentially(scriptLibrary, 0, function(){
// application is "ready to be executed"
// startProgram();
}); 
$(document).ready(function(){
  if(document.location.host!='gpsvts.vamosys.com'){
    $('.helpVideo').css('display','none');
  }
})


// var blink = document.getElementById('blink');
//         setInterval(function () {
//             blink.style.opacity = 
//             (blink.style.opacity == 0 ? 1 : 0);
//             blink.style.color='red';
//         }, 1000);

function validateDate(event){
  let valid = ( event.keyCode == 8 || event.keyCode==46 );
  if (valid) {
    event.preventDefault();
  }
}

function validateval(event){
  let valid = ( (event.keyCode >= 48 && event.keyCode <= 57) || event.keyCode == 186 || event.keyCode == 8|| event.keyCode == 13 || event.keyCode == 0 || (event.keyCode >= 37 && event.keyCode <= 40)  );
  if (!valid) {
    event.preventDefault();
  }
}

</script>
</body>
</html>
