<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <title><?php echo Lang::get('content.gps'); ?></title>
  <link rel="shortcut icon" href="assets/imgs/tab.ico">
  <link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
  <link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">
  <link href="assets/css/jVanilla.css" rel="stylesheet">
  <link href="assets/css/simple-sidebar.css" rel="stylesheet">
  <link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
  <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/css/bootstrap-select.css" rel="stylesheet">
  <link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
  <link href="../resources/views/assets/css/datebutton.css" rel="stylesheet" type="text/css" />
  <link href="../resources/views/reports/table/font-awesome.css" rel="stylesheet" type="text/css">
  <link href="assets/css/web.css" rel="stylesheet" /> <!-- added for table freezing -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Latest compiled and minified JavaScript -->
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
  <script src="//kendo.cdn.telerik.com/2016.3.914/js/kendo.all.min.js"></script>
  <script src="assets/js/gridviewscroll.js"></script> <!-- added for table freezing -->

  <style>
    .empty{
      height: 1px; width: 1px; padding-right: 30px; float: left;
    }
    .table-striped > tbody > tr:nth-child(even) > td, .table-striped > tbody > tr:nth-child(even) > th {
      background-color: #ffffff;
    }
    .fa-cloud-download {
      color: white;
      font-size: 1.5em;
      margin-top: 5px;
      cursor: pointer;
    }
    .upload-input select {height: 35px;}

    .stroke-transparent {
     -webkit-text-stroke: 1px #000;
     -webkit-text-fill-color: transparent;
   }

   body{
    font-family: 'Lato', sans-serif;
  } 

  .col-md-12 {
    width: 98% !important;
    left: 15px !important;
    padding-left: 20px !important;
  }

  .feeds li .col1>.cont {
    float: left;
    margin-right: 75px;
    overflow: hidden;
  }
  .feeds li .col1, .feeds li .col1>.cont>.cont-col2 {
    width: 98%;
    float: left;
  }
  .feeds li .col2 {
    float: left;
    width: 75px;
    margin-left: -105px;
  }
  .feeds li .col3 {
    float: left;
  }
  .feeds li .col1 {
    clear: both;
  }
/*.feeds li .col1 {
    color: #262829;
    }*/
    .feeds li .col2>.date {
      padding: 4px 9px 5px 4px;
      text-align: right;
      font-style: italic;
      color: #c1cbd0;
      white-space: nowrap;
    }
    .ml-30{
      margin-left: -25px;
    }

  </style>
  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-2X3F316479');

  </script>
</head>
<div id="preloader" >
  <div id="status">&nbsp;</div>
</div>
<div id="preloader02" >
  <div id="status02">&nbsp;</div>
</div>

<body ng-app="mapApp">
  <div ng-controller="mainCtrl" class="bg_color ng-cloak">
    <?php include('menubarlist.php');?>
    <div class="wrapper" style="margin-left:5%;">


      <div id="testLoad"></div>

      <div id="page-content-wrapper">
        <div class="container-fluid">
          <div class="panel panel-default"></div>   
        </div>
      </div>

      <div class="col-md-12">
       <div class="box box-primary" style="padding-top: 5px;">
        <!-- <div class="row"> -->
          <div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
            <h3 class="box-title" ng-hide="isGpsDayReport"><span><?php echo Lang::get('content.geofence_shift'); ?><?php echo Lang::get('content.report'); ?></span>
              <span><i class="fa fa-info-circle" style="color:#2f6581;font-size: 17px;margin-left: 2px;" aria-hidden="true" title="Some values / data will be different from live / intra day reports. This is due to late arrival of stored (out of order) data."></i></span>
            </h3>
            <h3 class="box-title" ng-show="isGpsDayReport"><span><?php echo Lang::get('content.gps_per_day'); ?><?php echo Lang::get('content.report'); ?></span>
              <span><i class="fa fa-info-circle" style="color:#2f6581;font-size: 17px;margin-left: 2px;" aria-hidden="true" title="Some values / data will be different from live / intra day reports. This is due to late arrival of stored (out of order) data."></i></span>
            </h3>
          </div>
          <div class="row">

            <div class="col-md-1" align="center"></div>
            <div class="col-md-1" align="center">
              Organization  
            </div>
            <div class="col-md-2">

              <div class="form-group">
                <select class="selectpicker" ng-model="selectedOrg" ng-change="orgChanged()" data-live-search="true">
                  <option ng-repeat="org in orgList" value="{{org}}">{{org}}</option>
                </select>
                        <!-- <select ng-model="selectedOrg" class="form-control" ng-change="orgChanged()">
                          <option ng-repeat="org in orgList" value="{{org}}">{{org}}</option>
                        </select> -->
                      </div>
                    </div>
                    <div class="col-md-2" align="center">
                      <div class="form-group">
                        <div class="input-group datecomp">
                          <input type="text" ng-model="uiDate.fromdate" class="form-control placholdercolor" id="dateFrom"  placeholder="<?php echo Lang::get('content.from_date'); ?>">

                        </div>
                      </div>
                    </div> 
                    <div class="col-md-2" align="center" ng-hide="isGpsDayReport">
                      <div class="form-group">
                        <div class="input-group datecomp">
                          <input type="text" ng-model="uiDate.todate" class="form-control placholdercolor" id="dateTo" placeholder="<?php echo Lang::get('content.to_date'); ?>">
                        </div>
                      </div>
                    </div>
                    
                    
                    <div class="col-md-1" align="center">
                      <button style="margin-left: -42%; padding : 5px" onClick="undo()" ng-click="submitFunction()"><?php echo Lang::get('content.submit'); ?></button>
                    </div>

                  </div>

                  <div class="row">
                    <div class="col-md-1" align="center"></div>
                    <div class="col-md-2" align="center">
                      <div class="form-group"></div>
                    </div>
                  </div>
                </div>

              </div>

              <div class="col-md-12" style="border-color: unset!important;">
                <div class="box box-primary" style="min-height:570px;border: none;">
                  <div class="pull-right" style="margin-right: 10px;margin-top: 10px;" ng-hide="geoShiftError||!isGpsDayReport">           
                    <img style="cursor: pointer;" ng-click="exportgpsPerDayreport()" src="../resources/views/reports/image/xls.png" />
                  </div> 
                  <div class="box-body">

                    <h5 ng-if="geoShiftError" style="color: red;" class="text-center">{{geoShiftError}}</h5>
                    <div class="panel panel-default" ng-repeat="shift in OrgShift" style="margin-bottom: 10px;" ng-hide="isGpsDayReport">
                      <div class="panel-heading">
                        <img style="cursor: pointer;width: 35px;height: 35px;margin-left: 10px;float: left;" src="assets/imgs/starttime.svg" />
                        <span style="float: left;">Start Time : {{shift.startTime}}</span>
                        <img style="cursor: pointer;width: 35px;height: 35px;margin-left: 30px;float: left;" src="assets/imgs/endtime.svg" />
                        <span style="float: left;">End Time : {{shift.endTime}}</span>
                        <img style="cursor: pointer;width: 35px;height: 35px;margin-left: 30px;float: left;" src="assets/imgs/shift.svg" />
                        <span style="width: 300px;float: left;"> Shift Type : {{shift.shiftName}} </span>
                        <img style="cursor: pointer;width: 35px;height: 35px;float: left;" src="assets/imgs/city1.svg" />
                        Site Name : {{shift.siteName}}
                        <i class="fa fa-cloud-download stroke-transparent pull-right" ng-hide="(fromdate==todaydate && shift.endTime>time )" ng-click="getOrgShiftTimeReportExcel(shift.startTime,shift.endTime,shift.shiftName,'no')"></i>
                      </div>
                    </div> 
                    <div id="gpsPerDayreport" style="margin-top: 30px;">
                      <table ng-hide="!isGpsDayReport||geoShiftError" cellspacing="0" id="gvMain" style="width: 100%; border-collapse: collapse;">
                        <tr class="GridViewScrollHeader">
                          <th scope="col" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">
                            <?php echo Lang::get('content.vehicle'); ?> Number
                          </th>
                          <th scope="col" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">
                            <span style="display: grid;">
                             <span><?php echo Lang::get('content.vehicle'); ?>   </span>
                             <span>Category</span>  
                           </span>
                         </th>
                         <th scope="col" ng-repeat="shift in OrgShift" style="font-weight:unset;font-size:12px;background-color:#ecf7fb;">
                          <span style="display: grid;">
                           <span>{{shift.shiftName}} </span>
                           <span>({{shift.startTime}} - {{shift.endTime}})</span>  
                         </span> 
                       </th>    
                     </tr>
                     <tr ng-repeat="vehicle in shiftDatetails"  class="GridViewScrollItem">
                      <td>{{vehicle.vehicleName}}</td>  
                      <td>{{vehicle.vehicleModel}}</td> 
                      <td ng-repeat="time in vehicle.shiftTimeArr track by $index">{{ time | date:'HH:mm'}}</td>
                    </tr> 
                  </table>

                </div>
              </div>
            </div>
          </div>
          <div id="geoshift" ng-show="false">
            <div ng-repeat="DateBasedShiftData in shiftDatetails" ng-if='!DateBasedShiftData.error'>
              <table>
                <tr>
                  <th>Date </th>
                  <th>{{DateBasedShiftData.date | date:'dd/MM/yyyy'}} </th>
                </tr>
                <tr>
                  <th>Shift Name</th>
                  <th>{{DateBasedShiftData.shiftName}}</th>
                </tr>
              </table>
              <table ng-repeat="siteDetails in DateBasedShiftData.siteDetails">
                <tr>
                 <th>Site Name</th>
                 <th>{{siteDetails.siteName}}</th>
               </tr>
               <tr>
                <th>Time </th>
                <th>From Time</th>
                <th>To Time</th>
              </tr>
              <tr>
                <th> </th>
                <th>{{DateBasedShiftData.shiftStartTime | date:'HH:mm' }}</th>
                <th>{{DateBasedShiftData.shiftEndTime | date:'HH:mm' }}</th>
              </tr>
              <tr>
                <th>Total Vehicle </th>
                <th>Vehicle Inside Site</th>
                <th>Vehicle Outside Site</th>
              </tr>
              <tr>
                <th>{{siteDetails.totalVehicles}}</th>
                <th>{{siteDetails.vehiclesInsideCount}}</th>
                <th>{{siteDetails.vehiclesOutsideCount}}</th>
              </tr>
              <tr></tr>
              <tr>
                <th>Vehicle Inside Site</th>
              </tr>
              <tr>
                <th>S.No</th>
                <th>Vehicle Number</th>
                <th>Vehicle Model</th>
                <th>Reached Time</th>
              </tr>
              <tr ng-repeat="siteInside in siteDetails.siteInside">
                <th>{{$index+1}}</th>
                <th>{{siteInside.vehicleName}}</th>
                <th>{{siteInside.vehicleModel}}</th>
                <th>{{siteInside.startTime | date:'yyyy-MM-dd HH:mm:ss'}}</th>
              </tr>
              <tr></tr>
              <tr>
                <th>Vehicle Outside Site</th>
              </tr>
              <tr>
                <th>S.No</th>
                <th>Vehicle Name</th>
                <th>Vehicle Model</th>
                <th>Not Reported Vehicle distance from Site</th>
              </tr>
              <tr ng-repeat="siteOutSide in siteDetails.siteOutSide">
                <th>{{$index+1}}</th>
                <th>{{siteOutSide.vehicleName}}</th>
                <th>{{siteOutSide.vehicleModel}}</th>
                <th>{{siteOutSide.address}}</th>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>

    <script src="assets/js/static.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
    <script src="assets/js/angular-translate.js"></script>
    <script src="../resources/views/reports/customjs/moment.js"></script>
    <script src="../resources/views/reports/customjs/FileSaver.js"></script>
    <script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
    <script src="../resources/views/reports/customjs/FileSaver.js"></script>
    <script src="assets/js/bootstrap-select.js"></script>
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/services.js"></script>
    <script src="assets/js/geofenceshift.js?v=<?php echo Config::get('app.version');?>"></script>
    
    <script>


      $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
      });  
      $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
      });
      $('.selectpicker').selectpicker();
    </script>
    <script type="text/javascript">
      var gridViewScroll = null;
      window.onload = function () {
        gridViewScroll = new GridViewScroll({
          elementID: "gvMain",
          width:  window.screen.width-200,
          height: window.screen.height-400,
          freezeColumn: true,
                //freezeFooter: true,
                freezeColumnCssClass: "GridViewScrollItemFreeze",
                //freezeFooterCssClass: "GridViewScrollFooterFreeze",
                freezeHeaderRowCount: 1,
                freezeColumnCount: 2,
                onscroll: function (scrollTop, scrollLeft) {
                  console.log(scrollTop + " - " + scrollLeft);
                }
              });
        gridViewScroll.enhance();
      }
      function enhance() {
        gridViewScroll.enhance();
      }
      function undo() {
        gridViewScroll.undo();
      }
    </script>
  </body>
  
  </html>
