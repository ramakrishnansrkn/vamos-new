<!DOCTYPE html>
<html lang="en" ng-app="mapApp">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="PRAGMA" content="NO-CACHE">
<meta name="description" content="">
<meta name="author" content="Satheesh">
<title><?php echo Lang::get('content.gps'); ?></title>
<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<!--<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css"/>-->
<link href="assets/css/MarkerCluster.css" rel="stylesheet">
<link href="assets/css/MarkerCluster.Default.css" rel="stylesheet">
<link href="assets/css/leaflet.css" rel="stylesheet">
<link href="assets/css/leaflet.label.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="assets/css/popup.bootstrap.min.css">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<link href="assets/font-awesome-4.2.0/css/font-awesome.css" rel="stylesheet">
<link href="assets/ui-drop/select.min.css" rel="stylesheet">

<style type="text/css">

body {
font-family: 'Lato', sans-serif;
/*font-weight: bold;*/  
/*font-family: 'Lato', sans-serif;
font-family: 'Roboto', sans-serif;
font-family: 'Open Sans', sans-serif;
font-family: 'Raleway', sans-serif;
font-family: 'Faustina', serif;
font-family: 'PT Sans', sans-serif;
font-family: 'Ubuntu', sans-serif;
font-family: 'Droid Sans', sans-serif;
font-family: 'Source Sans Pro', sans-serif;
*/
}

button[disabled], html input[disabled] {
    background-color: #bdbdbd !important;
}

#container, #speedGraph{
  max-width: 480px !important;
  max-height: 200px !important;
  min-width: 480px; height: 200px; margin: 0 auto
}

.box > .loading-img, .loading-img,#status, #status02{
   width: 1px !important;height: 1px !important;z-index: 9999;position: absolute;left: 50%;top: 50%;background-image: url(../imgs/loading.gif);background-repeat: no-repeat;background-position: center;margin: -100px 0 0 -100px; border-radius: 3px;
}

.leaflet-top,
.leaflet-bottom {
    position: absolute;
    z-index: 1 !important;
    pointer-events: none;
 }

.btn {
    display: inline-block;
    padding: 2px 12px;
    margin-bottom: 0;
    font-size: 12px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 4px;
}

.btn-primary {
    color: #060606;
    background-color: #ffffff;
    border-color: #ffffff;
 } 

 .btns {
    display: inline-block;
    padding: 4.7px 8.5px;
    margin-bottom: 0;
    font-size: 11px;
    font-weight: 200;
    line-height: 1.42857143;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    background-image: none;
    border: 1px solid transparent;
    border-radius: 2.5px;
    background-color: #fff;
 }

 .btn-primary.active.focus, .btn-primary.active:focus, .btn-primary.active:hover, .btn-primary:active.focus, .btn-primary:active:focus, .btn-primary:active:hover, .open>.dropdown-toggle.btn-primary.focus, .open>.dropdown-toggle.btn-primary:focus, .open>.dropdown-toggle.btn-primary:hover {
     color: #060606;
     background-color: #ffffff;
     border-color: #ffffff;
 }

  .btn-primary:hover {
    color: #060606;
    background-color: #ffffff;
    border-color: #ffffff;
  }

  .dropdown-menu {
    position: absolute;
    top: 100%;
    left: 0;
    z-index: 1000;
    display: none;
    float: left;
    min-width: 90px;
    padding: 0px 0;
    margin: 2px 0 0 16px;
    font-size: 14px;
    text-align: left;
    list-style: none;
    background-color: #fff;
    -webkit-background-clip: padding-box;
    background-clip: padding-box;
    border: 1px solid #ccc;
    border: 1px solid rgba(0,0,0,.15);
    border-radius: 4px;
    -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
    box-shadow: 0 6px 12px rgba(0,0,0,.175);
  }

  .dropdown-menu>li>a {
    display: block;
    padding: 1px 16px 2px;
    clear: both;
    font-weight: 400;
    line-height: 1.42857143;
    color: #333;
    white-space: nowrap;
  }


  #dropdownss{
      box-shadow: 0px 1px 2px rgba(0, 0, 0, 0.2); 
   /* top: 48px;
      left: 12%;*/
      position: absolute;
      z-index: 1;
    /* opacity: 0.9;
      position: fixed;*/
      top: 10px;
      left: 130px; 
   /* z-index:99;
      background: #fff;
      padding:2px;
      border-radius: 2px;*/
      cursor: pointer;

   /* height: 26px;
      width: 64px;
      background: rgba(255, 255, 255, 0.9);*/

 }

  .ui-select-bootstrap .ui-select-toggle {
    position: relative !important;
    padding-right: 22px !important;
    height:25px !important;
}

button, input, select, textarea {
    font-family: inherit !important;
    font-size: 12px !important;
    line-height: inherit !important;
}

.polygonOsm{ 
   stroke: green;
   fill: blue;
  /* stroke-dasharray: 10,10; */
   stroke-width: 1;  
}

 .polygonLabel{ 
  color:red;
  background-color:'#f1f1f1';
  z-index: 1;
}


.polygonLabel:before,
.polygonLabel:after {
    border-top: none;
    border-bottom:none;
    content: none;
    position:none;
    top: none;
}

.polygonLabel:before {
    border-right: none;
    border-right-color:none;
    left:none;
}

.polygonLabel:after {
    border-left: none;
    border-left-color: none;
    right:none;
}

.btn.btn-outline.green-haze {
    border-color: #44b6ae;
    color: #44b6ae;
    background: 0 0;
}

.btn-group>.btn-group:first-child:not(:last-child)>.btn:last-child, .btn-group>.btn-group:first-child:not(:last-child)>.dropdown-toggle, .btn-group>.btn:first-child:not(:last-child):not(.dropdown-toggle) {
    border-bottom-right-radius: 0;
    border-top-right-radius: 0;
}

.btn-group > .btn:first-child:not(:last-child):not(.dropdown-toggle) {
    border-top-right-radius: 0;
    border-bottom-right-radius: 0;
}

</style>
<script async src="https://www.googletagmanager.com/gtag/js?id=G-2X3F316479"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-2X3F316479');
</script>
</head>

<!-- <div id="preloader" > -->
  <div id="status" >
    <div class="showLoading">
    <h2><?php echo Lang::get('content.please_wait'); ?></h2><svg version="1.1" id="loader-1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     width="40px" height="40px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
  <path fill="#000" d="M25.251,6.461c-10.318,0-18.683,8.365-18.683,18.683h4.068c0-8.071,6.543-14.615,14.615-14.615V6.461z">
    <animateTransform attributeType="xml"
      attributeName="transform"
      type="rotate"
      from="0 25 25"
      to="360 25 25"
      dur="0.6s"
      repeatCount="indefinite"/>
    </path>
  </svg>
          </div>
    </div>

<!-- </div> -->
<!-- <div id="preloader02" >
       <div id="status02">&nbsp;</div>
     </div> -->

<body ng-controller="mainCtrl" class="ng-cloak" >
   <?php include('menubarlist.php');?>
  <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="panel panel-default">
                </div>          
            </div>
        </div>
        <div id="testLoad" style="margin-left: 6%"></div>

        <div class="box-tools pull-right">
            <!-- <div class="btn-group" style="right: 21%;margin-top: 22%">
                                <a class="btn btn-circle green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="margin-bottom: 10px;border-radius: 15px;"> 
                                                    <i class="icon-home"></i> {{trimColon(groupname)}}
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                
                                                <ul class="dropdown-menu pull-right" role="menu" >
                                                    
                                                    <li class="nav-item start" ng-class="(grp.grpId == groupid)?'active':''" ng-repeat="grp in orgIds | filter:grpName"  ng-click="groupSelection(grp.grpName, grp.grpId)">
                                        <a href="javascript:void(0);" class="nav-link " >
                                          <span class="title" style="overflow: hidden;text-overflow: ellipsis;"  ng-bind="grp.grpName"></span>
                                        </a>
                                                 </li>
                                             </ul>
                                            </div> -->
                                          
                        <!-- <div class="btn-group" style="right: 21%;margin-top: 22%" >
                                <a class="btn btn-circle green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="margin-bottom: 10px;border-radius: 15px;"> 
                                                    <i class="icon-home"></i> {{group_list[groupid].grpName}}
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                
                                                <ul class="dropdown-menu pull-right" role="menu" >
                                                    
                                                    <li class="nav-item start" ng-class="(grp.grpId == groupid)?'active':''" ng-repeat="grp in group_list | filter:grpName"  ng-click="groupSelection(grp.grpName, grp.grpId)">
                                        <a href="javascript:void(0);" class="nav-link " >
                                          <span class="title" style="overflow: hidden;text-overflow: ellipsis;"  ng-bind="grp.grpName"></span>
                                        </a>
                                                 </li>
                                             </ul>
                                            </div> -->
                             <!--  <div class="btn-group" style="margin-top: 27%;left: -27px;">
                                <a class="btn btn-circle green-haze btn-outline btn-circle btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true" style="margin-bottom: 10px;border-radius: 15px;"> 
                                                                        
                                                    <i class="fa fa-car"></i> <span ng-bind="trackVehID"><?php echo Lang::get('content.vehicles'); ?></span>
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                
                                                <ul class="dropdown-menu pull-right" role="menu" style="height: 450px;overflow-y: scroll;">
                                                    
                                                    <li ng-repeat="loc in orgvehiclelist | orderBy:natural('shortName') | filter:searchbox" ng-class="{active:vehiname ==loc.vehicle_id}"><a ng-class="{active: $index == selected, red:loc.status=='OFF'}" ng-click="getvehicleRoute(loc.vehicle_id, $index)">&nbsp;&nbsp;&nbsp;{{loc.vehicle_id}}</a></li>
                                                    
                                                </ul>
                                            </div>         -->
                    </div>
  <div id="page-content-wrapper">
    <div class="container-fluid">
      <div class="row">
         <h3 style="
    /*text-align: center;*/
    margin-top: 3%;
    margin-left: 20%;
"><?php echo Lang::get('content.manage_routes'); ?></h3>
        <div class="col-md-6 boxing_size" style="margin-left: 77px;width: 43%;">
                <div class="box box-primary" style="margin-left: 10px">
                    <div class="box-header">
                        
                        <h5 style="color: red">{{error}}</h5>
                       
                    </div>
                     
                    <div class="portlet-body" >
                    <div><b style="float:left;padding-top:5px;"><?php echo Lang::get('content.enter_route_name')?></b></div>  
                  <div style="float:left;">      
                    <input type="text" class="form-control" ng-model="routeName" placeholder="<?php echo Lang::get('content.enter_route_name'); ?>" style="width:200px; height: 30px;margin-left: 12px;">
                  </div>   
                  <div style="float: right; padding-right:50px">
                    <button type="button" ng-click="routesSubmit()" style="height:30px;text-align: center;padding-top:5px; " ><?php echo Lang::get('content.save')?></button>
                  </div>
              
                </div>
                <br>
                    <table class="dynData">
                   <!--  <table class="dynData"> -->
                      <thead>
                        <tr style="padding: 5px;">
                          <th>&nbsp;<?php echo Lang::get('content.route_name')?></th>
                          <th><?php echo Lang::get('content.edit')?></th>
                          <th><?php echo Lang::get('content.delete')?></th>
                        </tr>
                      </thead>
                      <tr ng-repeat="route in routedValue track by $index" ng-show='allroute'>
                        <td ng-click="getMap(route)"><a  data-target="#myModal2" data-toggle="modal" style="cursor: pointer;">{{route}}</a></td>
                        <td id="editAction"><a class="btn btn-xs btn-default"><span class="glyphicon glyphicon-pencil"></span> </a></td>
                        <td ng-click="deleteRouteName(route)"><a class="btn btn-xs btn-default"><span class="glyphicon glyphicon-trash"></span></a></td>
                        <td colspan="3" style="color:#F15E50;text-align: center;padding-top: 50px;padding-bottom: 50px;" ng-if="(routedValue == '')"><?php echo Lang::get('content.no_data_found')?></td>
                      </tr>
                      <tr ng-if="(routedValue == 'nill')&&(allroute!=true)||(routedValue == '')" >
                       <td colspan="3" style="color:#F15E50;text-align: center;padding-top: 50px;padding-bottom: 50px;"><?php echo Lang::get('content.no_data_found')?></td></tr>
                      <tr ng-hide='allroute'>
                        <td ng-click="getMap(route)" ng-if="routedValue != 'nill'"><a  data-target="#myModal2" data-toggle="modal" style="cursor: pointer;">{{routedValue}}</a></td>
                        <td id="editAction" ng-if="routedValue != 'nill'"><a class="btn btn-xs btn-default"><span class="glyphicon glyphicon-pencil"></span> </a></td>
                        <td ng-click="deleteRouteName(route)" ng-if="routedValue != 'nill'"><a class="btn btn-xs btn-default"><span class="glyphicon glyphicon-trash"></span></a></td>
                      </tr>

                    </table>
                   </div>
                  </div>
                  <div class="col-md-6 boxing_size" style="bottom: -15px;" >
                   
         
            <h4 class="bold" align="center">{{windowRouteName}}</h4>
          
      
                  <div id="dvMap" style="width:100%;height:450px;"></div>
                  </div>
                </div>
</div>
        
        <!-- </div> -->
        <!-- /#page-content-wrapper -->
    <!-- </div> -->
    
     
    <div class="modal fade" id="myModal">
      <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title"><?php echo Lang::get('content.error')?></h4>
            </div>
          <div class="modal-body">
              <p>{{hisloc.error}}</p>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
            </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
        
      </div>
    </div>
  </div>    
   <!--  <script src="assets/js/static.js"></script>
    <script src="assets/js/jquery-1.11.0.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js"></script>
    <script src="assets/js/ui-bootstrap-0.6.0.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,places" type="text/javascript"></script>
    <script src="assets/js/markerwithlabel.js"></script>
    <script src="assets/js/infobubble.js" type="text/javascript"></script>
    <script src="assets/js/moment.js" type="text/javascript"></script>
    <script src="assets/js/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="assets/js/infobox.js"  type="text/javascript"></script>
    <script src="assets/js/vamoApp.js"></script>
    <script src="assets/js/unique.js"></script>
    <script src="assets/js/customplay.js"></script> -->

<script>

 var apikey_url = JSON.parse(localStorage.getItem('apiKey'));
 var url = "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places,geometry";

  if(apikey_url != null || apikey_url != undefined) {
     url = "https://maps.googleapis.com/maps/api/js?key="+apikey_url+"&libraries=places,geometry";
   //url = "https://maps.googleapis.com/maps/api/js?key=AIzaSyABtcdlhUVm5aKq7wAlMatI56DKanIKS6o&libraries=places,geometry"; 
  }        

   function loadJsFilesSequentially(scriptsCollection, startIndex, librariesLoadedCallback) {
     if (scriptsCollection[startIndex]) {
       var fileref = document.createElement('script');
       fileref.setAttribute("type","text/javascript");
       fileref.setAttribute("src", scriptsCollection[startIndex]);
       fileref.onload = function(){
         startIndex = startIndex + 1;
         loadJsFilesSequentially(scriptsCollection, startIndex, librariesLoadedCallback)
       };
 
       document.getElementsByTagName("head")[0].appendChild(fileref)
     }
     else {
       librariesLoadedCallback();
     }
   }
 
 //An array of scripts you want to load in order
   var scriptLibrary = [];
   scriptLibrary.push("assets/js/static.js");
 //scriptLibrary.push("assets/js/jquery-1.11.0.js");
   scriptLibrary.push("https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js");
   scriptLibrary.push("assets/js/bootstrap.min.js");
 //scriptLibrary.push("https://ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular.min.js");
   scriptLibrary.push("https://ajax.googleapis.com/ajax/libs/angularjs/1.3.9/angular.min.js");
 //scriptLibrary.push("https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js");
   scriptLibrary.push(url);
   //scriptLibrary.push("https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places,geometry");
/* scriptLibrary.push("assets/js/leaflet.js"); 
   scriptLibrary.push("assets/js/leaflet.label.js");*/  
   scriptLibrary.push("assets/js/ui-bootstrap-0.6.0.min.js"); 
   scriptLibrary.push("assets/js/leaflet.js?no_cache=" + new Date().getTime());
   scriptLibrary.push("assets/js/leaflet.label.js?no_cache=" + new Date().getTime());
   scriptLibrary.push("assets/js/leaflet.markercluster.js?no_cache=" + new Date().getTime());
 //scriptLibrary.push("assets/js/bootstrap.min_3.3.7.js");
 //scriptLibrary.push("http://code.highcharts.com/highcharts.js");
 //scriptLibrary.push("http://code.highcharts.com/highcharts-more.js");
 //scriptLibrary.push("http://code.highcharts.com/modules/solid-gauge.js");
   scriptLibrary.push("assets/js/angular-translate.js");
   scriptLibrary.push("assets/js/markerwithlabel.js");
   scriptLibrary.push("assets/js/highcharts.js");
   scriptLibrary.push("assets/js/moment.js");
   //scriptLibrary.push("assets/js/bootstrap-datetimepicker.js");
   scriptLibrary.push("../resources/views/reports/datepicker/bootstrap-datetimepicker.js");
   scriptLibrary.push("assets/js/infobubble.js");
   scriptLibrary.push("assets/js/infobox.js");
   scriptLibrary.push("assets/ui-drop/select.min.js");
   scriptLibrary.push("assets/js/vamoApp.js");
   scriptLibrary.push("assets/js/services.js");
   scriptLibrary.push("assets/js/newcustomplay.js?v=<?php echo Config::get('app.version');?>");

 //Pass the array of scripts you want loaded in order and a callback function to invoke when its done
   loadJsFilesSequentially(scriptLibrary, 0, function(){
       // application is "ready to be executed"
       // startProgram();
   });
    // $(document).ready(function(){
    //     $('#minmax').click(function(){
    //         $('#contentmin').animate({
    //             height: 'toggle'
    //         },500);
    //     });
    // });
    // $("#menu-toggle").click(function(e) {
    //   e.preventDefault();
    //   $("#wrapper").toggleClass("toggled");
    // });
    
    //     $(function () {
    //     $('#dateFrom, #dateTo').datetimepicker({
    //       format:'YYYY-MM-DD',
    //       useCurrent:true,
    //       pickTime: false
    //     });
    //     $('#timeFrom').datetimepicker({
    //       pickDate: false,
    //                 useCurrent:true,
    //     });
    //     $('#timeTo').datetimepicker({
    //       useCurrent:true,
    //       pickDate: false
    //     });
    //     });
</script>

</body>
</html>
