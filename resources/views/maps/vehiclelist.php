<li ng-repeat="location in locations04 | orderBy:natural('group')"><a href="javascript:void(0);" ng-click="groupSelection(location.group, location.rowId)" ><span title="{{trimColon(location.group)}}" >{{trimColon(location.group)}}</span></a>
  
  <ul class="nav"  style="max-height: 400px; overflow-y: auto;"> 
    <li ng-repeat="loc in location.vehicleLocations | orderBy:natural('shortName') | filter:searchVehi" ng-class="{active:vehiname ==loc.vehicleId}" on-finish-render="callMyCustomMethod()">
      <a href="javascript:void(0);" ng-class="{red:loc.status=='OFF'}" id="id_{{loc.rowId}}"  ng-click="genericFunction(loc.vehicleId, loc.rowId, 'manualClick',loc.licenceExpiry,loc.deviceModel,loc.noOfTank,loc.shortName)">
        <img ng-if='trvShow==true  && loc.vehicleType!= "AirCompressor" && loc.vehicleType !="AugerBoringMachine" && loc.vehicleType !="Dozer" && 
                loc.vehicleType !="Grader" && loc.vehicleType !="HorizontalDirectionalDrillingMachine" && loc.vehicleType !="HydraulicCrane" && 
                loc.vehicleType!="PipeBendingMachine" && loc.vehicleType!="Pipelayer"' ng-src="assets/imgs/trvSideMarker/{{(loc.vehicleType=='Pickup')?'Car':(loc.vehicleType=='PassengerVan')?'Van':loc.vehicleType=='Carrier/Cement' || loc.vehicleType=='Bulker'?'Cement-Mixer':loc.vehicleType}}_{{loc.color}}.png" fall-back-src="assets/imgs/Car.png" width="16" height="16"/>
        <img ng-if='trvShow!=true&&location.fcode!="ETHIOPIA"  && loc.vehicleType!= "AirCompressor" && loc.vehicleType !="AugerBoringMachine" && loc.vehicleType !="Dozer" && 
                loc.vehicleType !="Grader" && loc.vehicleType !="HorizontalDirectionalDrillingMachine" && loc.vehicleType !="HydraulicCrane" && 
                loc.vehicleType!="PipeBendingMachine" && loc.vehicleType!="Pipelayer"' ng-hide="vehiImage" ng-src="assets/imgs/sideMarker/{{(loc.vehicleType=='Pickup')?'Car':(loc.vehicleType=='PassengerVan')?'Van':loc.vehicleType=='Carrier/Cement' || loc.vehicleType=='Bulker'?'Cement-Mixer':loc.vehicleType=='Tractor'?'HeavyVehicle':loc.vehicleType}}_{{loc.color}}.png" fall-back-src="assets/imgs/Car.png" width="16" height="16"/>
        <img ng-if='trvShow!=true && loc.vehicleType!= "AirCompressor" && loc.vehicleType !="AugerBoringMachine" && loc.vehicleType !="Dozer" && 
                loc.vehicleType !="Grader" && loc.vehicleType !="HorizontalDirectionalDrillingMachine" && loc.vehicleType !="HydraulicCrane" && 
                loc.vehicleType!="PipeBendingMachine" && loc.vehicleType!="Pipelayer"' ng-show="vehiImage" ng-src="assets/imgs/asset-marker/as_{{loc.color}}.png" fall-back-src="assets/imgs/assetImage.png" width="16" height="16"/>

        <img ng-if='trvShow!=true&&location.fcode=="ETHIOPIA"  && loc.vehicleType!= "AirCompressor" && loc.vehicleType !="AugerBoringMachine" && loc.vehicleType !="Dozer" && 
                loc.vehicleType !="Grader" && loc.vehicleType !="HorizontalDirectionalDrillingMachine" && loc.vehicleType !="HydraulicCrane" && 
                loc.vehicleType!="PipeBendingMachine" && loc.vehicleType!="Pipelayer"' ng-hide="vehiImage" ng-src="assets/imgs/newFranSideMarker/{{(loc.vehicleType=='Pickup')?'Car':(loc.vehicleType=='PassengerVan')?'Van':loc.vehicleType=='Carrier/Cement' || loc.vehicleType=='Bulker'?'Cement-Mixer':loc.vehicleType}}_{{loc.color}}.png" fall-back-src="assets/imgs/Car.png" width="16" height="16"/>

        <img ng-if='loc.vehicleType== "AirCompressor" || loc.vehicleType=="AugerBoringMachine" || loc.vehicleType=="Dozer" || 
                loc.vehicleType=="Grader"|| loc.vehicleType=="HorizontalDirectionalDrillingMachine" || loc.vehicleType=="HydraulicCrane" || 
                loc.vehicleType=="PipeBendingMachine" || loc.vehicleType=="Pipelayer"'  
                ng-src="assets/imgs/sideMarker/HeavyVehicle_{{loc.color}}.png" fall-back-src="assets/imgs/Car.png" width="16" height="16"/>
                

        <span  tooltips tooltips-position="right"> {{loc.shortName}} </span> </a> 
        <!--  data-toggle="tooltip" data-original-title="<div align='left'><span>Speed</span> &nbsp;-&nbsp; {{loc.speed}}; <br> <span>Ignition</span> &nbsp;-&nbsp; {{loc.ignitionStatus}} <br> <span>Address</span> &nbsp;-&nbsp; {{loc.address}}  </div>" tooltip-loader -->
      </li>
    </ul>
  </li> 