<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title><?php echo Lang::get('content.gps'); ?></title>
<link rel="shortcut icon" href="assets/imgs/tab.ico">
<link href="https://fonts.googleapis.com/css?family=Lato|Raleway:500|Roboto|Source+Sans+Pro|Ubuntu" rel="stylesheet">
<link rel="stylesheet" href="assets/css/popup.bootstrap.min.css">
<link href="../resources/views/reports/AdminLTE/AdminLTE.css" rel="stylesheet">
<link href="assets/css/bootstrap.css" rel="stylesheet">
<link href="assets/css/jVanilla.css" rel="stylesheet">
<link href="assets/css/simple-sidebar.css" rel="stylesheet">
<!--<link rel="stylesheet" href="assets/css/bootstrap.min.css">-->
<link rel="stylesheet" href="assets/css/bootstrap-select.css">
<link href="assets/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />

<style type="text/css">
  
  body{
    font-family: 'Lato', sans-serif;
  /*font-weight: bold;*/ 

  /*font-family: 'Lato', sans-serif;
    font-family: 'Roboto', sans-serif;
    font-family: 'Open Sans', sans-serif;
    font-family: 'Raleway', sans-serif;
    font-family: 'Faustina', serif;
    font-family: 'PT Sans', sans-serif;
    font-family: 'Ubuntu', sans-serif;
    font-family: 'Droid Sans', sans-serif;
    font-family: 'Source Sans Pro', sans-serif;
   */
} 

.disabled {
  pointer-events: none;
}
</style>

</head>

<div id="status">&nbsp;</div>
<body ng-app = "mapApp">
    <div ng-controller = "mainCtrl" class="ng-cloak">
        <div id="wrapper-site">
            <div id="sidebar-wrapper-site" style="min-width:92px;">
                <ul class="sidebar-nav" >
                    <li class="sidebar-brand"><a href="javascript:void(0);"><img id="imagesrc" src=""/></i></a></li>
                    <li class="track"><a href="../public/track"><div></div><label><?php echo Lang::get('content.track'); ?></label></a></li>
                    <!-- <li class="history"><a href="../public/track?maps=replay"><div></div><label>History</label></a></li> -->
                    <li class="alert01"><a href="../public/reports"><div></div><label><?php echo Lang::get('content.reports'); ?></label></a></li>
                    <li class="stastics"><a href="../public/statistics?ind=1"><div></div><label><?php echo Lang::get('content.statistics'); ?></label></a></li>
                    <li class="admin"><a href="../public/settings" class="active"><div></div><label><?php echo Lang::get('content.scheduled'); ?></label></a></li>
                    <li class="fms"><a href="../public/fleetManagement?fms=FleetManagement" id="fms"><div></div><label><?php echo Lang::get('content.FMS'); ?></label></a></li>
                    <li class="ReleaseNotes"><a ng-href="pdfView"><div></div><label><?php echo Lang::get('content.release'); ?> <?php echo Lang::get('content.notes'); ?></label></a></li>
                    <li class="logout"><a href="../public/logout"><img src="assets/imgs/logout.png"/></a></li>         
                </ul>
                <!-- <ul class="sidebar-subnav" style="max-height: 100vh; overflow-y: auto;" ng-init="vehicleStatus='ALL'">
                    <li style="margin-bottom: 15px;"><div class="right-inner-addon" align="center"><i class="fa fa-search"></i><input type="search" class="form-control" placeholder="Search" ng-model="searchbox" name="search" /></div>
                    </li>
                    <li ng-repeat="location in locations02"><a href="javascript:void(0);" ng-click="groupSelection(location.group, location.rowId)" ng-cloak >{{trimColon(location.group)}}</a>
                        <ul class="nav nav-second-level" style="max-height: 400px; overflow-y: auto;">
                            <li ng-repeat="loc in location.vehicleLocations | filter:searchbox"><a href="" ng-class="{active: $index == selected, red:loc.status=='OFF'}" ng-click="genericFunction(loc.vehicleId, $index, loc.shortName)"><img ng-src="assets/imgs/{{loc.vehicleType}}.png" fall-back-src="assets/imgs/Car.png" width="16" height="16"/><span>{{loc.shortName}}</span></a></li>

                        </ul>
                    </li>
                </ul> -->
            </div>
            <div id="testLoad"></div>

<!--<div ng-show="reportBanShow" class="modal fade" id="allReport" role="dialog" style="top: 100px">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-body">
                  <p class="err" style="text-align: center;"> You are not a premium user !... </p>
              </div>
          </div>
      </div>
      </div> -->

        <div ng-show="reportBanShow" class="col-md-10" >
            <div class="box box-primary" style="height:90px; padding-top:30px; margin-top:15%; margin-left:8%;">
                <p ><h5 class="err" style="text-align: center;border:1px solid bold;"> <?php echo Lang::get('content.no_report_found'); ?> </h5></p>
            </div>
        </div>
      <!--  <tabset class="nav-tabs-custom">
         <tab heading="Daily"  active="actsTab">-->

      <div class="col-md-12" style="margin-top: 70px;margin-left: 8px;" >
      <div class="span6 pull-right" align="center" style="margin-right: 10px;margin-top: 5px;"> 
                     <!--  <select ng-model="multiLang" id="lang" class="form-control" >
                            <option value="en">English</option>
                            <option value="hi">Hindi</option>
                     </select> -->
                </div>
      <tabset class="nav-tabs-custom" ng-show="reportDaily">

        <tab  heading="<?php echo Lang::get('content.daily'); ?>"  active="reportDailyAct">
          <tabset class="nav-tabs-custom">

               <tab  heading="<?php echo Lang::get('content.vehicle'); ?>"  active="true"  ng-click="handleTapChange()" >
                 <!--<div class="box box-primary">
                    <!--<div class="box-header" data-toggle="tooltip" title="" data-original-title="Header tooltip" >
                        <h5 class="box-title">Scheduled Report</h5>
                      </div>-->

          <div class="box box-primary"  style="min-height:650px;position: relative; padding-right: 15px; padding-left: 15px;  background-color: #fdfdfd;  border-radius: 3px; border: 0.7px solid #e0dfdf; ">
                <h4>Daily Mail Scheduling
                <div class="pull-right helpVideo">
                    <span>
                      <img height="25" width="25"  alt="logo" title="logo" src="assets/imgs/youtube1.png">
                    </span>
                    <span>
                      <a ng-href="{{videoLink}}" style="padding-left: 3px;color: #196481;text-decoration: none;position: relative;top: 1.5px;font-weight: bold;font-size: medium;" target="_blank"><?php echo Lang::get('content.help'); ?></a>
                    </span>
                </div>
              </h4>

                <h6 style="margin-left: 15px; color: red">{{error }}</h6>
                <div class="col-md-1" ></div>
                
                <div class="col-md-3">
                  <!-- <select class="form-control" ng-options="trimColon(grpName.group) for grpName in locations02" ng-model="groupName" ng-change="groupChange()">
                     <option style="display:none" value="">Select Group</option>
                  </select> -->
                  <select ng-options="groups.group as trimColon(groups.group) for groups in locations02" ng-model="groupSelected" ng-change="groupChange(groupSelected)" class="form-control" id="colorChange">
                            <option style="display:none" value=""><?php echo Lang::get('content.select_group'); ?></option>
                        </select>
                </div>

                <div class="col-md-3" align="center">
                  <div class="form-group">
                    <input type="text" class="form-control" id="mailIdDaily" ng-model="mailId" placeholder="<?php echo Lang::get('content.email'); ?>" data-toggle="tooltip" data-placement="left"  title="please don't use space"><span style="font-size: 10px"><?php echo Lang::get('content.use_comma'); ?></span>
                  </div>
                </div>

                <div class="col-md-2" align="center">
                  <div class="form-group">
                    <button ng-click="storeValue()"><?php echo Lang::get('content.submit'); ?></button>
                  </div>
                </div>
                

               <table class="table table-bordered table-striped table-condensed table-hover table-fixed-header1" id="reportable">
                 <thead class='header' style=" z-index: 1;">
                    <tr style="text-align:center;font-weight: bold;">
                      <th class="id" style="text-align:center;background-color: #deebf3;" width="10%">
                        <input type="checkbox" ng-model="selectId" ng-click="vehiSelect(selectId)"/>
                      </th>
                      <th class="id" style="text-align:center;background-color: #deebf3;color:#545353;" width="20%">{{vehiLabel}} <?php echo Lang::get('content.name'); ?></th>
                      <th class="id" sort="sort" style="text-align:center;background-color: #deebf3;" width="15%"> 
                        <select class="selectpicker" data-width="100px"  ng-options="option+' hrs' for option in hoursFrom" id="fromTime"  ng-model="from">
                        <option style="display:none" value=""><?php echo Lang::get('content.from'); ?></option>
                            <!-- <option>From Hours</option> -->
                            <!-- <option ng-repeat="x in hours">{{x}}&nbsp;hour</option> -->
                          </select>
                      </th>
                      <th class="id" sort="sort" style="text-align: center;background-color: #deebf3;" width="15%">
                        <select class="selectpicker" data-width="100px"  ng-options="option+' hrs' for option in hoursTo" id="toTime" ng-model="to">
                        <option style="display:none" value=""><?php echo Lang::get('content.to'); ?></option>
                            <!-- <option>From Hours</option> -->
                            <!-- <option ng-repeat="x in hours">{{x}}&nbsp;hour</option> -->
                          </select>
                      </th>
                      <th class="id" style="text-align: center;background-color: #deebf3;" width="40%"> 
                      <select name="selectdata" ng-model="reportSelected" ng-change="changeValue(reportSelected)" class="selectpicker" title="Select Report" ng-options="report for report in reports" multiple  data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true"></select></th>
                     <th style="text-align:center;font-weight: bold; cursor: pointer;background-color: #deebf3;" title="Delete All {{vehiLabel}}">
                        <span class="glyphicon glyphicon-trash" ng-click="deleteFn()"></span>
                     </th>
                    </tr>
                  </thead>
                <tbody>
                  <tr ng-repeat="vehi in vehicles">
                    <td><input type="checkbox" ng-model="vehiId[$index]"/></td>
                    <td>{{vehi.shortName}}</td>
                    <td>{{from}}&nbsp;hrs</td>
                    <td>{{to}}&nbsp;hrs</td>

                    
                    <td id="checking" ng-if="vehiView">
                    <input type="checkbox" ng-model="checkingValue.move[$index]">M</input>&nbsp;
                    <input type="checkbox" ng-model="checkingValue.over[$index]">O</input>&nbsp;
                    <input type="checkbox" ng-model="checkingValue.site[$index]">S</input>&nbsp;                    
                    <input type="checkbox" ng-model="checkingValue.geo[$index]">G</input>&nbsp;
                    <!-- <input type="checkbox" ng-model="checkingValue.poi[$index]">PI</input>&nbsp; -->
                    <!-- <input type="checkbox" ng-model="checkingValue.fuel[$index]">F</input>&nbsp; -->
                    <!-- <input type="checkbox" ng-model="checkingValue.fuelRaw[$index]">FR</input>&nbsp; -->
               <!-- <input type="checkbox" ng-model="checkingValue.temp[$index]">T</input> -->
                    </td>
                    
                    <td id="checking" ng-if="assetView">
                     <input type="checkbox" ng-model="checkingValue.move[$index]">M</input>&nbsp;
                <!-- <input type="checkbox" ng-model="checkingValue.temp[$index]">T</input> -->
                    </td>

                    <td style="cursor: pointer;" title="Delete Individual {{vehiLabel}}">
                        <span class="glyphicon glyphicon-trash" ng-click="deleteFn(vehi, $index)">
                    </td>
                  </tr>
                  <tr>
                    <td colspan="6" ng-if="starterError" style="color: red;font-size: 14px;">{{starterError}}</td>
                  </tr>
                </tbody>
                
              </table>

          </div>
     <!--    </div> -->

      </tab>

     <tab heading="<?php echo Lang::get('content.group'); ?>" active="false" ng-click="fetchController2()" disabled="isLoading">

         <div class="box box-primary"  style="min-height:650px;position: relative; padding-right: 15px; padding-left: 15px;  background-color: #fdfdfd;  border-radius: 3px; border: 0.7px solid #e0dfdf; ">
                <h4><?php echo Lang::get('content.group_mail'); ?></h4>

                <h6 style="margin-left: 15px; color: red">{{error}}</h6>
                <div class="col-md-1" ></div>
                
                <div class="col-md-3">
                  <!-- <select class="form-control" ng-options="trimColon(grpName.group) for grpName in locations02" ng-model="groupName" ng-change="groupChange()">
                     <option style="display:none" value="">Select Group</option>
                  </select> -->
              <!--    <select ng-options="groups.group as trimColon(groups.group) for groups in locations02" ng-model="groupSelected" ng-change="groupChange(groupSelected)" class="form-control" id="colorChange">
                            <option style="display:none" value="">Select Group</option>
                        </select>-->
                </div>

                <div class="col-md-3" align="center">
                  <div class="form-group">
                    <input type="text" class="form-control" id="mailIdDaily2" ng-model="mailId2" placeholder="E-Mail" data-toggle="tooltip" data-placement="left"  title="please don't use space"><span style="font-size: 10px"><?php echo Lang::get('content.use_comma'); ?></span>
                  </div>
                </div>

                <div class="col-md-2" align="center">
                  <div class="form-group">
                    <button ng-click="storeValue2()"><?php echo Lang::get('content.submit'); ?></button>
                  </div>
                </div>

               <table class="table table-bordered table-striped table-condensed table-hover" id="reportable">
                  <thead>
                    <tr style="text-align:center;font-weight: bold;">
                      <th class="id" style="text-align:center;background-color: #deebf3;" width="10%">
                        <input type="checkbox" ng-model="selectGrpId" ng-click="grpSelect(selectGrpId)"/>
                      </th>
                      <th class="id" style="text-align:center;background-color: #deebf3;color:#545353;" width="20%"><?php echo Lang::get('content.group'); ?> <?php echo Lang::get('content.name'); ?></th>
                      <th class="id" sort="sort" style="text-align:center;background-color: #deebf3;" width="15%"> 
                        <select class="selectpicker" data-width="100px"  ng-options="option+' hrs' for option in hoursFrom" id="fromTime2"  ng-model="from2">
                        <option style="display:none" value=""><?php echo Lang::get('content.from'); ?></option>
                            <!-- <option>From Hours</option> -->
                            <!-- <option ng-repeat="x in hours">{{x}}&nbsp;hour</option> -->
                          </select>
                      </th>
                      <th class="id" sort="sort" style="text-align: center;background-color: #deebf3;" width="15%">
                        <select class="selectpicker" data-width="100px"  ng-options="option+' hrs' for option in hoursTo" id="toTime2" ng-model="to2">
                        <option style="display:none" value=""><?php echo Lang::get('content.to'); ?></option>
                            <!-- <option>From Hours</option> -->
                            <!-- <option ng-repeat="x in hours">{{x}}&nbsp;hour</option> -->
                          </select>
                      </th>
                      <th class="id" style="text-align: center;background-color: #deebf3;" width="40%"> 

                      <select name="selectdata" ng-model="reportSelected2" ng-change="changeValue2(reportSelected2)" class="selectpicker" title="<?php echo Lang::get('content.select_report'); ?>" ng-options="report for report in grpReport" multiple  data-live-search="true" data-live-search-placeholder="<?php echo Lang::get('content.search'); ?>" data-actions-box="true"></select></th>
                     
                     <th style="text-align:center;font-weight: bold; cursor: pointer;background-color: #deebf3;" title="Delete All {{vehiLabel}}">
                        <span class="glyphicon glyphicon-trash" ng-click="deleteFn2()"></span>
                     </th>
                    </tr>
                  </thead>
                <tbody>
                  <tr ng-repeat="grp in groupList">
                    <td><input type="checkbox" ng-model="grpId[$index]"/></td>
                    <td>{{grp}}</td>
                    <td>{{from2}}&nbsp;hrs</td>
                    <td>{{to2}}&nbsp;hrs</td>
                    
                    <td id="checking" ng-if="vehiView">
                      <input type="checkbox" ng-model="checkingValue2.consr[$index]">&nbsp;CR</input>&nbsp;
                      <input type="checkbox" ng-model="checkingValue2.fuelCon[$index]">&nbsp;FC</input>&nbsp;
                      <input type="checkbox" ng-model="checkingValue2.travSum[$index]">&nbsp;TS</input>&nbsp;
                      <input type="checkbox" ng-model="checkingValue2.nodata[$index]">&nbsp;ND</input>&nbsp;
                      <input type="checkbox" ng-model="checkingValue2.exceFuel[$index]">&nbsp;EF</input>&nbsp;
                      <input type="checkbox" ng-model="checkingValue2.lastTran[$index]">&nbsp;LTR</input>&nbsp;
                      <input type="checkbox" ng-model="checkingValue2.vehWisePer[$index]">&nbsp;VP</input>&nbsp;
                      <input type="checkbox" ng-model="checkingValue2.monthlyDistance[$index]">&nbsp;MD</input>&nbsp;
                    </td>

                    <td style="cursor: pointer;" title="Delete Individual {{vehiLabel}}">
                        <span class="glyphicon glyphicon-trash" ng-click="deleteFn2(grp, $index)">
                    </td>
                  </tr>
                </tbody>
                
              </table>

          </div>

     </tab>
     

    </tabset>
</tab>
       <tab ng-show="false" heading="<?php echo Lang::get('content.monthly'); ?>"  active="reportMonthlyAct" ng-click="fetchMonController()">

              <div class="box box-primary" ng-show="reportMonthly" style="min-height:600px;position: relative; padding-right: 15px; padding-left: 15px;  background-color: #fdfdfd;  border-radius: 3px; border: 0.7px solid #e0dfdf; ">

                  <h4><?php echo Lang::get('content.monthly_mail'); ?></h4>

                <h6 style="margin-left: 15px; color: red">{{error}}</h6>
                <div class="col-md-1" ></div>
                
                <div class="col-md-2">
                  <!-- <select class="form-control" ng-options="trimColon(grpName.group) for grpName in locations02" ng-model="groupName" ng-change="groupChange()">
                     <option style="display:none" value="">Select Group</option>
                  </select> -->
                 <!--   <select ng-options="groups.group as trimColon(groups.group) for groups in locations02" ng-model="groupSelected" ng-change="groupChange()" class="form-control" id="colorChange">
                            <option style="display:none" value="">Select Group</option>
                        </select>  -->
                </div>

                <div class="col-md-3" align="center">
                  <div class="form-group">
                    <input type="text" class="form-control" id="groupMailId" ng-model="grpMailId" placeholder="E-Mail" data-toggle="tooltip" data-placement="left"  title="please don't use space"><span style="font-size: 10px"><?php echo Lang::get('content.type_comma'); ?></span>
                  </div>
                </div>

                <div class="col-md-2" align="center">
                  <div class="form-group">
                    <button ng-click="storeMonValues()"><?php echo Lang::get('content.submit'); ?></button>
                  </div>
                </div>

               <table class="table table-bordered table-striped table-condensed table-hover" id="reportable">
                  <thead>
                    <tr >
                      <th class="id" style="text-align:center;background-color: #deebf3;font-weight: bold;margin-top:0px;" width="10%">
                        <input type="checkbox" ng-model="selectGrpId" ng-click="grpSelect(selectGrpId)"/>
                      </th>

                      <th class="id" style="text-align:center;background-color: #deebf3;color:#545353;font-weight: bold;" width="40%"><?php echo Lang::get('content.group'); ?> <?php echo Lang::get('content.name'); ?></th>
           <!--       <th class="id" sort="sort" style="text-align:center;" width="15%"> 
                        <select class="selectpicker" data-width="100px"  ng-options="option+' hrs' for option in hoursFrom" ng-model="from">
                        <option style="display:none" value="">From</option>
                            <!-- <option>From Hours</option> -->
                            <!-- <option ng-repeat="x in hours">{{x}}&nbsp;hour</option> -->
           <!--         </select>
                      </th>
                      <th class="id" sort="sort" style="text-align: center;" width="15%">
                        <select class="selectpicker" data-width="100px"  ng-options="option+' hrs' for option in hoursTo" ng-model="to">
                        <option style="display:none" value="">To</option>
                            <!-- <option>From Hours</option> -->
                            <!-- <option ng-repeat="x in hours">{{x}}&nbsp;hour</option> -->
           <!--         </select>
                      </th>  -->

                      <th class="id" style="text-align: center;background-color: #deebf3;font-weight: bold;" width="30%"> 

                      <select name="selectdata" ng-model="monReportSel" ng-change="changeMonValue(monReportSel)" class="selectpicker" title="Select Report" ng-options="report for report in monReport" multiple  data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true"></select>

                      </th>


                     <th width="10%" style="text-align:center;font-weight: bold; cursor: pointer;background-color: #deebf3;" title="Delete All {{vehiLabel}}">
                        <span class="glyphicon glyphicon-trash" ng-click="deleteMonFunc()"></span>
                     </th>

                    </tr>
                  </thead>


                 <tbody>
                 <tr> <td style="border-color:none;"></td></tr>
                
                  <tr ng-repeat="groups in groupList">
                    <td><input type="checkbox" ng-model="grpId[$index]"/></td>
                    <td>{{groups}}</td>
                    
                    <!--<td>{{from}}&nbsp;hrs</td>
                        <td>{{to}}&nbsp;hrs</td> -->
                    
                    <td id="checking">
                      <input type="checkbox" ng-model="checkingValue.cstop[$index]">&nbsp;CS</input>&nbsp;
                    </td>

                    <td style="cursor: pointer;" title="Delete Individual {{vehiLabel}}">
                        <span class="glyphicon glyphicon-trash" ng-click="deleteMonFunc($index)">
                    </td>

                  </tr>
                </tbody>
                
              </table>

              </div>
       </tab>

    </tabset> 

     </div>
    </div>

</div>
         
  <script src="assets/js/static.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.3.8/angular.min.js"></script> 

  <script src="../resources/views/reports/customjs/ui-bootstrap-tpls-0.12.0.min.js"></script>
  <script src="assets/js/angular-translate.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <script src="../resources/views/reports/customjs/moment.js"></script>
<script src="../resources/views/reports/datepicker/bootstrap-datetimepicker.js"></script>
  <!--<script src="assets/js/ui-bootstrap-0.6.0.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>-->

  <script src="assets/js/bootstrap-select.js"></script>
  <script src="assets/js/vamoApp.js"></script>
  <script src="assets/js/services.js"></script>
  <script src="../resources/views/reports/customjs/settings.js?v=<?php echo Config::get('app.version');?>"></script>

  <script type="text/javascript">
        
    $(document).ready(function() {  
      $(".selectpicker").selectpicker();
    });

  </script>

</body>
</html>
