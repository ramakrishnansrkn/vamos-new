<!DOCTYPE html>
<html lang="en">
<head>
	<title>GPSVTS</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	<!-- <meta name="csrf-token" content="{{ csrf_token() }}" /> -->
	<link rel="icon" type="image/png" href=""/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/gpsmain.css">
  
<!--===============================================================================================-->
</head>
<style>
	body{
		background:#ebebeb;
	}
</style>

<body>
	<div class="row" style="margin:0px;">	
    <div class="col-sm-12" id="redirectionMsg">
      <div class="limiter">
        <div class="container-login100">
    
             <div id="preloader">
                <img src="{{ asset('/assets/imgs/Spin-loader.svg') }}">
              </div>
             <span id="error" style="color:blue;font-weight:bold;font-size:16px;margin-left: -20px;">
                You are being redirecting to secured website. Please wait...
             </span> 
           
        </div>
      </div>
    </div>
		<div class="col-sm-6" id="loginForm">
			<div class="limiter">
				<div class="container-login100">

					<div class="wrap-login100 p-l-85 p-r-85 p-t-60 p-b-60">
						<div class="text-center">
							<img class="img-responsive" alt="logo" title="logo" src="{{ asset('/assets/imgs/GPSVTS.png') }}">
						</div>
						<h5>
                
				          <?php if(Session::has('flash_notice')): ?>
				          	<span id="error" style="color:#ff6666;font-weight:bold;font-size:15px;"><?php echo Session::get('flash_notice') ?></span> 
				          <?php endif; ?>
				          
				            <span id="error" style="color:#ff6666;font-weight:bold;font-size:15px;">{{ HTML::ul($errors->all()) }}</span> 
				        </h5>
                        {{ Form::open(array('url' => "login$mobileNo",'class' => 'login100-form validate-form flex-sb flex-w')) }}
						<!-- <form class="login100-form validate-form flex-sb flex-w"> -->
							
							<span class="txt1 p-b-11">
								<b>Username</b>
							</span>
							<div class="wrap-input100 validate-input m-b-36" data-validate = "Username is required">
								<input class="input100" type="text" name="userName" placeholder="Enter your username" id="userIds" value="{{ old('userName') }}">
								<span class="focus-input100"></span>
							</div>
							
							<span class="txt1 p-b-11">
                <b>@if ($mobileNo == "") Password @else  OTP  @endif</b>
							</span>
							<div class="wrap-input100 validate-input m-b-12" data-validate = "{{($mobileNo == ""?"Password":"OTP")." is required"}}">
								<span class="btn-show-pass">
									<i class="fa fa-eye"></i>
								</span>
								<input class="input100" type="password" name="password" placeholder="Enter your {{($mobileNo == ""?"password":"OTP")}}">


								<span class="focus-input100"></span>
							</div>
							
							<div class="flex-sb-m w-full p-b-48">
								<div class="contact100-form-checkbox">
									<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
									<label class="label-checkbox100" for="ckb1">
										Remember me
									</label>
								</div>

								<div>
									<a href="password/resetingWin" class="txt3">
										Forgot Password?
									</a>
									@if($url=="yes")<a href="/gps/public/apiAcess" class="txt3" target="blank" > | API Access |</a>&nbsp;<a href="/gps/public/faq" class="txt3" target="blank">FAQ</a>@endif
								</div>
							</div>

							<div class="container-login100-form-btn"  id="clickme">
								<button class="login100-form-btn btn-block" id="loginBtn">
									Login
								</button>
							</div>
                        {{ Form::close() }}
						<!-- </form> -->
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6" id="fleetosAd">
			<div class="limiter">
				<div class="container-login100">
					<div class="p-l-0 p-r-85 p-t-35 p-b-55">
            <div>
						<div class="">
							<img class="img-responsive" alt="logo" title="logo" src="{{ asset('/assets/imgs/fleetOS.png') }}" width="200">
						</div>
						<div class="p-t-15">
							<h1 style="margin: 0px;font-family: 'Roboto', Sans-serif;;font-weight: 700;line-height: 1em;font-size: 1.8em;"> FleetOS - Fleet Mangement Solutions </h1>
						</div>
						<div class="p-t-15 p-l-20">
							<ul>
							<li style="margin: 0px;font-family:'Roboto', Sans-serif;;font-weight: 400;line-height: 1.8em;font-size:15px;">Meticulous trip planning & executions</li>
							<li style="margin: 0px;font-family:'Roboto', Sans-serif;;font-weight: 400;line-height: 1.8em;font-size:15px;">Reduce communication gaps with drivers</li>
							<li style="margin: 0px;font-family:'Roboto', Sans-serif;;font-weight: 400;line-height: 1.8em;font-size:15px;">Flawless Truck, Trip and Driver expenses management</li>
							<li style="margin: 0px;font-family:'Roboto', Sans-serif;;font-weight: 400;line-height: 1.8em;font-size:15px;">Focus more on business growth</li>
							</ul>
						</div>
						<div class="p-t-15 p-l-15 p-b-20">
							<a href="" id="fleetos" style="margin: 0px;font-family:'Roboto', Sans-serif;;font-weight: 400;line-height: 1.8em;font-size:15px;color:#222bb8;">Get FleetOS Freemium Now!</a>
						</div>
          </div>
          <div class="video-wrap">
            <object>
            <param name="movie" value="https://www.youtube.com/embed/yjrYLlJZKpU?html5=1&amp;rel=0&amp;hl=en_US&amp;version=3"/>
            <param name="allowFullScreen" value="true"/>
            <param name="allowscriptaccess" value="always"/>
            <embed width="500" height="315" src="https://www.youtube.com/embed/yjrYLlJZKpU?html5=1&amp;rel=0&amp;hl=en_US&amp;version=3" class="youtube-player" type="text/html" allowscriptaccess="always" allowfullscreen="true"/>
            </object>
          </div>
					</div>
				</div>
			</div>
		</div>

	</div>


    <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
	<script src="js/main.js"></script>

</body>

<style type="text/css">
li {
    display: list-item;
    text-align: -webkit-match-parent;
}
.video-wrap {
     position:relative;
     padding-bottom: 55%;
     padding-top: 30px;
     height: 0;
     overflow:hidden;
}

.video-wrap object,
.video-wrap embed {
     position:absolute;
     top:0;
     width:100%;
     height:100%;
}
</style>
<script>
          localStorage.clear();
          var logo = document.location.host;
          var substring = "www.";
         $("#fleetos").attr("href", 'https://vamosys.com/fleetos-fms-fleet-management-systems/?utm_source=referral&utm_medium=website&utm_campaign=gpsvts-login-page-fleetos' );
         if(logo.indexOf(substring) !== -1){
           var logocsk =logo.replace(substring,'');
           logo=logocsk;
         }
         console.log(logo);
         // var select = document.getElementById('lang');
         //  select.onchange = function(){
         //    $('#langform').trigger('submit');
         //      //this.form.submit();
         //  };
          function ValidateIPaddress(ipaddress) {  
           var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;  
            if(ipaddress.match(ipformat)) {
              return (true)  
            }  
          // alert("You have entered an invalid IP address!")  
          return (false)  
          }  

          if(ValidateIPaddress(logo)) {
             var parser    =   document.createElement('a');
             parser.href   =   document.location.ancestorOrigins[0];
             logo          =   parser.host;
          }

          // (function test(){
          //    console.log("arun")
          // }());
          
          var path       =  document.location.pathname;
          //alert(document.location.hostname);
          var splitpath  =  path.split("/");
          //console.log(' path '+"----"+splitpath[1]);

          //var imgName = '/'+splitpath[1]+'/public/uploads/'+logo+'.png'; 
          //var imgName= '/'+splitpath[1]+'/public/uploads/gpsvts.com.png';
		  var imgName = 'http://206.189.94.37/cpanel/public/uploads/'+logo+'.png';
          var wwwSplit = logo.split(".")
              if(wwwSplit[0]=="www"){
                wwwSplit.shift();
                imgName = '/'+splitpath[1]+'/public/uploads/'+wwwSplit[0]+'.'+wwwSplit[1]+'.png';
              }
           if(document.location.hostname=="cpanel.gpsvts.net"){
                  $('#avatarsrc').attr('src', "");
                  $('#imagesrc').attr('src', "");
                  $("#icons").hide();
                  var loginStyle = document.getElementById('loginStyle');
                  loginStyle.style.height = 90 + "%";
                  loginStyle.style.top = 10 + "%";
                  var loginFormStyle = document.getElementById('loginFormStyle');
                  loginFormStyle.style.top = 30 + "%";
          }
          // $('#imagesrc').attr('src', imgName);
          
        </script>
    
<script type="text/javascript">
   
 var globalIP = document.location.host;
 var contextMenu = '/'+document.location.pathname.split('/')[1];

  $.ajaxSetup({

        headers: {

            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

    });
  document.getElementById("redirectionMsg").style.display = "none";
  if(document.location.hostname=="gpsvts.net"){
    // var isAuth    =  "<?php echo Auth::check() ?>";
    // if(!isAuth){
      document.getElementById("redirectionMsg").style.display = "block";
      document.getElementById("loginForm").style.display = "none";
      document.getElementById("fleetosAd").style.display = "none";
      setTimeout(function(){ window.location.href = 'https://gpsvts.vamosys.com/gps/public/login';  }, 1000);
    //}
  }


  $('#clickme').click(function() {
      
      var userId  = $('#userIds').val().toUpperCase();
      var userIP=localStorage.getItem('myIP');
      var postVal = {
        'id':userId,
        'userIP':userIP,
      '_token': $("[name='_token']").val(),
      };

    //$.get('http://128.199.159.130:9000/isAssetUser?userId=MSS', function(response) {
      // $.get('//'+globalIP+contextMenu+'/public/isAssetUser', function(response) {
      //     //alert(response);
      //     localStorage.setItem('isAssetUser', response);
      // }).error(function(){
      //     console.log('error in isAssetUser');
      // });
      
     
      //alert(localStorage.getItem('myIP'));
       

       $.post('{{ route("ajax.fcKeyAcess") }}',postVal)
         .done(function(data) {

          localStorage.setItem('fCode',data);
        //alert(data);
        
        }).fail(function() {
            console.log("fcode fail..");
      });

      localStorage.setItem('userIdName', JSON.stringify('username'+","+userId));
      //var language=$('#lang').val();
      localStorage.setItem('lang','en');
      var usersID = JSON.stringify(userId);

      if(usersID == '\"BSMOTORS\"' || usersID == '\"TVS\"') {

        window.localStorage.setItem('refreshTime',120000);

      } else {

        window.localStorage.setItem('refreshTime',60000);
      }

  });


 $('#userIds').on('change', function() {
     
    var postValue = {
      'id': $(this).val().toUpperCase(),
      '_token': $("[name='_token']").val(),
      };
    // alert($('#groupName').val());
    $.post('{{ route("ajax.apiKeyAcess") }}',postValue)
      .done(function(data) {
        
        // $('#validation').text(data);
            localStorage.setItem('apiKey', JSON.stringify(data));
            
          }).fail(function() {
            console.log("fail");
      });

    $.post('{{ route("ajax.dealerAcess") }}',postValue)
      .done(function(data) {
        
        //alert(data);
          localStorage.setItem('dealerName', data);
            
      }).fail(function() {
          console.log("fail");
    });    

    
  });

// Get the modal
var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


</script>

</html>