app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global', '$translate' ,function($scope, $http, vamoservice, $filter, GLOBAL,$translate){
  //global declaration
  var language=localStorage.getItem('lang');
  $translate.use(language);
  var translate = $filter('translate');
  $scope.geoShiftError =  "";
  $scope.dealerName     =  localStorage.getItem('dealerName');
  var curl_error ='An internal error occurred during your request!..Pls try again.';
  function getParameterByName(name) {
      name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
          results = regex.exec(location.search);
      return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  //global declartion
  $scope.locations = [];
  $scope.time = $filter('date')(new Date(), 'HH:mm');
  var getOrgUrl       = GLOBAL.DOMAIN_NAME+'/getOrgBasedOnUser';
  $scope.gIndex    = 0;
  $scope.isGpsDayReport = false;
  $scope.tn       =  getParameterByName('tn'); 
  if($scope.tn=='gpsperday'){
    $scope.isGpsDayReport = true;
  }
  //$scope.locations01 = vamoservice.getDataCall($scope.url);
    $scope.trimColon = function(textVal) {

      if(textVal){
       var spltVal = textVal.split(":");
       return spltVal[0];
      }
    }
  $scope.sort = sortByDate('vehicleName');
  function sessionValue(vid, gname,licenceExpiry){
    localStorage.setItem('user', JSON.stringify(vid+','+gname));
    localStorage.setItem('licenceExpiry', licenceExpiry);
    $("#testLoad").load("../public/menu");
  }
  

  function convert_to_24h(time_str) {
  //console.log(time_str);
      var str      = time_str.split(' ');
      var stradd   = str[0].concat(":00");
      var strAMPM  = stradd.concat(' '+str[1]);
      var time     = strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
      var hours    = Number(time[1]);
      var minutes  = Number(time[2]);
      var seconds  = Number(time[2]);
      var meridian = time[4].toLowerCase();
  
      if (meridian == 'p' && hours < 12) {
        hours = hours + 12;
      }
      else if (meridian == 'a' && hours == 12) {
        hours = hours - 12;
      }     
      var marktimestr =''+hours+':'+minutes+':'+seconds;      
      return marktimestr;
    };

    
  function formatAMPM(date) {
      var date = new Date(date);
      var hours = date.getHours();
      var minutes = date.getMinutes();
      var ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
      return strTime;
  }

  $scope.init = function () {
    stopLoading();
      $http({
            method : "GET",
            url : getOrgUrl
        }).then(function mySuccess(response) {
            stopLoading();
            if(response!='Failed'){
              let orgUrlRes      =  response.data;
              if(orgUrlRes.response=='Success'){
                $scope.orgList = orgUrlRes.data;
                setTimeout(function () {
                    $('.selectpicker').selectpicker('refresh');
                },500)
                if($scope.orgList!=null){
                  $scope.selectedOrg = $scope.orgList[0];
                  //$scope.selectedOrg = $scope.orgList[$scope.orgList.length-1];
                   getOrgShiftsBasedOnOrg();
                }
                //console.log($scope.orgList);
                
              }else{
                console.log('failed');
                $scope.geoShiftError =  orgUrlRes.error; 
              }
            }else{
              $scope.geoShiftError =  curl_error; 
            }
           
      }, function myError(response) {
           console.log(response.statusText);
      });
  }
$scope.init();
$scope.orgChanged = function () {
  if(!$scope.geoShiftError){
    undo();
  }
  $scope.geoShiftError = "";
  getOrgShiftsBasedOnOrg();
}
function getOrgShiftsBasedOnOrg(){
    $scope.geoShiftError = "";
    $scope.OrgShift = [];
    $scope.fromdate =($('#dateFrom').val());
    var dt = new Date();
    $scope.todaydate=getTodayDate(dt.setDate(dt.getDate()));
    let shiftUrl = GLOBAL.DOMAIN_NAME+'/getOrgShiftsBasedOnOrg?orgId='+$scope.selectedOrg;
    $http({
            method : "GET",
            url : shiftUrl
        }).then(function mySuccess(response) {
            stopLoading();
            if(response!='Failed'){
              let shiftUrlRes      =  response.data;
              if(shiftUrlRes.response=='Success'){
                $scope.OrgShift = shiftUrlRes.data;
                //console.log($scope.OrgShift);
                // call only for gps per day report 
                if($scope.isGpsDayReport){
                   $scope.getOrgShiftTimeReportExcel('','','','yes');
                } 
              }else{
                console.log('failed');
                 $scope.geoShiftError =  'There is no geofence enteries for selected Organization.'; 
              }
            }else{
              $scope.geoShiftError =  curl_error; 
            }
            
      }, function myError(response) {
           console.log(response.statusText);
      });
}
$scope.getOrgShiftTimeReportExcel =function(start,end,shift,isconsolidated){
  //function getOrgShiftTimeReportExcel(shift){
  $scope.shiftDatetails = [];
  console.log(shift);
  $scope.geoShiftError = "";
  $scope.shiftStartTime = start;
  $scope.shiftEndTime = end;
  let fromDateutc = utcFormat($('#dateFrom').val(),"00:00");
  let toDateutc = utcFormat($('#dateTo').val(),"23:59");
  console.log(fromDateutc+'   ' +toDateutc)
  var orgShiftTimeUrl = "";
  if(isconsolidated=="yes"){
     toDateutc = utcFormat($('#dateFrom').val(),"23:59");
     orgShiftTimeUrl = GLOBAL.DOMAIN_NAME+'/getConsolidatedOrgShiftReportForUser?orgId='+$scope.selectedOrg+'&fromTimeUtc='+fromDateutc+'&toTimeUtc='+toDateutc;
  }else{
     orgShiftTimeUrl = GLOBAL.DOMAIN_NAME+'/getOrgShiftReportForUser?orgId='+$scope.selectedOrg+'&shiftName='+shift+'&fromTimeUtc='+fromDateutc+'&toTimeUtc='+toDateutc;
  }
  
  //var orgShiftTimeUrl='http://209.97.163.4:9000/getOrgShiftReportForUser?userId=MSS&orgId=MSS&shiftName=shift1&fromTimeUtc=1626110154000&toTimeUtc=1628110154000';
  startLoading();
  $http.get(orgShiftTimeUrl).success(function(data){

        //$scope.geofenceShiftData=response.data;
        let shiftDetailUrlRes = data;
        let isFirstEntry = true;
        if(shiftDetailUrlRes.response=='Success'){
          if($scope.isGpsDayReport){ // handled for gps per day report
            if(shiftDetailUrlRes.data!=null){
                angular.forEach(shiftDetailUrlRes.data, function(shift, key){
                  angular.forEach(shift.siteDetails, function(siteDetails,sIndex){
                    if(siteDetails.siteInside.length==0){
                      if(!isFirstEntry){
                        angular.forEach($scope.shiftDatetails, function(veh){
                          veh.shiftTimeArr.push('-');
                        });
                      }
                    }else{
                       angular.forEach(siteDetails.siteInside, function(vehicle, index){
                        if(isFirstEntry){
                          vehicle.shiftTimeArr= [];
                          if(shift.shiftEntryExit=="yes"){
                            vehicle.shiftTimeArr.push(vehicle.startTime);
                          }else{
                            vehicle.shiftTimeArr.push(vehicle.endTime);
                          }
                          $scope.shiftDatetails.push(vehicle);
                          if((index+1)==siteDetails.siteInside.length){
                            isFirstEntry = false;
                          }
                        }else{
                          let vehicleExistData = $scope.shiftDatetails.find(item => item.vehicleId==vehicle.vehicleId);
                          //console.log(vehicleExistData)
                          if(vehicleExistData!=undefined){
                            if(shift.shiftEntryExit=="yes"){
                              vehicleExistData.shiftTimeArr.push(vehicle.startTime);
                            }else{
                              vehicleExistData.shiftTimeArr.push(vehicle.endTime);
                            }
                            //vehicleExistData.shiftTimeArr.push(vehicle.startTime);
                          }else{
                            vehicle.shiftTimeArr= [];
                            let totalLength = key;
                            for (var i = 0; i < totalLength; i++) {
                              vehicle.shiftTimeArr.push('-');
                            }
                            if(shift.shiftEntryExit=="yes"){
                              vehicle.shiftTimeArr.push(vehicle.startTime);
                            }else{
                              vehicle.shiftTimeArr.push(vehicle.endTime);
                            }
                            //vehicle.shiftTimeArr.push(vehicle.startTime);
                            $scope.shiftDatetails.push(vehicle);
                          }
                          if((index+1)==siteDetails.siteInside.length){
                            angular.forEach($scope.shiftDatetails, function(veh){
                              let vehicleExistData = siteDetails.siteInside.find(item => item.vehicleId==veh.vehicleId);
                              if(!vehicleExistData){
                                veh.shiftTimeArr.push('-');
                              }
                            });
                          }
                        }
                      });
                    }
                    
                 });
               });
            }
            
            setTimeout(function(){ stopLoading(); enhance();  }, 100);
            stopLoading();
            //console.log($scope.shiftDatetails)
          }else{
               $scope.shiftDatetails=shiftDetailUrlRes.data;
               setTimeout(function() {
                      // $scope.$apply(function() {
                      //   //$scope.geofenceShiftData=shiftDetailUrlRes.data;
                      //   $scope.shiftDatetails=shiftDetailUrlRes.data;
                      // });
                      $scope.exportDataGeoFenceShift('Geofence_Shift_'+$scope.selectedOrg+'_'+shift);
                      stopLoading();
                }, shiftDetailUrlRes.data.length);
          }
         
        }else{
          stopLoading();
          $scope.geoShiftError =  'There is no geofence enteries for selected shift.'; 
          console.log('error');
        }
              
  });
}

$scope.submitFunction   = function(){
    $scope.yesterdayDisabled = false;
     $scope.weekDisabled = false;
     $scope.monthDisabled = false;
     $scope.todayDisabled = false;
     $scope.lastmonthDisabled = false;
     $scope.thisweekDisabled = false;
     startLoading();
     // call only for gps per day report 
    if($scope.isGpsDayReport){
      $scope.getOrgShiftTimeReportExcel('','','','yes');
    }else{
      getOrgShiftsBasedOnOrg();
    }
  }
  

function setBtnEnable(btnName){
   $scope.yesterdayDisabled = false;
   $scope.weekDisabled = false;
   $scope.monthDisabled = false;
   $scope.todayDisabled = false;
   $scope.lastmonthDisabled = false;
   $scope.thisweekDisabled = false;

   switch(btnName){

    case 'yesterday':
      $scope.yesterdayDisabled = true;
      break;
     case 'today':
      $scope.todayDisabled = true;
      break;
    case 'thisweek':
      $scope.thisweekDisabled = true;
      break;
    case 'lastweek':
      $scope.weekDisabled = true;
      break;
    case 'month':
      $scope.monthDisabled = true;
      break;
    case 'lastmonth':
      $scope.lastmonthDisabled = true;
      break;
   }


}
setBtnEnable("init");
$scope.durationFilter    =   function(duration){
  //alert('inside function');
  startLoading();
  var now = new Date();
  $scope.uiDate.todate      = getTodayDate(now.setDate(now.getDate() - 1));
  switch(duration){
    
    case 'yesterday':
       setBtnEnable('yesterday');
       var d = new Date();
       $scope.uiDate.fromdate= getTodayDate(d.setDate(d.getDate() - 1));
     break;
   case 'thisweek':
       setBtnEnable('thisweek');
       var d=new Date();
       var day = d.getDay(),
       diff = d.getDate() - day + (day == 0 ? -6:1);
       var firstday= new Date(d.getFullYear(), d.getMonth(), diff);
       $scope.uiDate.fromdate      = getTodayDate(firstday.setDate(firstday.getDate() ));
       $scope.uiDate.todate= getTodayDate(new Date().setDate(new Date().getDate() ));
    break;
    case 'lastweek':
       setBtnEnable('lastweek');
       var d = new Date();
       var day = d.getDay(),
       diff = d.getDate() - day + (day == 0 ? -6:1)-7;
       var diff1=d.getDate() - day + (day == 0 ? -6:1)-1;
       var firstday= new Date(d.getFullYear(), d.getMonth(), diff);
       var lastday= new Date(d.getFullYear(), d.getMonth(), diff1);
       $scope.uiDate.fromdate      = getTodayDate(firstday.setDate(firstday.getDate() ));
       $scope.uiDate.todate       = getTodayDate(lastday.setDate(lastday.getDate() ));
      //datechange();
     break;
    case 'month':
       setBtnEnable('month');
       var date = new Date();
       var firstdate = new Date(date.getFullYear(), date.getMonth(), 1);
       $scope.uiDate.fromdate       = getTodayDate(firstdate.setDate(firstdate.getDate() ));
       $scope.uiDate.todate       = getTodayDate(date.setDate(date.getDate() ));

     break;
    case 'today':
       setBtnEnable('today');
       var d = new Date();
       $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() ));
       $scope.uiDate.todate       = getTodayDate(d.setDate(d.getDate() ));
     break;
    case 'lastmonth':
       setBtnEnable('lastmonth');
       var date = new Date();
       var firstdate = new Date(date.getFullYear(), date.getMonth()-1, 1);
       var lastdate = new Date(date.getFullYear(), date.getMonth(), 0);
       $scope.uiDate.fromdate       = getTodayDate(firstdate.setDate(firstdate.getDate() ));
       $scope.uiDate.todate      = getTodayDate(lastdate.setDate(lastdate.getDate() ));

       break;
  }
  //webCall();
}
  $scope.exportDataGeoFenceShift = function (xlsVal) {
    var blob = new Blob([document.getElementById('geoshift').innerHTML], {
        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, xlsVal+".xls");
  };
  $scope.exportData = function (xlsVal) {
    var blob = new Blob([document.getElementById(xlsVal).innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, xlsVal+".xls");
        if(document.getElementById('gvMain').clientWidth>(window.screen.width-200)){
          setTimeout(function(){ enhance(); stopLoading(); }, 100);
        }
    };
 $scope.exportgpsPerDayreport = function () {
    if(document.getElementById('gvMain').clientWidth>(window.screen.width-200)){
      startLoading();
      undo();
    }
    $scope.exportData('gpsPerDayreport');
 }
 $("#testLoad").load("../public/menu"); 


}]);
