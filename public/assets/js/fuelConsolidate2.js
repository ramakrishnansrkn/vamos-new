app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global','$translate' , function($scope, $http, vamoservice, $filter, GLOBAL,$translate){

  //global declaration
  $scope.uiDate         =  {};
  $scope.uiValue        =  {};
  //$scope.sort           =  sortByDate('startTime');
  //$scope.totStartFuel   =  0;
  $scope.totEndFuel     =  0;
  $scope.totFuelCon     =  0;
  $scope.totFuelFill    =  0;
  $scope.totFuelTheft   =  0;
  $scope.totStartKms    =  0;
  $scope.totEndKms      =  0;
  $scope.totDist        =  0;
  $scope.totKmpl        =  0;                                                                     
  $scope.totLtrPerHrs   =  0;
  $scope.sensorCount   = "";
  $scope.isTankBased   = false;
  $scope.showOrgTime=false;
  $scope.showDelta = true;
  var language=localStorage.getItem('lang');
  $translate.use(language);
  var translate = $filter('translate');
  var licenceExpiry="";
  var strExpired='expired';
  $scope.tapchanged = true;
  if(localStorage.getItem('licenceExpiry'))licenceExpiry=localStorage.getItem('licenceExpiry');
  $scope.errMsg="";
  var tab               =   getParameterByName('tn');
  var assLabel          =   localStorage.getItem('isAssetUser');
  var expiryDays;
  $scope.licenceerrmsg='Licence is expired. Please contact sales for renewing licence.';
  //console.log(assLabel);
  $scope.sort           =   sortByDate('date',false);

  if( assLabel == "true" ) {
    $scope.vehiLabel  =  "Asset";
    $scope.vehiImage  =  true;
  } else if( assLabel == "false" ) {
    $scope.vehiLabel  =  "Vehicle";
    $scope.vehiImage  =  false;
  } else {
    $scope.vehiLabel  =  "Vehicle";
    $scope.vehiImage  =  false;
  }

  function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  //global declartion
  $scope.locations  =  [];
  $scope.url        =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
  $scope.gIndex     =  0;

  //$scope.locations01 = vamoservice.getDataCall($scope.url);
  $scope.trimColon = function(textVal){
    return textVal.split(":")[0].trim();
  }

  function sessionValue(vid, gname,licenceExpiry){
    localStorage.setItem('user', JSON.stringify(vid+','+gname));
    localStorage.setItem('licenceExpiry', licenceExpiry);
    $("#testLoad").load("../public/menu");
  }

  $scope.ltphValue = function(ms) {
   let hours = (ms / (1000 * 60 * 60)).toFixed(2);
   return hours;   
 }

 $scope.roundOffDecimal = function(val) {
  return parseFloat(val).toFixed(2);
}

$scope.parseInts = function(val) {
  return parseInt(val);   
}

$scope.totalKmpl = function(dist,fuel) {
  var totKmpl = dist/fuel;
  totKmpl     = totKmpl.toFixed(2);
  if(totKmpl=="NaN"||totKmpl==Infinity)totKmpl=0.0;
  return totKmpl;   
}

$scope.totalLtph = function(fuel,engHrs) { 
  var totLtph = fuel/$scope.ltphValue(engHrs);

      //console.log( fuel );
       //console.log( $scope.ltphValue(engHrs) );

       totLtph  = totLtph.toFixed(2);
       if(totLtph=="NaN"||totLtph==Infinity)totLtph=0.0;

       return totLtph;   
     }

     function convert_to_24h(time_str) {
    //console.log(time_str);
    var str       =  time_str.split(' ');
    var stradd    =  str[0].concat(":00");
    var strAMPM   =  stradd.concat(' '+str[1]);
    var time      =  strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
    var hours     =  Number(time[1]);
    var minutes   =  Number(time[2]);
    var seconds   =  Number(time[2]);
    var meridian  =  time[4].toLowerCase();

    if (meridian == 'p' && hours < 12) {
      hours = hours + 12;
    }
    else if (meridian == 'a' && hours == 12) {
      hours = hours - 12;
    }     
    var marktimestr =''+hours+':'+minutes+':'+seconds;      
    return marktimestr;
  };

  $scope.msToTime2 = function(ms) {

    days       =  Math.floor(ms / (24*60*60*1000));
    daysms     =  ms % (24*60*60*1000);
    hours      =  Math.floor((ms)/(60*60*1000));
    hoursms    =  ms % (60*60*1000);
    minutes    =  Math.floor((hoursms)/(60*1000));
    minutesms  =  ms % (60*1000);
    sec        =  Math.floor((minutesms)/(1000));
    hours = (hours<10)?"0"+hours:hours;
    minutes = (minutes<10)?"0"+minutes:minutes;
    sec = (sec<10)?"0"+sec:sec;
      //if(days>=1) {
      //  return (days-1)+"d : "+23+" : "+minutes;
       // } else {
        return hours+" : "+minutes+" : "+sec;
      //}
    }

    // millesec to day, hours, min, sec
    $scope.msToTime = function(ms) {

      days       =  Math.floor(ms / (24 * 60 * 60 * 1000));
      daysms     =  ms % (24 * 60 * 60 * 1000);
      hours      =  Math.floor((ms) / (60 * 60 * 1000));
      hoursms    =  ms % (60 * 60 * 1000);
      minutes    =  Math.floor((hoursms) / (60 * 1000));
      minutesms  =  ms % (60 * 1000);
      seconds    =  Math.floor((minutesms) / 1000);
      //if(days==0)
      //return hours +" h "+minutes+" m "+seconds+" s ";
      //else
      return hours +":"+minutes+":"+seconds;
    }
    
    var delayed4 = (function () {
      var queue = [];

      function processQueue() {
        if (queue.length > 0) {
          setTimeout(function () {
            queue.shift().cb();
            processQueue();
          }, queue[0].delay);
        }
      }

      return function delayed(delay, cb) {
        queue.push({ delay: delay, cb: cb });

        if (queue.length === 1) {
          processQueue();
        }
      };
    }());

    function google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent) {
      vamoservice.getDataCall(tempurlEvent).then(function(data) {
        $scope.addressEvent[index4] = data.results[0].formatted_address;
      //console.log(' address '+$scope.addressEvent[index4])
      //var t = vamo_sysservice.geocodeToserver(latEvent,lonEvent,data.results[0].formatted_address);
    })
    };

    $scope.recursiveEvent   =   function(locationEvent, indexEvent) {
      var index4 = 0;
      angular.forEach(locationEvent, function(value ,primaryKey){
      //console.log(' primaryKey '+primaryKey)
      index4 = primaryKey;
      if(locationEvent[index4].address == undefined)
      {
        var latEvent     =   locationEvent[index4].latitude;
        var lonEvent     =   locationEvent[index4].longitude;
        var tempurlEvent   =   "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latEvent+','+lonEvent+"&sensor=true";

        delayed4(2000, function (index4) {
          return function () {
            google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent);
          };
        }(index4));
      }
    })
    }


    function formatAMPM(date) {
      var date = new Date(date);
      var hours = date.getHours();
      var minutes = date.getMinutes();
      var ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
      return strTime;
    }

    $scope.orgName = '';

    function totalValues(data) {

      var startFuel        =  0.0;
      var endFuel          =  0.0;
      var fuelConsumption  =  0.0;
      var fuelFilling      =  0.0;
      var fuelDispenser=0.0;
      var fuelTheft        =  0.0;
      var startKms         =  0.0;
      var endKms           =  0.0;
      var dist             =  0.0;
      var kmpl             =  0.0;    
      var ltrsPerHrs       =  0.0;

      var engineIdleHrs    =  0;
      var ignitionHrs      =  0;
      var engineHrs        =  0;
      var secIgnitionHrs   =  0;
      var fuelBalance      =  0;


      angular.forEach(data, function(val, key) {
        if(val!=null){
              //console.log(val);
              engineIdleHrs    =  engineIdleHrs+parseInt(val.engineIdleHrs);
              ignitionHrs      =  ignitionHrs+parseInt(val.ignitionHrs);
              engineHrs        =  engineHrs+parseInt(val.engineRunningHrs);
              if(val.secondaryEngineDuration==null)val.secondaryEngineDuration=0;
              secIgnitionHrs   =  secIgnitionHrs+parseInt(val.secondaryEngineDuration);

              //console.log(engineIdleHrs);


              startKms         =  startKms+parseFloat(val.startKms);
              endKms           =  endKms+parseFloat(val.endKms);
              dist             =  dist+parseFloat(val.dist);
              var sensorData = null;
              if(parseInt(val.sensor)==1){
               sensorData = val.fuelsensors[0];
             }else{
              sensorData = val.allSensor;
              fuelBalance  =  fuelBalance+parseFloat(sensorData.fuelBalance);
            }
            if(sensorData){
              startFuel        =  startFuel+parseFloat(sensorData.startFuel);
              endFuel          =  endFuel+parseFloat(sensorData.endFuel);
              fuelConsumption  =  fuelConsumption+parseFloat(sensorData.fuelConsumption);
              fuelFilling      =  fuelFilling+parseFloat(sensorData.fuelFilling);
              fuelDispenser=fuelDispenser+parseFloat(sensorData.fuelDispenser);
              fuelTheft        =  fuelTheft+parseFloat(sensorData.fuelTheft);
              kmpl             =  kmpl+parseFloat(sensorData.kmpl);     
              ltrsPerHrs       =  ltrsPerHrs+parseFloat(sensorData.ltrsPerHrs);
            }


          }

        });  

      $scope.startFuel        =  startFuel;
      $scope.endFuel          =  endFuel;
      $scope.fuelConsumption  =  fuelConsumption;
      $scope.fuelFilling      =  fuelFilling;
      $scope.fuelDispenser=fuelDispenser;
      $scope.fuelTheft        =  fuelTheft;
      $scope.startKms         =  startKms;
      $scope.endKms           =  endKms;
      $scope.dist             =  dist;
      $scope.kmpl             =  kmpl.toFixed(2);     
      $scope.ltrsPerHrs       =  ltrsPerHrs.toFixed(2);

      $scope.engineIdleHrs    =  engineIdleHrs;
      $scope.ignitionHrs      =  ignitionHrs;
      $scope.engineHrs        =  engineHrs;
      $scope.secIgnitionHrs   =  secIgnitionHrs;
      $scope.fuelBalance   =  fuelBalance;

      console.log( 'Engine Hours....  :  ' + $scope.engineHrs );
              //console.log(  $scope.startFuel  );
            }

            function setBtnEnable(btnName){
             $scope.yesterdayDisabled = false;
             $scope.weekDisabled = false;
             $scope.monthDisabled = false;
             $scope.lastmonthDisabled = false;
             $scope.thisweekDisabled = false;

             switch(btnName){

              case 'yesterday':
              $scope.yesterdayDisabled = true;
              break;
              case 'thisweek':
              $scope.thisweekDisabled = true;
              break;
              case 'lastweek':
              $scope.weekDisabled = true;
              break;
              case 'month':
              $scope.monthDisabled = true;
              break;
              case 'lastmonth':
              $scope.lastmonthDisabled = true;
              break;
            }


          }
          setBtnEnable("init");
          $scope.durationFilter    =   function(duration){
  //alert('inside function');
  startLoading();
  var now = new Date();
  $scope.uiDate.todate      = getTodayDate(now.setDate(now.getDate() - 1));
  switch(duration){

    case 'yesterday':
    
    setBtnEnable('yesterday');

    var d = new Date();
    
    if($scope.showOrgTime){
      $scope.uiDate.fromdate= getTodayDate(d.setDate(d.getDate() - 1));
      $scope.uiDate.fromdate= getTodayDate(d.setDate(d.getDate()));
      
      calculateDateTime();
    }
    else {
      $scope.uiDate.fromdate= getTodayDate(d.setDate(d.getDate() - 1));}

      $scope.uiDate.totime  = '11:59 PM';
      // datechange();
      
      break;
      case 'thisweek':
      setBtnEnable('thisweek');

      var d=new Date();
      var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1);
      var firstday= new Date(d.getFullYear(), d.getMonth(), diff);

      
      $scope.uiDate.fromdate      = getTodayDate(firstday.setDate(firstday.getDate()-1 ));
      $scope.uiDate.todate= getTodayDate(new Date().setDate(new Date().getDate()-1 ));

      $scope.uiDate.totime       = '11:59 PM';
      if($scope.showOrgTime){

        calculateDateTime();
      }


      //datechange();
      break;
      case 'lastweek':
      setBtnEnable('lastweek');
      
      var d = new Date();
      var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1)-7;
      var diff1=d.getDate() - day + (day == 0 ? -6:1)-1;
      var firstday= new Date(d.getFullYear(), d.getMonth(), diff);
      var lastday= new Date(d.getFullYear(), d.getMonth(), diff1);

      
      $scope.uiDate.fromdate      = getTodayDate(firstday.setDate(firstday.getDate() ));
      $scope.uiDate.todate       = getTodayDate(lastday.setDate(lastday.getDate() ));
      $scope.uiDate.totime  = '11:59 PM';
      
      //datechange();
      
      break;
      case 'month':
      setBtnEnable('month');

      var date = new Date();
      var firstdate = new Date(date.getFullYear(), date.getMonth(), 1);

      
      $scope.uiDate.fromdate       = getTodayDate(firstdate.setDate(firstdate.getDate()));

      $scope.uiDate.todate       = getTodayDate(date.setDate(date.getDate()-1 ));

      $scope.uiDate.totime      = '11:59 PM';
      if($scope.showOrgTime){

        calculateDateTime();
      }

       //datechange();
       
       break;
       case 'lastmonth':

       var date = new Date();
       var firstdate = new Date(date.getFullYear(), date.getMonth()-1, 1);
       var lastdate = new Date(date.getFullYear(), date.getMonth(), 0);


       $scope.uiDate.fromdate       = getTodayDate(firstdate.setDate(firstdate.getDate() ));
       $scope.uiDate.todate      = getTodayDate(lastdate.setDate(lastdate.getDate() ));

       $scope.uiDate.totime   = '11:59 PM';

      //datechange();
      
      break;

    }
    webCall();
  }

  function webCall() {
    console.log($scope.orgTime);
    $scope.showDelta = false;
    var fromDate=moment($scope.uiDate.fromdate,'DD-MM-YYYY').format('YYYY-MM-DD');
    var toDate=moment($scope.uiDate.todate,'DD-MM-YYYY').format('YYYY-MM-DD');
    var shifttime=$scope.orgTime=='12:00 - 23:59'?0:$scope.orgTime;


        //var fuelConUrl  =  GLOBAL.DOMAIN_NAME+'/getConsolidatedFuelReportBasedOnVehicle?vehicleId='+$scope.vehIds+'&fromDate='+$scope.uiDate.fromdate+'&toDate='+$scope.uiDate.todate;
        if($scope.showOrgTime){
          var fuelConUrl  =  GLOBAL.DOMAIN_NAME+'/getConsolidatedVehicleWise?vehicleId='+$scope.vehIds+'&fromDate='+fromDate+'&toDate='+toDate+'&shiftTime='+shifttime;}
          else {
            var fuelConUrl  =  GLOBAL.DOMAIN_NAME+'/getConsolidatedVehicleWise?vehicleId='+$scope.vehIds+'&fromDate='+fromDate+'&toDate='+toDate;
          }
          console.log( fuelConUrl );
          $scope.fuelConData = [];
          var expdate=moment().add(expiryDays,'days').format('DD-MM-YYYY'),
          convertedexpdate=utcFormat(expdate,convert_to_24h('11:59 PM')),
          convertedtodate=utcFormat($scope.uiDate.todate,convert_to_24h('11:59 PM')),
          convertedfromdate=utcFormat($scope.uiDate.fromdate,convert_to_24h('11:59 PM'));
          if(convertedtodate<=convertedexpdate && convertedfromdate<=convertedexpdate){
            $http.get(fuelConUrl).success(function(data) {
              stopLoading();
              if(data!='Failed'){
                $scope.vehiMode=data[0].vehicleMode;
                if(!data[0].error){
                 totalValues(data);
                 $scope.fuelConData = data.map(function (value, key) {
                  if(parseInt(value.sensor)>1){
                    if($scope.isTankBased){
                     value.sensor = value.nTanks;
                     value.fuelsensors = value.tankData;
                   }else{
                    $scope.showDelta = true;
                    value.fuelsensors.push(value.allSensor)
                  }
                }
                return value;
              });
               }else{
                $scope.fuelConData = data;
              }


              if($scope.fuelConData.length!=0){
                $scope.orgName  =  $scope.fuelConData[0].orgId;
              }
            }else{
              $scope.fuelConData =  [{ 'error' : translate("curl_error") }];
            }    


          }); 
          }else{
            stopLoading();
            $scope.errMsg=licenceExpiry;
          } 
        }

  //get the value from the ui
  function getUiValue() {
    $scope.uiDate.fromdate   =  $('#dtFrom').val();
    $scope.uiDate.fromtime   =  $('#timeFrom').val();
    $scope.uiDate.todate     =  $('#dtTo').val();
    $scope.uiDate.totime     =  $('#timeTo').val();
  }


// service call for the event report

/*function webServiceCall(){
    $scope.siteData = [];
    if((checkXssProtection($scope.uiDate.fromdate) == true) && (checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true)) {
      
      var url   = GLOBAL.DOMAIN_NAME+"/getActionReport?vehicleId="+$scope.vehiname+"&fromDate="+$scope.uiDate.fromdate+"&fromTime="+convert_to_24h($scope.uiDate.fromtime)+"&toDate="+$scope.uiDate.todate+"&toTime="+convert_to_24h($scope.uiDate.totime)+"&interval="+$scope.interval+"&stoppage="+$scope.uiValue.stop+"&stopMints="+$scope.uiValue.stopmins+"&idle="+$scope.uiValue.idle+"&idleMints="+$scope.uiValue.idlemins+"&notReachable="+$scope.uiValue.notreach+"&notReachableMints="+$scope.uiValue.notreachmins+"&overspeed="+$scope.uiValue.speed+"&speed="+$scope.uiValue.speedkms+"&location="+$scope.uiValue.locat+"&site="+$scope.uiValue.site+'&fromDateUTC='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateUTC='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
      vamoservice.getDataCall(url).then(function(responseVal){
        $scope.recursiveEvent(responseVal, 0);
        $scope.eventData = responseVal;
        var entry=0,exit=0; 
        angular.forEach(responseVal, function(val, key){
          if(val.state == 'SiteExit')
            exit++ 
          else if (val.state == 'SiteEntry')
            entry++
        })
        $scope.siteEntry  = entry;
        $scope.siteExit   = exit;
        stopLoading();
      });
    }
    stopLoading();
  }*/

  // initial method
  $scope.total = function(val1, val2){
    return parseFloat(val1)+parseFloat(val2);
  }
  function initialAPICall(){
    vamoservice.getDataCall($scope.url).then(function(data) {          
          //startLoading();
          $scope.selectVehiData  =  [];
          $scope.vehicle_group   =  [];
          $scope.vehicle_list    =  data;

          if(data.length) {

            $scope.vehiname  =  getParameterByName('vid');
            $scope.uiGroup   =  $scope.trimColon(getParameterByName('vg'));
            $scope.gName   =  getParameterByName('vg');
        //console.log(data);
        //$scope.gName   =  data[0].group;
          //console.log(data[0].group);

          if($scope.vehiname == 'undefined' || $scope.vehiname == '') {
           $scope.vehiname  =  data[$scope.gIndex].vehicleLocations[0].vehicleId; 
           licenceExpiry=data[$scope.gIndex].vehicleLocations[0].licenceExpiry;
           expiryDays=data[$scope.gIndex].vehicleLocations[0].expiryDays;
           $scope.sensorCount   = data[$scope.gIndex].vehicleLocations[0].sensorCount;
           $scope.vehicleMode   = data[$scope.gIndex].vehicleLocations[0].vehicleMode;
           $scope.vehicleModel   = data[$scope.gIndex].vehicleLocations[0].vehicleModel;
             //alert($scope.vehiname );
           } 

           if($scope.gName == 'undefined' || $scope.gName == ''){
             $scope.gName  =  data[$scope.gIndex].group; 
           } 

           angular.forEach(data, function(val, key) {
              //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
              if($scope.gName == val.group) {
                $scope.gIndex = val.rowId;

                angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys){

                  $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

                  if($scope.vehiname == value.vehicleId){
                    $scope.shortNam  =  value.shortName;
                    $scope.vehIds   = value.vehicleId;
                    licenceExpiry=value.licenceExpiry;
                    expiryDays=value.expiryDays;
                    $scope.sensorCount   = value.sensorCount;
                    $scope.vehicleMode   = value.vehicleMode;
                    $scope.vehicleModel   = value.vehicleModel;
                  }
                });
              }
            });
        //console.log($scope.selectVehiData);
        sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
        $scope.notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
        $('#notncount').text($scope.notncount);
        window.localStorage.setItem('totalNotifications',$scope.notncount);
      }

      var dateObj1       =   new Date();
      var  dateNow        = new Date(dateObj1.setDate(dateObj1.getDate()));
      var fromdatenow         = getTodayDate(dateNow);
            //alert(fromdatenow);


            var dateObj       =   new Date();
            $scope.fromNowTS    = new Date(dateObj.setDate(dateObj.getDate()-1));


            $scope.uiDate.fromdate    = getTodayDate(new Date().setDate(new Date().getDate() - 1));
            $scope.uiDate.todate    = getTodayDate(new Date().setDate(new Date().getDate()-1));
            $scope.uiDate.fromtime    = localStorage.getItem('fromTime');
            $scope.uiDate.totime    =  localStorage.getItem('toTime');
            if(localStorage.getItem('timeTochange')!='yes'){
              updateToTime();
              $scope.uiDate.totime    =   localStorage.getItem('toTime');
            }


      //webServiceCall();
      startLoading();
      if($scope.showOrgTime){
        calculateDateTime();}
        webCall();
      //stopLoading();
    }); 
  }
  $scope.$watch("url", function (val) {
   $scope.orgShiftTime=[];
   $scope.orgShiftName=[];
   var timeUrl    = GLOBAL.DOMAIN_NAME+'/getOrgShiftTimeList';
   vamoservice.getDataCall(timeUrl).then(function(response){

    if(response.data==0){
      $scope.showOrgTime=false;
      initialAPICall();
    }
    else {

      $scope.showOrgTime=true;
      for(i=1;i<response.data.length;i++){
        if(response.data[i]!="00:00 - 23:59"){

          var orgnametime=response.data[i].split(',');

          var t=orgnametime[0].split('-');
          var t1=t[0].split(':');
          var t2=t[1].split(':');
          if(t1[0]=='00'){
            $scope.orgShiftTime.push('12:'+t1[1]+'-'+t[1]);
          }
          else if(t2[0]=='00'){
            $scope.orgShiftTime.push(t[0]+'-'+'12:'+t2[1]);
          }
          else {
            $scope.orgShiftTime.push(orgnametime[0]);
          }

        }
        else {

          var t=response.data[i].split('-');
          var t1=t[0].split(':');
          var t2=t[1].split(':');
          if(t1[0]=='00'){
            $scope.orgShiftTime.push('12:'+t1[1]+'-'+t[1]);
          }
          else if(t2[0]=='00'){
            $scope.orgShiftTime.push(t[0]+'-'+'12:'+t2[1]);
          }


        }
        $scope.orgShiftName.push(response.data[i]);

      }
      $scope.organisation=response.data[1].split(',');

      $scope.organName=$scope.organisation[1];
      var res=response.data[0];
      if(res=='00:00 - 23:59'){
        var index=$scope.orgShiftTime.indexOf('12:00 - 23:59');
        $scope.orgTime=$scope.orgShiftTime[index];}
        else {
          var index=$scope.orgShiftTime.indexOf(response.data[0]);
          $scope.orgTime=$scope.orgShiftTime[index];
        }
        if($scope.orgTime=='12:00 - 23:59'){
          $scope.showOrganization=false;
          $scope.fromtotime=('00:00 - 23:59').split('-');
        }
        else {
          $scope.showOrganization=true;
          $scope.fromtotime=$scope.orgTime.split('-');
        }

        initialAPICall(); 

      }


    });

 });


  $scope.changeTime=function(time){
    $scope.orgTime=time;
    



    
  }

  function calculateDateTime(){
    if($scope.orgTime=='12:00 - 23:59'){
      $scope.showOrganization=false;
      $scope.fromtotime=('00:00 - 23:59').split('-');
    }
    else {
      $scope.showOrganization=true;
      $scope.fromtotime=$scope.orgTime.split('-');

    }

    var index=$scope.orgShiftTime.indexOf($scope.orgTime);
    var name=$scope.orgShiftName[index].split(',');
    $scope.organName=name[1];
    var frmhrs=$scope.fromtotime[0].split(':');
    var tohrs=$scope.fromtotime[1].split(':');

    var dateString1 = moment($scope.uiDate.fromdate,'DD-MM-YYYY').format('MM-DD-YYYY'); 
    var hrs=new Date().getHours();
    var date1 = new Date(dateString1);
    var dateString2 = moment($scope.uiDate.todate,'DD-MM-YYYY').format('MM-DD-YYYY'); 
    yesterdaydate=getTodayDate(new Date().setDate(new Date().getDate()-1));
    todaydate=getTodayDate(new Date().setDate(new Date().getDate()));
    var startDate = moment($scope.uiDate.fromdate, 'DD-MM-YYYY');
    var endDate = moment($scope.uiDate.todate, 'DD-MM-YYYY');

    var dayDiff = endDate.diff(startDate, 'days');

    var date2 = new Date(dateString2);
    if($scope.uiDate.fromdate==$scope.uiDate.todate){
      if($scope.orgTime=='12:00 - 23:59'){
        $scope.uiDate.fromdate=getTodayDate(date1.setDate(date1.getDate()));
        $scope.uiDate.todate=getTodayDate(date2.setDate(date2.getDate()));
      }
      else if(parseInt(tohrs[0])>parseInt(frmhrs[0]) && parseInt(tohrs[0])<parseInt(hrs)){
        $scope.uiDate.fromdate=getTodayDate(date1.setDate(date1.getDate()));
        $scope.uiDate.todate=getTodayDate(date2.setDate(date2.getDate()));
      }
      
      else {
        $scope.uiDate.fromdate=getTodayDate(date1.setDate(date1.getDate()));
        $scope.uiDate.todate=getTodayDate(date1.setDate(date1.getDate()+1));
      }
    }
    else if((dayDiff==1)&&(getTodayDate(date2.setDate(date2.getDate()))!=yesterdaydate && getTodayDate(date2.setDate(date2.getDate()))!=todaydate)){

     if(parseInt(tohrs[0])>parseInt(frmhrs[0])&& parseInt(tohrs[0])<parseInt(hrs)){
      $scope.uiDate.fromdate=getTodayDate(date1.setDate(date1.getDate()));
      $scope.uiDate.todate=getTodayDate(date2.setDate(date2.getDate()));
    }
    else if($scope.orgTime=='12:00 - 23:59'){
     $scope.uiDate.fromdate=getTodayDate(date1.setDate(date1.getDate()));
     $scope.uiDate.todate=getTodayDate(date2.setDate(date2.getDate()));
   }
   else {
    $scope.uiDate.fromdate=getTodayDate(date1.setDate(date1.getDate()));
    $scope.uiDate.todate=getTodayDate(date2.setDate(date2.getDate()+1));}
  }
  else {   

    if(getTodayDate(date2.setDate(date2.getDate()))==yesterdaydate && $scope.orgTime!='12:00 - 23:59'){
      if(parseInt(tohrs[0])>parseInt(frmhrs[0])&& parseInt(tohrs[0])<parseInt(hrs)){
        $scope.uiDate.fromdate=getTodayDate(date1.setDate(date1.getDate()));
        $scope.uiDate.todate=getTodayDate(date2.setDate(date2.getDate()+1));
      }
      
      else {
        $scope.uiDate.fromdate=getTodayDate(date1.setDate(date1.getDate()));
        $scope.uiDate.todate=getTodayDate(date2.setDate(date2.getDate()+1));}
      }
      else if((getTodayDate(date2.setDate(date2.getDate()))==todaydate) && $scope.orgTime=='12:00 - 23:59'){
        $scope.uiDate.fromdate=getTodayDate(date1.setDate(date1.getDate()));
        $scope.uiDate.todate=getTodayDate(date2.setDate(date2.getDate()-1));
      }
      else {
        $scope.uiDate.fromdate=getTodayDate(date1.setDate(date1.getDate()));
        $scope.uiDate.todate=getTodayDate(date2.setDate(date2.getDate()));
      }

    }

  }

  $scope.getAMPM=function(time){
    var str       =  time.split(':');     
    var hours     =  Number(str[0]);
    var minutes   =  Number(str[1]);

    if(hours=='00'){
     return ('12'+':'+('0'+minutes).slice(-2)).concat(" ",'AM');
   }

   else if(hours==12){
     return ('12'+':'+('0'+minutes).slice(-2)).concat(" ",'PM');
   }
   else if(hours < 12) {
    return (hours+':'+('0'+minutes).slice(-2)).concat(" ",'AM');
  }
  else{
    hours=hours-12;
    return (hours+':'+('0'+minutes).slice(-2)).concat(" ",'PM');
  }     


}
$scope.groupSelection = function(groupName, groupId) {
  startLoading();
  licenceExpiry="";
  $scope.errMsg="";
  $scope.gName  =  groupName;
  $scope.uiGroup  =  $scope.trimColon(groupName);
  $scope.gIndex =  groupId;
  var url     =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+groupName;

  vamoservice.getDataCall(url).then(function(response) {

    $scope.vehicle_list     =  response;
    $scope.shortNam       =  response[$scope.gIndex].vehicleLocations[0].shortName;
    $scope.vehiname       =  response[$scope.gIndex].vehicleLocations[0].vehicleId;
    licenceExpiry   = response[$scope.gIndex].vehicleLocations[0].licenceExpiry;
    expiryDays  = response[$scope.gIndex].vehicleLocations[0].expiryDays;
    $scope.sensorCount   = response[$scope.gIndex].vehicleLocations[0].sensorCount;
    $scope.vehicleMode   = response[$scope.gIndex].vehicleLocations[0].vehicleMode;  
    $scope.vehicleModel   = response[$scope.gIndex].vehicleLocations[0].vehicleModel;  
    sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
    $scope.notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
    $('#notncount').text($scope.notncount);
    window.localStorage.setItem('totalNotifications',$scope.notncount);
    $scope.selectVehiData   =  [];
            //console.log(response);
            angular.forEach(response, function(val, key) {
              if($scope.gName == val.group){
          //  $scope.gIndex = val.rowId;
          angular.forEach(response[$scope.gIndex].vehicleLocations, function(value, keys) {

            $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

            if($scope.vehiname == value.vehicleId){
              $scope.shortNam  =  value.shortName;
              $scope.vehIds   = value.vehicleId;
              expiryDays= value.expiryDays;
            }
          });
        }
      });

            getUiValue();
            webCall();

      //webServiceCall();
        //stopLoading();
      });

}


$scope.genericFunction  = function (vehid, index,shortname,position, address,groupName,licenceExp){
 startLoading();
 licenceExpiry=licenceExp;
 $scope.errMsg="";
 $scope.vehiname    = vehid;
 angular.forEach($scope.vehicle_list[$scope.gIndex].vehicleLocations, function(val, key){
  if(vehid == val.vehicleId) {
    $scope.shortNam = val.shortName;
    $scope.vehIds   = val.vehicleId;
    $scope.sensorCount   = val.sensorCount;
    $scope.vehicleMode   = val.vehicleMode;
    $scope.vehicleModel=val.vehicleModel;
    expiryDays= val.expiryDays;
  }
});
 sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
 getUiValue();
 webCall();
   //webServiceCall();
 }

 $scope.getSensorDetail   = function(){
  $scope.showDelta = true;
  $scope.isTankBased =  false;
  startLoading();
  webCall();

}
$scope.getTankDetail   = function(){
  $scope.showDelta = true;
  $scope.isTankBased =  true;
  startLoading();
  webCall();
}




$scope.submitFunction   = function(){

 $scope.yesterdayDisabled = false;
 $scope.weekDisabled = false;
 $scope.monthDisabled = false;
 $scope.lastmonthDisabled = false;
 $scope.thisweekDisabled = false;
 startLoading();

 getUiValue();
 if($scope.showOrgTime){
  $scope.uiDate.fromdate   =   $('#dtFrom').val();
  $scope.uiDate.todate   =   $('#dtTo').val();
  calculateDateTime();

}
webCall();
        //webServiceCall();
          //stopLoading();
        }

        $scope.exportData = function (data) {
    //console.log(data);
    
    var blob = new Blob([document.getElementById(data).innerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, data+".xls");

  };

  $scope.exportDataCSV = function (data) {
    //console.log(data);
    
    CSV.begin('#'+data).download(data+'.csv').go();
    
  };
  $scope.generatePDF = function() {
    let tableName='Vehicle Wise Fuel Report';
    let vehiclename=$scope.shortNam;
    var doc = new jsPDF({
     orientation: 'l',
     unit: 'mm',
     format: 'a3',
   });
    var table1=doc.autoTableHtmlToJson(document.getElementById("table1"));
    var table2 = doc.autoTableHtmlToJson(document.getElementById("table2"));
    doc.setFontSize(12);
    doc.text(`${tableName.toUpperCase()}`,5,10);
    doc.setFontSize(12);
    doc.text(`${"VehicleName: "}${vehiclename.toUpperCase()}`,70,10);
        // doc.autoTable(table1.columns, table1.rows, {
        //     theme: 'plain', 
        //     margin: [15,5,10,5],
        //     tableLineColor: [189, 195, 199],
        //     styles: {
        //       fontSize: 10,
        //       overflow: 'linebreak',
        //       lineWidth: 0.01,
        //     }
        //   });
        // table2.rows.shift();
        doc.autoTable(table2.columns, table2.rows, {
          theme: 'plain', 
          margin: {
            top: 15,
            left: 5,
            right: 5,
            bottom: 5 },
            tableLineColor: [189, 195, 199],
            styles: {
              fontSize: 10,
              overflow: 'linebreak',
              lineWidth: 0.01
            },
            pageBreak: 'always',
            columnStyles: {
              0: { columnWidth: 'wrap' },
              1: { columnWidth: 20 },
              2: { columnWidth: 15 },
              3: { columnWidth: 15 },
              4: { columnWidth: 15 },
              5: { columnWidth: 15 },
              6: { columnWidth: 15 },
              7: { columnWidth: 15 },
              8: { columnWidth: 15 },
              9: { columnWidth: 15 },
              10: { columnWidth: 15 },
              11: { columnWidth: 15 },
              12: { columnWidth: (!$scope.showDelta)? 25 : 15 },
              13: { columnWidth: 25 },
              14: { columnWidth: ($scope.showDelta)? 25 : 15  },
              15: { columnWidth: (!$scope.showDelta)? 25 : 15 },
              16: { columnWidth: 25 },
              17: { columnWidth: ($scope.showDelta)? 25 : 15 },
              18: { columnWidth: 15 },
              19: { columnWidth: 15 },
              20: { columnWidth: 15 },
              21: { columnWidth: 10 },
              22: { columnWidth: 10 },
            }
          });
        doc.save(tableName+'.pdf');
      }

      $('#minus').click(function(){
        $('#menu').toggle(1000);
      })


      $(window).scroll(function (event) {
        if($scope.tapchanged){
          $scope.tapchanged=false;
          console.log('scrolled');
          $('.table-fixed-header1').fixedHeader();
        }
      });


    }]);
