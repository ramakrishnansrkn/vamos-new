
app.directive('map', function($http, vamoservice) {
	return {
		restrict: 'E',
		replace: true,
		template: '<div></div>',
		link: function(scope, element, attrs) {

		}
	};
});

app.directive('maposm', function($http, vamoservice) {
	return {
		restrict: 'E',
		replace: true,
		template: '<div></div>',
		link: function(scope, element, attrs){
        // console.log('osm map directive...');
    }
}
});

var modalss = document.getElementById('poi');
var spanss  = document.getElementsByClassName("poi_close")[0];

function popUp_Open_Close(){

	alert('hello...');

	modalss.style.display = "block";
	modalss.style.zIndex= 9999;
	spanss.onclick = function() {
		modalss.style.display = "none";
	}
}  

var poiLat;
var	poiLng;

function addPoi(lat,lng) {

	poiLat = lat;
	poiLng = lng;

	alert('add poi....lat'+lat+'lng'+lng);

	// $scope.poiLat = lat;
	   //$scope.poiLng = lng;
	   popUp_Open_Close();

	}

	$('#notifyMsg').hide();

	var timeOutVar;   

	function setsTimeOuts() {
      //alert('timeOut');
      $("#notifyS").hide(1200);
      $("#notifyF").hide(1200); 
      if(timeOutVar!=null){
          //console.log('timeOutVar'+timeOutVar);
          clearTimeout(timeOutVar);
      }
  }

  function success(lat,lng) {   

  	$('#notifyS span').text('Successfully updated !..');
  	$('#notifyMsg').show();
  	$("#notifyS").show(500);
  	$("#notifyF").hide(); 
  	timeOutVar = setTimeout(setsTimeOuts, 2000);
  }

  function fails(){

  	alert('faillll...');

  	$('#notifyF span').text("Enter all the field / Mark the Site ");
  	$('#notifyMsg').show();
  	$("#notifyF").show(500);
  	$("#notifyS").hide(); 
  	timeOutVar = setTimeout(setsTimeOuts, 2000);

  }


  var speedSize='100%',speedCenter='100%',startangle=-90,endangle=90,sensor=1;
  app.controller('mainCtrl',['$scope', '$http', 'vamoservice', '_global','$filter','$translate', function($scope, $http, vamoservice, GLOBAL,$filter,$translate){ 
  	var language=localStorage.getItem('lang');
  	$scope.multiLang=language;
  	$translate.use(language);
  	console.log('track');
  	var translate = $filter('translate');
  	var Address = translate("Address")
  	$scope.lineCount_osm  =  0;
  	$scope.newArr         =  [];
  	$scope.mapInitOsm     =  0;
  	$scope.mapInitGoog    =  0;
  	$scope.polyInit       =  0;
  	$scope.noOfTank = 1;
  	$scope.fuelLitreArr=[0,0,0,0,0];
  	$scope.tankSizeArr=[0,0,0,0,0];
  	$scope.osmPaths       =  [];
  //$scope.polyline       =  [];
  var lineCountOsm      =  0;
  var markerInit        =  0;
  var rotationAngle=0;
  var deg=0;
  var distance=0;
  var latLngdiff=0;
  var distanceInterval=2;
  var gDistanceInterval=2;
  var liveInterval=2;
  var currZoom=13;
  var gzoom = 13;
  var _mapsDetails = {};
  var osm_mapsDetails = {};
  var arrowMarker={};
  var liveArrows={};
  var gLiveArrows={};
  var removedArrow={};
  var gRemovedArrow ={};
  var gArrowMarker={};
  var gArrows={};
  var arrows={};
  $scope.expectedFuelMileage;
  $scope.SiteCheckbox = {
  	value1 : true,
  	value2 : 'YES'
  }
  $scope.SiteCheckbox.value1='YES';

  var mapsVal           =  localStorage.getItem('mapNo');
  if(mapsVal==null)
  {
  	mapsVal=1;
  }
  console.log(mapsVal);
  $scope.polylineCheck='YES';
 //    $scope.polylineCheck = {
	//   value1: 'YES',
	//   value2 : 'NO'
	// }


	$scope.range = function(min, max, step) {
		step = step || 1;
		var input = [];
		for (var i = min; i <= max; i += step) {
			input.push(i);
		}
		return input;
	};

	$scope.hideHistoryDetail = function(val) {

		if(val == 'NO') {
			$scope.polylineCheck='NO';
  	 //alert($scope.polylineCheck);
  	 if($scope.maps_no==0) { 
  	 	HistoryPath.setMap(null);
  	 	startMarker.setMap(null);
  	 	gArrows=gArrowMarker;
  	 	for(key in gArrows) {
  	 		gArrows[key].setMap(null);
  	 	}
  	 	gArrows = {};
  	 }else{
  	 	$scope.map_osm.removeLayer($scope.markerStart);
  	 	$scope.map_osm.removeLayer(polylinesOSM); 
  	 	arrows=arrowMarker;
  	 	for(key in arrows) {
  	 		$scope.map_osm.removeLayer(arrows[key]);
  	 	}
  	 	arrows = {};
  	 }


  	} else if(val == 'YES') {
  	//alert($scope.polylineCheck);
  	$scope.polylineCheck='YES';
  	if($scope.maps_no==0) {
  		HistoryPath.setMap($scope.map);
  		startMarker.setMap($scope.map);
  		if($scope.SiteCheckbox.value1=='YES'){
  			gArrows=gArrowMarker;
  			for(key in gArrows) {
  				gArrows[key].setMap($scope.map);
  			}
  		}
  	}
  	else{
  		polylinesOSM=L.polyline(latLngArrOSM, {color: '#4F2412'}).setStyle({weight: 5}).addTo($scope.map_osm);
  		$scope.markerStart     = new L.marker(startFlagVal,{icon: myIconStart});
  		$scope.markerStart.addTo($scope.map_osm);
  		if($scope.SiteCheckbox.value1=='YES'){
  			arrows=arrowMarker;
  			for(key in arrows) {
  				$scope.map_osm.addLayer(arrows[key]);
  			}
  		}
  	}
  } 
}
var assLabel         =  localStorage.getItem('isAssetUser');
$scope.trvShow       =  localStorage.getItem('trackNovateView');

if(assLabel=="true") {
	$scope.vehiLabel = "Asset";
} else if(assLabel=="false") {
	$scope.vehiLabel = "Vehicle";
} else {
	$scope.vehiLabel = "Vehicle";
}

if(mapsVal==0) {
	$scope.maps_no  = 0;
	$scope.mapsHist = 0;

	document.getElementById("map_canvas2").style.display="none"; 
	document.getElementById("map_canvas").style.display="block"; 

} else if(mapsVal==1) {
	$scope.maps_no  = 1;
	$scope.mapsHist = 1;

	document.getElementById("map_canvas").style.display="none"; 
	document.getElementById("map_canvas2").style.display="block"; 
}
var d = new Date();
var toUtc = d.getTime();
d.setUTCHours(0,0,0,0);
var fromUtc = d.getTime();
var latLngArr=[];
var latLngArrOSM=[];
var res                 =  document.location.href.split("?");
$scope.vehicleno        =  getParameterByName('vehicleId');
$scope.url              =  GLOBAL.DOMAIN_NAME+'/getSelectedVehicleLocation?'+res[1];
$scope.urlVeh           =  GLOBAL.DOMAIN_NAME+'/getSelectedVehicleLocation1?'+res[1];
$scope.latLngUrl        =  GLOBAL.DOMAIN_NAME+'/getVehicleHistoryForOnlyLatLng?'+res[1]+'&fromDateUTC='+fromUtc+'&toDateUTC='+toUtc;
$scope.orgUrl           =  GLOBAL.DOMAIN_NAME+'/viewSite';
$scope.path             =  [];
$scope.firstPath        =  [];
$scope.speedval         =  [];
$scope.inter            =  0;
$scope.cityCircle       =  [];
$scope.cityCirclecheck  =  false;
$scope.histVal 	        =  [];
$scope.rotationd        =  null;
$scope.videoLink="https://www.youtube.com/watch?v=AyFyNAIxoxI&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=14";
var np          =  [];
var npl         =  null;
var npls        =  null;
var nplen       =  null;
var linesCount  =  1;  
var initsvalss  =  0;
var polylinesOSM;
var startFlagVal;
var myIconStart;
var startMarker;
var startmarkerLatlng;
var HistoryPath;
var osmVisit="no";

$('#graphsId').hide();
mapInit();



$scope.getValueCheck = function(getStatus){

        // if($scope.intervalValue){
        //    clearInterval($scope.intervalValue);
        // }

        $scope.getValue = getStatus;

        if($scope.getValue == 'YES') {
        	if($scope.maps_no==0){
        		if($scope.polylineCheck=='YES'){
        			gArrows=Object.assign(gArrowMarker, gLiveArrows);
        		}
        		else{
        			gArrows=gLiveArrows;
        		}

        		for(key in gArrows) {
        			gArrows[key].setMap($scope.map);
        		}
        		gArrows = {};
        	} else if($scope.maps_no==1){
        		if($scope.polylineCheck=='YES'){
        			arrows=Object.assign(arrowMarker, liveArrows);
        		}
        		else{
        			arrows=arrowMarker;
        		}
        		for(key in arrows) {
							 //$scope.map_osm.removeLayer(arrowMarker[key]);
							 $scope.map_osm.addLayer(arrows[key]);
							}
							arrows = {};
						}
					} else if ($scope.getValue == 'NO') {
          //alert('hello');

          arrowclear();
                //map_osm.removeLayer($scope.polygonOsm);

            }
        }

        function arrowclear(){
        	$scope.SiteCheckbox.value1='NO';
        	if($scope.maps_no==0){
                         //gRemovedArrow=gArrowMarker;
                         gArrows=Object.assign(gArrowMarker, gLiveArrows);
                         for(key in gArrows) {
                         	gArrows[key].setMap(null);

                         }
                         gArrows = {};

                     }
                     else if($scope.maps_no==1){ 
		      	        //removedArrow=Object.assign(arrowMarker, liveArrows);
		      	        arrows=Object.assign(arrowMarker, liveArrows);
		      	        for(key in arrows) {
		      	        	$scope.map_osm.removeLayer(arrows[key]);

		      	        }
		      	        arrows = {};
		      	    }
		      	}
		      	function lineDraw(lat, lan) {

         // if(data.isOverSpeed=='N'){
	     // var strokeColorvar = '#00b3fd';
		 // }else{
		 // var strokeColorvar = '#ff0000';
		 // }
		 // var latlng1 = new google.maps.LatLng(lat, lan);
		 // console.log(' value -->'+ latlng1);

		 $scope.slatlong = new google.maps.LatLng(lat, lan);
		 np.push(new google.maps.LatLng(lat, lan));

		 $scope.polyline = new google.maps.Polyline({
		 	map: $scope.map,
		 	path: [$scope.elatlong, $scope.slatlong],
		 	strokeColor: '#00b3fd',
		 	strokeOpacity: 0.7,
		 	strokeWeight: 5,
		 	clickable: true
		 });
	  //$scope.polylines.setMap($scope.map);
	  $scope.elatlong = $scope.slatlong;
	}



	function timesInterval(){




		vamoservice.getDataCall($scope.url).then(function(data) {
			$scope.expectedFuelMileage=data.expectedFuelMileage;
			var fuelLitres = data.fuelLitres;
			var nTankSize   = data.nTankSize;
			sensor=data.noOfTank;
			console.log(data.noOfTank);
			$scope.fuelLitreArr = fuelLitres.split(":");
			$scope.tankSizeArr = nTankSize.split(":");
			if($scope.path.length==1){
				linesCount=0;
			}

		   		/*	if(data != '' && data){
		   				if(data.position == 'M')
		   					scope.histVal.unshift(data);
		   				else
		   					scope.histVal[0]=data;
		   			}*/
		   			
		   			var locs = data;
		   			if($scope.histVal[0].date == locs.date)
		   				return false; 
		   			var vehicType;
		   			var vehicIcon=[];

		   			vehicType=data.vehicleType;
		   			vehicIcon=vehiclesChange(vehicType); 

					// var myOptions = {
					// zoom: 13,
					// center: new google.maps.LatLng(locs.latitude, locs.longitude),
					// mapTypeId: google.maps.MapTypeId.ROADMAP
	                // };

	                $('#vehiid span').text(locs.shortName);
	                $('#toddist span span').text(locs.distanceCovered);
	                $('#vehstat span').text(locs.position);
	                $('#deviceVolt span').text(locs.deviceVolt);
					// total = parseInt(locs.speed);
					$('#vehdevtype span').text(locs.odoDistance);
					$('#mobno span').text(locs.overSpeedLimit);
					
					$('#graphsId #speed').text(locs.speed);
					$('#graphsId #fuel').text(locs.tankSize);
					tankSize 		 = parseInt(locs.tankSize);
					fuelLtr 		 = parseInt(locs.fuelLitre);
					total  			 = parseInt(locs.speed);

					$('#positiontime').text(vamoservice.statusTime(locs).tempcaption);
					$('#regno span').text(vamoservice.statusTime(locs).temptime);
					// scope.getLocation(locs.latitude, locs.longitude, function(count){
					// 	$('#lastseentrack').text(count); 
					// });

					if((data && data != '') && (data.address == null || data.address == undefined || data.address == ' ')){
						$scope.getLocation(locs.latitude, locs.longitude, function(count){
							$('#lastseentrack').text(count);
							data.address = count;
							$scope.addres = count;  

							if(data.position == 'M'){
								$scope.histVal.unshift(data);
							}
							else{
								$scope.histVal[0]=data;
							}

						});
					} else {
						$('#lastseentrack').text(data.address);
						$scope.addres = data.address;

						if(data.position == 'M'){
							$scope.histVal.unshift(data);
						}
						else{
							$scope.histVal[0]=data;
						}
					}

					$scope.path.push(new google.maps.LatLng(data.latitude, data.longitude));

					if($scope.path.length>1){
						var latLngBounds = new google.maps.LatLngBounds();
						latLngBounds.extend($scope.path[$scope.path.length-1]);
					}
					var labelAnchorpos = new google.maps.Point(-40, -30);
					var myLatlng = new google.maps.LatLng(data.latitude, data.longitude);
				  //$scope.map.setCenter(scope.marker.getPosition());

				  if($scope.vehiLabel=="Asset") {

				  	$scope.marker.setMap(null);

				  	var image = {
				  		url: vamoservice.assetImage(data),
				  		scaledSize: new google.maps.Size(30, 30),
				  		labelOrigin:  new google.maps.Point(30,30)
				  	};

				  	$scope.marker = new MarkerWithLabel({
				  		position: myLatlng, 
				  		map: $scope.map,
					// center: myLatlng,
					icon: image,
					  /* icon: 
				        {
				          path:vehicIcon[0],
				          scale:vehicIcon[1],
						  strokeWeight: 1,
				      //  fillColor: $scope.polylinearr[lineCount],
				          fillColor:'#6dd538',
				          fillOpacity: 1,
				          anchor:vehicIcon[2],
				          rotation: $scope.rotationd,				 
				      },*/
				      labelContent: data.shortName,
				      labelAnchor: labelAnchorpos,
				      labelClass: "labels",
				      labelInBackground: false
				  });


				  } else {

				  	if(($scope.path[linesCount].lat()+','+$scope.path[linesCount].lng())!=($scope.path[linesCount+1].lat()+','+$scope.path[linesCount+1].lng())){

				  		$scope.marker.setMap(null);
				  		$scope.rotationd=getBearing($scope.path[linesCount].lat(),$scope.path[linesCount].lng(),$scope.path[linesCount+1].lat(),$scope.path[linesCount+1].lng());

				  		$scope.marker = new MarkerWithLabel({
				  			position: myLatlng, 
				  			map: $scope.map,
					// center: myLatlng,
				    // icon: vamoservice.iconURL(data),
				    icon: 
				    {
				    	path:vehicIcon[0],
				    	scale:vehicIcon[1],
				    	strokeWeight: 1,
				      //  fillColor: $scope.polylinearr[lineCount],
				      fillColor:'#6dd538',
				      fillOpacity: 1,
				      anchor:vehicIcon[2],
				      rotation: $scope.rotationd,				 
				  },
				  labelContent: data.shortName,
				  labelAnchor: labelAnchorpos,
				  labelClass: "labels",
				  labelInBackground: false
				});
				  	}

				  }	  

				  $scope.map.setCenter(myLatlng);
					/*var contentString = '<div style="padding:5px; padding-top:10px; width:auto; max-height:170px; height:auto;">'
						+'<div style="width:200px; display:inline-block;"><b>Address</b> - <span>'+$scope.addres+'</span></div></br>'+'</br>'+'<a href="#" onclick="addPoi('+data.latitude+','+data.longitude+')">Save Site</a>';
						*/
						var contentString;
						if($scope.response.serial1!="temperature"){
							contentString = '<div style="padding:5px; padding-top:10px; width:auto; max-height:170px; height:auto;">'
							+'<div style="width:200px; display:inline-block;"><b>Address</b> - <span>'+$scope.addres+'</span></div>';
						}else{
							contentString = '<div style="padding:5px; padding-top:10px; width:auto; max-height:170px; height:auto;">'
							+'<div style="width:200px;overflow-wrap: break-word; display:inline-block;"><b >Address</b> - <span>'+$scope.addres+'</span><br><b >Temperature</b> - <span>'+$scope.response.temperature+' &#x2103;</span></div>'; 
						}
						var infowindow = new google.maps.InfoWindow({
							content: contentString
						});

						google.maps.event.addListener($scope.marker, "click", function(e) {
							infowindow.open($scope.map, $scope.marker);
						});

						$scope.endlatlong = new google.maps.LatLng(data.latitude, data.longitude);

						if(data.isOverSpeed=='N'){
							var strokeColorvar = '#00b3fd';
						}else{
							var strokeColorvar = '#ff0000';
						}
						$scope.polyline = new google.maps.Polyline({
							map: $scope.map,
							path: [$scope.startlatlong, $scope.endlatlong],
							strokeColor: strokeColorvar,
							strokeOpacity: 0.7,
							strokeWeight: 5
						});
			       // $scope.polylines.setMap($scope.map);

			       $scope.startlatlong = $scope.endlatlong;
			       google.maps.event.trigger(document.getElementById('maploc'), "resize");

			       if($scope.histVal.length > 200){
			       	$scope.histVal = $scope.histVal.slice(0, $scope.histVal.length - 50);
			       }
                //added for live track arrows
                if($scope.path.length>=2){
                	$scope.arrowlatlong = new google.maps.LatLng($scope.path[linesCount+1].lat(),$scope.path[linesCount+1].lng());
                	latLngdiff=calcCrow($scope.path[linesCount].lat(),$scope.path[linesCount].lng(),$scope.path[linesCount+1].lat(),$scope.path[linesCount+1].lng() );
                	distance=parseFloat(latLngdiff)+parseFloat(distance);
				      	//alert(distance.toFixed(2));
				      	if(distance>liveInterval){
				      		distance=0;
				      		var beachMarker = new google.maps.Marker({
				      			position: $scope.arrowlatlong,
									          icon: {scaledSize: new google.maps.Size(80, 80),/* url: image,
									          rotation:180*/ path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,scale: 2,rotation: $scope.rotationd/*, anchor: new google.maps.Point(38,40)*/}
									      });
		                    // gArrowMarker['live'+$scope.path.length]=beachMarker;
		                    gLiveArrows['live'+$scope.path.length]=beachMarker;
		                    if($scope.SiteCheckbox.value1=='YES'){
		                    	beachMarker.setMap($scope.map);
		                    }
		                }

		            }
                //end live track arrows
                linesCount++;	
            });

}

function lineDraw_Osm(data) {
          //console.log(data.length);

          for(var i=0;i<data.length;i++){
        		//console.log(i);
        		if(i!=data.length-1) {

        			var lat1=data[i];
        			var lat2=data[i+1];

        		} else {
        			var lat1=data[i];
        			var lat2=data[i];
        		}

        		var nVars     = [lat1,lat2];
        		var polylines = L.polyline(nVars, {color:'#00b3fd'}).addTo($scope.map_osm);
        	}
        }

        function mapInit() {
        	console.log('googMapInit......');
        	vamoservice.getDataCall($scope.url).then(function(data) {
        		$scope.expectedFuelMileage=data.expectedFuelMileage;
        		$scope.noOfTank = data.noOfTank;
        		getFuelTank(data);
        		$scope.response=data;
              	//console.log(data);
              	var locs     = data;
              	$scope.locss = data;

              	$('#vehiid span').text(locs.shortName);
              	$('#toddist span span').text(locs.distanceCovered);
				// total = parseInt(locs.speed);
				$('#deviceVolt span').text(locs.deviceVolt);
				$('#vehdevtype span').text(locs.odoDistance);
				$('#mobno span').text(locs.overSpeedLimit);
				
				$('#graphsId #speed').text(locs.speed);
				$('#graphsId #fuel').text(locs.tankSize);

				tankSize   = parseInt(locs.tankSize);
				fuelLtr    = parseInt(locs.fuelLitre);
				total  	   = parseInt(locs.speed);
				
				if((data && data != '') && (data.address == null || data.address == undefined || data.address == ' ')){
					$scope.getLocation(locs.latitude, locs.longitude, function(count){
						$('#lastseentrack').text(count);
						data.address = count;
						$scope.addres = count;  

						if(data.position == 'M'){
							$scope.histVal.unshift(data);
						}else{
							$scope.histVal[0]=data;
						}

					});
				} else {
					$('#lastseentrack').text(data.address);
					$scope.addres = data.address;

					if(data.position == 'M'){
						$scope.histVal.unshift(data);
					}else{
						$scope.histVal[0]=data;
					}
				} 
				
				$('#positiontime').text(vamoservice.statusTime(locs).tempcaption);
				$('#regno span').text(vamoservice.statusTime(locs).temptime);
				$scope.speedval.push(data.speed);


			//alert(	$scope.maps_no );

			if($scope.maps_no==0) { 

            	//alert('google map...');

            	var vehicType;
            	var vehicIcon=[];

            	vehicType=data.vehicleType;
            	vehicIcon=vehiclesChange(vehicType); 

            	var myOptions = {
            		zoom: 13,zoomControlOptions: { position: google.maps.ControlPosition.LEFT_TOP}, 
            		center: new google.maps.LatLng(locs.latitude, locs.longitude),
					mapTypeId: google.maps.MapTypeId.ROADMAP/*,
					*/
				};
            	//google map init
            	$scope.map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
            	$scope.map.setOptions({maxZoom:18});
            	//google map zoom start 
            	google.maps.event.addListener($scope.map,'zoom_changed',function(){
            		gArrows=Object.assign(gArrowMarker, gLiveArrows);
            		for(key in gArrows) {
            			gArrows[key].setMap(null);
            		}
            		gArrows = {};
            		gArrowMarker={};
            		gLiveArrows={};
            		if($scope.map.getZoom() > gzoom) {
                      //alert('You just zoomed in.');
                      if($scope.map.getZoom()==13){
                      	gDistanceInterval=2;
                      }
                      else{
                                // if($scope.map.getZoom()>=13){
                                //     gDistanceInterval=gDistanceInterval/2;
                                // }else{
                                	gDistanceInterval=gDistanceInterval/2;
                                //}
                            }
                        }else if($scope.map.getZoom() < gzoom) {
                      //alert('You just zoomed out.');
                      if($scope.map.getZoom()==13){
                      	gDistanceInterval=2;
                      }
                      else{
                                // if($scope.map.getZoom()>=13){
                                //     gDistanceInterval=gDistanceInterval*2;
                                // }else{
                                	gDistanceInterval=gDistanceInterval*2;
                                //}

                            }
                        }
                        gzoom = $scope.map.getZoom();
				  //added for live arrows
				  distance=0;
				  for (var i = 0; i < $scope.path.length; i++) {
				  	if(i>=2&&gzoom>=7){
				  		$scope.startlatlong = new google.maps.LatLng($scope.path[i-1].lat(), $scope.path[i-1].lng());
				  		var dx = $scope.path[i].lat() - $scope.path[i-1].lat();
				  		var dy = $scope.path[i].lng() - $scope.path[i-1].lng();
				  		rotationAngle = Math.atan2(dy, dx)* 180 / Math.PI;
				  		latLngdiff=calcCrow($scope.path[i-1].lat(), $scope.path[i-1].lng(),$scope.path[i].lat(),$scope.path[i].lng());
				  		distance=parseFloat(latLngdiff)+parseFloat(distance);
				      	//alert(distance.toFixed(2));
				      	if(distance>gDistanceInterval){
				      		distance=0;
				      		var beachMarker = new google.maps.Marker({
				      			position: $scope.startlatlong,
									          icon: {scaledSize: new google.maps.Size(80, 80),/* url: image,
									          rotation:180*/ path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,scale: 2,rotation: rotationAngle/*, anchor: new google.maps.Point(38,40)*/}
									      });
		                    //gArrowMarker['live'+i]=beachMarker;
		                    gLiveArrows['live'+$scope.path.length]=beachMarker;
		                    if($scope.SiteCheckbox.value1=='YES'){
		                    	beachMarker.setMap($scope.map);
		                    }
		                }

		            }
		        }
				  //end live arrow
				  distance=0;
				  for (var i = 0; i < gLatLngUrlData.length; i++) {
				  	if(i!=0){
				  		var dx = gLatLngUrlData[i].lat - gLatLngUrlData[i-1].lat;
				  		var dy = gLatLngUrlData[i].lng - gLatLngUrlData[i-1].lng;
				  		rotationAngle = Math.atan2(dy, dx)* 180 / Math.PI;
				  	}

				  	var markerLatlng = new google.maps.LatLng(gLatLngUrlData[i].lat,gLatLngUrlData[i].lng);
				  	if(i!=0&&gzoom>=7){
				  		latLngdiff=calcCrow(gLatLngUrlData[i-1].lat ,gLatLngUrlData[i-1].lng ,gLatLngUrlData[i].lat ,gLatLngUrlData[i].lng  );
				  		distance=parseFloat(latLngdiff)+parseFloat(distance);
							      	//alert(distance.toFixed(2));
							      	if(distance>gDistanceInterval){
							      		distance=0;
							      		var beachMarker = new google.maps.Marker({
							      			position: markerLatlng,
							      			map: $scope.map,
									          icon: {scaledSize: new google.maps.Size(80, 80),/* url: image,
									          rotation:180*/ path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,scale: 2,rotation: rotationAngle/*, anchor: new google.maps.Point(38,40)*/}
									      });
									      //beachMarker.setMap(null);

									      gArrowMarker[i]=beachMarker;
									      if($scope.SiteCheckbox.value1=='YES'){
									      	beachMarker.setMap($scope.map);
									      }
									      
									  }
									}

								}

							});
            	//google map zoom end

            	google.maps.event.addListener($scope.map, 'click', function(event) {
            		$scope.clickedLatlng = event.latLng.lat() +','+ event.latLng.lng();
            		$('#latinput').val($scope.clickedLatlng);
            	});

            	$scope.path.push(new google.maps.LatLng(data.latitude, data.longitude));

            	var labelAnchorpos  = new google.maps.Point(20, -30);
            	var myLatlng        = new google.maps.LatLng(data.latitude, data.longitude);

            	$scope.startlatlong = new google.maps.LatLng(data.latitude, data.longitude);
            	$scope.endlatlong   = new google.maps.LatLng(data.latitude, data.longitude);
            	prev_latitude=data.latitude;
            	prev_longitude=data.longitude;

            	if($scope.path.length>1){
            		var latLngBounds = new google.maps.LatLngBounds();
            		latLngBounds.extend($scope.path[$scope.path.length-1]);
            	}
            	(function latlanArr(){

            		var image = 'assets/imgs/arrow.png';
            		vamoservice.getDataCall($scope.latLngUrl).then(function(response) {
            			gLatLngUrlData=response;
            			rotationAngle=0;
            			for (var i = 0; i < response.length; i++) {
            				if(i!=0){
            					var dx = response[i].lat - response[i-1].lat;
            					var dy = response[i].lng - response[i-1].lng;
            					rotationAngle = Math.atan2(dy, dx)* 180 / Math.PI;
            				}

            				latLngArr.push(new google.maps.LatLng(response[i].lat,response[i].lng));
            				var markerLatlng = new google.maps.LatLng(response[i].lat,response[i].lng);
            				startmarkerLatlng = new google.maps.LatLng(response[i].lat,response[i].lng);
            				if(i!=0){
            					latLngdiff=calcCrow(response[i-1].lat ,response[i-1].lng ,response[i].lat ,response[i].lng  );
            					distance=parseFloat(latLngdiff)+parseFloat(distance);
							      	//alert(distance.toFixed(2));
							      	if(distance>gDistanceInterval){
							      		distance=0;
							      		var beachMarker = new google.maps.Marker({
							      			position: markerLatlng,
									          icon: {scaledSize: new google.maps.Size(80, 80),/* url: image,
									          rotation:180*/ path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,scale: 2,rotation: rotationAngle/*, anchor: new google.maps.Point(38,40)*/}
									      });
									      //beachMarker.setMap(null);

									      gArrowMarker[i]=beachMarker;
									      if($scope.SiteCheckbox.value1=='YES'){
									      	beachMarker.setMap($scope.map);
									      }
									      
									  }
									}
									if(i==0){

										startMarker = new google.maps.Marker({
											position: startmarkerLatlng,
											map: $scope.map,
											icon: {scaledSize: new google.maps.Size(68,70),url: 'assets/imgs/startflag.png', anchor: new google.maps.Point(43,68)}
										});

										if($scope.polylineCheck!='YES'){
											startMarker.setMap(null);
										}
									}

								}

								HistoryPath = new google.maps.Polyline({
									path: latLngArr,
									geodesic: true,
									strokeColor: '#4F2412',
									strokeOpacity: 1.0,
									strokeWeight: 3
								});
								if($scope.polylineCheck=='YES'){
									HistoryPath.setMap($scope.map);
								}



							});

            	}());		

            	(function latlan(){

            		vamoservice.getDataCall($scope.urlVeh).then(function(response) {
            			
            			$scope.noOfTank = response.noOfTank;
            			getFuelTank(response);
            			if(response.latLngOld != undefined){


            				for (var i = 0; i < response.latLngOld.length; i++) {
            					sp = response.latLngOld[i].split(',');
            					if(i==0){
            						$scope.elatlong = new google.maps.LatLng(sp[0], sp[1]);
            					}
            					lineDraw(sp[0], sp[1]);
            				}

            				npl=np.length; 
            				npls=np.length;

            				for(var i=0;i<npl;i++){

            					if(npls>2){

            						if((np[npls-2].lat()+','+np[npls-2].lng())!=(np[npls-1].lat()+','+np[npls-1].lng())){

            							nplen=npls-2;
            							break; 
            						}
            						else{
            							npls=npls-1;
            						}
            					}
            					else if(npls==2){

            						if((np[npls-2].lat()+','+np[npls-2].lng())!=(np[npls-1].lat()+','+np[npls-1].lng())){

            							nplen=npls-1;
            							break;
            						}
            						else{

            							npls=npls-1;

            						}

            					}
            					else{

            						if((np[npls-1].lat()+','+np[npls-1].lng())!=($scope.path[0].lat()+','+$scope.path[0].lng())){

            							nplen=npls-1;
            							break;

            						} else {

            							nplen=null;

            							if($scope.vehiLabel=="Asset") {

            								var image = {
            									url: vamoservice.assetImage(data),
            									scaledSize: new google.maps.Size(30, 30),
            									labelOrigin:  new google.maps.Point(30,30)
            								};

            								$scope.marker = new MarkerWithLabel({
            									position: myLatlng, 
            									map: $scope.map,
            									icon: image,
				                  /* icon: 
				                    {
				                      path:vehicIcon[0],
				                      scale:vehicIcon[1],
						              strokeWeight: 1,
				                    //fillColor: $scope.polylinearr[lineCount],
				                      fillColor:'#6dd538',
				                      fillOpacity: 1,
				                      anchor:vehicIcon[2],
				                    //rotation: rotationd,				 
				                },*/
				                labelContent: data.shortName,
				                labelAnchor: labelAnchorpos,
			                     //labelAnchor: vehicIcon[2],
			                     labelClass: "labels", 
			                     labelInBackground: false
			                 });


            							} else {

            								$scope.marker = new MarkerWithLabel({
            									position: myLatlng, 
            									map: $scope.map,
			                     //icon: vamoservice.iconURL(data),
			                     icon: 
			                     {
			                     	path:vehicIcon[0],
			                     	scale:vehicIcon[1],
			                     	strokeWeight: 1,
				                    //fillColor: $scope.polylinearr[lineCount],
				                    fillColor:'#6dd538',
				                    fillOpacity: 1,
				                    anchor:vehicIcon[2],
				                    //rotation: rotationd,				 
				                },
				                labelContent: data.shortName,
				                labelAnchor: labelAnchorpos,
			                     //labelAnchor: vehicIcon[2],
			                     labelClass: "labels", 
			                     labelInBackground: false
			                 });

            							}

            							google.maps.event.addListener($scope.marker, "click", function(e) {
            								infowindow.open($scope.map, $scope.marker);
            							});
      	             	 //}
      	             	}
      	             }

                 }//for ends... 


                 if(nplen!=null) {

                 	if($scope.vehiLabel=="Asset") {

                 		var image = {
                 			url: vamoservice.assetImage(data),
                 			scaledSize: new google.maps.Size(30, 30),
                 			labelOrigin:  new google.maps.Point(30,30)
                 		};

                 		$scope.marker = new MarkerWithLabel({
                 			position: myLatlng, 
                 			map: $scope.map,
                 			icon: image,
				  /* icon: 
				        {
				          path:vehicIcon[0],
				          scale:vehicIcon[1],
						  strokeWeight: 1,
				        //fillColor: $scope.polylinearr[lineCount],
				          fillColor:'#6dd538',
				          fillOpacity: 1,
				          anchor:vehicIcon[2],
				          rotation: rotationd,				 
				      },*/
				      labelContent: data.shortName,
				      labelAnchor: labelAnchorpos,
			     //labelAnchor: vehicIcon[2],
			     labelClass: "labels", 
			     labelInBackground: false
			 });


                 	} else {

                 		var rotationd=getBearing(np[nplen].lat(),np[nplen].lng(),$scope.path[0].lat(),$scope.path[0].lng());

                 		$scope.marker = new MarkerWithLabel({
                 			position: myLatlng, 
                 			map: $scope.map,
			     //icon: vamoservice.iconURL(data),
			     icon: 
			     {
			     	path:vehicIcon[0],
			     	scale:vehicIcon[1],
			     	strokeWeight: 1,
				        //fillColor: $scope.polylinearr[lineCount],
				        fillColor:'#6dd538',
				        fillOpacity: 1,
				        anchor:vehicIcon[2],
				        rotation: rotationd,				 
				    },
				    labelContent: data.shortName,
				    labelAnchor: labelAnchorpos,
			     //labelAnchor: vehicIcon[2],
			     labelClass: "labels", 
			     labelInBackground: false
			 });

                 	}

                 	google.maps.event.addListener($scope.marker, "click", function(e) {
                 		infowindow.open($scope.map, $scope.marker);
                 	});

                 	$scope.path.push(new google.maps.LatLng(np[nplen].lat(),np[nplen].lng()));
                 }
                 
             } else {

                 //console.log('latlanOld data not found.....');

                 if($scope.vehiLabel=="Asset") {

                 	var image = {
                 		url: vamoservice.assetImage(data),
                 		scaledSize: new google.maps.Size(30, 30),
                 		labelOrigin:  new google.maps.Point(30,30)
                 	};

                 	$scope.marker = new MarkerWithLabel({
                 		position: myLatlng, 
                 		map: $scope.map,
                 		icon: image,
				  /*icon: 
				        {
				          path:vehicIcon[0],
				          scale:vehicIcon[1],
						  strokeWeight: 1,
				      //  fillColor: $scope.polylinearr[lineCount],
				          fillColor:'#6dd538',
				          fillOpacity: 1,
				          anchor:vehicIcon[2],
				      //  rotation: rotationd,				 
				  },*/
				  labelContent: data.shortName,
				  labelAnchor: labelAnchorpos,
			     // labelAnchor: vehicIcon[2],
			     labelClass: "labels", 
			     labelInBackground: false
			 });

                 } else {

                 	$scope.marker = new MarkerWithLabel({
                 		position: myLatlng, 
                 		map: $scope.map,
			      //icon: vamoservice.iconURL(data),
			      icon: 
			      {
			      	path:vehicIcon[0],
			      	scale:vehicIcon[1],
			      	strokeWeight: 1,
				      //  fillColor: $scope.polylinearr[lineCount],
				      fillColor:'#6dd538',
				      fillOpacity: 1,
				      anchor:vehicIcon[2],
				      //  rotation: rotationd,				 
				  },
				  labelContent: data.shortName,
				  labelAnchor: labelAnchorpos,
			     // labelAnchor: vehicIcon[2],
			     labelClass: "labels", 
			     labelInBackground: false
			 });


                 }

                 google.maps.event.addListener($scope.marker, "click", function(e) {
                 	infowindow.open($scope.map, $scope.marker);
                 });

            } // if else ends...     

        }); // lanlan vamoservice ends...

    }()); // latlan func ends...


	  /*  var contentString = '<div style="padding:5px; padding-top:10px; width:auto; max-height:170px; height:auto;">'
						+'<div style="width:200px; display:inline-block;"><b >Address</b> - <span>'+$scope.addres+'</span></div></br>'+'<a href="#" onclick="addPoi('+data.latitude+','+data.longitude+')">Save Site</a>';
						*/  
						var contentString
						console.log($scope.response.serial1);
						if($scope.response.serial1!="temperature"){
							contentString = '<div style="padding:5px; padding-top:10px; width:auto; max-height:170px; height:auto;">'
							+'<div style="width:200px; display:inline-block;"><b>Address</b> - <span>'+$scope.addres+'</span></div>';
						}else{
							contentString = '<div style="padding:5px; padding-top:10px; width:auto; max-height:170px; height:auto;">'
							+'<div style="width:200px;overflow-wrap: break-word; display:inline-block;"><b >Address</b> - <span>'+$scope.addres+'</span><br><b >Temperature</b> - <span>'+$scope.response.temperature+' &#x2103;</span></div>'; 
						}
						var infowindow = new google.maps.InfoWindow({
							content: contentString
						});

		 /* google.maps.event.addListener(scope.marker, "click", function(e){
				infowindow.open(scope.map, scope.marker);
			}); */

     // Global section ...

     $(document).on('pageshow', '#maploc', function(e, data){       
     	google.maps.event.trigger(document.getElementById('	maploc'), "resize");
     });

// Global section ends...

$scope.timesIntervalss = setInterval(function(){ timesInterval() }, 10000);


} else if($scope.maps_no == 1){
     //alert($scope.polylineCheck);
     latLngArrOSM=[];
     console.log('osm map tracking.....');
     if(osmVisit=='no'){
     	osmVisit='yes';
     }
     else if(osmVisit=='yes'){
     	$scope.map_osm.remove();
     }
     // var mapLink  =  '<a href="https://osm.vamosys.com/nominatim/lf.html">OpenStreeetMap</a>'
     //      $scope.map_osm  =  new L.map('map_canvas2',{ center: new L.LatLng(12.993803, 80.193075),/* minZoom: 4,*/zoom: 13 });

     //    new L.tileLayer('https://osm.vamosys.com/osm_tiles/{z}/{x}/{y}.png', {
     //         attribution: '&copy; '+mapLink+' Contributors',
     //      // maxZoom: 18,
     //    }).addTo($scope.map_osm);


     //   $scope.map_osm.setView(new L.LatLng($scope.locss.latitude, $scope.locss.longitude), 13);

     var mapLink = '<a href="https://osm.vamosys.com/nominatim/lf.html">OpenStreeetMap</a>'
     $scope.map_osm = new L.map('map_canvas2',{ center:new L.LatLng($scope.locss.latitude, $scope.locss.longitude),/* minZoom: 4,*/zoom: 13 });

     new L.tileLayer(
     	'https://osm.vamosys.com/osm_tiles/{z}/{x}/{y}.png', {
     		attribution: '&copy; '+mapLink+' Contributors',
          // maxZoom: 18,
      }).addTo($scope.map_osm); 

     $scope.myLatLngOsm = new L.LatLng($scope.locss.latitude, $scope.locss.longitude);

     $scope.markerss = new L.marker().bindLabel($scope.locss.shortName, { noHide: true });
     $scope.infowin_osm = new L.popup({maxWidth:800});

     if($scope.vehiLabel=="Asset") {

     	var myIcon1 = L.icon({
     		iconUrl: vamoservice.assetImage($scope.locss),
     		iconSize: [30,30], 
     		iconAnchor:[20,40],
               popupAnchor:[-1,-40] //scaledSize: new google.maps.Size(25, 25)
           }); 

     } else {

     	if($scope.trvShow=='true'){

     		var myIcon1 = L.icon({
     			iconUrl:vamoservice.trvIcon($scope.locss),
     			iconSize: [40,40], 
     			iconAnchor: [20,40],
     			popupAnchor: [-1, -40],
     		});

     	} else {

     		var myIcon1 = L.icon({
     			iconUrl:vamoservice.iconURL($scope.locss),
     			iconSize: [40,40], 
     			iconAnchor: [20,40],
     			popupAnchor: [-1, -40],
     		});
     	}

     }

     /*   var conString_osm = '<div style="padding:5px; padding-top:10px; width:auto; max-height:170px; height:auto;">'
						+'<div style="width:200px;overflow-wrap: break-word; display:inline-block;"><b >Address</b> - <span>'+$scope.addres+'</span></div></br>'+'<a href="#" onclick="addPoi('+data.latitude+','+data.longitude+')">Save Site</a>';
						*/
						var conString_osm;
						if($scope.response.serial1!="temperature"){
							conString_osm = '<div style="padding:5px; padding-top:10px; width:auto; max-height:170px; height:auto;">'
							+'<div style="width:200px; display:inline-block;"><b>Address</b> - <span>'+$scope.addres+'</span></div>';
						}else{
							conString_osm = '<div style="padding:5px; padding-top:10px; width:auto; max-height:170px; height:auto;">'
							+'<div style="width:200px;overflow-wrap: break-word; display:inline-block;"><b >Address</b> - <span>'+$scope.addres+'</span><br><b >Temperature</b> - <span>'+$scope.response.temperature+' &#x2103;</span></div>'; 
						}

						$scope.infowin_osm.setContent(conString_osm);
						$scope.markerss.bindPopup($scope.infowin_osm);
						$scope.markerss.setIcon(myIcon1);

						$scope.markerss.addEventListener('click', function(e) {

							$scope.markerss.openPopup();
						});

						var prevZoom = $scope.map_osm.getZoom();
						$scope.map_osm.on('zoomend',function(e){
      //alert('hello');
      debugger;
      currZoom = $scope.map_osm.getZoom();
      var diff = prevZoom - currZoom;
      markersLen=arrowMarker.length;
      arrows=Object.assign(arrowMarker, liveArrows);
      for(key in arrows) {
      	$scope.map_osm.removeLayer(arrows[key]);
      }
      arrows = {};
      arrowMarker={};
      liveArrows={};

      if(diff > 0){
        //alert('zoomed out'+diff);
        if(currZoom==13){
        	distanceInterval=2;
        }
        else{
            // if(currZoom>=13){
            //     distanceInterval=distanceInterval*2;
            // }else{
            	distanceInterval=distanceInterval*2;
            //}
        }
        
    } else if(diff < 0) {
        //alert('zoomed in'+diff);
        if(currZoom==13){
        	distanceInterval=2;
        }
        else{
              //  if(currZoom>=13){
              //     distanceInterval=distanceInterval/2;
              // }else{
              	distanceInterval=distanceInterval/2;
              //}
          }
      }
      prevZoom = currZoom;
        //before zoom 
        var myIconOSM = L.icon({
        	iconUrl: 'assets/imgs/arrow.png',
        	iconSize: [15,15], 
        	iconAnchor:[5,10],
        	popupAnchor: [0, -65],
        	className: 'u-turn-icon'
					          /*rotateWithView: true,
					          rotation: 180*/

					      });
        for (var i = 0; i < $scope.osmPaths.length; i++) {
        	if(i>=2&&currZoom>=7){

        		$scope.markerNext     = new L.marker($scope.osmPaths[i-1],{icon: myIconOSM,rotationAngle: deg-45});
        		latLngdiff=calcCrow($scope.osmPaths[i-1].lat, $scope.osmPaths[i-1].lng,$scope.osmPaths[i].lat,$scope.osmPaths[i].lng);
        		distance=parseFloat(latLngdiff)+parseFloat(distance);
							      	//alert(distance-distanceInterval);
							      	if(distance>distanceInterval){
							        //arrowMarker['live'+i]=$scope.markerNext;
							        liveArrows['live'+i]=$scope.markerNext;
								    //$scope.map_osm.panTo(nVarss[0]);
								    distance=0;
								    if($scope.SiteCheckbox.value1=='YES'){
								    	$scope.map_osm.addLayer($scope.markerNext);
								    }

								}
							}
						}
        //after zoom 
        for (var i = 0; i < latLngUrlData.length; i++) {


        	if(i!=0){
        		var dx = latLngUrlData[i].lat - latLngUrlData[i-1].lat;
        		var dy = latLngUrlData[i].lng - latLngUrlData[i-1].lng;
        		deg = Math.atan2(dy, dx)* 180 / Math.PI;
        	}

        	var myIconStart = L.icon({
        		iconUrl: 'assets/imgs/arrow.png',
        		iconSize: [15,15], 
        		iconAnchor:[5,10],
        		popupAnchor: [0, -65],
        		className: 'u-turn-icon'
				          /*rotateWithView: true,
				          rotation: 180*/

				      });
        	if(i!=0&&currZoom>=7){
        		var startFlagVal = new L.LatLng(latLngUrlData[i-1].lat,latLngUrlData[i-1].lng);
        		$scope.markerStart     = new L.marker(startFlagVal,{icon: myIconStart,rotationAngle: deg-45});
        		latLngdiff=calcCrow(latLngUrlData[i-1].lat,latLngUrlData[i-1].lng,latLngUrlData[i].lat,latLngUrlData[i].lng);
        		distance=parseFloat(latLngdiff)+parseFloat(distance);
				      	//alert(distance.toFixed(2));
				      	if(distance>distanceInterval){
					      		//alert(distance);
					        //alert(distanceInterval);
					        distance=0;
					      	// $scope.markerStart     = new L.marker(startFlagVal,{icon: myIconStart,rotationAngle: deg-45});
					      	arrowMarker[i]=$scope.markerStart;
					      	if($scope.SiteCheckbox.value1=='YES'){
					      		$scope.map_osm.addLayer($scope.markerStart);
					      	}
					      }

					  }
					}


				});

						(function latlanArrOSM(){


							vamoservice.getDataCall($scope.latLngUrl).then(function(response) {
								latLngUrlData=response;
								for (var i = 0; i < response.length; i++) {

									latLngArrOSM.push( new L.LatLng(response[i].lat,response[i].lng) );
									var nextFlagVal = new L.LatLng(response[i].lat,response[i].lng);
									startFlagVal = new L.LatLng(response[0].lat,response[0].lng);
									if(i!=0){
										var dx = response[i].lat - response[i-1].lat;
										var dy = response[i].lng - response[i-1].lng;
										deg = Math.atan2(dy, dx)* 180 / Math.PI;
									}


									if(i==0){
										myIconStart = L.icon({
											iconUrl: 'assets/imgs/startflag.png',
											iconSize: [68,70], 
											iconAnchor:[43,68],
											popupAnchor: [0, -65],
											className: 'u-turn-icon'
										});
										$scope.markerStart     = new L.marker(startFlagVal,{icon: myIconStart});
										if($scope.polylineCheck=='YES'){
											$scope.markerStart.addTo($scope.map_osm);
										}

									}
									var myIconNext = L.icon({
										iconUrl: 'assets/imgs/arrow.png',
										iconSize: [15,15], 
										iconAnchor:[5,10],
										popupAnchor: [0, -65],
										className: 'u-turn-icon'
				          /*rotateWithView: true,
				          rotation: 180*/

				      });


									if(i!=0){
										var nextMarkerVal = new L.LatLng(response[i-1].lat,response[i-1].lng);
										$scope.markerStart     = new L.marker(nextMarkerVal,{icon: myIconNext,rotationAngle: deg-45});
										latLngdiff=calcCrow(response[i-1].lat,response[i-1].lng,response[i].lat,response[i].lng);
										distance=parseFloat(latLngdiff)+parseFloat(distance);
										if(distance>distanceInterval){
											distance=0;
											arrowMarker[i]=$scope.markerStart;
											if($scope.SiteCheckbox.value1=='YES'){
												$scope.map_osm.addLayer($scope.markerStart);
											}
										}
				      	//$scope.map_osm.removeLayer($scope.markerStart);
				      }



				  }

				  if($scope.polylineCheck=='YES'){
				  	polylinesOSM=L.polyline(latLngArrOSM, {color: '#4F2412'}).setStyle({weight: 5}).addTo($scope.map_osm);
				  }

				});

						}());



						(function latlan_osm(){

							vamoservice.getDataCall($scope.urlVeh).then(function(response) {
								if(response.latLngOld != undefined){

									for (var i = 0; i < response.latLngOld.length; i++) {

										var sp_osm = response.latLngOld[i].split(',');
										$scope.newArr.push( new L.LatLng(sp_osm[0], sp_osm[1]) );
									}

									lineDraw_Osm($scope.newArr);

									var setLatlng = $scope.newArr[$scope.newArr.length-1];

									$scope.markerss.setLatLng(setLatlng);
									$scope.markerss.addTo($scope.map_osm);
									$scope.map_osm.setView(setLatlng); 
								} else {

									$scope.markerss.setLatLng($scope.myLatLngOsm);
									$scope.markerss.addTo($scope.map_osm);
									$scope.map_osm.setView($scope.myLatLngOsm); 

								}

            }); // lanlan vamoservice ends...

    }()); // latlan func ends...

						$scope.osmTimeInterval = setInterval(function(){ osmIntervals() }, 10000);

					}

 }); // first vamoservice ends..... 

}

function osmIntervals(){

	console.log('oosm...');

	vamoservice.getDataCall($scope.url).then(function(data) {
		$scope.response=data;

		   		 /* if(data != '' && data){
		   				if(data.position == 'M')
		   					scope.histVal.unshift(data);
		   				else
		   					scope.histVal[0]=data;
		   			} */
		   			
		   			var locs = data;
		   			if($scope.histVal[0].date == locs.date)
		   				return false; 
		   			$('#vehiid span').text(locs.shortName);
		   			$('#toddist span span').text(locs.distanceCovered);
		   			$('#vehstat span').text(locs.position);
		   			$('#deviceVolt span').text(locs.deviceVolt);
		   			$('#vehdevtype span').text(locs.odoDistance);
		   			$('#mobno span').text(locs.overSpeedLimit);
		   			$('#graphsId #speed').text(locs.speed);
		   			$('#graphsId #fuel').text(locs.tankSize);

		   			tankSize  =  parseInt(locs.tankSize);
		   			fuelLtr   =  parseInt(locs.fuelLitre);
		   			total  	  =  parseInt(locs.speed);

		   			$('#positiontime').text(vamoservice.statusTime(locs).tempcaption);
		   			$('#regno span').text(vamoservice.statusTime(locs).temptime);

				 // $scope.getLocation(locs.latitude, locs.longitude, function(count){
					// $('#lastseentrack').text(count); 
				 // });

				 if((data && data != '') && (data.address == null || data.address == undefined || data.address == ' ')){
				 	$scope.getLocation(locs.latitude, locs.longitude, function(count){
				 		$('#lastseentrack').text(count);
				 		data.address  = count;
				 		$scope.addres = count;  

				 		if(data.position == 'M') {
				 			$scope.histVal.unshift(data);
				 		} else {
				 			$scope.histVal[0]=data;
				 		}

				 	});

				 } else {

				 	$('#lastseentrack').text(data.address);
				 	$scope.addres = data.address;

				 	if(data.position == 'M') {
				 		$scope.histVal.unshift(data);
				 	} else {
				 		$scope.histVal[0]=data;
				 	}
				 }

                  //  console.log('osm intervals.....');

                  if($scope.polyInit==0){
                  	if($scope.newArr.length>0){
                  		$scope.osmPaths.push($scope.newArr[$scope.newArr.length-1]);
                  	}
                  	$scope.polyInit++;
                  }  

                  $scope.osmPaths.push(new L.LatLng(data.latitude,data.longitude));
                  var markerLatLng  = new L.LatLng(data.latitude,data.longitude);


                  if( markerInit==0 ){

                  	if( $scope.vehiLabel=="Asset" ) {

                  		var myIcons = L.icon({
                  			iconUrl: vamoservice.assetImage(data),
                  			iconSize: [30,30], 
                  			iconAnchor:[20,40],
				               popupAnchor:[-1,-40] //scaledSize: new google.maps.Size(25, 25)
				           }); 

                  	} else {

                  		if( $scope.trvShow == 'true' ) {

                  			var myIcons = L.icon({
                  				iconUrl:vamoservice.trvIcon(data),
                  				iconSize: [40,40], 
                  				iconAnchor: [20,40],
                  				popupAnchor: [-1, -40],
                  			});


                  		} else {

                  			var myIcons = L.icon({
                  				iconUrl:vamoservice.iconURL(data),
                  				iconSize: [40,40], 
                  				iconAnchor: [20,40],
                  				popupAnchor: [-1, -40],
                  			});

                  		}
                  	}

                  	$scope.markerss.setIcon(myIcons);
                  	$scope.markerss.setLatLng(markerLatLng);
                  	$scope.markerss.addTo($scope.map_osm);

                  	markerInit++;
                  }

                  if($scope.osmPaths.length>1){

                  	if( $scope.osmPaths[lineCountOsm].lat!=$scope.osmPaths[lineCountOsm+1].lat && $scope.osmPaths[lineCountOsm].lng!=$scope.osmPaths[lineCountOsm+1].lng ){

                  		if($scope.vehiLabel=="Asset") { 

                  			var myIcons = L.icon({
                  				iconUrl: vamoservice.assetImage(data),
                  				iconSize: [30,30], 
                  				iconAnchor:[20,40],
				    popupAnchor:[-1,-40] //scaledSize: new google.maps.Size(25, 25)
				}); 

                  		} else {

                  			if( $scope.trvShow == 'true' ) {

                  				var myIcons = L.icon({
                  					iconUrl:vamoservice.trvIcon(data),
                  					iconSize: [40,40], 
                  					iconAnchor: [20,40],
                  					popupAnchor: [-1, -40],
                  				});

                  			} else {

                  				var myIcons = L.icon({
                  					iconUrl:vamoservice.iconURL(data),
                  					iconSize: [40,40], 
                  					iconAnchor: [20,40],
                  					popupAnchor: [-1, -40],
                  				});

                  			}
                  		}

          /*  var conString_osm = '<div style="padding:5px; padding-top:10px; width:auto; max-height:170px; height:auto;">'
						+'<div style="width:200px;overflow-wrap: break-word; display:inline-block;"><b >Address</b> - <span>'+$scope.addres+'</span></div></br>'+'<a href="#" onclick="addPoi('+data.latitude+','+data.longitude+')">Save Site</a>';
						*/
						var conString_osm;
						if($scope.response.serial1!="temperature"){
							conString_osm = '<div style="padding:5px; padding-top:10px; width:auto; max-height:170px; height:auto;">'
							+'<div style="width:200px; display:inline-block;"><b>Address</b> - <span>'+$scope.addres+'</span></div>';
						}else{
							conString_osm = '<div style="padding:5px; padding-top:10px; width:auto; max-height:170px; height:auto;">'
							+'<div style="width:200px;overflow-wrap: break-word; display:inline-block;"><b >Address</b> - <span>'+$scope.addres+'</span><br><b >Temperature</b> - <span>'+$scope.response.temperature+' &#x2103;</span></div>'; 
						}

						$scope.infowin_osm.setContent(conString_osm);
						$scope.markerss.bindPopup($scope.infowin_osm);

						$scope.markerss.setIcon(myIcons);
						$scope.markerss.setLatLng(markerLatLng);


						var nVarss=[$scope.osmPaths[lineCountOsm],$scope.osmPaths[lineCountOsm+1]];


						var myIconOSM = L.icon({
							iconUrl: 'assets/imgs/arrow.png',
							iconSize: [15,15], 
							iconAnchor:[5,10],
							popupAnchor: [0, -65],
							className: 'u-turn-icon'
				          /*rotateWithView: true,
				          rotation: 180*/

				      });


						$scope.markerNext     = new L.marker(nVarss[0],{icon: myIconOSM,rotationAngle: deg});

						var polylines  = L.polyline(nVarss, {color:'#00b3fd'}).addTo($scope.map_osm);


						$scope.map_osm.panTo(markerLatLng);
        //added for live arrows
        var dx = nVarss[nVarss.length-1].lat - nVarss[nVarss.length-2].lat;
        var dy = nVarss[nVarss.length-1].lng - nVarss[nVarss.length-2].lng;
        deg = Math.atan2(dy, dx)* 180 / Math.PI;
        if($scope.osmPaths.length>=2){
        	$scope.markerNext     = new L.marker(nVarss[nVarss.length-2],{icon: myIconOSM,rotationAngle: deg-45});
        	latLngdiff=calcCrow(nVarss[nVarss.length-2].lat, nVarss[nVarss.length-2].lng,nVarss[nVarss.length-1].lat,nVarss[nVarss.length-1].lng);
        	distance=parseFloat(latLngdiff)+parseFloat(distance);
				      	//alert(distance.toFixed(2));
				      	if(distance>liveInterval){
				        //arrowMarker['live'+$scope.osmPaths.length]=$scope.markerNext;
				        liveArrows['live'+$scope.osmPaths.length]=$scope.markerNext;
					    //$scope.map_osm.panTo(nVarss[0]);
					    distance=0;
				       //$scope.markerNext     = new L.marker(nVarss[0],{icon: myIconOSM,rotationAngle: deg-45});
				       if($scope.SiteCheckbox.value1=='YES'){
				       	$scope.markerNext.addTo($scope.map_osm);
				       }
				      // $scope.map_osm.addLayer($scope.markerNext);
				  }
				}
        //end live arrow
    }

    lineCountOsm++;

}

});

}



function calcLatLongForDrawShapes(longitude, lat, distance, bearing) {
	var EARTH_RADIUS_EQUATOR  =  6378140.0;
	var RADIAN                =  180 / Math.PI;

	var b    =  bearing / RADIAN;
	var lon  =  longitude / RADIAN;
	var lat  =  lat / RADIAN;
	var f    =  1/298.257;
	var e    =  0.08181922;

	var R       =  EARTH_RADIUS_EQUATOR * (1 - e * e) / Math.pow( (1 - e*e * Math.pow(Math.sin(lat),2)), 1.5);  
	var psi     =  distance/R;
	var phi     =  Math.PI/2 - lat;
	var arccos  =  Math.cos(psi) * Math.cos(phi) + Math.sin(psi) * Math.sin(phi) * Math.cos(b);
	var latA    =  (Math.PI/2 - Math.acos(arccos)) * RADIAN;

	var arcsin  =  Math.sin(b) * Math.sin(psi) / Math.sin(phi);
	var longA   =  (lon - Math.asin(arcsin)) * RADIAN;

	return latA+':'+longA;
};



/* Get Org Ids */

$scope.$watch("orgUrl", function (val) {

	$http.get($scope.orgUrl).success(function(response) {
		console.log(response);
		$scope.orgList = [];
		var orgId = response.orgIds;
		$scope.organIds = response.orgIds;

		for( var i=0; i<orgId.length; i++ ) {
			console.log(orgId[i]);
			$scope.orgList.push( { 'orgName' : orgId[i] } );
		}
	});

}); 

//create save site
$scope.markPoi   =   function(textValue, latlanList) {

	alert('markpoi...'+textValue);


	$scope.toast    = '';
   // if(checkXssProtection(textValue) == true)
   // try
   // {

   	var URL_ROOT    = "AddSiteController/";    /* Your website root URL */
   	var text        = textValue;
   	var drop        = 'Home Site';
   	var org         = $scope.organIds[0];

   	alert('mark poi..');

      // post request
      if(text && drop && latlanList.length>=3 && org)
      {
      	$.ajax({
      		async: false,
      		method: 'POST', 
      		'url' : URL_ROOT+'store',
      		data: {'_token': $('meta[name=csrf-token]').attr('content'), 'siteName': text, 'siteType': drop, 'org':org, 'latLng': latlanList},
      		success: function (response) {

      			alert(response.length);

      			if(response!='') {
      				success();

      			} else if(response.length==0){

      				alert('else...');

      				fails();


      				stopLoading();

      			}

      			stopLoading();
      		}
      	}).fail(function() {
      		console.log("fail");
      		stopLoading();
      	});

      } else {

      	$('#notifyF span').text(translate("Enter all the field / Mark the Site"));
      	$('#notifyMsg').show();
      	$("#notifyF").show(500);
      	$("#notifyS").hide(); 
      	timeOutVar = setTimeout(setsTimeOuts, 2000);
      	stopLoading();
      }

 /*} catch (err)    {

               $('#notifyF span').text("Enter all the field / Mark the Site ");
               $('#notifyMsg').show();
               $("#notifyF").show(500);
               $("#notifyS").hide(); 
               timeOutVar = setTimeout(setsTimeOuts, 2000);
      stopLoading();
  }*/
  stopLoading();

}

  //split methods
  $scope.split_fcode = function(fcode){
  	var str = $scope.fcode[0].group;
  	var strFine = str.substring(str.lastIndexOf(':'));
  	while(strFine.charAt(0)===':')
  		strFine = strFine.substr(1);
  	return strFine;
  }





  $scope.submitPoi  = function(poiName){


  	alert(translate('submit')+'...');

  	var width       = 1000;
  	var latlngList  = [];

  	if($scope.maps_no==0){

  		if($scope.map.getZoom()>5 && $scope.map.getZoom()<=8){
  			width = 1000;
  		}
  		else if($scope.map.getZoom()>8 && $scope.map.getZoom()<=12){
  			width = 100;
  		}
  		else if($scope.map.getZoom()>12 && $scope.map.getZoom()<=15){
  			width = 10;
  		}
  		else if($scope.map.getZoom()>15){
  			width = 1;
  		}

  	} else if($scope.maps_no==1){

  		var zoomValue = $scope.map_osm.getZoom();
      //console.log(zoomValue);

      if(zoomValue>5 && zoomValue<=8){
      	width = 1000;
      }
      else if(zoomValue>8 && zoomValue<=12){
      	width = 100;
      }
      else if(zoomValue>12 && zoomValue<=15){
      	width = 10;
      }
      else if(zoomValue>15){
      	width = 1;
      }
  }

  var radius = (Math.sqrt (2 * (width * width))) / 2;

  latlngList[0] = calcLatLongForDrawShapes(poiLng, poiLat, radius, 45)
  latlngList[1] = calcLatLongForDrawShapes(poiLng, poiLat, radius, -45)
  latlngList[2] = calcLatLongForDrawShapes(poiLng, poiLat, radius, -135)
  latlngList[3] = calcLatLongForDrawShapes(poiLng, poiLat, radius, 45)

    //console.log(latlngList);
    $scope.markPoi(poiName, latlngList);

    modalss.style.display = "none";

} 



$scope.mapChanges=function(val){

	$scope.maps_no = val;

	if($scope.maps_no == 0){
        	//console.log('google...');
        	if($scope.mapInitGoog  !=  0){
        		$scope.hideHistoryDetail($scope.polylineCheck);
        		clearInterval($scope.osmTimeInterval);
        		$scope.timesIntervalss = setInterval(function(){ timesInterval() }, 10000);
        	}

        	if($scope.mapInitGoog  ==  0){
        		clearInterval($scope.osmTimeInterval);
        		mapInit();
        		$scope.mapInitGoog++; 
        	}      

        	document.getElementById("map_canvas2").style.display="none"; 
        	document.getElementById("map_canvas").style.display="block"; 

        } else if($scope.maps_no == 1) {

         /* if($scope.map_osm != null){
                 $scope.map_osm.remove();
             } */


             if($scope.mapInitOsm!=0){
             	$scope.hideHistoryDetail($scope.polylineCheck);
             	clearInterval($scope.timesIntervalss);
             	$scope.osmTimeInterval = setInterval(function(){ osmIntervals() }, 10000);
             }

             if($scope.mapInitOsm==0){
             	clearInterval($scope.timesIntervalss);
             	mapInit();
             	$scope.mapInitOsm++; 
             }

             document.getElementById("map_canvas").style.display="none"; 
             document.getElementById("map_canvas2").style.display="block"; 

         }
     }


     var markerSearch  = new google.maps.Marker({});
     var input_value   =  document.getElementById('pac-inputs');
     var sbox          =  new google.maps.places.SearchBox(input_value);
  // search box function
  sbox.addListener('places_changed', function() {

  	if($scope.timesIntervalss){
  		window.clearInterval($scope.timesIntervalss);	
  	}  

  	if($scope.osmTimeInterval){
  		window.clearInterval($scope.osmTimeInterval);	
  	} 

  	markerSearch.setMap(null);
  	var places = sbox.getPlaces();
  	markerSearch = new google.maps.Marker({
  		position: new google.maps.LatLng(places[0].geometry.location.lat(), places[0].geometry.location.lng()),
  		animation: google.maps.Animation.BOUNCE,
  		map: $scope.map,
  	});
  	console.log(' lat lan  '+places[0].geometry.location.lat(), places[0].geometry.location.lng())
  	$scope.map.setCenter(new google.maps.LatLng(places[0].geometry.location.lat(), places[0].geometry.location.lng()));
  	$scope.map.setZoom(13);
  });


  function saveAddressFunc(val, lat, lan){    
    //console.log(val);
    var saveAddUrl = GLOBAL.DOMAIN_NAME+'/saveAddress?address='+encodeURIComponent(val)+'&lattitude='+lat+'&longitude='+lan+'&status=web';
    //console.log(saveAddUrl);

    $http({
    	method: 'GET',
    	url: saveAddUrl
    }).then(function successCallback(response) {
    	if(response.status==200){
    		console.log("Save address successfully!..");
    	}
    }, function errorCallback(response) {
    	console.log(response.status);
    });

}


$scope.getLocation = function(lat, lon, callback){
	geocoder   = new google.maps.Geocoder();
	var latlng = new google.maps.LatLng(lat,lon);
	geocoder.geocode({'latLng': latlng}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			if(results[0]) {
				var newVals = vamoservice.googleAddress(results[0]);   
				saveAddressFunc(newVals, lat, lon);
			} 

			if (results[1]) {
				if(typeof callback === "function") callback(results[1].formatted_address)
			} else {
				//alert('No results found');
			}
		} 
	});
};


vamoservice.getDataCall($scope.url).then(function(data) {

	$scope.locations = data;
	/*	var locs=data;
	        $scope.getLocation(locs.latitude, locs.longitude, function(count){
				data.address = count;
			 // $scope.addres = count;  
 
                    if(data.position == 'M'){
		   			    $scope.histVal.push(data);
                    }
		   		    else{
		   			    $scope.histVal[0]=data;
		   		    }
		   		}); */

	//	$scope.histVal.push(data);

	var url = GLOBAL.DOMAIN_NAME+'/getGeoFenceView?'+res[1];
	$scope.createGeofence(url);

	$scope.vehiclFuel=graphChange($scope.locations.fuel);

				//speed size
				if($scope.noOfTank==1){
	               //alert('one')
	               speedSize='100%';speedCenter='100%';startangle=-90;endangle=90;
	           }
	           else if($scope.noOfTank==2){
	                //alert('two')
	                speedSize='68%';speedCenter='78%';startangle=-150;endangle=150;
	            }else {
	            	speedSize='60%';speedCenter='75%';startangle=-150;endangle=150;
	            }
                  //for fuel 
                  if($scope.vehiclFuel==true){
                  	if($scope.noOfTank==1){
                  		document.getElementById("graphsId").style.width = "360px";
                  	}
                  	else {
                  		if($scope.noOfTank==2){
                  			document.getElementById("graphsId").style.width = "550px";
                  		}else if($scope.noOfTank==3){
                  			document.getElementById("graphsId").style.width = "630px";
                  		}else if($scope.noOfTank==4){
                  			document.getElementById("graphsId").style.width = "785px";
                  		}else if($scope.noOfTank==5){
                  			document.getElementById("graphsId").style.width = "940px";
                  		}
                  	}
                  } else {
                  	document.getElementById("graphsId").style.width = "190px";
                  }
                  $('#graphsId').show();
              });


$scope.addMarker= function(pos){
	var myLatlng = new google.maps.LatLng(pos.lat,pos.lng);
	var marker = new google.maps.Marker({
		position: myLatlng, 
		map: $scope.map
	});
}

$scope.timems = function(t){

	var cd = 24 * 60 * 60 * 1000,
	ch = 60 * 60 * 1000,
	d = Math.floor(t / cd),
	h = Math.floor( (t - d * cd) / ch),
	m = Math.round( (t - d * cd - h * ch) / 60000),
	pad = function(n){ return n < 10 ? '0' + n : n; };
	if( m === 60 ){
		h++;
		m = 0;
	}
	if( h === 24 ){
		d++;
		h = 0;
	}
	return [d+'d', pad(h)+'h', pad(m)+'m'].join(':');

}


$scope.enterkeypress = function(){
	if(checkXssProtection(document.getElementById('poival').value) == true){
		var poiUrl = GLOBAL.DOMAIN_NAME+'/setPOIName?vehicleId='+$scope.vehicleno+'&poiName='+document.getElementById('poival').value;
		if(document.getElementById('poival').value=='' || $scope.vehicleno==''){}else{
			vamoservice.getDataCall(poiUrl).then(function(data) {
				document.getElementById('poival').value='';
			});
		}
	}
}

$scope.trafficLayer = new google.maps.TrafficLayer();
$scope.checkVal=false;
$scope.clickflagVal =0;

$scope.checkme = function(val){
	if($scope.checkVal==false){
		document.getElementById(val).style.backgroundColor = "yellow"
		$scope.trafficLayer.setMap($scope.map);
		$scope.checkVal = true;
	}else{
		document.getElementById(val).style.backgroundColor = "#FFFFFF"
		$scope.trafficLayer.setMap(null);
		$scope.checkVal = false;
	}
}

$scope.createGeofence=function(url){

	if($scope.cityCirclecheck==false){
		$scope.cityCirclecheck=true;
	}
	if($scope.cityCirclecheck==true){
		for(var i=0; i<$scope.cityCircle.length; i++){
			$scope.cityCircle[i].setMap(null);
		}
	}
	vamoservice.getDataCall(url).then(function(data) {
		$scope.geoloc = data;
		if (typeof(data.geoFence) !== 'undefined' && data.geoFence.length) {	
				//alert(data.geoFence[0].proximityLevel);
				for(var i=0; i<data.geoFence.length; i++){
					if(data.geoFence[i]!=null){
						var populationOptions = {
							strokeColor: '#FF0000',
							strokeOpacity: 0.8,
							strokeWeight: 2,
							fillColor: '#FF0000',
							fillOpacity: 0.02,
							map: $scope.map,
							center: new google.maps.LatLng(data.geoFence[i].latitude,data.geoFence[i].longitude),
							radius: parseInt(data.geoFence[i].proximityLevel)
						};
						$scope.cityCircle[i]  =  new google.maps.Circle(populationOptions);
						var centerPosition    =  new google.maps.LatLng(data.geoFence[i].latitude, data.geoFence[i].longitude);
						var labelText         =  data.geoFence[i].poiName;
						var image             =  'assets/imgs/busgeo.png';

						var beachMarker = new google.maps.Marker({
							position: centerPosition,
							map: $scope.map,
							icon: image
						});

						var myOptions = {
							content: labelText,
							boxStyle: {textAlign: "center", fontSize: "8pt", fontColor: "#0031c4", width: "100px"
						},
						disableAutoPan: true,
						pixelOffset: new google.maps.Size(-50, 0),
						position: centerPosition,
						closeBoxURL: "",
						isHidden: false,
						pane: "mapPane",
						enableEventPropagation: true
					};

					var labelinfo = new InfoBox(myOptions);
					labelinfo.open($scope.map);
					labelinfo.setPosition($scope.cityCircle[i].getCenter());
				}
			}
		}
	})
}      


	//This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
	function calcCrow(lat1, lon1, lat2, lon2) 
	{
      var R = 6371; // km
      var dLat = toRad(lat2-lat1);
      var dLon = toRad(lon2-lon1);
      var lat1 = toRad(lat1);
      var lat2 = toRad(lat2);

      var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
      var d = R * c;
      return d;
  }

    // Converts numeric degrees to radians
    function toRad(Value) 
    {
    	return Value * Math.PI / 180;
    }   


    function getFuelTank(data){
    	if($scope.noOfTank>1){
    		var fuelLitres = data.fuelLitres;
    		var nTankSize   = data.nTankSize;
    		sensor=data.noOfTank;
    		console.log(data.noOfTank);
    		$scope.fuelLitreArr = fuelLitres.split(":");
    		$scope.tankSizeArr = nTankSize.split(":");
    		fuelGraph();
    	}
    }

    function fuelGraph(){
	  //START
	  var widthValue=180;
	  var center2 ='90%';
	  if($scope.noOfTank>2){
	  	widthValue = 150;
	  	center2 = '80%';
	  }
	  var sensorGaugeOptions = {
	  	chart: {
	  		type: 'solidgauge',
	  		height: 100 ,
	  		width : widthValue,
	  	},
	  	credits: { enabled: false },
	  	title: null,

	  	pane: {
	  		center: ['50%',center2],
	  		size: widthValue+'%',
	  		startAngle: -90,
	  		endAngle: 90,
	  		background: {
	  			backgroundColor:
	  			Highcharts.defaultOptions.legend.backgroundColor || '#EEE',
	  			innerRadius: '60%',
	  			outerRadius: '100%',
	  			shape: 'arc'
	  		}
	  	},

	  	exporting: {
	  		enabled: false
	  	},

	  	tooltip: {
	  		enabled: false
	  	},

	    // the value axis
	    yAxis: {
	    	stops: [
	            [0.1, '#55BF3B'], // green
	            [0.5, '#DDDF0D'], // yellow
	            [0.9, '#DF5353'] // red
	            ],
	            lineWidth: 0,
	            tickWidth: 0,
	            minorTickInterval: null,
	            title: {
	            	y: -70
	            },
	            labels: {
	            	enabled: false
	            }
	        },

	        plotOptions: {
	        	solidgauge: {
	        		dataLabels: {
	        			y: 5,
	        			borderWidth: 0,
	        			useHTML: true
	        		}
	        	}
	        }
	    };

	    var chartFuelSensor=[];
	    for (var i = 0; i < $scope.noOfTank; i++) {
	  //alert(i);
	    // The speed gauge
	    var id='container-fuelSensor'+(i+1);
	//     alert(id)
	//        if(document.getElementById(id)){
	//     alert("Element exists");
	// } else {
	//     alert("Element does not exist");
	// }
	chartFuelSensor[i]= $('#'+id).highcharts(Highcharts.merge(sensorGaugeOptions, {
		yAxis: {
			min: 0,
			max: $scope.tankSizeArr[i],
			title: {
				text: ''
			}
		},

		credits: {
			enabled: false
		},

		series: [{
			name: 'Fuel',
			data: [$scope.fuelLitreArr[i]],
			dataLabels: {
				format:
				'<div style="text-align:center"><span style="font-size:12px; font-weight:normal;color: #196481'+ '">Fuel - {y} Ltr</span><br/>'

			},
			tooltip: {
				valueSuffix: ' km/h'
			}
		}]

	}))
}

$('#container-speed').highcharts({

	chart: {
		type: 'gauge',
		plotBackgroundColor: null,
		plotBackgroundImage: null,
		plotBorderWidth: 0,
		plotShadow: false,
		spacingBottom: 10,
		spacingTop: -60,
		spacingLeft: -20,
		spacingRight: -20,
	},

	title: {
		text: ''
	},

	pane: {
	          // startAngle: -150,
	          // endAngle: 150,
	          // // startAngle: -90,
	          // // endAngle: 90,
	          // center:['50%', '78%'],
	          // size: '68%',
	          startAngle: startangle,
	          endAngle: endangle,
	          center:['50%', speedCenter],
	          size: speedSize,
	          background: [{
	          	backgroundColor: {
	          		linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
	          		stops: [
	          		[0, '#FFF'],
	          		[1, '#333']
	          		]
	          	},
	          	borderWidth: 0,
	          	outerRadius: '109%'
	          }, {
	          	backgroundColor: {
	          		linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
	          		stops: [
	          		[0, '#333'],
	          		[1, '#FFF']
	          		]
	          	},
	          	borderWidth: 1,
	          	outerRadius: '107%'
	          }, {
	              // default background
	          }, {
	          	backgroundColor: '#DDD',
	          	borderWidth: 0,
	          	outerRadius: '105%',
	          	innerRadius: '103%'
	          }]
	      },
	      credits: { enabled: false },
	      // the value axis
	      yAxis: {
	      	min: 0,
	      	max: 200,

	      	minorTickInterval: 'auto',
	      	minorTickWidth: 1,
	      	minorTickLength: 10,
	      	minorTickPosition: 'inside',
	      	minorTickColor: '#666',

	      	tickPixelInterval: 30,
	      	tickWidth: 2,
	      	tickPosition: 'inside',
	      	tickLength: 10,
	      	tickColor: '#666',
	      	labels: {
	      		step: 2,
	      		rotation: 'auto'
	      	},
	      	title: {
	              // text: 'km/h'
	          },
	          plotBands: [{
	          	from: 0,
	          	to: 120,
	              color: '#55BF3B' // green
	          }, {
	          	from: 120,
	          	to: 160,
	              color: '#DDDF0D' // yellow
	          }, {
	          	from: 160,
	          	to: 200,
	              color: '#DF5353' // red
	          }]        
	      },

	      series: [{
	      	name: 'Speed',
	      	data: [total],
	      	tooltip: {
	      		valueSuffix: ' km/h'
	      	}
	      }]

	  });

	  // Bring life to the dials
	  setInterval(function () {
	  	var point;
	  	if($scope.noOfTank>1){
	  		for (var i = 0; i < $scope.noOfTank ; i++) {
		       // alert('cxvf');
		       var fuelId='#container-fuelSensor'+(i+1);
		       var chartFuelSensor = $(fuelId).highcharts(), point;
		       if (chartFuelSensor) {
		       	chartFuelSensor.yAxis[0].update({max:$scope.tankSizeArr[i]});
		       	point = chartFuelSensor.series[0].points[0];
		       	point.update(parseInt($scope.fuelLitreArr[i]));
		       }
		   }
		}

	}, 1000);
	  //END

	}


	setInterval(function () {
		var chart = $('#container-speed').highcharts(), point,options;
		if (chart) {
			point = chart.series[0].points[0];
			point.update(total);
		}

	}, 1000);


//end controller

}]);

$(document).ready(function(e) {
	$('#container-speed').highcharts({

		chart: {
			type: 'gauge',
			plotBackgroundColor: null,
			plotBackgroundImage: null,
			plotBorderWidth: 0,
			plotShadow: false,
			spacingBottom: 10,
			spacingTop: -60,
			spacingLeft: -20,
			spacingRight: -20,
		},

		title: {
			text: ''
		},

		pane: {
			startAngle: -90,
			endAngle: 90,
			center:['50%', '100%'],
			size: '100%',
			background: [{
				backgroundColor: {
					linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
					stops: [
					[0, '#FFF'],
					[1, '#333']
					]
				},
				borderWidth: 0,
				outerRadius: '109%'
			}, {
				backgroundColor: {
					linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
					stops: [
					[0, '#333'],
					[1, '#FFF']
					]
				},
				borderWidth: 1,
				outerRadius: '107%'
			}, {
	            // default background
	        }, {
	        	backgroundColor: '#DDD',
	        	borderWidth: 0,
	        	outerRadius: '105%',
	        	innerRadius: '103%'
	        }]
	    },
	    credits: { enabled: false },
	    // the value axis
	    yAxis: {
	    	min: 0,
	    	max: 200,

	    	minorTickInterval: 'auto',
	    	minorTickWidth: 1,
	    	minorTickLength: 10,
	    	minorTickPosition: 'inside',
	    	minorTickColor: '#666',

	    	tickPixelInterval: 30,
	    	tickWidth: 2,
	    	tickPosition: 'inside',
	    	tickLength: 10,
	    	tickColor: '#666',
	    	labels: {
	    		step: 2,
	    		rotation: 'auto'
	    	},
	    	title: {
	            // text: 'km/h'
	        },
	        plotBands: [{
	        	from: 0,
	        	to: 120,
	            color: '#55BF3B' // green
	        }, {
	        	from: 120,
	        	to: 160,
	            color: '#DDDF0D' // yellow
	        }, {
	        	from: 160,
	        	to: 200,
	            color: '#DF5353' // red
	        }]        
	    },

	    series: [{
	    	name: 'Speed',
	    	data: [total],
	    	tooltip: {
	    		valueSuffix: ' km/h'
	    	}
	    }]

	});


  /*  var gaugeOptions = {
        chart: {
            type: 'solidgauge',
         // backgroundColor:'rgba(255, 255, 255, 0)',
            spacingBottom: -10,
	        spacingTop: -40,
	        spacingLeft: 0,
	        spacingRight: 0,
        },
        title: null,
        pane: {
            center: ['50%', '90%'],
            size: '110%',
            startAngle: -90,
            endAngle: 90,
            background: {
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },
        tooltip: {
            enabled: false
        },
        yAxis: {
            stops: [
                [0.1, '#55BF3B'], 
                [0.5, '#DDDF0D'], 
                [0.9, '#DF5353'] 
            ],
            lineWidth: 0,
            minorTickInterval: null,
            tickPixelInterval: 400,
            tickWidth: 0,
            title: {
                y: -50
            },
            labels: {
                y: -100
            }
        },
        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 5,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    };

    $('#container-fuel').highcharts(Highcharts.merge(gaugeOptions, {
        yAxis: {
            min: 0,
            max: 300,
            title: { text: '' }
        },
        credits: { enabled: false },
        series: [{
            name: 'Speed',
            data: [fuelLtr],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:12px; font-weight:normal;color: #196481'+ '">Fuel - {y} Ltr</span><br/>',
                 // y: 25
            },
            tooltip: { valueSuffix: ' Ltr'}
        }]
    }));
    setInterval(function () {
      var chart = $('#container-speed').highcharts(), point;
        if (chart) {
            point = chart.series[0].points[0];
            point.update(total);
        }
       var chartFuel = $('#container-fuel').highcharts(), point;
        if (chartFuel) {
            point = chartFuel.series[0].points[0];

            console.log(point );

            point.update(fuelLtr);
            
            console.log(point);

            if(tankSize==0)
            	tankSize =200;
                chartFuel.yAxis[0].update({
			    max: tankSize,
			}); 

        }
    }, 1000);*/
    
    var gaugeOptions = {

    	chart: {
    		type: 'solidgauge',
    		spacingBottom: -10,
    		spacingTop: -40,
    		spacingLeft: 0,
    		spacingRight: 0,
    	},

    	title: null,

    	pane: {
    		center: ['50%', '90%'],
    		size: '110%',
    		startAngle: -90,
    		endAngle: 90,
    		background: {
    			backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
    			innerRadius: '60%',
    			outerRadius: '100%',
    			shape: 'arc'
    		}
    	},

    	tooltip: {
    		enabled: false
    	},

    // the value axis
    yAxis: {
    	stops: [
            [0.1, '#55BF3B'], // green
            [0.5, '#DDDF0D'], // yellow
            [0.9, '#DF5353'] // red
            ],
            lineWidth: 0,
            minorTickInterval: null,
            title: {
            	y: -50
            },
            labels: {
            	y: -100
            }
        },

        plotOptions: {
        	solidgauge: {
        		dataLabels: {
        			y: 5,
        			borderWidth: 0,
        			useHTML: true
        		}
        	}
        }
    };


    $('#container-fuel').highcharts(Highcharts.merge(gaugeOptions, {
    	yAxis: {
    		min: 0,
    		max: 300,
    		title: { text: '' }
    	},
    	credits: { enabled: false },
    	series: [{
    		name: 'Speed',
    		data: [fuelLtr],
    		dataLabels: {
    			format: '<span style="font-size:12px;text-align:center;top:10px;font-weight:normal;color: #196481'+ '">Fuel - {y} Ltr</span><br/>'
             // y: 25
         },
         tooltip: { valueSuffix: ' Ltr'}
     }]
 }));

    setInterval(function () {

        // var chart = $('#container-speed').highcharts(), point;

        // if (chart) {
        //     point = chart.series[0].points[0];
        //     point.update(total);
        // }


        if(sensor==1){
        	var chartFuel = $('#container-fuel').highcharts(), point;
        	if (chartFuel) {
        		chartFuel.yAxis[0].update({max:tankSize});
        		point = chartFuel.series[0].points[0];
        		point.update(fuelLtr);

        	}
        }

    }, 1000);

    $(document).ready(function(){
    	$('#minmax1').click(function(){
    		$('#contentreply').animate({
    			height: 'toggle'
    		},2000);
    	});
    });
    $("#menu-toggle").click(function(e) {
    	e.preventDefault();
    	$("#wrapper").toggleClass("toggled");
    });
});

