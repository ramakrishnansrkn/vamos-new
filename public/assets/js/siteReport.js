app.controller('mainCtrl',['$scope','vamoservice','$filter','_global','$translate',function($scope, vamoservice, $filter, GLOBAL,$translate){
	var language=localStorage.getItem('lang');
	$scope.multiLang=language;
	$translate.use(language);
	var translate = $filter('translate');
	var vehiArr = [];
	$scope.rowSpan=1;
	$scope.showLocTrips = 0;
 // trip summary , 	site report, trip report , rfid Report, multiple sites
 // global declaration
 $scope.trvShow       =  localStorage.getItem('trackNovateView');
 var assLabel  =  localStorage.getItem('isAssetUser');
 $scope.dealerName     =  localStorage.getItem('dealerName');
  //console.log(assLabel);
  $scope.vehiAssetView  = true; 
  
  if(assLabel=="true") {
  	$scope.vehiLabel = "Asset";
  	$scope.vehiImage = true;
  	$scope.vehiAssetView  = false;
  } else if(assLabel=="false") {
  	$scope.vehiLabel = "Vehicle";
  	$scope.vehiImage = false;
  	$scope.vehiAssetView  = true;
  } else {
  	$scope.vehiLabel = "Vehicle";
  	$scope.vehiImage = false;
  	$scope.vehiAssetView  = true;
  }
  var licenceExpiry="";
  $scope.vehicleMode = "";
  var strExpired='expired';
  if(localStorage.getItem('licenceExpiry'))licenceExpiry=localStorage.getItem('licenceExpiry');
  $scope.errMsg="";
  $scope.g_Url 	=	GLOBAL.DOMAIN_NAME;
  var expiryDays="";
  $scope.interval          =   getParameterByName('interval')?getParameterByName('interval'):"";
  
  $scope.locations = [];
  $scope.url = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');

  $scope.addressFuel 			= 	[];
  $scope.uiDate 				=	{};
  
  $scope.siteEntry 			=	0;
  $scope.siteExit 			=	0;
  
  $scope._site1               =  {}; 
  $scope._site2               =  {};
  $scope._site1.siteName      =  '';
  $scope._site2.siteName      =  '';
  $scope.mSiteError           =   0;

  $scope.msToTime		=	function(ms) {
  	if (ms < 0) {
  		ms = ms * -1;
  	}
  	if (ms == undefined || ms == null || ms == '')
  		return '-';
  	else{
  		days = Math.floor(ms / (24*60*60*1000));
  		daysms=ms % (24*60*60*1000);
  		hours = Math.floor((ms)/(60*60*1000));
  		hoursms=ms % (60*60*1000);
  		minutes = Math.floor((hoursms)/(60*1000));
  		minutesms=ms % (60*1000);
  		sec = Math.floor((minutesms)/(1000));
  		if(tab == 'tripshift'){
  			hours   = hours < 10 ? '0'+hours : hours;
  			minutes = minutes < 10 ? '0'+minutes : minutes; 
  			return hours+":"+minutes;
  		}else{
  			hours = (hours<10)?"0"+hours:hours;
  			minutes = (minutes<10)?"0"+minutes:minutes;
  			sec = (sec<10)?"0"+sec:sec;
  			return hours+":"+minutes+":"+sec;
  		}
  	}
  }


  $scope.convert_to_24hrs = function(time_str) {

  	if(time_str != undefined){
  		
  		var str   = time_str.split(' ');
  		var stradd  = str[0].concat(":00");
  		var strAMPM = stradd.concat(' '+str[1]);
  		var time = strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
  		var hours = Number(time[1]);
  		var minutes = Number(time[2]);
  		var seconds = Number(time[2]);
  		var meridian = time[4].toLowerCase();
  		
  		if (meridian == 'p' && hours < 12) {
  			hours = hours + 12;
  		}
  		else if (meridian == 'a' && hours == 12) {
  			hours = hours - 12;
  		} 

  		hours   = hours < 10 ? '0'+hours : hours;
  		minutes = minutes < 10 ? '0'+minutes : minutes;  
  		seconds = seconds < 10 ? '0'+seconds : seconds; 

  		var marktimestr = ''+hours+':'+minutes+':'+seconds;   
  	}

  	return marktimestr;
  };


    //chants for temperature

	// $(function () {
		function plottinGraphs(valueGraph, timeData){
			

			$('#temperatureChart').highcharts({
    	                                             // This is for all plots, change Date axis to local timezone
    	                                             title: {
    	                                             	text: ' '
    	                                             },

    	                                             xAxis: {
    	                                             	
    	                                             	type: 'datetime',
            // labels: {
            //     overflow: 'justify'
            // },
            // startOnTick: true,
            // showFirstLabel: true,
            // endOnTick: true,
            // showLastLabel: true,
            categories: timeData,
            // tickInterval: 10,
            // labels: {
            //     formatter: function() {
            //         return this.value.toString().substring(0, 6);
            //     },
            //     rotation: 0.1,
            //     align: 'left',
            //     step: 10,
            //     enabled: true
            // },
            style: {
            	fontSize: '8px'
            }
        },

        yAxis: {
        	title: {
        		text: null
        	}
        },

        tooltip: {
        	crosshairs: true,
        	shared: true,
        	valueSuffix: '°C'
        },

        legend: {
        },

        series: [{
        	name: translate('Temperature'),
        	
        	data: valueGraph,
        	zIndex: 1,
        	color: Highcharts.getOptions().colors[0],
        	marker: {
        		enabled: true,
        		fillColor: 'white',
        		lineWidth: 2,
        		lineColor: Highcharts.getOptions().colors[0],
        		symbol: 'circle'
        	}
        }]
    });
		};
		
		

		function getParameterByName(name) {
			name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
			var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search);
			return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
		}

		var tab = getParameterByName('tn');
		$scope.sort = sortByDate('date');
		
	/*
		THIS TWO VARIABLES FOR MUITLE SITE AND SITE TRIP REPORT ONLY
		*/
		$scope.caption 		= 'Multiple_Site_Report';
		$scope.hideShow 	= false;

		if(tab == 'tripkms' || tab == 'site' || tab == 'multiSite'||tab == 'tripshift'){
			$scope.sort 	= 	sortByDate('startTime')        						
		}
		else if (tab == 'rfid'){
			$scope.sort 	= 	sortByDate('fromTime')
		}
		else if (tab == 'rfidNew'){
			$scope.sort 	= 	sortByDate('dateTime')
		}
		else if(tab == 'alarm'){
			$scope.sort 	= 	sortByDate('alarmTime')
		}
		else if(tab == 'tripSite'){
			$scope.caption 	= 'Site_Trip';
			$scope.hideShow = true;	
			$scope.videoLink="https://www.youtube.com/watch?v=sIvahkk7uG8&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=5";
		} else if(tab == 'dailyFuel') {
			$scope.caption 	= 'Daily_Fuel_Burn';
			$scope.sort 	= 	sortByDate('time');
		}
		

	//$scope.locations01 = vamoservice.getDataCall($scope.url);
	$scope.trimColon = function(textVal){
		return (textVal != undefined) ? textVal.split(":")[0].trim() : textVal;
	}


	function sessionValue(vid, gname,licenceExpiry){
		localStorage.setItem('user', JSON.stringify(vid+','+gname));
		localStorage.setItem('licenceExpiry', licenceExpiry);
		$("#testLoad").load("../public/menu");
	}
	
	function convert_to_24h(time_str) {
		//console.log(time_str);
		var str		=	time_str.split(' ');
		var stradd	=	str[0].concat(":00");
		var strAMPM	=	stradd.concat(' '+str[1]);
		var time = strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
		var hours = Number(time[1]);
		var minutes = Number(time[2]);
		var seconds = '00';
		var meridian = time[4].toLowerCase();
		
		if (meridian == 'p' && hours < 12) {
			hours = hours + 12;
		}
		else if (meridian == 'a' && hours == 12) {
			hours = hours - 12;
		}	    
		var marktimestr	=	''+hours+':'+minutes+':'+seconds;	    
		return marktimestr;
	};

	
	function formatAMPM(date) {
		var date = new Date(date);
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? 'PM' : 'AM';
		hours = hours % 12;
		  hours = hours ? hours : 12; // the hour '0' should be '12'
		  minutes = minutes < 10 ? '0'+minutes : minutes;
		  var strTime = hours + ':' + minutes + ' ' + ampm;
		  return strTime;
		}

	//get the value from the ui

	function getUiValue(){
		$scope.uiDate.fromdate 		=	$('#dateFrom').val();
		$scope.uiDate.fromtime		=	$('#timeFrom').val();
		$scope.uiDate.todate		=	$('#dateTo').val();
		$scope.uiDate.totime 		=	$('#timeTo').val();
		if(localStorage.getItem('timeTochange')!='yes'){
			updateToTime();
			$scope.uiDate.totime    =   localStorage.getItem('toTime');
		}
	}

	
	function getSite(){
		var data;
		var urlSite = GLOBAL.DOMAIN_NAME+'/viewSite';  

        //console.log('urlSite called....');

        $.ajax({
        	url:urlSite, 
        	async: false,   
        	success:function(response) {
        		data = response;
        	}
        })

		//console.log(data);

		return data;
	}
	
	function urlReport(){
		var urlWebservice;
		var fromDate = moment($scope.uiDate.fromdate, "DD-MM-YYYY").format("YYYY-MM-DD");
		var toDate = moment($scope.uiDate.todate, "DD-MM-YYYY").format("YYYY-MM-DD");
		switch  (tab){
			case  'site' : 
			urlWebservice 	= 	$scope.g_Url+"/getSiteReport?vehicleId="+$scope.vehiname+"&fromDate="+fromDate+"&fromTime="+convert_to_24h($scope.uiDate.fromtime)+"&toDate="+toDate+"&toTime="+convert_to_24h($scope.uiDate.totime)+"&interval="+$scope.interval+"&site=true";
			break;
			case 'trip' :
			urlWebservice 	= 	$scope.g_Url+"/getTripReport?vehicleId="+$scope.vehiname+"&fromDate="+fromDate+"&fromTime="+convert_to_24h($scope.uiDate.fromtime)+"&toDate="+toDate+"&toTime="+convert_to_24h($scope.uiDate.totime)+"&interval="+$scope.interval;
			break;
			case 'tripkms' :
			if($scope.uiGroup == "TPMS") {
				urlWebservice 	= $scope.g_Url+"/getSitePlannedTripReport?vehicleId="+$scope.vehiname+"&fromDate="+fromDate+"&fromTime="+convert_to_24h($scope.uiDate.fromtime)+"&toDate="+toDate+"&toTime="+convert_to_24h($scope.uiDate.totime);
			} else {
				urlWebservice 	= $scope.g_Url+"/getTripSummary?vehicleId="+$scope.vehiname+"&fromDate="+fromDate+"&fromTime="+$scope.convert_to_24hrs($scope.uiDate.fromtime)+"&toDate="+toDate+"&toTime="+$scope.convert_to_24hrs($scope.uiDate.totime);
			}
			break;
			case 'tripshift' :
			urlWebservice 	= $scope.g_Url+"/getSitePlannedTripReport?vehicleId="+$scope.vehiname+"&fromDate="+fromDate+"&fromTime="+convert_to_24h($scope.uiDate.fromtime)+"&toDate="+toDate+"&toTime="+convert_to_24h($scope.uiDate.totime);
			//urlWebservice 	= $scope.g_Url+"/getTripSummary?vehicleId="+$scope.vehiname+"&fromDate="+fromDate+"&fromTime="+convert_to_24h($scope.uiDate.fromtime)+"&toDate="+toDate+"&toTime="+convert_to_24h($scope.uiDate.totime);
			break;
			case 'load' :           
			urlWebservice 	=	$scope.g_Url+"/getLoadReport?vehicleId="+$scope.vehiname+"&fromDate="+fromDate+"&fromTime="+convert_to_24h($scope.uiDate.fromtime)+"&toDate="+toDate+"&toTime="+convert_to_24h($scope.uiDate.totime);
			break;
			case 'temperature': case 'temperatureDev':
			urlWebservice 	= 	$scope.g_Url+"/getTemperatureReport?vehicleId="+$scope.vehiname+"&fromDate="+fromDate+"&fromTime="+convert_to_24h($scope.uiDate.fromtime)+"&toDate="+toDate+"&toTime="+convert_to_24h($scope.uiDate.totime)+"&interval="+$scope.interval;
			if (tab == 'temperatureDev')
				$scope.tempCation 	= "Temperature Deviation";
			else if (tab == 'temperature')
				$scope.tempCation 	= "Temperature Report";
			break;
			case 'alarm' :
			urlWebservice   =  $scope.g_Url+"/getAlarmReport?vehicleId="+$scope.vehiname+"&fromDate="+fromDate+"&fromTime="+convert_to_24h($scope.uiDate.fromtime)+"&toDate="+toDate+"&toTime="+convert_to_24h($scope.uiDate.totime);
			break;
			case 'rfid' :
			urlWebservice   =  $scope.g_Url+"/getRfidReport?vehicleId="+$scope.vehiname+"&fromDate="+fromDate+"&fromTime="+convert_to_24h($scope.uiDate.fromtime)+"&toDate="+toDate+"&toTime="+convert_to_24h($scope.uiDate.totime);
			break;
			case 'rfidNew' :
			urlWebservice   =  $scope.g_Url+"/getRfidReportNew?vehicleId="+$scope.vehiname+"&fromDate="+fromDate+"&fromTime="+convert_to_24h($scope.uiDate.fromtime)+"&toDate="+toDate+"&toTime="+convert_to_24h($scope.uiDate.totime);
			break;	
			case 'multiSite' :
			try{
					//startLoading();
					urlWebservice   =  $scope.g_Url+"/getSiteSummary?vehicleId="+$scope.vehiname+"&fromDate="+fromDate+"&fromTime="+convert_to_24h($scope.uiDate.fromtime)+"&toDate="+toDate+"&toTime="+convert_to_24h($scope.uiDate.totime)+"&stopTime="+$scope.interval+"&language=en"+"&site1="+$scope._site1.siteName+"&site2="+$scope._site2.siteName;
				}
				catch (e){
					stopLoading();
					return
				}
				break;
				case 'tripSite':
				urlWebservice   =  $scope.g_Url+"/getSiteTripReport?vehicleId="+$scope.vehiname+"&fromDate="+fromDate+"&fromTime="+convert_to_24h($scope.uiDate.fromtime)+"&toDate="+toDate+"&toTime="+convert_to_24h($scope.uiDate.totime)+"&language=en"+"&site1="+$scope._site1.siteName+"&site2="+$scope._site2.siteName;				
				break;
				case 'siteTripFuel':
				urlWebservice   =  $scope.g_Url+"/getSiteTripFuelReport?vehicleId="+$scope.vehiname+"&fromDate="+fromDate+"&fromTime="+changeTimeFormat($scope.uiDate.fromtime)+"&toDate="+toDate+"&toTime="+changeTimeFormat($scope.uiDate.totime)+"&language=en"+"&site1="+$scope._site1.siteName+"&site2="+$scope._site2.siteName;				
				break;
				case 'dailyFuel':
				urlWebservice   =  $scope.g_Url+"/getFuelReportDaily?vehicleId="+$scope.vehiname;
				// urlWebservice   =  $scope.g_Url+"/getFuelReportDaily?vehicleId="+$scope.vehiname+"&fromDate="+fromDate+"&fromTime="+convert_to_24h($scope.uiDate.fromtime)+"&toDate="+toDate+"&toTime="+convert_to_24h($scope.uiDate.totime)+"&language=en"+"&site1="+$scope._site1.siteName+"&site2="+$scope._site2.siteName;				
				break;
				default :
				break;
			}

			return urlWebservice;
		}

		var delayed6 = (function () {
			var queue = [];

			function processQueue() {
				if (queue.length > 0) {
					setTimeout(function () {
						queue.shift().cb();
						processQueue();
					}, queue[0].delay);
				}
			}

			return function delayed(delay, cb) {
				queue.push({ delay: delay, cb: cb });

				if (queue.length === 1) {
					processQueue();
				}
			};
		}());

		function google_api_call(tempurlFuel, index6, latFuel, lonFuel) {
			vamoservice.getDataCall(tempurlFuel).then(function(data){
				$scope.addressFuel[index6] = data.results[0].formatted_address;
			})
		};

	// for address in alarm report in address resolving
	$scope.recursive 	= 	function(locationFuel, indexFuel)
	{
		var index6 = 0;
		angular.forEach(locationFuel, function(value ,primaryKey){
			//console.log(' primaryKey '+primaryKey)
			index6 = primaryKey;
			if(locationFuel[index6].address == undefined)
			{
				var latFuel		 =	locationFuel[index6].lat;
				var lonFuel		 =	locationFuel[index6].lng;
				var tempurlFuel =	"https://maps.googleapis.com/maps/api/geocode/json?latlng="+latFuel+','+lonFuel+"&sensor=true";
				delayed6(2000, function (index6) {
					return function () {
						google_api_call(tempurlFuel, index6, latFuel, lonFuel);
					};
				}(index6));
			}
		})
	}

	//get the value
	$scope.rfidSplit 	= 	function(value, index){
		var _tageValue = value.split(';');
		return (index == 'one') ? _tageValue[0] :  _tageValue[1];
	}

	//get the value
	$scope.rfidSplitNew 	= 	function(value){
		var spValue = value.split(';');
		return spValue;
	}

	function rfidNewFunc(data){
		$scope.error=data.error;
		$scope.rfiData=[];
		var dataVar=[];
        //console.log(data);

        for(var i=0;i<data.gd.length;i++) {
           //console.log(data.gd[i]);
           var tagVal=$scope.rfidSplitNew(data.gd[i].tagDetail);
           if(tagVal.length==3){
           	dataVar.push({tagNo:tagVal[0],employeeId:null,employeeName:null,dept:null,mobile:null,dateTime:tagVal[2],address:data.gd[i].address,lat:data.gd[i].lat,lng:data.gd[i].lng});
           }else{
           	dataVar.push({tagNo:tagVal[0],employeeId:tagVal[2],employeeName:tagVal[1],dept:tagVal[4],mobile:tagVal[5],dateTime:tagVal[6],address:data.gd[i].address,lat:data.gd[i].lat,lng:data.gd[i].lng});
           }
       }

       $scope.rfiData=dataVar;
          //console.log($scope.rfiData);
      }


      $scope.histRedirect 	=	function(obj, ind){
      	var url_Hist 		= '../public/track?maps=replay&vehicleId='+$scope.vehiname+'&gid='+$scope.gName;
      	var ind_list 		= (ind % 2) ? [ind-1, ind] : [ind, ind+1];
      	window.open(url_Hist+'&fTime='+obj[ind_list[0]].startTime+'&tTime='+obj[ind_list[1]].endTime, '_blank');
      }

      $scope.getLocationTrips    =   function(val){
      	$scope.showLocTrips = val
      }
      function setBtnEnable(btnName){
      	$scope.yesterdayDisabled = false;
      	$scope.weekDisabled = false;
      	$scope.monthDisabled = false;
      	$scope.todayDisabled = false;
      	$scope.lastmonthDisabled = false;
      	$scope.thisweekDisabled = false;

      	switch(btnName){

      		case 'yesterday':
      		$scope.yesterdayDisabled = true;
      		break;
      		case 'today':
      		$scope.todayDisabled = true;
      		break;
      		case 'thisweek':
      		$scope.thisweekDisabled = true;
      		break;
      		case 'lastweek':
      		$scope.weekDisabled = true;
      		break;
      		case 'month':
      		$scope.monthDisabled = true;
      		break;
      		case 'lastmonth':
      		$scope.lastmonthDisabled = true;
      		break;
      	}


      }
      setBtnEnable("init");
      $scope.durationFilter    =   function(duration){
  //alert('inside function');
  startLoading();
  var now = new Date();
  $scope.uiDate.todate      = getTodayDate(now.setDate(now.getDate() - 1));
  switch(duration){
  	
  	case 'yesterday':
  	setBtnEnable('yesterday');
  	var d = new Date();
  	$scope.uiDate.fromdate= getTodayDate(d.setDate(d.getDate() - 1));
  	$scope.uiDate.totime  = '11:59 PM';
      // datechange();
      break;
      case 'thisweek':
      setBtnEnable('thisweek');
      var d=new Date();
      var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1);
      var firstday= new Date(d.getFullYear(), d.getMonth(), diff);
      $scope.uiDate.fromdate      = getTodayDate(firstday.setDate(firstday.getDate() ));
      $scope.uiDate.todate= getTodayDate(new Date().setDate(new Date().getDate() ));

      $scope.uiDate.totime       = formatAMPM(d);
      //datechange();
      break;
      case 'lastweek':
      setBtnEnable('lastweek');
      var d = new Date();
      var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1)-7;
      var diff1=d.getDate() - day + (day == 0 ? -6:1)-1;
      var firstday= new Date(d.getFullYear(), d.getMonth(), diff);
      var lastday= new Date(d.getFullYear(), d.getMonth(), diff1);
      $scope.uiDate.fromdate      = getTodayDate(firstday.setDate(firstday.getDate() ));
      $scope.uiDate.todate       = getTodayDate(lastday.setDate(lastday.getDate() ));
      $scope.uiDate.totime  = '11:59 PM';
      //datechange();
      break;
      case 'month':
      setBtnEnable('month');
      var date = new Date();
      var firstdate = new Date(date.getFullYear(), date.getMonth(), 1);
      $scope.uiDate.fromdate       = getTodayDate(firstdate.setDate(firstdate.getDate() ));
      $scope.uiDate.todate       = getTodayDate(date.setDate(date.getDate() ));

      $scope.uiDate.totime      = formatAMPM(date);
       //datechange();
       break;
       case 'today':
       setBtnEnable('today');
       var d = new Date();
       $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() ));
       $scope.uiDate.todate       = getTodayDate(d.setDate(d.getDate() ));
       $scope.uiDate.totime       = formatAMPM(d);
      //datechange();
      break;
      case 'lastmonth':
      setBtnEnable('lastmonth');
      var date = new Date();
      var firstdate = new Date(date.getFullYear(), date.getMonth()-1, 1);
      var lastdate = new Date(date.getFullYear(), date.getMonth(), 0);
      $scope.uiDate.fromdate       = getTodayDate(firstdate.setDate(firstdate.getDate() ));
      $scope.uiDate.todate      = getTodayDate(lastdate.setDate(lastdate.getDate() ));

      $scope.uiDate.totime   = '11:59 PM';
      //datechange();
      break;
  }
  webServiceCall();
}
$scope.getTimeSpansToShift    =   function(timeUTC){

	var date = new Date(parseInt(timeUTC));
	var h = date.getHours();
	var m = date.getMinutes();
	var minutes = (h* 60) + m;
	//alert(minutes);
	var shift;
    if(minutes > 470 && minutes < 1000){ // 7.50-16.40
    	shift = "A";
    }else if((minutes > 1000 && minutes < 1440)|| (minutes >= 0 && minutes < 90)){ // 16 : 40 - 01:30
    	shift = "B";
    }else if(minutes > 90 && minutes < 470){ // 1:30 - 7:50
    	shift = "C";
    }
    return shift;

}


	function webServiceCall(){
		$scope.siteTripError='';
		
		var url = urlReport();
		var graphList = [];
		var graphTime = [];		
		$scope.siteData = [];
		$scope.siteTripFuelData = [];
		var urlUTC;
		$scope.errMsg="";
		if(tab=='tripkms' && $scope.uiGroup!='TPMS'){
			urlUTC = url+'&fromDateUTC='+utcFormat($scope.uiDate.fromdate,$scope.convert_to_24hrs($scope.uiDate.fromtime))+'&toDateUTC='+utcFormat($scope.uiDate.todate,changeTimeFormat($scope.uiDate.totime));}
			else {
				urlUTC = url+'&fromDateUTC='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateUTC='+utcFormat($scope.uiDate.todate,changeTimeFormat($scope.uiDate.totime));
			}
			var expdate=moment().add(expiryDays,'days').format('DD-MM-YYYY'),
			convertedexpdate=utcFormat(expdate,$scope.convert_to_24hrs('11:59 PM')),
			convertedtodate=utcFormat($scope.uiDate.todate,$scope.convert_to_24hrs($scope.uiDate.totime)),
			convertedfromdate=utcFormat($scope.uiDate.fromdate,$scope.convert_to_24hrs($scope.uiDate.fromtime));
			if(convertedtodate<=convertedexpdate && convertedfromdate<=convertedexpdate){
				if ((url != undefined) && (checkXssProtection($scope.uiDate.fromdate) == true) && (checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true)){
					vamoservice.getDataCall(urlUTC).then(function(responseVal){
						stopLoading();
						if(responseVal=='Failed'){
							$scope.siteTripError =  translate("curl_error");
						}                                                                                                                          
						else {
							$scope.error=responseVal.error;
              //console.log(responseVal);

              try {

              	if(tab == 'rfidNew'){
              		rfidNewFunc(responseVal);
              	}  

              	if(tab == 'alarm')
              		try{
              			$scope.recursive(responseVal.alarmList,0);
              		} catch (er){
              			console.log(' address Solving '+er);
              		}
              		$scope.siteData = responseVal;
              		$scope.mSiteError=1;
              		if(tab=='siteTripFuel'){
              			$scope.siteTripFuelData=responseVal;
              		}
              		
              		stopLoading();

              		if(tab == 'tripshift'){
              			$scope.siteData.countDetail=[];
              			var locArr=$scope.siteData.tripData.map(site=>site.location);
              			var uniqueLocs = [];
              			$.each(locArr, function(i, el){
              				if($.inArray(el, uniqueLocs) === -1) uniqueLocs.push(el);
              			});

              			angular.forEach(uniqueLocs, function(loc,index){
              				var count = 0;
              				var totalLoss = 0;
              				var indLocData = [];

              				var indLocData=$scope.siteData.tripData.filter(site => site.location === loc);
              				var count = indLocData.length;
              				angular.forEach(indLocData, function(value, key){
              					
              					if (value.delayTime > 0) {
              						totalLoss = totalLoss+value.delayTime;
              					}

              				})
              				$scope.siteData.countDetail.push({'location':loc, 'count':count, 'totalLoss': totalLoss})

              			});
              			console.log($scope.siteData.countDetail);
              			$scope.rowSpan=$scope.siteData.countDetail.length;

              		}
              		if(tab == 'tripkms'){
              			$scope.siteData.countDetail=[];
              			var locArr=$scope.siteData.tripData.map(site=>site.location);
              			var uniqueLocs = [];
              			$.each(locArr, function(i, el){
              				if($.inArray(el, uniqueLocs) === -1) uniqueLocs.push(el);
              			});

              			angular.forEach(uniqueLocs, function(loc,index){
              				var onTimeAchievedCount = 0;
              				var onTimeNotAchievedCount = 0;
              				var totalLoss = 0;
              				var averageActualTime = 0;
              				var totalActualTime = 0;
              				var indLocData = [];
              				var onTimeAchievedCount = [];
              				var onTimeNotAchievedCount = [];
              				var indLocData=$scope.siteData.tripData.filter(site => site.location === loc);
              				var onTimeAchieved=$scope.siteData.tripData.filter(site => site.location === loc&&site.duration<site.planTime);
              				onTimeAchievedCount = onTimeAchieved.length;
              				var onTimeNotAchieved=$scope.siteData.tripData.filter(site => site.location === loc&&site.duration>site.planTime);
              				onTimeNotAchievedCount = onTimeNotAchieved.length;
              				angular.forEach(indLocData, function(value, key){
	                               //delay time calculation
	                               if (value.delayTime > 0) {
	                               	totalLoss = totalLoss+value.delayTime;
	                               }
	                               totalActualTime = totalActualTime+value.duration;
	                           })
							  //actual time average
							  if(indLocData.length){
							  	averageActualTime = totalActualTime/indLocData.length
							  }else{
							  	averageActualTime = totalActualTime
							  }
							  $scope.siteData.countDetail.push({'location':loc, 'onTimeAchievedCount': onTimeAchievedCount, 'onTimeNotAchievedCount': onTimeNotAchievedCount, 'totalDelay': totalLoss,'averageActualTime' : averageActualTime})

							});
              			$scope.rowSpan=$scope.siteData.countDetail.length;

              		}


					/*
						FOR TEMPORARY LOAD
						*/

						if(tab == 'load')
						{
							$scope.siteData ={};
							$scope.siteData.load =[];
							var spLoading ;
							try
							{


								angular.forEach(responseVal.load, function(value, keyLoad){
									spLoading = splitColon(value.load);
									$scope.siteData.load.push({'date':value.date, 'lat':value.lat, 'lng': value.lng, 'Axle1': spLoading[0], 'Axle2': spLoading[1], 'Axle3': spLoading[2], 'Axle4': spLoading[3], 'Axle5': spLoading[4], 'Axle6': spLoading[5], 'Axle7': spLoading[6], 'Axle8': spLoading[7],  'LoadTruck': spLoading[8], 'LoadTrailer': spLoading[9], 'TotalLoadTruck': spLoading[10], 'TotalLoadTrailer': spLoading[11]})
								})

							}
							catch(err)
							{

							}
						}

						
						var entry=0,exit=0; 
						if (tab == 'site')
							angular.forEach(responseVal, function(val, key){
								if(tab == 'site'){
									if(val.state == 'SiteExit')
										exit++ 
									else if (val.state == 'SiteEntry')
										entry++
								}
							})

						if(tab == 'temperature' || tab == 'temperatureDev'){
							$scope.videoLink="https://www.youtube.com/watch?v=n1aeUGV-JDo&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=18";

							$scope.siteData = [];
							angular.forEach(responseVal.temperature, function(graphValue, graphKey){
						//var time = moment(graphValue.date).format("DD-MM-YYYY h:mm:ss");
						if(graphValue.isdeviation == 'No' && tab == 'temperature'){
							
							graphList.push(Number(graphValue.temperature));
							graphTime.push(moment(graphValue.date).format("DD-MM-YYYY h:mm:ss"));
							$scope.siteData.push(graphValue);
								// plottinGraphs(temperature);
							} else if(graphValue.isdeviation == 'Yes' && tab == 'temperatureDev'){
								
								graphList.push(Number(graphValue.temperature));
								graphTime.push(moment(graphValue.date).format("DD-MM-YYYY h:mm:ss"));
								$scope.siteData.push(graphValue);
							}	
						})
							plottinGraphs(graphList, graphTime);
						} 
					// else if(tab == 'temperatureDev'){
					// 	$scope.siteData = [];

					// }

					$scope.siteEntry 	=	entry;
					$scope.siteExit 	=	exit;

					stopLoading();	
				} catch (err){
					console.log(' print err '+err);
					stopLoading();	
				}
			}
			
		});
} else
stopLoading();
}else{
	stopLoading();
	$scope.errMsg=licenceExpiry;
} 
}

	// initial method

	$scope.$watch("url", function (val) {
		startLoading();
		vamoservice.getDataCall($scope.url).then(function(data) {
			$scope.vehicle_list = data;
			var siteNames 	= 	[];
			if(data.length){
				$scope.vehiname	= getParameterByName('vid');
				$scope.uiGroup 	= $scope.trimColon(getParameterByName('vg'));
				$scope.gName 	= getParameterByName('vg');
				angular.forEach(data, function(val, key){
					if($scope.gName == val.group){
						$scope.gIndex = val.rowId;
						angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys){

							if($scope.vehiname=='undefined' && keys==0){

								$scope.shortNam	= value.shortName;
								$scope.vehicleMode	= value.vehicleMode;
								licenceExpiry=value.licenceExpiry;
								expiryDays=value.expiryDays;
								$scope.hasTempratue = value.serial1;
								if(tab == 'multiSite' || tab == 'tripSite' || tab=='siteTripFuel')
								{
									$scope.orgId 	= value.orgId;
									var siteValue = getSite();
									angular.forEach(JSON.parse(siteValue).siteParent, function(val, key){
										if(val.orgId == $scope.orgId){
											if(val.site.length > 0){
						      					// if(tab == 'multiSite')
						      					siteNames.push({'siteName':'All'});
						      					angular.forEach(val.site, function(siteName, keys){
						      						siteNames.push(siteName);

						      					})
						      					
						      					$scope.siteName = siteNames;
						      					$scope._site1 	=	siteNames[0];
						      					$scope._site2 	=	siteNames[0];
						      				}
						      			}
						      			
						      		})
								}         

							} else {

								if($scope.vehiname == value.vehicleId){

									$scope.shortNam	= value.shortName;
									$scope.vehicleMode	= value.vehicleMode;
									licenceExpiry=value.licenceExpiry;
									expiryDays=value.expiryDays;
									$scope.hasTempratue = value.serial1;
									if(tab == 'multiSite' || tab == 'tripSite' || tab=='siteTripFuel')
									{
										$scope.orgId 	= value.orgId;
										var siteValue = getSite();
										angular.forEach(JSON.parse(siteValue).siteParent, function(val, key){
											if(val.orgId == $scope.orgId){
												if(val.site.length > 0){
									      					// if(tab == 'multiSite')
									      					siteNames.push({'siteName':'All'});
									      					angular.forEach(val.site, function(siteName, keys){
									      						siteNames.push(siteName);

									      					})
									      					
									      					$scope.siteName = siteNames;
									      					$scope._site1 	=	siteNames[0];
									      					$scope._site2 	=	siteNames[0];
									      				}
									      			}
									      			
									      		})
									} 
									
								}
							}
						})
						
					}
					
				})
				
				sessionValue($scope.vehiname, $scope.gName,licenceExpiry)
				$scope.notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
				window.localStorage.setItem('totalNotifications',$scope.notncount);
			}
			$scope.interval				= 	-1;
			$scope.uiDate.fromdate 		=	localStorage.getItem('fromDate');
			$scope.uiDate.fromtime		=	localStorage.getItem('fromTime');
			$scope.uiDate.todate		=	localStorage.getItem('toDate');
			$scope.uiDate.totime 		=  localStorage.getItem('toTime');
			if(localStorage.getItem('timeTochange')!='yes'){
				updateToTime();
				$scope.uiDate.totime    =   localStorage.getItem('toTime');
			}
			// $scope.fromNowTS			=	new Date();
			// $scope.uiDate.fromdate 		=	getTodayDate($scope.fromNowTS);
		 //  	$scope.uiDate.fromtime		=	'12:00 AM';
		 //  	$scope.uiDate.todate		=	getTodayDate($scope.fromNowTS);
		 //  	$scope.uiDate.totime 		=	formatAMPM($scope.fromNowTS.getTime());
		 if(tab == 'tripSite' ){
		 	var dateObj1 			= 	new Date();
		 	var  dateNow		    =	new Date(dateObj1.setDate(dateObj1.getDate()));
		 	var fromdatenow         =	getTodayDate(dateNow);
				//alert(fromdatenow);
				if(fromdatenow==localStorage.getItem('fromDate'))
				{
					$scope.fromNowTS1			=	new Date().getTime() - 86400000;
					console.log($scope.fromNowTS1);
					$scope.uiDate.fromdate 		=	getTodayDate($scope.fromNowTS1);
			  //$scope.uiDate.fromdate 	    =	getTodayDate($scope.fromNowTS.setDate($scope.fromNowTS.getDate()-7));
			  $scope.uiDate.fromtime		=	'12:00 AM';
			  $scope.uiDate.todate		=	getTodayDate($scope.fromNowTS1);
			  $scope.uiDate.totime 		=	localStorage.getItem('toTime');
			  if(localStorage.getItem('timeTochange')!='yes'){
                //updateToTime();
                $scope.uiDate.totime    =   '11:59 PM';
            }
			  	//$scope.uiDate.totime 		=	'11:59 PM';
			  }else
			  {
			  	$scope.uiDate.fromdate 		=	localStorage.getItem('fromDate');
			  	$scope.uiDate.fromtime		=	localStorage.getItem('fromTime');
			  	$scope.uiDate.todate		=	localStorage.getItem('toDate');
			  	$scope.uiDate.totime 		=   localStorage.getItem('toTime');
			  	if(localStorage.getItem('timeTochange')!='yes'){
			  		updateToTime();
			  		$scope.uiDate.totime    =   localStorage.getItem('toTime');
			  	}
			  }
			}else if(tab == 'dailyFuel'){
				$scope.fromNowTS1			=	new Date().getTime() - 86400000;
		  	  //$scope.uiDate.fromdate 	    =	getTodayDate($scope.fromNowTS);
		  	  $scope.uiDate.fromdate 		=	getTodayDate($scope.fromNowTS.setDate($scope.fromNowTS.getDate()-7));
		  	  $scope.uiDate.fromtime		=	'12:00 AM';
		  	  $scope.uiDate.todate		=	getTodayDate($scope.fromNowTS1);
		  	  $scope.uiDate.totime 		=	localStorage.getItem('toTime');
		  	  if(localStorage.getItem('timeTochange')!='yes'){
		  	  	updateToTime();
		  	  	$scope.uiDate.totime    =   localStorage.getItem('toTime');
		  	  }
			  	//$scope.uiDate.totime 		=	'11:59 PM';
		  		// $ new Date().getTime() - 86400000
		  		// console.log(' dailyFuel condition ');
		  		// console.log(' dailyFuel condition '+ $scope.uiDate.fromdate);
		  	}
		  	webServiceCall();
		  	//stopLoading();
		  });	
});


$scope.groupSelection 	= function(groupName, groupId) {
	startLoading();
	licenceExpiry="";
	$scope.errMsg="";
	$scope.mSiteError  =   0;
	$scope.gName 	= 	groupName;
	$scope.uiGroup 	= 	$scope.trimColon(groupName);
	$scope.gIndex	=	groupId;
	
	var url  		= 	$scope.g_Url+"/getVehicleLocations?group="+groupName;
	vamoservice.getDataCall(url).then(function(response){
			//stopLoading();
			$scope.vehicle_list = response;
			$scope.shortNam		= response[$scope.gIndex].vehicleLocations[0].shortName;
			$scope.vehiname		= response[$scope.gIndex].vehicleLocations[0].vehicleId;
			$scope.orgId		= response[$scope.gIndex].vehicleLocations[0].orgId;
			$scope.vehicleMode	= response[$scope.gIndex].vehicleLocations[0].vehicleMode;
			licenceExpiry		= response[$scope.gIndex].vehicleLocations[0].licenceExpiry;
			expiryDays=response[$scope.gIndex].vehicleLocations[0].expiryDays;
			$scope.hasTempratue = response[$scope.gIndex].vehicleLocations[0].serial1;
			sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
			$scope.notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
			window.localStorage.setItem('totalNotifications',$scope.notncount);
			if(tab == 'tripSite'){
				var pageUrl='tripSite?vid='+$scope.vehiname+'&vg='+$scope.gName+'&tn=tripSite';
				$(location).attr('href',pageUrl);
			}
			else{
				webServiceCall();
			}
			
		});
	
}


$scope.genericFunction 	= function (vehid, index,shortname,position, address,groupName,licenceExp,hasTemp){
	startLoading();
	$scope.vehiname		= vehid;
	
	$scope.vehTripName = vehid;
	$scope.shortNam=shortname;
	licenceExpiry=licenceExp;
	$scope.hasTempratue=hasTemp;
	$scope.errMsg="";


	sessionValue($scope.vehiname, $scope.gName,licenceExpiry)
	angular.forEach($scope.vehicle_list[$scope.gIndex].vehicleLocations, function(val, key){
		if(vehid == val.vehicleId){
			$scope.shortNam	= val.shortName;
			$scope.orgId = val.orgId;
			$scope.vehicleMode = val.vehicleMode;
			expiryDays=val.expiryDays;
		}
	})
	getUiValue();
	webServiceCall();

}

$scope.submitFunction 	=	function(){
	
	$scope.yesterdayDisabled = false;
	$scope.weekDisabled = false;
	$scope.monthDisabled = false;
	$scope.todayDisabled = false;
	$scope.lastmonthDisabled = false;
	$scope.thisweekDisabled = false;
	startLoading();
	$scope.mSiteError   =   0;
	getUiValue();
	webServiceCall();
}

$scope.exportData = function (xlsVal) {
		// console.log(data);
		var blob = new Blob([document.getElementById(xlsVal).innerHTML], {
			type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
		});
		saveAs(blob, xlsVal+".xls");
	};

	$scope.exportDataCSV = function (data) {
		// console.log(data);
		CSV.begin('#'+data).download(data+'.csv').go();
	};

	$('#minus').click(function(){
		$('#menu').toggle(1000);
	})

	

	var map, myLatlng;

	function initialize(latLngVal) {

		console.log('init..');
		
		myLatlng = latLngVal;
		var mapOptions = {
			zoom: 14,	
			zoomControlOptions: { position: google.maps.ControlPosition.LEFT_TOP },
			center: myLatlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		map = new google.maps.Map(document.getElementById("map_canvas"),mapOptions);

		$('#mapmodals').on('shown.bs.modal', function () {
			google.maps.event.trigger(map, "resize");
			map.setCenter(myLatlng);
		}); 
	}

	
//if(tab == 'tripkms')
	//initialize();
//google.maps.event.addDomListener(window, 'load', initialize);


  //start of modal google map
  

  // 	jQuery('#mapmodals')
 	// .on('shown.bs.modal',
  //     function(){
  //       google.maps.event.trigger(map,'resize',{});
  //       map.setCenter(myLatlng);
  //    });

  var latLanPath =[];
  var marker, markerList =[];

  $scope.drawLine = function(loc1, loc2){
  	startLoading();

  	$scope.startlatlong = new google.maps.LatLng(loc1, loc2);

  	var flightPlanCoordinates=[];
  	if(map==null){
  		initialize($scope.startlatlong);
  		var image = '';
  		
  		image = 'assets/imgs/startflag.png';
  		marker = new google.maps.Marker({
  			position: new google.maps.LatLng(loc1, loc2),
  			map: map,
  			icon: image
  		});
  		markerList.push(marker);

  	}
  	
  	else {
  		if($scope.endlatlong!=null){
  			flightPlanCoordinates = [$scope.startlatlong,$scope.endlatlong];}
  			map.setCenter($scope.startlatlong);
  		}
  		
  		$scope.flightPath = new google.maps.Polyline({
  			path: flightPlanCoordinates,
  			geodesic: true,
  			strokeColor: "#00c4ff",
  			strokeOpacity: 0.7,
  			strokeWeight: 5,
  			map: map,
  		});
  		$scope.endlatlong =$scope.startlatlong;
  		latLanPath.push($scope.flightPath);
		// console.log(' value '+loc1+'-----'+loc2);
		
		stopLoading();
	}
	

	// clear the polyline function
	function clearMap(){
		$scope.endlatlong = null;
		$scope.startlatlong = null;
		for (var i=0; i<latLanPath.length; i++){
			latLanPath[i].setMap(null);
		}
		
		for (var i = 0; i < markerList.length; i++) {
			markerList[i].setMap(null);
		};
	}

	
	//marker function in maps
	function startEndMarker(lat, lan, ind){
		var image = '';
		if(ind == 0)
			image = 'assets/imgs/startflag.png'
		else
			image = 'assets/imgs/endflag.png'
		marker = new google.maps.Marker({
			position: new google.maps.LatLng(lat, lan),
			map: map,
			icon: image
		});
		markerList.push(marker);
	}

  //end of modal google map
  $scope.getInput = function(inputValue, vehicleDetails){
  	clearMap(latLanPath);
  	var startDate, endDate, startTime, endTime;
  	startDate	= 	$filter('date')(inputValue.startTime, 'yyyy-MM-dd');
  	endDate 	= 	$filter('date')(inputValue.endTime, 'yyyy-MM-dd');
  	startTime	= 	$filter('date')(inputValue.startTime, 'HH:mm:ss');
  	endTime		= 	$filter('date')(inputValue.endTime, 'HH:mm:ss');
  	console.log(utcFormat(startDate,startTime));
  	var url 	= 	$scope.g_Url+"/getVehicleHistory?vehicleId="+vehicleDetails.vehicleId+"&fromDate="+startDate+"&fromTime="+startTime+"&toDate="+endDate+"&toTime="+endTime;
  	vamoservice.getDataCall(url).then(function(dataGet){
 			// if(dataGet.vehicleLocations[0] || dataGet.vehicleLocations[dataGet.vehicleLocations.length-1])
 			// 	startEndMarker(val.latitude, val.longitude);
 			var len = dataGet.vehicleLocations.length;
 			angular.forEach(dataGet.vehicleLocations, function(val, key){
 				if(key == 0 || key == len-1){
 					startEndMarker(val.latitude, val.longitude, key);
 					// console.log(' key value ---'+key);
 					$scope.drawLine(val.latitude, val.longitude);}

 				});
 		});

  	
  }

  $scope.showMap=function() {
  	$scope.stoppageFromDate = $scope.uiDate.fromDate,
  	$scope.stoppageToDate = $scope.uiDate.toDate,
  	stoppageurl = GLOBAL.DOMAIN_NAME+"/getVehicleBasedStoppageReport?vehicleId="+$scope.vehiname+'&fromDateUTC='+utcFormat($scope.stoppageFromDate,convert_to_24h('12:00 AM'))+'&toDateUTC='+utcFormat($scope.stoppageToDate,convert_to_24h('11:59 PM'));
  	vamoservice.getDataCall(stoppageurl).then(function(data){
  		$('#monthlyModal').on('shown.bs.modal', function () {
  			mapOptions = {
  				zoom: 7, 
  				zoomControlOptions: { position: google.maps.ControlPosition.LEFT_TOP },
  				center: new google.maps.LatLng(data.history[0].latitude,data.history[0].langitude),
  				mapTypeId: google.maps.MapTypeId.ROADMAP
  			};
  			map = new google.maps.Map(document.getElementById("map_canvas"),mapOptions);
  		}); 
  	});
  }

// });

// if(tab === 'temperature')
// setInterval(function () {
//       var chart = $('#temperatureChart').highcharts(), point;
//         if (chart) {
//             point = chart.series[0].points[0];
//             point.update(total);
//         }
//        var chartFuel = $('#container-fuel').highcharts(), point;
//         if (chartFuel) {
//             point = chartFuel.series[0].points[0];
//             point.update(fuelLtr);
//             if(tankSize==0)
//             	tankSize =200;
//             chartFuel.yAxis[0].update({
// 			    max: tankSize,
// 			}); 

//         }
//     }, 1000);

}]);