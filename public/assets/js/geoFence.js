app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global','$translate', function($scope, $http, vamoservice, $filter, GLOBAL,$translate){
  
  var language=localStorage.getItem('lang');
  $scope.multiLang=language;
  $translate.use(language);
  var translate = $filter('translate');
  $scope.groupId      =   0;

  function getParameterByName(name) {
      name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
          results = regex.exec(location.search);
      return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  $scope.locations = [];
  $scope.url = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
  $scope.gIndex =0;

  $scope.trimColon = function(textVal){
    return textVal.split(":")[0].trim();
  }

  function sessionValue(vid, gname){
    localStorage.setItem('user', JSON.stringify(vid+','+gname));
    $("#testLoad").load("../public/menu");
  }
  
  
  // initial method

  $scope.$watch("url", function (val) {
      startLoading();
      $http.get($scope.url).success(function(data) {
        stopLoading();
        if(data.length >0 && data != '') {

          if(data[$scope.groupId].vehicleLocations!=null){
            
          $scope.vehicle_list  =  [];
          //$scope.locations     =  $scope.vehiSidebar(data);
          $scope.locations     =  data;
          $scope.vehicle_list  =  data;

          $scope.vehigroup    =   data[$scope.groupId].group;
          $scope.totalVehicles   =  data[$scope.groupId]['totalVehicles'];
          $scope.vehicleOnline   =  data[$scope.groupId]['online'];
          $scope.noDataCount     =  data[$scope.groupId]['totalNoDataVehicles'];
          $scope.movingCount     =  data[$scope.groupId]['totalMovingVehicles'];
          var notncount =getParkedIgnitionAlert(data[$scope.groupId].vehicleLocations)[3];
          $('#notncount').text(notncount);
          window.localStorage.setItem('totalNotifications',notncount);
          window.localStorage.setItem("groupname",$scope.vehigroup);
          $scope.geoVehLocations();
          $scope.vehiname   = data[$scope.groupId].vehicleLocations[0].vehicleId;
          //localStorage.setItem('user', JSON.stringify($scope.vehiname+','+$scope.vehigroup));
          sessionValue($scope.vehiname, $scope.vehigroup);
          }
        }
        //stopLoading();
      }).error(function(){stopLoading();});
  });

  var newGroupSelectionName="";
  var oldGroupSelectionName="";
  var initValss=0;
  $scope.groupSelections=0;
  
  $scope.groupSelection = function(groupname, groupid){

      $scope.groupSelections=1;

       if(initValss>0){
        oldGroupSelectionName=newGroupSelectionName;
       }

        newGroupSelectionName=groupname;
        initValss++;

      if(oldGroupSelectionName != newGroupSelectionName){

    $scope.groupId  =   groupid;
    $scope.vehigroup = groupname;
    $scope.url      =   GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+$scope.vehigroup;
    

     }
  }
   
  $scope.geoVehLocations = function() {
    startLoading();
    $scope.sitesDataGroup=[];

    var userMasterId  = window.localStorage.getItem("userMasterName");
    var groupMasterId = window.localStorage.getItem("groupname");

      if (userMasterId != null) {
        var splitValue    = userMasterId.split(',');
        var userName      = splitValue[1];
      }
     
      var requestUrl    =  GLOBAL.DOMAIN_NAME+'/getSitewiseVehicleCount?groupId='+groupMasterId;
        
      //console.log(requestUrl);

        $http.get(requestUrl).success(function(data) { 
          stopLoading();
          $scope.getGeoFence    = data.siteDetails;

          if($scope.getGeoFence != null){
            $scope.verifyGeoCount = $scope.getGeoFence.length;
          }

      });
  }

   $scope.siteSplitName  = function(data,num) {
     //console.log(num);
       var splitRetValue;

       if(data)
         {
         splitRetValue = data.split(/[:]+/);
       //splitRetValue = splitValue[0];
         switch(num){ 
          case 1:
            return splitRetValue[0];
          break;
          case 2:
            return splitRetValue[1];
          break;
          case 3:
            return splitRetValue[2];
          break;
          case 4:
           return splitRetValue[3];
          break; 
          case 5:
           return splitRetValue[4];
          break;      
          }

       }else{
         
         return splitRetValue ="";        
        }
  }
   // millesec to day, hours, min, sec
  $scope.msToTime = function(ms) 
  {    

      if(ms != null){
        days = Math.floor(ms / (24 * 60 * 60 * 1000));
        daysms = ms % (24 * 60 * 60 * 1000);
        hours = Math.floor((ms) / (60 * 60 * 1000));
        hoursms = ms % (60 * 60 * 1000);
        minutes = Math.floor((hoursms) / (60 * 1000));
        minutesms = ms % (60 * 1000);
        sec        =  Math.floor((minutesms)/(1000));
        hours = (hours<10)?"0"+hours:hours;
        minutes = (minutes<10)?"0"+minutes:minutes;
        sec = (sec<10)?"0"+sec:sec;
        // if(days==0)
        //  return hours +" h "+minutes+" m "+seconds+" s ";
        // else
        return hours +":"+minutes+":"+sec;
      }
  }

  $scope.exportData = function (data) {
    // console.log(data);
    var blob = new Blob([document.getElementById(data).innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, data+".xls");
    };

    $scope.exportDataCSV = function (data) {
    // console.log(data);
    CSV.begin('#'+data).download(data+'.csv').go();
    };

}]);
