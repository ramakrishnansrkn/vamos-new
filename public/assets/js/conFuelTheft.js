app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global','$translate', function($scope, $http, vamoservice, $filter, GLOBAL,$translate){
  
  $scope.videoLink="https://www.youtube.com/watch?v=LOhxGEkuzuA&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5";
  var language=localStorage.getItem('lang');
  $scope.multiLang=language;
  $translate.use(language);
  var translate = $filter('translate');
  $scope.groupId      =   0;
  $scope.monthsVal=["January", "February","March","April","May","June","July","August","September","October","November","December"];

  function getParameterByName(name) {
      name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
          results = regex.exec(location.search);
      return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  $scope.vehicle_list = [];
  $scope.url = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
  $scope.gIndex =0;
  $scope.monVals = new Date().getMonth()+1; 
  $scope.yearVals = new Date().getFullYear();
  $scope.fromMonthss = $scope.monVals+'/'+$scope.yearVals;
  $scope.lenMon   = getDaysInMonthYear($scope.monVals,$scope.yearVals);

  $scope.trimColon = function(textVal){
    return textVal.split(":")[0].trim();
  }

  function sessionValue(vid, gname){
    localStorage.setItem('user', JSON.stringify(vid+','+gname));
    $("#testLoad").load("../public/menu");
  }
  
  
  // initial method

  $scope.$watch("url", function (val) {
      startLoading();
      $scope.vehicle_list  =  [];
      $http.get($scope.url).success(function(data) {
        stopLoading();
        if(data.length >0 && data != '') {

          if(data[$scope.groupId].vehicleLocations!=null){
          $scope.vehicle_list  =  data;
          $scope.vehigroup    =   data[$scope.groupId].group;
          var notncount =getParkedIgnitionAlert(data[$scope.groupId].vehicleLocations)[3];
          $('#notncount').text(notncount);
          window.localStorage.setItem('totalNotifications',notncount);
          window.localStorage.setItem("groupname",$scope.vehigroup);
          webcall();
          $scope.vehiname   = data[$scope.groupId].vehicleLocations[0].vehicleId;
          //localStorage.setItem('user', JSON.stringify($scope.vehiname+','+$scope.vehigroup));
          sessionValue($scope.vehiname, $scope.vehigroup);
          }
        }
        //stopLoading();
      }).error(function(){stopLoading();});
  });

  var newGroupSelectionName="";
  var oldGroupSelectionName="";
  var initValss=0;
  $scope.groupSelections=0;
  
  $scope.groupSelection = function(groupname, groupid){
    setTimeout(function(){ stopLoading(); enhance(); }, 100);
  
      $scope.groupSelections=1;
       if(initValss>0){
        oldGroupSelectionName=newGroupSelectionName;
       }

        newGroupSelectionName=groupname;
        initValss++;

      if(oldGroupSelectionName != newGroupSelectionName){

    $scope.groupId  =   groupid;
    $scope.vehigroup = groupname;
    $scope.url      =   GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+$scope.vehigroup;
    undo();
    webcall();

     }
  }
  function getDaysInMonthYear(month,year) {
    return new Date(year, month, 0).getDate();
  }
  function webcall(argument) {
      $scope.fuelTheftError = "";
      if($scope.vehicle_list[$scope.gIndex].vehicleLocations!=null){
      var isAllStarter = $scope.vehicle_list[$scope.gIndex].vehicleLocations.every(function(v){
        return v.licenceType == 'Starter';
      })
    }
      if(isAllStarter){
           $scope.fuelTheftError = 'This feature is not privileged for selected group';
           stopLoading();
      }else{
        var monthFuelTheftUrl =GLOBAL.DOMAIN_NAME+'/getFuelReports?groupId='+$scope.vehigroup+'&month='+$scope.monVals+'&year='+$scope.yearVals+'&fuelMethod=DROP';
        
               
            $scope.monthFuelTheftData  = [];
            $http.get(monthFuelTheftUrl).success(function(data){
              if(data=='Failed'){
                stopLoading();
                $scope.fuelTheftError =  translate("curl_error"); 
              }else{
                if(data.error==null){
                $scope.monthFuelTheftData = data.vehicleFuelData;
                  $scope.monthDates=[];
                  for(var i=0;i<$scope.lenMon;i++){
                    $scope.monthDates.push(i+1);
                  }
                setTimeout(function(){ stopLoading(); enhance(); }, 100);
              }
              else {
                $scope.fuelTheftError = data.error;
              }
              }

        });
      }
    
  }
   
$scope.submitMon=function(){
  undo();
  var newmonVals  = document.getElementById("monthFrom").value.split('/');
  $scope.monVals  = parseInt(newmonVals[0]);
  $scope.yearVals = newmonVals[1];

  $scope.lenMon   = getDaysInMonthYear($scope.monVals,$scope.yearVals);
  $scope.colValss = $scope.lenMon+2;
  webcall();
}
 
   // millesec to day, hours, min, sec
  $scope.msToTime = function(ms) 
  {
      if(ms != null){
        days = Math.floor(ms / (24 * 60 * 60 * 1000));
        daysms = ms % (24 * 60 * 60 * 1000);
        hours = Math.floor((ms) / (60 * 60 * 1000));
        hoursms = ms % (60 * 60 * 1000);
        minutes = Math.floor((hoursms) / (60 * 1000));
        minutesms = ms % (60 * 1000);
        seconds = Math.floor((minutesms) / 1000);
        // if(days==0)
        //  return hours +" h "+minutes+" m "+seconds+" s ";
        // else
        return hours +":"+minutes+":"+seconds;
      }
  }

  $scope.exportData = function (data) {
    // console.log(data);
    undo();
    var blob = new Blob([document.getElementById(data).innerHTML], {
            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, data+".xls");
      enhance();
    };

    $scope.exportDataCSV = function (data) {
    // console.log(data);
    undo();
    CSV.begin('#'+data).download(data+'.csv').go();
    enhance();
    };

}]);
