app.controller('mainCtrl',['$scope','$http','vamoservice','_global','$filter','$translate', function($scope, $http, vamoservice, GLOBAL,$filter,$translate){
$scope.videoLink="https://www.youtube.com/watch?v=AyFyNAIxoxI&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=14";
 var language=localStorage.getItem('lang');
    $scope.multiLang=language;
    $translate.use(language);
 var translate = $filter('translate');
 var assLabel    =  localStorage.getItem('isAssetUser');
 $scope.trvShow  =  localStorage.getItem('trackNovateView');
 $scope.allVehicles = 0;
 var infoBoxs ={};
 var infowindow  = {};
 if( localStorage.getItem('mapNo')!=undefined){
 	$scope.map_change=localStorage.getItem('mapNo');
 	preMap=$scope.map_change;
 }
 else {
 	localStorage.setItem('mapNo',0);
 	$scope.map_change=0;
 	preMap=0;
 }
  
 var zoomValue=7;
 var userMasterId  = window.localStorage.getItem("userMasterName");
  if (userMasterId != null) {
    var splitValue    = userMasterId.split(',');
    var userName      = splitValue[1];
  }
  if(userName=="TPMS"){
  	zoomValue=18;
  }
 $scope.vg       =  getParameterByName('vg').split(":")[0];

 if(assLabel=="true") {
     $scope.vehiLabel = "Asset";
 } else if(assLabel=="false") {
     $scope.vehiLabel = "Vehicle";
 } else {
     $scope.vehiLabel = "Vehicle";
 }	
//Global Variables
$scope.selectScreen 		 =  ['screen1'];
getVehicleLocations 		 =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations';
getSelectedVehicleLocation   =  GLOBAL.DOMAIN_NAME+'/getSelectedVehicleLocation?vehicleId=';
getSelectedVehicleLocation1  =  GLOBAL.DOMAIN_NAME+'/getSelectedVehicleLocation1?vehicleId=';

path1  = [];
path2  = [];
path3  = [];
path4  = [];

var _mapsDetails = {};
var _pathDetails = {};
var osm_mapsDetails = {};	
//osm map init
var maposm = L.map('map_osm').setView([12.993803, 80.193075], zoomValue);

L.tileLayer('https://osm.vamosys.com/osm_tiles/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://osm.vamosys.com/nominatim/lf.html">OpenStreeetMap</a> contributors'
}).addTo(maposm);

//google map init
    mapProp = {
		    		center: new google.maps.LatLng(12.993803, 80.193075),
		    		zoom:zoomValue,
			    	zoomControlOptions: { position: google.maps.ControlPosition.LEFT_TOP}, 
					mapTypeId: google.maps.MapTypeId.ROADMAP
				};
				
   googleMap = new google.maps.Map(document.getElementById("maploc1"),mapProp);
 


// var myIcon = L.icon({
//     iconUrl: 'assets/imgs/P.png',
//      iconSize: [40,40], 
//      iconAnchor:[20,40],
//      popupAnchor:[-1,-40] 
// });
if($scope.map_change==0) {

               //console.log('init_google.....');
                 document.getElementById("map_osm").style.display="none"; 
                 document.getElementById("maploc1").style.display="block"; 
                 
                
            } else if($scope.map_change==1) {

                //console.log('init_osm.....');
                  document.getElementById("maploc1").style.display="none"; 
                  document.getElementById("map_osm").style.display="block";

            }


$scope.getshortNames = function(vg) {
	startLoading();
       if($scope.vg!=undefined){
        $username =JSON.parse(localStorage.getItem('userIdName')).split(",")[1];
        //alert($username)
        var groupName=vg;
        $scope.vehiSelected=vg;

       $scope.gName=vg+':'+$scope.fcode;
       var urlGroup = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+$scope.gName;
	   $http.get(urlGroup).success(function(data){
	     angular.forEach(data, function(val, key){
	          if($scope.gName == val.group){
	          	$scope.repNames = [];
                angular.forEach(data[val.rowId].vehicleLocations, function(vehicle, index){
                	if(vehicle.licenceType != 'Starter'){
                		$scope.repNames.push({'vehicleId': vehicle.vehicleId , 'shortName': vehicle.vehicleId })
                	}
                });
                console.log($scope.repNames);
	          }
	        });
	       $scope.vehiclelist = $scope.repNames;
	       setTimeout(function () {
		       $('.selectpicker').selectpicker('refresh');
		   },500)
	      
	   });

  }
return $scope.repNames;
}


$scope.trackMultiVehi =  function(vehicles){
       //alert($scope.vehiSelected);
  if($scope.vehiSelected.length)
    angular.forEach($scope.vehiSelected, function(value){
     alert(value.vehicleId);
    })
}

function getParameterByName(name) {
      name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
          results = regex.exec(location.search);
      return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

// $scope.update = function(){
// 	//startLoading();
// 	vamoservice.getDataCall(getVehicleLocations+'?group='+$scope.vehicleSelected+':'+$scope.fcode).then(function(data){
// 		angular.forEach(data, function(groupVehicle){
// 			if($scope.vehicleSelected+':'+$scope.fcode == groupVehicle.group)
// 				if(groupVehicle.vehicleLocations){
// 					$scope.vehicles = groupVehicle.vehicleLocations;
// 					//stopLoading();
// 				}
// 		})
// 	})
// }

 // $scope.selectAll = function() {

 // 	if($scope.vehicleSelected!=undefined) {

 // 		startLoading();

 //    for(key in _mapsDetails) {
	// 	 _mapsDetails[key].setMap(null);
	// }

 //    _mapsDetails = {};


	// 	    vamoservice.getDataCall(getVehicleLocations+'?group='+$scope.vehicleSelected+':'+$scope.fcode).then(function(data) {

 //               angular.forEach(data, function(groupVehicle){

	// 	    if($scope.vehicleSelected+':'+$scope.fcode == groupVehicle.group){



	// 		   angular.forEach(groupVehicle.vehicleLocations, function(value, key) {

	// 		   	   console.log(value);

	// 		   	   var url      =  getSelectedVehicleLocation+value.vehicleId;
	//                    var urlVeh   =  getSelectedVehicleLocation1+value.vehicleId;

			

	// 		        selectingScreen('screen1', value, value.vehicleId);
	// 		        serviceCall('screen1', url, urlVeh, value.vehicleId);

	// 			});	

	// 		   $scope.allVehicles=1;

	// 	    } 
	// 	          });

	// 		}); 


 //        stopLoading();

 //        } else {

 //        	alert(translate('Select group')+'!');
 //        }
 // }



 $scope.update = function(){
   startLoading();
   
   $scope.gName=$scope.groupSelected+':'+$scope.fcode;
       var urlGroup = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+$scope.gName;
    $http.get(urlGroup).success(function(data){
     angular.forEach(data, function(val, key){
                  //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
                  //alert($scope.gName);
          if($scope.gName == val.group){
            $scope.gIndex = val.rowId;
            $scope.groupid=$scope.gIndex;
            $scope.vehiname   =  data[$scope.gIndex].vehicleLocations[0].vehicleId;
            $scope.gName      =  data[$scope.gIndex].group;
            localStorage.setItem('user', JSON.stringify($scope.vehiname+','+$scope.gName)); 
          }
        });
      
     var pageUrl='track?vehicleId='+$scope.vehiname+'&vg='+$scope.gName+'&track=multiTrack&maps=mulitple';
      $(location).attr('href',pageUrl);
   //stopLoading();
   });
  
  
}
$scope.selectedVehi=[];
 $scope.selectAll = function(type,mapchange) {
    $scope.groupSelected=$scope.vg;
 	if($scope.groupSelected!=undefined) {

 		startLoading();
		if($scope.map_change==0){
		    for(key in _mapsDetails) {
				 _mapsDetails[key].setMap(null);
				}
				_mapsDetails = {};

				for(vehi in infoBoxs) {
				 infoBoxs[vehi].setMap(null);
				}
			     infoBoxs={};
				
			}
			else if($scope.map_change==1){
				 for(key in osm_mapsDetails) {
				 maposm.removeLayer(osm_mapsDetails[key]);
				}
				osm_mapsDetails = {};
			}
		
		 

    
		    if(type=='All'){
		    	$(".bs-select-all").trigger('click');
		    	$scope.selectedVehi=$scope.vehiclelist;
		    	//alert($scope.selectedVehi);
		    }
           if($scope.selectedVehi.length!=0||type=='All'){
		    vamoservice.getDataCall(getVehicleLocations+'?group='+$scope.groupSelected+':'+$scope.fcode).then(function(data) {

               angular.forEach(data, function(groupVehicle){

		    if($scope.groupSelected+':'+$scope.fcode == groupVehicle.group){



			   angular.forEach(groupVehicle.vehicleLocations, function(value, key) {

			   	   console.log(value);
          if(type!='All'){
            angular.forEach($scope.selectedVehi, function(selVehi){
     
				     if(selVehi.vehicleId==value.vehicleId){
				     	//alert(selVehi.vehicleId);
				     	            var url      =  getSelectedVehicleLocation+value.vehicleId;
					                var urlVeh   =  getSelectedVehicleLocation1+value.vehicleId;
							        selectingScreen('screen1', value, value.vehicleId);
							        serviceCall('screen1', url, urlVeh, value.vehicleId);
				     }
		    })
         }
		else{
		                    var url      =  getSelectedVehicleLocation+value.vehicleId;
			                var urlVeh   =  getSelectedVehicleLocation1+value.vehicleId;
					        selectingScreen('screen1', value, value.vehicleId);
					        serviceCall('screen1', url, urlVeh, value.vehicleId);
		}



			   	   

				});	

			   $scope.allVehicles=1;

		    } 
		          });

			}); 


        stopLoading();
    }
    else{
    	 alert(translate('Select vehicle')+'!');
    	stopLoading();
      }

        } else {

        	 alert(translate('Select group')+'!');
        }
 }

 
 $scope.initMethod = function(){
    if($scope.map_change==0){
		    for(key in _mapsDetails) {
				 _mapsDetails[key].setMap(null);
				}
				_mapsDetails = {};
				
			}
			else if($scope.map_change==1){
				 for(key in osm_mapsDetails) {
				 maposm.removeLayer(osm_mapsDetails[key]);
				}
				osm_mapsDetails = {};
			}
	// for(key in _mapsDetails) {
	// 	  if($scope.map_change==0)
	// 		   _mapsDetails[key].setMap(null);
	//       else if($scope.map_change==1)
	// 		   maposm.removeLayer(_mapsDetails[key]);
	// }

	// _mapsDetails = {};   
	
 } 

function filterGroup(groups){
	var filter = [];
	var splitValue ='';
	angular.forEach(groups, function(value){
		if (value.group)
			splitValue = value.group.split(":");
			filter.push(splitValue[0])
	})
	return filter
}

// var getJoke = function(addressUrl){
//    var fetchingAddress = '';
// 	  $.ajax({
// 	    url:addressUrl, 
// 	    async: false,   
// 	    success:function(dat) {
// 	      fetchingAddress = dat.results[0].formatted_address
// 	    }
// 	})
//   return fetchingAddress;
//  }


function resolveAddress(response)
{	var address = ' ';
	try
	{
		var addressUrl 	= 	"https://maps.googleapis.com/maps/api/geocode/json?latlng="+response.latitude+','+response.longitude+"&sensor=true"
		address = (response.address==undefined? getJoke(addressUrl) : response.address)
		return address;
	} 
	catch (err) 
	{
		return address;
		stopLoading();
		console.log(' error '+err)
	}
	
}

function markerDrop(response, screen, marker){
	marker.setPosition(new google.maps.LatLng(response.latitude,response.longitude));
	marker.setMap(screen);
	// screen.setCenter(new google.maps.LatLng(response.latitude,response.longitude));
	marker.set('labelAnchor', (new google.maps.Point(0,0)));
	marker.set('labelClass', "multi");
	marker.set('labelInBackground', false);
    
    if($scope.trvShow == 'true'){
	  marker.setIcon(vamoservice.trvIcon(response));
	} else {
	  marker.setIcon(vamoservice.iconURL(response));	
	}
	
	_mapsDetails[response.vehicleId] = marker;
	 marker.addListener('click', function() {
		    infowindow[response.vehicleId].open(googleMap,marker);
		  });
	stopLoading();
}
function markerDrop_osm(response, screen, marker){
	marker.bindLabel('Vehicle Id : '+response.vehicleId+'<br>Speed : ' +response.speed+'km/h'+'<br>Odo Distance : ' +response.odoDistance+'<br>Address : ' +response.address);
    marker.setLatLng([response.latitude,response.longitude]);
    marker.addTo(screen);
    
    if($scope.trvShow == 'true'){
	  markerIcon = L.icon({
      iconUrl: vamoservice.trvIcon(response),
     iconSize: [40,40], 
     iconAnchor:[20,40],
     popupAnchor:[-1,-40] 
   });
	   marker.setIcon(markerIcon);
	} else {
	  //osmMarker.setIcon(vamoservice.iconURL(response));	
	  markerIcon = L.icon({
      iconUrl:vamoservice.iconURL(response),
     iconSize: [40,40], 
     iconAnchor:[20,40],
     popupAnchor:[-1,-40] 
   });
	   marker.setIcon(markerIcon);
	}
	
	osm_mapsDetails[response.vehicleId] = marker;
	//osmMarker.remove();
	stopLoading();
}

function selectingScreen(screen, response, vehid) {

  switch (screen){
		case 'screen1':
			// googleMap = new google.maps.Map(document.getElementById("maploc1"),mapProp);
			if($scope.map_change==0) {
                //marker1 = new MarkerWithLabel({labelContent: ''+translate($scope.vehiLabel)+' '+translate('Name')+' - '+response.shortName+'<br>'+translate('Speed')+' - '+response.speed+' '+translate('kms/h')+'<br>'+translate('odoDistance')+' - '+response.odoDistance+'<br>'+translate('Address')+' - '+resolveAddress(response)});
                 //start lable
                marker1 = new google.maps.Marker({
				          position: new google.maps.LatLng(response.latitude,response.longitude),
				          map: googleMap,
				          //title: translate('Speed')+' - '+response.speed+' | '+translate('Address')+' - '+resolveAddress(response)
				        });

			    var myOptions = {
				  content: response.shortName,
				  boxStyle: {
                  background: '#FFFFFF',
                  color: '#000000',
                  textAlign: "center",
                  fontSize: "8pt",
                  fontWeight:"bold",
                  border:"0.5px solid black",
                  padding:"2px 2px 2px 2px",
                  borderRadius:"6px"
                },
				  disableAutoPan: true,
				  pixelOffset: new google.maps.Size(0, -10), 
                  position: new google.maps.LatLng(response.latitude,response.longitude),
				  closeBoxURL: "",
				  isHidden: false,
				  pane: "mapPane",
                  zIndex: 100,
				  enableEventPropagation: true
				};


				infoBoxs[response.vehicleId] = new InfoBox(myOptions);
				infoBoxs[response.vehicleId].open(googleMap);



			  var contentString = '<div><b>'+
			      ''+translate($scope.vehiLabel)+' '+translate('Name')+' - '+response.shortName+'<br>'+translate('Speed')+' - '+response.speed+' '+translate('kms/h')+'<br>'+translate('odoDistance')+' - '+response.odoDistance+'<br>'+translate('Address')+' - '+resolveAddress(response)+
			      '</b></div>';

			  infowindow[response.vehicleId] = new google.maps.InfoWindow({
			    content: contentString
			  });
			 
				//end label
			    markerDrop(response, googleMap, marker1);
                
            } else if($scope.map_change==1) {
            	osmMarker=L.marker([response.latitude,response.longitude]);
		    // osmMarker.bindPopup('Vehicle Id : '+response.vehicleId+'<br>Speed : ' +response.speed+'km/h'+'<br>Odo Distance : ' +response.odoDistance+'<br>Address : ' +response.address)
		    // .openPopup();
			markerDrop_osm(response, maposm, osmMarker);
            }
			// emptyJob(screen);
			// path1.push(response.latitude,response.longitude);
			// obj1 = response;
		break;
		case 'screen2':
			$scope.screen2 = new google.maps.Map(document.getElementById("maploc2"),mapProp);
			marker2 = new MarkerWithLabel({labelContent: ''+translate($scope.vehiLabel)+' '+translate('Name')+' - '+response.shortName+'<br>'+translate('Speed')+' - '+response.speed+' '+translate('kms/h')+'<br>'+translate('odoDistance')+' - '+response.odoDistance+'<br>'+translate('Address')+' - '+resolveAddress(response)});
			markerDrop(response, $scope.screen2, marker2);
			emptyJob(screen);
			path2.push(response.latitude,response.longitude);
			obj2 = response;
			break;
		case 'screen3':
			$scope.screen3 = new google.maps.Map(document.getElementById("maploc3"),mapProp);
			marker3 = new MarkerWithLabel({labelContent: ''+translate($scope.vehiLabel)+' '+translate('Name')+' - '+response.shortName+'<br>'+translate('Speed')+' - '+response.speed+' '+translate('kms/h')+'<br>'+translate('odoDistance')+' - '+response.odoDistance+'<br>'+translate('Address')+' - '+resolveAddress(response)});
			markerDrop(response, $scope.screen3, marker3);
			emptyJob(screen);
			path3.push(response.latitude,response.longitude);
			obj3 = response;
			break;
		case 'screen4':
			$scope.screen4 = new google.maps.Map(document.getElementById("maploc4"),mapProp);
			marker4 = new MarkerWithLabel({labelContent: ''+translate($scope.vehiLabel)+' ' +translate('Name')+' - '+response.shortName+'<br>'+translate('Speed')+' - '+response.speed+' '+translate('kms/h')+'<br>'+translate('odoDistance')+' - '+response.odoDistance+'<br>'+translate('Address')+' - '+resolveAddress(response)});
			markerDrop(response, $scope.screen4, marker4);
			emptyJob(screen);
			path4.push(response.latitude,response.longitude);
			obj4 = response;
			break;
		default:
			break;
	} 

	//stopLoading();

}

// function selectMap(screen){
// 	switch (screen){
// 		case 'screen1':
// 			return googleMap;
// 			break;
// 		case 'screen2':
// 			return $scope.screen2;
// 			break;
// 		case 'screen3':
// 			return $scope.screen3;
// 			break;
// 		case 'screen4':
// 			return $scope.screen4;
// 			break;
// 		default:
// 			break;
// 	}
// }

var pathlist = []
function lineDraw(lat, lan, screen, vehicleid){

	if($scope.map_change==0) {
                $scope.slatlong = new google.maps.LatLng(lat, lan);
				$scope.polyline = new google.maps.Polyline({
					map: googleMap,
					path: [$scope.elatlong,$scope.slatlong],
					strokeColor: '#00b3fd',
					strokeOpacity: 0.7,
					strokeWeight: 5,
					
					clickable: true
				
				});

				$scope.elatlong = 	$scope.slatlong;
                
      } else if($scope.map_change==1) {
            	$scope.slatlong = [parseFloat(lat), parseFloat(lan)];
				var latlngs = [$scope.elatlong,$scope.slatlong];
				var polyline = L.polyline(latlngs, {color: '#00b3fd'}).addTo(maposm);
				$scope.elatlong = 	$scope.slatlong;
            }
	
	// _pathDetails[vehicleid] = pathlist.push($scope.slatlong);

    
}


function joinLine(urlVehi, vehicleid)
{
	vamoservice.getDataCall(urlVehi).then(function(res){
       
// create a red polyline from an array of LatLng points


		var sp;
		var latlan = [];
		$scope.slatlong = {};
		$scope.elatlong = {};
		$scope.polyline = {};
		// var latLngOld= ["20.345136,74.180436","20.344211,74.18148","20.343227,74.182578","20.342202,74.183724","20.3412,74.184862","20.34022,74.185991","20.339247,74.187102","20.338271,74.188196","20.337422,74.189111","20.336598,74.190031","20.336598,74.190031"]
		for (var i = 0; i < res.latLngOld.length; i++) {
			sp = res.latLngOld[i].split(',');
			lineDraw(sp[0], sp[1], screen, vehicleid);
		}
		// selectingScreen('screen1', res, vehicleno)
	})
}

function serviceCall(screen, url, urlVehi, vehicleid)
{
	
	// vamoservice.getDataCall(url).then(function(response){
	// 	mapProp = {
 //    		center: new google.maps.LatLng(response.latitude,response.longitude),
 //    		zoom:13,
	//     	zoomControlOptions: { position: google.maps.ControlPosition.LEFT_TOP}, 
	// 		mapTypeId: google.maps.MapTypeId.ROADMAP
	// 	};
	// 	googleMap = new google.maps.Map(document.getElementById("maploc1"),mapProp);
	// 	joinLine(urlVehi);
	// 	// selectingScreen(screen, response, vehicleid)
			
	// })
joinLine(urlVehi, vehicleid);

}


function init() {

	var vehicleno  =  getParameterByName('vehicleId');
	if(vehicleno==""||vehicleno==undefined){
	vehicleno=JSON.parse(localStorage.getItem('user')).split(',')[0];
    }
	var url        =  getSelectedVehicleLocation+vehicleno;
	var urlVeh     =  getSelectedVehicleLocation1+vehicleno;
	try {
		
		startLoading();
		
		vamoservice.getDataCall(getVehicleLocations).then(function(data){
			vamoservice.getDataCall(url).then(function(response){
				
				if($scope.map_change==0) {
					//googleMap.setCenter(response.latitude,response.longitude);
                    googleMap.setCenter(new google.maps.LatLng(response.latitude,response.longitude));

                
            } else if($scope.map_change==1) {
            	
            	maposm.setView([response.latitude,response.longitude], zoomValue);
            }
				
				
				selectingScreen('screen1', response, vehicleno);
				serviceCall('screen1', url, urlVeh, vehicleno);

		});

			$scope.sliceGroup = filterGroup(data)
			groupName	 	=	data[0].group.split(':');
			$scope.fcode 	=	groupName[1];
			$scope.getshortNames($scope.vg);

			// $scope.getVehicle = data
			stopLoading();
		})
	} catch (err){
		console.log('print err'+err)
		
	}
    
  /*   $http({
            method : "GET",
            url : getVehicleLocations
        }).then(function mySuccess(response) {

            $scope.data = response.data;
           
            mapProp = {
		       center: new google.maps.LatLng( $scope.data[0].latitude, $scope.data[0].longitude ),
		       zoom: 7,
			   zoomControlOptions: { position: google.maps.ControlPosition.LEFT_TOP}, 
			   mapTypeId: google.maps.MapTypeId.ROADMAP
		    };


            setMarkers(response.data[0]);

		    googleMap = new google.maps.Map(document.getElementById("maploc1"),mapProp);


            

		    //stopLoading();
				
      }, function myError(response) {
            console.log( response.statusText );
      });*/

};
init();

function setMarkers(data){

	angular.forEach(data.vehicleLocations, function(value, key){

		console.log(value);

        selectingScreen(value);

	});	
}


// (function (){
// 	console.log(' hello world ');
// }())

// function emptyJob(screenNo)
// {
// 	switch (screenNo){
// 		case 'screen1':
// 			path1 =[];
// 			obj1 =[];
// 			break;
// 		case 'screen2':
// 			path2 =[];
// 			obj2 =[];
// 			break;
// 		case 'screen3':
// 			path3 =[];
// 			obj3 =[];
// 			break;
// 		case 'screen4':
// 			path4 =[];
// 			obj4 =[];
// 			break;
// 		default:
// 			break;
// 	}
// }


// $scope.multiTracking = function(vehicle)
// {	

//   // if($scope.allVehicles==1){

//    	//console.log('all');
     
//     for(key in _mapsDetails) {
//     	if($scope.map_change==0)
// 		 _mapsDetails[key].setMap(null);
// 		else if($scope.map_change==1)
// 		 maposm.removeLayer(_mapsDetails[key]);

// 		// console.log(_mapsDetails[key]);
// 	}
    
// 	_mapsDetails = {};

// 	//$scope.allVehicles=0;

//   // }

// 	startLoading();
// 	try
// 	{
// 		// var vehiclelist = [];
// 		// console.log($scope.vehicles);
// 		// for (key in $scope.vehicles){
// 		// 	if($scope.vehicles[key].vehicleId == vehicle.vehicleId){
// 		// 		delete $scope.vehicles[key];
// 		// 		 console.log($scope.vehicles);
// 		// 	}
// 		// 	$scope.vehicles.push($scope.vehicles[key]) 
// 		// }
// 		// $scope.vehicles = null;
// 		// $scope.vehicles = vehiclelist;
// 		var status =  0;
// 		for(key in _mapsDetails)
// 			if (vehicle.vehicleId == key)
// 				status = 1;

// 			if(status == 0)
// 				vamoservice.getDataCall(getSelectedVehicleLocation+vehicle.vehicleId).then(function(response){
// 					selectingScreen('screen1', response, vehicle.vehicleId);
// 					serviceCall('screen1', getSelectedVehicleLocation+vehicle.vehicleId, getSelectedVehicleLocation1+vehicle.vehicleId, vehicle.vehicleId);
// 				});
// 		// emptyJob('screen1');
		
// 		stopLoading();
// 	} catch (er) {
// 		console.log(' print error '+er)
// 		stopLoading();
// 	}
// }

// // for connect two lines
// function drawLine(loc1, loc2, maps){
// 	var flightPlanCoordinates = [loc1, loc2];
// 	var flightPath = new google.maps.Polyline({
// 		map: maps,
// 		path: flightPlanCoordinates,
// 		strokeColor: '#00b3fd',
// 		strokeOpacity: 0.7,
// 		strokeWeight: 5,
// 		clickable: true
// 	});
	
// }

// function intervalServiceCall(obj, marker, maps, path, screen){
// 	// marker.setMap(null);
// 	try{
// 		vamoservice.getDataCall(getSelectedVehicleLocation+obj.vehicleId).then(function(response){
// 			// console.log(' hi arun nice coding '+obj.vehicleId)
// 			drawLine(new google.maps.LatLng(path[0], path[1]), new google.maps.LatLng(response.latitude,response.longitude), maps);
// 			marker.setPosition(new google.maps.LatLng(response.latitude,response.longitude));
// 			// marker.set({labelContent: ' '});
// 			// marker.set({labelContent:   'Vehicle Name - '+response.shortName+'<br>'+'Speed - '+response.speed+' kms/h'+'<br>'+'odoDistance - '+response.odoDistance+'<br>'+'Address - '+response.address});
// 			marker.labelContent = 'Vehicle Name - '+response.shortName+'<br>'+'Speed - '+response.speed+' kms/h'+'<br>'+'odoDistance - '+response.odoDistance+'<br>'+'Address - '+resolveAddress(response);
// 			marker.setMap(maps)
// 			maps.setCenter(new google.maps.LatLng(response.latitude,response.longitude));
				
// 			// maps.setZoom(13);
// 			// path.push(response.latitude,response.longitude);
// 			switch (screen){
// 				case 'screen1':
// 					path1 =[];
// 					path1.push(response.latitude,response.longitude);
// 					break;
// 				case 'screen2':
// 					path2 =[];
// 					path2.push(response.latitude,response.longitude);
// 					break;
// 				case 'screen3':
// 					path3 =[];
// 					path3.push(response.latitude,response.longitude);
// 					break;
// 				case 'screen4':
// 					path4 =[];
// 					path4.push(response.latitude,response.longitude);
// 					break;
// 				default:
// 					break;
// 			}
// 		})

// 	} catch (err){
// 		console.log(' print err  '+err);
// 	}
	
function joinTwoLine(loc1, loc2, vehicleid){
	// $scope.slatlong = new google.maps.LatLng(lat, lan);
	$scope.polyline = new google.maps.Polyline({
		map: googleMap,
		path: [loc1,loc2],
		strokeColor: '#00b3fd',
		strokeOpacity: 0.7,
		strokeWeight: 5,
		
		clickable: true
	});
	// $scope.elatlong = 	$scope.slatlong;
	// _pathDetails[vehicleid] = pathlist.push($scope.slatlong);
}
	
// }

// setInterval(function() {
// 	// marker4.setMap(null);
// 	if(typeof obj1.vehicleId === 'string')
// 		intervalServiceCall(obj1, marker1, googleMap, path1, 'screen1');
// 	if(typeof obj2 !== 'undefined')
// 		if(typeof obj2.vehicleId === 'string')
// 			intervalServiceCall(obj2,marker2, $scope.screen2, path2, 'screen2');
// 	if(typeof obj3 !== 'undefined')
// 		if(typeof obj3.vehicleId === 'string')
// 			intervalServiceCall(obj3, marker3, $scope.screen3, path3, 'screen3');
// 	if(typeof obj4 !== 'undefined')
// 		if(typeof obj4.vehicleId === 'string')
// 			intervalServiceCall(obj4, marker4, $scope.screen4, path4, 'screen4');
// },10000);
// function markerDrop1(response, screen, marker){
// 	marker.setPosition(new google.maps.LatLng(12.499287, 78.569536));

// 	marker.setMap(screen);
// 	screen.setCenter(new google.maps.LatLng(12.499287, 78.569536));
// 		// marker.set('labelAnchor', (new google.maps.Point(0,0)));
// 		// marker.set('labelClass', "multi");
// 		// marker.set('labelInBackground', false);
// 	marker.setIcon(vamoservice.iconURL(response));
	
// 	_mapsDetails[response.vehicleId] = marker;
// 	stopLoading();
// }

function getMarkerDetails(vId, marker){
	console.log(' inside ')
	// joinLine(getSelectedVehicleLocation+vId, vId);
	vamoservice.getDataCall(getSelectedVehicleLocation+vId, vId).then(function(res){
		marker.labelContent = ''+translate($scope.vehiLabel)+' '+translate('Name')+' - '+res.shortName+'<br>'+translate('Speed')+' - '+res.speed+' '+translate('kms/h')+'<br>'+translate('odoDistance')+' - '+res.odoDistance+'<br>'+translate('Address')+' - '+resolveAddress(res);
// 			marker.setMap(maps)
		// var sp;
		// var latlan = [];
		// $scope.slatlong = {};
		// $scope.elatlong = {};
		// $scope.polyline = {};
		// var value =res.position == 'M'? lineDraw(res.latitude, res.longitude, 'screen1', vId) : false
		// var latLngOld= ["20.345136,74.180436","20.344211,74.18148","20.343227,74.182578","20.342202,74.183724","20.3412,74.184862","20.34022,74.185991","20.339247,74.187102","20.338271,74.188196","20.337422,74.189111","20.336598,74.190031","20.336598,74.190031"]
		// for (var i = 0; i < res.latLngOld.length; i++) {
			// sp = res.latLngOld[i].split(',');
			// if(res.position == 'M'){
				// _mapsDetails[key].position.lat(), _mapsDetails[key].position.lng()
				// marker.position.lat()  marker.position.lng()
				if($scope.map_change==0){
				if(infowindow[res.vehicleId]){

					infowindow[res.vehicleId].setContent('<div><b>'+''+translate($scope.vehiLabel)+' '+translate('Name')+' - '+res.shortName+'<br>'+translate('Speed')+' - '+res.speed+' '+translate('kms/h')+'<br>'+translate('odoDistance')+' - '+res.odoDistance+'<br>'+translate('Address')+' - '+resolveAddress(res)+'</b></div>');
				}
				joinTwoLine(new google.maps.LatLng(marker.position.lat(), marker.position.lng()),  new google.maps.LatLng(res.latitude, res.longitude), vId)
				markerDrop(res, googleMap, marker)
			   }
               else if($scope.map_change==1){
               	//alert(marker.getLatLng());
               	//var latlngs = [marker.getLatLng(),[12.52733,78.20151111]];
               	var latlngs = [marker.getLatLng(),[parseFloat(res.latitude), parseFloat(res.longitude)]];
				var polyline = L.polyline(latlngs, {color: '#00b3fd'}).addTo(maposm);
				markerDrop_osm(res, maposm, marker);
			}

				// console.log(' _mapsDetails[key].position.lat() ')
			// }
			// lineDraw(res.latitude, res.longitude, 'screen1', vId);
		// }
		// selectingScreen('screen1', res, vehicleno)
	})
}

$scope.changeMap=function() {

    if($scope.map_change==0){

        
        document.getElementById("map_osm").style.display  = "none"; 
        document.getElementById("maploc1").style.display  = "block"; 
        preMap=localStorage.getItem('mapNo');
        localStorage.setItem('mapNo',0);
        
		        if($scope.groupSelected!=undefined&&$scope.selectedVehi.length!=0)
		        	{   
				        		if($scope.vehiclelist.length==$scope.selectedVehi.length){
                                 $scope.selectAll('All');
				        	    }
				        		else{
				        			$scope.selectAll('selectedVehi');
				        		}
				        		
		        	}
		        else 
		        {   $scope.initMethod();
			        init();
			        }

    } else if($scope.map_change==1) {

     
         document.getElementById("maploc1").style.display="none"; 
         document.getElementById("map_osm").style.display="block"; 
         preMap=localStorage.getItem('mapNo');
         localStorage.setItem('mapNo',1);
         
          if($scope.groupSelected!=undefined&&$scope.selectedVehi.length!=0)
		        	{
		        		     if($scope.vehiclelist.length==$scope.selectedVehi.length){
                                 $scope.selectAll('All');
				        	    }
				        		else{
				        			$scope.selectAll('selectedVehi');
				        		}
		        	}
		        else
		        {    $scope.initMethod();
			        init();
			        }
}
}

setInterval(function(){
	if($scope.map_change==0) {
     for(key in _mapsDetails){
		getMarkerDetails(key, _mapsDetails[key]);
	}
	}
	else if($scope.map_change==1) {
		for(key in osm_mapsDetails){
		getMarkerDetails(key, osm_mapsDetails[key]);
	}
	}
}, 10000)

}]);

