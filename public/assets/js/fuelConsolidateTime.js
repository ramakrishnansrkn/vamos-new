app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global', function($scope, $http, vamoservice, $filter, GLOBAL){

  //global declaration
  $scope.uiDate         =  {};
  $scope.uiValue        =  {};
  //$scope.sort           =  sortByDate('startTime');
  //$scope.totStartFuel   =  0;
  $scope.totEndFuel     =  0;
  $scope.totFuelCon     =  0;
  $scope.totFuelFill    =  0;
  $scope.totFuelTheft   =  0;
  $scope.totStartKms    =  0;
  $scope.totEndKms      =  0;
  $scope.totDist        =  0;
  $scope.totKmpl        =  0;                                                                     
  $scope.totLtrPerHrs   =  0;
  var licenceExpiry="";
  var strExpired='expired';
  if(localStorage.getItem('licenceExpiry'))licenceExpiry=localStorage.getItem('licenceExpiry');
  $scope.errMsg="";
  $scope.sensorCount   = 1;
  $scope.vehicleMode = "";
  $scope.sensor = 1;
  var tab               =   getParameterByName('tn');
  var assLabel          =   localStorage.getItem('isAssetUser');
  //console.log(assLabel);
  $scope.sort           =   sortByDate('date',true);
  
  if( assLabel == "true" ) {
    $scope.vehiLabel  =  "Asset";
    $scope.vehiImage  =  true;
  } else if( assLabel == "false" ) {
    $scope.vehiLabel  =  "Vehicle";
    $scope.vehiImage  =  false;
  } else {
    $scope.vehiLabel  =  "Vehicle";
    $scope.vehiImage  =  false;
  }
  var expiryDays='';
  
  function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  //global declartion
  $scope.locations  =  [];
  $scope.url        =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
  $scope.gIndex     =  0;

  //$scope.locations01 = vamoservice.getDataCall($scope.url);
  $scope.trimColon = function(textVal){
    return textVal.split(":")[0].trim();
  }

  $scope.range = function(min, max, step) {
    step = step || 1;
    var input = [];
    for (var i = min; i <= max; i += step) {
      input.push(i);
    }
    return input;
  };

  function sessionValue(vid, gname,licenceExpiry){
    localStorage.setItem('user', JSON.stringify(vid+','+gname));
    localStorage.setItem('licenceExpiry', licenceExpiry);
    $("#testLoad").load("../public/menu");
  }

  $scope.ltphValue = function(ms) {

    hours       =  Math.floor((ms) / (60 * 60 * 1000));
    hoursms     =  ms % (60 * 60 * 1000);
    minutes     =  Math.floor((hoursms) / (60 * 1000));

    var retVal  =  hours+"."+minutes;

        //alert(retVal);

        return parseFloat(retVal);   
      }
      

      $scope.roundOffDecimal = function(val) {
       if(val){
         return parseFloat(val).toFixed(2);
       }else{
         return val;
       }
     }

     $scope.parseInts = function(val) {
      return parseInt(val);   
    }

    $scope.totalKmpl = function(dist,fuel) {
      var totKmpl = dist/fuel;
      totKmpl     = totKmpl.toFixed(2);
      if(totKmpl=="NaN"||totKmpl==Infinity)totKmpl=0.0;
      return totKmpl;   
    }

    $scope.totalLtph = function(fuel,engHrs) { 
      var totLtph = fuel/$scope.ltphValue(engHrs);

      //console.log( fuel );
       //console.log( $scope.ltphValue(engHrs) );
       
       totLtph  = totLtph.toFixed(2);
       if(totLtph=="NaN"||totLtph==Infinity)totLtph=0.0;

       return totLtph;   
     }

     function convert_to_24h(time_str) {
    //console.log(time_str);
    var str       =  time_str.split(' ');
    var stradd    =  str[0].concat(":00");
    var strAMPM   =  stradd.concat(' '+str[1]);
    var time      =  strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
    var hours     =  Number(time[1]);
    var minutes   =  Number(time[2]);
    var seconds   =  Number(time[2]);
    var meridian  =  time[4].toLowerCase();
    
    if (meridian == 'p' && hours < 12) {
      hours = hours + 12;
    }
    else if (meridian == 'a' && hours == 12) {
      hours = hours - 12;
    }     
    var marktimestr =''+hours+':'+minutes+':'+seconds;      
    return marktimestr;
  };

  $scope.msToTime2 = function(ms) {

    days       =  Math.floor(ms / (24*60*60*1000));
    daysms     =  ms % (24*60*60*1000);
    hours      =  Math.floor((ms)/(60*60*1000));
    hoursms    =  ms % (60*60*1000);
    minutes    =  Math.floor((hoursms)/(60*1000));
    minutesms  =  ms % (60*1000);
    sec        =  Math.floor((minutesms)/(1000));
    
    hours = (hours<10)?"0"+hours:hours;
    minutes = (minutes<10)?"0"+minutes:minutes;
    sec = (sec<10)?"0"+sec:sec; 
      //if(days>=1) {
      //  return (days-1)+"d : "+23+" : "+minutes;
       // } else {
        return hours+" : "+minutes+" : "+sec;
      //}
    }

    // millesec to day, hours, min, sec
    $scope.msToTime = function(ms) {

      days       =  Math.floor(ms / (24 * 60 * 60 * 1000));
      daysms     =  ms % (24 * 60 * 60 * 1000);
      hours      =  Math.floor((ms) / (60 * 60 * 1000));
      hoursms    =  ms % (60 * 60 * 1000);
      minutes    =  Math.floor((hoursms) / (60 * 1000));
      minutesms  =  ms % (60 * 1000);
      seconds    =  Math.floor((minutesms) / 1000);
      //if(days==0)
      //return hours +" h "+minutes+" m "+seconds+" s ";
      //else
      return hours +":"+minutes+":"+seconds;
    }
    
    var delayed4 = (function () {
      var queue = [];

      function processQueue() {
        if (queue.length > 0) {
          setTimeout(function () {
            queue.shift().cb();
            processQueue();
          }, queue[0].delay);
        }
      }

      return function delayed(delay, cb) {
        queue.push({ delay: delay, cb: cb });

        if (queue.length === 1) {
          processQueue();
        }
      };
    }());

    function google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent) {
      vamoservice.getDataCall(tempurlEvent).then(function(data) {
        $scope.addressEvent[index4] = data.results[0].formatted_address;
      //console.log(' address '+$scope.addressEvent[index4])
      //var t = vamo_sysservice.geocodeToserver(latEvent,lonEvent,data.results[0].formatted_address);
    })
    };

    $scope.recursiveEvent   =   function(locationEvent, indexEvent) {
      var index4 = 0;
      angular.forEach(locationEvent, function(value ,primaryKey){
      //console.log(' primaryKey '+primaryKey)
      index4 = primaryKey;
      if(locationEvent[index4].address == undefined)
      {
        var latEvent     =   locationEvent[index4].latitude;
        var lonEvent     =   locationEvent[index4].longitude;
        var tempurlEvent   =   "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latEvent+','+lonEvent+"&sensor=true";

        delayed4(2000, function (index4) {
          return function () {
            google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent);
          };
        }(index4));
      }
    })
    }


    function formatAMPM(date) {
      var date = new Date(date);
      var hours = date.getHours();
      var minutes = date.getMinutes();
      var ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
      return strTime;
    }

  //$scope.orgName = '';

  // function totalValues(datavalue) {
  //           var val=datavalue;
  //           var startFuel        =  0.0;
  //           var endFuel          =  0.0;
  //           var fuelConsumption  =  0.0;
  //           var fuelFilling      =  0.0;
  //           var fuelTheft        =  0.0;
  //           var startKms         =  0.0;
  //           var endKms           =  0.0;
  //           var dist             =  0.0;
  //           var kmpl             =  0.0;    
  //           var ltrsPerHrs       =  0.0;

  //           var engineIdleHrs    =  0.0;
  //           var ignitionHrs      =  0;
  //           var engineHrs        =  0;


  //          // angular.forEach($scope.fuelConData , function(val, key) {

  //             //console.log(val);
  //               engineIdleHrs    =  engineIdleHrs+parseInt(val.engineIdleHrs);
  //               ignitionHrs      =  ignitionHrs+parseInt(val.ignitionHrs);
  //               engineHrs        =  engineHrs+parseInt(val.engineRunningHrs);

  //             //console.log(engineIdleHrs);
  
  //               startFuel        =  startFuel+parseFloat(val.startFuel);
  //               endFuel          =  endFuel+parseFloat(val.endFuel);
  //               fuelConsumption  =  fuelConsumption+parseFloat(val.fuelConsumption);
  //               fuelFilling      =  fuelFilling+parseFloat(val.fuelFilling);
  //               fuelTheft        =  fuelTheft+parseFloat(val.fuelTheft);
  //               startKms         =  startKms+parseFloat(val.startKms);
  //               endKms           =  endKms+parseFloat(val.endKms);
  //               dist             =  dist+parseFloat(val.dist);
  //               kmpl             =  kmpl+parseFloat(val.kmpl);     
  //               ltrsPerHrs       =  ltrsPerHrs+parseFloat(val.ltrsPerHrs);
  
  

  //               $scope.startFuel        =  startFuel;
  //               $scope.endFuel          =  endFuel;
  //               $scope.fuelConsumption  =  fuelConsumption;
  //               $scope.fuelFilling      =  fuelFilling;
  //               $scope.fuelTheft        =  fuelTheft;
  //               $scope.startKms         =  startKms;
  //               $scope.endKms           =  endKms;
  //               $scope.dist             =  dist;
  //               $scope.kmpl             =  kmpl.toFixed(2);     
  //               $scope.ltrsPerHrs       =  ltrsPerHrs.toFixed(2);

  //               $scope.engineIdleHrs    =  engineIdleHrs;
  //               $scope.ignitionHrs      =  ignitionHrs;
  //               $scope.engineHrs        =  engineHrs;

  //               console.log( 'Engine Hours....  :  ' + $scope.engineHrs );
  //             //console.log(  $scope.startFuel  );
  // }

  function setBtnEnable(btnName){
   $scope.yesterdayDisabled = false;
   $scope.todayDisabled = false;

   switch(btnName){

    case 'yesterday':
    $scope.yesterdayDisabled = true;
    break;
    case 'today':
    $scope.todayDisabled = true;
    break;
  }


}
setBtnEnable("init");
$scope.durationFilter    =   function(duration){
  //alert('inside function');
  startLoading();
  var now = new Date();
  $scope.uiDate.todate      = getTodayDate(now.setDate(now.getDate() - 1));
  switch(duration){

    case 'yesterday':
    setBtnEnable('yesterday');
    var d = new Date();
    $scope.uiDate.fromdate= getTodayDate(d.setDate(d.getDate() - 1));
    $scope.uiDate.totime  = '11:59 PM';
      // datechange();
      break;
      case 'today':
      setBtnEnable('today');
      var d = new Date();
      $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() ));
      $scope.uiDate.todate       = getTodayDate(d.setDate(d.getDate() ));
      $scope.uiDate.totime       = formatAMPM(d);
      //datechange();
      break;
    }
    webCall();
  }

  function findMidptBetLatlng(lat1,lon1,lat2,lon2){
    // var dLon = toRad(lon2 - lon1),

    // //convert to radians
    // lat1 = toRad(lat1),
    // lat2 = toRad(lat2),
    // lon1 = toRad(lon1),

    // Bx = Math.cos(lat2) * Math.cos(dLon),
    // By = Math.cos(lat2) * Math.sin(dLon),
    // lat3 = Math.atan2(Math.sin(lat1) + Math.sin(lat2), Math.sqrt((Math.cos(lat1) + Bx) * (Math.cos(lat1) + Bx) + By * By)),
    // lon3 = lon1 + Math.atan2(By, Math.cos(lat1) + Bx);
    // return new google.maps.LatLng(lat3,lon3);
    var lat3=(parseFloat(lat1)+parseFloat(lat2))/2,
    lon3=(parseFloat(lon1)+parseFloat(lon2))/2;
    return new google.maps.LatLng(lat3,lon3);
  }

  function toRad(Value) {
    /** Converts numeric degrees to radians */
    return Value * Math.PI / 180;
  }

  function webCall() {
    $scope.errMsg='';
    $scope.showErrMsg = false;
      //alert('hello');
      if((checkXssProtection($scope.uiDate.fromdate) == true) && ((checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true))) {
           //var tollUrl = GLOBAL.DOMAIN_NAME+'/getTollgateReport?group='+$scope.gName+'&fromTimeUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toTimeUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
             //var fuelConUrl  =  GLOBAL.DOMAIN_NAME+'/getConsolidatedFuelReport?groupName='+$scope.gName+'&date='+$scope.uiDate.fromdate;
        //}
        //var fuelConUrl  =  'http://188.166.244.126:9000/getConsolidatedFuelReport?userId=naplogi&groupName=NAPLOGI:SMP&date=2018-07-05';
        //var fuelConUrl  =  GLOBAL.DOMAIN_NAME+'/getConsolidatedFuelReport?groupName='+$scope.gName+'&date='+$scope.uiDate.fromdate;
        //var fuelConUrl  =  'http://188.166.244.126:9000/getConsolidatedFuelReportBasedOnVehicle?userId=naplogi&vehicleId=NAPLOGI-AP16T53666&fromDate=2018-08-25&toDate=2018-08-29';
        if(utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime)) - utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime)) <= 259200000 || $scope.sensor!='All'){

          var fuelConUrl  =  "";
          if($scope.sensorCount>1){
           if($scope.sensor=='All'){
            fuelConUrl  =  GLOBAL.DOMAIN_NAME+'/getConsolidatedFuelReportBasedOnTime?vehicleId='+$scope.vehIds+'&fromDateUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime))+"&sensor=&allSensor=true";
          }else{
            fuelConUrl  =  GLOBAL.DOMAIN_NAME+'/getConsolidatedFuelReportBasedOnTime?vehicleId='+$scope.vehIds+'&fromDateUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime))+"&sensor="+$scope.sensor;
          }
          
        }else{
          fuelConUrl  =  GLOBAL.DOMAIN_NAME+'/getConsolidatedFuelReportBasedOnTime?vehicleId='+$scope.vehIds+'&fromDateUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));

        }

        console.log( fuelConUrl );

        $scope.fuelConData = [];
        var expdate=moment().add(expiryDays,'days').format('DD-MM-YYYY'),
        convertedexpdate=utcFormat(expdate,convert_to_24h('11:59 PM')),
        convertedtodate=utcFormat($scope.uiDate.todate,convert_to_24h('11:59 PM')),
        convertedfromdate=utcFormat($scope.uiDate.fromdate,convert_to_24h('11:59 PM'));
        if(convertedtodate<=convertedexpdate && convertedfromdate<=convertedexpdate){
          startLoading();
          $http.get(fuelConUrl).success(function(data) {
            stopLoading();
            if($scope.sensor==1&&document.getElementById("sensor1")){
              document.getElementById("sensor1").disabled = true;
            }
            if(data.startLoc!=null && data.startLoc!=undefined && data.startLoc!='-'){
              var marker=findMidptBetLatlng(data.startLoc.split(',')[0],data.startLoc.split(',')[1],data.endLoc.split(',')[0],data.endLoc.split(',')[1]);
              initializeMap(marker);
            }
            $scope.vehiMode=data.vehicleMode;
            $scope.vehiModel=data.vehicleModel;
            $scope.fuelConData  =  data;
            console.log($scope.fuelConData);

                    //totalValues($scope.fuelConData );
                    //$scope.orgName  =  $scope.fuelConData[0].orgId;

                    stopLoading();
                  }); 
        }else{
          stopLoading();
          $scope.errMsg=licenceExpiry;
          $scope.showErrMsg = true;
        } 
      }else {
        stopLoading();
        $scope.errMsg='Please select date range within 3 days.';
        $scope.showErrMsg = true;
      }
    }
  }

  //get the value from the ui
  function getUiValue() {
    $scope.uiDate.fromdate   =  $('#dateFrom').val();
    $scope.uiDate.fromtime   =  $('#timeFrom').val();
    $scope.uiDate.todate     =  $('#dateTo').val();
    $scope.uiDate.totime     =  $('#timeTo').val();
    if(localStorage.getItem('timeTochange')!='yes'){
     updateToTime();
     $scope.uiDate.totime    =   localStorage.getItem('toTime');
   }
   
 }
 

// service call for the event report

/*function webServiceCall(){
    $scope.siteData = [];
    if((checkXssProtection($scope.uiDate.fromdate) == true) && (checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true)) {
      
      var url   = GLOBAL.DOMAIN_NAME+"/getActionReport?vehicleId="+$scope.vehiname+"&fromDate="+$scope.uiDate.fromdate+"&fromTime="+convert_to_24h($scope.uiDate.fromtime)+"&toDate="+$scope.uiDate.todate+"&toTime="+convert_to_24h($scope.uiDate.totime)+"&interval="+$scope.interval+"&stoppage="+$scope.uiValue.stop+"&stopMints="+$scope.uiValue.stopmins+"&idle="+$scope.uiValue.idle+"&idleMints="+$scope.uiValue.idlemins+"&notReachable="+$scope.uiValue.notreach+"&notReachableMints="+$scope.uiValue.notreachmins+"&overspeed="+$scope.uiValue.speed+"&speed="+$scope.uiValue.speedkms+"&location="+$scope.uiValue.locat+"&site="+$scope.uiValue.site+'&fromDateUTC='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateUTC='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
      vamoservice.getDataCall(url).then(function(responseVal){
        $scope.recursiveEvent(responseVal, 0);
        $scope.eventData = responseVal;
        var entry=0,exit=0; 
        angular.forEach(responseVal, function(val, key){
          if(val.state == 'SiteExit')
            exit++ 
          else if (val.state == 'SiteEntry')
            entry++
        })
        $scope.siteEntry  = entry;
        $scope.siteExit   = exit;
        stopLoading();
      });
    }
    stopLoading();
  }*/

  // initial method
  $scope.total = function(val1, val2){
    return parseFloat(val1)+parseFloat(val2);
  }
  $scope.$watch("url", function (val) {
    vamoservice.getDataCall($scope.url).then(function(data) {          
          //startLoading();
          $scope.selectVehiData  =  [];
          $scope.vehicle_group   =  [];
          $scope.vehicle_list    =  data;

          console.log($scope.vehicle_list);

          if(data.length) {

            $scope.vehiname  =  getParameterByName('vid');
            $scope.uiGroup   =  $scope.trimColon(getParameterByName('vg'));
            $scope.gName   =  getParameterByName('vg');
        //console.log(data);
        //$scope.gName   =  data[0].group;
          //console.log(data[0].group);

          if($scope.vehiname == 'undefined' || $scope.vehiname == '') {
           $scope.vehiname  =  data[$scope.gIndex].vehicleLocations[0].vehicleId; 
           licenceExpiry=data[$scope.gIndex].vehicleLocations[0].licenceExpiry;
           $scope.sensorCount   = data[$scope.gIndex].vehicleLocations[0].sensorCount;
           $scope.vehicleMode=data[$scope.gIndex].vehicleLocations[0].vehicleMode;
           $scope.vehicleModel=data[$scope.gIndex].vehicleLocations[0].vehicleModel;
           expiryDays=data[$scope.gIndex].vehicleLocations[0].expiryDays;
             //alert($scope.vehiname );
           } 

           if($scope.gName == 'undefined' || $scope.gName == ''){
             $scope.gName  =  data[$scope.gIndex].group; 
           } 
           
           angular.forEach(data, function(val, key) {
              //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
              if($scope.gName == val.group) {
                $scope.gIndex = val.rowId;
                
                angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys){

                  $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

                  if($scope.vehiname == value.vehicleId){
                    $scope.shortNam  =  value.shortName;
                    $scope.vehIds   = value.vehicleId;
                    licenceExpiry = value.licenceExpiry;
                    $scope.sensorCount   = value.sensorCount;
                    $scope.vehicleMode   = value.vehicleMode;
                    $scope.vehicleModel   = value.vehicleModel;
                    expiryDays = value.expiryDays;
                  }
                });
              }
            });
        //console.log($scope.selectVehiData);
        sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
      }

      var dateObj1      =   new Date();
      var  dateNow        = new Date(dateObj1.setDate(dateObj1.getDate()));
      var fromdatenow         = getTodayDate(dateNow);
      if(fromdatenow==localStorage.getItem('fromDate'))
      {
        var dateObj       =   new Date();
        var dateObj1       =   new Date();
        $scope.fromNowTS    = new Date(dateObj.setDate(dateObj.getDate()-1));
        $scope.uiDate.fromdate    = getTodayDate($scope.fromNowTS);
        $scope.uiDate.fromtime    = '7:00 AM';
        $scope.fromNowTS1    = new Date(dateObj1.setDate(dateObj1.getDate()))
        $scope.uiDate.todate    = getTodayDate($scope.fromNowTS1);
            //$scope.uiDate.totime    = formatAMPM($scope.fromNowTS.getTime());
            $scope.uiDate.totime    =   '7:00 AM';
            if(localStorage.getItem('timeTochange')!='yes'){
              updateToTime();
              $scope.uiDate.totime    =   '7:00 AM';
            }
                  //alert($scope.uiDate.totime);
                }else{

                  $scope.uiDate.fromdate    = localStorage.getItem('fromDate');
                  $scope.uiDate.fromtime    = localStorage.getItem('fromTime');
                  $scope.uiDate.todate    = localStorage.getItem('toDate');
                  $scope.uiDate.totime    =   localStorage.getItem('toTime');
                  if(localStorage.getItem('timeTochange')!='yes'){
                    updateToTime();
                    $scope.uiDate.totime    =   localStorage.getItem('toTime');
                  }
                  $scope.uiDate.fromtimes  =  convert_to_24h($scope.uiDate.fromtime);
                  $scope.uiDate.totimes    =  convert_to_24h($scope.uiDate.totime);
                  //alert($scope.uiDate.totime);
                }

            // var dateObj1       =   new Date();
            // var  dateNow        = new Date(dateObj1.setDate(dateObj1.getDate()));
            // var fromdatenow         = getTodayDate(dateNow);
            // //alert(fromdatenow);
            //   if(fromdatenow==localStorage.getItem('fromDate'))
            //   {
            //       var dateObj       =   new Date();
            //       $scope.fromNowTS    = new Date(dateObj.setDate(dateObj.getDate()));
            //       $scope.uiDate.fromdate    = getTodayDate($scope.fromNowTS);
            //       $scope.uiDate.todate    = getTodayDate($scope.fromNowTS);
            //   }else{

            //       $scope.uiDate.fromdate    = localStorage.getItem('fromDate');
            //       $scope.uiDate.todate      = localStorage.getItem('toDate');
            //        }


      // $scope.uiDate.fromdate    = localStorage.getItem('fromDate');
      // $scope.uiDate.fromtime    = localStorage.getItem('fromTime');
      // $scope.uiDate.todate      = localStorage.getItem('toDate');
      // $scope.uiDate.totime      = localStorage.getItem('toTime');
      // if(localStorage.getItem('timeTochange')!='yes'){
      //           updateToTime();
      //           $scope.uiDate.totime    =   localStorage.getItem('toTime');
      //           }
      // $scope.uiDate.fromtimes  =  convert_to_24h($scope.uiDate.fromtime);
      // $scope.uiDate.totimes    =  convert_to_24h($scope.uiDate.totime);
      //webServiceCall();
      startLoading();
      webCall();
      //stopLoading();
    }); 
});
var map;
$scope.showMap=function(fueldata,type) {
  var stmarker=new google.maps.LatLng(fueldata.startLoc.split(',')[0], fueldata.startLoc.split(',')[1]),
  endmarker=new google.maps.LatLng(fueldata.endLoc.split(',')[0],fueldata.endLoc.split(',')[1]);
  new google.maps.Marker({
    position: stmarker,
    map: map,
    icon: 'assets/imgs/startflag.png'
  });
  new google.maps.Marker({
    position: endmarker,
    map: map,
    icon: 'assets/imgs/endflag.png'
  });
  new google.maps.Polyline({
    path: [stmarker,endmarker],
    geodesic: true,
    strokeColor: "#00c4ff",
    strokeOpacity: 0.7,
    strokeWeight: 5,
    map: map,
  });
  $('#mapmodals').on('shown.bs.modal', function () {
    google.maps.event.trigger(map, "resize");
  }); 
}

initializeMap=function(marker){
  mapOptions = {
    zoom: 7, 
    zoomControlOptions: { position: google.maps.ControlPosition.LEFT_TOP },
    center: marker,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById("map_canvas"),mapOptions);
}

$scope.groupSelection = function(groupName, groupId) {
  startLoading();
  licenceExpiry="";
  $scope.errMsg="";
  $scope.gName  =  groupName;
  $scope.uiGroup  =  $scope.trimColon(groupName);
  $scope.gIndex =  groupId;
  var url     =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+groupName;

  vamoservice.getDataCall(url).then(function(response) {

    $scope.vehicle_list   =  response;
    $scope.shortNam       =  response[$scope.gIndex].vehicleLocations[0].shortName;
    $scope.vehiname       =  response[$scope.gIndex].vehicleLocations[0].vehicleId;
    licenceExpiry         =  response[$scope.gIndex].vehicleLocations[0].licenceExpiry;
    $scope.sensorCount    =  response[$scope.gIndex].vehicleLocations[0].sensorCount;
    $scope.vehicleMode    =  response[$scope.gIndex].vehicleLocations[0].vehicleMode;
    $scope.vehicleModel    =  response[$scope.gIndex].vehicleLocations[0].vehicleModel;
    expiryDays         =  response[$scope.gIndex].vehicleLocations[0].expiryDays;
    sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
    $scope.selectVehiData   =  [];
            //console.log(response);
            angular.forEach(response, function(val, key) {
              if($scope.gName == val.group){
          //  $scope.gIndex = val.rowId;
          angular.forEach(response[$scope.gIndex].vehicleLocations, function(value, keys) {

            $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

            if($scope.vehiname == value.vehicleId){
              $scope.shortNam  =  value.shortName;
              $scope.vehIds   = value.vehicleId;
            }
          });
        }
      });

            getUiValue();
            webCall();
            
      //webServiceCall();
        //stopLoading();
      });

}


$scope.genericFunction  = function (vehid, index,shortname,position, address,groupName,licenceExp){
 startLoading();
 licenceExpiry=licenceExp;
 $scope.errMsg="";
 $scope.vehiname    = vehid;
 sessionValue($scope.vehiname, $scope.gName,licenceExpiry)
 angular.forEach($scope.vehicle_list[$scope.gIndex].vehicleLocations, function(val, key){
  if(vehid == val.vehicleId) {
    $scope.shortNam = val.shortName;
    $scope.vehIds   = val.vehicleId;
    $scope.sensorCount   = val.sensorCount;
    $scope.vehicleMode   = val.vehicleMode; 
    $scope.vehicleModel   = val.vehicleModel; 
    expiryDays=val.expiryDays;
  }
});
 
 getUiValue();
 webCall();
   //webServiceCall();
 }

 $scope.sensorChange   = function(sensorNo){
  console.log("sensor  " + sensorNo);
  for (var i = 1; i <= $scope.sensorCount ; i++) {
    if(i==sensorNo){
     document.getElementById("sensor"+i).disabled = true;
   }else{
     document.getElementById("sensor"+i).disabled = false;
   }
 }
 if(sensorNo=='All'){
  document.getElementById("sensorAll").disabled = true;
}else{
  document.getElementById("sensorAll").disabled = false;
}
$scope.sensor=sensorNo;
webCall();
}


$scope.submitFunction   = function(){
  $scope.yesterdayDisabled = false;
  $scope.weekDisabled = false;
  $scope.monthDisabled = false;
  startLoading();
  getUiValue();
  webCall();
  //webServiceCall();
    //stopLoading();
  }

  $scope.exportData = function (data) {
    //console.log(data);
    var blob = new Blob([document.getElementById(data).innerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, data+".xls");
  };

  $scope.exportDataCSV = function (data) {
    //console.log(data);
    CSV.begin('#'+data).download(data+'.csv').go();
  };

  $('#minus').click(function(){
    $('#menu').toggle(1000);
  })

}]);
