app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '$translate', '_global', function($scope, $http, vamoservice, $filter ,$translate,GLOBAL){
	$scope.videoLink="https://www.youtube.com/watch?v=LOhxGEkuzuA&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=1";
  //global declaration
  $scope.uiDate 	 =	{};
  $scope.uiValue	 = 	{};
  var language=localStorage.getItem('lang');
  $translate.use(language);
  var translate = $filter('translate');
  //$scope.sort      =  sortByDate('startTime');

   // $scope.totStartFuel  = 0;
   $scope.totEndFuel    = 0;
   $scope.totFuelCon    = 0;
   $scope.totFuelFill   = 0;
   $scope.totFuelTheft  = 0;
   $scope.totStartKms   = 0;
   $scope.totEndKms = 0;
   $scope.totDist = 0;
   $scope.totKmpl  = 0;
	// $scope.vehicleMode;
   $scope.totLtrPerHrs = 0;
   $scope.isTankBased   = false;
   $scope.groupHasMulSensor  = false;
   $scope.showDelta = true;
   $scope.showOrgTime=false;
   $scope.showOrganization=true;
   $scope.tapchanged=true;
   var assLabel          =   localStorage.getItem('isAssetUser');
  //console.log(assLabel);
  $scope.sort           =   sortByDate('vehicleName');

  if( assLabel == "true" ) {
  	$scope.vehiLabel  =  "Asset";
  	$scope.vehiImage  =  true;
  } else if( assLabel == "false" ) {
  	$scope.vehiLabel  =  "Vehicle";
  	$scope.vehiImage  =  false;
  } else {
  	$scope.vehiLabel  =  "Vehicle";
  	$scope.vehiImage  =  false;
  }

  function getParameterByName(name) {
  	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
  	results = regex.exec(location.search);
  	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  //global declartion
  $scope.locations  =  [];
  $scope.url        =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
  $scope.gIndex     =  0;

  //$scope.locations01 = vamoservice.getDataCall($scope.url);
  $scope.trimColon = function(textVal){
  	return textVal.split(":")[0].trim();
  }

  function sessionValue(vid, gname){
  	localStorage.setItem('user', JSON.stringify(vid+','+gname));
  	$("#testLoad").load("../public/menu");
  }

  $scope.ltphValue = function(ms) {

  	hours       =  Math.floor((ms) / (60 * 60 * 1000));
  	hoursms     =  ms % (60 * 60 * 1000);
  	minutes     =  Math.floor((hoursms) / (60 * 1000));

  	var retVal  =  hours+"."+minutes;

        //alert(retVal);

        return parseFloat(retVal);   
    }


    $scope.roundOffDecimal = function(val) {
    	return parseFloat(val).toFixed(2);
    }

    $scope.parseInts = function(val) {
    	return parseInt(val);   
    }
	
	$scope.totalkmpl = function(dist1,fuelcon1) {
		// debugger
    	var totKmpl = dist1/fuelcon1;
    	totKmpl = totKmpl.toFixed(2);
    	if(totKmpl=="NaN"||totKmpl==Infinity)totKmpl=0.0;
    	return totKmpl;   
    }


    $scope.totalLtph = function(fuelcon2,ignitionHrs1) { 
    	var totLtph = fuelcon2/$scope.ltphValue(ignitionHrs1);
        console.log("egni",$scope.ltphValue(ignitionHrs1))
    	totLtph  = totLtph.toFixed(2);
    	if(totLtph=="NaN"||totLtph==Infinity)totLtph=0.0;
    	return totLtph;   
    }

    function convert_to_24h(time_str) {
	  //console.log(time_str);
	  var str		  =	 time_str.split(' ');
	  var stradd	  =	 str[0].concat(":00");
	  var strAMPM	  =	 stradd.concat(' '+str[1]);
	  var time      =  strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
	  var hours     =  Number(time[1]);
	  var minutes   =  Number(time[2]);
	  var seconds   =  Number(time[2]);
	  var meridian  =  time[4].toLowerCase();

	  if (meridian == 'p' && hours < 12) {
	  	hours = hours + 12;
	  }
	  else if (meridian == 'a' && hours == 12) {
	  	hours = hours - 12;
	  }	    
	  var marktimestr	=''+hours+':'+minutes+':'+seconds;	    
	  return marktimestr;
	};

	$scope.msToTime2 = function(ms) {

		days       =  Math.floor(ms / (24*60*60*1000));
		daysms     =  ms % (24*60*60*1000);
		hours      =  Math.floor((ms)/(60*60*1000));
		hoursms    =  ms % (60*60*1000);
		minutes    =  Math.floor((hoursms)/(60*1000));
		minutesms  =  ms % (60*1000);
		sec        =  Math.floor((minutesms)/(1000));
		hours = (hours<10)?"0"+hours:hours;
		minutes = (minutes<10)?"0"+minutes:minutes;
		sec = (sec<10)?"0"+sec:sec;

	   // if(days>=1) {
	     // return days+"d : "+hours+" : "+minutes;
      //  } else {
      	return hours+" : "+minutes+" : "+sec;
	   // }
	}

    // millesec to day, hours, min, sec
    $scope.msToTime = function(ms) {

    	days = Math.floor(ms / (24 * 60 * 60 * 1000));
    	daysms = ms % (24 * 60 * 60 * 1000);
    	hours = Math.floor((ms) / (60 * 60 * 1000));
    	hoursms = ms % (60 * 60 * 1000);
    	minutes = Math.floor((hoursms) / (60 * 1000));
    	minutesms = ms % (60 * 1000);
    	seconds = Math.floor((minutesms) / 1000);
		// if(days==0)
		// 	return hours +" h "+minutes+" m "+seconds+" s ";
		// else
		return hours +":"+minutes+":"+seconds;
	}

	var delayed4 = (function () {
		var queue = [];

		function processQueue() {
			if (queue.length > 0) {
				setTimeout(function () {
					queue.shift().cb();
					processQueue();
				}, queue[0].delay);
			}
		}

		return function delayed(delay, cb) {
			queue.push({ delay: delay, cb: cb });

			if (queue.length === 1) {
				processQueue();
			}
		};
	}());

	function google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent) {
		vamoservice.getDataCall(tempurlEvent).then(function(data) {
			$scope.addressEvent[index4] = data.results[0].formatted_address;
			//console.log(' address '+$scope.addressEvent[index4])
			//var t = vamo_sysservice.geocodeToserver(latEvent,lonEvent,data.results[0].formatted_address);
		})
	};

	$scope.recursiveEvent 	= 	function(locationEvent, indexEvent)	{
		var index4 = 0;
		angular.forEach(locationEvent, function(value ,primaryKey){
			//console.log(' primaryKey '+primaryKey)
			index4 = primaryKey;
			if(locationEvent[index4].address == undefined)
			{
				var latEvent		 =	locationEvent[index4].latitude;
				var lonEvent		 =	locationEvent[index4].longitude;
				var tempurlEvent     =	"https://maps.googleapis.com/maps/api/geocode/json?latlng="+latEvent+','+lonEvent+"&sensor=true";

				delayed4(2000, function (index4) {
					return function () {
						google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent);
					};
				}(index4));
			}
		})
	}


	function formatAMPM(date) {
		var date = new Date(date);
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? 'PM' : 'AM';
		hours = hours % 12;
		  hours = hours ? hours : 12; // the hour '0' should be '12'
		  minutes = minutes < 10 ? '0'+minutes : minutes;
		  var strTime = hours + ':' + minutes + ' ' + ampm;
		  return strTime;
		}

		$scope.orgName = '';


		function totalValues(data) {

			var startFuel        =  0.0;
			var endFuel          =  0.0;
			var fuelConsumption  =  0.0;
			var fuelcon1         =  0.0;
			var fuelcon2         =  0.0;
			var fuelFilling      =  0.0;
			var fuelTheft        =  0.0;
			var startKms         =  0.0;
			var endKms           =  0.0;
			var dist             =  0.0;
			var dist1            =  0.0;
			var kmpl             =  0.0; 
			
			var ltrsPerHrs       =  0.0;

			var engineIdleHrs    =  0;
			var ignitionHrs      =  0;
			var ignitionHrs1     =  0;
			var secIgnitionHrs   =  0;
			var engineHrs        =  0;
			var fuelBalance      =  0;


			// var dist1=0;
			angular.forEach(data, function(val, key) {
            // console.log(val);
                // startFuel        =  startFuel+parseFloat(val.startFuel);
                // endFuel          =  endFuel+parseFloat(val.endFuel);
                // fuelConsumption  =  fuelConsumption+parseFloat(val.fuelConsumption);
                // fuelFilling      =  fuelFilling+parseFloat(val.fuelFilling);
                // fuelTheft        =  fuelTheft+parseFloat(val.fuelTheft);
                startKms         =  startKms+parseFloat(val.startKms);
                endKms           =  endKms+parseFloat(val.endKms);
                dist             =  dist+parseFloat(val.dist);
				console.log(val.vehicleMode)
				if(val.vehicleMode != 'DG' && val.vehicleMode != 'Machinery' && val.sensor ==1){
				dist1            =  dist1+parseFloat(val.dist);
				}
				if(val.vehicleMode != 'DG' && val.vehicleMode != 'Machinery' && val.sensor ==1){
				fuelcon1         =  fuelcon1+parseFloat(val.fuelConsumption);
				}
				console.log(val.vehicleMode);
                ltrsPerHrs       =  ltrsPerHrs+parseFloat(val.ltrsPerHrs);
				

                engineIdleHrs    =  engineIdleHrs+parseInt(val.engineIdleHrs);
                ignitionHrs      =  ignitionHrs+parseInt(val.ignitionHrs);
				console.log(ignitionHrs)
				if(val.vehicleMode != 'Dispenser' && val.vehicleMode != 'Moving Vehicle' && val.sensor ==1){
				ignitionHrs1     =  ignitionHrs1+parseInt(val.ignitionHrs);
				}
				if(val.vehicleMode != 'Dispenser' && val.vehicleMode != 'Moving Vehicle' && val.sensor ==1){
					fuelcon2         =  fuelcon2+parseFloat(val.fuelConsumption);
					}
				// console.log("ignition",ignitionHrs1);
                secIgnitionHrs   =  secIgnitionHrs+parseInt(val.secondaryEngineDuration);
                engineHrs        =  engineHrs+parseInt(val.engineRunningHrs);
                var fuelsensors = [];
                if($scope.isTankBased){
                	var tankData = val.tankData;
                	if(parseInt(val.sensor)<parseInt(val.nTanks)){
                		tankData = val.fuelsensors;
                		val.nTanks = val.sensor
                	}
                	angular.forEach(tankData, function(fuel, index) {
                		if(val.tankData){
                			startFuel        =  startFuel+parseFloat(fuel.startFuel);
                			endFuel          =  endFuel+parseFloat(fuel.endFuel);
                			fuelConsumption  =  fuelConsumption+parseFloat(fuel.fuelConsumption);
							// if(val.vehicleMode != 'DG' && val.vehicleMode != 'Machinery' && val.sensor ==1){
							// fuelcon1         =  fuelConsumption+parseFloat(fuel.fuelConsumption)
                			fuelFilling      =  fuelFilling+parseFloat(fuel.fuelFilling);
                			fuelTheft        =  fuelTheft+parseFloat(fuel.fuelTheft);
                			kmpl             =  kmpl+parseFloat(fuel.kmpl);   
                			ltrsPerHrs       =  ltrsPerHrs+parseFloat(fuel.ltrsPerHrs);
							// if(val.vehicleMode == 'DG' && val.vehicleMode == 'Machinery' && val.sensor ==1){
							// 	ltrsPerHrs       =  ltrsPerHrs+parseFloat(val.ltrsPerHrs);
							// 	}
							// debugger
                		}
                	});
                }else{	
                	var sensorData = null;
                	if(parseInt(val.sensor)==1){
                		sensorData = val.fuelsensors[0];
                	}else{
                		sensorData = val.allSensor;
                		fuelBalance  =  fuelBalance+parseFloat(sensorData.fuelBalance);
                	}
                	if(sensorData){
                		startFuel        =  startFuel+parseFloat(sensorData.startFuel);
                		endFuel          =  endFuel+parseFloat(sensorData.endFuel);
                		fuelConsumption  =  fuelConsumption+parseFloat(sensorData.fuelConsumption);
						// if(sensorData.vehicleMode == 'Dispenser' || sensorData.vehicleMode == 'Moving Vehicle'){
						// 	fuelcon1     =fuelConsumption+parseFloat(sensorData.fuelConsumption)
						// }
						// console.log(sensorData)
						// console.log("fuel",fuelcon1);
                		fuelFilling      =  fuelFilling+parseFloat(sensorData.fuelFilling);
                		fuelTheft        =  fuelTheft+parseFloat(sensorData.fuelTheft);
						ltrsPerHrs       =  ltrsPerHrs+parseFloat(val.ltrsPerHrs);
					
                	}
                }
                

               //console.log(engineIdleHrs);
           });  
		//    console.log(kmplll)
		console.log("dis",dist1)
		console.log("fue",fuelcon1)
		console.log("fue2",fuelcon2)

			$scope.startFuel        =  startFuel;
			$scope.endFuel          =  endFuel;
			$scope.fuelConsumption  =  fuelConsumption;
			$scope.fuelcon1         =  fuelcon1;
			$scope.fuelcon2         =  fuelcon2;
			$scope.fuelFilling      =  fuelFilling;
			$scope.fuelTheft        =  fuelTheft;
			$scope.fuelBalance      =  fuelBalance;
			$scope.startKms         =  startKms;
			$scope.endKms           =  endKms;
			$scope.dist             =  dist;
			$scope.dist1            =  dist1;
			$scope.kmpl             =  kmpl; 
			   
			$scope.ltrsPerHrs       =  ltrsPerHrs;

			$scope.engineIdleHrs    =  engineIdleHrs;
			$scope.ignitionHrs      =  ignitionHrs;
			$scope.ignitionHrs1      =  ignitionHrs1;
			$scope.secIgnitionHrs   =  secIgnitionHrs;
			$scope.engineHrs        =  engineHrs;

                //console.log(  $scope.startFuel  );
            }

            $scope.yesterdayDisabled = false;
            $scope.weekDisabled = false;
            $scope.monthDisabled = false;
            $scope.todayDisabled = false;
            $scope.durationFilter    =   function(duration){
	//alert('inside function');
	startLoading();
	var now = new Date();
	$scope.uiDate.todate       = getTodayDate(now.setDate(now.getDate() - 1));
	switch(duration){

		case 'yesterday':
		$scope.yesterdayDisabled = true;
		$scope.weekDisabled = false;
		$scope.monthDisabled = false;
		$scope.todayDisabled = false;
		

		var d = new Date();
		if($scope.showOrgTime){
			$scope.uiDate.fromdate    = getTodayDate(d.setDate(d.getDate() - 2));
		}
		else {
			$scope.uiDate.fromdate= getTodayDate(d.setDate(d.getDate() - 1));
		}

		webCall();
		break;
		case 'lastweek':
		$scope.yesterdayDisabled = false;
		$scope.weekDisabled = true;
		$scope.monthDisabled = false;
		$scope.todayDisabled = false;
		var d = new Date();
		$scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() - 8));
		webCall();
		break;
		case 'month':
		$scope.yesterdayDisabled = false;
		$scope.weekDisabled = false;
		$scope.monthDisabled = true;
		$scope.todayDisabled = false;
		var d = new Date();
		$scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() - 31));
		webCall();
		break;
		case 'today':
		$scope.yesterdayDisabled = false;
		$scope.weekDisabled = false;
		$scope.monthDisabled = false;
		$scope.todayDisabled = true;
		var d = new Date();
		$scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() ));
		$scope.uiDate.todate       = getTodayDate(d.setDate(d.getDate() ));
		webCall();
		break;

	}
}
function initTotalValue(){
	$scope.startFuel        =  0;
	$scope.endFuel          =  0;
	$scope.fuelConsumption  =  0;
	$scope.fuelcon1         =  0;
	$scope.fuelcon2         =  0;
	$scope.fuelFilling      =  0;
	$scope.fuelTheft        =  0;
	$scope.fuelBalance      =  0;
	$scope.startKms         =  0;
	$scope.endKms           =  0;
	$scope.dist             =  0;
	$scope.dist1            =  0;
	$scope.kmpl             =  0;    
	$scope.vehicleMode; 
	$scope.ltrsPerHrs       =  0;
	$scope.engineIdleHrs    = 0;
	$scope.ignitionHrs      = 0;
	$scope.ignitionHrs1      = 0;
	$scope.secIgnitionHrs   = 0;
	$scope.engineHrs        = 0;
}

function webCall() {
	$scope.groupHasMulSensor  = false;
	$scope.conFuelError = "";
	$scope.showDelta = false;
	var date=moment($scope.uiDate.fromdate,'DD-MM-YYYY').format('YYYY-MM-DD');
	var shifttime=$scope.orgTime=='12:00 - 23:59'?0:$scope.orgTime;
	var dateString = moment($scope.uiDate.fromdate,'DD-MM-YYYY').format('MM-DD-YYYY'); // Oct 23

	
        //if((checkXssProtection($scope.uiDate.fromdate) == true) && ((checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true))) {
           //var tollUrl = GLOBAL.DOMAIN_NAME+'/getTollgateReport?group='+$scope.gName+'&fromTimeUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toTimeUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
             //var fuelConUrl  =  GLOBAL.DOMAIN_NAME+'/getConsolidatedFuelReport?groupName='+$scope.gName+'&date='+$scope.uiDate.fromdate;
        //}


        //var fuelConUrl  =  'http://188.166.244.126:9000/getConsolidatedFuelReport?userId=naplogi&groupName=NAPLOGI:SMP&date=2018-07-05';
        if($scope.showOrgTime){
        	var fuelConUrl  =  GLOBAL.DOMAIN_NAME+'/getConsolidatedFuelReport?groupName='+$scope.gName+'&date='+date+'&shiftTime='+shifttime;}
        	else {
        		var fuelConUrl  =  GLOBAL.DOMAIN_NAME+'/getConsolidatedFuelReport?groupName='+$scope.gName+'&date='+date;
        	}
        	console.log( fuelConUrl );
        	$scope.fuelConData = [];
			$scope.vehicleMode= [];
        	$scope.fuelOriginData = [];  
        	initTotalValue();
        	$http.get(fuelConUrl).success(function(data) {
        		stopLoading();
				// $scope.vehicleMode=data.vehicleMode;
        		if(data!='Failed'){
	        	    $scope.fuelOriginData = JSON.parse(JSON.stringify(data));  // Deep Clone
	        	    
		            //set error msg
		            if(data[0].error){
		            	$scope.conFuelError= data[0].error;
		            	$scope.fuelConData = data;
						// $scope.vehicleMode = $scope.fuelConData.vehicleMode;
						
		            }else{
		            	totalValues(data);
		        	    //total added in Multiple Sensor
		        	    $scope.fuelConData = data.map(function (value, key) {
		        	    	if(parseInt(value.sensor)>1){
		        	    		$scope.groupHasMulSensor  = true;
		        	    		if($scope.isTankBased){
		        	    			value.sensor = value.nTanks;
		        	    			value.fuelsensors = value.tankData;
		        	    		}else{
		        	    			$scope.showDelta = true;
		        	    			value.fuelsensors.push(value.allSensor)
		        	    		}
		        	    	}
		        	    	return value;
		        	    });
		        	}

		        	if($scope.fuelConData!=null&&$scope.fuelConData!=""){
		        		$scope.orgName = $scope.fuelConData[0].orgId;
		        	}
		        }else{
		        	$scope.conFuelError =  translate("curl_error"); 
		        }
		    }); 
        }

  //get the value from the ui
  function getUiValue() {
  	$scope.uiDate.fromdate   =   $('#dtFrom').val();
	  //$scope.uiDate.fromtime   =   $('#timeFrom').val();
	  //$scope.uiDate.todate	 =   $('#dateTo').val();
	  //$scope.uiDate.totime 	 =   $('#timeTo').val();
	}

	function calculateDateTime(){
		if($scope.orgTime=='12:00 - 23:59'){
			$scope.showOrganization=false;
			$scope.fromtotime=('00:00 - 23:59').split('-');
		}
		else {
			$scope.showOrganization=true;
			$scope.fromtotime=$scope.orgTime.split('-');
		}

		var index=$scope.orgShiftTime.indexOf($scope.orgTime);
		var name=$scope.orgShiftName[index].split(',');
		$scope.organName=name[1];

		var frmhrs=$scope.fromtotime[0].split(':');
		var tohrs=$scope.fromtotime[1].split(':');
		var dateString = moment($scope.uiDate.fromdate,'DD-MM-YYYY').format('MM-DD-YYYY'); // Oct 23
		var hrs=new Date().getHours();
		var date1 = new Date(dateString);
		if($scope.orgTime=='12:00 - 23:59'){
			$scope.uiDate.fromdate=getTodayDate(date1.setDate(date1.getDate()));
			$scope.uiDate.todate=getTodayDate(date1.setDate(date1.getDate()));
		}
		else if(parseInt(tohrs[0])>parseInt(frmhrs[0]) && parseInt(tohrs[0])<parseInt(hrs)){
			$scope.uiDate.fromdate=getTodayDate(date1.setDate(date1.getDate()));
			$scope.uiDate.todate=getTodayDate(date1.setDate(date1.getDate()));
		}
		else {
			$scope.uiDate.fromdate=getTodayDate(date1.setDate(date1.getDate()));
			$scope.uiDate.todate=getTodayDate(date1.setDate(date1.getDate()+1));
		}
	}


// service call for the event report

/*function webServiceCall(){
		$scope.siteData = [];
		if((checkXssProtection($scope.uiDate.fromdate) == true) && (checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true)) {
			
			var url 	= GLOBAL.DOMAIN_NAME+"/getActionReport?vehicleId="+$scope.vehiname+"&fromDate="+$scope.uiDate.fromdate+"&fromTime="+convert_to_24h($scope.uiDate.fromtime)+"&toDate="+$scope.uiDate.todate+"&toTime="+convert_to_24h($scope.uiDate.totime)+"&interval="+$scope.interval+"&stoppage="+$scope.uiValue.stop+"&stopMints="+$scope.uiValue.stopmins+"&idle="+$scope.uiValue.idle+"&idleMints="+$scope.uiValue.idlemins+"&notReachable="+$scope.uiValue.notreach+"&notReachableMints="+$scope.uiValue.notreachmins+"&overspeed="+$scope.uiValue.speed+"&speed="+$scope.uiValue.speedkms+"&location="+$scope.uiValue.locat+"&site="+$scope.uiValue.site+'&fromDateUTC='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateUTC='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
			vamoservice.getDataCall(url).then(function(responseVal){
				$scope.recursiveEvent(responseVal, 0);
				$scope.eventData = responseVal;
				var entry=0,exit=0; 
				angular.forEach(responseVal, function(val, key){
					if(val.state == 'SiteExit')
						exit++ 
					else if (val.state == 'SiteEntry')
						entry++
				})
				$scope.siteEntry 	=	entry;
				$scope.siteExit 	=	exit;
				stopLoading();
			});
		}
		stopLoading();
	}*/

	// initial method
	$scope.total = function(val1, val2) {
		return parseFloat(val1)+parseFloat(val2);
	}

	function initialAPICall(){
		vamoservice.getDataCall($scope.url).then(function(data) {    

          //startLoading();
          $scope.selectVehiData  =  [];
          $scope.vehicle_group   =  [];
          $scope.vehicle_list    =  data;

          if(data.length) {

          	$scope.vehiname	 =  getParameterByName('vid');
          	$scope.uiGroup 	 =  $scope.trimColon(getParameterByName('vg'));
          	$scope.gName 	 =  getParameterByName('vg');
			  //console.log(data);
			  //$scope.gName   =  data[0].group;
		      //console.log(data[0].group);

		      if($scope.vehiname == 'undefined' || $scope.vehiname == '') {
		      	$scope.vehiname  =  data[$scope.gIndex].vehicleLocations[0].vehicleId; 
		         //alert($scope.vehiname );
		     } 

		     if($scope.gName == 'undefined' || $scope.gName == ''){
		     	$scope.gName  =  data[$scope.gIndex].group; 
		     } 

		     angular.forEach(data, function(val, key) {
		          //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
		          if($scope.gName == val.group) {
		          	$scope.gIndex = val.rowId;

		          	angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys){

		          		$scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

		          		if($scope.vehiname == value.vehicleId){
		          			$scope.shortNam  =  value.shortName;
		          		}
		          	});
		          }
		      });
			  //console.log($scope.selectVehiData);
			  sessionValue($scope.vehiname, $scope.gName);
			  $scope.notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
			  $('#notncount').text($scope.notncount);
			  window.localStorage.setItem('totalNotifications',$scope.notncount);
			}

			var dateObj 			 = 	 new Date();
			$scope.fromNowTS		 =	 new Date(dateObj.setDate(dateObj.getDate()-1));
			

			
			
			$scope.uiDate.fromdate 	 =	 getTodayDate($scope.fromNowTS);
			
		  //$scope.uiDate.fromtime	 =	 '12:00 AM';
		  //$scope.uiDate.todate	 =	 getTodayDate($scope.fromNowTS);
		  //$scope.uiDate.totime 	 =	 formatAMPM($scope.fromNowTS.getTime());
          //$scope.uiDate.totime 	 =   '11:59 PM';
		  //webServiceCall();
		  if($scope.showOrgTime){
		  	calculateDateTime();
		  }
		  startLoading();
		  webCall();
		  //stopLoading();
		});	
	}

	$scope.$watch("url", function (val) {
		$scope.orgShiftTime=[];
		$scope.orgShiftName=[];
		var timeUrl    = GLOBAL.DOMAIN_NAME+'/getOrgShiftTimeList';
		vamoservice.getDataCall(timeUrl).then(function(response){

			if(response.data==0){
				$scope.showOrgTime=false;
				initialAPICall();
			}
			else {

				$scope.showOrgTime=true;
				for(i=1;i<response.data.length;i++){
					if(response.data[i]!="00:00 - 23:59"){

						var orgnametime=response.data[i].split(',');

						var t=orgnametime[0].split('-');
						var t1=t[0].split(':');
						var t2=t[1].split(':');
						if(t1[0]=='00'){
							$scope.orgShiftTime.push('12:'+t1[1]+'-'+t[1]);
						}
						else if(t2[0]=='00'){
							$scope.orgShiftTime.push(t[0]+'-'+'12:'+t2[1]);
						}
						else {
							$scope.orgShiftTime.push(orgnametime[0]);
						}

					}
					else {

						var t=response.data[i].split('-');
						var t1=t[0].split(':');
						var t2=t[1].split(':');
						if(t1[0]=='00'){
							$scope.orgShiftTime.push('12:'+t1[1]+'-'+t[1]);
						}
						else if(t2[0]=='00'){
							$scope.orgShiftTime.push(t[0]+'-'+'12:'+t2[1]);
						}


					}
					$scope.orgShiftName.push(response.data[i]);

				}
				$scope.organisation=response.data[1].split(',');

				$scope.organName=$scope.organisation[1];
				var res=response.data[0];
				if(res=='00:00 - 23:59'){
					var index=$scope.orgShiftTime.indexOf('12:00 - 23:59');
					$scope.orgTime=$scope.orgShiftTime[index];}
					else {
						var index=$scope.orgShiftTime.indexOf(response.data[0]);
						$scope.orgTime=$scope.orgShiftTime[index];
					}
					if($scope.orgTime=='12:00 - 23:59'){
						$scope.showOrganization=false;
						$scope.fromtotime=('00:00 - 23:59').split('-');
					}
					else {
						$scope.showOrganization=true;
						$scope.fromtotime=$scope.orgTime.split('-');
					}

					initialAPICall(); 

				}


			});
		
	});



	$scope.groupSelection = function(groupName, groupId) {
		startLoading();
		$scope.gName 	=  groupName;
		$scope.uiGroup  =  $scope.trimColon(groupName);
		$scope.gIndex	=  groupId;
		$scope.isTankBased   = false;
		$scope.groupHasMulSensor  = false;
		var url  		=  GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+groupName;

		vamoservice.getDataCall(url).then(function(response) {

			$scope.vehicle_list     =  response;
			$scope.shortNam		    =  response[$scope.gIndex].vehicleLocations[0].shortName;
			$scope.vehiname		    =  response[$scope.gIndex].vehicleLocations[0].vehicleId;
			sessionValue($scope.vehiname, $scope.gName);
			$scope.selectVehiData   =  [];
            //console.log(response);
            angular.forEach(response, function(val, key) {
            	if($scope.gName == val.group){
					//	$scope.gIndex = val.rowId;
					angular.forEach(response[$scope.gIndex].vehicleLocations, function(value, keys) {

						$scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

						if($scope.vehiname == value.vehicleId){
							$scope.shortNam  =  value.shortName;
						}
					});
				}
			});

            $scope.notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
            $('#notncount').text($scope.notncount);
            window.localStorage.setItem('totalNotifications',$scope.notncount);

            getUiValue();
            webCall();
		  //webServiceCall();
	      //stopLoading();
	  });

	}


  /*$scope.genericFunction 	= function (vehid, index){
	   startLoading();
	   $scope.vehiname		= vehid;
	   sessionValue($scope.vehiname, $scope.gName)
	   angular.forEach($scope.vehicle_list[$scope.gIndex].vehicleLocations, function(val, key){
		  if(vehid == val.vehicleId){
			$scope.shortNam	= val.shortName;
		  }
	   });
	   getUiValue();
	 //webServiceCall();
	}*/

	$scope.submitFunction 	=	function(){

		$scope.yesterdayDisabled = false;
		$scope.weekDisabled = false;
		$scope.monthDisabled = false;
		$scope.todayDisabled = false;
		startLoading();
		getUiValue();
		if($scope.showOrgTime){
			calculateDateTime();
		}
		webCall();
	//webServiceCall();
    //stopLoading();
}

$scope.changeTime=function(time){
	$scope.orgTime=time;
	

}
$scope.showSensorData 	=	function(){
	$scope.isTankBased   = false;
	$scope.showDelta = true;
	var data = JSON.parse(JSON.stringify($scope.fuelOriginData)); 
	$scope.fuelConData = data.map(function (value, key) {
		if(parseInt(value.sensor)>1){ 
			value.fuelsensors.push(value.allSensor); 
		}
		return value;
	});
}
$scope.showTankData 	=	function(){
	$scope.isTankBased = true;
	$scope.showDelta = false;
	var data = JSON.parse(JSON.stringify($scope.fuelOriginData)); 
	$scope.fuelConData = data.map(function (value, key) {
		if(parseInt(value.sensor)>1){
			value.sensor = value.nTanks;
			value.fuelsensors = value.tankData;
		}
		return value;
	});
}

$scope.getAMPM=function(time){
	var str       =  time.split(':');     
	var hours     =  Number(str[0]);
	var minutes   =  Number(str[1]);

	if(hours=='00'){
		return ('12'+':'+('0'+minutes).slice(-2)).concat(" ",'AM');
	}

	else if(hours==12){
		return ('12'+':'+('0'+minutes).slice(-2)).concat(" ",'PM');
	}
	else if(hours < 12) {
		return (hours+':'+('0'+minutes).slice(-2)).concat(" ",'AM');
	}
	else{
		hours=hours-12;
		return (hours+':'+('0'+minutes).slice(-2)).concat(" ",'PM');
	}     


}

$scope.exportData = function (data) {
	  //console.log(data);
	  var blob = new Blob([document.getElementById(data).innerHTML], {
		type: 'binary',
	  	type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
	  });
	  saveAs(blob, data+".xls");
	};

// 	$scope.exportData = function (fuelconreport) {
// 	  //console.log(data);
// 	 const data = [document.getElementById(fuelconreport).innerHTML];
// 	  const workbook = XLSX.utils.book_new();

// // Add a new worksheet to the workbook
// 		const worksheet = XLSX.utils.aoa_to_sheet(data);
// 		XLSX.utils.book_append_sheet(workbook, worksheet, 'Sheet1');

// // Write the workbook to a binary string
// 		const binaryString = XLSX.write(workbook, {bookType:'xlsx', type:'binary'});

// // Convert the binary string to a Blob object
// 		const blob = new Blob([s2ab(binaryString)], {type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"});

// // Download the Blob object as an Excel file
// 		saveAs(blob, 'example.xlsx');
// 	};

// 	function s2ab(s) {
//   	const buf = new ArrayBuffer(s.length);
//   	const view = new Uint8Array(buf);
//   	for (let i = 0; i < s.length; i++) {
//     view[i] = s.charCodeAt(i) & 0xFF;
//   	}
//   	return buf;
// 	}

	$scope.exportDataCSV = function (data) {
	  //console.log(data);
	  CSV.begin('#'+data).download(data+'.csv').go();
	};
	$scope.generatePDF = function() {
		$('.addr').show();
		$('.loc').hide();
		let tableName='Consolidated Fuel Report';
		var doc = new jsPDF({
			orientation: 'l',
			unit: 'mm',
			format: 'a3',
		});
		var table1=doc.autoTableHtmlToJson(document.getElementById("table1"));
		var table2 = doc.autoTableHtmlToJson(document.getElementById("table2"));
		doc.setFontSize(12);
		doc.text(`${tableName.toUpperCase()}`,5,10);
        // doc.autoTable(table1.columns, table1.rows, {
        //     theme: 'plain', 
        //     margin: [15,5,10,5],
        //     tableLineColor: [189, 195, 199],
        //     styles: {
        //       fontSize: 10,
        //       overflow: 'linebreak',
        //       lineWidth: 0.01,
        //     }
        //   });
        //table2.rows.shift();
        doc.autoTable(table2.columns, table2.rows, {
        	theme: 'plain', 
        	margin: {
        		top: 15,
        		left: 5,
        		right: 5,
        		bottom: 5 },
        		tableLineColor: [189, 195, 199],
        		styles: {
        			fontSize: 10,
        			overflow: 'linebreak',
        			lineWidth: 0.01
        		},
        		pageBreak: 'always',
        		// columnStyles: {
        		// 	0: { columnWidth: 'wrap' },
        		// 	1: { columnWidth: 30 },
        		// 	2: { columnWidth: 15 },
        		// 	3: { columnWidth: 15 },
        		// 	4: { columnWidth: 15 },
        		// 	5: { columnWidth: 15 },
        		// 	6: { columnWidth: 15 },
        		// 	7: { columnWidth: 15 },
        		// 	8: { columnWidth: 15 },
        		// 	9: { columnWidth: 15 },
        		// 	10: { columnWidth: 15 },
        		// 	11: { columnWidth: 15 },
        		// 	12: { columnWidth: (!$scope.showDelta)? 25 : 15 },
        		// 	13: { columnWidth: 25 },
        		// 	14: { columnWidth: ($scope.showDelta)? 25 : 15  },
        		// 	15: { columnWidth: (!$scope.showDelta)? 25 : 15 },
        		// 	16: { columnWidth: 25 },
        		// 	17: { columnWidth: ($scope.showDelta)? 25 : 15 },
        		// 	18: { columnWidth: 15 },
        		// 	19: { columnWidth: 15 },
        		// 	20: { columnWidth: 15 },
        		// 	21: { columnWidth: 10 },
        		// 	22: { columnWidth: 10 },
        		// }
        	});
        doc.save(tableName+'.pdf');
        $('.loc').show();
        $('.addr').hide();

    }


    $('#minus').click(function(){
    	$('#menu').toggle(1000);
    })
    // $(window).scroll(function (event) {
    // 	if($scope.tapchanged){
    // 		$scope.tapchanged=false;
    // 		console.log('scrolled');
    // 		var table = $('#table-fixed-header1').DataTable( {
    // 			"fixedHeader": { 
    // 				"header": true, 
    // 				"footer": false             
    // 			}
    // 		})
    // 	}
    // });

}]);