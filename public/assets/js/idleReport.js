app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global', function($scope, $http, vamoservice, $filter, GLOBAL){
	
  //global declaration
  $scope.uiDate 				=	{};
  $scope.uiValue	 			= 	{};
  //$scope.sort                 =   sortByDate('startTime');
  $scope.interval             =   '';

  var tab = getParameterByName('tn');
  var licenceExpiry="";
  var strExpired='expired';
  if(localStorage.getItem('licenceExpiry'))licenceExpiry=localStorage.getItem('licenceExpiry');
  $scope.errMsg="";
  $scope.trvShow       =  localStorage.getItem('trackNovateView');
  var expiryDays='';
  $scope.idleType='Idle Wastage';
  $scope.idleAngleWastage=['Idle Wastage','Angle Sensor Wastage'];

  function getParameterByName(name) {
  	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
  	results = regex.exec(location.search);
  	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

	//global declartion
	$scope.locations = [];
	$scope.url = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
	$scope.gIndex =0;

  //$scope.locations01 = vamoservice.getDataCall($scope.url);
  $scope.trimColon = function(textVal){
  	if(textVal!=undefined){
  		return textVal.split(":")[0].trim();
  	}
  }

  function sessionValue(vid, gname,licenceExpiry){
  	localStorage.setItem('user', JSON.stringify(vid+','+gname));
  	localStorage.setItem('licenceExpiry', licenceExpiry);
  	$("#testLoad").load("../public/menu");
  }

  function convert_to_24h(time_str) {
		//console.log(time_str);
		var str		=	time_str.split(' ');
		var stradd	=	str[0].concat(":00");
		var strAMPM	=	stradd.concat(' '+str[1]);
		var time = strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
		var hours = Number(time[1]);
		var minutes = Number(time[2]);
		var seconds = Number(time[2]);
		var meridian = time[4].toLowerCase();

		if (meridian == 'p' && hours < 12) {
			hours = hours + 12;
		}
		else if (meridian == 'a' && hours == 12) {
			hours = hours - 12;
		}	    
		var marktimestr	=''+hours+':'+minutes+':'+seconds;	    
		return marktimestr;
	};

    // millesec to day, hours, min, sec
    $scope.msToTime = function(ms) {

    	days      = Math.floor(ms / (24 * 60 * 60 * 1000));
    	daysms    = ms % (24 * 60 * 60 * 1000);
    	hours     = Math.floor((ms) / (60 * 60 * 1000));
    	hoursms   = ms % (60 * 60 * 1000);
    	minutes   = Math.floor((hoursms) / (60 * 1000));
    	minutesms = ms % (60 * 1000);
    	seconds   = Math.floor((minutesms) / 1000);

		// if(days==0)
		// 	return hours +" h "+minutes+" m "+seconds+" s ";
		// else
		return hours +":"+minutes+":"+seconds;
	}

	var delayed4 = (function () {
		var queue = [];

		function processQueue() {
			if (queue.length > 0) {
				setTimeout(function () {
					queue.shift().cb();
					processQueue();
				}, queue[0].delay);
			}
		}

		return function delayed(delay, cb) {
			queue.push({ delay: delay, cb: cb });

			if (queue.length === 1) {
				processQueue();
			}
		};
	}());

	function google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent) {
		vamoservice.getDataCall(tempurlEvent).then(function(data) {
			$scope.addressEvent[index4] = data.results[0].formatted_address;
			//console.log(' address '+$scope.addressEvent[index4])
			// var t = vamo_sysservice.geocodeToserver(latEvent,lonEvent,data.results[0].formatted_address);
		})
	};

	$scope.recursiveEvent 	= 	function(locationEvent, indexEvent)
	{
		var index4 = 0;
		angular.forEach(locationEvent, function(value ,primaryKey){
			//console.log(' primaryKey '+primaryKey)
			index4 = primaryKey;
			if(locationEvent[index4].address == undefined)
			{
				var latEvent		 =	locationEvent[index4].latitude;
				var lonEvent		 =	locationEvent[index4].longitude;
				var tempurlEvent =	"https://maps.googleapis.com/maps/api/geocode/json?latlng="+latEvent+','+lonEvent+"&sensor=true";
				delayed4(2000, function (index4) {
					return function () {
						google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent);
					};
				}(index4));
			}
		})
	}


	function formatAMPM(date) {
		var date = new Date(date);
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? 'PM' : 'AM';
		hours = hours % 12;
		  hours = hours ? hours : 12; // the hour '0' should be '12'
		  minutes = minutes < 10 ? '0'+minutes : minutes;
		  var strTime = hours + ':' + minutes + ' ' + ampm;
		  return strTime;
		}


		function _pairFilter(data) {

			var ret_Arr=[];

			for(var i=0; i<data.length; i++) {

				if(data[i].ignitionStatus == "ON") {
					if(ign_On==0){

						console.log(i+' '+'ON');

						ret_Arr.push(data[i]);

						ign_On=1;
					}

				} 
			}


			return ret_Arr; 
		}


		function setBtnEnable(btnName){
			$scope.yesterdayDisabled = false;
			$scope.weekDisabled = false;
			$scope.monthDisabled = false;
			$scope.todayDisabled = false;
			$scope.lastmonthDisabled = false;
			$scope.thisweekDisabled = false;

			switch(btnName){

				case 'yesterday':
				$scope.yesterdayDisabled = true;
				break;
				case 'today':
				$scope.todayDisabled = true;
				break;
				case 'thisweek':
				$scope.thisweekDisabled = true;
				break;
				case 'lastweek':
				$scope.weekDisabled = true;
				break;
				case 'month':
				$scope.monthDisabled = true;
				break;
				case 'lastmonth':
				$scope.lastmonthDisabled = true;
				break;
			}


		}
		setBtnEnable("init");
		$scope.durationFilter    =   function(duration){
  //alert('inside function');
  startLoading();
  var now = new Date();
  $scope.uiDate.todate      = getTodayDate(now.setDate(now.getDate() - 1));
  switch(duration){

  	case 'yesterday':
  	setBtnEnable('yesterday');
  	var d = new Date();
  	$scope.uiDate.fromdate= getTodayDate(d.setDate(d.getDate() - 1));
  	$scope.uiDate.totime  = '11:59 PM';
      // datechange();
      break;
      case 'thisweek':
      setBtnEnable('thisweek');
      var d=new Date();
      var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1);
      var firstday= new Date(d.getFullYear(), d.getMonth(), diff);
      $scope.uiDate.fromdate      = getTodayDate(firstday.setDate(firstday.getDate() ));
      $scope.uiDate.todate= getTodayDate(new Date().setDate(new Date().getDate() ));

      $scope.uiDate.totime       = formatAMPM(d);
      //datechange();
      break;
      case 'lastweek':
      setBtnEnable('lastweek');
      var d = new Date();
      var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1)-7;
      var diff1=d.getDate() - day + (day == 0 ? -6:1)-1;
      var firstday= new Date(d.getFullYear(), d.getMonth(), diff);
      var lastday= new Date(d.getFullYear(), d.getMonth(), diff1);
      $scope.uiDate.fromdate      = getTodayDate(firstday.setDate(firstday.getDate() ));
      $scope.uiDate.todate       = getTodayDate(lastday.setDate(lastday.getDate() ));
      $scope.uiDate.totime  = '11:59 PM';
      //datechange();
      break;
      case 'month':
      setBtnEnable('month');
      var date = new Date();
      var firstdate = new Date(date.getFullYear(), date.getMonth(), 1);
      $scope.uiDate.fromdate       = getTodayDate(firstdate.setDate(firstdate.getDate() ));
      $scope.uiDate.todate       = getTodayDate(date.setDate(date.getDate() ));

      $scope.uiDate.totime      = formatAMPM(date);
       //datechange();
       break;
       case 'today':
       setBtnEnable('today');
       var d = new Date();
       $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() ));
       $scope.uiDate.todate       = getTodayDate(d.setDate(d.getDate() ));
       $scope.uiDate.totime       = formatAMPM(d);
      //datechange();
      break;
      case 'lastmonth':
      setBtnEnable('lastmonth');
      var date = new Date();
      var firstdate = new Date(date.getFullYear(), date.getMonth()-1, 1);
      var lastdate = new Date(date.getFullYear(), date.getMonth(), 0);
      $scope.uiDate.fromdate       = getTodayDate(firstdate.setDate(firstdate.getDate() ));
      $scope.uiDate.todate      = getTodayDate(lastdate.setDate(lastdate.getDate() ));

      $scope.uiDate.totime   = '11:59 PM';
      //datechange();
      break;
  }
  webCall();
}


var inIdleVal = 0;

function webCall(){

      // if((checkXssProtection($scope.uiDate.fromdate) == true) && ((checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true))) {
           //var tollUrl = GLOBAL.DOMAIN_NAME+'/getTollgateReport?group='+$scope.gName+'&fromTimeUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toTimeUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
       //}
       $scope.errMsg='';
       idleUrl   =  GLOBAL.DOMAIN_NAME+"/getVehicleHistory?vehicleId="+$scope.vehIds+"&interval="+$scope.interval+'&fromDateUTC='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateUTC='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));


       console.log(idleUrl);

       
       var expdate=moment().add(expiryDays,'days').format('DD-MM-YYYY'),
       convertedexpdate=utcFormat(expdate,convert_to_24h('11:59 PM')),
       convertedtodate=utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime)),
       convertedfromdate=utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime));
       if(convertedtodate<=convertedexpdate && convertedfromdate<=convertedexpdate){
       	$http.get(idleUrl).success(function(data) {

       		$scope.idleData = [];
       		totalIdleTime=0;
        	//$scope.idleData = data;
        	console.log(data);

        	$scope.idleData2 = data;

        	if(data.vehicleLocations!=null){
        		if($scope.idleData2.angleEnabled!='yes'){
        			$scope.idleType='Idle Wastage';
        		}
        		else {
        			$scope.idleType=$scope.idleType;
        		}
        		console.log($scope.idleType);
        		$scope.changeIdleType($scope.idleType);

           //alert($scope.totalIdleTime);

           console.log($scope.idleData);

       }
       stopLoading();
   }); 
       }else{
       	stopLoading();
       	$scope.errMsg=licenceExpiry;
       	$scope.showErrMsg = true;
       } 
   }

	//get the value from the ui
	function getUiValue(){
		$scope.uiDate.fromdate 	=  $('#dateFrom').val();
		$scope.uiDate.fromtime	=  $('#timeFrom').val();
		$scope.uiDate.todate	=  $('#dateTo').val();
		$scope.uiDate.totime 	=  $('#timeTo').val();
		if(localStorage.getItem('timeTochange')!='yes'){
			updateToTime();
			$scope.uiDate.totime 		=   localStorage.getItem('toTime');
		}

	}

// service call for the event report

/*	function webServiceCall(){
		$scope.siteData = [];
		if((checkXssProtection($scope.uiDate.fromdate) == true) && (checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true)) {
			
			var url 	= GLOBAL.DOMAIN_NAME+"/getActionReport?vehicleId="+$scope.vehiname+"&fromDate="+$scope.uiDate.fromdate+"&fromTime="+convert_to_24h($scope.uiDate.fromtime)+"&toDate="+$scope.uiDate.todate+"&toTime="+convert_to_24h($scope.uiDate.totime)+"&interval="+$scope.interval+"&stoppage="+$scope.uiValue.stop+"&stopMints="+$scope.uiValue.stopmins+"&idle="+$scope.uiValue.idle+"&idleMints="+$scope.uiValue.idlemins+"&notReachable="+$scope.uiValue.notreach+"&notReachableMints="+$scope.uiValue.notreachmins+"&overspeed="+$scope.uiValue.speed+"&speed="+$scope.uiValue.speedkms+"&location="+$scope.uiValue.locat+"&site="+$scope.uiValue.site+'&fromDateUTC='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateUTC='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
			vamoservice.getDataCall(url).then(function(responseVal){
				$scope.recursiveEvent(responseVal, 0);
				$scope.eventData = responseVal;
				var entry=0,exit=0; 
				angular.forEach(responseVal, function(val, key){
					if(val.state == 'SiteExit')
						exit++ 
					else if (val.state == 'SiteEntry')
						entry++
				})
				$scope.siteEntry 	=	entry;
				$scope.siteExit 	=	exit;
				stopLoading();
			});
		}
		stopLoading();
	}*/

	// initial method

	$scope.$watch("url", function (val) {
		vamoservice.getDataCall($scope.url).then(function(data) {

          //startLoading();
          $scope.selectVehiData = [];
          $scope.vehicle_group=[];
          $scope.vehicle_list = data;

          if(data.length){
          	$scope.vehiname	= getParameterByName('vid');
          	$scope.uiGroup 	= $scope.trimColon(getParameterByName('vg'));
          	$scope.gName 	= getParameterByName('vg');
          	angular.forEach(data, function(val, key){
                  //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
                  if($scope.gName == val.group){
                  	$scope.gIndex = val.rowId;

                  	angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys){

                  		$scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

                  		if($scope.vehiname == value.vehicleId){
                  			$scope.shortNam	= value.shortName;
                  			$scope.vehIds   = value.vehicleId;
                  			licenceExpiry=value.licenceExpiry;
                  			expiryDays=value.expiryDays;
                  		}
                  	})

                  }
              })

		  //console.log($scope.selectVehiData);
		  sessionValue($scope.vehiname, $scope.gName,licenceExpiry)
		  $scope.notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
		  $('#notncount').text($scope.notncount);
		  window.localStorage.setItem('totalNotifications',$scope.notncount);

		}   var dateObj1 			= 	new Date();
		var  dateNow		    =	new Date(dateObj1.setDate(dateObj1.getDate()));
		var fromdatenow         =	getTodayDate(dateNow);
		if(fromdatenow==localStorage.getItem('fromDate'))
		{
			var dateObj 			= 	new Date();
			$scope.fromNowTS		=	new Date(dateObj.setDate(dateObj.getDate()-1));
			$scope.uiDate.fromdate 		=	getTodayDate($scope.fromNowTS);
			$scope.uiDate.fromtime		=	'12:00 AM';
			$scope.uiDate.todate		=	getTodayDate($scope.fromNowTS);
					  //$scope.uiDate.totime 		=	formatAMPM($scope.fromNowTS.getTime());
					  $scope.uiDate.totime 		=   localStorage.getItem('toTime');
					  if(localStorage.getItem('timeTochange')!='yes'){
					  	updateToTime();
					  	$scope.uiDate.totime 		=   localStorage.getItem('toTime');
					  }
			            //alert($scope.uiDate.totime);
			        }else{

			        	$scope.uiDate.fromdate 		=	localStorage.getItem('fromDate');
			        	$scope.uiDate.fromtime		=	localStorage.getItem('fromTime');
			        	$scope.uiDate.todate		=	localStorage.getItem('toDate');
			        	$scope.uiDate.totime 		=   localStorage.getItem('toTime');
			        	if(localStorage.getItem('timeTochange')!='yes'){
			        		updateToTime();
			        		$scope.uiDate.totime 		=   localStorage.getItem('toTime');
			        	}
			            //alert($scope.uiDate.totime);
			        }
			        startLoading();
			        webCall();
		  //stopLoading();
		});	
	});



	$scope.groupSelection 	= function(groupName, groupId) {
		startLoading();
		licenceExpiry="";
		$scope.errMsg="";
		$scope.gName 	= 	groupName;
		$scope.uiGroup 	= 	$scope.trimColon(groupName);
		$scope.gIndex	=	groupId;
		var url  		= 	GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+groupName;

		vamoservice.getDataCall(url).then(function(response){

			$scope.vehicle_list = response;
			$scope.shortNam		= response[$scope.gIndex].vehicleLocations[0].shortName;
			$scope.vehiname		= response[$scope.gIndex].vehicleLocations[0].vehicleId;
			licenceExpiry   = response[$scope.gIndex].vehicleLocations[0].licenceExpiry;
			expiryDays=response[$scope.gIndex].vehicleLocations[0].expiryDays;
			sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
			$scope.selectVehiData=[];
            //console.log(response);
            angular.forEach(response, function(val, key){
            	if($scope.gName == val.group){
					//	$scope.gIndex = val.rowId;
					angular.forEach(response[$scope.gIndex].vehicleLocations, function(value, keys){

						$scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

						if($scope.vehiname == value.vehicleId)
							$scope.shortNam	= value.shortName;
						$scope.vehIds   = value.vehicleId;
					})
				}
			})

            $scope.notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
            $('#notncount').text($scope.notncount);
            window.localStorage.setItem('totalNotifications',$scope.notncount);

            getUiValue();
            webCall();
		  //webServiceCall();
	      //stopLoading();
	  });

	}


	$scope.genericFunction 	= function (vehid, index,shortname,position, address,groupName,licenceExp,expdate) {
		startLoading();
		licenceExpiry=licenceExp;
		$scope.errMsg="";
		$scope.vehiname  =  vehid;
		sessionValue($scope.vehiname, $scope.gName,licenceExpiry)
		angular.forEach($scope.vehicle_list[$scope.gIndex].vehicleLocations, function(val, key){
			if(vehid == val.vehicleId){
				$scope.shortNam	= val.shortName;
				$scope.vehIds   = val.vehicleId;
				expiryDays=val.expiryDays;
			}
		});
		getUiValue();
		webCall();
	}

	$scope.submitFunction 	=	function(){
		$scope.yesterdayDisabled = false;
		$scope.weekDisabled = false;
		$scope.monthDisabled = false;
		$scope.todayDisabled = false;
		$scope.lastmonthDisabled = false;
		$scope.thisweekDisabled = false;
		startLoading();
		getUiValue();
		webCall(); 
	//webServiceCall();
    //stopLoading();
}

$scope.exportData = function (data) {
		// console.log(data);
		var blob = new Blob([document.getElementById(data).innerHTML], {
			type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
		});
		saveAs(blob, data+".xls");
	};

	$scope.exportDataCSV = function (data) {
		// console.log(data);
		CSV.begin('#'+data).download(data+'.csv').go();
	};

	$scope.changeIdleType = function(idletype){
		$scope.idleType=idletype;
		$scope.idleData=[];
		if(idletype=='Idle Wastage'){
			$scope.idleData = ($filter('filter')($scope.idleData2.vehicleLocations, {'position':"S"}));	
		}
		else {
			$scope.idleData = ($filter('filter')($scope.idleData2.vehicleLocations, {'angleIdleTime':">0"}));
		}
	}


	$('#minus').click(function(){
		$('#menu').toggle(1000);
	})

}]);
