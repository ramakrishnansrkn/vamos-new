app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global','$translate', function($scope, $http, vamoservice, $filter, GLOBAL,$translate){
  //global declaration
  //alert("hello world...");
  $scope.videoLink="https://www.youtube.com/watch?v=LOhxGEkuzuA&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=1";
  var language=localStorage.getItem('lang');
  $scope.multiLang=language;
  $translate.use(language);
  var translate = $filter('translate');
  $scope.uiDate       =  {};
  $scope.uiValue      =  {};
  $scope.sort         =  sortByDate('dt', false);
  if(localStorage.getItem('interval')!=null){
    $scope.interval=localStorage.getItem('interval');
  }
  else {
    $scope.interval     =  "5";
  }
  $scope.fuelDataMsg  =  "";
  $scope.fuelRaw=[];
  $scope.fuelRawData=[];
  $scope.dates={};
  $scope.count=0;
  $scope.showInterval=true;
  $scope.dealerName=localStorage.getItem('dealerName');
  $scope.error="";
  $scope.hideGraph = false;
  var tab = getParameterByName('tn');
  var noise = getParameterByName('R');
  var est = getParameterByName('Q');
  var errorEst = getParameterByName('P');
  var fuelDataVals;
  var fuelChart = ""
  var licenceExpiry="";
  var expiryDays='';
  $scope.defaultMileage="no";
  var strExpired='expired';
  $scope.millage =""
  $scope.ltphValue = "";
  $scope.ignitionOnPer = "";
  $scope.minFuelTheft = "";
  $scope.customMinFuelTheftFilterValue = "";
  $scope.showFueltheftFilter = false;
  $scope.theftBeta = false;
  $scope.dayDiff=false;
  $scope.showFuelData=false;
  $scope.position='';
  if(localStorage.getItem('licenceExpiry'))licenceExpiry=localStorage.getItem('licenceExpiry');
  $scope.errMsg="";
  $scope.datashow=false;
  $scope.sensorCount   = 1;
  $scope.vehicleMode   = "";
  $scope.sensor = 1;
  $scope.count=0;
  var alreadyclicked = false;
  $scope.fuelfill=false;
  var fixedHeaderInitiated = false;
  var expiryDays='';
  $scope.licenceerrmsg='Licence is expired. Please contact sales for renewing licence.';

  $scope.range = function(min, max, step) {
    step = step || 1;
    var input = [];
    for (var i = min; i <= max; i += step) {
      input.push(i);
    }
    return input;
  };



  function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  //global declartion
  $scope.locations = [];
  $scope.url       = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
  $scope.gIndex    = 0;


  //$scope.locations01 = vamoservice.getDataCall($scope.url);
  $scope.trimColon = function(textVal) {

    if(textVal){
     var spltVal = textVal.split(":");
     return spltVal[0];
   }
 }
 $scope.firstClickedIndex = -1;
 $scope.lastClickedIndex = -1;

 $scope.getValueCheck = function(fuel, index) {
  if($scope.fuelRawData[index].recordStatus == 1)
  {
  if ($scope.firstClickedIndex === -1) {
    $scope.firstClickedIndex = index;
  } else if ($scope.lastClickedIndex === -1) {
    $scope.lastClickedIndex = index;
    var startIndex = Math.min($scope.firstClickedIndex, $scope.lastClickedIndex);
    var endIndex = Math.max($scope.firstClickedIndex, $scope.lastClickedIndex);
    for (var i = startIndex + 1; i < endIndex; i++) {
      $scope.fuelRawData[i].recordStatus = true;
      // $scope.fuelRawData[i].dt = fuel.dt;
      $scope.dates.recordStatus = true;
      $scope.dates.deviceTime.push($scope.fuelRawData[i].dt);
    }
    $scope.firstClickedIndex = -1;
    $scope.lastClickedIndex = -1 ;  
  }
}
  $scope.showSubmit = true;
  $scope.dates.recordStatus = true;
  $scope.dates.deviceTime.push(fuel.dt);
};


$scope.submitDates=function(){
  startLoading1();
  $scope.firstClickedIndex = -1;
    $scope.lastClickedIndex = -1 ;  
  var vehid=localStorage.getItem('vehicleId')!=null?localStorage.getItem('vehicleId'):$scope.vehIds;
  $http.post(GLOBAL.DOMAIN_NAME+'/getFuelFilteredData', $scope.dates).
  success(function(data, status, headers, config) {

    console.log('success');
    $scope.dates.deviceTime=[];
    var additionalURL= "",noiseURL= "", estURL = "", errorEstURL = "";
    if(noise){
      noiseURL = "&R="+noise;
    }else{
      noiseURL = "&R=1200";
    }
    if(est){
      estURL = "&Q="+est;
    }
    if(errorEst){
      errorEstURL = "&P="+errorEst;
    }
    additionalURL = noiseURL+estURL+errorEstURL;
    if((checkXssProtection($scope.uiDate.fromdate) == true) && ((checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true))) {                 
      if($scope.sensorCount>1){
        if($scope.sensor=='All'){
          fuelRawUrl  =  GLOBAL.DOMAIN_NAME+'/getFuelRawDataAll?vehicleId='+vehid+'&fromTimeUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toTimeUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime))+"&interval="
        }else{
          fuelRawUrl  =  GLOBAL.DOMAIN_NAME+'/getFuelRawData?vehicleId='+vehid+'&fromTimeUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toTimeUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime))+"&interval="+$scope.interval+"&sensor="+$scope.sensor;
        }
      }else{
        fuelRawUrl  =  GLOBAL.DOMAIN_NAME+'/getFuelRawData?vehicleId='+vehid+'&fromTimeUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toTimeUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime))+"&interval="+$scope.interval;        
      }
      fuelRawUrl = fuelRawUrl+additionalURL;
      var expdate=moment().add(expiryDays,'days').format('DD-MM-YYYY'),
      convertedexpdate=utcFormat(expdate,convert_to_24h('11:59 PM')),
      convertedtodate=utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime)),
      convertedfromdate=utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime));
      if(convertedtodate<=convertedexpdate && convertedfromdate<=convertedexpdate){
       $http.get(fuelRawUrl).success(function(data) { 
        $scope.fuelRawData=data;
        stopLoading1();

      });
     }
     else{
      stopLoading1();
      $scope.errMsg=licenceExpiry;
      $scope.showErrMsg = true;
      $scope.hideGraph = true;
      $scope.datashow=true;
    } 

  }
}).
  error(function(data, status, headers, config) {
    console.log('failed');

  });
  $scope.showRecords('filltheft')
}




function sessionValue(vid, gname,licenceExpiry){
  localStorage.setItem('user', JSON.stringify(vid+','+gname));
  localStorage.setItem('licenceExpiry', licenceExpiry);
  $("#testLoad").load("../public/menu");
}

function convert_to_24h(time_str) {
    //console.log(time_str);
    var str       =  time_str.split(' ');
    var stradd    =  str[0].concat(":00");
    var strAMPM   =  stradd.concat(' '+str[1]);
    var time      =  strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
    var hours     =  Number(time[1]);
    var minutes   =  Number(time[2]);
    var seconds   =  Number(time[2]);
    var meridian  =  time[4].toLowerCase();

    if (meridian == 'p' && hours < 12) {
      hours = hours + 12;
    }
    else if (meridian == 'a' && hours == 12) {
      hours = hours - 12;
    }     
    var marktimestr = ''+hours+':'+minutes+':'+seconds;      
    return marktimestr;
  };

    // millesec to day, hours, min, sec
    $scope.msToTime = function(ms) {
      days = Math.floor(ms / (24 * 60 * 60 * 1000));
      daysms = ms % (24 * 60 * 60 * 1000);
      hours = Math.floor((ms) / (60 * 60 * 1000));
      hoursms = ms % (60 * 60 * 1000);
      minutes = Math.floor((hoursms) / (60 * 1000));
      minutesms = ms % (60 * 1000);
      seconds = Math.floor((minutesms) / 1000);
    // if(days==0)
    //  return hours +" h "+minutes+" m "+seconds+" s ";
    // else
    return hours +":"+minutes+":"+seconds;
  }

  $scope.msToTime2 = function(ms) {

    days       =  Math.floor(ms / (24 * 60 * 60 * 1000));
    daysms     =  ms % (24 * 60 * 60 * 1000);
    hours      =  Math.floor((daysms) / (60 * 60 * 1000));
    hoursms    =  ms % (60 * 60 * 1000);
    minutes    =  Math.floor((hoursms) / (60 * 1000));
    minutesms  =  ms % (60 * 1000);
    seconds    =  Math.floor((minutesms) / 1000);

    if(days>1) {
      return days+":"+hours+":"+minutes+":"+seconds;
    } else if(days==1) {
      return days+":"+hours+":"+minutes+":"+seconds;
    } else if(days==0) {
      return hours +":"+minutes+":"+seconds;
    }
  }

  var delayed4 = (function () {
    var queue = [];

    function processQueue() {
      if (queue.length > 0) {
        setTimeout(function () {
          queue.shift().cb();
          processQueue();
        }, queue[0].delay);
      }
    }

    return function delayed(delay, cb) {
      queue.push({ delay: delay, cb: cb });

      if (queue.length === 1) {
        processQueue();
      }
    };
  }());

  function google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent) {
    vamoservice.getDataCall(tempurlEvent).then(function(data) {
      $scope.addressEvent[index4] = data.results[0].formatted_address;
      //console.log(' address '+$scope.addressEvent[index4])
      //var t = vamo_sysservice.geocodeToserver(latEvent,lonEvent,data.results[0].formatted_address);
    })
  };

  $scope.recursiveEvent   =   function(locationEvent, indexEvent){
    var index4 = 0;
    angular.forEach(locationEvent, function(value ,primaryKey){
    //console.log(' primaryKey '+primaryKey)
    index4 = primaryKey;
    if(locationEvent[index4].address == undefined)
    {
      var latEvent     =  locationEvent[index4].latitude;
      var lonEvent     =  locationEvent[index4].longitude;
      var tempurlEvent =  "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latEvent+','+lonEvent+"&sensor=true";
      delayed4(2000, function (index4) {
        return function () {
          google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent);
        };
      }(index4));
    }
  })
  }


  function formatAMPM(date) {
    var date = new Date(date);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
      return strTime;
    }

    //get the value from the ui
    $scope.getfromtodates=function(frmdate,todate){
     var startDate = moment(frmdate, 'DD-MM-YYYY');
     var endDate = moment(todate, 'DD-MM-YYYY');

     var dayDiff = endDate.diff(startDate, 'days');
     if(dayDiff>2){
      $scope.showInterval=false;
      $scope.interval="5";
    }
    else {
     $scope.showInterval=true;
   }

 }

 function getUiValue(){
  $scope.dates=[];
  $scope.uiDate.fromdate   =  $('#dateFrom').val();
  $scope.uiDate.fromtime   =  $('#timeFrom').val();
  $scope.uiDate.todate     =  $('#dateTo').val();
  $scope.uiDate.totime     =  $('#timeTo').val();
  // if(localStorage.getItem('timeTochange')!='yes'){
  //   updateToTime();
  //   $scope.uiDate.totime    =   localStorage.getItem('toTime');
  // }
  var vehid=localStorage.getItem('vehicleId')!=null?localStorage.getItem('vehicleId'):$scope.vehIds;

       // var re=moment($scope.uiDate.fromdate).diff(moment($scope.uiDate.todate), 'days');
       $scope.dates={userId:$scope.userName,vehicleId:vehid,fromTimeUtc:utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime)),toTimeUtc:utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime)),deviceTime:[],recordStatus:false};
       $scope.uiDate.fromtimes  =  convert_to_24h($scope.uiDate.fromtime);
       $scope.uiDate.totimes    =  convert_to_24h($scope.uiDate.totime);
     }


     function setBtnEnable(btnName){
       $scope.yesterdayDisabled = false;
       $scope.todayDisabled = false;


       switch(btnName){

        case 'yesterday':
        $scope.yesterdayDisabled = true;
        break;
        case 'today':
        $scope.todayDisabled = true;
        break;
      }


    }
    setBtnEnable("init");
    $scope.durationFilter    =   function(duration){
  //alert('inside function');
  startLoading();
  var now = new Date();
  $scope.uiDate.todate      = getTodayDate(now.setDate(now.getDate() - 1));
  switch(duration){

    case 'yesterday':
    setBtnEnable('yesterday');
    var d = new Date();

    localStorage.setItem('fromDate',getTodayDate(d.setDate(d.getDate() -1 )));
    localStorage.setItem('toDate',getTodayDate(d.setDate(d.getDate())));
    $scope.uiDate.totime  = '11:59 PM';
    location.reload();
      // datechange();
      break;
      case 'today':
      setBtnEnable('today');
      var d=new Date();
      localStorage.setItem('fromDate',getTodayDate(d.setDate(d.getDate() )));
      localStorage.setItem('toDate',getTodayDate(d.setDate(d.getDate() )));
      $scope.uiDate.totime       = formatAMPM(d);
      location.reload();
      
      //datechange();
      break;
    }
    $scope.getfromtodates($scope.uiDate.fromdate,$scope.uiDate.todate);
    filteredData();
    
  }
  $scope.roundOffDecimal = function(val) {
    return parseFloat(val).toFixed(2);
  }
  $scope.FuelValParseFloat = function(val) {
    return Number.parseFloat(val).toFixed(2);
  }
  function webCall() {

     /*   var urlAllow    =  true; 
        var fromTms     =  utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime));
        var toTms       =  utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
        var totalTms    =  toTms-fromTms;                       
        var splitTimes  =  $scope.msToTime2(totalTms).split(':');
        var daysDiff    =  0;

          if( splitTimes.length == 4 ) {
            daysDiff = splitTimes[0];
          }
                        
      //alert(daysDiff);

      if( daysDiff == 0 ) { */

       var fuelRawUrl  = "";
       var vehid=localStorage.getItem('vehicleId')!=null?localStorage.getItem('vehicleId'):$scope.vehIds;
       alreadyclicked = false;
       $scope.fuelfill=false;
       $scope.datashow = false;
       $scope.error= '';
       $scope.millage =""
       $scope.ltphValue = "";
       $scope.ignitionOnPer = "";
       $scope.minFuelTheft = "";
       $scope.customMinFuelTheftFilterValue = "";
       var d1 = Date.parse($scope.uiDate.fromdate);
       var d2 = Date.parse($scope.uiDate.todate);
       
       if (d1 > d2) {
        $scope.fuelRawData = [];
        $scope.error="Please Select Valid Date Range";
        $scope.hideGraph = true;
        $scope.datashow=true;
        stopLoading1();
      }else{
       var additionalURL= "",noiseURL= "", estURL = "", errorEstURL = "";
       if(noise){
        noiseURL = "&R="+noise;
      }else{
        noiseURL = "&R=1200";
      }
      if(est){
        estURL = "&Q="+est;
      }
      if(errorEst){
        errorEstURL = "&P="+errorEst;
      }
      additionalURL = noiseURL+estURL+errorEstURL;

      if((checkXssProtection($scope.uiDate.fromdate) == true) && ((checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true))) {                 
       if($scope.sensorCount>1){
        if($scope.sensor=='All'){
          fuelRawUrl  =  GLOBAL.DOMAIN_NAME+'/getFuelRawDataAll?vehicleId='+vehid+'&fromTimeUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toTimeUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime))+"&interval="
        }else{
          fuelRawUrl  =  GLOBAL.DOMAIN_NAME+'/getFuelRawData?vehicleId='+vehid+'&fromTimeUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toTimeUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime))+"&interval="+$scope.interval+"&sensor="+$scope.sensor;
        }
      }else{
        fuelRawUrl  =  GLOBAL.DOMAIN_NAME+'/getFuelRawData?vehicleId='+vehid+'&fromTimeUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toTimeUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime))+"&interval="+$scope.interval;        
      }
      fuelRawUrl = fuelRawUrl+additionalURL 
             //var fuelRawUrl  = 'http://128.199.159.130:9000/getFuelRawData?userId=naplogi&vehicleId=NAPLOGI-AP16TE3458&fromTimeUtc=1533037324617&toTimeUtc=1533195298000';
               //console.log( fuelRawUrl );
               $http.get(fuelRawUrl).success(function(data) { 

                if(data=='Failed'){
                  stopLoading1();
                  $scope.error = translate("curl_error") ;
                  $scope.hideGraph = true;
                  $scope.datashow=true;
                }else{
                 if($scope.sensor==1&&document.getElementById("sensor1")){
                  document.getElementById("sensor1").disabled = true;
                }
                if(data.length) {
                        // $scope.fuelType=data[0].fuelType;
                        //alert(data.length);

                        if(data.length>1){

                         $scope.fuelType=data[0].fuelType;
                         setFuelType($scope.fuelType);
                         $scope.totalData=data.length;
                         $scope.voltageValue     = data[0].voltageValue;
                         $scope.engineType     = data[0].engineType;
                       }
                       $scope.fuelRaw  =  data;
                       if(data[0].tankSize==undefined)
                       {
                        $scope.tankSize =0.0;
                      }
                      else{
                        $scope.tankSize     =  parseInt(data[0].tankSize);
                      }
                      if(data[data.length-1].fuelLitr==undefined)
                      {
                        $scope.fueLiter = 0.0;
                      }
                      else
                      {
                       $scope.fueLiter = parseInt(data[data.length-1].fuelLitr);
                     }
                        //console.log($scope.fueLiter);
                        if($scope.fuelRaw[0].error!=undefined){
                          $scope.error=$scope.fuelRaw[0].error;
                          
                          $scope.hideGraph = true;
                          $scope.datashow=true;
                          $scope.fuelRawData=$scope.fuelRaw;
                        }else{
                          $scope.hideGraph = false;
                        }
                        //alert($scope.tankSize);
                        fuelRawGraph(data);
                        fuelFillTheftCall();
                        if(!$scope.fuelRaw[0].error){

                          $scope.fuelfill=true;
                          if($scope.theftBeta == true){
                            $scope.sort  =  sortByDate('startTime', false);
                          }
                        }else{
                          stopLoading1();
                        }
                      }  
                    }

                  }); 


             }  else {

               stopLoading1();
             }

           }



    /*  } else {

          $scope.fuelRawData = [];
          document.getElementById("container").innerHTML = '';

          alert('Plaese select less than 1 day.');
          stopLoading1(); 
  
        }*/
      }

      function setFuelType(fueltype){
        if($scope.sensor=='All'){
          if(fueltype.includes('serial')){
            $scope.fuelType=translate('serial_value');
          }
          else if(fueltype.includes('analog')) {
            $scope.fuelType=translate('volt');
          }
          else {
            $scope.fuelType=translate('value');
          }
        }
        else {
          if(fueltype.includes('serial')){
            $scope.fuelType=translate('serial_value');
          }
          else {
            $scope.fuelType=translate('volt');
          }
        }
      }
      function fuelFillTheftCall() {

       startLoading1();
       var vehid=localStorage.getItem('vehicleId')!=null?localStorage.getItem('vehicleId'):$scope.vehIds;
       $scope.fuelRunningTheft  = [];
       if((checkXssProtection($scope.uiDate.fromdate) == true) && ((checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true))) {                 

         var fuelMachineUrl  = "";

         if($scope.sensorCount>1){
          if($scope.sensor=='All'){
            fuelMachineUrl  =  GLOBAL.DOMAIN_NAME+'/getFuelDetailForAll?vehicleId='+vehid+'&fromDateTime='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateTime='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
          }else{
            fuelMachineUrl  =  GLOBAL.DOMAIN_NAME+'/getFuelDetailForMachinery?vehicleId='+vehid+'&fromDateTime='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateTime='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime))+"&sensor="+$scope.sensor;
          }
        }else{
         fuelMachineUrl  =  GLOBAL.DOMAIN_NAME+'/getFuelDetailForMachinery?vehicleId='+vehid+'&fromDateTime='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateTime='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
       }
       console.log( fuelMachineUrl );
       $http.get( fuelMachineUrl ).success(function(data) { 
         stopLoading1();

         if(data=='Failed'){
          $scope.error = { 
            'error' : translate("curl_error"),
            fuelFills: [ ],
            fuelDrops: [ ],
            fuelFillBeta: [ ],
            fuelDropBeta: [ ],
            fuelRunningTheft: [ ],
          };
        }else{

          if($scope.sensor==1&&document.getElementById("sensor1")){
            document.getElementById("sensor1").disabled = true;
          }
          $scope.fuelMachineData = Object.assign({}, data);
          
          if(!data.error){

            fuelFillTheftGraph(data);

            if($scope.sensor!='All'){
              totalFuelTheftCalculation($scope.fuelMachineData.fuelRunningTheft);
              if(tab == "fuelDispenser"){

                if($scope.fuelMachineData.sensorMode == "Dispenser"){
                  console.log('Dispenser');


                }else{
                  $scope.fuelMachineData.fuelDrops = null;
                }
              }
                    // added for default filter
                    $scope.millage = data.expectedMileage;
                    $scope.ltphValue = data.expectedMileage;
                    if(data.excessConsumptionFilter=='yes'){
                      $scope.showFueltheftFilter = true;
                      $scope.handleFuelFilter('');
                    }else{
                      $scope.showFueltheftFilter = false;
                    }
                    $scope.applyFilter();
                  }
                }
              }
            });


     }  else {

       stopLoading1();
     }

   }

   function filteredData(){
    var vehid=localStorage.getItem('vehicleId')!=null?localStorage.getItem('vehicleId'):$scope.vehIds;
    $scope.filtData=[];
    var additionalURL= "",noiseURL= "", estURL = "", errorEstURL = "";
    if(noise){
      noiseURL = "&R="+noise;
    }else{
      noiseURL = "&R=1200";
    }
    if(est){
      estURL = "&Q="+est;
    }
    if(errorEst){
      errorEstURL = "&P="+errorEst;
    }
    additionalURL = noiseURL+estURL+errorEstURL;
    if((checkXssProtection($scope.uiDate.fromdate) == true) && ((checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true))) {                 
      if($scope.sensorCount>1){
        if($scope.sensor=='All'){
          fuelRawUrl  =  GLOBAL.DOMAIN_NAME+'/getFuelRawDataAll?vehicleId='+vehid+'&fromTimeUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toTimeUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime))+"&interval="+"&zeroRecord="+false;
        }else{
          fuelRawUrl  =  GLOBAL.DOMAIN_NAME+'/getFuelRawData?vehicleId='+vehid+'&fromTimeUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toTimeUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime))+"&interval="+$scope.interval+"&zeroRecord="+false+"&sensor="+$scope.sensor;
        }
      }else{
        fuelRawUrl  =  GLOBAL.DOMAIN_NAME+'/getFuelRawData?vehicleId='+vehid+'&fromTimeUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toTimeUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime))+"&interval="+$scope.interval+"&zeroRecord="+false;        
      }
      fuelRawUrl = fuelRawUrl+additionalURL;
      var expdate=moment().add(expiryDays,'days').format('DD-MM-YYYY'),
      convertedexpdate=utcFormat(expdate,convert_to_24h('11:59 PM')),
      convertedtodate=utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime)),
      convertedfromdate=utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime));
      if(convertedtodate<=convertedexpdate && convertedfromdate<=convertedexpdate){
       $http.get(fuelRawUrl).success(function(data) { 
        $scope.filtData=data;
        webCall();
        stopLoading1();
      });

     }
     else {
      stopLoading1();
      $scope.errMsg=licenceExpiry;
      $scope.showErrMsg = true;
      $scope.hideGraph = true;
      $scope.datashow=true;
    }

  }
}

function fuelRawGraph() {

  startLoading1();

  var data = $scope.fuelRaw;
  var filt=$scope.filtData;
  var filtered=[];
  var Ltrs         =  [];
  var Speed        =  [];
  var fuelDate     =  [];
  var NormalizedData   =  [];
  var ignitionColorList   =  [];

  var Ignition   =  [];
  //var distCovered  =  [];
  //var spdVals      =  [];
  var tankSize = $scope.tankSize;

    //console.log(data);

    console.log( tankSize );

    //$scope.fueLiter = (data[data.length-1].fuelLitr);

    //console.log($scope.fueLiter);

    var fuelLiter = $scope.fueLiter;

    //console.log(fuelLiter);
    try {

      if(filt.length) {
        for(var i = 0; i < filt.length; i++) {
          if(filt[i].fuelLitr !='0' || filt[i].fuelLitr !='0.0') {
            filtered.push([parseInt(filt[i].dt) , parseFloat(filt[i].fuelLitr)]);
            
              //distCovered.push(data[i].distanceCovered);
              //spdVals.push(data[i].speed);
              //added for Ignition on off color start
              
              //endIndex
            }
          }
        }

      } catch(err) {
        console.log(err.message);
      }


      try {

        if(data.length) {
          for(var i = 0; i < data.length; i++) {
            if(data[i].fuelLitr !='0' || data[i].fuelLitr !='0.0') {
              Ltrs.push([parseInt(data[i].dt) , parseFloat(data[i].fuelLitr)]);
              Ignition.push([parseInt(data[i].dt) , 0]);
              Speed.push([parseInt(data[i].dt) , parseFloat(data[i].sp)]);
              NormalizedData.push([parseInt(data[i].dt) , parseFloat(data[i].kalmanLitre)]);
              var dat = $filter('date')(data[i].dt, "dd/MM/yyyy HH:mm:ss");
              fuelDate.push(dat);
              //distCovered.push(data[i].distanceCovered);
              //spdVals.push(data[i].speed);
              //added for Ignition on off color start
              if(i==0){
                if(data[i].primaryEngine=="OFF"){
                  ignitionColorList.push({ color: "#6f6d6d",value: data[i].dt});
                }else{
                  ignitionColorList.push({ color: "yellow",value: data[i].dt});
                }
              }else if(i==data.length-1){
                if(data[i].primaryEngine=="OFF"){
                  ignitionColorList.push({color: "#6f6d6d",value: data[i].dt});
                }else{
                  ignitionColorList.push({color: "yellow",value: data[i].dt});
                }
              }else if(data[i].primaryEngine!=data[i-1].primaryEngine){
                if(data[i].primaryEngine=="OFF"){
                  ignitionColorList.push({color: "yellow",value: data[i].dt});
                }else{
                  ignitionColorList.push({color: "#6f6d6d",value: data[i].dt});
                }
              }
              //end
            }
          }
        }

      } catch(err) {
        console.log(err.message);
      }

      

      var seriesList = [ {

        type: 'area',
        name: 'All Fuel Data',
        data: Ltrs,
        visible: $scope.dealerName === 'FUELVIEW' || $scope.dealerName === 'FUELVIEW1'? false : true,
        

        events: {
          legendItemClick: function() {

            this.chart.series.forEach(function(p, i) {

              // p.yAxis.update({
              //   tickInterval:60,
              //   max:$scope.tankSize
              // });

              if (p.name === 'Filtered Fuel Data') {
                if(p.visible==true){
                  p.hide();


                }
                
              }
            })
          }
        }
      },
      {
        type: 'area', 
        name: 'Filtered Fuel Data',
        color: '#7cb5ec',
        fillColor:'#7cb5ec',
        data: filtered,        
        visible: $scope.dealerName === 'FUELVIEW' || $scope.dealerName === 'FUELVIEW1'? true : false,

        events: {

          legendItemClick: function() {

            this.chart.series.forEach(function(p, i) {

              p.yAxis.update({
                tickAmount:5
              });

              if (p.name === 'All Fuel Data') {
                if(p.visible==true){
                  p.hide();


                }
                
              }
            })
          }

        }
      },{
        type: 'area',
        name: 'Normalize',
        color: 'rgb(128,0,0)',
        data: NormalizedData,
        fillColor: 'transparent',
        lineWidth: 3,
        visible: false,
        events: {

        }
      },{
        type: 'area',
        name: 'Engine',
        data: Ignition,
        pointStart: Date.UTC($scope.uiDate.fromdate),
        pointInterval: 1000*60,
        color: "yellow",
        fillColor: 'transparent',
        lineWidth: 4,
        visible: false,
        zoneAxis: 'x',
        zones: ignitionColorList,
        events: {

        }
      },{
        type: 'area',
        name: 'Speed',
        color: 'rgba(248,161,63,1)',
        data: Speed,
        fillColor: 'transparent',
        yAxis: 1,
        visible: false,
        events: {

        }
      },

      ];

  Highcharts.setOptions({                                            // This is for all plots, change Date axis to local timezone
    global : {
      useUTC : false
    }
  });
  var color1 ='#7cb5ec';
  var color2='#7cb5ec0f';
  fuelChart = Highcharts.chart('container', {
    chart: {
      zoomType: 'x',
              //alignTicks: true
            },
            title: {
              text: "Fuel & Speed"
            },
            credits: {
              enabled: false
            },

            xAxis: {
             type: 'datetime',
               // categories: fuelDate,
               title: {
                text: translate('Date&Time')
              }

            },
            
            yAxis: [{
              min: 0,
              max: parseFloat($scope.tankSize),
              title: {
               text: translate('Fuel(Ltrs)')

             },
             endOnTick: true    
           }, {
            min: 0,
            max: 100,
            title: {
             text: translate('Speed(km/h)')
           },
           endOnTick: true,
           opposite: true
         }],
         tooltip: {
          formatter: function() {
            var s, a = 0;

                //console.log(this.points);
                $.each(this.points, function() {
                  var date = $filter('date')(  this.x, "HH:mm:ss dd/MM/yyyy");
                  var fuelData      =  ($filter('filter')(this.series.name==='Filtered Fuel Data'?$scope.filtData:$scope.fuelRaw, 
                    {'dt':this.x}));
                      //console.log(fuelData);
                      var point = fuelData[0];
                      var deviceVolt = (point.deviceVolt=='-1.0') ? '0' :point.deviceVolt;
                      
                      if($scope.defaultMileage=='yes' ){
                        s = '<b>'+date+'</b>'+'</b>'+'<br>'+' '+'</br>'+
                        '--------------------'+'<br>'+' '+'</br>'+
                        translate('Fuel')+'     : ' +'  '+'<b>' + $scope.roundOffDecimal(point.fuelLitr) + '</b>'+' Ltrs'+'<br>'+' '+'</br>'+

                        translate('Engine')+' : ' +'  '+'<b>' + point.primaryEngine + '</b>'+'<br>'+' '+'</br>'+
                        translate('device_odo')+' : ' +'  '+'<b>' + point.odoMeterDevice + '</b>'+'<br>'+' '+'</br>'+

                        translate('Speed')+'      : ' +'  '+'<b>' + point.sp + '</b>'+'<br>'+' '+'</br>'+  
                        $scope.fuelType+' : ' +'  '+'<b>' + deviceVolt + '</b>'+'<br>'+' '+'</br>'+
                        translate('Veh_Battery')+'      : ' +'  '+'<b>' + point.mainBatteryVolt + '</b>';                      
                      }

                      else{
                        s = '<b>'+date+'</b>'+'</b>'+'<br>'+' '+'</br>'+
                        '--------------------'+'<br>'+' '+'</br>'+
                        translate('Fuel')+'     : ' +'  '+'<b>' + $scope.roundOffDecimal(point.fuelLitr) + '</b>'+' Ltrs'+'<br>'+' '+'</br>'+

                        translate('Engine')+' : ' +'  '+'<b>' + point.primaryEngine + '</b>'+'<br>'+' '+'</br>'+
                        translate('Odo')+' : ' +'  '+'<b>' + point.odoMeterReading + '</b>'+'<br>'+' '+'</br>'+
                        translate('Speed')+'      : ' +'  '+'<b>' + point.sp + '</b>'+'<br>'+' '+'</br>'+
                        $scope.fuelType+' : ' +'  '+'<b>' + deviceVolt + '</b>'+'<br>'+' '+'</br>'+
                        translate('Veh_Battery')+'      : ' +'  '+'<b>' + point.mainBatteryVolt + '</b>';  
                      }
                    });
                return s;
              },
              shared: true,
              crosshairs: true,
            },
            legend: {
              enabled: true,
              align: 'right',
              verticalAlign: 'middle',
              layout: 'vertical'
            },
            plotOptions: {
              area: {
                fillColor: {
                  linearGradient: {
                    x1: 0,
                    y1: 0,
                    x2: 0,
                    y2: 1
                  },
                  stops: [
                  [0, color1],
                  [1, color2]
                  ]
                },
                marker: {
                  radius: 2
                },
                lineWidth: 1,
                states: {
                  hover: {
                    lineWidth: 3
                  }
                },
                threshold: null,
                turboThreshold: 0
              }
            },
            
            series: seriesList,
          });

stopLoading1();

}
function fuelFillTheftGraph(data) {
  startLoading1();
  var fuelRaw = $scope.fuelRaw;
  var fuelFillTheftData = data;
  var Ltrs         =  [];
  var fuelFillColorList   =  [];
  var fuelDropColorList   =  [];
  try {
    angular.forEach(fuelRaw, function(fuel, key){
     if(fuel.fuelLitr !='0' || fuel.fuelLitr !='0.0') {
      Ltrs.push([parseInt(fuel.dt) , parseFloat(fuel.fuelLitr)]);
    }
  });
      //added for fuel fill
      angular.forEach(fuelFillTheftData.fuelFills, function(val, key){

        if(key==0) { 
          fuelFillColorList.push({color:   Highcharts.getOptions().colors[0], value: utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime)) });
          fuelFillColorList.push({color:   Highcharts.getOptions().colors[0], value: utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime)) })
        }
        var fuelFillStartData = $scope.fuelRaw.filter(fuel => fuel.dt >= val.startTime);
        var startIndex = $scope.fuelRaw.findIndex(element => element.dt == fuelFillStartData[0].dt)
        if(startIndex!=0){ fuelFillColorList.push({color:   Highcharts.getOptions().colors[0],value: parseInt($scope.fuelRaw[startIndex-1].dt)}); }

        var fuelFillEndData = $scope.fuelRaw.filter(fuel => fuel.dt <= val.time);
        var endIndex = $scope.fuelRaw.findIndex(element => element.dt == fuelFillEndData[fuelFillEndData.length-1].dt)
          //fuel fill start
          fuelFillColorList.push({color: "green",value: fuelFillStartData[0].dt});
          fuelFillColorList.push({color: "green",value: fuelFillEndData[fuelFillEndData.length-1].dt});
          //end
          if((endIndex+1)!=$scope.fuelRaw.length){ fuelFillColorList.push({color:   Highcharts.getOptions().colors[0],value: parseInt($scope.fuelRaw[endIndex+1].dt)}); }
          if(key+1==fuelFillTheftData.fuelFills.length) { 
           fuelFillColorList.push({color:   Highcharts.getOptions().colors[0], value: parseInt($scope.fuelRaw[$scope.fuelRaw.length-1].dt) })
         }
       });
      //fuel Drops start
      //added for fuel Drops
      angular.forEach(fuelFillTheftData.fuelDrops, function(val, key){
        var endColorData;
        if(key==0) { 
          if(val.startTime < utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))){
           fuelDropColorList.push({color: "red", value: parseInt($scope.fuelRaw[0].dt) });
         }else{
          endColorData = $scope.fuelRaw.filter(fuel => fuel.dt < val.startTime);
          if(endColorData.length==0){
            endColorData = $scope.fuelRaw.filter(fuel => fuel.dt <= val.startTime);
          }
          fuelDropColorList.push({color:   Highcharts.getOptions().colors[0], value: utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime)) });
          fuelDropColorList.push({color: Highcharts.getOptions().colors[0],value: parseInt(endColorData[endColorData.length-1].dt) });
        }
      }else{
        endColorData = $scope.fuelRaw.filter(fuel => fuel.dt < val.startTime);
        if(endColorData.length==0){
          endColorData = $scope.fuelRaw.filter(fuel => fuel.dt <= val.startTime);
        }
        fuelDropColorList.push({color: Highcharts.getOptions().colors[0],value: parseInt(endColorData[endColorData.length-1].dt) });
      }
      var fuelTheftStartData = $scope.fuelRaw.filter(fuel => fuel.dt >= val.startTime);
      var startIndex = $scope.fuelRaw.findIndex(element => element.dt == fuelTheftStartData[0].dt)


      var fuelTheftEndData = $scope.fuelRaw.filter(fuel => fuel.dt <= val.time);
      var endIndex = $scope.fuelRaw.findIndex(element => element.dt == fuelTheftEndData[fuelTheftEndData.length-1].dt)
          //fuel drop start
          fuelDropColorList.push({color: "red",value: parseInt(fuelTheftStartData[0].dt)});
          fuelDropColorList.push({color: "red",value: parseInt(fuelTheftEndData[fuelTheftEndData.length-1].dt) });
          //end
          //if(startIndex!=0){ fuelDropColorList.push({color:   Highcharts.getOptions().colors[0],value: parseInt($scope.fuelRaw[startIndex-1].dt)}); }
          if((endIndex+1)!=$scope.fuelRaw.length){ fuelDropColorList.push({color:   Highcharts.getOptions().colors[0],value: parseInt($scope.fuelRaw[endIndex+1].dt)}); }
          if(key+1==fuelFillTheftData.fuelDrops.length) { 
            fuelDropColorList.push({color:   Highcharts.getOptions().colors[0], value: parseInt($scope.fuelRaw[$scope.fuelRaw.length-1].dt) })
          }
        });
     //fuel Drops end


     console.log(fuelDropColorList)
   } catch(err) {
    console.log(err.message);
  }
  
  if(fuelFillTheftData.fuelFills!=null){ 
    if(fuelFillTheftData.fuelFills.length!=0) {
      fuelChart.addSeries({
        type: 'area',
        name: 'Fuel Fill',
        data: Ltrs,
        pointStart: Date.UTC($scope.uiDate.fromdate),
        pointInterval: 1000*60,
        color: "green",
        fillColor: 'transparent',
        lineWidth: 3,
        visible: false,
        zoneAxis: 'x',
        zones: fuelFillColorList
      })
    }
  }
  if(fuelFillTheftData.fuelDrops!=null){ 
    if(fuelFillTheftData.fuelDrops.length!=0 && data.sensorMode!='Dispenser') {
      fuelChart.addSeries({
        type: 'area',
        name: 'Fuel Theft',
        data: Ltrs,
        pointStart: Date.UTC($scope.uiDate.fromdate),
        pointInterval: 1000*60,
        color: "red",
        fillColor: 'transparent',
        lineWidth: 3,
        visible: false,
        zoneAxis: 'x',
        zones: fuelDropColorList
      })
    }
  }
  stopLoading1();


}


  // initial method
  $scope.$watch("url", function(val) {
    vamoservice.getDataCall($scope.url).then(function(data) {
    //startLoading1();
      //$scope.selectVehiData = [];
      $scope.vehicle_group = [];
      $scope.vehicle_list  = data;


      if(data.length) {

        $scope.vehiname = getParameterByName('vid');
        $scope.uiGroup  = $scope.trimColon(getParameterByName('vg'));
        $scope.gName    = getParameterByName('vg');

        angular.forEach(data, function(val, key){
        //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
        if($scope.gName == val.group){

         $scope.gIndex = val.rowId;
            //alert( $scope.gName );
            angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys){
          //$scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});
          if($scope.vehiname == value.vehicleId){
           $scope.shortNam = value.shortName;
           $scope.vehIds   = value.vehicleId;
           licenceExpiry=value.licenceExpiry;
           expiryDays=value.expiryDays;
           $scope.defaultMileage=value.defaultMileage;
           $scope.sensorCount   = value.sensorCount;
           $scope.vehicleMode   = value.vehicleMode;
           $scope.position=value.position;
                   //alert($scope.vehIds);
                 }
               });
          }
        });
        var userNameVal       =  localStorage.getItem('userIdName').split(',');
        var userVals          =  userNameVal[1].split('"');
        $scope.userName          =  userVals[0];


      //console.log($scope.selectVehiData);
      sessionValue($scope.vehiname, $scope.gName,licenceExpiry)
      $scope.notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
      $('#notncount').text($scope.notncount);
      window.localStorage.setItem('totalNotifications',$scope.notncount);
    }
      //$scope.interval       =   '';
      var d=new Date();
      $scope.uiDate.fromdate    = localStorage.getItem('fromDate');
      $scope.uiDate.fromtime    = localStorage.getItem('fromTime');
      $scope.uiDate.todate    = localStorage.getItem('toDate');
      $scope.uiDate.totime    =  localStorage.getItem('toTime');
      if(localStorage.getItem('timeTochange')!='yes'){
        updateToTime();
        $scope.uiDate.totime    =   localStorage.getItem('toTime');
      }
      $scope.uiDate.fromtimes  =   convert_to_24h($scope.uiDate.fromtime);
      $scope.uiDate.totimes    =   convert_to_24h($scope.uiDate.totime);
      $scope.getfromtodates($scope.uiDate.fromdate,$scope.uiDate.todate);
      if(localStorage.getItem('showShortName')=='true'){
        $scope.shortNam=localStorage.getItem('shortName');
        $scope.vehiname=localStorage.getItem('vehicleId');
      }
      
      if($scope.uiDate.fromdate==getTodayDate(d.setDate(d.getDate())) && $scope.uiDate.todate==getTodayDate(d.setDate(d.getDate())) ){
        setBtnEnable('today');
        
      }
      if($scope.uiDate.fromdate==getTodayDate(d.setDate(d.getDate()-1)) && $scope.uiDate.todate==getTodayDate(d.setDate(d.getDate())-1) ){
        setBtnEnable('yesterday');
        
      }


    //$scope.uiDate.totime     =   '11:59 PM';
    var vehid=localStorage.getItem('vehicleId')!=null?localStorage.getItem('vehicleId'):$scope.vehIds;
    $scope.dates={userId:$scope.userName,vehicleId:vehid,fromTimeUtc:utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime)),toTimeUtc:utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime)),deviceTime:[],recordStatus:false};
    startLoading1();
    filteredData();

    //stopLoading1();
  }); 
  });

  trimFcode=function(groupname){
   if(groupname){
     var spltVal = groupname.split(":");
     return spltVal[1];
   }
 }

 $scope.groupSelection = function(groupName, groupId) {
  startLoading1();
    clearMultipleSensor(); // added for multiple sensor clear
    $scope.sensor = 1;
    $scope.datashow=false;
    $scope.error="";
    $scope.totalData="";
    licenceExpiry="";
    $scope.errMsg="";
    $scope.gName    =  groupName;
    $scope.uiGroup  =  $scope.trimColon(groupName);
    $scope.gIndex   =  groupId;
    var url         =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+groupName;

    vamoservice.getDataCall(url).then(function(response) {

      $scope.vehicle_list = response;
      $scope.shortNam     = response[$scope.gIndex].vehicleLocations[0].shortName;
      $scope.vehiname     = response[$scope.gIndex].vehicleLocations[0].vehicleId;
      licenceExpiry   = response[$scope.gIndex].vehicleLocations[0].licenceExpiry;
      expiryDays   = response[$scope.gIndex].vehicleLocations[0].expiryDays;
      $scope.defaultMileage = response[$scope.gIndex].vehicleLocations[0].defaultMileage;
      $scope.sensorCount   = response[$scope.gIndex].vehicleLocations[0].sensorCount;
      $scope.vehicleMode   = response[$scope.gIndex].vehicleLocations[0].vehicleMode;
      sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
      $scope.notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
      $('#notncount').text($scope.notncount);
      window.localStorage.setItem('totalNotifications',$scope.notncount);
      $scope.selectVehiData = [];
    //console.log(response);
    angular.forEach(response, function(val, key) {
      if($scope.gName == val.group) {
          //$scope.gIndex = val.rowId;
          angular.forEach(response[$scope.gIndex].vehicleLocations, function(value, keys) {

            $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

            if($scope.vehiname == value.vehicleId) {
              $scope.shortNam = value.shortName;
              $scope.vehIds   = value.vehicleId;
            }
          });
        }
      });
    var expdate=moment().add(expiryDays,'days').format('DD-MM-YYYY'),
    convertedexpdate=utcFormat(expdate,convert_to_24h('11:59 PM')),
    convertedtodate=utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime)),
    convertedfromdate=utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime));
    if(convertedtodate>convertedexpdate){
     stopLoading1();
     $scope.errMsg=licenceExpiry;
     $scope.showErrMsg = true;
     $scope.datashow=true;
     $scope.hideGraph=true;
     
   }else{
    getUiValue();
    filteredData();

  }

      //stopLoading1();
    });

  }

  $scope.genericFunction  = function (vehid, index,shortname,position, address,groupName,licenceExp,serial1,defaultMileage){

    startLoading1();
    clearMultipleSensor(); // added for multiple sensor clear
    $scope.sensor = 1;
    licenceExpiry=licenceExp;
    $scope.defaultMileage = defaultMileage;
    $scope.errMsg="";
    $scope.fuelRawData=[];
    $scope.datashow=false;
    $scope.error="";
    $scope.totalData="";
    $scope.vehiname = vehid;
    localStorage.setItem('vehicleId',vehid);
    $scope.position=position;
    sessionValue($scope.vehiname, $scope.gName,licenceExpiry)
    angular.forEach($scope.vehicle_list[$scope.gIndex].vehicleLocations, function(val, key){
      if(vehid == val.vehicleId){
        $scope.shortNam = val.shortName;
        localStorage.setItem('shortName',val.shortName);
        localStorage.setItem('showShortName',true);
        $scope.vehIds   = val.vehicleId;
        $scope.sensorCount   = val.sensorCount;
        $scope.vehicleMode   = val.vehicleMode;
        expiryDays=val.expiryDays;
      }
    });


    var expdate=moment().add(expiryDays,'days').format('DD-MM-YYYY'),
    convertedexpdate=utcFormat(expdate,convert_to_24h('11:59 PM')),
    convertedtodate=utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime)),
    convertedfromdate=utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime));
    if(convertedtodate>convertedexpdate){
      stopLoading1();
      $scope.errMsg=licenceExpiry;
      $scope.showErrMsg = true;
      $scope.datashow=true;
      $scope.hideGraph=true;
    }else{
      getUiValue();
      filteredData();
      
    }
  }

  $scope.intervalChange=function(interval){
    localStorage.setItem('interval',interval);
  }

  $scope.submitFunction   = function(){

    $scope.dates.deviceTime=[];
    $scope.yesterdayDisabled = false;
    $scope.todayDisabled = false;
    $scope.monthDisabled = false;
    $scope.datashow=false;
    $scope.error="";
    $scope.totalData="";
    startLoading1();
  //$scope.interval="";
  var expdate=moment().add(expiryDays,'days').format('DD-MM-YYYY'),
  convertedexpdate=utcFormat(expdate,convert_to_24h('11:59 PM')),
  convertedtodate=utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime)),
  convertedfromdate=utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime));
  if(convertedtodate<=convertedexpdate && convertedfromdate<=convertedexpdate){
    getUiValue();
    var startDate = moment($scope.uiDate.fromdate, 'DD-MM-YYYY');
    var endDate = moment($scope.uiDate.todate, 'DD-MM-YYYY');

    var dayDiff = endDate.diff(startDate, 'days');
    if(dayDiff>2){
      $scope.showFuelData=true;
      $scope.showInterval=false;
    }
    else {
      $scope.showInterval=true;
      $scope.showFuelData=false;
    }
    filteredData();
    
  }else{     
   stopLoading1();
   $scope.errMsg=licenceExpiry;
   $scope.showErrMsg = true;
   $scope.datashow=true;
   $scope.hideGraph=true;
 } 
  //webServiceCall();
    //stopLoading1();
  }
  function clearMultipleSensor(){
    if($scope.sensorCount>1){
      if($scope.sensor=="All"){
        document.getElementById("sensorAll").disabled = false;
      }else{
        for (var i = 1; i <= $scope.sensorCount ; i++) {
          if(i==$scope.sensor){
            document.getElementById("sensor"+i).disabled = false;
          }
        }
      }
    }
  }
  $scope.sensorChange   = function(sensorNo){
    startLoading1();
    console.log("sensor  " + sensorNo);
    //added for multiple sensors
    for (var i = 1; i <= $scope.sensorCount ; i++) {
      if(i==sensorNo){
       document.getElementById("sensor"+i).disabled = true;
     }else{
       document.getElementById("sensor"+i).disabled = false;
     }
   }
    // added for all sensor 
    if(sensorNo=='All'){
      document.getElementById("sensorAll").disabled = true;
    }else{
      document.getElementById("sensorAll").disabled = false;
    }
    $scope.sensor=sensorNo;
    filteredData();
    
  }

  $scope.exportData = function (data) {
    //console.log(data);
    var blob = new Blob([document.getElementById(data).innerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, data+".xls");
  };

  $scope.exportDataCSV = function (data) {
    //console.log(data);
    CSV.begin('#'+data).download(data+'.csv').go();
  };
  $scope.showRecords = function (type) {
    if(type=='filltheft'){
      $scope.datashow=false;
      $scope.fuelfill=true;  
      // if(!alreadyclicked){
      //   alreadyclicked = true;
      //   fuelFillTheftCall();
      // }
    }
    else{
      startLoading1();
      $scope.fuelfill=false;
      setTimeout(function() {
        $scope.$apply(function() {
         $scope.fuelRawData=$scope.fuelRaw;
         $scope.datashow=true;
       });
        $scope.showSubmit=false;
        
        stopLoading1();
        if(!fixedHeaderInitiated){
          $('.table-fixed-header1').fixedHeader();
        }
        fixedHeaderInitiated = true;
      }, $scope.fuelRaw.length);

    }
    

    
  };
  function totalFuelTheftCalculation(fuelRunningTheft){
    $scope.totalFuelTheftBeta = 0;
    angular.forEach(fuelRunningTheft, function(val, key){ $scope.totalFuelTheftBeta +=val.fuelTheftBeta });
  }
  $scope.handleFuelTheftBeta = function (val) {
    if(val=='yes'){
      $scope.theftBeta = true;
      $scope.sort         =  sortByDate('startTime', false);
    }else{
      $scope.theftBeta = false;
      $scope.sort         =  sortByDate('dt', false);
    }

  }
  $scope.handleFuelFilter = function (action){
   if(action=='btnClick'){
     $scope.showFueltheftFilter = !$scope.showFueltheftFilter;
   }
   if(!$scope.showFueltheftFilter){
     $scope.minFuelTheft = "";
     $scope.customMinFuelTheftFilterValue = "";
   }else{
    if($scope.fuelMachineData.minFuelTheft=="5"||$scope.fuelMachineData.minFuelTheft=="10"||$scope.fuelMachineData.minFuelTheft=="20"||$scope.fuelMachineData.minFuelTheft=="30"||$scope.fuelMachineData.minFuelTheft=="40"||$scope.fuelMachineData.minFuelTheft=="50"){
      $scope.minFuelTheft = $scope.fuelMachineData.minFuelTheft;
    }else{
      $scope.minFuelTheft = 'others';
      $scope.customMinFuelTheftFilterValue = $scope.fuelMachineData.minFuelTheft;
    }
  }
  $scope.applyFilter();
}
// $scope.fuelMileage;
$scope.applyFilter = function () {
  $scope.fuelRunningTheft = $scope.fuelMachineData.fuelRunningTheft;
  if($scope.fuelMachineData.fuelRunningTheft!=null){
    if(($scope.fuelMachineData.sensorMode == 'DG'||$scope.fuelMachineData.sensorMode == 'Machinery')){
      if($scope.ltphValue!=""){
        $scope.fuelRunningTheft = $scope.fuelMachineData.fuelRunningTheft.filter(fuel=>fuel.ltrsphr > $scope.ltphValue);
      }
      if($scope.ignitionOnPer!=""){
       $scope.fuelRunningTheft = $scope.fuelRunningTheft.filter(fuel => fuel.ignitionOnPer <= $scope.ignitionOnPer );
     }
     if($scope.minFuelTheft!="" && $scope.minFuelTheft!="others"){
       $scope.fuelRunningTheft = $scope.fuelRunningTheft.filter(fuel => fuel.fuelTheftBeta >= $scope.minFuelTheft );
     }
     if($scope.customMinFuelTheftFilterValue!=""&&$scope.minFuelTheft=="others"){
      $scope.fuelRunningTheft = $scope.fuelRunningTheft.filter(fuel => fuel.fuelTheftBeta >= $scope.customMinFuelTheftFilterValue );
    }

  }else{
    if($scope.millage==""){
      $scope.fuelRunningTheft='';
    }
    if($scope.millage!=""){
      $scope.fuelRunningTheft = $scope.fuelMachineData.fuelRunningTheft.filter(fuel=>fuel.fuelMileage <= $scope.millage);
      // var fuelMileage = fuleMileage + fuelMileage;
      // $scope.fuelMileage = fuelMileage;
    }
    if($scope.ignitionOnPer!=""){
     $scope.fuelRunningTheft = $scope.fuelRunningTheft.filter(fuel => fuel.ignitionOnPer <= $scope.ignitionOnPer );
   }
   if($scope.minFuelTheft!="" && $scope.minFuelTheft!="others"){
     $scope.fuelRunningTheft = $scope.fuelRunningTheft.filter(fuel => fuel.fuelTheftBeta >= $scope.minFuelTheft );
   }
   if($scope.customMinFuelTheftFilterValue!=""&&$scope.minFuelTheft=="others"){
    $scope.fuelRunningTheft = $scope.fuelRunningTheft.filter(fuel => fuel.fuelTheftBeta >= $scope.customMinFuelTheftFilterValue );
  }
}
totalFuelTheftCalculation($scope.fuelRunningTheft);
}
}
// $scope.fuelMileage = fuelMileage;
$('#minus').click(function(){
  $('#menu').toggle(1000);
});
var startLoading1    = function () {
  $('#status01').show(); 
  $('#preloader').show();
};

  //loading stop function
  var stopLoading1   = function () {
    $('#status01').fadeOut(); 
    $('#preloader').delay(350).fadeOut('slow');
    $('body').delay(350).css({'overflow':'visible'});
  };

}]);
