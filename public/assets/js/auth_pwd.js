app.controller('mainCtrl',['$scope', '$location', 'vamoservice','_global', '$http','$filter','$translate', function($scope, $location, vamoservice, GLOBAL, $http,$filter,$translate){
  var language=localStorage.getItem('lang');
  $scope.multiLang=language;
  $translate.use(language);
  var translate = $filter('translate');
  
  var assLabel  =  localStorage.getItem('isAssetUser');
  if(assLabel=="true") {
    $scope.vehiLabel = "Asset";
  } else if(assLabel=="false") {
    $scope.vehiLabel = "Vehicle";
  } else {
    $scope.vehiLabel = "Vehicle";
  }

  var url             = $location.absUrl();
  $scope._tabValue    = url.includes("groupEdit");
  var _gUrl           = GLOBAL.DOMAIN_NAME+'/getVehicleLocations';
  var _addUrl         = GLOBAL.DOMAIN_NAME+'/addMobileNumberSubscription';
  var _showUrl        = GLOBAL.DOMAIN_NAME+'/getSpecificRouteDetails';
  var _deleteUrl      = GLOBAL.DOMAIN_NAME+'/stopSmsSubscription';
  var _searchUrl      = GLOBAL.DOMAIN_NAME+'/getStudentDetailsOfSpecifyNum';
  var _siteUrl        = GLOBAL.DOMAIN_NAME+'/viewSite';
  var _notifyUrl      = GLOBAL.DOMAIN_NAME+'/getUserNotification';
  var _updateNotify   = GLOBAL.DOMAIN_NAME+'/updateNotification?notification=';
  $scope.switchingVar = false;
// $scope.switchEdit   = false;
$scope.caption      = "MobileNo Search";
$scope.dealerName     =  localStorage.getItem('dealerName');
$scope.sort         = {sortingOrder : 'vehicles', reverse : true };
$scope.notifyUpdate = [];
$scope.rowsValue    = [];
// $scope.selectEdit   = (getParameterByName('userlevel') == 'reset') ? true : (getParameterByName('userlevel') == 'notify') : false;
// $scope.apiDateTime="";
$scope.apiValDays=undefined;
$scope.showPasswordError=false;
$scope.canremove = true; 
$scope.showSidebar=true;
$scope.groupCount = 0;
$scope.checkValue = function(value){
  if(value == 'true'){
    return true;
  }
  else  if(value == 'false')
    return false;
}

var groupName = window.localStorage.getItem('userMasterName');
if(groupName!=null){
 $scope.GroupName = groupName.split(',');
 $scope.finalUserName = $scope.GroupName[1];
}
 //console.log($scope.finalUserName);

 function getTodayDateTimes() {
  var date = new Date();

 // console.log(date);
 return date.getFullYear()+'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+("0" + (date.getDate())).slice(-2)+' '+("0" + (date.getHours())).slice(-2)+':'+("0" + (date.getMinutes())).slice(-2)+':'+("0" + (date.getSeconds())).slice(-2);
};

function utcFormats(val){return new Date(val).getTime()/1000;}

//console.log( getTodayDateTimes() );

$scope.getApiKey = function(){

  $scope.validDayShow  = false;

    // console.log($scope.apiValDays);
    // console.log( utcFormats( getTodayDateTimes() ) );

    var apiKeyUrl = GLOBAL.DOMAIN_NAME+'/getApiKey?validDays='+$scope.apiValDays+'&time='+utcFormats( getTodayDateTimes() );

    $scope.apiKeyData=[];
    $http.get(apiKeyUrl).success(function(data){

     $scope.apiKeyVal=data.apiKey;
                //console.log(data.apiKey);
              });
  }
   //$scope.selectEdit2 = false;
   $scope.selectStopEdit = false;
 //$scope.selectEdit     = false;
 if(getParameterByName('userlevel') == 'reset'){
  $scope.showPasswordError=false;
  $scope.selectEdit1 = true;
  $scope.selectEdit2 = false;
  $scope.selectStopEdit=false;
  $scope.videoLink="https://www.youtube.com/watch?v=Y27lguna7l4&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=15";


}
else if(getParameterByName('userlevel') == 'fromLogin'){
  $scope.showPasswordError=true;
  $scope.showSidebar=false;
  $scope.selectEdit1 = true;
  $scope.selectEdit2 = false;
  $scope.selectStopEdit=false;
  $scope.videoLink="https://www.youtube.com/watch?v=Y27lguna7l4&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=15";


}
else if(getParameterByName('userlevel') == 'apiKeys'){

  $scope.selectEdit3 = true;
  $scope.validDayShow  = true;
  $scope.selectEdit2 = false;
  $scope.selectStopEdit=false;

}else if(getParameterByName('userlevel') == 'notify'){
  startLoading();
  $scope.selectEdit2 = true;
  $scope.notify   = [{'check': true, 'value' : 'lowbat', 'caption':'LOW BATTERY'}];
  $scope.videoLink="https://www.youtube.com/watch?v=kIenpGpwZhg&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=19";
  
 /* $.ajax({
        async: false,
        method: 'GET', 
        url: 'notificationFrontend',
        // data: {"orgId":},
        success: function (response) {
          if (response != 'fail')
            $scope.notificationValue = response;
            angular.forEach($scope.notificationValue, function(ke, val){
              $scope.notifyUpdate.push($scope.checkValue(ke));
            })
        }x
      })*/

      $http({
        method: 'GET',
        url: _notifyUrl
      }).then(function successCallback(response) {
        if(response.status==200){
          var respVal               =  response.data;
          $scope.notifyUpdate       =  [];
          $scope.notificationValue  =  [];
          $scope.notificationSel    =  [];
          $scope.notiValue=[];
          $scope.notiValue=respVal.notification;
          var initVal               =  0;

          angular.forEach(respVal.selectedNotification,function(val){
            if(val=='SAFETY MOVEMENT'){
              $scope.notificationSel.push('UNAUTHORISED MOVEMENT');
            }
            else if(val=='DUE ALERT'){
              $scope.notificationSel.push('FMS ALERT');
            }
            else if(val=='HARSH BREAKING'){
              $scope.notificationSel.push('HARSH BRAKING');
            }

            else {
              $scope.notificationSel.push(val);
            }
          });

          angular.forEach(respVal.notification,function(val){
            if(val=='SAFETY MOVEMENT'){
              $scope.notificationValue.push('UNAUTHORISED MOVEMENT');
            }
            else if(val=='DUE ALERT'){
              $scope.notificationValue.push('FMS ALERT');
            }
            else if(val=='HARSH BREAKING'){
              $scope.notificationValue.push('HARSH BRAKING');
            }
            else {
              $scope.notificationValue.push(val);
            }
          });
          var index=$scope.notificationValue.indexOf('AREA ENTRY/EXIT LEVEL 1');
          $scope.notificationValue.splice(index,1);
          var index1=$scope.notiValue.indexOf('AREA ENTRY/EXIT LEVEL 1');
          $scope.notiValue.splice(index1,1);
          var index2=$scope.notificationSel.indexOf('AREA ENTRY/EXIT LEVEL 1');
          if(index2!=-1){
            $scope.notificationSel.splice(index2,1);}
            angular.forEach($scope.notificationValue, function(key, val){
              for(var i=0;i<$scope.notificationSel.length;i++){

                if(key==$scope.notificationSel[i]) {
                  initVal = 1;

                  $scope.notifyUpdate.push(true);
                } 
              }
              if(initVal==0) {
                $scope.notifyUpdate.push(false);  
              }
              initVal=0;

            }); 

          }

        }, function errorCallback(response) {
         console.log(response.status);
       });

    } else if(getParameterByName('userlevel') == 'busStop'){
      document.getElementById("sidebarMenu").style.marginTop = "32px";
      $scope.selectStopEdit = true;
      $scope.selectEdit1 = false;
      $scope.selectEdit2 = false;
      $scope.selectEdit3 = false;
    }

/*
  remove group from this user 
  */

  $scope.removeGroup = function(gId){
    if($scope.canremove){
      startLoading();
      $scope.canremove = false;
      $scope.check_box = false;
      $scope.error ="";
      $scope.grpName = gId;
      var list = [];
      angular.forEach($scope.groupList, function(album){

        if (album.groupName !== '') list.push({'groupName': album.groupName});

      });
      if(gId !== '')
        $http.post("VdmGroup/removingGroup", {'grpName': gId})
      .success(function (response) {
        if(response =='success'){
          if(localStorage.getItem('user')){
           let obj = JSON.parse(localStorage.getItem('user'));
           if(obj!=null){
            let sp     =  obj.split(',');
            let gname  =  sp[1];
            if(gname==gId){
              localStorage.setItem('user','');
            }
          }
        }
        location.reload();
      }
    })
      .error(function (response) {
        $scope.canremove = true;
        console.log("fail");
      });            

      $scope.groupList =[];
      $scope.groupList =list;

      console.log($scope.groupList)
      stopLoading();

    }

  }

  /*
    Edit group for user
    */

    $scope.editGroup  = function(gId){
      startLoading();
      $scope.check_box = false;
      $scope.error ="";
      $scope.grpName = gId;


      if(gId !== undefined)
        $http.post("VdmGroup/showGroup", {'grpName': gId})
      .success(function (response, status) {
        console.log("success");
        if(typeof response !== "string"){
          $scope.vehiList =[];
          $scope.vehiList   = response;
        }
      })
      .error(function (response, status) {
        console.log("fail");
      });
      stopLoading();
    }


  /*
    Add group
    */

    $scope.addGroup   = function(){
      $scope.error = "*" +translate("Add only one group at a time");
      var count = 1;    
      $scope.groupList.push({'groupName':''})
      var listGroup = $scope.groupList;
      $scope.groupList = [];
      angular.forEach(listGroup, function(val, ke){
        if(val.groupName == '' && count == 1){
          $scope.groupList.push({'groupName':''})
          count ++;
          return;
        } else if(val.groupName !== '') $scope.groupList.push({'groupName':val.groupName})

      })
      $scope.editGroup('');
    }

  /*
    Add group in db
    */
    $scope.changevalue = function(gN, ind)
    {
      $scope.error = '';
      $scope.grpName = gN;
    // console.log($addNewGroup)
    $http.post("VdmGroup/groupId", {'id': gN})
    .success(function (response) {
      if(response=='fail')
        $scope.error = translate('No groups available ,Please select another user');

                  // location.reload();
                  console.log(response)
                  
                })
    .error(function (response) {
      console.log("fail");
    });
  }

  /*
    updateGroup for user
    */

    $scope.updateGroup  = function(g_Name){

      console.log($scope.groupList)
      $scope.check_box = false;
      var newValu = '';
      if(g_Name === '')
        angular.forEach($scope.groupList, function(va, ke){
          if(va.newGp !== ''){
            g_Name = va.newGp;
            newValu = 'newValue'
          }
        })
      startLoading();
      $scope.error ="";
      $scope.grpName = g_Name;
      var list  = [];
      angular.forEach($scope.vehiList, function(selected_Vehi){

        (selected_Vehi.check == true)? list.push(selected_Vehi.vehicles) : '';

      });

      console.log("list")
      if(list.length >0){
        $http.post("VdmGroup/saveGroup", {'grplist': list, 'grpName': g_Name, 'newValu': newValu})
        .success(function (response) {

          if(response == 'sucess') location.reload()
            else $scope.error = "* Group may not save Sucessfully"
          })
        .error(function (response) {
         console.log("fail");
       });

      } else {
        console.log('no vehicle')
        $scope.error ="* Please select a vehicle to save the group name";
      }
      stopLoading();

    }

/*
  for get orgId
    */
  function getOrgValue(val){
    var orgId   = '';
    angular.forEach($scope.routeNameList, function(org, k){
      angular.forEach(org, function(stop, key){
        if(val == stop){
          orgId = k
          return;
        }
      })
    });
    return orgId;
  }

/*
  get bus stops 
  */
  function _getBustopValue(id){
    var _returnValue;
    angular.forEach($scope.stopList, function(value, key){
      if(value.poiId == id){
        _returnValue = value;
        return _returnValue;
      }
    });
    if(_returnValue == undefined)
      _returnValue = $scope.stopList[$scope.stopList.length-1];
    return _returnValue;
  }


/*
  check null values
  */

  function _assignValue (obj, status){
// return
switch (status){

  case 'name':
  return (obj.sNameNew == undefined) ? obj.sName : obj.sNameNew;
  break;
  case 'eNum':
  return (obj.eNumNew == undefined) ? obj.eNum : obj.eNumNew;
  break;  
  case 'num':
  return (obj.mNumNew == undefined) ? obj.mNum : obj.mNumNew;
  break;
  case 'std':
  return (obj.stdNew == undefined) ? obj.std : obj.stdNew;
  break;
  case 'stId':
  return (obj.poiId == undefined) ? obj.stopId : obj.poiId.poiId;
  case 'id':
  return (obj.id == undefined) ? 0 : obj.id;
  break;

}
}


$scope.submitValue  = function(){

  console.log('submitValue ...');

  startLoading();
  $scope.error = '';
  $scope.rowsCount = 0;
  var statusValue = true;
  
  /*
    validation
    */
    var _url = '';

    for (var i = 0; i < $scope.rowsValue.length; i++) {

    // if(validCharCheck(_assignValue($scope.rowsValue[i], 'name')) == false){
    //   statusValue = false, $scope.error = '* Enter Student Name, without special characters in '+( i+1 )+' th row .';
    //   stopLoading();
    //   return;
    // }
    if(checkXssProtection(_assignValue($scope.rowsValue[i], 'name')) && checkXssProtection(_assignValue($scope.rowsValue[i], 'num')) && checkXssProtection(_assignValue($scope.rowsValue[i], 'std')))
    {
      if(mobNumCheckTenDigit(_assignValue($scope.rowsValue[i], 'num')) == false){
        statusValue = false, $scope.error = '*'+translate('Enter mobile number ,as 10 or 12 digits in a row')+' '+( i+1 )+'.';
        stopLoading();
        return;
      }
    } 
    else {
      statusValue = false, $scope.error = '* '+translate('Not Valid Data in')+' '+( i+1 )+' '+translate('th row')+'.';
      stopLoading();
      return;
    }
    // if(removeColonStar(_assignValue($scope.rowsValue[i], 'std')) == false){
    //   statusValue = false, $scope.error = '* Enter Standard, dnt use special characters '+( i+1 )+' th row.';
    //   stopLoading();
    //   return;
    // }


    _url +=$scope.selectRouteName+':'+_assignValue($scope.rowsValue[i], 'name')+':'+_assignValue($scope.rowsValue[i], 'num')+':'+_assignValue($scope.rowsValue[i], 'std')+':'+_assignValue($scope.rowsValue[i], 'stId')+':'+_assignValue($scope.rowsValue[i], 'id')+':'+_assignValue($scope.rowsValue[i], 'eNum')+'*';

  }

  if(statusValue){

   console.log('inserted...');

   $http.post(_addUrl, {
    'orgId' : getOrgValue($scope.selectRouteName),
    'studentDetails' : _url,
  })
   .success(function (response) {
    console.log(response)
            // if(response.trim() == 'Success'){
              if(response.trim() == 'Success'){
                $scope.toast = 'Updated Successfully';
              } else {
                $scope.toast = response;
              }
              toastMsg();
              $scope._addDetails();
            })
   .error(function (response) {
    console.log("fail");
  });

 }
 stopLoading();
}



function _editGlobal(ind){

  $scope.rowsValue[ind].sNameNew  = ($scope.rowsValue[ind].sName) ? $scope.rowsValue[ind].sName : '';
  $scope.rowsValue[ind].sName     = ' ';
  $scope.rowsValue[ind].eNumNew   = ($scope.rowsValue[ind].eNum) ? $scope.rowsValue[ind].eNum : '';
  $scope.rowsValue[ind].eNum      = ' ';
  $scope.rowsValue[ind].mNumNew   = ($scope.rowsValue[ind].mNum) ? $scope.rowsValue[ind].mNum : '';
  $scope.rowsValue[ind].mNum      = ' ';
  $scope.rowsValue[ind].stdNew    = ($scope.rowsValue[ind].std) ? $scope.rowsValue[ind].std : '';
  $scope.rowsValue[ind].std       = ' ';
  $scope.rowsValue[ind].poiId     = (_getBustopValue($scope.rowsValue[ind].stopId)) ? _getBustopValue($scope.rowsValue[ind].stopId) : '';
  $scope.rowsValue[ind].poiIds    = '';

}


/*
  Switching function
  */
  $scope.switching  = function(){

    if($scope.switchingVar == false)
      $scope.switchingVar = true,$scope.caption      = translate("Back");
    else
      $scope.switchingVar = false, $scope.caption      = translate("MobileNo_Search");

  }

/*
    searching mobile number function
    */

    $scope.searchingMobile  = function(mobile){
      $scope.error      = '';
      var _addMobileNo  = '', statusValue = true, _serviceUrl = '', checkNull = '';
      mobile  = splitComma(mobile);
      for (var i = 0; i < mobile.length; i++) {
        checkNull = removeSpace(mobile[i]);
        if(mobNumCheckTenDigit(checkNull) == false){
          statusValue   = false ,$scope.error = '* '+translate('Enter 10 or 12 digit mobile number for each')+'.';
          stopLoading();
          return;
        }
        _addMobileNo  += checkNull+',';
      }
      _serviceUrl    = _searchUrl+'?mobileNo='+_addMobileNo;
      if(statusValue)
        vamoservice.getDataCall(_serviceUrl).then(function(value){

      // console.log(value)
      $scope.searchValue  = value;

    });


    }


/*
  edit bus stops

  */
  $scope._editStop  = function(ind, status){
    startLoading();

  // console.log($scope.rowsValue);
  // console.log($scope.stopList);
  // $scope.switchEdit   = true;

  if(status == 'one'){
    _editGlobal(ind);
  }
  // else if(status == 'all')
  //   for (var i = 0; i < $scope.rowsValue.length; i++) {
  //     _editGlobal(i);
  //   }
  stopLoading();
}


/*
  undo edit
  */

  $scope._undoEdit  = function(ind, status){

    $scope.rowsValue[ind].sName    =  ($scope.rowsValue[ind].sNameNew) ? $scope.rowsValue[ind].sNameNew : '';
    $scope.rowsValue[ind].eNum     =  ($scope.rowsValue[ind].eNumNew) ? $scope.rowsValue[ind].eNumNew : '';
    $scope.rowsValue[ind].mNum     =  ($scope.rowsValue[ind].mNumNew) ? $scope.rowsValue[ind].mNumNew : '';
    $scope.rowsValue[ind].std      =  ($scope.rowsValue[ind].stdNew) ? $scope.rowsValue[ind].stdNew : '';
//$scope.rowsValue[ind].poiIds   =  _getBustopValue($scope.rowsValue[ind].stopId).stop;
$scope.rowsValue[ind].poiIds   =  _getBustopValue($scope.rowsValue[ind].poiId.poiId).stop;
//$scope.rowsValue[ind].poiIds   =  ($scope.rowsValue[ind].stopId == $scope.rowsValue[ind].poiId.poiId) ? _getBustopValue($scope.rowsValue[ind].stopId).stop : ;

}

/*
  delete busstop values
  */

  $scope._deleteMobNum    = function(index){

    console.log(index);
    console.log($scope.rowsValue[index].mNum);

    $scope.error = '';
    console.log(' _deleteMobNum ');

    $http.post(_deleteUrl, {
      'mobNum' : $scope.rowsValue[index].mNum,
      'routeNam': $scope.selectRouteName,
      'orgId' : getOrgValue($scope.selectRouteName),
    })
    .success(function (response) {
      console.log("success");
      console.log(response)
    })
    .error(function (response) {
      console.log("fail");
    });

    $scope.rowsValue.splice(index, 1);
  // $scope._addDetails();
}


function arrayObjectIndexOf(myArray, searchTerm, property) {
  for(var i = 0, len = myArray.length; i < len; i++) {
    if (myArray[i][property] === searchTerm) return i;
  }
  return -1;
}
//arrayObjectIndexOf(arr, "stevie", "hello"); // 1

$scope._deleteFilterMobNum = function(index,mobb,routeN){

  console.log('delete....');

  console.log(routeN);

 // console.log(mobb);
 // console.log($scope.selectRouteName);
 // console.log(routeN);

 var indexNew=arrayObjectIndexOf($scope.rowsValue, mobb, "mNum");

 $scope.searchValue.studentDetails.splice(index, 1);

 $scope.error = '';
 console.log(' _deleteFilterMobNum ');

 console.log(getOrgValue(routeN));
 $http.post(_deleteUrl, {
  'mobNum' : mobb,
  'routeNam': routeN,
  'orgId'  : getOrgValue(routeN),
})
 .success(function (response) {
  console.log(response)
})
 .error(function (response) {
  console.log("fail");
});

 $scope.rowsValue.splice(indexNew, 1);
  //$scope._addDetails();
}

function trimNull(val){

 if( val == null ) {
   return '';
 } else {
   return val;
 }
}

/*
  * show the stop details   
  */

  function showStop() {

    var _showStopUrl  = _showUrl+'?routeNo='+$scope.selectRouteName;

    console.log((_showStopUrl));
    vamoservice.getDataCall(_showStopUrl).then(function(value){

      $scope.rowsValue =[];
      var busStopValue;
      angular.forEach(value.studentDetails, function(val, key){
        busStopValue = _getBustopValue(val.busStop);
        $scope.rowsValue.push({'sName' : trimed(val.name), 'eNum':trimNull(val.enrollNum), 'mNum': trimed(val.mobileNumber), 'std' : trimed(val.class), 'poiIds' : busStopValue.stop, 'stopId' : val.busStop, 'id' : val.rowId});

        console.log($scope.rowsValue);
      });
    })
  }


/*
  check all vehicle 
  */

  $scope.checkAll   = function(){
    $scope.error ="";
    angular.forEach($scope.vehiList, function(val, key){
      val.check=$scope.check_box;
    })

  }

/*
    add rows
    */

    $scope.addRows      = function(counts){

      console.log(' inside the row function '+counts);
  // $scope.rowsValue  = [];
  $scope.rowsCount  = counts;

  for (var i=0; i<counts; i++) {
    $scope.rowsValue.push({'sName' : ' ','eNum': ' ' , 'mNum': ' ', 'std' : ' ', 'poiIds' : ''});  
  }

}


$scope._addDetails  = function(){
  startLoading();
  $scope.error      = '';
  $scope.stopList   = [];
  var _geoUrl       = GLOBAL.DOMAIN_NAME+'/getBusStops?routeNo='+$scope.selectRouteName+'&orgId='+getOrgValue($scope.selectRouteName);
  
  vamoservice.getDataCall(_geoUrl).then(function(data) {
    try{
      if((data && data != '') && (data.studentDetails.length > 0)){
        angular.forEach(data.studentDetails, function(value, key){

          if((value && value != '') && (parseInt(value['BusStopId']) >= 0))
            $scope.stopList.push({'stop' :value['BusStopId']+', '+value['BusStopName'], 'poiId' :value['BusStopId']});

        });

      }
      showStop();
      stopLoading();

    }catch (err){stopLoading()}

  });


}


function getRouteName (){

  $scope.vehiStopList   = [];
  var stopList          = [];
  vamoservice.getDataCall(_siteUrl).then(function(value, key){

    if((value && value != '') && (value.orgIds && value.orgIds.length > 0))
      $.ajax({
        async: false,
        method: 'GET', 
        url: 'VdmOrg/getStopName',
        data: {
          'orgId' : value.orgIds,
        },
        success: function (response) {
          $scope.routeNameList  = response;
          angular.forEach(response, function(value, key){
            angular.forEach(value, function(val, key){
              stopList.push(val);
            })
          });
          $scope.vehiStopList   = stopList;
          
          var countryList = $scope.vehiStopList;

          $("#routeDataList").select2({
            data: countryList,
          });

          if($scope.vehiStopList.length >0){
            $scope.selectRouteName = $scope.vehiStopList[0];
            $scope._addDetails();
            //showStop();
          }
        }
      });
  })

}

function _addStops() {
  getRouteName();
}


/*
  initial for groupEdit
  */
  function initial(){

    startLoading();
    $scope.videoLink="https://www.youtube.com/watch?v=WmBd5hPR07w&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=20";
    $scope.error ="";
    vamoservice.getDataCall(_gUrl).then(function(data) {
      $scope.groupList    = [];
      var obj = null;
      if(localStorage.getItem('user')){
        obj = JSON.parse(localStorage.getItem('user'));
      }
      if(obj==null){
       $scope.vehiname  = data[0].vehicleLocations[0].vehicleId;
       $scope.gName     = data[0].group;
       localStorage.setItem('user', JSON.stringify($scope.vehiname+','+$scope.gName));
     }
     
     angular.forEach(data, function(value, key){
      $scope.groupList.push({'groupName': value.group});
    })
     $scope.groupCount = $scope.groupList.length;
     $scope.selectStopEdit == true ? _addStops() : console.log(' Edit Group');
     stopLoading();
   });
  }  

  ($scope._tabValue == true || $scope.selectStopEdit == true)?initial():stopLoading();


  $scope.trimColon = function(textVal){
    if(textVal)
      return textVal.split(":")[0].trim();
  }


// checking auth in the addsite and rfid 

$scope.passwordConfirm  = function(){

  startLoading();
  if(checkXssProtection($scope.pwd) == true){
    var password  = $scope.pwd;
    var URL_ROOT    = "AddSiteController/";    /* Your website root URL */

    $http.post("AddSiteController/checkPwd", {
      '_token': $('meta[name=csrf-token]').attr('content'),
      'pwd': password})
    .success(function (data, status) {
      console.log('Sucess---------->' + data+location.pathname);
      stopLoading();
      if(data == 'incorrect'){
        $scope.statusUi   = "Invalid Password"; 
        alert($scope.statusUi);
      }
      else if (data == 'correct'){
        var path = context+'/public/track?maps=sites';
        location.href = path;
        localStorage.setItem('auth', 'sitesVal');
      }

    })
    .error(function (data, status) {
      console.log("fail");
      stopLoading();
      $scope.statusUi   = "Response Failure"; 
      alert($scope.statusUi); 
    });
          // $.post(URL_ROOT+'checkPwd',{
          //     '_token': $('meta[name=csrf-token]').attr('content'),
          //     'pwd': password,
          // }).done(function(data){
          //     console.log('Sucess---------->' + data+location.pathname);
          //     stopLoading();
          //     if(data == 'incorrect'){
          //       $scope.statusUi   = "Invalid Password"; 
          //       alert($scope.statusUi);
          //     }
          //     else if (data == 'correct'){
          //       var path = context+'/public/track?maps=sites';
          //       location.href = path;
          //       localStorage.setItem('auth', 'sitesVal');
          //     }

          // }).fail(function(){
          //   stopLoading();
          //     console.log('fail');
          //     $scope.statusUi   = "Response Failure";  
          // })
        }
        stopLoading();
      }

      $scope.error = '';
    /*
      update password
      */

      $scope.updatePwd  = function() {
        startLoading();

        $scope.error        = '';
        if((checkXssProtection($scope.oldValue) == true) && (checkXssProtection($scope.firstVal) == true) && (checkXssProtection($scope.reEnterVal) == true))
        {
          if($scope.oldValue == '' || $scope.oldValue == undefined && $scope.firstVal == '' || $scope.firstVal == undefined && $scope.reEnterVal == '' || $scope.reEnterVal == undefined){

            $scope.error = "* "+translate("Fill all Fields")+" ."; 

          } else {
            if($scope.firstVal.length<6){
             $scope.error = translate("New password must contain atleast 6 characters")+" .";  
           }

           else if($scope.oldValue != $scope.firstVal && $scope.oldValue != $scope.reEnterVal && $scope.firstVal == $scope.reEnterVal){
            var menuValue           =  JSON.parse(localStorage.getItem('userIdName'));
            var sp                  =  menuValue.split(",");
            $http.post("resetPassword", {
              "userId":sp[1],
              "oldPassword": $scope.oldValue,
              "newPassword": $scope.firstVal})
            .success(function (response) {
              console.log("success");
              if(response.response =='Success'){
                console.log(response);
                $scope.error = translate("Updated Sucessfully")+" .";
                setTimeout(function () {
                  document.location.href = 'login';

                }, 1000); 

              } else{
                $scope.error = '*'+response.error+'.';               
              }
            })
            .error(function (response) {
              $scope.error  = '* '+translate("Connection Fails")+' .';
            }); 

          } else {

            $scope.error = "* "+translate("Old and new passwords don't match")+".";
          }

        }
      }
      stopLoading();

    }

    $scope.updateNotify = function() {

      var notifyValu = "";
      var noInit     = 0;
    //console.log($scope.notificationValue);
    for(var i=0;i<$scope.notiValue.length;i++) {
        //console.log($scope.notifyUpdate[i]);
        if($scope.notifyUpdate[i]==true) {

         if(noInit==0) {
           notifyValu+=$scope.notiValue[i];
           noInit=1;
         } else {
           notifyValu+=','+$scope.notiValue[i];
         }
       }
     }

     console.log(notifyValu);
     var upNotifyUrl = _updateNotify+encodeURIComponent(notifyValu);

     $http({
      method: 'GET',
      url: upNotifyUrl
    }).then(function successCallback(response) {

      if(response.status==200) {
        console.log("Update successfully!..");
        location.reload();
      } else {
       console.log("Update not successfully!..");
     }
   }, function errorCallback(response) {
     console.log(response.status);
   });

  }
  $("#testLoad").load("../public/menu");

}]);

