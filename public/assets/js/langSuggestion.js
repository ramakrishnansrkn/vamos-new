app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global','$translate', function($scope, $http, vamoservice, $filter, GLOBAL,$translate){
	
  //global declaration
	$scope.uiDate 				=	{};
	$scope.uiValue	 			= 	{};
  //$scope.sort                 = sortByDate('startTime');
    
    $scope.trvShow       =  localStorage.getItem('trackNovateView');

    var tab = getParameterByName('tn');
    var language=localStorage.getItem('lang');
    $scope.multiLang=language;
    $translate.use(language);
    // $scope.words=[];
    // $scope.words.push({'word':'w1','Eword':'Ew1'});
	function getParameterByName(name) {
    	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	//global declartion
	$scope.locations = [];
	$scope.url = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
	$scope.gIndex =0;

    $scope.trimColon = function(textVal){
		return textVal.split(":")[0].trim();
	}
	
    $("#testLoad").load("../public/menu");
	$scope.$watch("url", function (val) {
		vamoservice.getDataCall($scope.url).then(function(data) {
           
          //startLoading();
            $scope.selectVehiData = [];
			$scope.vehicle_group=[];
			$scope.vehicle_list = data;

			if(data.length){
				$scope.vehiname	= getParameterByName('vid');
				$scope.uiGroup 	= $scope.trimColon(getParameterByName('vg'));
				$scope.gName 	= getParameterByName('vg');
				angular.forEach(data, function(val, key){
                  //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
					if($scope.gName == val.group){
						$scope.gIndex = val.rowId;
 
						angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys){

                            $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

							if($scope.vehiname == value.vehicleId)
							$scope.shortNam	= value.shortName;
						})
						
					}
			    })

		  //console.log($scope.selectVehiData);
		    //sessionValue($scope.vehiname, $scope.gName)
			}
			var dateObj 			= 	new Date();
			$scope.fromNowTS		=	new Date(dateObj.setDate(dateObj.getDate()-1));
			$scope.uiDate.fromdate 		=	getTodayDate($scope.fromNowTS);
		  	$scope.uiDate.fromtime		=	'12:00 AM';
		  	$scope.uiDate.todate		=	getTodayDate($scope.fromNowTS);
		  //$scope.uiDate.totime 		=	formatAMPM($scope.fromNowTS.getTime());
            $scope.uiDate.totime 		=   '11:59 PM';
		  //webServiceCall();
		   
		   
		  stopLoading();
		});	
	});


        //Language Suggestion box
    $scope.words = [];
    $scope.WordsArr=[];
    $scope.words.push({
        })
      $scope.addWords = function() {
      	$scope.error="";
  	    $scope.msg="";
        $scope.words.push({
        })
      }


    $scope.sendSuggession = function(){
    $scope.error="";
    $scope.msg="";
    console.log("sendSuggestion");
     angular.forEach($scope.words, function(val, key){
     	if(!(val.word==undefined&&val.Eword==undefined)){
          $scope.WordsArr.push(val.word+':'+val.Eword);
          }
        })
     console.log($scope.WordsArr);
    if($scope.WordsArr.length >0){
      $http.post("sendSuggession", {'wordList': $scope.WordsArr})
            .success(function (response) {
              console.log(response);
          //if(response == 'sucess') location.reload()
           $scope.msg = "* Suggession send Sucessfully";
            })
            .error(function (response) {
              $scope.error = "* Your Suggession not send,please try again...";
              console.log("fail");
            });

    } else {
        $scope.error ="* Please Enter words...";
    }
    stopLoading();
     $scope.cancel();
  }
  $scope.cancel   = function(){
       $scope.words = [];
       $scope.WordsArr=[];
       $scope.words.push({
        })
  }

    

}]);