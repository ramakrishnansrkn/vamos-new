app.controller('mainCtrl',['$scope','$http','vamoservice','$filter','_global', function($scope,$http,vamoservice, $filter,GLOBAL) {

 // global declaration

  $scope.fileName        =  '';  
//$scope.fileExtn        =  false;
  $scope.fileNameShow    =  false;
  $scope.fileUploadsBox  =  true;  
  $scope.tripSearchBox   =  true;   
  $scope.tripBaseShow    =  false;   
  $scope.vehiBaseShow    =  false; 
  $scope.backBtnShow     =  false;
  $scope.organId         =  '';

  $('#notifyMsg').hide();

  $scope.getFileName = function() {

        $.ajax({
            url: 'getFiles', 
            dataType: 'text',  
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(response){
            //console.log(response);
              var arrName = response;
              var sptVal  = arrName.split('"');

              //console.log(arrName);
                var retArr =[];

                for(var i=0;i<sptVal.length;i++){
                    if(sptVal[i].includes(".csv")){
                     //console.log(sptVal[i]);   
                       retArr.push(sptVal[i])
                    }
                }
                $scope.fileData = retArr;
              //console.log('File uploaded successfully...');

            },
            error: function (textStatus, errorThrown) {
                console.log('File upload failed...');
                  timeOutVar = setTimeout(setsTimeOuts, 3000);
            }

        });
  }

  $scope.getFileName();
  $scope.vehUrl  =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations';
  $scope.orgUrl  =  GLOBAL.DOMAIN_NAME+'/viewSite';
   
  stopLoading();

  function sessionValue(vid, gname){
    $("#testLoad").load("../public/menu");
  }
  sessionValue();
  $scope.groupId = 0;

  $scope.$watch("vehUrl", function (val) {
    
        $http({
            method : "GET",
            url : $scope.vehUrl
        }).then(function mySuccess(response) {

            console.log(response.data);

           // $scope.vehiName   = response.data[0].vehicleId;
            $scope.grpName  = response.data[$scope.groupId].group;

            $scope.data     = response.data;

            $scope.grpList  = [];
            $scope.vehiList = [];

            angular.forEach(response.data, function(value, key){

               //var spltVal = value.group.split(":");

                 $scope.grpList.push({gName:value.group});

                  if($scope.grpName == value.group) { 

                     $scope.groupId=value.rowId;

                     angular.forEach(value.vehicleLocations, function(val, k){

                        $scope.vehiList.push({'vehiId' : val.vehicleId, 'vName' : val.shortName});

                            if(val.vehicleId == $scope.vehiName){
                                $scope.shortVehiId  = val.shortName;
                                $scope.vehiId  = val.vehicleId;
                            }
                    });
                  } 

            });     

             console.log( $scope.grpList );
                console.log( $scope.vehiList );


        }, function errorCallback(response) {
              
          console.log(response.status);

    });

  });

    
   $scope.groupSelection = function(val) {

    console.log(val);

    var grpVal     =  val;
    $scope.grpName =  val;

    var grpVehUrl  =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+grpVal;
    console.log(grpVehUrl);

        $http({
            method : "GET",
            url : grpVehUrl
        }).then(function mySuccess(response) {

            console.log(response.data);

            //$scope.vehiName = response.data[0].vehicleId;
            //$scope.grpName  = response.data[$scope.groupId].group;

            $scope.data     = response.data;

            $scope.grpList  = [];
            $scope.vehiList = [];

            angular.forEach(response.data, function(value, key){

               //var spltVal = value.group.split(":");

                $scope.grpList.push({gName:value.group});

                  if($scope.grpName == value.group) { 

                    $scope.groupId = value.rowId;

                    angular.forEach(value.vehicleLocations, function(val, k){

                        $scope.vehiList.push({'vehiId' : val.vehicleId, 'vName' : val.shortName});

                            if(val.vehicleId == $scope.vehiName){
                                $scope.shortVehiId  = val.shortName;
                                $scope.vehiId  = val.vehicleId;
                            }
                    });
                  } 

            });     
             console.log( $scope.grpList );
                console.log( $scope.vehiList );

        }, function errorCallback(response) {
            console.log(response.status);
    });
  }  

    /* Get Org Ids */
   
    $scope.$watch("orgUrl", function (val) {

      $http.get($scope.orgUrl).success(function(response) {
        console.log(response);
        $scope.orgList = [];
        var orgId = response.orgIds;

          for( var i=0; i<orgId.length; i++ ) {
               console.log(orgId[i]);
                 $scope.orgList.push( { 'orgName' : orgId[i] } );
          }
      });

    });  


  function callApiFunc() {

      console.log($scope.organId);
    //console.log($scope.fileName);
    //var tripUrl = "http://128.199.159.130:9000/uploadTripsheet?userId=MSS&orgId=RAJ&tripName=sampleTripsheet.csv";  
      var tripUrl = GLOBAL.DOMAIN_NAME+'/uploadTripsheet?orgId='+$scope.organId+'&tripName='+$scope.fileName;  
    //encodeURIComponent($scope.fileName)


      $http({
          method: 'GET',
          url: tripUrl
      }).then(function successCallback(response) {

            if(response.status==200) {
                console.log("Send File Name Successfully!..");
            }
      }, function errorCallback(response) {
              console.log(response.status);
      });
  }


    $(document).ready(function() {

        $('input[type="file"]').change(function(e) {
           
             var fileVal  = e.target.files[0].name;
           //alert('The file "' + fileName +  '" has been selected.');
             var splitVal = fileVal.split(".");

              if(splitVal[1]=="csv"){
                 $scope.fileName = fileVal;
                 //$scope.fileExtn = true;
                   console.log($scope.fileName);
              } else{
                  $scope.fileName='';
                  $('#file').val('');
                  alert("Please choose '.csv' files to upload.");
                  //$scope.fileExtn=false;
              }
        });
    });

    $scope.orgFunction = function(val) {
       console.log(val);
         $scope.organId = val.orgName;
    }

    $scope.vehiSelFunction  = function(val) {
       console.log(val);
      $scope.selVehiId = val.vehiId;
    }

    $scope.vehiHisFunc  = function(val) {
       console.log(val);
         $scope.vehiIdHist = val.vehiId;
    }

    $scope.tripSheetView = function() {

      $scope.fileUploadsBox  =  false; 
      $scope.fileNameShow    =  true;
        
    }

    $scope.backUpload = function(){

      $scope.fileNameShow    =  false;
      $scope.fileUploadsBox  =  true; 
    }

   $scope.tripFunc = function() {

      $scope.tripSearchBox  =  false; 
      $scope.vehiBaseShow   =  false;
      $scope.tripBaseShow   =  true;  
      $scope.backBtnShow    =  true;
   }

   $scope.vehiFunc= function() {

      $scope.tripSearchBox  =  false; 
      $scope.tripBaseShow   =  false;
      $scope.vehiBaseShow   =  true;
      $scope.backBtnShow    =  true;

   }

   
    $scope.backFunction = function() {

      $scope.backBtnShow    =  false;
      $scope.tripBaseShow   =  false;
      $scope.vehiBaseShow   =  false;
      $scope.tripSearchBox  =  true; 
    }
   
   /* $scope.gotoHistory = function(a) {
      $('.history_tab a').tab('show');$('.history_tab a').click();
    }
   */

    var timeOutVar;   
    function setsTimeOuts() {

        $('#notifyF').hide(1200);
             $('#notifyS').hide(1200);
                    $('#notifyMsg').hide(1200);


        if(timeOutVar!=null){
          //console.log('timeOutVar'+timeOutVar);
            clearTimeout(timeOutVar);
        }
    }

    $scope.tripNo='';

    function tripBaseReport() {

      if($scope.organId!=='' && $scope.tripNo!='') {

      //console.log("tripBaseReport...");
      //var tripBaseUrl = 'http://128.199.159.130:9000/getTripsheetValue?userId=MSS&orgId=RAJ&tripNumber=2000668258&date=2018-04-05&vehicleId=AP04TW4344'; 
      //var tripBaseUrl = 'http://128.199.159.130:9000/getTripsheetValue?userId=ZCLYGL&orgId=ZCLYGL&tripNumber=2000668258&date='; 
        var tripBaseUrl = GLOBAL.DOMAIN_NAME+'/getTripsheetValue?orgId='+$scope.organId+'&tripNumber='+$scope.tripNo+'&date=';
          $scope.tripNo='';
        
          console.log(tripBaseUrl);

            $http({
                method: 'GET',
                url: tripBaseUrl
            }).then(function successCallback(response) {

                if(response.status==200) {
                     
                        $scope.tripBaseData = [];
                        //console.log(response.data);
                            //$scope.tripBaseData = response.data.getTripData;
                              $scope.tripBaseData = response.data;

                                console.log($scope.tripBaseData);
                }

            }, function errorCallback(response) {
                    console.log(response.status);
            });

          } else {

              alert('Please fill all fields.');
          }
      }


  $scope.dateTime   =  '';
  $scope.selVehiId  =  '';

  function vehiBaseReport() {

    if($scope.organId!=='' && $scope.dateTime!='' && $scope.selVehiId!='') {

    //var vehiBaseUrl = 'http://128.199.159.130:9000/getTripsheetValue?userId=MSS&orgId=RAJ&tripNumber=2000668258&date=2018-04-05&vehicleId=AP04TW4344'; 
      //var vehiBaseUrl = 'http://128.199.159.130:9000/getTripsheetValue?userId=ZCLYGL&orgId=ZCLYGL&date=2018-04-05&vehicleId=AP04TW4344';
         var vehiBaseUrl = GLOBAL.DOMAIN_NAME+'/getTripsheetValue?orgId='+$scope.organId+'&date='+$scope.dateTime+'&vehicleId='+$scope.selVehiId; 

             console.log(vehiBaseUrl);

      $http({
          method: 'GET',
          url: vehiBaseUrl
      }).then(function successCallback(response) {

        console.log(response.status);

            if(response.status==200) {

                $scope.vehiBaseData = [];

                console.log("vehiBaseReport...");
                   console.log(response.data);
                 //$scope.vehiBaseData = response.data.getTripData;
                 //$scope.vehiBaseData = response.data.getTripData;
                   $scope.vehiBaseData = response.data;

                   console.log($scope.vehiBaseData);
            }

      }, function errorCallback(response) {

              console.log(response.status);

      });

     } else {

        alert('Please choose all fields.');
       
     }

    }


    $scope.tripBasedSubmit = function() {

       //console.log( $('#tripSearchVal').val() );
         var tripSerVal =  $('#tripSearchVal').val(); 

            if( isNaN(tripSerVal)!=true ) {

               $scope.tripNo = tripSerVal;
               //console.log(tripSerVal);
                    console.log("tripBaseReport...");

                    tripBaseReport();

            } else {
               
                 alert('Enter Trip Number!');
            }
    }

    $scope.vehiBasedSubmit = function() {

      console.log( $('#dateFrom').val() );

       $scope.dateTime = $('#dateFrom').val();

        vehiBaseReport();
    }

  $scope.tripHistoySubmit = function() {

      console.log( 'history...');

        var tripSerVal    =  $('#tripHisVal').val(); 
        $scope.tripNoHist =  tripSerVal;

      //var tripHisUrl = 'http://128.199.159.130:9000/getTriphistory?userId=ZCLYGL&vehicleId=AP04TW4344&tripNumber=2000672244&interval=1';
         var tripHisUrl = GLOBAL.DOMAIN_NAME+'/getTriphistory?vehicleId='+$scope.vehiIdHist+'&tripNumber='+$scope.tripNoHist+'&interval=1';

         console.log(tripHisUrl);

       $http({
            method: 'GET',
            url: tripHisUrl
        }).then(function successCallback(response) {

              if(response.status==200) {
                console.log("Trip Search Successfully!..");

                  console.log(response.data);
                    $scope.tripHistData = response.data;

              }

        }, function errorCallback(response) {
              
              console.log(response.status);

        });

  
    }



  $scope.submitFunction = function() {

    console.log("submit.....");
      console.log($scope.fileName);

     if($scope.fileName!=='' && $scope.organId!='') {        

          var file_data = $('#file').prop('files')[0];   
            var form_data = new FormData();         

        form_data.append('file', file_data);
          //alert(form_data);  
            console.log(form_data);

        $.ajax({
            url: 'fileUpload', // point to server-side PHP script 
            dataType: 'text',  // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         
            type: 'post',
            success: function(response){

                console.log('File uploaded successfully...');

                    $('#notifyMsg').show();
                    $('#notifyF').hide();
                    $('#notifyS').show(500);

                    timeOutVar = setTimeout(setsTimeOuts, 3000);
                    $scope.getFileName();

                    callApiFunc();

            },
            error: function (textStatus, errorThrown) {

                    console.log('File upload failed...');

                    $('#notifyMsg').show();
                    $('#notifyS').hide();
                    $('#notifyF').show(500);

          timeOutVar = setTimeout(setsTimeOuts, 3000);
                }

        });

      } else {

          if($scope.fileName=='' && $scope.organId==''){

              alert("Please.. Choose CSV File and Organization Name.");

          } else if($scope.fileName=='' && $scope.organId!='') {

              alert("Please.. Choose CSV file.");

          } else if($scope.fileName!='' && $scope.organId=='') {

              alert("Please.. Choose Organization Name.");

          }


       
      }

    }



    
    $scope.tripSubmitFunc = function() {  

      var tripSearchUrl = 'http://128.199.159.130:9000/getTripsheetValue?userId=MSS&orgId=RAJ&tripNumber=2000668258&date=2018-04-05&vehicleId=AP04TW4344';

        console.log(tripSearchUrl);

       $http({
            method: 'GET',
            url: tripSearchUrl
        }).then(function successCallback(response) {

              if(response.status==200) {
                  console.log("Trip Search Successfully!..");

                  console.log(response.data);
              }

        }, function errorCallback(response) {
              
              console.log(response.status);

        });

    }




}]);
