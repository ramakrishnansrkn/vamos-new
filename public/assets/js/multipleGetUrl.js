app.controller('mainCtrl',['$scope','$http','vamoservice','_global','$filter','$translate', function($scope, $http, vamoservice, GLOBAL,$filter,$translate){
 var language=localStorage.getItem('lang');
 $scope.multiLang=language;
 $scope.dealerName     =  localStorage.getItem('dealerName');
 $translate.use(language);
 var translate = $filter('translate');
 var assLabel    =  localStorage.getItem('isAssetUser');
 $scope.trvShow  =  localStorage.getItem('trackNovateView');
 $scope.allVehicles = 0;
 $scope.error="";
 $scope.msg="";
 $scope.modal_error = "";
 if( localStorage.getItem('mapNo')!=undefined){
 	$scope.map_change=localStorage.getItem('mapNo');
 	preMap=$scope.map_change;
 }
 else {
 	localStorage.setItem('mapNo',0);
 	$scope.map_change=0;
 	preMap=0;
 }
 $scope.vg       =  getParameterByName('vg').split(":")[0];

 if(assLabel=="true") {
   $scope.vehiLabel = "Asset";
 } else if(assLabel=="false") {
   $scope.vehiLabel = "Vehicle";
 } else {
   $scope.vehiLabel = "Vehicle";
 }	
//Global Variables
$scope.selectScreen 		 =  ['screen1'];
getVehicleLocations 		 =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations';
getSelectedVehicleLocation   =  GLOBAL.DOMAIN_NAME+'/getSelectedVehicleLocation?vehicleId=';
getSelectedVehicleLocation1  =  GLOBAL.DOMAIN_NAME+'/getSelectedVehicleLocation1?vehicleId=';

path1  = [];
path2  = [];
path3  = [];
path4  = [];

var _mapsDetails = {};
var _pathDetails = {};
var osm_mapsDetails = {};	



$scope.getshortNames = function(vg) {
	startLoading();
 if($scope.vg!=undefined){
  $username =JSON.parse(localStorage.getItem('userIdName')).split(",")[1];
        //alert($username)
        var groupName=vg;
        $scope.vehiSelected=vg;
        $.ajax({
          async: false,
          method: 'GET',
          url: "HomeController/getVehicle",
          data: {'groupName': groupName,'username' : $username },
          success: function (response) {

           //alert(response);
           $scope.repNames = [];
           $scope.repNames = response;
         //location.reload();
       }

     });
      }
      return $scope.repNames;
    }
    $scope.vehiclelist  = $scope.getshortNames($scope.vg);

    $scope.trackMultiVehi =  function(vehicles){
       //alert($scope.vehiSelected);
       if($scope.vehiSelected.length)
        angular.forEach($scope.vehiSelected, function(value){
     //alert(value.vehicleId);
   })
    }

    function getParameterByName(name) {
      name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
      results = regex.exec(location.search);
      return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }


    function ValidateEmail(mail)   
    {  
     if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))  
     {  
      return (true)  
    }  
    $scope.modal_error=translate('email_error');
    return (false)  
  }  

  // function validatePhone(phone) {

  //   var mobile = phone;
  //   var pattern = /^[0-9]{10}(,[0-9]{10})*$/;
    
  //   if (pattern.test(mobile)) {
  //     return true;
  //   }
  //   if(mobile){
  //     $scope.modal_error=translate("mob_error");
  //     return false;
  //   }else{
  //     return true;
  //   }

    
  // }

// $scope.update = function(){
// 	//startLoading();
// 	vamoservice.getDataCall(getVehicleLocations+'?group='+$scope.vehicleSelected+':'+$scope.fcode).then(function(data){
// 		angular.forEach(data, function(groupVehicle){
// 			if($scope.vehicleSelected+':'+$scope.fcode == groupVehicle.group)
// 				if(groupVehicle.vehicleLocations){
// 					$scope.vehicles = groupVehicle.vehicleLocations;
// 					//stopLoading();
// 				}
// 		})
// 	})
// }

 // $scope.selectAll = function() {

 // 	if($scope.vehicleSelected!=undefined) {

 // 		startLoading();

 //    for(key in _mapsDetails) {
	// 	 _mapsDetails[key].setMap(null);
	// }

 //    _mapsDetails = {};


	// 	    vamoservice.getDataCall(getVehicleLocations+'?group='+$scope.vehicleSelected+':'+$scope.fcode).then(function(data) {

 //               angular.forEach(data, function(groupVehicle){

	// 	    if($scope.vehicleSelected+':'+$scope.fcode == groupVehicle.group){



	// 		   angular.forEach(groupVehicle.vehicleLocations, function(value, key) {

	// 		   	   console.log(value);

	// 		   	   var url      =  getSelectedVehicleLocation+value.vehicleId;
	//                    var urlVeh   =  getSelectedVehicleLocation1+value.vehicleId;



	// 		        selectingScreen('screen1', value, value.vehicleId);
	// 		        serviceCall('screen1', url, urlVeh, value.vehicleId);

	// 			});	

	// 		   $scope.allVehicles=1;

	// 	    } 
	// 	          });

	// 		}); 


 //        stopLoading();

 //        } else {

 //        	alert(translate('Select group')+'!');
 //        }
 // }



 $scope.update = function(){
   startLoading();
   
   $scope.gName=$scope.groupSelected+':'+$scope.fcode;
   var urlGroup = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+$scope.gName;
   $http.get(urlGroup).success(function(data){
     angular.forEach(data, function(val, key){
                  //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
                  //alert($scope.gName);
                  if($scope.gName == val.group){
                    $scope.gIndex = val.rowId;
                    $scope.groupid=$scope.gIndex;
                    $scope.vehiname   =  data[$scope.gIndex].vehicleLocations[0].vehicleId;
                    $scope.gName      =  data[$scope.gIndex].group;
                    localStorage.setItem('user', JSON.stringify($scope.vehiname+','+$scope.gName)); 
                  }
                });


     var pageUrl='geturl?vg='+$scope.gName;
     $(location).attr('href',pageUrl);
   //stopLoading();
 });


 }
 $scope.mulVehicles="";
 $scope.selectedVehi=[];
 $scope.selectVehGroup= function(type) {
   $scope.error="";
   $scope.msg="";
   $scope.modal_error = "";
   $scope.selectionType=type;
     //alert($scope.days);
     if($scope.days!=undefined&&$scope.days!=""){
      if(type!='All'){
       if($scope.selectedVehi.length!=0){
        angular.forEach($scope.selectedVehi, function(selVehi,key){
          if(key==0)
            $scope.mulVehicles = selVehi.vehicleId;
          else
            $scope.mulVehicles = $scope.mulVehicles+','+selVehi.vehicleId; 
                            //alert($scope.mulVehicles);
                          })
        $('#dialog').trigger('click');

      }else{
        $scope.error=translate("sel_veh");
      }

    }else{
      $(".bs-select-all").trigger('click');
      $scope.group=getParameterByName('vg');
      $('#dialog').trigger('click');
    }
  }else{
    $scope.error=translate("sel_day");
  }


};
$scope.geturl = function() {
  stopLoading();
  $scope.error="";
  $scope.msg="";
  $scope.modal_error="";
  console.log('inside geturl');
  $scope.FileNames=[];
  var getUrl;
  var mailId = document.getElementById("mail").value;
  var phone  = document.getElementById("phone").value;
  var comments  = document.getElementById("comments").value;
      //alert($scope.selectionType);
      if(mailId=='' && phone==''){
        $scope.modal_error='Please enter email id or phone number to get url';
      }
      else if(ValidateEmail(mailId) || phone!=''){
        $scope.modal_error='';
        $('#closeBut').trigger('click');
        if($scope.selectionType!='All'){
          getUrl=GLOBAL.DOMAIN_NAME+'/getVehicleExpForMultipleVehicles?multipleVehicles='+$scope.mulVehicles+'&fcode='+$scope.fcode+'&days='+$scope.days+'&mailId='+mailId+'&phone='+phone+'&comments='+comments;
        }else{
         getUrl=GLOBAL.DOMAIN_NAME+'/getVehicleExpForMultipleVehicles?multipleVehicles=&fcode='+$scope.fcode+'&days='+$scope.days+'&mailId='+mailId+'&phone='+phone+'&group='+$scope.group+'&comments='+comments;
       }
       console.log(getUrl);

       $http.get(getUrl).success(function(data){
        stopLoading();
                  //alert(data);
                  if(data.trim()!='Failed'){
                    $scope.msg=translate("req_send");
                  }else{
                   $scope.error=translate("req_fail");
                 }
                 clearValues();

               });

     }
   }

   function clearValues(){
     document.getElementById("mail").value ="";
     document.getElementById("phone").value ="";
     document.getElementById("comments").value ="";
     $scope.days="";
   }

   function filterGroup(groups){
     var filter = [];
     var splitValue ='';
     angular.forEach(groups, function(value){
      if (value.group)
       splitValue = value.group.split(":");
     filter.push(splitValue[0])
   })
     return filter
   }

// var getJoke = function(addressUrl){
//    var fetchingAddress = '';
// 	  $.ajax({
// 	    url:addressUrl, 
// 	    async: false,   
// 	    success:function(dat) {
// 	      fetchingAddress = dat.results[0].formatted_address
// 	    }
// 	})
//   return fetchingAddress;
//  }


function init() {

	// var vehicleno  =  getParameterByName('vehicleId');
	// if(vehicleno==""||vehicleno==undefined){
	// vehicleno=JSON.parse(localStorage.getItem('user')).split(',')[0];
 //    }
	// var url        =  getSelectedVehicleLocation+vehicleno;
	// var urlVeh     =  getSelectedVehicleLocation1+vehicleno;
	try {
		
		startLoading();
		
		vamoservice.getDataCall(getVehicleLocations).then(function(data){
			

			$scope.sliceGroup = filterGroup(data)
			groupName	 	=	data[0].group.split(':');
			$scope.fcode 	=	groupName[1];
      $scope.error="";
      $scope.msg="";
			// $scope.getVehicle = data
			stopLoading();
		})
	} catch (err){
		console.log('print err'+err)
		
	}

  /*   $http({
            method : "GET",
            url : getVehicleLocations
        }).then(function mySuccess(response) {

            $scope.data = response.data;
           
            mapProp = {
		       center: new google.maps.LatLng( $scope.data[0].latitude, $scope.data[0].longitude ),
		       zoom: 7,
			   zoomControlOptions: { position: google.maps.ControlPosition.LEFT_TOP}, 
			   mapTypeId: google.maps.MapTypeId.ROADMAP
		    };


            setMarkers(response.data[0]);

		    googleMap = new google.maps.Map(document.getElementById("maploc1"),mapProp);


            

		    //stopLoading();
				
      }, function myError(response) {
            console.log( response.statusText );
          });*/

        };
        init();
        $("#testLoad").load("../public/menu");

      }]);

