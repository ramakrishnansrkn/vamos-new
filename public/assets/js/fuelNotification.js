app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global', '$translate' ,function($scope, $http, vamoservice, $filter, GLOBAL,$translate){
  //global declaration
  $scope.uiDate        =  {};
  $scope.uiValue       =  {};
 
  $scope.interval      =  "";
  $scope.fuelDataMsg   =  "";
  $scope.portNoFilter  =  false;
  $scope.showPage=false;
  $scope.tapchanged=true;
  var licenceExpiry="";
  var strExpired='expired';
  if(localStorage.getItem('licenceExpiry'))licenceExpiry=localStorage.getItem('licenceExpiry');
  $scope.errMsg="";
  var language=localStorage.getItem('lang');
  var expiryDays='';
  $translate.use(language);
  var translate = $filter('translate');
  var assLabel          =  localStorage.getItem('isAssetUser');
  $scope.vehiAssetView  =  true;
  $scope.trvShow        =  localStorage.getItem('trackNovateView');

  if(assLabel=="true") {
        $scope.vehiLabel = "Asset";
        $scope.vehiImage = true;
        $scope.vehiAssetView = false;
  } else if(assLabel=="false") {
        $scope.vehiLabel = "Vehicle";
        $scope.vehiImage = false;
        $scope.vehiAssetView = true;
  } else {
        $scope.vehiLabel = "Vehicle";
        $scope.vehiImage = false;
        $scope.vehiAssetView = true;
  }
   $scope.fill_list=[];
   $scope.drop_list=[];
   $scope.excess_consumption_list=[];
   // $scope.dropped_list=[];
   

  var tab = getParameterByName('tn');
  var fuelDataVals;
       
  function getParameterByName(name) {
      name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
      var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
          results = regex.exec(location.search);
      return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  //global declartion
  $scope.locations = [];
  $scope.url       = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
  $scope.gIndex    = 0;

  //$scope.locations01 = vamoservice.getDataCall($scope.url);
    $scope.trimColon = function(textVal) {

      if(textVal){
       var spltVal = textVal.split(":");
       return spltVal[0];
      }
    }

  function sessionValue(vid, gname,licenceExpiry){
    localStorage.setItem('user', JSON.stringify(vid+','+gname));
    localStorage.setItem('licenceExpiry', licenceExpiry);
    $("#testLoad").load("../public/menu");
  }
  

  function convert_to_24h(time_str) {
  //console.log(time_str);
      var str      = time_str.split(' ');
      var stradd   = str[0].concat(":00");
      var strAMPM  = stradd.concat(' '+str[1]);
      var time     = strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
      var hours    = Number(time[1]);
      var minutes  = Number(time[2]);
      var seconds  = Number(time[2]);
      var meridian = time[4].toLowerCase();
  
      if (meridian == 'p' && hours < 12) {
        hours = hours + 12;
      }
      else if (meridian == 'a' && hours == 12) {
        hours = hours - 12;
      }     
      var marktimestr =''+hours+':'+minutes+':'+seconds;      
      return marktimestr;
    };

    
  function formatAMPM(date) {
      var date = new Date(date);
      var hours = date.getHours();
      var minutes = date.getMinutes();
      var ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
      return strTime;
  }

    //get the value from the ui
  function getUiValue(){
      $scope.uiDate.fromdate   = $('#dateFrom').val();
      $scope.uiDate.fromtime   = $('#timeFrom').val();
      $scope.uiDate.todate     = $('#dateTo').val();
      $scope.uiDate.totime     = $('#timeTo').val();
      if(localStorage.getItem('timeTochange')!='yes'){
             updateToTime();
             $scope.uiDate.totime    =   localStorage.getItem('toTime');
        }
      $scope.uiDate.fromtimes  = convert_to_24h($scope.uiDate.fromtime);
      $scope.uiDate.totimes    = convert_to_24h($scope.uiDate.totime);
  }

function setBtnEnable(btnName){
   $scope.yesterdayDisabled = false;
   $scope.weekDisabled = false;
   $scope.monthDisabled = false;
   $scope.todayDisabled = false;
   $scope.lastmonthDisabled = false;
   $scope.thisweekDisabled = false;

   switch(btnName){

    case 'yesterday':
      $scope.yesterdayDisabled = true;
      break;
     case 'today':
      $scope.todayDisabled = true;
      break;
    case 'thisweek':
      $scope.thisweekDisabled = true;
      break;
    case 'lastweek':
      $scope.weekDisabled = true;
      break;
    case 'month':
      $scope.monthDisabled = true;
      break;
    case 'lastmonth':
      $scope.lastmonthDisabled = true;
      break;
   }


}
setBtnEnable("init");
$scope.durationFilter    =   function(duration){
  $scope.showPage=false;
  //alert('inside function');
  startLoading();
  var now = new Date();
  $scope.uiDate.todate      = getTodayDate(now.setDate(now.getDate() - 1));
  switch(duration){
    
    case 'yesterday':
       setBtnEnable('yesterday');
       var d = new Date();
       $scope.uiDate.fromdate= getTodayDate(d.setDate(d.getDate() - 1));
       $scope.uiDate.totime  = '11:59 PM';
      // datechange();
     break;
   case 'thisweek':
       setBtnEnable('thisweek');
       var d=new Date();
       var day = d.getDay(),
       diff = d.getDate() - day + (day == 0 ? -6:1);
       var firstday= new Date(d.getFullYear(), d.getMonth(), diff);
       $scope.uiDate.fromdate      = getTodayDate(firstday.setDate(firstday.getDate() ));
       $scope.uiDate.todate= getTodayDate(new Date().setDate(new Date().getDate() ));

       $scope.uiDate.totime       = formatAMPM(d);
      //datechange();
    break;
    case 'lastweek':
       setBtnEnable('lastweek');
       var d = new Date();
       var day = d.getDay(),
          diff = d.getDate() - day + (day == 0 ? -6:1)-7;
       var diff1=d.getDate() - day + (day == 0 ? -6:1)-1;
       var firstday= new Date(d.getFullYear(), d.getMonth(), diff);
       var lastday= new Date(d.getFullYear(), d.getMonth(), diff1);
       $scope.uiDate.fromdate      = getTodayDate(firstday.setDate(firstday.getDate() ));
       $scope.uiDate.todate       = getTodayDate(lastday.setDate(lastday.getDate() ));
       $scope.uiDate.totime  = '11:59 PM';
      //datechange();
     break;
    case 'month':
       setBtnEnable('month');
       var date = new Date();
       var firstdate = new Date(date.getFullYear(), date.getMonth(), 1);
       $scope.uiDate.fromdate       = getTodayDate(firstdate.setDate(firstdate.getDate() ));
       $scope.uiDate.todate       = getTodayDate(date.setDate(date.getDate() ));

      $scope.uiDate.totime      = formatAMPM(date);
       //datechange();
     break;
    case 'today':
       setBtnEnable('today');
       var d = new Date();
       $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() ));
       $scope.uiDate.todate       = getTodayDate(d.setDate(d.getDate() ));
       $scope.uiDate.totime       = formatAMPM(d);
      //datechange();
     break;
    case 'lastmonth':
       setBtnEnable('lastmonth');
       var date = new Date();
       var firstdate = new Date(date.getFullYear(), date.getMonth()-1, 1);
       var lastdate = new Date(date.getFullYear(), date.getMonth(), 0);
       $scope.uiDate.fromdate       = getTodayDate(firstdate.setDate(firstdate.getDate() ));
       $scope.uiDate.todate      = getTodayDate(lastdate.setDate(lastdate.getDate() ));

       $scope.uiDate.totime   = '11:59 PM';
      //datechange();
       break;
  }
   
  webCall();
}

  function webCall(){
      $scope.NotificationData=[];
      $scope.fill_list = [];
      $scope.drop_list  = [];
      $scope.filled_list = [];
      $scope.excess_consumption_list=[];
      // $scope.dropped_list = [];
      $scope.notificationError = "" ;
      $scope.notiError = "" ;

      $('.nav-tabs li').removeClass('active');
      $('.tab-content .tab-pane').removeClass('active');
      
      if((checkXssProtection($scope.uiDate.fromdate) == true) && ((checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true))) {
          var fuelNotificationUrl  = GLOBAL.DOMAIN_NAME+'/getFuelNotificationReport?vehicleId='+$scope.vehIds+'&fromDateUTC='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateUTC='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));           
          console.log(fuelNotificationUrl);
        if((licenceExpiry.indexOf(strExpired)== -1)||licenceExpiry==""||licenceExpiry=="-"){
            $http.get(fuelNotificationUrl).success(function(data) {
              $scope.showPage=true;
              stopLoading();
              $scope.activeTap='FILL';
              if(data=='Failed'){
                  $scope.notificationError = translate("curl_error");
              }else{
                if(data!=""){
                $scope.NotificationData = data;
                if(data[0].error){
                 $scope.notiError = data[0].error ;
                }
                if(!data[0].error){
                    angular.forEach(data, function(val, key) {
                      if(val.type=='FUEL FILL'){
                        if(val.subType!="Instant"){
                          $scope.fill_list.push(val);
                        }

                      }
                      else if(val.type=='FUEL DROP'){
                         if(val.subType!="Instant"){
                          $scope.drop_list.push(val);
                        }

                      }
                      else {                      
                          $scope.excess_consumption_list.push(val);
                      }
                    });
                    changeTabs('FILL');
                   // $scope.fill_list     =  $filter('filter')(data, function (item) { return item.type.toUpperCase() == "FUEL FILL" },true); 
                   // $scope.drop_list     =  $filter('filter')(data, function (item) { return item.type.toUpperCase() == "FUEL DROP" },true); 
                   // $scope.filled_list   =  $filter('filter')(data, function (item) { return item.type.toUpperCase() == "EXCESS FUEL CONSUMPTION" },true); 
                   // $scope.dropped_list  =  $filter('filter')(data, function (item) { return item.type.toUpperCase() == "DROPPED" },true);
                }
                // else if($scope.dropped_list.length){
                //   $scope.activeTap = "DROPPED"
                // }
              }
                
              }   
            });
        }else{
              stopLoading();
              $scope.errMsg=licenceExpiry;
              $scope.showErrMsg = true;
        } 
    }
  } 

  $scope.changeTabs=function(tab){
    $scope.ongoing_list=[];
    $scope.detection_list=[];
    $scope.finalised_list=[];
    if(tab=='fill'){
      $scope.ongoing_list     =  $filter('filter')($scope.fill_list, function (item) { return item.subType=='Ongoing' },true); 
      $scope.detection_list     =  $filter('filter')($scope.fill_list, function (item) { return item.subType=='Detection' },true); 
      $scope.finalised_list    =  $filter('filter')($scope.fill_list, function (item) { return item.subType.toUpperCase()=='ACCURACY' },true); 
    }
    else if(tab=='drop'){
      $scope.ongoing_list     =  $filter('filter')($scope.drop_list, function (item) { return item.subType=='Ongoing' },true); 
      $scope.detection_list     =  $filter('filter')($scope.drop_list, function (item) { return item.subType=='Detection' },true); 
      $scope.finalised_list    =  $filter('filter')($scope.drop_list, function (item) { return item.subType.toUpperCase()=='ACCURACY' },true);
    }
    else {
      
    }
  }
  


  // initial method
  $scope.$watch("url", function (val) {
    vamoservice.getDataCall($scope.url).then(function(data) {
    //startLoading();
      $scope.selectVehiData = [];
      $scope.vehicle_group=[];
      $scope.vehicle_list = data;

      if(data.length) {
        $scope.vehiname = getParameterByName('vid');
        $scope.uiGroup  = $scope.trimColon(getParameterByName('vg'));
        $scope.gName  = getParameterByName('vg');
        angular.forEach(data, function(val, key) {
        //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
          if($scope.gName == val.group) {
            $scope.gIndex = val.rowId;
 
            angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys){

              $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

                if($scope.vehiname == value.vehicleId){
                  $scope.shortNam = value.shortName;
                  $scope.vehIds   = value.vehicleId;
                  licenceExpiry=value.licenceExpiry;
                  expiryDays=value.expiryDays;
                }
            })
          }
        });

      //console.log($scope.selectVehiData);
        sessionValue($scope.vehiname, $scope.gName,licenceExpiry)
        $scope.notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
        $('#notncount').text($scope.notncount);
        window.localStorage.setItem('totalNotifications',$scope.notncount);
      }
      
      $scope.uiDate.fromdate    = localStorage.getItem('fromDate');
      $scope.uiDate.fromtime    = localStorage.getItem('fromTime');
      $scope.uiDate.todate      = localStorage.getItem('toDate');
      $scope.uiDate.totime      =  localStorage.getItem('toTime');
      if(localStorage.getItem('timeTochange')!='yes'){
                updateToTime();
                $scope.uiDate.totime    =   localStorage.getItem('toTime');
                }
      $scope.uiDate.fromtimes  =  convert_to_24h($scope.uiDate.fromtime);
      $scope.uiDate.totimes    =  convert_to_24h($scope.uiDate.totime);
    //$scope.uiDate.totime     =  '11:59 PM';
    
      startLoading();
      webCall();
    //stopLoading();
    }); 
  });

    
  $scope.groupSelection   = function(groupName, groupId) {
    startLoading();
    
    licenceExpiry="";
    $scope.errMsg="";
    $scope.gName    =  groupName;
    $scope.uiGroup  =  $scope.trimColon(groupName);
    $scope.gIndex   =  groupId;
    var url         =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+groupName;

    vamoservice.getDataCall(url).then(function(response){
    
      $scope.vehicle_list  =  response;
      $scope.shortNam      =  response[$scope.gIndex].vehicleLocations[0].shortName;
      $scope.vehiname      =  response[$scope.gIndex].vehicleLocations[0].vehicleId;
      licenceExpiry   = response[$scope.gIndex].vehicleLocations[0].licenceExpiry;
      expiryDays   = response[$scope.gIndex].vehicleLocations[0].expiryDays;
      sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
      $scope.selectVehiData = [];
    //console.log(response);
        angular.forEach(response, function(val, key){
          if($scope.gName == val.group){
          //$scope.gIndex = val.rowId;
             angular.forEach(response[$scope.gIndex].vehicleLocations, function(value, keys){

                $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

                if($scope.vehiname == value.vehicleId) {

                  $scope.shortNam = value.shortName;
                  $scope.vehIds   = value.vehicleId;
                  
                }
              });
            }
        });
      $scope.notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
      $('#notncount').text($scope.notncount);
      window.localStorage.setItem('totalNotifications',$scope.notncount);

            getUiValue();
            webCall();
          
      //stopLoading();
    });

  }

  
  $scope.genericFunction  = function (vehid, index,shortname,position, address,groupName,licenceExp) {
    $scope.showPage=false;
      startLoading();
     
      licenceExpiry=licenceExp;
      $scope.errMsg="";
      $scope.vehiname = vehid;
      //alert($scope.vehiname);
      console.log($scope.vehiname);

    sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
  
    angular.forEach($scope.vehicle_list[$scope.gIndex].vehicleLocations, function(val, key){

          //console.log(val);

      if( vehid == val.vehicleId ) {

          $scope.shortNam = val.shortName;
          $scope.vehIds   = val.vehicleId;
          expiryDays=val.expiryDays;
      }

    });

              getUiValue();
              webCall();
  }


  $scope.submitFunction   = function(){
     $scope.showPage=false;
    $scope.yesterdayDisabled = false;
     $scope.weekDisabled = false;
     $scope.monthDisabled = false;
     $scope.todayDisabled = false;
     $scope.lastmonthDisabled = false;
     $scope.thisweekDisabled = false;
     startLoading();
      getUiValue();
      webCall(); 
  //webServiceCall();
    //stopLoading();
  }

  $('#minus').click(function(){
    $('#menu').toggle(1000);
  });

  

}]);
