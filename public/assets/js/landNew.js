
var globalIP  =  document.location.host;
var context   =  '/'+document.location.pathname.split('/')[1];
//logo loading
var logo = document.location.host;
var LandTarr=["LANDT" , "CMTL" , "RPTL" , "GTTPLGOA" , "MBTL" , "DNTL" , "RMTL", "NATL" , "ABTL" , "BBTL" , "PBTL" , "KPTL" , "CKTL" , "MJTL" , "RPTL-01" , "JJTL" , "JSTL" , "KSEB" , "GKTL" , "PTTL", "AGNTL" ,"BKGTL","GATL","PKGAJ","GHTL2","BLTL","PKG-AM","TR208" , "KPCMKNHTS" ,"KPCHO"];
const deviceModalList = ["VTRACK2", "JV200", "V5", "TR06","GT06N"];


function ValidateIPaddress(ipaddress) {  
  var ipformat = /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;  
  if(ipaddress.match(ipformat)) {
    return (true);  
}  
  //alert("You have entered an invalid IP address!")  
  return (false);  
}  
//added for changing theme color
var themeColor = "#196481"; 
if(localStorage.getItem('themeColor')!=undefined && localStorage.getItem('themeColor')!=null){
    themeColor = localStorage.getItem('themeColor')
}  
//added for sidebar themecolor Changes end
$('.sidebar-nav').css('background-color',themeColor);
//added for sidebar themecolor Changes end
//logo = 'localhost';
if(ValidateIPaddress(logo)) {
  var parser    =   document.createElement('a');
  //parser.href   =   document.location.ancestorOrigins[0];
  /*if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 ) 
   {
        parser.href   =   document.location.ancestorOrigins[0];
   }
   else if(navigator.userAgent.indexOf("Chrome") != -1 )
   {
        parser.href   =   document.location.ancestorOrigins[0];
   }
   else if(navigator.userAgent.indexOf("Safari") != -1)
   {
        parser.href   =   document.location.ancestorOrigins[0];
   }
   else if(navigator.userAgent.indexOf("Firefox") != -1 ) 
   {
         parser.href   =   document.location.hostname;
   }
   else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) //IF IE > 10
   {
      parser.href   =   document.location.ancestorOrigins[0]; 
   }  
   else 
   {
       parser.href   =   document.location.ancestorOrigins[0];
   }*/
   if(typeof (document.location.ancestorOrigins) != "undefined"){
       parser.href = document.location.ancestorOrigins[0];
   }
   else{
       parser.href = document.location.hostname;
   }


   logo          =   parser.host;
}

var imgName   =  'https://sgp1.digitaloceanspaces.com/vamotest/picture/'+logo.replace("www.", "")+'.small.png';
// var folder    =  imgName;
// var wwwSplit  =  logo.split(".");

// if(wwwSplit[0]=="www"){
//   wwwSplit.shift();
//   imgName = context+'/public/uploads/'+wwwSplit[0]+'.'+wwwSplit[1]+'.small.png';
// }
var contextMenu = '/'+document.location.pathname.split('/')[1];
$.get('//'+globalIP+contextMenu+'/public/getFileNames', function(response) {
    var FileNames   = response;
    if(FileNames.length==0){
        document.getElementsByClassName('ReleaseNotes')[0].style.display = 'none';
    }

}).error(function(){
    console.log('error in getFileNames...');
});

$('#imagesrc').attr('src', imgName);

 //var apps = angular.module('mapApps', ['ui.select','naturalSort']);
 var apps = angular.module('mapApps', ['ui.select','naturalSort','pascalprecht.translate']);
 //added for language translation
 apps.config(function($translateProvider) {
  $translateProvider.translations('hi', {
    'Successfully updated' : 'सफलतापूर्वक सामयिक  किया',
    'Site Already Present' : 'साइट पहले ही उपस्थित',
    'Enter Site Name'  : 'साइट का नाम भरे',
    'Enter Valid Site Name' : 'सही साइट का नाम भरे',
    'Enter all the field / Mark the Site' : 'सभी क्षेत्रों को भरे /साइट निशान लगाए',
    'Payment overdue for' : 'भुगतान बांकी है',
    'day' : 'दिन',
    'Please make payment at the earliest to avoid de-activation' :'कृपया निष्क्रियकरण टालने के लिए भुगतान करे',
    'Yes' :'हाँ',
    'No'  :'नहीं',
    'ON'  : 'चालू करे',
    'OFF' : 'बंद करे',
    'Heavy Vehicle' : 'भारी गाडी',
    'Name' : 'नाम',
    'Today_Distance' : 'आज का सफर',
    'ACC_Status' : 'प्रज्वलन का स्थिति',
    'Loc_Time' : 'स्थान का समय',
    'Comm_Time' : 'संचारण_अवधि',
    'expired' : 'इस गाडी का जीपीएस गतवधिक हो गया हे !',
    'Odo' : 'ओडोमीटर (किमी)',
    'Ignition' : 'प्रज्वलन',
    'Veh_Battery' : 'गाडी का बैटरी',
    'Dev_Battery' : 'उपकरण का बैटरी',
    'Covered' : 'सफर किया गया दूर(किमी)',
    'Max_Speed' : 'अधिकतम गति (किमी)',
    'Speed' : 'गति (किमी)',
    'Direction' : 'दिशा',
    'kms' : 'किमी',
    'Track' : 'खोजे',
    'Reports' : 'रिपोर्ट्स',
    'History' : 'History',
    'Site' : 'साईट',
    'MultiTrack' : 'MultiTrack',
    'Position'  : 'Position',
    'Parked' : 'Parked',
    'Moving' : 'Moving',
    'Idle'  : 'Idle',
    'No_data' : 'No data',
    'reason' : 'Reason',
    'Vehicle' : 'Vehicle',
    'Asset' : 'Asset',
})
  .translations('en', {
    'Payment_overdue_for' : 'Payment overdue for',
    'Successfully updated' : 'Successfully updated',
    'Site Already Present' : 'Site Already Present',
    'Enter Site Name'  : 'Enter Site Name',
    'Enter Valid Site Name' : 'Enter Valid Site Name',
    'Enter all the field / Mark the Site' : 'Enter all the field / Mark the Site',
    'Payment overdue for' : 'Payment overdue for',
    'day' : 'day',
    'Please make payment at the earliest to avoid de-activation' :'Please make payment at the earliest to avoid de-activation',
    'Yes' :'Yes',
    'No'  :'No',
    'ON'  : 'ON',
    'OFF' : 'OFF',
    'Heavy Vehicle' : 'Heavy Vehicle',
    'Name' : 'Name',
    'Today_Distance' : 'Today Distance',
    'ACC_Status' : 'ACC Status',
    'Loc_Time' : 'Loc Time',
    'Comm_Time' : 'Comm Time',
    'expired' : 'This Vehicle GPS has been expired!',
    'Odo' : 'Odo (Kms)',
    'Ignition' : 'Ignition',
    'Veh_Battery' : 'Veh Battery',
    'Dev_Battery' : ' Dev Battery',
    'Covered' : 'Covered (Kms)',
    'Max_Speed' : 'Max Speed (Kms)',
    'Speed' : 'Speed (Kms)',
    'Direction' : 'Direction',
    'kms' : 'kms',
    'Track' : 'Track',
    'Reports' : 'Reports',
    'History' : 'History',
    'Site' : 'Site',
    'MultiTrack' : 'MultiTrack',
    'Position'  : 'Position',
    'Parked' : 'Parked',
    'Moving' : 'Moving',
    'Idle'  : 'Idle',
    'No_data' : 'No Data',
    'no_data_reason' : 'No Data Reason',
    'Vehicle' : 'Vehicle',
    'Asset' : 'Asset',
    'Load_Details' : 'Load Details',
    'temperature' : 'Temperature'
}).translations('es', {
    'Payment_overdue_for' : 'Pago atrasado para',
    'Successfully updated' : 'Exitosamente actualizado',
    'Site Already Present' : 'Sitio ya presente',
    'Enter Site Name'  : 'ingresar nombre de sitio',
    'Enter Valid Site Name' : 'Ingresar nombre de sitio válido',
    'Enter all the field / Mark the Site' : 'Ingrese todo el campo / Marque el sitio inglés',
    'Payment overdue for' : 'Pago atrasado para',
    'day' : 'Día',
    'Please make payment at the earliest to avoid de-activation' :'Realice el pago lo antes posible para evitar la desactivación',
    'Yes' :'Sí',
    'No'  :'No',
    'ON'  : 'encendido',
    'OFF' : 'desactivado',
    'Heavy Vehicle' : 'Vehículo pesado',
    'Name' : 'Nombre',
    'Today_Distance' : 'Distancia de hoy',
    'ACC_Status' : 'Estado de la Cuenta',
    'Loc_Time' : 'tiempo ubicación',
    'Comm_Time' : 'Tiempo de comunicación',
    'expired' : '¡Este GPS del vehículo ha caducado!',
    'Odo' : 'Odómetro (Kilómetros)',
    'Ignition' : 'encendido',
    'Veh_Battery' : 'Batería del vehículo',
    'Dev_Battery' : 'Desarrollo de Batería',
    'Covered' : 'Cubierto (Kilómetros)',
    'Max_Speed' : 'Velocidad máxima (Kilómetros)',
    'Speed' : 'Speed (Kms)',
    'Direction' : 'Dirección',
    'kms' : 'kilómetros',
    'Track' : 'vía',
    'Reports' : 'Informes',
    'History' : 'Historia',
    'Site' : 'Sitio',
    'MultiTrack' : 'Multipista',
    'Position'  : 'Position',
    'Parked' : 'Estacionado',
    'Moving' : 'conmovedor',
    'Idle'  : 'inactivo',
    'No_data' : 'Sin datos',
    'Vehicle' : 'Vehículo',
    'Asset' : 'Activo',
    'Load_Details' : 'Detalles de carga',
    'temperature' : 'Temperatura',
}).translations('fr', {
    'Payment overdue for' : 'Paiement en retard pour',
    'Successfully updated' : 'Mis à jour avec succès',
    'Site Already Present' : 'Site déjà présent',
    'Enter Site Name'  : 'Entrez le nom du site',
    'Enter Valid Site Name' : 'Entrez un nom de site valide',
    'Enter all the field / Mark the Site' : 'Entrez tous les champs / Marquez le site anglais',
    'Payment overdue for' : 'Paiement en retard pour',
    'day' : 'Jour',
    'Please make payment at the earliest to avoid de-activation' :'Veuillez effectuer le paiement tôt pour éviter la désactivation',
    'Yes' :'Oui',
    'No'  :'Non',
    'ON'  : 'AU',
    'OFF' : 'DÉSACTIVÉ',
    'Heavy Vehicle' : 'Heavy Vehicle',
    'Name' : 'Nom',
    'Today_Distance' : "Distance aujourd'hui",
    'ACC_Status' : "ACC",
    'Loc_Time' : 'Heure locale',
    'Comm_Time' : 'Temps de communication',
    'expired' : 'Le GPS de ce véhicule a expiré !',
    'Odo' : 'Odo (Kms)',
    'Ignition' : 'Allumage',
    'Veh_Battery' : 'Batterie de véhicule',
    'Dev_Battery' : 'Batterie de développement',
    'Covered' : 'Couvert (Km)',
    'Max_Speed' : 'Vitesse (Kms)',
    'Speed' : 'Vitesse (Kms)',
    'Direction' : 'Direction',
    'kms' : 'kms',
    'Track' : 'Suivre',
    'Reports' : 'Rapports',
    'History' : 'Histoire',
    'Site' : 'Site',
    'MultiTrack' : 'MultiTrack',
    'Position'  : 'Poste',
    'Parked' : 'Stationné',
    'Moving' : 'En mouvement',
    'Idle'  : 'Au repos',
    'No_data' : 'Pas de données',
    'Vehicle' : 'Véhicule',
    'Asset' : 'Asset',
    'Load_Details' : 'Détails du chargement',
    'temperature' : 'Température'
}).translations('ar', {
    'Payment_overdue_for' : 'دفع تأخر',
    'Successfully updated' : 'تم التحديث بنجاح',
    'Site Already Present' : 'الموقع الحالي إذا',
    'Enter Site Name'  : 'أدخل اسم الموقع',
    'Enter Valid Site Name' : 'أدخل اسم صالح الموقع',
    'Enter all the field / Mark the Site' : 'أدخل كل مجال / مارك الانجليزية الموقع',
    'Payment overdue for' : 'دفع تأخر',
    'day' : "يوم",
    'Please make payment at the earliest to avoid de-activation' :'رجى إجراء الدفع مبكرًا لتجنب التعطيل.',
    'Yes' :"نعم",
    'No'  :"لا",
    'ON'  : "على",
    'OFF' : "إيقاف",
    'Heavy Vehicle' : 'Heavy Vehicle',
    'Name' : 'اسم',
    'Today_Distance' : "اليوم القطر",
    'ACC_Status' : 'فعيل حالة التلامس مع الضوء الداخلي.',
    'Loc_Time' : 'وقيت محلي.',
    'Comm_Time' : 'وقت المحادثة',
    'expired' : 'نتهت صلاحية نظام تحديد المواقع !',
    'Odo' : "أودو",
    'Ignition' : "اشتعال",
    'Veh_Battery' : 'طارية السيارة',
    'Dev_Battery' : 'طارية التطوير.',
    'Covered' : 'لتغطية ب (كم).',
    'Max_Speed' : 'سرعة',
    'Speed' : "سرعة",
    'Direction' : "اتجاه",
    'kms' : 'kms',
    'Track' : "مسار",
    'Reports' : "تقارير",
    'History' : "تاريخ",
    'Site' : "موقع",
    'MultiTrack' : "متعدد المسار",
    'Position'  : "نقاط البيع",
    'Parked' : "متوقفة",
    'Moving' : "متحرك",
    'Idle'  : "عاطل",
    'No_data' : "لايوجد بيانات",
    'Vehicle' : "مركبة",
    'Asset' : "أصل",
    'Load_Details' : "تفاصيل تحميل",
    'temperature' : "درجة حرارة",
}).translations('pt', {
    'Payment_overdue_for' : 'Pagamento em atraso para',
    'Successfully updated':'Atualizado com sucesso',
    'Site Already Present':'Site já presente',
    'Enter Site Name':'Digite o nome do site',
    'Enter Valid Site Name':'Insira um nome de site válido',
    'Enter all the field / Mark the Site':'Insira todos os campos / Marque o Site',
    'Payment overdue for' : 'Pagamento em atraso para',
    'day' : 'Dia',
    'Please make payment at the earliest to avoid de-activation':'Faça o pagamento o mais rápido possível para evitar a desativação',
    'Yes':'sim',
    'No':'Não',
    'ON':'SOBRE',
    'OFF':'DESLIGADO',
    'Heavy Vehicle' : 'veículo pesado',
    'Name':'Nome',
    'Today_Distance':'Distância Hoje',
    'ACC_Status':'Status ACC',
    'Loc_Time':'Hora local',
    'Comm_Time':'Tempo de comunicação',
    'expired':'Este GPS do veículo expirou!',
    'Ignition':'Ignição',
    'Odo' : 'Odo (kms)',
    'Veh_Battery':'Bateria Veh',
    'Dev_Battery' : ' Dev Battery',
    'Covered':'Coberto (Kms)',
    'Max_Speed' : 'velocidade máxima (kms)',
    'Speed' : 'velocidade (kms)',
    'Direction':'Direção',
    'kms':'kms',
    'Track':'Acompanhar',
    'Reports':'Relatórios',
    'History':'História',
    'Site':'Local',
    'MultiTrack' : 'Multi Track',
    'Position':'Posição',
    'Parked':'Estacionado',
    'Moving':'Em movimento',
    'Idle':'Ocioso',
    'No_data':'Sem dados',
    'Vehicle':'Veículo',
    'Asset':'De ativos',
    'Load_Details':'Detalhes de carga',
    'temperature':'Temperatura',
}).translations('gu',{
    'Payment_overdue_for' : 'ચૂકવી ચૂકવણી',
    'Successfully updated' : 'સફળતાપૂર્વક અપડેટ',
    'Site Already Present' : 'સાઇટ પહેલેથી જ હાજર છે',
    'Enter Site Name' : 'સાઇટ નામ દાખલ કરો',
    'Enter Valid Site Name' : 'માન્ય સાઇટ નામ દાખલ કરો',
    'Enter all the field / Mark the Site' : 'બધા ક્ષેત્ર દાખલ કરો / સાઇટને ચિહ્નિત કરો',
    'Payment overdue for' : 'ચૂકવી ચૂકવણી',
    'day' : 'દિવસ',
    'Please make payment at the earliest to avoid de-activation' : 'ડી-એક્ટિવેશન ટાળવા માટે કૃપા કરીને વહેલી તકે ચુકવણી કરો',
    'Yes' : 'હા',
    'No' : 'કોઈ',
    'ON' : 'ચાલુ',
    'OFF' : 'બંધ',
    'Heavy Vehicle' : 'ભારે વાહન',
    'Name' : 'નામ',
    'Today_Distance' : 'આજનું અંતર',
    'ACC_Status' : 'એ.એમ.સી.',
    'Loc_Time' : 'લોક સમય',
    'Comm_Time' : 'કોમ ટાઇમ',
    'expired' : 'આ વાહન જીપીએસ સમાપ્ત થઈ ગયું છે!',
    'Odo' : 'ઓડો (કેએમએસ)',
    'Ignition' : 'પ્રભુત્વ',
    'Veh_Battery' : 'ચલચવર',
    'Dev_Battery' : 'દેવદૂત',
    'Covered' : 'આવરી લેવામાં (કે.એમ.એસ.)',
    'Max_Speed' : 'મેક્સ સ્પીડ (કેએમએસ)',
    'Speed' : 'ગતિ (કેએમએસ)',
    'Direction' : 'દિશા',
    'kms' : 'કીએમએસ',
    'Track' : 'ટ્રેક',
    'Reports' : 'અહેવાલો',
    'History' : 'ઇતિહાસ',
    'Site' : 'સ્થળ',
    'MultiTrack' : 'એક જાતની કળા',
    'Position' : 'પદ',
    'Parked' : 'ઉદાસ',
    'Moving' : 'ફરતું',
    'Idle' : 'આળસુ',
    'No_data' : 'કોઈ ડેટા નથી',
    'no_data_reason' : 'ડેટા કારણ નથી',
    'Vehicle' : 'વાહન',
    'Asset' : 'સંપત્તિ',
    'Load_Details' : 'લોડ વિગતો',
    'temperature' : 'તાપમાન'
});

$translateProvider.preferredLanguage('en');
});
apps.run(function($translate) {
    $('#lang').on('change', function() {
        var language=$(this).val();
        alert(language);
        localStorage.setItem('lang',language);
        $translate.use(language); 
    });
});


var total = 0, tankSize = 0, fuelLtr = 0, chart = null,speedSize='100%',speedCenter='100%',startangle=-90,endangle=90,sensor=1;
var map_osm, map_goog, map_change, markerSearch, osmSearch=0;
var input_value, sbox;  

!function(){"use strict";function e(e){return angular.isUndefined(e)||null===e}var t={TAB:9,ENTER:13,ESC:27,SPACE:32,LEFT:37,UP:38,RIGHT:39,DOWN:40,SHIFT:16,CTRL:17,ALT:18,PAGE_UP:33,PAGE_DOWN:34,HOME:36,END:35,BACKSPACE:8,DELETE:46,COMMAND:91,MAP:{91:"COMMAND",8:"BACKSPACE",9:"TAB",13:"ENTER",16:"SHIFT",17:"CTRL",18:"ALT",19:"PAUSEBREAK",20:"CAPSLOCK",27:"ESC",32:"SPACE",33:"PAGE_UP",34:"PAGE_DOWN",35:"END",36:"HOME",37:"LEFT",38:"UP",39:"RIGHT",40:"DOWN",43:"+",44:"PRINTSCREEN",45:"INSERT",46:"DELETE",48:"0",49:"1",50:"2",51:"3",52:"4",53:"5",54:"6",55:"7",56:"8",57:"9",59:";",61:"=",65:"A",66:"B",67:"C",68:"D",69:"E",70:"F",71:"G",72:"H",73:"I",74:"J",75:"K",76:"L",77:"M",78:"N",79:"O",80:"P",81:"Q",82:"R",83:"S",84:"T",85:"U",86:"V",87:"W",88:"X",89:"Y",90:"Z",96:"0",97:"1",98:"2",99:"3",100:"4",101:"5",102:"6",103:"7",104:"8",105:"9",106:"*",107:"+",109:"-",110:".",111:"/",112:"F1",113:"F2",114:"F3",115:"F4",116:"F5",117:"F6",118:"F7",119:"F8",120:"F9",121:"F10",122:"F11",123:"F12",144:"NUMLOCK",145:"SCROLLLOCK",186:";",187:"=",188:",",189:"-",190:".",191:"/",192:"`",219:"[",220:"\\",221:"]",222:"'"},isControl:function(e){var s=e.which;switch(s){case t.COMMAND:case t.SHIFT:case t.CTRL:case t.ALT:return!0}return!!(e.metaKey||e.ctrlKey||e.altKey)},isFunctionKey:function(e){return e=e.which?e.which:e,e>=112&&e<=123},isVerticalMovement:function(e){return~[t.UP,t.DOWN].indexOf(e)},isHorizontalMovement:function(e){return~[t.LEFT,t.RIGHT,t.BACKSPACE,t.DELETE].indexOf(e)},toSeparator:function(e){var s={ENTER:"\n",TAB:"\t",SPACE:" "}[e];return s?s:t[e]?void 0:e}};void 0===angular.element.prototype.querySelectorAll&&(angular.element.prototype.querySelectorAll=function(e){return angular.element(this[0].querySelectorAll(e))}),void 0===angular.element.prototype.closest&&(angular.element.prototype.closest=function(e){for(var t=this[0],s=t.matches||t.webkitMatchesSelector||t.mozMatchesSelector||t.msMatchesSelector;t;){if(s.bind(t)(e))return t;t=t.parentElement}return!1});var s=0,i=angular.module("ui.select",[]).constant("uiSelectConfig",{theme:"bootstrap",searchEnabled:!0,sortable:!1,placeholder:"",refreshDelay:1e3,closeOnSelect:!0,skipFocusser:!1,dropdownPosition:"auto",removeSelected:!0,resetSearchInput:!0,generateId:function(){return s++},appendToBody:!1,spinnerEnabled:!1,spinnerClass:"glyphicon glyphicon-refresh ui-select-spin",backspaceReset:!0}).service("uiSelectMinErr",function(){var e=angular.$$minErr("ui.select");return function(){var t=e.apply(this,arguments),s=t.message.replace(new RegExp("\nhttp://errors.angularjs.org/.*"),"");return new Error(s)}}).directive("uisTranscludeAppend",function(){return{link:function(e,t,s,i,c){c(e,function(e){t.append(e)})}}}).filter("highlight",function(){function e(e){return(""+e).replace(/([.?*+^$[\]\\(){}|-])/g,"\\$1")}return function(t,s){return s&&t?(""+t).replace(new RegExp(e(s),"gi"),'<span class="ui-select-highlight">$&</span>'):t}}).factory("uisOffset",["$document","$window",function(e,t){return function(s){var i=s[0].getBoundingClientRect();return{width:i.width||s.prop("offsetWidth"),height:i.height||s.prop("offsetHeight"),top:i.top+(t.pageYOffset||e[0].documentElement.scrollTop),left:i.left+(t.pageXOffset||e[0].documentElement.scrollLeft)}}}]);i.directive("uiSelectChoices",["uiSelectConfig","uisRepeatParser","uiSelectMinErr","$compile","$window",function(e,t,s,i,c){return{restrict:"EA",require:"^uiSelect",replace:!0,transclude:!0,templateUrl:function(t){t.addClass("ui-select-choices");var s=t.parent().attr("theme")||e.theme;return s+"/choices.tpl.html"},compile:function(i,n){if(!n.repeat)throw s("repeat","Expected 'repeat' expression.");var l=n.groupBy,a=n.groupFilter;if(l){var r=i.querySelectorAll(".ui-select-choices-group");if(1!==r.length)throw s("rows","Expected 1 .ui-select-choices-group but got '{0}'.",r.length);r.attr("ng-repeat",t.getGroupNgRepeatExpression())}var o=t.parse(n.repeat),u=i.querySelectorAll(".ui-select-choices-row");if(1!==u.length)throw s("rows","Expected 1 .ui-select-choices-row but got '{0}'.",u.length);u.attr("ng-repeat",o.repeatExpression(l)).attr("ng-if","$select.open");var d=i.querySelectorAll(".ui-select-choices-row-inner");if(1!==d.length)throw s("rows","Expected 1 .ui-select-choices-row-inner but got '{0}'.",d.length);d.attr("uis-transclude-append","");var p=c.document.addEventListener?u:d;return p.attr("ng-click","$select.select("+o.itemName+",$select.skipFocusser,$event)"),function(t,s,c,n){n.parseRepeatAttr(c.repeat,l,a),n.disableChoiceExpression=c.uiDisableChoice,n.onHighlightCallback=c.onHighlight,n.minimumInputLength=parseInt(c.minimumInputLength)||0,n.dropdownPosition=c.position?c.position.toLowerCase():e.dropdownPosition,t.$watch("$select.search",function(e){e&&!n.open&&n.multiple&&n.activate(!1,!0),n.activeIndex=n.tagging.isActivated?-1:0,!c.minimumInputLength||n.search.length>=c.minimumInputLength?n.refresh(c.refresh):n.items=[]}),c.$observe("refreshDelay",function(){var s=t.$eval(c.refreshDelay);n.refreshDelay=void 0!==s?s:e.refreshDelay}),t.$watch("$select.open",function(e){e?(i.attr("role","listbox"),n.refresh(c.refresh)):s.removeAttr("role")})}}}}]),i.controller("uiSelectCtrl",["$scope","$element","$timeout","$filter","$$uisDebounce","uisRepeatParser","uiSelectMinErr","uiSelectConfig","$parse","$injector","$window",function(s,i,c,n,l,a,r,o,u,d,p){function h(e,t,s){if(e.findIndex)return e.findIndex(t,s);for(var i,c=Object(e),n=c.length>>>0,l=0;l<n;l++)if(i=c[l],t.call(s,i,l,c))return l;return-1}function g(){y.resetSearchInput&&(y.search=x,y.selected&&y.items.length&&!y.multiple&&(y.activeIndex=h(y.items,function(e){return angular.equals(this,e)},y.selected)))}function f(e,t){var s,i,c=[];for(s=0;s<t.length;s++)for(i=0;i<e.length;i++)e[i].name==[t[s]]&&c.push(e[i]);return c}function v(e,t){var s=I.indexOf(e);t&&s===-1&&I.push(e),!t&&s>-1&&I.splice(s,1)}function m(e){return I.indexOf(e)>-1}function $(e){function t(e,t){var s=i.indexOf(e);t&&s===-1&&i.push(e),!t&&s>-1&&i.splice(s,1)}function s(e){return i.indexOf(e)>-1}if(e){var i=[];y.isLocked=function(e,i){var c=!1,n=y.selected[i];return n&&(e?(c=!!e.$eval(y.lockChoiceExpression),t(n,c)):c=s(n)),c}}}function b(e){var s=!0;switch(e){case t.DOWN:if(!y.open&&y.multiple)y.activate(!1,!0);else if(y.activeIndex<y.items.length-1)for(var i=++y.activeIndex;m(y.items[i])&&i<y.items.length;)y.activeIndex=++i;break;case t.UP:var c=0===y.search.length&&y.tagging.isActivated?-1:0;if(!y.open&&y.multiple)y.activate(!1,!0);else if(y.activeIndex>c)for(var n=--y.activeIndex;m(y.items[n])&&n>c;)y.activeIndex=--n;break;case t.TAB:y.multiple&&!y.open||y.select(y.items[y.activeIndex],!0);break;case t.ENTER:y.open&&(y.tagging.isActivated||y.activeIndex>=0)?y.select(y.items[y.activeIndex],y.skipFocusser):y.activate(!1,!0);break;case t.ESC:y.close();break;default:s=!1}return s}function w(){var e=i.querySelectorAll(".ui-select-choices-content"),t=e.querySelectorAll(".ui-select-choices-row");if(t.length<1)throw r("choices","Expected multiple .ui-select-choices-row but got '{0}'.",t.length);if(!(y.activeIndex<0)){var s=t[y.activeIndex],c=s.offsetTop+s.clientHeight-e[0].scrollTop,n=e[0].offsetHeight;c>n?e[0].scrollTop+=c-n:c<s.clientHeight&&(y.isGrouped&&0===y.activeIndex?e[0].scrollTop=0:e[0].scrollTop-=s.clientHeight-c)}}var y=this,x="";if(y.placeholder=o.placeholder,y.searchEnabled=o.searchEnabled,y.sortable=o.sortable,y.refreshDelay=o.refreshDelay,y.paste=o.paste,y.resetSearchInput=o.resetSearchInput,y.refreshing=!1,y.spinnerEnabled=o.spinnerEnabled,y.spinnerClass=o.spinnerClass,y.removeSelected=o.removeSelected,y.closeOnSelect=!0,y.skipFocusser=!1,y.search=x,y.activeIndex=0,y.items=[],y.open=!1,y.focus=!1,y.disabled=!1,y.selected=void 0,y.dropdownPosition="auto",y.focusser=void 0,y.multiple=void 0,y.disableChoiceExpression=void 0,y.tagging={isActivated:!1,fct:void 0},y.taggingTokens={isActivated:!1,tokens:void 0},y.lockChoiceExpression=void 0,y.clickTriggeredSelect=!1,y.$filter=n,y.$element=i,y.$animate=function(){try{return d.get("$animate")}catch(e){return null}}(),y.searchInput=i.querySelectorAll("input.ui-select-search"),1!==y.searchInput.length)throw r("searchInput","Expected 1 input.ui-select-search but got '{0}'.",y.searchInput.length);y.isEmpty=function(){return e(y.selected)||""===y.selected||y.multiple&&0===y.selected.length},y.activate=function(e,t){if(y.disabled||y.open)y.open&&!y.searchEnabled&&y.close();else{t||g(),s.$broadcast("uis:activate"),y.open=!0,y.activeIndex=y.activeIndex>=y.items.length?0:y.activeIndex,y.activeIndex===-1&&y.taggingLabel!==!1&&(y.activeIndex=0);var n=i.querySelectorAll(".ui-select-choices-content"),l=i.querySelectorAll(".ui-select-search");if(y.$animate&&y.$animate.on&&y.$animate.enabled(n[0])){var a=function(t,s){"start"===s&&0===y.items.length?(y.$animate.off("removeClass",l[0],a),c(function(){y.focusSearchInput(e)})):"close"===s&&(y.$animate.off("enter",n[0],a),c(function(){y.focusSearchInput(e)}))};y.items.length>0?y.$animate.on("enter",n[0],a):y.$animate.on("removeClass",l[0],a)}else c(function(){y.focusSearchInput(e),!y.tagging.isActivated&&y.items.length>1&&w()})}},y.focusSearchInput=function(e){y.search=e||y.search,y.searchInput[0].focus()},y.findGroupByName=function(e){return y.groups&&y.groups.filter(function(t){return t.name===e})[0]},y.parseRepeatAttr=function(e,t,i){function c(e){var c=s.$eval(t);if(y.groups=[],angular.forEach(e,function(e){var t=angular.isFunction(c)?c(e):e[c],s=y.findGroupByName(t);s?s.items.push(e):y.groups.push({name:t,items:[e]})}),i){var n=s.$eval(i);angular.isFunction(n)?y.groups=n(y.groups):angular.isArray(n)&&(y.groups=f(y.groups,n))}y.items=[],y.groups.forEach(function(e){y.items=y.items.concat(e.items)})}function n(e){y.items=e||[]}y.setItemsFn=t?c:n,y.parserResult=a.parse(e),y.isGrouped=!!t,y.itemProperty=y.parserResult.itemName;var l=y.parserResult.source,o=function(){var e=l(s);s.$uisSource=Object.keys(e).map(function(t){var s={};return s[y.parserResult.keyName]=t,s.value=e[t],s})};y.parserResult.keyName&&(o(),y.parserResult.source=u("$uisSource"+y.parserResult.filters),s.$watch(l,function(e,t){e!==t&&o()},!0)),y.refreshItems=function(e){e=e||y.parserResult.source(s);var t=y.selected;if(y.isEmpty()||angular.isArray(t)&&!t.length||!y.multiple||!y.removeSelected)y.setItemsFn(e);else if(void 0!==e&&null!==e){var i=e.filter(function(e){return angular.isArray(t)?t.every(function(t){return!angular.equals(e,t)}):!angular.equals(e,t)});y.setItemsFn(i)}"auto"!==y.dropdownPosition&&"up"!==y.dropdownPosition||s.calculateDropdownPos(),s.$broadcast("uis:refresh")},s.$watchCollection(y.parserResult.source,function(e){if(void 0===e||null===e)y.items=[];else{if(!angular.isArray(e))throw r("items","Expected an array but got '{0}'.",e);y.refreshItems(e),angular.isDefined(y.ngModel.$modelValue)&&(y.ngModel.$modelValue=null)}})};var E;y.refresh=function(e){void 0!==e&&(E&&c.cancel(E),E=c(function(){if(s.$select.search.length>=s.$select.minimumInputLength){var t=s.$eval(e);t&&angular.isFunction(t.then)&&!y.refreshing&&(y.refreshing=!0,t["finally"](function(){y.refreshing=!1}))}},y.refreshDelay))},y.isActive=function(e){if(!y.open)return!1;var t=y.items.indexOf(e[y.itemProperty]),s=t==y.activeIndex;return!(!s||t<0)&&(s&&!angular.isUndefined(y.onHighlightCallback)&&e.$eval(y.onHighlightCallback),s)};var S=function(e){return y.selected&&angular.isArray(y.selected)&&y.selected.filter(function(t){return angular.equals(t,e)}).length>0},I=[];y.isDisabled=function(e){if(y.open){var t=e[y.itemProperty],s=y.items.indexOf(t),i=!1;if(s>=0&&(angular.isDefined(y.disableChoiceExpression)||y.multiple)){if(t.isTag)return!1;y.multiple&&(i=S(t)),!i&&angular.isDefined(y.disableChoiceExpression)&&(i=!!e.$eval(y.disableChoiceExpression)),v(t,i)}return i}},y.select=function(t,i,c){if(e(t)||!m(t)){if(!y.items&&!y.search&&!y.tagging.isActivated)return;if(!t||!m(t)){if(y.clickTriggeredSelect=!1,c&&("click"===c.type||"touchend"===c.type)&&t&&(y.clickTriggeredSelect=!0),y.tagging.isActivated&&y.clickTriggeredSelect===!1){if(y.taggingLabel===!1)if(y.activeIndex<0){if(void 0===t&&(t=void 0!==y.tagging.fct?y.tagging.fct(y.search):y.search),!t||angular.equals(y.items[0],t))return}else t=y.items[y.activeIndex];else if(0===y.activeIndex){if(void 0===t)return;if(void 0!==y.tagging.fct&&"string"==typeof t){if(t=y.tagging.fct(t),!t)return}else"string"==typeof t&&(t=t.replace(y.taggingLabel,"").trim())}if(S(t))return void y.close(i)}g(),s.$broadcast("uis:select",t),y.closeOnSelect&&y.close(i)}}},y.close=function(e){y.open&&(y.ngModel&&y.ngModel.$setTouched&&y.ngModel.$setTouched(),y.open=!1,g(),s.$broadcast("uis:close",e))},y.setFocus=function(){y.focus||y.focusInput[0].focus()},y.clear=function(e){y.select(null),e.stopPropagation(),c(function(){y.focusser[0].focus()},0,!1)},y.toggle=function(e){y.open?(y.close(),e.preventDefault(),e.stopPropagation()):y.activate()},y.isLocked=function(){return!1},s.$watch(function(){return angular.isDefined(y.lockChoiceExpression)&&""!==y.lockChoiceExpression},$);var C=null,k=!1;y.sizeSearchInput=function(){var e=y.searchInput[0],t=y.$element[0],i=function(){return t.clientWidth*!!e.offsetParent},n=function(t){if(0===t)return!1;var s=t-e.offsetLeft;return s<50&&(s=t),y.searchInput.css("width",s+"px"),!0};y.searchInput.css("width","10px"),c(function(){null!==C||n(i())||(C=s.$watch(function(){k||(k=!0,s.$$postDigest(function(){k=!1,n(i())&&(C(),C=null)}))},angular.noop))})},y.searchInput.on("keydown",function(e){var i=e.which;~[t.ENTER,t.ESC].indexOf(i)&&(e.preventDefault(),e.stopPropagation()),s.$apply(function(){var s=!1;if((y.items.length>0||y.tagging.isActivated)&&(b(i)||y.searchEnabled||(e.preventDefault(),e.stopPropagation()),y.taggingTokens.isActivated)){for(var n=0;n<y.taggingTokens.tokens.length;n++)y.taggingTokens.tokens[n]===t.MAP[e.keyCode]&&y.search.length>0&&(s=!0);s&&c(function(){y.searchInput.triggerHandler("tagged");var s=y.search.replace(t.MAP[e.keyCode],"").trim();y.tagging.fct&&(s=y.tagging.fct(s)),s&&y.select(s,!0)})}}),t.isVerticalMovement(i)&&y.items.length>0&&w(),i!==t.ENTER&&i!==t.ESC||(e.preventDefault(),e.stopPropagation())}),y.searchInput.on("paste",function(e){var s;if(s=window.clipboardData&&window.clipboardData.getData?window.clipboardData.getData("Text"):(e.originalEvent||e).clipboardData.getData("text/plain"),s=y.search+s,s&&s.length>0)if(y.taggingTokens.isActivated){for(var i=[],c=0;c<y.taggingTokens.tokens.length;c++){var n=t.toSeparator(y.taggingTokens.tokens[c])||y.taggingTokens.tokens[c];if(s.indexOf(n)>-1){i=s.split(n);break}}0===i.length&&(i=[s]);var l=y.search;angular.forEach(i,function(e){var t=y.tagging.fct?y.tagging.fct(e):e;t&&y.select(t,!0)}),y.search=l||x,e.preventDefault(),e.stopPropagation()}else y.paste&&(y.paste(s),y.search=x,e.preventDefault(),e.stopPropagation())}),y.searchInput.on("tagged",function(){c(function(){g()})});var A=l(function(){y.sizeSearchInput()},50);angular.element(p).bind("resize",A),s.$on("$destroy",function(){y.searchInput.off("keyup keydown tagged blur paste"),angular.element(p).off("resize",A)}),s.$watch("$select.activeIndex",function(e){e&&i.find("input").attr("aria-activedescendant","ui-select-choices-row-"+y.generatedId+"-"+e)}),s.$watch("$select.open",function(e){e||i.find("input").removeAttr("aria-activedescendant")})}]),i.directive("uiSelect",["$document","uiSelectConfig","uiSelectMinErr","uisOffset","$compile","$parse","$timeout",function(e,t,s,i,c,n,l){return{restrict:"EA",templateUrl:function(e,s){var i=s.theme||t.theme;return i+(angular.isDefined(s.multiple)?"/select-multiple.tpl.html":"/select.tpl.html")},replace:!0,transclude:!0,require:["uiSelect","^ngModel"],scope:!0,controller:"uiSelectCtrl",controllerAs:"$select",compile:function(c,a){var r=/{(.*)}\s*{(.*)}/.exec(a.ngClass);if(r){var o="{"+r[1]+", "+r[2]+"}";a.ngClass=o,c.attr("ng-class",o)}return angular.isDefined(a.multiple)?c.append("<ui-select-multiple/>").removeAttr("multiple"):c.append("<ui-select-single/>"),a.inputId&&(c.querySelectorAll("input.ui-select-search")[0].id=a.inputId),function(c,a,r,o,u){function d(e){if(g.open){var t=!1;if(t=window.jQuery?window.jQuery.contains(a[0],e.target):a[0].contains(e.target),!t&&!g.clickTriggeredSelect){var s;if(g.skipFocusser)s=!0;else{var i=["input","button","textarea","select"],n=angular.element(e.target).controller("uiSelect");s=n&&n!==g,s||(s=~i.indexOf(e.target.tagName.toLowerCase()))}g.close(s),c.$digest()}g.clickTriggeredSelect=!1}}function p(){var t=i(a);m=angular.element('<div class="ui-select-placeholder"></div>'),m[0].style.width=t.width+"px",m[0].style.height=t.height+"px",a.after(m),$=a[0].style.width,e.find("body").append(a),a[0].style.position="absolute",a[0].style.left=t.left+"px",a[0].style.top=t.top+"px",a[0].style.width=t.width+"px"}function h(){null!==m&&(m.replaceWith(a),m=null,a[0].style.position="",a[0].style.left="",a[0].style.top="",a[0].style.width=$,g.setFocus())}var g=o[0],f=o[1];g.generatedId=t.generateId(),g.baseTitle=r.title||"Select box",g.focusserTitle=g.baseTitle+" focus",g.focusserId="focusser-"+g.generatedId,g.closeOnSelect=function(){return angular.isDefined(r.closeOnSelect)?n(r.closeOnSelect)():t.closeOnSelect}(),c.$watch("skipFocusser",function(){var e=c.$eval(r.skipFocusser);g.skipFocusser=void 0!==e?e:t.skipFocusser}),g.onSelectCallback=n(r.onSelect),g.onRemoveCallback=n(r.onRemove),g.ngModel=f,g.choiceGrouped=function(e){return g.isGrouped&&e&&e.name},r.tabindex&&r.$observe("tabindex",function(e){g.focusInput.attr("tabindex",e),a.removeAttr("tabindex")}),c.$watch(function(){return c.$eval(r.searchEnabled)},function(e){g.searchEnabled=void 0!==e?e:t.searchEnabled}),c.$watch("sortable",function(){var e=c.$eval(r.sortable);g.sortable=void 0!==e?e:t.sortable}),r.$observe("backspaceReset",function(){var e=c.$eval(r.backspaceReset);g.backspaceReset=void 0===e||e}),r.$observe("limit",function(){g.limit=angular.isDefined(r.limit)?parseInt(r.limit,10):void 0}),c.$watch("removeSelected",function(){var e=c.$eval(r.removeSelected);g.removeSelected=void 0!==e?e:t.removeSelected}),r.$observe("disabled",function(){g.disabled=void 0!==r.disabled&&r.disabled}),r.$observe("resetSearchInput",function(){var e=c.$eval(r.resetSearchInput);g.resetSearchInput=void 0===e||e}),r.$observe("paste",function(){g.paste=c.$eval(r.paste)}),r.$observe("tagging",function(){if(void 0!==r.tagging){var e=c.$eval(r.tagging);g.tagging={isActivated:!0,fct:e!==!0?e:void 0}}else g.tagging={isActivated:!1,fct:void 0}}),r.$observe("taggingLabel",function(){void 0!==r.tagging&&("false"===r.taggingLabel?g.taggingLabel=!1:g.taggingLabel=void 0!==r.taggingLabel?r.taggingLabel:"(new)")}),r.$observe("taggingTokens",function(){if(void 0!==r.tagging){var e=void 0!==r.taggingTokens?r.taggingTokens.split("|"):[",","ENTER"];g.taggingTokens={isActivated:!0,tokens:e}}}),r.$observe("spinnerEnabled",function(){var e=c.$eval(r.spinnerEnabled);g.spinnerEnabled=void 0!==e?e:t.spinnerEnabled}),r.$observe("spinnerClass",function(){var e=r.spinnerClass;g.spinnerClass=void 0!==e?r.spinnerClass:t.spinnerClass}),angular.isDefined(r.autofocus)&&l(function(){g.setFocus()}),angular.isDefined(r.focusOn)&&c.$on(r.focusOn,function(){l(function(){g.setFocus()})}),e.on("click",d),c.$on("$destroy",function(){e.off("click",d)}),u(c,function(e){var t=angular.element("<div>").append(e),i=t.querySelectorAll(".ui-select-match");if(i.removeAttr("ui-select-match"),i.removeAttr("data-ui-select-match"),1!==i.length)throw s("transcluded","Expected 1 .ui-select-match but got '{0}'.",i.length);a.querySelectorAll(".ui-select-match").replaceWith(i);var c=t.querySelectorAll(".ui-select-choices");if(c.removeAttr("ui-select-choices"),c.removeAttr("data-ui-select-choices"),1!==c.length)throw s("transcluded","Expected 1 .ui-select-choices but got '{0}'.",c.length);a.querySelectorAll(".ui-select-choices").replaceWith(c);var n=t.querySelectorAll(".ui-select-no-choice");n.removeAttr("ui-select-no-choice"),n.removeAttr("data-ui-select-no-choice"),1==n.length&&a.querySelectorAll(".ui-select-no-choice").replaceWith(n)});var v=c.$eval(r.appendToBody);(void 0!==v?v:t.appendToBody)&&(c.$watch("$select.open",function(e){e?p():h()}),c.$on("$destroy",function(){h()}));var m=null,$="",b=null,w="direction-up";c.$watch("$select.open",function(){"auto"!==g.dropdownPosition&&"up"!==g.dropdownPosition||c.calculateDropdownPos()});var y=function(e,t){e=e||i(a),t=t||i(b),b[0].style.position="absolute",b[0].style.top=t.height*-1+"px",a.addClass(w)},x=function(e,t){a.removeClass(w),e=e||i(a),t=t||i(b),b[0].style.position="",b[0].style.top=""},E=function(){l(function(){if("up"===g.dropdownPosition)y();else{a.removeClass(w);var t=i(a),s=i(b),c=e[0].documentElement.scrollTop||e[0].body.scrollTop;t.top+t.height+s.height>c+e[0].documentElement.clientHeight?y(t,s):x(t,s)}b[0].style.opacity=1})},S=!1;c.calculateDropdownPos=function(){if(g.open){if(b=angular.element(a).querySelectorAll(".ui-select-dropdown"),0===b.length)return;if(""!==g.search||S||(b[0].style.opacity=0,S=!0),!i(b).height&&g.$animate&&g.$animate.on&&g.$animate.enabled(b)){var e=!0;g.$animate.on("enter",b,function(t,s){"close"===s&&e&&(E(),e=!1)})}else E()}else{if(null===b||0===b.length)return;b[0].style.opacity=0,b[0].style.position="",b[0].style.top="",a.removeClass(w)}}}}}}]),i.directive("uiSelectMatch",["uiSelectConfig",function(e){function t(e,t){return e[0].hasAttribute(t)?e.attr(t):e[0].hasAttribute("data-"+t)?e.attr("data-"+t):e[0].hasAttribute("x-"+t)?e.attr("x-"+t):void 0}return{restrict:"EA",require:"^uiSelect",replace:!0,transclude:!0,templateUrl:function(s){s.addClass("ui-select-match");var i=s.parent(),c=t(i,"theme")||e.theme,n=angular.isDefined(t(i,"multiple"));return c+(n?"/match-multiple.tpl.html":"/match.tpl.html")},link:function(t,s,i,c){function n(e){c.allowClear=!!angular.isDefined(e)&&(""===e||"true"===e.toLowerCase())}c.lockChoiceExpression=i.uiLockChoice,i.$observe("placeholder",function(t){c.placeholder=void 0!==t?t:e.placeholder}),i.$observe("allowClear",n),n(i.allowClear),c.multiple&&c.sizeSearchInput()}}}]),i.directive("uiSelectMultiple",["uiSelectMinErr","$timeout",function(s,i){return{restrict:"EA",require:["^uiSelect","^ngModel"],controller:["$scope","$timeout",function(e,t){var s,i=this,c=e.$select;angular.isUndefined(c.selected)&&(c.selected=[]),e.$evalAsync(function(){s=e.ngModel}),i.activeMatchIndex=-1,i.updateModel=function(){s.$setViewValue(Date.now()),i.refreshComponent()},i.refreshComponent=function(){c.refreshItems&&c.refreshItems(),c.sizeSearchInput&&c.sizeSearchInput()},i.removeChoice=function(s){if(c.isLocked(null,s))return!1;var n=c.selected[s],l={};return l[c.parserResult.itemName]=n,c.selected.splice(s,1),i.activeMatchIndex=-1,c.sizeSearchInput(),t(function(){c.onRemoveCallback(e,{$item:n,$model:c.parserResult.modelMapper(e,l)})}),i.updateModel(),!0},i.getPlaceholder=function(){if(!c.selected||!c.selected.length)return c.placeholder}}],controllerAs:"$selectMultiple",link:function(c,n,l,a){function r(e){return angular.isNumber(e.selectionStart)?e.selectionStart:e.value.length}function o(e){function s(){switch(e){case t.LEFT:return~g.activeMatchIndex?u:l;case t.RIGHT:return~g.activeMatchIndex&&a!==l?o:(p.activate(),!1);case t.BACKSPACE:return~g.activeMatchIndex?g.removeChoice(a)?u:a:l;case t.DELETE:return!!~g.activeMatchIndex&&(g.removeChoice(g.activeMatchIndex),a)}}var i=r(p.searchInput[0]),c=p.selected.length,n=0,l=c-1,a=g.activeMatchIndex,o=g.activeMatchIndex+1,u=g.activeMatchIndex-1,d=a;return!(i>0||p.search.length&&e==t.RIGHT)&&(p.close(),d=s(),p.selected.length&&d!==!1?g.activeMatchIndex=Math.min(l,Math.max(n,d)):g.activeMatchIndex=-1,!0)}function u(e){if(void 0===e||void 0===p.search)return!1;var t=e.filter(function(e){return void 0!==p.search.toUpperCase()&&void 0!==e&&e.toUpperCase()===p.search.toUpperCase()}).length>0;return t}function d(e,t){var s=-1;if(angular.isArray(e))for(var i=angular.copy(e),c=0;c<i.length;c++)if(void 0===p.tagging.fct)i[c]+" "+p.taggingLabel===t&&(s=c);else{var n=i[c];angular.isObject(n)&&(n.isTag=!0),angular.equals(n,t)&&(s=c)}return s}var p=a[0],h=c.ngModel=a[1],g=c.$selectMultiple;p.multiple=!0,p.focusInput=p.searchInput,h.$isEmpty=function(e){return!e||0===e.length},h.$parsers.unshift(function(){for(var e,t={},s=[],i=p.selected.length-1;i>=0;i--)t={},t[p.parserResult.itemName]=p.selected[i],e=p.parserResult.modelMapper(c,t),s.unshift(e);return s}),h.$formatters.unshift(function(e){var t,s=p.parserResult&&p.parserResult.source(c,{$select:{search:""}}),i={};if(!s)return e;var n=[],l=function(e,s){if(e&&e.length){for(var l=e.length-1;l>=0;l--){if(i[p.parserResult.itemName]=e[l],t=p.parserResult.modelMapper(c,i),p.parserResult.trackByExp){var a=/(\w*)\./.exec(p.parserResult.trackByExp),r=/\.([^\s]+)/.exec(p.parserResult.trackByExp);if(a&&a.length>0&&a[1]==p.parserResult.itemName&&r&&r.length>0&&t[r[1]]==s[r[1]])return n.unshift(e[l]),!0}if(angular.equals(t,s))return n.unshift(e[l]),!0}return!1}};if(!e)return n;for(var a=e.length-1;a>=0;a--)l(p.selected,e[a])||l(s,e[a])||n.unshift(e[a]);return n}),c.$watchCollection(function(){return h.$modelValue},function(e,t){t!=e&&(angular.isDefined(h.$modelValue)&&(h.$modelValue=null),g.refreshComponent())}),h.$render=function(){if(!angular.isArray(h.$viewValue)){if(!e(h.$viewValue))throw s("multiarr","Expected model value to be array but got '{0}'",h.$viewValue);h.$viewValue=[]}p.selected=h.$viewValue,g.refreshComponent(),c.$evalAsync()},c.$on("uis:select",function(e,t){if(!(p.selected.length>=p.limit)){p.selected.push(t);var s={};s[p.parserResult.itemName]=t,i(function(){p.onSelectCallback(c,{$item:t,$model:p.parserResult.modelMapper(c,s)})}),g.updateModel()}}),c.$on("uis:activate",function(){g.activeMatchIndex=-1}),c.$watch("$select.disabled",function(e,t){t&&!e&&p.sizeSearchInput()}),p.searchInput.on("keydown",function(e){var s=e.which;c.$apply(function(){var i=!1;t.isHorizontalMovement(s)&&(i=o(s)),i&&s!=t.TAB&&(e.preventDefault(),e.stopPropagation())})}),p.searchInput.on("keyup",function(e){if(t.isVerticalMovement(e.which)||c.$evalAsync(function(){p.activeIndex=p.taggingLabel===!1?-1:0}),p.tagging.isActivated&&p.search.length>0){if(e.which===t.TAB||t.isControl(e)||t.isFunctionKey(e)||e.which===t.ESC||t.isVerticalMovement(e.which))return;if(p.activeIndex=p.taggingLabel===!1?-1:0,p.taggingLabel===!1)return;var s,i,n,l,a=angular.copy(p.items),r=angular.copy(p.items),o=!1,h=-1;if(void 0!==p.tagging.fct){if(n=p.$filter("filter")(a,{isTag:!0}),n.length>0&&(l=n[0]),a.length>0&&l&&(o=!0,a=a.slice(1,a.length),r=r.slice(1,r.length)),s=p.tagging.fct(p.search),r.some(function(e){return angular.equals(e,s)})||p.selected.some(function(e){return angular.equals(e,s)}))return void c.$evalAsync(function(){p.activeIndex=0,p.items=a});s&&(s.isTag=!0)}else{if(n=p.$filter("filter")(a,function(e){return e.match(p.taggingLabel)}),n.length>0&&(l=n[0]),i=a[0],void 0!==i&&a.length>0&&l&&(o=!0,a=a.slice(1,a.length),r=r.slice(1,r.length)),s=p.search+" "+p.taggingLabel,d(p.selected,p.search)>-1)return;if(u(r.concat(p.selected)))return void(o&&(a=r,c.$evalAsync(function(){p.activeIndex=0,p.items=a})));if(u(r))return void(o&&(p.items=r.slice(1,r.length)))}o&&(h=d(p.selected,s)),h>-1?a=a.slice(h+1,a.length-1):(a=[],s&&a.push(s),a=a.concat(r)),c.$evalAsync(function(){if(p.activeIndex=0,p.items=a,p.isGrouped){var e=s?a.slice(1):a;p.setItemsFn(e),s&&(p.items.unshift(s),p.groups.unshift({name:"",items:[s],tagging:!0}))}})}}),p.searchInput.on("blur",function(){i(function(){g.activeMatchIndex=-1})})}}}]),i.directive("uiSelectNoChoice",["uiSelectConfig",function(e){return{restrict:"EA",require:"^uiSelect",replace:!0,transclude:!0,templateUrl:function(t){t.addClass("ui-select-no-choice");var s=t.parent().attr("theme")||e.theme;return s+"/no-choice.tpl.html"}}}]),i.directive("uiSelectSingle",["$timeout","$compile",function(s,i){return{restrict:"EA",require:["^uiSelect","^ngModel"],link:function(c,n,l,a){var r=a[0],o=a[1];o.$parsers.unshift(function(t){if(e(t))return t;var s,i={};return i[r.parserResult.itemName]=t,s=r.parserResult.modelMapper(c,i)}),o.$formatters.unshift(function(t){if(e(t))return t;var s,i=r.parserResult&&r.parserResult.source(c,{$select:{search:""}}),n={};if(i){var l=function(e){return n[r.parserResult.itemName]=e,s=r.parserResult.modelMapper(c,n),s===t};if(r.selected&&l(r.selected))return r.selected;for(var a=i.length-1;a>=0;a--)if(l(i[a]))return i[a]}return t}),c.$watch("$select.selected",function(e){o.$viewValue!==e&&o.$setViewValue(e)}),o.$render=function(){r.selected=o.$viewValue},c.$on("uis:select",function(t,i){r.selected=i;var n={};n[r.parserResult.itemName]=i,s(function(){r.onSelectCallback(c,{$item:i,$model:e(i)?i:r.parserResult.modelMapper(c,n)})})}),c.$on("uis:close",function(e,t){s(function(){r.focusser.prop("disabled",!1),t||r.focusser[0].focus()},0,!1)}),c.$on("uis:activate",function(){u.prop("disabled",!0)});var u=angular.element("<input ng-disabled='$select.disabled' class='ui-select-focusser ui-select-offscreen' type='text' id='{{ $select.focusserId }}' aria-label='{{ $select.focusserTitle }}' aria-haspopup='true' role='button' />");i(u)(c),r.focusser=u,r.focusInput=u,n.parent().append(u),u.bind("focus",function(){c.$evalAsync(function(){r.focus=!0})}),u.bind("blur",function(){c.$evalAsync(function(){r.focus=!1})}),u.bind("keydown",function(e){return e.which===t.BACKSPACE&&r.backspaceReset!==!1?(e.preventDefault(),e.stopPropagation(),r.select(void 0),void c.$apply()):void(e.which===t.TAB||t.isControl(e)||t.isFunctionKey(e)||e.which===t.ESC||(e.which!=t.DOWN&&e.which!=t.UP&&e.which!=t.ENTER&&e.which!=t.SPACE||(e.preventDefault(),e.stopPropagation(),r.activate()),c.$digest()))}),u.bind("keyup input",function(e){e.which===t.TAB||t.isControl(e)||t.isFunctionKey(e)||e.which===t.ESC||e.which==t.ENTER||e.which===t.BACKSPACE||(r.activate(u.val()),u.val(""),c.$digest())})}}}]),i.directive("uiSelectSort",["$timeout","uiSelectConfig","uiSelectMinErr",function(e,t,s){return{require:["^^uiSelect","^ngModel"],link:function(t,i,c,n){if(null===t[c.uiSelectSort])throw s("sort","Expected a list to sort");var l=n[0],a=n[1],r=angular.extend({axis:"horizontal"},t.$eval(c.uiSelectSortOptions)),o=r.axis,u="dragging",d="dropping",p="dropping-before",h="dropping-after";t.$watch(function(){return l.sortable},function(e){e?i.attr("draggable",!0):i.removeAttr("draggable")}),i.on("dragstart",function(e){i.addClass(u),(e.dataTransfer||e.originalEvent.dataTransfer).setData("text",t.$index.toString())}),i.on("dragend",function(){v(u)});var g,f=function(e,t){this.splice(t,0,this.splice(e,1)[0])},v=function(e){angular.forEach(l.$element.querySelectorAll("."+e),function(t){angular.element(t).removeClass(e)})},m=function(e){e.preventDefault();var t="vertical"===o?e.offsetY||e.layerY||(e.originalEvent?e.originalEvent.offsetY:0):e.offsetX||e.layerX||(e.originalEvent?e.originalEvent.offsetX:0);t<this["vertical"===o?"offsetHeight":"offsetWidth"]/2?(v(h),i.addClass(p)):(v(p),i.addClass(h))},$=function(t){t.preventDefault();var s=parseInt((t.dataTransfer||t.originalEvent.dataTransfer).getData("text"),10);e.cancel(g),g=e(function(){b(s)},20)},b=function(e){var s=t.$eval(c.uiSelectSort),n=s[e],l=null;l=i.hasClass(p)?e<t.$index?t.$index-1:t.$index:e<t.$index?t.$index:t.$index+1,f.apply(s,[e,l]),a.$setViewValue(Date.now()),t.$apply(function(){t.$emit("uiSelectSort:change",{array:s,item:n,from:e,to:l})}),v(d),v(p),v(h),i.off("drop",$)};i.on("dragenter",function(){i.hasClass(u)||(i.addClass(d),i.on("dragover",m),i.on("drop",$))}),i.on("dragleave",function(e){e.target==i&&(v(d),v(p),v(h),i.off("dragover",m),i.off("drop",$))})}}}]),i.factory("$$uisDebounce",["$timeout",function(e){return function(t,s){var i;return function(){var c=this,n=Array.prototype.slice.call(arguments);i&&e.cancel(i),i=e(function(){t.apply(c,n)},s)}}}]),i.directive("uisOpenClose",["$parse","$timeout",function(e,t){return{restrict:"A",require:"uiSelect",link:function(s,i,c,n){n.onOpenCloseCallback=e(c.uisOpenClose),s.$watch("$select.open",function(e,i){e!==i&&t(function(){n.onOpenCloseCallback(s,{isOpen:e});
})})}}}]),i.service("uisRepeatParser",["uiSelectMinErr","$parse",function(e,t){var s=this;s.parse=function(s){var i;if(i=s.match(/^\s*(?:([\s\S]+?)\s+as\s+)?(?:([\$\w][\$\w]*)|(?:\(\s*([\$\w][\$\w]*)\s*,\s*([\$\w][\$\w]*)\s*\)))\s+in\s+(\s*[\s\S]+?)?(?:\s+track\s+by\s+([\s\S]+?))?\s*$/),!i)throw e("iexp","Expected expression in form of '_item_ in _collection_[ track by _id_]' but got '{0}'.",s);var c=i[5],n="";if(i[3]){c=i[5].replace(/(^\()|(\)$)/g,"");var l=i[5].match(/^\s*(?:[\s\S]+?)(?:[^\|]|\|\|)+([\s\S]*)\s*$/);l&&l[1].trim()&&(n=l[1],c=c.replace(n,""))}return{itemName:i[4]||i[2],keyName:i[3],source:t(c),filters:n,trackByExp:i[6],modelMapper:t(i[1]||i[4]||i[2]),repeatExpression:function(e){var t=this.itemName+" in "+(e?"$group.items":"$select.items");return this.trackByExp&&(t+=" track by "+this.trackByExp),t}}},s.getGroupNgRepeatExpression=function(){return"$group in $select.groups track by $group.name"}}])}(),angular.module("ui.select").run(["$templateCache",function(e){e.put("bootstrap/choices.tpl.html",'<ul class="ui-select-choices ui-select-choices-content ui-select-dropdown dropdown-menu" ng-show="$select.open && $select.items.length > 0"><li class="ui-select-choices-group" id="ui-select-choices-{{ $select.generatedId }}"><div class="divider" ng-show="$select.isGrouped && $index > 0"></div><div ng-show="$select.isGrouped" class="ui-select-choices-group-label dropdown-header" ng-bind="$group.name"></div><div ng-attr-id="ui-select-choices-row-{{ $select.generatedId }}-{{$index}}" class="ui-select-choices-row" ng-class="{active: $select.isActive(this), disabled: $select.isDisabled(this)}" role="option"><span class="ui-select-choices-row-inner"></span></div></li></ul>'),e.put("bootstrap/match-multiple.tpl.html",'<span class="ui-select-match"><span ng-repeat="$item in $select.selected track by $index"><span class="ui-select-match-item btn btn-default btn-xs" tabindex="-1" type="button" ng-disabled="$select.disabled" ng-click="$selectMultiple.activeMatchIndex = $index;" ng-class="{\'btn-primary\':$selectMultiple.activeMatchIndex === $index, \'select-locked\':$select.isLocked(this, $index)}" ui-select-sort="$select.selected"><span class="close ui-select-match-close" ng-hide="$select.disabled" ng-click="$selectMultiple.removeChoice($index)">&nbsp;&times;</span> <span uis-transclude-append=""></span></span></span></span>'),e.put("bootstrap/match.tpl.html",'<div class="ui-select-match" ng-hide="$select.open && $select.searchEnabled" ng-disabled="$select.disabled" ng-class="{\'btn-default-focus\':$select.focus}"><span tabindex="-1" class="btn btn-default form-control ui-select-toggle" aria-label="{{ $select.baseTitle }} activate" ng-disabled="$select.disabled" ng-click="$select.activate()" style="outline: 0;"><span ng-show="$select.isEmpty()" class="ui-select-placeholder text-muted">{{$select.placeholder}}</span> <span ng-hide="$select.isEmpty()" class="ui-select-match-text pull-left" ng-class="{\'ui-select-allow-clear\': $select.allowClear && !$select.isEmpty()}" ng-transclude=""></span> <i class="caret pull-right" ng-click="$select.toggle($event)"></i> <a ng-show="$select.allowClear && !$select.isEmpty() && ($select.disabled !== true)" aria-label="{{ $select.baseTitle }} clear" style="margin-right: 10px" ng-click="$select.clear($event)" class="btn btn-xs btn-link pull-right"><i class="glyphicon glyphicon-remove" aria-hidden="true"></i></a></span></div>'),e.put("bootstrap/no-choice.tpl.html",'<ul class="ui-select-no-choice dropdown-menu" ng-show="$select.items.length == 0"><li ng-transclude=""></li></ul>'),e.put("bootstrap/select-multiple.tpl.html",'<div class="ui-select-container ui-select-multiple ui-select-bootstrap dropdown form-control" ng-class="{open: $select.open}"><div><div class="ui-select-match"></div><input type="search" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="ui-select-search input-xs" placeholder="{{$selectMultiple.getPlaceholder()}}" ng-disabled="$select.disabled" ng-click="$select.activate()" ng-model="$select.search" role="combobox" aria-expanded="{{$select.open}}" aria-label="{{$select.baseTitle}}" ng-class="{\'spinner\': $select.refreshing}" ondrop="return false;"></div><div class="ui-select-choices"></div><div class="ui-select-no-choice"></div></div>'),e.put("bootstrap/select.tpl.html",'<div class="ui-select-container ui-select-bootstrap dropdown" ng-class="{open: $select.open}"><div class="ui-select-match"></div><span ng-show="$select.open && $select.refreshing && $select.spinnerEnabled" class="ui-select-refreshing {{$select.spinnerClass}}"></span> <input type="search" autocomplete="off" tabindex="-1" aria-expanded="true" aria-label="{{ $select.baseTitle }}" aria-owns="ui-select-choices-{{ $select.generatedId }}" class="form-control ui-select-search" ng-class="{ \'ui-select-search-hidden\' : !$select.searchEnabled }" placeholder="{{$select.placeholder}}" ng-model="$select.search" ng-show="$select.open"><div class="ui-select-choices"></div><div class="ui-select-no-choice"></div></div>'),e.put("select2/choices.tpl.html",'<ul tabindex="-1" class="ui-select-choices ui-select-choices-content select2-results"><li class="ui-select-choices-group" ng-class="{\'select2-result-with-children\': $select.choiceGrouped($group) }"><div ng-show="$select.choiceGrouped($group)" class="ui-select-choices-group-label select2-result-label" ng-bind="$group.name"></div><ul id="ui-select-choices-{{ $select.generatedId }}" ng-class="{\'select2-result-sub\': $select.choiceGrouped($group), \'select2-result-single\': !$select.choiceGrouped($group) }"><li role="option" ng-attr-id="ui-select-choices-row-{{ $select.generatedId }}-{{$index}}" class="ui-select-choices-row" ng-class="{\'select2-highlighted\': $select.isActive(this), \'select2-disabled\': $select.isDisabled(this)}"><div class="select2-result-label ui-select-choices-row-inner"></div></li></ul></li></ul>'),e.put("select2/match-multiple.tpl.html",'<span class="ui-select-match"><li class="ui-select-match-item select2-search-choice" ng-repeat="$item in $select.selected track by $index" ng-class="{\'select2-search-choice-focus\':$selectMultiple.activeMatchIndex === $index, \'select2-locked\':$select.isLocked(this, $index)}" ui-select-sort="$select.selected"><span uis-transclude-append=""></span> <a href="javascript:;" class="ui-select-match-close select2-search-choice-close" ng-click="$selectMultiple.removeChoice($index)" tabindex="-1"></a></li></span>'),e.put("select2/match.tpl.html",'<a class="select2-choice ui-select-match" ng-class="{\'select2-default\': $select.isEmpty()}" ng-click="$select.toggle($event)" aria-label="{{ $select.baseTitle }} select"><span ng-show="$select.isEmpty()" class="select2-chosen">{{$select.placeholder}}</span> <span ng-hide="$select.isEmpty()" class="select2-chosen" ng-transclude=""></span> <abbr ng-if="$select.allowClear && !$select.isEmpty()" class="select2-search-choice-close" ng-click="$select.clear($event)"></abbr> <span class="select2-arrow ui-select-toggle"><b></b></span></a>'),e.put("select2/no-choice.tpl.html",'<div class="ui-select-no-choice dropdown" ng-show="$select.items.length == 0"><div class="dropdown-content"><div data-selectable="" ng-transclude=""></div></div></div>'),e.put("select2/select-multiple.tpl.html",'<div class="ui-select-container ui-select-multiple select2 select2-container select2-container-multi" ng-class="{\'select2-container-active select2-dropdown-open open\': $select.open, \'select2-container-disabled\': $select.disabled}"><ul class="select2-choices"><span class="ui-select-match"></span><li class="select2-search-field"><input type="search" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" role="combobox" aria-expanded="true" aria-owns="ui-select-choices-{{ $select.generatedId }}" aria-label="{{ $select.baseTitle }}" aria-activedescendant="ui-select-choices-row-{{ $select.generatedId }}-{{ $select.activeIndex }}" class="select2-input ui-select-search" placeholder="{{$selectMultiple.getPlaceholder()}}" ng-disabled="$select.disabled" ng-hide="$select.disabled" ng-model="$select.search" ng-click="$select.activate()" style="width: 34px;" ondrop="return false;"></li></ul><div class="ui-select-dropdown select2-drop select2-with-searchbox select2-drop-active" ng-class="{\'select2-display-none\': !$select.open || $select.items.length === 0}"><div class="ui-select-choices"></div></div></div>'),e.put("select2/select.tpl.html",'<div class="ui-select-container select2 select2-container" ng-class="{\'select2-container-active select2-dropdown-open open\': $select.open, \'select2-container-disabled\': $select.disabled, \'select2-container-active\': $select.focus, \'select2-allowclear\': $select.allowClear && !$select.isEmpty()}"><div class="ui-select-match"></div><div class="ui-select-dropdown select2-drop select2-with-searchbox select2-drop-active" ng-class="{\'select2-display-none\': !$select.open}"><div class="search-container" ng-class="{\'ui-select-search-hidden\':!$select.searchEnabled, \'select2-search\':$select.searchEnabled}"><input type="search" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" ng-class="{\'select2-active\': $select.refreshing}" role="combobox" aria-expanded="true" aria-owns="ui-select-choices-{{ $select.generatedId }}" aria-label="{{ $select.baseTitle }}" class="ui-select-search select2-input" ng-model="$select.search"></div><div class="ui-select-choices"></div><div class="ui-select-no-choice"></div></div></div>'),e.put("selectize/choices.tpl.html",'<div ng-show="$select.open" class="ui-select-choices ui-select-dropdown selectize-dropdown" ng-class="{\'single\': !$select.multiple, \'multi\': $select.multiple}"><div class="ui-select-choices-content selectize-dropdown-content"><div class="ui-select-choices-group optgroup"><div ng-show="$select.isGrouped" class="ui-select-choices-group-label optgroup-header" ng-bind="$group.name"></div><div role="option" class="ui-select-choices-row" ng-class="{active: $select.isActive(this), disabled: $select.isDisabled(this)}"><div class="option ui-select-choices-row-inner" data-selectable=""></div></div></div></div></div>'),e.put("selectize/match-multiple.tpl.html",'<div class="ui-select-match" data-value="" ng-repeat="$item in $select.selected track by $index" ng-click="$selectMultiple.activeMatchIndex = $index;" ng-class="{\'active\':$selectMultiple.activeMatchIndex === $index}" ui-select-sort="$select.selected"><span class="ui-select-match-item" ng-class="{\'select-locked\':$select.isLocked(this, $index)}"><span uis-transclude-append=""></span> <span class="remove ui-select-match-close" ng-hide="$select.disabled" ng-click="$selectMultiple.removeChoice($index)">&times;</span></span></div>'),e.put("selectize/match.tpl.html",'<div ng-hide="$select.searchEnabled && ($select.open || $select.isEmpty())" class="ui-select-match"><span ng-show="!$select.searchEnabled && ($select.isEmpty() || $select.open)" class="ui-select-placeholder text-muted">{{$select.placeholder}}</span> <span ng-hide="$select.isEmpty() || $select.open" ng-transclude=""></span></div>'),e.put("selectize/no-choice.tpl.html",'<div class="ui-select-no-choice selectize-dropdown" ng-show="$select.items.length == 0"><div class="selectize-dropdown-content"><div data-selectable="" ng-transclude=""></div></div></div>'),e.put("selectize/select-multiple.tpl.html",'<div class="ui-select-container selectize-control multi plugin-remove_button" ng-class="{\'open\': $select.open}"><div class="selectize-input" ng-class="{\'focus\': $select.open, \'disabled\': $select.disabled, \'selectize-focus\' : $select.focus}" ng-click="$select.open && !$select.searchEnabled ? $select.toggle($event) : $select.activate()"><div class="ui-select-match"></div><input type="search" autocomplete="off" tabindex="-1" class="ui-select-search" ng-class="{\'ui-select-search-hidden\':!$select.searchEnabled}" placeholder="{{$selectMultiple.getPlaceholder()}}" ng-model="$select.search" ng-disabled="$select.disabled" aria-expanded="{{$select.open}}" aria-label="{{ $select.baseTitle }}" ondrop="return false;"></div><div class="ui-select-choices"></div><div class="ui-select-no-choice"></div></div>'),e.put("selectize/select.tpl.html",'<div class="ui-select-container selectize-control single" ng-class="{\'open\': $select.open}"><div class="selectize-input" ng-class="{\'focus\': $select.open, \'disabled\': $select.disabled, \'selectize-focus\' : $select.focus}" ng-click="$select.open && !$select.searchEnabled ? $select.toggle($event) : $select.activate()"><div class="ui-select-match"></div><input type="search" autocomplete="off" tabindex="-1" class="ui-select-search ui-select-toggle" ng-class="{\'ui-select-search-hidden\':!$select.searchEnabled}" ng-click="$select.toggle($event)" placeholder="{{$select.placeholder}}" ng-model="$select.search" ng-hide="!$select.isEmpty() && !$select.open" ng-disabled="$select.disabled" aria-label="{{ $select.baseTitle }}"></div><div class="ui-select-choices"></div><div class="ui-select-no-choice"></div></div>')}]);
//# sourceMappingURL=select.min.js.map

// Create a module for naturalSorting
angular.module("naturalSort", [])

// The core natural service
.factory("naturalService", ["$locale", function($locale) {
  "use strict";
    // the cache prevents re-creating the values every time, at the expense of
    // storing the results forever. Not recommended for highly changing data
    // on long-term applications.
    var natCache = {},
    // amount of extra zeros to padd for sorting
    padding = function(value) {
      return "00000000000000000000".slice(value.length);
  },

    // Converts a value to a string.  Null and undefined are converted to ''
    toString = function(value) {
      if(value === null || value === undefined) return '';
      return ''+value;
  },

    // Calculate the default out-of-order date format (dd/MM/yyyy vs MM/dd/yyyy)
    natDateMonthFirst = $locale.DATETIME_FORMATS.shortDate.charAt(0) === "M",
    // Replaces all suspected dates with a standardized yyyy-m-d, which is fixed below
    fixDates = function(value) {
      // first look for dd?-dd?-dddd, where "-" can be one of "-", "/", or "."
      return toString(value).replace(/(\d\d?)[-\/\.](\d\d?)[-\/\.](\d{4})/, function($0, $m, $d, $y) {
        // temporary holder for swapping below
        var t = $d;
        // if the month is not first, we'll swap month and day...
        if(!natDateMonthFirst) {
                    // ...but only if the day value is under 13.
                    if(Number($d) < 13) {
                        $d = $m;
                        $m = t;
                    }
                } else if(Number($m) > 12) {
          // Otherwise, we might still swap the values if the month value is currently over 12.
          $d = $m;
          $m = t;
      }
        // return a standardized format.
        return $y+"-"+$m+"-"+$d;
    });
  },

    // Fix numbers to be correctly padded
    fixNumbers = function(value) {
      // First, look for anything in the form of d.d or d.d.d...
      return value.replace(/(\d+)((\.\d+)+)?/g, function ($0, integer, decimal, $3) {
        // If there's more than 2 sets of numbers...
        if (decimal !== $3) {
                    // treat as a series of integers, like versioning,
                    // rather than a decimal
                    return $0.replace(/(\d+)/g, function ($d) {
                        return padding($d) + $d;
                    });
                } else {
          // add a decimal if necessary to ensure decimal sorting
          decimal = decimal || ".0";
          return padding(integer) + integer + decimal + padding(decimal);
      }
  });
  },

    // Finally, this function puts it all together.
    natValue = function (value) {
        if(natCache[value]) {
            return natCache[value];
        }
        natCache[value] = fixNumbers(fixDates(value));
        return natCache[value];
    };

  // The actual object used by this service
  return {
    naturalValue: natValue,
    naturalSort: function(a, b) {
      a = natVale(a);
      b = natValue(b);
      return (a < b) ? -1 : ((a > b) ? 1 : 0);
  }
};
}])

// Attach a function to the rootScope so it can be accessed by "orderBy"
.run(["$rootScope", "naturalService", function($rootScope, naturalService) {
  "use strict";
  $rootScope.natural = function (field) {
    return function (item) {
        return naturalService.naturalValue(item[field]);
    };
};
}]);


apps.factory('vamoservice', function($http, $q){

    return {

        timeCalculate: function(duration){
            var milliseconds = parseInt((duration%1000)/100), seconds = parseInt((duration/1000)%60);
            var minutes      = parseInt((duration/(1000*60))%60), hours = parseInt((duration/(1000*60*60))%24);

            hours    = (hours < 10) ? "0" + hours : hours;
            minutes  = (minutes < 10) ? "0" + minutes : minutes;
            seconds  = (seconds < 10) ? "0" + seconds : seconds;
            temptime = hours + " H : " + minutes +' M';

            return temptime;
        },

        dayhourmin:function(t){
            var cd = 24 * 60 * 60 * 1000,
            ch = 60 * 60 * 1000,
            d = Math.floor(t / cd),
            h = Math.floor( (t - d * cd) / ch),
            m = Math.round( (t - d * cd - h * ch) / 60000),
            pad = function(n){ return n < 10 ? '0' + n : n; };

            if( m === 60 ){
              h++;
              m = 0;
          }

          if( h === 24 ){
              d++;
              h = 0;
          }

          return [d+'D', pad(h)+'H', pad(m)+'M'].join(':');
      },

      geocodeToserver: function (lat, lng, address) {
          try {
            //var reversegeourl = 'http://'+globalIP+context+'/public/store?geoLocation='+lat+','+lng+'&geoAddress='+address;
            //return this.getDataCall(reversegeourl);
        } catch(err) {
            console.log(err);
        }

    },

    getDataCall: function(url) {
      var defdata = $q.defer();
      $http.get(url).success(function(data){
         defdata.resolve(data);
     }).error(function() {
        defdata.reject("Failed to get data");
    });

     return defdata.promise;
 },


 statusTime:function(arrVal) {

  var posTime     = {};
  var temptime    = 0;
  var tempcaption = 'Position';

  if(arrVal.parkedTime!=0){
    temptime = this.dayhourmin(arrVal.parkedTime);
    tempcaption = 'Parked';

}else if(arrVal.movingTime!=0){
    temptime = this.dayhourmin(arrVal.movingTime);
    tempcaption = 'Moving';

}else if(arrVal.idleTime!=0){
    temptime = this.dayhourmin(arrVal.idleTime);
    tempcaption = 'Idle';

}else if(arrVal.noDataTime!=0){
    temptime = this.dayhourmin(arrVal.noDataTime);
    tempcaption = 'No_data';

}

posTime['temptime'] = temptime;
posTime['tempcaption'] = tempcaption;

return posTime;
},

iconURL:function(temp) {

  var pinImage;
  var fransPath="";
  if(localStorage.getItem('fCode')=="ETHIOPIA"){
      fransPath ="/newFranMarker"
  }
  if(temp.vehicleType=="Van"){
                //alert('hello');
                if(temp.color =='P' || temp.color =='N' || temp.color =='A'){

                  if(temp.color =='A'){
                   pinImage = 'assets/imgs'+fransPath+'/vanMarker/yellow.png';

               }else{
                   pinImage = 'assets/imgs'+fransPath+'/vanMarker/'+temp.color+'.png';
               }

           } else if(temp.position == 'N') {
               pinImage =  'assets/imgs/trans.png';

           } else if(temp.position=="M" && temp.ignitionStatus=="OFF"){
               pinImage = 'assets/imgs'+fransPath+'/vanMarker/P.png';

           }  else {

              if(temp.color=="R"){
               pinImage = 'assets/imgs'+fransPath+'/vanMarker/R.png';

           } else {

               pinImage = 'assets/imgs'+fransPath+'/vanMarker/'+temp.color+'_'+temp.direction+'.png';
           }

       }

   } else{
      if(temp.color =='P' || temp.color =='N' || temp.color =='A'){

          if(temp.color =='A'){
           pinImage = 'assets/imgs'+fransPath+'/orangeB.png';

       }else{
           pinImage = 'assets/imgs'+fransPath+'/'+temp.color+'.png';
       }

   } else if(temp.position == 'N') {
       pinImage =  'assets/imgs/trans.png';

   } else if(temp.position=="M" && temp.ignitionStatus=="OFF"){
       pinImage = 'assets/imgs'+fransPath+'/P.png';

   } else{
       if(temp.color=="R"){
         pinImage = 'assets/imgs'+fransPath+'/R.png';

     } else {

         pinImage = 'assets/imgs/'+temp.color+'_'+temp.direction+'.png';
     }
 }
}


return pinImage;
},

trvIcon:function(temp){

  var pinImage;

  if(temp.color =='P' || temp.color =='N' || temp.color =='A'){

    if(temp.color =='A') {

     pinImage = 'assets/imgs/trvMarker/yellow.png';

 } else {

     pinImage = 'assets/imgs/trvMarker/'+temp.color+'.png';

 }

} else if(temp.position == 'N') {

 pinImage =  'assets/imgs/trvMarker/trans.png';

} else if(temp.position=="M" && temp.ignitionStatus=="OFF") {

 pinImage = 'assets/imgs/trvMarker/P.png';

} else {

    if(temp.color=="R"){
     pinImage = 'assets/imgs/trvMarker/R.png';

 } else {

     pinImage = 'assets/imgs/trvMarker/'+temp.color+'_'+temp.direction+'.png';
 }

}

return pinImage;
},

markerImage:function(temp) {

    var pinImage;

    if(temp.color =='P' || temp.color =='N' || temp.color =='A'){

        if(temp.color =='A'){
                   // pinImage = 'assets/imgs/orangeB.png';
                   pinImage = 'assets/imgs/vehicle-marker/'+temp.vehicleType+'_Idle.png';

               }else{
                   // pinImage = 'assets/imgs/'+temp.color+'.png';
                   pinImage = 'assets/imgs/vehicle-marker/'+temp.vehicleType+'_'+temp.color+'.png';
               }

           } else if(temp.position == 'N') {
               pinImage =  'assets/imgs/trans.png';
                 //  pinImage =  'assets/imgs/vehicle-marker/'+temp.vehicleType+'_N.png';
             } else if(temp.position=="M" && temp.ignitionStatus=="OFF"){
                // pinImage = 'assets/imgs/P.png';
                pinImage = 'assets/imgs/vehicle-marker/'+temp.vehicleType+'_P.png';

            } else{
               //  pinImage = 'assets/imgs/'+temp.color+'_'+temp.direction+'.png';
               pinImage = 'assets/imgs/vehicle-marker/'+temp.vehicleType+'_'+temp.color+'.png';
           }

           return pinImage;
       },

       assetImage:function(temp) {

        var pinImage;

        if(temp.color =='P' || temp.color =='N' || temp.color =='A') {
                   // pinImage = 'assets/imgs/'+temp.color+'.png';
                   pinImage = 'assets/imgs/asset-marker/as_'+temp.color+'.png';

               } else if(temp.position == 'N') {
                   pinImage =  'assets/imgs/asset-marker/as_N.png';
                 //  pinImage =  'assets/imgs/vehicle-marker/'+temp.vehicleType+'_N.png';
             } else if(temp.position=="M" && temp.ignitionStatus=="OFF"){
                // pinImage = 'assets/imgs/P.png';
                pinImage = 'assets/imgs/asset-marker/as_P.png';

            } else{
               //  pinImage = 'assets/imgs/'+temp.color+'_'+temp.direction+'.png';
               pinImage = 'assets/imgs/asset-marker/as_'+temp.color+'.png';
           }

           return pinImage;
       },

       googleAddress:function(data) {

        var tempVar = data.address_components;
        var strNo  = 'sta:null';
        var rotNam = 'rot:null';
        var locs   = 'loc:null';
        var add1   = 'ad1:null';
        var add2   = 'ad2:null';
        var coun   = 'con:null';
        var postal = 'pin:null';

        if(tempVar!=null || tempVar.length!=0){    

            for(var i=0;i<tempVar.length;i++){
             //console.log(newVarr[i].types);
             for(var j=0;j<tempVar[i].types.length;j++){
               //console.log(newVarr[i].types[j]);
                //console.log(newVarr[i].long_name);
                var valType = tempVar[i].types[j];
                var valName = tempVar[i].long_name;

                switch(valType){

                    case "street_number":
                 //console.log("stn : "+valName);
                 strNo ='sta:'+valName;
                 break;
                 case "route":
                 //console.log("rot : "+valName);
                 rotNam='rot:'+valName;
                 break;
                 case "neighborhood":
                  //console.log("neigh : "+valName);
                  //retVar+='nei:'+valName;
                  break;
                /*case "sublocality":
                  //console.log("loc : "+valName);
                  retVar+='loc:'+valName+' ';
                  break;*/
                  case "locality":
                  //console.log("loc : "+valName);
                  locs='loc:'+valName;
                  break;
                  case "administrative_area_level_1":
                  //console.log("ad1 : "+valName);
                  add1='ad1:'+valName;
                  break;
                  case "administrative_area_level_2":
                  //console.log("ad2 : "+valName);
                  add2='ad2:'+valName;
                  break;
                  case "country":
                  //console.log("con : "+valName);
                  coun='con:'+valName;
                  break;
                  case "postal_code":
                  //console.log("pin : "+valName);
                  postal='pin:'+valName;
                  break;
              }

          }
      }

  }

  var retVar = strNo+' '+rotNam+' '+locs+' '+add1+' '+add2+' '+coun+' '+postal;
          //console.log(retVar);

          return retVar;
      },

  }  
});

apps.constant("globe", {
  'DOMAIN_NAME' : '//'+globalIP+context+'/public',
});
apps.directive('onFinishRender', ['$timeout', '$parse', function ($timeout, $parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('ngRepeatFinished');
                    if ( !! attr.onFinishRender) {
                        $parse(attr.onFinishRender)(scope);
                    }
                });
            }

            if (!!attr.onStartRender) {
                if (scope.$first === true) {
                    $timeout(function () {
                        scope.$emit('ngRepeatStarted');
                        if ( !! attr.onStartRender) {
                            $parse(attr.onStartRender)(scope);
                        }
                    });
                }
            }
        }
    }
}]);
//apps.controller('mainCtrls', ['$scope', '$http','vamoservice', 'globe', '$filter', '$compile', function($scope, $http, vamoservice, GLOBAL, $filter, $compile){
    apps.controller('mainCtrls', ['$scope', '$http','vamoservice', 'globe', '$filter', '$compile','$translate',function($scope, $http, vamoservice, GLOBAL, $filter, $compile, $translate){
      $scope.debugValue ='Disable';
  //document.getElementById("searchBox").disabled = true;
  var searchEnabled = false;
  var language=localStorage.getItem('lang');
  $scope.multiLang=language;
  $translate.use(language);
  $scope.command='SET-IP';
  $scope.res=0;
  $scope.showSendCmd=false;
  $scope.selectedVehDeviceModel = "";
  $scope.noOfTank = 1;
  $scope.fuelLitreArr=[0,0,0,0,0];
  $scope.tankSizeArr=[0,0,0,0,0];
  $scope.marqueeMsg=false;
  $scope.dealerName     =  localStorage.getItem('dealerName');
  $scope.showProButton=false;
  $scope.expectedFuelMileage;

  // document.getElementById("search_box").value="Search......";
  // $scope.changeLanguage = function(){
  //  $translate.use(language);
  // }

  // added for multi language
         // $('#lang').on('change', function($translate) {
         //        var language=$(this).val();
         //        alert(language);
         //        localStorage.setItem('lang',language);
         //        $translate.use(language);
         //        });
         var _mapsDetails = {};
         var osm_mapsDetails = {};
         $scope.SiteCheckbox = {
           value1 : true,
           value2 : 'YES'
       }
       var translate = $filter('translate');
       localStorage.setItem('selectedVehicleId', null);
       $scope.viewEditforAll            =  true;
       $scope.viewEditforEthopia        =  false;
       var mapfcode          =  localStorage.getItem('fCode');
  // var fcodeValue       =  "newFrans";
  $scope.markerPath = "";
  if(mapfcode=="ETHIOPIA"){
    $scope.markerPath = "/newFranMarker/statusMarker";
    $scope.viewEditforAll    =  false;
    $scope.viewEditforEthopia    =  true;
}

if(document.location.href.indexOf('gpsvts.net')>-1 || document.location.href.indexOf('vamosys.com')>-1){
    $scope.showProButton=true;
}

var userNameVal       =  localStorage.getItem('userIdName').split(',');
var userVals          =  userNameVal[1].split('"');
var userName          =  userVals[0];
$scope.VirtualUser      = false;
var assLabel          =  "false";
var boolName          =  '';
var labelName         =  '';
$scope.vehicleData=[];

$scope.vehiLabel      =  translate("Vehicle");
$scope.vehiAssetView  =  true;
$scope.labelUrl       =  GLOBAL.DOMAIN_NAME+'/isAssetUser';
$scope.virtualUrl       =  GLOBAL.DOMAIN_NAME+'/isVirtualUser';

$scope.singleVehicle  =  0;

var initValOsm        =  0;
var initValGoog       =  0;
$scope.data02         =  [];
$scope.reportUrl      =  GLOBAL.DOMAIN_NAME+'/getReportsList';

$scope.trvShow          =  false;  
$scope.bottomTableShow  =  false;

$scope.isrenamed= localStorage.getItem('isnameChanged');

  // // added for User name change
  // $http.get( 'getReportsRestrictForMobile' ).success(function(response){
  //   $scope.marqueeMsg=response.FUEL;  
  // });
  $scope.range = function(min, max, step) {
      step = step || 1;
      var input = [];
      for (var i = min; i <= max; i += step) {
          input.push(i);
      }
      return input;
  };
   //added for GPS Command
   $scope.getCommand = function(command) {
    $scope.inputEmp="";
    $scope.pwdErr="";
    $scope.res=0;
    $scope.remove=0;
    //alert(command);
    $scope.command=command;
    if($scope.command=='SET-BASICDATA' || $scope.command=='SET-HARDWAREDATA' || $scope.command=='SET-OTHERDATA' ){

        $scope.remove=1;
        $scope.input=1;
        gpscall($scope.command);
    }

}
$scope.removeCommand = function() {
   $scope.inputEmp="";
   $scope.pwdErr="";  
   $scope.input=0;
   gpscall('remove');
   $scope.remove=0;

}
$scope.getCommandHistory = function() {
   $scope.inputEmp="";
   $scope.pwdErr="";
   $scope.fromTimeUtc=new Date(new Date().setHours(0, 0, 0, 0)).getTime();
   $scope.toTimeUtc = new Date().getTime();
   $scope.comHisurl = GLOBAL.DOMAIN_NAME+'/getSentcommandsHistory?vehicleId='+$scope.vehiname+'&fromTimeUtc='+$scope.fromTimeUtc+'&toTimeUtc='+$scope.toTimeUtc;
   $http({
    method : "GET",
    url : $scope.comHisurl
}).then(function mySuccess(response) {
          //alert(response);
          $scope.ComHistory=response.data;
      });

}
$scope.submitGPSCommand = function() {
    $scope.inputEmp="";
    $scope.pwdErr="";
    if($scope.command=='SET-IP'){
        $scope.input=document.getElementById("ipaddress").value;
        $scope.pwd=document.getElementById("password1").value;
        if( $scope.input==""){
          $scope.inputEmp="please Enter IP Address";
      }

  }
  else if($scope.command=='SET-APN'){
    $scope.pwd=document.getElementById("password2").value;
    $scope.input=document.getElementById("apnName").value;
    if( $scope.input==""){
      $scope.inputEmp="please enter APN name"
  }
}
else if($scope.command=='SET-CENTERNUMBER'){
    $scope.pwd=document.getElementById("password3").value;
    $scope.input=document.getElementById("mobileNo").value;
    if( $scope.input==""){
      $scope.inputEmp="please enter mobile no"
  }
}
else if($scope.command=='SET-INTERVAL'){
    $scope.pwd=document.getElementById("password4").value;
    $scope.input=document.getElementById("interval").value;
    if( $scope.input==""){
      $scope.inputEmp="Please enter intervel"
  }else if(parseInt($scope.input)>30){
    $scope.inputEmp="Please enter intervel within 30 minutes";
    $scope.input="";
}
}else if($scope.command=='SET-RESTART'){
    $scope.pwd=document.getElementById("password5").value;
    $scope.input=-1;
}
else if($scope.command=='RESET'){
    $scope.pwd=document.getElementById("resetpwd").value;
    $scope.input=-1;
}
else if($scope.command=='CUSTOM-COMMAND'){
    $scope.input=document.getElementById("customCmd").value;
    $scope.pwd=document.getElementById("cusCmdPwd").value;
    if( $scope.input==""){
      $scope.inputEmp="Please enter command"
  }
}
if($scope.pwd==""){
    $scope.pwdErr="Please enter password";
}
if( $scope.input!=""&&$scope.pwd!=""){
  if(checkXssProtection($scope.pwd) == true){
    var password  = $scope.pwd;
    var URL_ROOT    = "AddSiteController/";    /* Your website root URL */


    $http({
      method: 'POST',
      url: URL_ROOT+'checkPwd',
      data: JSON.stringify({'pwd': password})
  })
    .then(function (response) {
      let data = response.data;
      console.log('Sucess---------->' + data+location.pathname);
      if(data == 'incorrect'){
          $scope.pwdErr   = "Invalid Password";
          alert($scope.pwdErr);
      }
      else if (data == 'correct'){
        if($scope.selectedVehDeviceModel=="GV05"){
            gpscall($scope.command);
        }else if(deviceModalList.includes($scope.selectedVehDeviceModel)){
            gpscallForGT06N($scope.command)
        }
                      // localStorage.setItem('auth', 'sitesVal');
                  }

              }, function (error) {
                console.log('fail');
                $scope.pwdErr   = "Response Failure";   
            });

}
}

}
function gpscallForGT06N(GPScomm){
    $scope.inputEmp="";
    $scope.pwdErr="";
    let commandCode = "";
    if(GPScomm=='RESET'){
        commandCode = '1001';
        command = "";
    }else if(GPScomm=='CUSTOM-COMMAND'){
        commandCode = '7634';
        command = $scope.input
    }
    data = {
      "vehicleId": $scope.vehiname, 
      "commandId": commandCode, 
      "customCommand": command
  }
  let GPSurl = GLOBAL.DOMAIN_NAME+'/sendCommandFromServer';

  $http({
      method: 'POST',
      url: GPSurl,
      data: JSON.stringify(data)
  })
  .then(function (res) {
      console.log(res);
      let response = res.data;
      if(response.response!='failure'){
          $scope.GPSrespose=response.data;
          if($scope.GPSrespose.includes(",")){
            $scope.GPSrespose = $scope.GPSrespose.replace(/,/g, " ,");
        }
        $scope.res=1;
    }else{
      $scope.pwdErr = response.error;
  }

}, function (error) {
  console.log(error.data);
  $scope.pwdErr   = "Response Failure";  
});
}

function gpscall(GPScomm){
 $scope.inputEmp="";
 $scope.pwdErr="";
 $scope.GPSurl = GLOBAL.DOMAIN_NAME+'/sentCommandsFromFrontEnd?vehicleId='+$scope.vehiname+'&functionality='+GPScomm+'&input='+$scope.input;
 $http({
    method : "GET",
    url : $scope.GPSurl
}).then(function mySuccess(response) {
          //alert(response);
          $scope.GPSrespose=response.data;
          // $scope.GPSrespose="YYYYBDBFBGHJKJFJHDGHFDU,YQWERTYUIOP[ASDFGHJKLZ,XCVBNMERTYUIOOPPDFGHJ,ASDFGHFGHJ";
          if($scope.GPSrespose.includes(",")){
            $scope.GPSrespose = $scope.GPSrespose.replace(/,/g, " ,");
        }
        if(GPScomm=='remove'){
            $scope.GPSrespose="Removed Successfully.";
        }
        $scope.res=1;
          //alert($scope.GPSrespose);
      });
}
 //added for username change
 $scope.changeUserName = function(status) {
   $http({
       method: 'get',
       url: 'changeUserName'
   }).
   then(function(response) {

       $scope.status = response.status;
       console.log(response.data);
       if(response.data=='success')
         $scope.Successmsg="Successfully Updated";
     else
       $scope.Successmsg="Request failed";
   setTimeout(function () {
      document.location.href = 'logout';

  }, 1000);

}, function(response) {
   $scope.error="Request failed";
   $scope.data   = response.data || 'Request failed';
   $scope.status = response.status;
});
}

$http.get( $scope.reportUrl ).success(function(data){
    //alert('report');
    var trvShow = data;
     //console.log(trvShow);

     if(trvShow != "" && trvShow != null) {    

       //console.log('not Empty getReportList API ...');

       angular.forEach(trvShow,function(val, key){

        var newReportName = Object.getOwnPropertyNames(val).sort();

        if(newReportName == 'TRACK_PAGE') {

            if( val.TRACK_PAGE[0].Customized_View == true ) {                

              $scope.trvShow          =  true;
              $scope.bottomTableShow  =  true;
              localStorage.setItem('trackNovateView', true);

                    //alert('customize'+     $scope.trvShow  );

                } else if( val.TRACK_PAGE[0].Normal_View == true ) {

                  $scope.trvShow          =  false;  
                  $scope.bottomTableShow  =  false;
                  localStorage.setItem('trackNovateView', false);

              }
          }
      });
   }
   
});


$scope.$watch("virtualUrl", function (val) {
    //console.log($scope.labelUrl);
    $http.get($scope.virtualUrl).success(function(data){
      console.log(data);
      if(!data.error){
          boolName = data.trim();
      }
      console.log('Virtual : '+boolName);
      console.log( $scope.virtualUrl);

      if( boolName == "true" ) {
          $scope.VirtualUser      = true;
      } else {
          $scope.VirtualUser      = false;
      }
  })  
    .error(function (error, status){
       $scope.VirtualUser      = false;
   });
});




$scope.$watch("labelUrl", function (val) {
    //console.log($scope.labelUrl);
    $http.get($scope.labelUrl).success(function(data){
          //console.log(data);
          if(!data.error){
              boolName = data.trim();
          }
          console.log('Asset: '+boolName);

          if( boolName == "true" ) {
              assLabel              = "true";
              $scope.vehiLabel      = translate("Asset");
              $scope.vehiImage      = true;
              $scope.vehiAssetView  = false;
              localStorage.setItem('isAssetUser', "true");
          } else {
              assLabel              = "false";
              $scope.vehiLabel      = translate("Vehicle");
              $scope.vehiImage      = false;
              $scope.vehiAssetView  = true;
              localStorage.setItem('isAssetUser', "false");
          }
      })  
    .error(function (error, status){
       $scope.vehiLabel      = translate("Vehicle");
       $scope.vehiImage      = false;
       $scope.vehiAssetView  = true;
       localStorage.setItem('isAssetUser', "false");
        // console.log('vehi label...');
    });
});

 //console.log(userName);
//  var googleUserArr=["QTRACK","UNOTELOS","FLEETOPTECHNOLOGIES","RADAR123","ICOM","INTROGPS"];
//  if($.inArray( userName, LandTarr ) > -1||userName=="DHARIKAZEEM") {

//   map_change          =  0;
//   $scope.mapsHist     =  0;
//   $scope.maps_name    =  0;
//   localStorage.setItem('mapNo',0);
//   $('.cMarker').hide();

// } else {
// mapfcode="VAM";
// if(mapfcode=="VAM" && localStorage.getItem('dealerName')=='ACSQ'){
//      map_change          =  0;
//         $scope.mapsHist     =  0;
//         $scope.maps_name    =  0;
//         localStorage.setItem('mapNo',0);
//         $('.cMarker').hide();
// }
//     else if(mapfcode=="SMP" || (mapfcode=="VAM" && $.inArray( localStorage.getItem('dealerName'), googleUserArr ) == -1)){
//         map_change          =  1;
//         $scope.mapsHist     =  1;
//         $scope.maps_name    =  1;
//         localStorage.setItem('mapNo',1);
//     } else {
//         map_change          =  0;
//         $scope.mapsHist     =  0;
//         $scope.maps_name    =  0;
//         localStorage.setItem('mapNo',0);
//         $('.cMarker').hide();
//     }

// }

$scope.markLabss        =  true;
$scope.polyLabs         =  true;
$scope.selected=false;
$scope.days             =  0;
$scope.days1            =  0;
$scope.fcode            =  [];
$scope.final_data;
$scope.name             =  "Calvin";
$scope.groupid          =  0;
$scope.Filter           =  'ALL';
$scope.enableCluster    =  0;
$scope.marker           =  [];
$scope.marker_osm       =  [];
$scope.markerLabel      =  [];
$scope.customZoom       =  0;
$scope.positionLng      =  0;
$scope.positionLat      =  0;
$scope.vehiclFuel       =  true;
$scope.checkVal         =  false;
$scope.clickflag        =  false;
$scope.clickflagVal              =  0;
$scope.nearbyflag                =  false;
$scope._editValue_con            =  false;
// if(userName=='TPMS'){
//   $scope.zoom  =  16;
// }else{
  $scope.zoom  =  6;
//}

$("#currenStatus").show();
$scope._editValue                =  {};
$scope._editValue._vehiTypeList  =  [];
$scope._editValue._vehiModelList  = [];
$scope.showOthers=false;
$scope.labeldisplay              =  true;
$scope.collapse=false;
var geocoderVar;
$scope.flightpathall    =  [];
var tempdistVal         =  0;
$scope.parseInt         =  parseInt;
$scope.zohod            =  0;  
var googSite            =  0;
var osmSite             =  0;

var infowindow_osm;
$('#notifyMsg').hide();
$scope.hideMe           =  true;
$scope.markLab          =  true;
$scope.circleLabel      =  false;

$scope.siteFind         =  '';
$scope.siteExec         =  false;
$scope.siteExec2        =  false;
$scope.sitevalu         =  0;
var siteListInit        =  0;
$scope.fuelErrorMsg = false

$scope.navReports       =  "../public/reports";
$scope.navStats         =  "../public/statistics?ind=1";
$scope.navSched         =  "../public/settings";
$scope.navFms           =  "../public/fleetManagement?fms=FleetManagement";
//$scope.navFms           =  "http://gpsvts.net/fms/public/switchLogin/"+userName;

function sessionValue(vid, gname,licenceExpiry){
    localStorage.setItem('user', JSON.stringify(vid+','+gname));
    localStorage.setItem('licenceExpiry', licenceExpiry);
    $("#testLoad").load("../public/menu");
}
function setDeviceVolt(vehicleInfo){
  $scope.fuelErrorMsg = false;
  if(vehicleInfo.fuel=='yes'){
    if(vehicleInfo.noOfTank>1){
      var fuelSensorVolt = vehicleInfo.fuelSensorVolt.split(":");
      fuelSensorVolt.map(volt => {
        if(volt>=65532){
          $scope.fuelErrorMsg = true;
      }
  });
  }else if(vehicleInfo.deviceVolt>=65532){
     $scope.fuelErrorMsg = true
 }
}
}

$scope.callMyCustomMethod = function() {
  if(localStorage.getItem('selectedVehicleId') != null) {
        //$("#id_"+localStorage.getItem('selectedVehicleId')).click();
        var vehicleID = localStorage.getItem('selectedVehicleId').split("__");
        $scope.genericFunction(vehicleID[0], vehicleID[1]);
    }

}

$scope.changeCollapse=function(){

    setTimeout(function () {
        $('.selectpicker2').selectpicker('refresh');
        noneSelectedText : 'Please Select'
    },500);
    // $(".selectpicker2").selectpicker({
    //                     noneSelectedText : 'Please Select' 
    //                 });

    $scope.collapse=!$scope.collapse;

}


$scope.siteFindFunc = function(val){

    if(map_change==1){

        for(var i=0; i<$scope.markerPolyss2.length; i++) {

          if(val.sName == $scope.markerPolyss2[i].label._content) {

           $scope.siteExec2 = true;
           $scope.sitevalu=i;

           map_osm.setZoom(20);
            //$scope.map_osm.setView( L.latLng( $scope.markerPolyss2[i]._latlng.lat, $scope.markerPolyss2[i]._latlng.lng ) );
            map_osm.setView($scope.markerPolyss2[i].getLatLng());

        }    
    }
} else if(map_change==0){

    for(var i=0; i<$scope.markerPolysG.length; i++) {

        if(val.sName == $scope.markerPolysG[i].labelContent) {

                //console.log($scope.markerPolysG[i].labelContent);

                $scope.siteExec2 = true;
                $scope.sitevalu  = i;

                map_goog.setZoom(18);
                map_goog.setCenter($scope.markerPolysG[i].getPosition());
            }
        }
    }
}

$scope.sort      = sortByDate('date');
$scope.column    = 'shortName'; // set the default sort type
$scope.reverse   = true;  // set the default sort order
$scope.sortGroup = 'shortName';

// called on header click
$scope.sortColumn = function(col){
  $scope.column = col;
  if($scope.reverse){
     $scope.reverse = false;
     $scope.reverseclass = 'icon-chevron-up';
 }else{
     $scope.reverse = true;
     $scope.reverseclass = 'icon-chevron-down';
 }
};

 // remove and change class
 $scope.sortClass = function(col){
  if($scope.column == col ){
     if($scope.reverse){
        return 'icon-chevron-down';
    }else{
        return 'icon-chevron-up';
    }
}else{
 return 'icon-sort';
}
}

var startLoading    = function () {
   $('#statusLoad').show();
};

// loading stop function
var stopLoading   = function () {
    $('#statusLoad').fadeOut();
    // if(!searchEnabled){
    //   //document.getElementById("searchBox").disabled = false;
    //   searchEnabled = true;
    // }
    
    // if($scope.searchbox=='Search'){
    //     $( document ).ready(function() {

    //       //alert('hello');
    //       $scope.searchbox="";
    //     });
    //  }
};

function sortByDate(field){
  var sort = {sortingOrder : field, reverse : false };
  return sort;
}

function graphChange(vehifuel){
  return vehifuel == 'yes' ? true : false;
}

$scope.trimColon = function(textVal){

    if (textVal != null || textVal != undefined ){
       return textVal.split(":")[0].trim();
   }
};
function getTodayDate() {
  var date = new Date();
  return ("0" + (date.getDate())).slice(-2)+'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+date.getFullYear();
};
function setDateTimeSessionValues(){

    var dateObj    =  new Date();
    var fromNowTS  =  new Date(dateObj.setDate(dateObj.getDate()));
    var fromDate   =  getTodayDate();
    var fromTime   =  '12:00 AM';
    var toTime     =  formatAMPM(fromNowTS.getTime());
    localStorage.setItem('fromDate', fromDate);
    localStorage.setItem('fromTime', fromTime);
    localStorage.setItem('toDate', fromDate);
    localStorage.setItem('toTime', toTime);
        //alert(localStorage.getItem('toTime'));

    }
    function formatAMPM(date) {
      var date = new Date(date);
      var hours = date.getHours();
      var minutes = date.getMinutes();
      var ampm = hours >= 12 ? 'PM' : 'AM';
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
      return strTime;
  }

  $scope.msToTime   = function(ms) {
      days = Math.floor(ms / (24*60*60*1000));
      daysms=ms % (24*60*60*1000);
      hours = Math.floor((ms)/(60*60*1000));
      hoursms=ms % (60*60*1000);
      minutes = Math.floor((hoursms)/(60*1000));
      minutesms=ms % (60*1000);
      sec = Math.floor((minutesms)/(1000));
      // return days+"d : "+hours+"h : "+minutes+"m : "+sec+"s";
      return hours+":"+minutes+":"+sec;
  }

  function convertToMillis(duration){
    if(duration.indexOf(':')>-1){
       var hrs= duration.split(":")[0].trim();
       var mins=duration.split(":")[1].trim();
       var secs=duration.split(":")[2].trim();
       var regex = /\d+/g;
       var matches1 = parseInt(hrs.match(regex)); 
       var matches2 = parseInt(mins.match(regex));
       var matches3 = parseInt(secs.match(regex));
       return matches1*60000*60+matches2*60000+matches3*1000;}
       else {
        return parseInt(0);
    }

}

function checkXssProtection(str){

  var regex     = /<\/?[^>]+(>|$)/g;
  var enc       = encodeURI(str);
  var dec       = decodeURI(str);
  var replaced  = enc.search(regex) >= 0;
  var replaced1 = dec.search(regex) >= 0;

  return (replaced == false && replaced1 == false) ? true : false ;
}

function getMaxOfArray(numArray) {
 return Math.max.apply(null, numArray);
}

function trimDueDays(value){
   var splitValue=value.split(/[ ]+/);
   return  splitValue[2];
}

$scope.ZohoCall = function(){
    //console.log('close button...');
    $scope.zohod        = 0;
    $scope.zohoCloseBut = 1;
}


function zohoDayValue(datValue){

    $scope.zohoHighVal=datValue;

    if(datValue>0) {

      if(datValue==1) {

        $scope.zohoReports = 1;
        $scope.zohod       = 1;
        $scope.zohoDays    = translate('Payment overdue for')+" "+ datValue+" "+translate('day')+'.'+ translate('Please make payment at the earliest to avoid de-activation')+'.';

    } else if(datValue>1) {

        $scope.zohoReports = 1;
        $scope.zohod       = 1;
        $scope.zohoDays    = translate('Payment overdue for')+" "+datValue+" "+translate('days')+'.'+translate('Please make payment at the earliest to avoid de-activation')+'.';
    }

    return datValue;
}
}


function zohoDataCall(data) {

   $scope.zohoData =[];
   $scope.zohoDayss=[];
   var zohoDatas=[];

   zohoDatas.push({customerName:data.customerName});
   zohoDatas[0].hist=[];

   angular.forEach(data.hist,function(val, key){
      $scope.zohoDayss.push(trimDueDays(val.dueDays));
      zohoDatas[0].hist.push({customerName:val.customerName,balanceAmount:val.balanceAmount,dueDate:val.dueDate,dueDays:val.dueDays,inVoice:val.invoiceLink});
  });

   $scope.zohoData=zohoDatas;
   zohoDayValue(getMaxOfArray($scope.zohoDayss));

   angular.forEach(data.hist,function(val, key){
     var newZohoVal=trimDueDays(val.dueDays);

     if(newZohoVal==$scope.zohoHighVal){
      $scope.zohoLink=val.invoiceLink;
  }      
});
}


$scope.routeDataNames=function(data){

    var vehiRouteList = [];
    $scope._editValue._vehiRoutesList = [];

    angular.forEach(data.routeParents,function(val, key){

      if(val.route != null){
             //console.log(val.orgId);
             //console.log(val.route);
             vehiRouteList.push(val.route);    
         }
     });

        //console.log(vehiRouteList);

        $scope._editValue._vehiRoutesList.push("nill");

        angular.forEach(vehiRouteList,function(val, key){
            angular.forEach(val,function(sval, skey){

              $scope._editValue._vehiRoutesList.push(sval);

          });
        });
    // console.log($scope._editValue._vehiRoutesList);
}

$scope.$watch("getRoutes", function (val) {

    $http.get($scope.getRoutes).success(function(data){
          //document.getElementById("search_box").value="";
       // vamoservice.getDataCall($scope.getRoutes).then(function(data) {
          $scope.routeDataNames(data);
      });
});

$scope.$watch("getZoho", function (val) {
   vamoservice.getDataCall($scope.getZoho).then(function(data) {
      //document.getElementById("search_box").value="";
      zohoDataCall(data);
  });
});

$scope.updateDetails    =   function() {
    var userNameVal       =  localStorage.getItem('userIdName').split(',');
    var userVals          =  userNameVal[1].split('"');
    var userName          =  userVals[0];
    if($scope._editValue._overSpeed=="0" || $scope._editValue._overSpeed==undefined){
        $scope._editValue._overSpeed="60";
    }

    if((checkXssProtection($scope._editValue._odoDista) == true) 
        && (checkXssProtection($scope._editValue._shortName) == true) &&
        (checkXssProtection($scope._editValue._overSpeed) == true) && 
        (checkXssProtection($scope._editValue._driverName) == true) && 
        (checkXssProtection($scope._editValue._mobileNo) == true) && 
        (checkXssProtection($scope._editValue._regNo) == true))        {  

        var vehTyp;

    if($scope._editValue.vehType=='Heavy Vehicle'){
     vehTyp = 'heavyVehicle';
 }else if($scope._editValue.vehType=='Passenger Van'){
     vehTyp = 'PassengerVan';
 }else if($scope._editValue.vehType=='Pick-up'){
     vehTyp = 'Pickup';
 } else {
     vehTyp = $scope._editValue.vehType;
 }

 if($scope._editValue.isFreezedKm=='yes'){
    if(( $scope._editValue._freezedKm!=null && $scope._editValue._freezedKm!='' 
        && $scope._editValue._freezedKm!='0') && $scope._editValue._shiftNames.toString()!=''){
     $http.post("postVehicleData", 
        {"userId":userName,
        "shortName":$scope._editValue._shortName,
        "vehicleId": $scope.vehicleid,
        "odoDistance": $scope._editValue._odoDista!=null?$scope._editValue._odoDista:0,
        "overSpeedLimit":$scope._editValue._overSpeed,
        'driverName':$scope._editValue._driverName,
        'driverMobile' : $scope._editValue._driverMobile,
        'regNo': $scope._editValue._regNo,
        'vehicleType': vehTyp!=undefined?vehTyp:'',    
        'routeName':$scope._editValue.routeName,
        'vehicleModel':$scope.showOthers==true?($scope._editValue._veModel!=undefined?$scope._editValue._veModel:''):$scope._editValue.vehiModel,
        'userIpAddress':localStorage.getItem('ipAddress'),
        'secondaryEngineHours':convertToMillis($scope._editValue._secondaryEngineOn),
        'freezedKm':$scope._editValue._freezedKm,
        'freezedKmDate':$('#freezedKmDate').val()!=undefined?$('#freezedKmDate').val():'',
        'shiftNames':$scope._editValue._shiftNames.toString(),
        'shiftDate':$('#shiftDate').val()!=undefined?$('#shiftDate').val():''
    })
 .success(function (response) {
  console.log("success");

  location.reload();

})
 .error(function (response) {
  console.log("fail");
});}
 else {
    alert('Please enter valid Freezed Km value and Shift Names to proceed');
}
}
else {
   $http.post("postVehicleData", 
    {"userId":userName,
    "shortName":$scope._editValue._shortName,
    "vehicleId": $scope.vehicleid,
    "odoDistance": $scope._editValue._odoDista!=null?$scope._editValue._odoDista:0,
    "overSpeedLimit":$scope._editValue._overSpeed,
    'driverName':$scope._editValue._driverName,
    'driverMobile' : $scope._editValue._driverMobile,
    'regNo': $scope._editValue._regNo,
    'vehicleType': vehTyp!=undefined?vehTyp:'',
    'routeName':$scope._editValue.routeName,
    'vehicleModel':$scope.showOthers==true?($scope._editValue._veModel!=undefined?$scope._editValue._veModel:''):$scope._editValue.vehiModel,
    'userIpAddress':localStorage.getItem('ipAddress'),
    'secondaryEngineHours':convertToMillis($scope._editValue._secondaryEngineOn),
    'freezedKm':$scope._editValue._freezedKm,
    'freezedKmDate':$('#freezedKmDate').val()!=undefined?$('#freezedKmDate').val():'',
    'shiftNames':$scope._editValue._shiftNames.toString(),
    'shiftDate':$('#shiftDate').val()!=undefined?$('#shiftDate').val():''
    
})
   .success(function (response) {
      console.log("success");

      location.reload();

  })
   .error(function (response) {
      console.log("fail");
  }); 
}


// $scope._editValue.vehiModel=$scope.showOthers==true?$scope._editValue._veModel:$scope._editValue.vehiModel;

// var URL_ROOT = "vdmVehicles/";
// $http.post( URL_ROOT+'updateLive/'+$scope.vehicleid, {
//     '_token': $('meta[name=csrf-token]').attr('content'),
//     'shortName':$scope._editValue._shortName,
//     'odoDistance': $scope._editValue._odoDista,
//     'overSpeedLimit':$scope._editValue._overSpeed,
//     'driverName': $scope._editValue._driverName,
//     'mobileNo' :$scope._editValue._mobileNo,
//     'driverMobile' :$scope._editValue._driverMobile,
//     'regNo': $scope._editValue._regNo,
//     'vehicleType': vehTyp,
//     'routeName':$scope._editValue.routeName,
//     'vehicleModel':$scope._editValue.vehiModel,
//     'otherVehicleModel':$scope._editValue._veModel,
//     'secondaryEngineHours': convertToMillis($scope._editValue._secondaryEngineOn),
// })
// .success(function(data) {
//             // updateCall();
//             console.log("Success");
//             location.reload();
//         })
// .fail(function() {
//             // updateCall();
//             console.log("Fail");
//         });

}
}

var timeOutVar;  

function setsTimeOuts() {
      //alert('timeOut');
      $("#notifyS").hide(1200);
      $("#notifyF").hide(1200);
      if(timeOutVar!=null){
          //console.log('timeOutVar'+timeOutVar);
          clearTimeout(timeOutVar);
      }
  }

  $scope.updateDebugDays=function(){
    startLoading();
//alert($scope.debugDays);
// if($scope.debugValue=='Enable'&&($scope.debugDays==""||$scope.debugDays==undefined)){
//     //alert('Enter valid days');
//                $('#notifyF').text(translate("Enter valid days")+'...');
//                $('#notifyMsg').show();
//                $("#notifyF").show(500);
//                $("#notifyS").hide();
//                timeOutVar = setTimeout(setsTimeOuts, 2000);
//               stopLoading();
// }
// else{
// if($scope.debugValue=='Enable'&&$scope.debugDays!=""){
//             switch($scope.debugDays){
//             case '1':
//             $('#enablelog span').text('Enabled('+$scope.debugDays+' day)');
//             break;
//             case '7':
//             $('#enablelog span').text('Enabled(1 week)');
//             break;
//             default:
//             $('#enablelog span').text('Enabled('+$scope.debugDays+' days)');
//             break;

//         }
//     }
//     else{
//             $('#enablelog span').text('Disabled');
//         }
if($scope.debugValue=='Enable'){
 $scope.debugDays='1';
 $('#enablelog span').text('Enabled(24 hrs)');
}
else{
    $('#enablelog span').text('Disabled');
}
console.log('vdmVehicles updateDebugDays.....');
$http.post("updateLogDays", {'vehicleId':$scope.vehicleid,'debugValue': $scope.debugValue,'debugDays': $scope.debugDays})
.success(function (response) {
    console.log("success");
    stopLoading();
                //alert(response);
                if((response!='')&&(response=='Debug Enabled successfully')) {
                  console.log(response);
                  $('#notifyS').text(translate("Debug Enabled Successfully")+'...');
                  $('#notifyMsg').show();
                  $("#notifyS").show(500);
                  $("#notifyF").hide();
                  timeOutVar = setTimeout(setsTimeOuts, 2000);
              }
              else if(response=='Debug Disabled successfully'){
                 $('#notifyF').text(translate("Debug Disabled Successfully")+'...');
                 $('#notifyMsg').show();
                 $("#notifyF").show(500);
                 $("#notifyS").hide();
                 timeOutVar = setTimeout(setsTimeOuts, 2000);
                 stopLoading();
             }
             else {
                 $('#notifyF').text(translate("Enter valid days")+'...');
                 $('#notifyMsg').show();
                 $("#notifyF").show(500);
                 $("#notifyS").hide();
                 timeOutVar = setTimeout(setsTimeOuts, 2000);
                 stopLoading();

             }
              //location.reload();
          })
.error(function (response) {
    console.log("fail");
});
//}

stopLoading();

}

$scope.updateSafePark=function(){

    console.log('updateSafePark.....');

    var spVal;
    if($scope.sparkType=='Yes') {
      spVal = 'yes';
      $('#safePark span').text($scope.sparkType);
  } else if($scope.sparkType=='No') {
      spVal = 'no';
      $('#safePark span').text($scope.sparkType);
  }

  var saferParkUrl  = GLOBAL.DOMAIN_NAME+'/configureSafetyParkingAlarm?vehicleId='+ $scope.vehicleid+'&enableOrDisable='+spVal;
//console.log(saferParkUrl);

(function(){

    $.ajax({
      async: false,
      method: 'GET',
      url: saferParkUrl,
      success: function (response){
     //   console.log(response);
     if(response=="success") {
      $('#notifyMsg').show();
      $('#notifyS span').text(translate("Safety Parking Updated SucessFully.")+' !..');
      $("#notifyS").show(500);
      $("#notifyF").hide();
      timeOutVar = setTimeout(setsTimeOuts, 2000);

  } else if(response=="fail") {

      $('#notifyMsg').show();

      $('#notifyS span').text(response+'!..');
      $("#notifyS").show(500);
      $("#notifyF").hide();
      timeOutVar = setTimeout(setsTimeOuts, 2000);

  } else {

      $('#notifyMsg').show();

      $('#notifyF span').text(response);
      $("#notifyF").show(500);
      $("#notifyS ").hide();
      timeOutVar = setTimeout(setsTimeOuts, 2000);
  }
        //$scope.toast = response;
        //toastMsg();
    }
});

})();

}


function saveAddressFunc(val, lat, lan){
    //console.log(val);
    var saveAddUrl = GLOBAL.DOMAIN_NAME+'/saveAddress?address='+encodeURIComponent(val)+'&lattitude='+lat+'&longitude='+lan+'&status=web';
    //console.log(saveAddUrl);

    $http({
        method: 'GET',
        url: saveAddUrl
    }).then(function successCallback(response) {
      if(response.status==200){
          console.log("Save address successfully!..");
      }
  }, function errorCallback(response) {
   console.log(response.status);
});

}

function saveAddress(lat, lon){

    var latlng = new google.maps.LatLng(lat, lon);
    geocoderVar.geocode({'latLng': latlng}, function(results, status) {

      if(status == google.maps.GeocoderStatus.OK) {
        if(results[0]) {
          var newVal = vamoservice.googleAddress(results[0]);  
          saveAddressFunc(newVal, lat, lon);
      }
  }
});

}

function fetchAddress(dataVal){
    if( dataVal.address == '_' || dataVal.address == '-' || dataVal.address == ','  || dataVal.address == ',,' || dataVal.address == ' ' || dataVal.address == null || dataVal.address == undefined ) {
        $scope.getLocation(dataVal.latitude, dataVal.longitude, function(count) {
         $scope.address=count;
     });
    } else {
        $scope.address=(dataVal.address.split('<br>{{ "Address" | translate }}+ ":"')[1] ? dataVal.address.split('<br> {{ "Address" | translate }} +":"')[1] : dataVal.address);
      //saveAddress(dataVal.latitude, dataVal.longitude);
  }
}

$scope.showOthersInput=function(vehimodel){
    if(vehimodel=='Other'){
        $scope.showOthers=true;
    }
    else {
        $scope.showOthers=false;
    }
}


$scope.assignValue = function(dataVal){


    $scope.vehShort  = dataVal.shortName;
    getVehicleData($scope.vehShort);
    $scope.ododis    = dataVal.odoDistance;
    $scope.spLimit   = dataVal.overSpeedLimit;
    $scope.driName   = dataVal.driverName;
    $scope.refname   = dataVal.regNo;
    $scope.vehType   = dataVal.vehicleType;
    $scope.expectedFuelMileage=dataVal.expectedFuelMileage;

    $scope.isDGVehicle = false;
        //alert(dataVal.enableLog);
        //alert(dataVal.logDays);
        

        $('#vehiid #val').text(dataVal.shortName);
        $('#toddist #val').text(dataVal.distanceCovered);
        $('#vehstat #val').text(dataVal.position);
        $('#regNo span').text(dataVal.regNo);
        $('#mobNo span').text(dataVal.mobileNo);
        /* $('#driverMob span').text(dataVal.driverMobile); */
        $('#graphsId #speed').text(dataVal.speed);
        $('#graphsId #fuel').text(dataVal.tankSize);

        if(dataVal.powerStatus !=0 && dataVal.powerStatus !=1 && dataVal.powerStatus !=0.0 && dataVal.powerStatus !=1.0){
          $('#vehBat span').text(dataVal.powerStatus);
      } else if(dataVal.powerStatus==1 && dataVal.powerStatus==1.0) {
          $('#vehBat span').text(translate('ON'));
      } else if(dataVal.powerStatus==0 && dataVal.powerStatus==0.0) {
         $('#vehBat span').text(translate('OFF'));
     }


     if(dataVal.tankSize!=0 && dataVal.fuelLitre!=0){
      tankSize       =  parseInt(dataVal.tankSize);
      fuelLtr        =  parseInt(dataVal.fuelLitre);
  }else if(dataVal.tankSize!=0 && dataVal.fuelLitre==0){
      tankSize       =  parseInt(dataVal.tankSize);
      fuelLtr        =  parseInt(dataVal.fuelLitre);
  }else if(dataVal.tankSize==0 && dataVal.fuelLitre==0){
      tankSize       =  parseInt(dataVal.tankSize);
      fuelLtr        =  parseInt(dataVal.fuelLitre);
  }

  total = parseInt(dataVal.speed);
  sensor = dataVal.noOfTank;
  $scope.vehiclFuel=graphChange(dataVal.fuel); 
        //alert($scope.noOfTank)
        if($scope.noOfTank==1){
               //alert('one')
               speedSize='100%';speedCenter='100%';startangle=-90;endangle=90;
           }
           else if($scope.noOfTank==2){
                //alert('two')
                speedSize='68%';speedCenter='78%';startangle=-150;endangle=150;
            }else {
              speedSize='60%';speedCenter='75%';startangle=-150;endangle=150;
          }

          $('.vehdevtype').text(dataVal.odoDistance);
          $('#mobno #val').text(dataVal.overSpeedLimit);
          $('#positiontime').text(vamoservice.statusTime(dataVal).tempcaption);
          $('#regno #val').text(vamoservice.statusTime(dataVal).temptime);
          $('#driverName #val').text(dataVal.driverName);
          $('#lstseendate').html(new Date(dataVal.date).toString().split('GMT')[0])


          if(dataVal.vehicleMode=='DG'){
             $scope.isDGVehicle = true;
         }




         $scope._editValue._mobileNo     =  dataVal.mobileNo;

         $scope._editValue.gpsSimICCID    =  dataVal.gpsSimICCID; 
         $scope._editValue.vehicleNumber     =  dataVal.vehicleName;      



         fetchAddress(dataVal);



         if($scope.vehiclFuel==true){
          if($scope.noOfTank==1){
            document.getElementById("graphsId").style.width = "360px";
        }
        else {
            if(localStorage.getItem('manualClick')=="yes"){
             fuelGraph();
         }
         if($scope.noOfTank==2){
             document.getElementById("graphsId").style.width = "550px";
         }else if($scope.noOfTank==3){
             document.getElementById("graphsId").style.width = "630px";
         }else if($scope.noOfTank==4){
             document.getElementById("graphsId").style.width = "785px";
         }else if($scope.noOfTank==5){
             document.getElementById("graphsId").style.width = "940px";
         }
     }
 } else {
    document.getElementById("graphsId").style.width = "190px";
}

$('#graphsId').show();

}

$scope.filterExpire = function(data){

   var ret_obj=[];
   angular.forEach(data,function(val, key){
      //console.log(val.expired);
      if(val.expired == "No"){
         ret_obj.push(val);
     }
 });  
   return ret_obj;
}

var obj=null;
var sp;
var vid;
var gname;
if(localStorage.getItem('user')){
    obj = JSON.parse(localStorage.getItem('user'));
}

if(obj!=null){
    sp     =  obj.split(',');
    vid    =  sp[0];
    gname  =  sp[1];
}
if(vid=="null"){
    gname="undefined";
}

$scope.vehUrl = GLOBAL.DOMAIN_NAME+'/getVehicleLocations';
if(gname=="undefined"){

    $scope.vehUrl  = GLOBAL.DOMAIN_NAME+'/getVehicleLocations';
    $http({
        method : "GET",
        url : $scope.vehUrl
    }).then(function mySuccess(response) {
        gname=response.data[0].group;
        initCall();

    });
}
else if(gname!=undefined){
    var grp=[];
    $scope.vehUrl  = GLOBAL.DOMAIN_NAME+'/getVehicleLocations';
    $http({
        method : "GET",
        url : $scope.vehUrl
    }).then(function mySuccess(response) {

      angular.forEach(response.data, function(vals, keys){
          grp.push(vals.group);
      });
      var index=grp.indexOf(gname);
      if(index==-1){
        $scope.vehUrl  = GLOBAL.DOMAIN_NAME+'/getVehicleLocations';
        $http({
            method : "GET",
            url : $scope.vehUrl
        }).then(function mySuccess(response) {
            gname=response.data[0].group;
            initCall();

        });
    }
    else {
        $scope.vehUrl  = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group=' + gname;
        initCall();
    }


});



}
else
{
  $scope.vehUrl  = GLOBAL.DOMAIN_NAME+'/getVehicleLocations';
  initCall();
}



var setIntrvl = setInterval(function() {


    if($.inArray( userName, LandTarr ) > -1) {

      clearInterval(setIntrvl);

  } else {


      $scope.refData          =  true;

    //$scope.groupid          =  0;
    $scope.Filter           =  $scope.vehicleStatus;
      //$scope.marker           =  [];
      //$scope.marker_osm       =  [];
      //$scope.markerLabel      =  [];
    //$scope.zoom             =  6;

     // geocoderVar             =  undefined;
    //  $scope.flightpathall    =  [];
    //  tempdistVal             =  0;
     // $scope.parseInt         =  parseInt;
     // $scope.zohod            =  0;  
     // googSite                =  0;
     // osmSite                 =  0;
     infowindow_osm          =  undefined;
     
     // $scope.hideMe           =  true;
     // $scope.markLab          =  true;

      //$scope.siteFind         =  '';
     // $scope.siteExec         =  false;
     // $scope.siteExec2        =  false;
     // $scope.sitevalu         =  0;
     // siteListInit            =  0;

    //$scope.vehUrl = GLOBAL.DOMAIN_NAME+'/getVehicleLocations';
    $scope.vehUrl = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+$scope.gName;
    initCall2();
    //markerChange_osm($scope.makerType);
}

},120000);


function initCall2() {

   // $scope.$watch("vehUrl", function (val) {

    $http({
        method : "GET",
        url : $scope.vehUrl
    }).then(function mySuccess(response) {

        $scope.data            =  response.data;
         // $scope.data02          =  $scope.filterExpire(response.data[$scope.groupid].vehicleLocations);
         $scope.data02          =  response.data[$scope.groupid].vehicleLocations;
         $scope.groupFuel      =  ($filter('filter')($scope.data02, {'fuel':"yes"}));
         $scope.vehiname        =  response.data[$scope.groupid].vehicleLocations[0].vehicleId;
         $scope.gName           =  response.data[$scope.groupid].group;

         $scope.totalVehicles   =  response.data[$scope.groupid]['totalVehicles'];
         $scope.vehicleOnline   =  response.data[$scope.groupid]['online'];
         $scope.attention       =  response.data[$scope.groupid]['attention'];
         $scope.parkedCount     =  response.data[$scope.groupid]['totalParkedVehicles'];
         $scope.movingCount     =  response.data[$scope.groupid]['totalMovingVehicles'];
         $scope.idleCount       =  response.data[$scope.groupid]['totalIdleVehicles'];

         $scope.companyName          =  response.data[$scope.groupid]['compName']; 
         $scope.gender               =  response.data[$scope.groupid]['gender'];
         $scope.systemName           =  response.data[$scope.groupid]['systemName'];

         // $scope.trafficLayer    =  new google.maps.TrafficLayer();
         // markerSearch           =  new google.maps.Marker({});
         // geocoderVar            =  new google.maps.Geocoder();
         $scope.data1           =  response.data[$scope.groupid].vehicleLocations;
         $scope.locations04     =  [];

         for(i=0;i<$scope.data.length;i++) {

            val = $scope.data[i];
            $scope.locations04.push({rowId:val.rowId,group:val.group,fcode:val.fcode});

            if(typeof $scope.locations04[$scope.groupid] != 'undefined') {
              $scope.locations04[$scope.groupid].vehicleLocations=[];
          }

          $scope.locations04.vehicleLocations = [];
      }

          //console.log($scope.locations04);
            //console.log(map_change);

            if(map_change==0) {

               //console.log('init_google.....');
               document.getElementById("map_osm").style.display="none";
               document.getElementById("maploc").style.display="block";
                 //$scope.initilize('maploc');
                 //markerChange($scope.makerType);
                 $scope.clearMarkers();
                   //$scope.setMarkers2($scope.data02);
                   $scope.setMarkers($scope.data02);

               } else if(map_change==1) {

                //console.log('init_osm.....');
                document.getElementById("maploc").style.display="none";
                document.getElementById("map_osm").style.display="block";

                /*  if(map_osm!=null){
                      map_osm.remove();
                  }  */

                  //$scope.initilize_osm('map_osm');
                  $scope.clearMarkers_osm();
                  $scope.setMarkers_osm($scope.data02);
              }
          }, function myError(response) {
            $scope.myWelcome = response.statusText;
        });
  //});

}    




function initCall() {

   // $scope.$watch("vehUrl", function (val) {

    $http({
        method : "GET",
        url : $scope.vehUrl
    }).then(function mySuccess(response) {

        $scope.data      =  response.data;
        if(response.data[0].isDbDown){
          $scope.dpdown    =  response.data[0].isDbDown;
      }

      if(gname!=undefined){
         angular.forEach($scope.data, function(val, key){
              //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
              var map=val.defaultMap;
              if(map=="osm"){
               map_change          =  1;
               $scope.mapsHist     =  1;
               $scope.maps_name    =  1;
               localStorage.setItem('mapNo',1);
           }
           else {
            map_change          =  0;
            $scope.mapsHist     =  0;
            $scope.maps_name    =  0;
            localStorage.setItem('mapNo',0);
            $('.cMarker').hide();
        }
        if(gname == val.group){

         $scope.groupid = val.rowId;
         $scope.data02    =  response.data[$scope.groupid].vehicleLocations;
         $scope.gName     =  response.data[$scope.groupid].group;
         $scope.groupFuel      =  ($filter('filter')($scope.data02, {'fuel':"yes"}));
                  //alert($scope.groupFuel.length);
                  //alert( $scope.gName );
                  angular.forEach($scope.data[$scope.groupid].vehicleLocations, function(value, keys){
                //$scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});
                if(vid!=undefined){
                  if(vid == value.vehicleId){

                   $scope.vehiname   = value.vehicleId;
                   licenceExpiry=value.licenceExpiry;
                   $scope.noOfTank = value.noOfTank;
                   setDeviceVolt(value)
                         //alert($scope.vehIds);
                     }
                 }
             });
              }

          });


     }  
     else{
          //$scope.data02    =  $scope.filterExpire(response.data[$scope.groupid].vehicleLocations);
          $scope.data02    =  response.data[$scope.groupid].vehicleLocations;
          var map=response.data[0].defaultMap;
          if(map=="osm"){
           map_change          =  1;
           $scope.mapsHist     =  1;
           $scope.maps_name    =  1;
           localStorage.setItem('mapNo',1);
       }
       else {
        map_change          =  0;
        $scope.mapsHist     =  0;
        $scope.maps_name    =  0;
        localStorage.setItem('mapNo',0);
        $('.cMarker').hide();
    }
            //console.log(response.data[$scope.groupid].vehicleLocations[0].vehicleId);

            $scope.vehiname  =  response.data[$scope.groupid].vehicleLocations[0].vehicleId;
            licenceExpiry=response.data[$scope.groupid].vehicleLocations[0].licenceExpiry;
            $scope.noOfTank = response.data[$scope.groupid].vehicleLocations[0].noOfTank;
            $scope.gName     =  response.data[$scope.groupid].group;
        }
        sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
        setDateTimeSessionValues();
        $scope.fcode.push(response.data[$scope.groupid]);
        $scope.totalVehicles   =  response.data[$scope.groupid]['totalVehicles'];
        $scope.vehicleOnline   =  response.data[$scope.groupid]['online'];
        $scope.attention       =  response.data[$scope.groupid]['attention'];
        $scope.parkedCount     =  response.data[$scope.groupid]['totalParkedVehicles'];
        $scope.movingCount     =  response.data[$scope.groupid]['totalMovingVehicles'];
        $scope.idleCount       =  response.data[$scope.groupid]['totalIdleVehicles'];
         // $scope.overspeedCount  =  response.data[$scope.groupid]['topSpeed'];
         $scope.apiKeys         =  response.data[$scope.groupid].apiKey;
         $scope.support         =  response.data[$scope.groupid].supportDetails;
         $scope.trafficLayer    =  new google.maps.TrafficLayer();
         markerSearch           =  new google.maps.Marker({});
         geocoderVar            =  new google.maps.Geocoder();
         $scope.data1           =  response.data[$scope.groupid].vehicleLocations;
         $scope.companyName          =  response.data[$scope.groupid]['compName']; 
         $scope.gender               =  response.data[$scope.groupid]['gender'];
         $scope.systemName           =  response.data[$scope.groupid]['systemName'];
         $scope.locations04     =  [];

         for(i=0;i<$scope.data.length;i++) {

          val = $scope.data[i];
          $scope.locations04.push({rowId:val.rowId,group:val.group,fcode:val.fcode});

          if(typeof $scope.locations04[$scope.groupid] != 'undefined') {
            $scope.locations04[$scope.groupid].vehicleLocations=[];
        }

        $scope.locations04.vehicleLocations = [];
    }

          //console.log($scope.locations04);
            //console.log(map_change);

            if(map_change==0) {

               //console.log('init_google.....');
               document.getElementById("map_osm").style.display="none";
               document.getElementById("maploc").style.display="block";
               $scope.initilize('maploc');
               //markerChange($scope.makerType);
               
           } else if(map_change==1) {

                //console.log('init_osm.....');
                document.getElementById("maploc").style.display="none";
                document.getElementById("map_osm").style.display="block";

                if(map_osm!=null){
                    map_osm.remove();
                }

                $scope.initilize_osm('map_osm');

            }
            var notncount =getParkedIgnitionAlert($scope.data02)[3];
            window.localStorage.setItem('totalNotifications',notncount);
            fuelGraph();
        }, function myError(response) {
            $scope.myWelcome = response.statusText;
        });


var vehicleurl = GLOBAL.DOMAIN_NAME+'/v2/getVehicleData';
$http({
    method : "GET",
    url : vehicleurl
}).then(function mySuccess(response) {

   $scope.vehicleData=response.data.data;

});
var vehiclemodelurl  = GLOBAL.DOMAIN_NAME+'/mobile/v2/getVehicleModelList';
$http({
    method : "GET",
    url : vehiclemodelurl
})
.success(function(vehiclemodel) {
    var modelarray=[];
    modelarray[0]='';
    modelarray.push(...vehiclemodel.data);
    $scope._editValue._vehiModelList =modelarray.sort(function (a, b) {
            return a.localeCompare(b); //using String.prototype.localCompare()
        });
    $scope._editValue._vehiModelList=modelarray;
});
var vehicletypeurl  = GLOBAL.DOMAIN_NAME+'/v2/getVehicleTypeList';
$http({
    method : "GET",
    url : vehicletypeurl
})
.success(function(vehicletype) {
    angular.forEach(vehicletype.data,function(type){
      $scope._editValue._vehiTypeList.push(type.label);
  });
});
}

// // ADDED FOR MULTI SENSOR
// $scope.$watch("noOfTank", function (val) {
//    });

function getFuelTank(){
    var vehicleInfo = $scope.locations04[$scope.groupid].vehicleLocations.find(data => data.vehicleId === $scope.vehicleid); 
    if(vehicleInfo){
        var fuelLitres = vehicleInfo.fuelLitres;
        var nTankSize   = vehicleInfo.nTankSize;
            //console.log('fuelLitres');
            $scope.fuelLitreArr = fuelLitres.split(":");
            $scope.fuelLitreArr = $scope.fuelLitreArr.map(fuel => {
              return parseInt(fuel);
          });
            //console.log($scope.fuelLitreArr);
            $scope.tankSizeArr = nTankSize.split(":");
            $scope.tankSizeArr = $scope.tankSizeArr.map(fuel => {
              return parseInt(fuel);
          });
        } 

    }

    function fuelGraph(){
      getFuelTank();
  //START
  var widthValue=180;
  var center2 ='90%';
  if($scope.noOfTank>2){
      widthValue = 150;
      center2 = '80%';
  }
  var sensorGaugeOptions = {
    chart: {
        type: 'solidgauge',
        height: 100 ,
        width : widthValue,
    },
    credits: { enabled: false },
    title: null,

    pane: {
        center: ['50%',center2],
        size: widthValue+'%',
        startAngle: -90,
        endAngle: 90,
        background: {
            backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#EEE',
            innerRadius: '60%',
            outerRadius: '100%',
            shape: 'arc'
        }
    },

    exporting: {
        enabled: false
    },

    tooltip: {
        enabled: false
    },

    // the value axis
    yAxis: {
        stops: [
            [0.1, '#55BF3B'], // green
            [0.5, '#DDDF0D'], // yellow
            [0.9, '#DF5353'] // red
            ],
            lineWidth: 0,
            tickWidth: 0,
            minorTickInterval: null,
        //tickAmount: 2,
        title: {
            y: -70
        },
        labels: {
            enabled: false
        }
    },

    plotOptions: {
        solidgauge: {
            dataLabels: {
                y: 5,
                borderWidth: 0,
                useHTML: true
            }
        }
    }
};

var chartFuelSensor=[];
for (var i = 0; i < $scope.noOfTank; i++) {
  //alert(i);
    // The speed gauge
    var id='container-fuelSensor'+(i+1);
//     alert(id)
//        if(document.getElementById(id)){
//     alert("Element exists");
// } else {
//     alert("Element does not exist");
// }

chartFuelSensor[i]= $('#'+id).highcharts(Highcharts.merge(sensorGaugeOptions, {
    yAxis: {
        min: 0,
        max: $scope.tankSizeArr[i],
        title: null
    },

    credits: {
        enabled: false
    },

    series: [{
        name: 'Fuel',
        data: [$scope.fuelLitreArr[i]],
        dataLabels: {
            format:
            '<div style="text-align:center"><span style="font-size:12px; font-weight:normal;color: #196481'+ '">Fuel - {y} Ltr</span><br/>'

        },
        tooltip: {
            valueSuffix: ' km/h'
        }
    }]

}))
}

$('#container-speed').highcharts({

  chart: {
      type: 'gauge',
      plotBackgroundColor: null,
      plotBackgroundImage: null,
      plotBorderWidth: 0,
      plotShadow: false,
      spacingBottom: 10,
      spacingTop: -60,
      spacingLeft: -20,
      spacingRight: -20,
  },

  title: {
      text: ''
  },

  pane: {
          // startAngle: -150,
          // endAngle: 150,
          // // startAngle: -90,
          // // endAngle: 90,
          // center:['50%', '78%'],
          // size: '68%',
          startAngle: startangle,
          endAngle: endangle,
          center:['50%', speedCenter],
          size: speedSize,
          background: [{
              backgroundColor: {
                  linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                  stops: [
                  [0, '#FFF'],
                  [1, '#333']
                  ]
              },
              borderWidth: 0,
              outerRadius: '109%'
          }, {
              backgroundColor: {
                  linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                  stops: [
                  [0, '#333'],
                  [1, '#FFF']
                  ]
              },
              borderWidth: 1,
              outerRadius: '107%'
          }, {
              // default background
          }, {
              backgroundColor: '#DDD',
              borderWidth: 0,
              outerRadius: '105%',
              innerRadius: '103%'
          }]
      },
      credits: { enabled: false },
      // the value axis
      yAxis: {
          min: 0,
          max: 200,

          minorTickInterval: 'auto',
          minorTickWidth: 1,
          minorTickLength: 10,
          minorTickPosition: 'inside',
          minorTickColor: '#666',

          tickPixelInterval: 30,
          tickWidth: 2,
          tickPosition: 'inside',
          tickLength: 10,
          tickColor: '#666',
          labels: {
              step: 2,
              rotation: 'auto'
          },
          title: {
              // text: 'km/h'
          },
          plotBands: [{
              from: 0,
              to: 120,
              color: '#55BF3B' // green
          }, {
              from: 120,
              to: 160,
              color: '#DDDF0D' // yellow
          }, {
              from: 160,
              to: 200,
              color: '#DF5353' // red
          }]        
      },

      series: [{
          name: 'Speed',
          data: [total],
          tooltip: {
              valueSuffix: ' km/h'
          }
      }]

  });


  //END

}


  // Bring life to the dials
  setInterval(function () {
      var point;
      if($scope.noOfTank>1){

       getFuelTank();
       for (var i = 0; i < $scope.noOfTank ; i++) {
           // alert('cxvf');
           var fuelId='#container-fuelSensor'+(i+1);
           var chartFuelSensor = $(fuelId).highcharts(), point;
           if (chartFuelSensor) {
              chartFuelSensor.yAxis[0].update({max:$scope.tankSizeArr[i]});
              point = chartFuelSensor.series[0].points[0];
                  //console.log($scope.fuelLitreArr[i])
                  point.update(parseInt($scope.fuelLitreArr[i]));
              }
          }
      }
      
  },1000);

  setInterval(function () {
      var chart = $('#container-speed').highcharts(), point,options;
      if (chart) {
        point = chart.series[0].points[0];
        point.update(total);
    }

}, 1000);



  angular.element(function () {
   // console.log('page loading completed');
   if(localStorage.getItem('selectedVehicleId') != null)
   {
        //$("#id_"+localStorage.getItem('selectedVehicleId')).click();
        var vehicleID = localStorage.getItem('selectedVehicleId').split("__");
        $scope.genericFunction(vehicleID[0], vehicleID[1]);
    }
});

  $scope.$on('$viewContentLoaded', function(){
    //Here your view content is fully loaded !!
    if(localStorage.getItem('selectedVehicleId') != null)
    {
       // $("#id_"+localStorage.getItem('selectedVehicleId')).click();
       var vehicleID = localStorage.getItem('selectedVehicleId').split("__");
       $scope.genericFunction(vehicleID[0], vehicleID[1]);
   }

});

  $scope.getZoho    =  GLOBAL.DOMAIN_NAME+'/getZohoInvoice';
  $scope.getRoutes  =  GLOBAL.DOMAIN_NAME+'/getRouteList';
  $scope.getValueCheck = function(getStatus){

        // if($scope.intervalValue){
        //    clearInterval($scope.intervalValue);
        // }

        localStorage.setItem('manualClick','no');
        $scope.getValue = getStatus;

        if($scope.getValue == 'YES') {
            if(($.inArray( userName, LandTarr ) > -1)||(userName=="IGT")) {
                landtDraw();}
                if(map_change==0){
                   polygenFunction($scope.data);
               } else if(map_change==1){
                 polygenFunction($scope.data);
             }
         } else if ($scope.getValue == 'NO') {
          //alert('hello');

          siteclear();
                //map_osm.removeLayer($scope.polygonOsm);

            }
        }
  // polygen draw function
  function polygenFunction(getVehicle){
    //console.log(' getVehicle ')
    var polygenOrgs   =  [];
    var unique        =  new Set();
    if($scope.vehiname!="null"){
        polygenOrgs       =  ($filter('filter')(getVehicle[$scope.groupid].vehicleLocations, {'live': 'yes','vehicleId':$scope.vehiname}));
    }
    else
    {
      polygenOrgs       =  ($filter('filter')(getVehicle[$scope.groupid].vehicleLocations, {'live': 'yes'}));
  }
  for (var i=0; polygenOrgs.length > i; i++) {
      unique.add(polygenOrgs[i].orgId)
  };

  if(unique.size>0){
      $scope._addPoi  =   false;
      angular.forEach(unique, function(value, key) {
        //service call to site details
        siteInvoke(value);
    });
  }
  else {
      // $scope._addPoi   =   true;
      siteclear();
      siteInvoke();
  }
}

$scope.markerPolyss   =  [];
$scope.markerPolyss2  =  [];
$scope.markerPolysG   =  [];
$scope.site_list      =  [];

function polygonDraw_osm(data) {

    var sp_osm,myLatlngs;
    polygonList2 = [];

    var splits   = data.latLng.split(",");

    for(var i=0; splits.length>i; i++) {
     sp_osm    = splits[i].split(":");
     myLatlngs = new L.LatLng(sp_osm[0], sp_osm[1]);
     polygonList2.push(myLatlngs);
 }
 if($.inArray( data.siteName,Object.getOwnPropertyNames(osm_mapsDetails)) ==-1 ) {
    $scope.polygonOsm = L.polygon(polygonList2,{ className: 'polygon_osm' }).addTo(map_osm).bindPopup(data.siteName);
    osm_mapsDetails[data.siteName] = $scope.polygonOsm;
    $scope.markerPolyss.push($scope.polygonOsm);
}

if(myLatlngs) {
    var markerPoly = new L.marker(myLatlngs);

    if(siteListInit==0){
     $scope.site_list.push({'sName' : data.siteName});
 }

 markerPoly.setIcon($scope.iconPoly).bindLabel(data.siteName, { noHide: true, className: 'polygonLabel', clickable: true, direction:'auto' });
 $scope.markerPolyss2.push(markerPoly);  
 markerPoly.addTo(map_osm);
}

}


//draw polygen in map function
function polygenDrawFunction(list,key){

  var sp;
  polygenList  =  [];
  var split    =  list.latLng.split(",");

  for(var i = 0; split.length>i; i++){
      sp    = split[i].split(":");
      polygenList.push(new google.maps.LatLng(sp[0], sp[1]));
  }

  if(siteListInit==0){
      $scope.site_list.push({'sName' : list.siteName});
  }

  var labelAnchorpos = new google.maps.Point(19, 0);
  if($.inArray( list.siteName,Object.getOwnPropertyNames(_mapsDetails)) ==-1 ) {
      var polygenColor = new google.maps.Polygon({
        path: polygenList,
            strokeColor: "#000",//7e7e7e
            strokeWeight: 0.7,
            fillColor: colorChange(list.siteName),//'#' + Math.floor(Math.random()*16777215).toString(16),//'#fe716d',
          //fillOpacity: ,
          map: map_goog
      });
      _mapsDetails[list.siteName] = polygenColor;
  }


  $scope.markerss = new MarkerWithLabel({
   position: centerMarker(polygenList),
   map: map_goog,
   icon: " ",
   labelContent: list.siteName,
   labelAnchor: new google.maps.Point(19, 0),
   labelClass: "labelsP",
});

  $scope.markerPolysG.push($scope.markerss);

      //if($scope.siteListLen==key){
        if($scope.refData==undefined){
          if(localStorage.getItem('manualClick')!='yes'){
            map_goog.setCenter(centerMarker(polygenList));
            map_goog.setZoom(14);  
        }
        //}
    }

}

function centerMarker(listMarker){
    var bounds = new google.maps.LatLngBounds();
    for (i = 0; i < listMarker.length; i++) {
      bounds.extend(listMarker[i]);
  }
  return bounds.getCenter()
}


function colorChange(value){
  var color ='';
  switch(value){
    case 'Virugambakkam' :
    color   = 'c17b97';
    break;
    case 'Kolathur' :
    color   = 'f76c7c';
    break;
    case 'Egmore(SC)' :
    color   = 'e746bc';
    break;
    case 'Thiyagaraya_Nagar' :
    color   = '277f07';
    break;
    case 'Saidapet' :
    color   = 'fad195';
    break;
    case 'Dr_Radhakrishnan_Nagar':
    color   = '28909c';
    break;
    case 'Perambur' :
    color   = 'f381a7';
    break;
    case 'Chepauk_Thiruvallikeni':
    color   = 'a05071';
    break;
    case 'Thiru_Vi_Ka_Nagar_(SC)' :
    color   = '3d59be';
    break;
    case 'Harbour' :
    color   = 'b28d53';
    break;
    case 'Royapuram' :
    color   = '98beb6';
    break;
    case 'Mylapore' :
    color   = '84c8b6';
    break;
    case 'Velachery' :
    color   = '6f738d';
    break;
    case 'Thousand_Lights':
    color   = '456d4d';
    break;
    case 'Anna_Nagar' :
    color   = 'aca6b7';
    break;
    case 'Villivakkam':
    color   = 'f22af0';
    break;
    default:
    color   = 'f22af0';
    break;
}
return '#'+color;

}


function siteInvoke(val) {

  var url_site  =  GLOBAL.DOMAIN_NAME+'/viewSite';

  vamoservice.getDataCall(url_site).then(function(data) {

       // console.log(data);
       if(data.siteParent && $scope._addPoi == false){

        $scope.siteExec  = true;

        if(map_change==0){
          angular.forEach(data.siteParent, function(value, key){
          //console.log(' value'+key);
          if(val == value.orgId){

            $scope.siteListLen = value.site.length-1;
            angular.forEach(value.site, function(vals, keys){
              //console.log('inside the for loop');
              polygenDrawFunction(vals,keys);
          });

            if(value.location.length>0){
                angular.forEach(value.location, function(locs, ind){
                   locat_address(locs);
               });
            }
        }
    });

          siteListInit=1;

      } else if(map_change==1){

         $scope.iconPoly = L.icon({
           iconUrl: 'assets/imgs/trans.png',
           iconAnchor:[0,0],
           labelAnchor: [-15,10],
       });

         angular.forEach(data.siteParent, function(value, key){
           //console.log(' value'+key);
           if(val == value.orgId){
              angular.forEach(value.site, function(vals, keys){
              //console.log('inside the for loop');
              polygonDraw_osm(vals);
          });

              if($scope.polygonOsm){
                  if($scope.refData==undefined){
                    if(localStorage.getItem('manualClick')!='yes'){
                      map_osm.fitBounds($scope.polygonOsm.getBounds());
                  }
              }
          }  

             /* if(value.location.length>0){
                  angular.forEach(value.location, function(locs, ind){
                     locat_address(locs);
                  });
              } */
          }
      });

         siteListInit=1;

     }
 }

 if(data && data.orgIds != undefined){
    $scope.orgIds   = data.orgIds;
}

});

}


function calcLatLongForDrawShapes(longitude, lat, distance, bearing) {
   var EARTH_RADIUS_EQUATOR  =  6378140.0;
   var RADIAN                =  180 / Math.PI;

   var b    =  bearing / RADIAN;
   var lon  =  longitude / RADIAN;
   var lat  =  lat / RADIAN;
   var f    =  1/298.257;
   var e    =  0.08181922;

   var R       =  EARTH_RADIUS_EQUATOR * (1 - e * e) / Math.pow( (1 - e*e * Math.pow(Math.sin(lat),2)), 1.5);  
   var psi     =  distance/R;
   var phi     =  Math.PI/2 - lat;
   var arccos  =  Math.cos(psi) * Math.cos(phi) + Math.sin(psi) * Math.sin(phi) * Math.cos(b);
   var latA    =  (Math.PI/2 - Math.acos(arccos)) * RADIAN;

   var arcsin  =  Math.sin(b) * Math.sin(psi) / Math.sin(phi);
   var longA   =  (lon - Math.asin(arcsin)) * RADIAN;

   return latA+':'+longA;
};



  //create save site
  $scope.markPoi   =   function(textValue, latlanList) {



    $scope.toast    = '';
    if(checkXssProtection(textValue) == true)
        try
    {

      var URL_ROOT    = "AddSiteController/";    /* Your website root URL */
      var text        = textValue;
      var drop        = 'Home Site';
      //alert($scope.orgID);
      var org         = $scope.orgID;

      // post request
      if(text && drop && latlanList.length>=3 && org)
      {
       $http.post(URL_ROOT+'store', {'siteName': text, 'siteType': drop, 'org':org, 'latLng': latlanList})
       .success(function (response) {

          stopLoading();

          if((response!='')&&(response=='Sucess')) {
            $scope.orgID="";
            $scope.poiName="";
            console.log(response);
            $('#notifyS').text(translate("Successfully updated")+'!..');
            $('#notifyMsg').show();
            $("#notifyS").show(500);
            $("#notifyF").hide();
            timeOutVar = setTimeout(setsTimeOuts, 2000);
        }
        else if(response=='site already present'){
           $('#notifyF').text(translate("Site Already Present"));
           $('#notifyMsg').show();
           $("#notifyF").show(500);
           $("#notifyS").hide();
           timeOutVar = setTimeout(setsTimeOuts, 2000);
           stopLoading();
       }
       else if(response=='Send correct data'){
           $('#notifyF').text(translate("Enter Site Name"));
           $('#notifyMsg').show();
           $("#notifyF").show(500);
           $("#notifyS").hide();
           timeOutVar = setTimeout(setsTimeOuts, 2000);
           stopLoading();
       }
       else {

           $('#notifyF').text(translate("Enter Valid Site Name"));
           $('#notifyMsg').show();
           $("#notifyF").show(500);
           $("#notifyS").hide();
           timeOutVar = setTimeout(setsTimeOuts, 2000);
       }

   }) .error(function (response) {
    console.log("fail");
    stopLoading();
});

} else {
  if(!org){
    $('#notifyF').text( translate('Please select organisation name'));
}else{
    $('#notifyF').text( translate('Enter Site Name'));
}
$('#notifyMsg').show();
$("#notifyF").show(500);
$("#notifyS").hide();
timeOutVar = setTimeout(setsTimeOuts, 2000);
stopLoading();
}

} catch (err)    {
 $('#notifyF span').text(translate("Enter all the field / Mark the Site"));
 $('#notifyMsg').show();
 $("#notifyF").show(500);
 $("#notifyS").hide();
 timeOutVar = setTimeout(setsTimeOuts, 2000);
 stopLoading();
}

stopLoading();

}

  //split methods
  $scope.split_fcode = function(fcode){
    var str = $scope.fcode[0].group;
    var strFine = str.substring(str.lastIndexOf(':'));
    while(strFine.charAt(0)===':')
        strFine = strFine.substr(1);
    return strFine;
}

$scope.getLoad   = function(vehicleId){
  startLoading();
    //alert(vehicleId);
    //var loadUrl ='209.97.163.4:9000/getReportDataForGentrax?userId=MSS&vehicleId=HEADCOUNT';
    var loadUrl = GLOBAL.DOMAIN_NAME+'/getReportDataForGentrax?vehicleId='+vehicleId;

    $http({
        method : "GET",
        url : loadUrl
    }).then(function mySuccess(response) {
            //console.log(response.data);
            stopLoading();
            //alert(response.data);
            $scope.loadData=response.data;

        }, function myError(response) {

            console.log( response.statusText );
        });

}

var modalss = document.getElementById('poi');
var spanss  = document.getElementsByClassName("poi_close")[0];

function popUp_Open_Close(){

    modalss.style.display = "block";
    modalss.style.zIndex= 9999;
    spanss.onclick = function() {
        modalss.style.display = "none";
    }
}  


$scope.addPoi   = function(lat, lng){
    $scope.orgID="";
    $scope.poiName="";
    $scope.getOrgId             =   GLOBAL.DOMAIN_NAME+'/viewSite';
    $http.get($scope.getOrgId).success(function(response){
      console.log(response);
      if(response){
          $scope.organsIds = response.orgIds;
          $scope.orgIds = $scope.organsIds[0];
      }
      $scope.poiLat = lat;
      $scope.poiLng = lng;
      popUp_Open_Close();
  });
}



$scope.submitPoi  = function(poiName){

    var width       = 1000;
    var latlngList  = [];

    if(map_change==0){

        if(map_goog.getZoom()>5 && map_goog.getZoom()<=8){
          width = 1000;
      }
      else if(map_goog.getZoom()>8 && map_goog.getZoom()<=12){
          width = 100;
      }
      else if(map_goog.getZoom()>12 && map_goog.getZoom()<=15){
          width = 10;
      }
      else if(map_goog.getZoom()>15){
          width = 1;
      }

  } else if(map_change==1){

      var zoomValue = map_osm.getZoom();
      //console.log(zoomValue);

      if(zoomValue>5 && zoomValue<=8){
          width = 1000;
      }
      else if(zoomValue>8 && zoomValue<=12){
        width = 100;
    }
    else if(zoomValue>12 && zoomValue<=15){
        width = 10;
    }
    else if(zoomValue>15){
        width = 1;
    }
}

var radius = (Math.sqrt (2 * (width * width))) / 2;
    //alert(radius);
    latlngList[0] = calcLatLongForDrawShapes($scope.poiLng, $scope.poiLat, radius, 45)
    latlngList[1] = calcLatLongForDrawShapes($scope.poiLng, $scope.poiLat, radius, -45)
    latlngList[2] = calcLatLongForDrawShapes($scope.poiLng, $scope.poiLat, radius, -135)
    latlngList[3] = calcLatLongForDrawShapes($scope.poiLng, $scope.poiLat, radius, 45)

    //console.log(latlngList);
    $scope.markPoi(poiName, latlngList)
    modalss.style.display = "none";

}

var infoBoxs  = [];
var infoBoxs2 = [];

function circleData(data) {

    var circData = data.circlePoints;  
    var rectData = data.rectanglePoints;

    if(circData) {

       for(var i=0; i<circData.length; i++) {

             //console.log( circData[i] );

             var valuSplt  =  circData[i].location.split(",");
             var latCircs  =  valuSplt[0];
             var lngCircs  =  valuSplt[1];

             var spltLocName = circData[i].locationName.split(":");

             var spltLocVals='';

             if(spltLocName.length==2){

                 spltLocVals = spltLocName[1];
             } else {

                var sptVals = circData[i].locationName.split("LAN-");

                if(sptVals.length==2){

                    spltLocVals = sptVals[1];

                } else {

                    spltLocVals = circData[i].locationName;
                }

            }



            if( osmSite == 1 ) {
              //console.log('...'+latCircs+'...'+lngCircs+'...');
              var circls2 = new  L.circleMarker([latCircs, lngCircs], {
                color: 'transparent',
                fillColor: '#f03',
                fillOpacity: 0,
                radius: 50
            }).bindLabel(spltLocVals, { noHide: true, className : "markerLabels" }).addTo(map_osm);

              var circls = new  L.circle([latCircs, lngCircs], 50,{
                color: 'red',
                fillColor: '#f03',
                fillOpacity: 0,
                radius: 1
            }).addTo(map_osm);

          } else {

            var circlClass = new google.maps.Circle({
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0,
                map: map_goog,
                center: new google.maps.LatLng(latCircs,lngCircs),
                radius: 50
            });

            var myOpts = {
                content: spltLocVals,
                boxStyle: {
                  background: '#FFFFFF',
                  color: '#000000',
                  textAlign: "center",
                  fontSize: "8pt",
                  fontWeight:"bold",
                  width: "60px",
                  border:"0.5px solid black",
                  padding:"2px 2px 2px 2px",
                  borderRadius:"6px"
              },
              disableAutoPan: true,
                pixelOffset: new google.maps.Size(-25, -10), // left upper corner of the label
                position: new google.maps.LatLng(latCircs,lngCircs),
                closeBoxURL: "",
                isHidden: true,
                pane: "floatPane",
                zIndex: 100,
                enableEventPropagation: true
            };

            var ibs = new InfoBox(myOpts);

            infoBoxs2.push(ibs);
            ibs.open(map_goog);

        }

    }    
}

if(rectData!=undefined&&rectData.length!=undefined && rectData.length !=0) {

    lastArr = "";
    prevLoc = "";

    for(var i=0; i<rectData.length; i++) {
        //console.log(rectData[i]);
        var secArr = rectData[i];            
        //console.log(secArr.length);
        var secArrLast = secArr.length-1;
        var polygenList = [], circleList  = [], polygenList1 = [];

        for(var j=0; j<secArr.length; j++){

            if(secArr[j].location!=""){

            //console.log(secArr[j]);
            var valueSplit = secArr[j].location.split(",");
            var latCirc    = valueSplit[0];
            var lngCirc    = valueSplit[1];

            var splitLocNam = secArr[j].locationName.split(":");

            var spltLocVal='';

            if(splitLocNam.length==2){

             spltLocVal = splitLocNam[1];

         } else {

             var sptVal = secArr[j].locationName.split("LAN-");

             if(sptVal.length==2){

               spltLocVal = sptVal[1];

           } else {

               spltLocVal = secArr[j].locationName;
           }


       }


       if(osmSite == 1) {

           var circle1 = new  L.circleMarker([latCirc, lngCirc], {
            color: 'transparent',
            fillColor: '#f03',
            fillOpacity: 0,
            radius: 50
        }).bindLabel(spltLocVal, { noHide: true, className : "markerLabels" }).addTo(map_osm);

           var circle = new  L.circle([latCirc, lngCirc], 50,{
            color: 'red',
            fillColor: '#f03',
            fillOpacity: 0,
            radius: 1
        }).addTo(map_osm);
                //circle.bindTooltip(secArr[j].locationName,{permanent:true,sticky:true}).openTooltip();
                circleList.push(circle);
                if(j==0) {
                  polygenList.push(  [circleList[0].getBounds().getSouthWest().lat+0.0001,circleList[0].getBounds().getCenter().lng+0.002] ,
                    [circleList[0].getBounds().getCenter().lat+0.002,circleList[0].getBounds().getSouthWest().lng+0.0001]  );
              } else if(j==secArrLast) {
                //console.log(secArrLast);
                polygenList.push( [ circleList[secArrLast].getBounds().getNorthEast().lat+0.0001,circleList[secArrLast].getBounds().getCenter().lng-0.002 ],
                    [ circleList[secArrLast].getBounds().getCenter().lat-0.002,circleList[secArrLast].getBounds().getNorthEast().lng+0.0001 ] );
            }

            if(j==1 && lastArr!="" && prevLoc == secArr[j].state)
            {
              polygenList1.push([ circleList[1].getBounds().getSouthWest().lat+0.0001,circleList[1].getBounds().getCenter().lng+0.002 ],
                [ circleList[1].getBounds().getCenter().lat+0.002,circleList[1].getBounds().getSouthWest().lng+0.0001 ]);

              polygenList1.push([ lastArr.getBounds().getNorthEast().lat+0.0001,lastArr.getBounds().getCenter().lng-0.002 ],
                [ lastArr.getBounds().getCenter().lat-0.002,lastArr.getBounds().getNorthEast().lng+0.0001 ])
          }
          if(j==secArrLast)
          {
              lastArr = circleList[secArrLast-1];
              prevLoc = secArr[j].state;

          }
      }
      else {

          var circleClass = new google.maps.Circle({
            strokeColor: '#FF0000',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: '#FF0000',
            fillOpacity: 0,
            map: map_goog,
            center: new google.maps.LatLng(latCirc,lngCirc),
            radius: 50
        });

          var myOptions = {
            content: spltLocVal,
            boxStyle: {
              background: '#FFFFFF',
              color: '#000000',
              textAlign: "center",
              fontSize: "8pt",
              fontWeight:"bold",
              width: "60px",
              border:"0.5px solid black",
              padding:"2px 2px 2px 2px",
              borderRadius:"6px"
          },
          disableAutoPan: true,
                pixelOffset: new google.maps.Size(-25, -10), // left upper corner of the label
                position: new google.maps.LatLng(latCirc,lngCirc),
                closeBoxURL: "",
                isHidden: true,
                pane: "floatPane",
                zIndex: 100,
                enableEventPropagation: true
            };

            var ib = new InfoBox(myOptions);

            infoBoxs.push(ib);
            ib.open(map_goog);

            circleList.push(circleClass);

             //console.log(j);
            // if(osmSite == 1)
             // {
             // }
             // else{
                if(j==0) {
                    polygenList.push( new google.maps.LatLng( circleList[0].getBounds().getSouthWest().lat()+0.0001,circleList[0].getCenter().lng()+0.002 ),
                      new google.maps.LatLng( circleList[0].getCenter().lat()+0.002,circleList[0].getBounds().getSouthWest().lng()+0.0001 ) );
                } else if(j==secArrLast) {
              //console.log(secArrLast);
              polygenList.push( new google.maps.LatLng( circleList[secArrLast].getBounds().getNorthEast().lat()+0.0001,circleList[secArrLast].getCenter().lng()-0.002 ),
                  new google.maps.LatLng( circleList[secArrLast].getCenter().lat()-0.002,circleList[secArrLast].getBounds().getNorthEast().lng()+0.0001 ) );
          }

          if(j==1 && lastArr!="" && prevLoc == secArr[j].state)
          {
            polygenList1.push(new google.maps.LatLng( circleList[1].getBounds().getSouthWest().lat()+0.0001,circleList[1].getCenter().lng()+0.002 ),
              new google.maps.LatLng( circleList[1].getCenter().lat()+0.002,circleList[1].getBounds().getSouthWest().lng()+0.0001 ));

            polygenList1.push(new google.maps.LatLng( lastArr.getBounds().getNorthEast().lat()+0.0001,lastArr.getCenter().lng()-0.002 ),
              new google.maps.LatLng( lastArr.getCenter().lat()-0.002,lastArr.getBounds().getNorthEast().lng()+0.0001 ))
        }
        if(j==secArrLast)
        {
            lastArr = circleList[secArrLast-1];
            prevLoc = secArr[j].state;

        }
    }
              //}
             /* if(j==0) {
                polygenList.push( new google.maps.LatLng( circleList[0].getBounds().getSouthWest().lat(),circleList[0].getCenter().lng() ),
                  new google.maps.LatLng( circleList[0].getCenter().lat(),circleList[0].getBounds().getSouthWest().lng() ) );
              } else */
             /**** if(j==secArrLast) {
              //console.log(secArrLast);
                polygenList.push( new google.maps.LatLng( circleList[secArrLast].getBounds().getSouthWest().lat()+0.0001,circleList[0].getCenter().lng()+0.002 ),
                  new google.maps.LatLng( circleList[0].getCenter().lat()+0.002,circleList[0].getBounds().getSouthWest().lng()+0.0001 ) );

                polygenList.push( new google.maps.LatLng( circleList[0].getBounds().getNorthEast().lat()+0.0001,circleList[0].getCenter().lng()-0.002 ),
                  new google.maps.LatLng( circleList[0].getCenter().lat()-0.002,circleList[0].getBounds().getNorthEast().lng()+0.0001 ) );
              }*/ /*else {
                console.log('else');
                  polygenList.push( new google.maps.LatLng( circleList[j].getBounds().getNorthEast().lat(),circleList[j].getCenter().lng()),
                  new google.maps.LatLng( circleList[j].getBounds().getSouthWest().lat(), circleList[j].getCenter().lng() ) );
              }*/
          }
      }


        //console.log( osmSite );

        if(osmSite == 1)
        {
            var polygon = L.polygon(polygenList,{color: '#FFFF00',
              fillColor: '#f03',
              fillOpacity: 0
          }).addTo(map_osm);
            var polygon = L.polygon(polygenList1,{color: '#BDD7EE',
              fillColor: '#f03',
              fillOpacity: 0
          }).addTo(map_osm);
        }
        else{
            var polygon = new google.maps.Polygon({
              path: polygenList,
                  strokeColor: "#FFFF00",//7e7e7e
                  strokeWeight: 3,
                  fillColor: "#000",
                  fillOpacity: 0,
                  map: map_goog
              });

            var polygon = new google.maps.Polygon({
              path: polygenList1,
                  strokeColor: "#BDD7EE",//7e7e7e
                  strokeWeight: 3,
                  fillColor: "#000",
                  fillOpacity: 0,
                  map: map_goog
              });
        }
    }

}


stopLoading();
}

function landtDraw() {
    startLoading();
      //var landtVar = 'http://188.166.244.126:9000/viewSiteForAssetLandt?userId=LANDT';
      var landtVar = GLOBAL.DOMAIN_NAME+'/viewSiteForAssetLandt';

      $http({
        method : "GET",
        url : landtVar
    }).then(function mySuccess(response) {
            //console.log(response.data);
            circleData(response.data);

        }, function myError(response) {
            console.log( response.statusText );
        });

}

$scope.initilize_osm = function(ID){
 if($scope.customZoom != 0) {
     $scope.zoom  = $scope.customZoom;
 }
 var mapLink  =  '<a href="https://osm.vamosys.com/nominatim/lf.html">OpenStreeetMap</a>'
 map_osm  =  new L.map('map_osm',{ center: [$scope.data[$scope.groupid]['latitude'], $scope.data[$scope.groupid]['longitude']],/* minZoom: 4,*/zoom: $scope.zoom });

 new L.tileLayer('https://osm.vamosys.com/osm_tiles/{z}/{x}/{y}.png', {
   attribution: '&copy; '+mapLink+' Contributors',
          // maxZoom: 18,
      }).addTo(map_osm);

 osmSite = 1;
 $scope.setMarkers_osm($scope.data02);

      //  if(initValOsm==0){
         //initValOsm++;



         if($scope.SiteCheckbox.value1=='YES'){
             if(($.inArray( userName, LandTarr ) > -1)||(userName=="IGT")) {
                landtDraw();}
                polygenFunction($scope.data);

            }

       //}

       map_osm.on('zoomend', function() {
          //alert(map_osm.getZoom());
          var zoomValue = map_osm.getZoom();
          $scope.customZoom = parseFloat(zoomValue);
      });
       map_osm.on('dragend', function() {
        var mapCentre=map_osm.getCenter();
        var mapLat = mapCentre.lat;
        var mapLng = mapCentre.lng
        $scope.positionLng = parseFloat(mapLng);
        $scope.positionLat = parseFloat(mapLat);  
    });

       stopLoading();
   }

   $scope.initilize = function(ID){

       googleCall();

       if($scope.customZoom != 0) {
         $scope.zoom  = $scope.customZoom;
     }
     var mapOptions={
      zoom: $scope.zoom,
      zoomControlOptions:{position: google.maps.ControlPosition.LEFT_TOP},
      center: new google.maps.LatLng($scope.data[$scope.groupid]['latitude'], $scope.data[$scope.groupid]['longitude']),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
  };

  map_goog = new google.maps.Map(document.getElementById(ID), mapOptions);

  $(document).on('pageshow', '#maploc', function(e){      
      google.maps.event.trigger(document.getElementById('maploc'), "resize");
  });

  googSite = 1;
  osmSite=0;
  $scope.setMarkers($scope.data02);
        //console.log(userName);

       // if(initValGoog==0){
         // initValGoog++;

         if($scope.SiteCheckbox.value1=='YES'){
             if(($.inArray( userName, LandTarr ) > -1)||(userName=="IGT")) {
                landtDraw();}
                polygenFunction($scope.data);

            }


     // }

     stopLoading();

     if(osmSearch==1){
         document.getElementById("searchOsm").style.display="none";
         osmSearch=0;
     }

     input_value  =  document.getElementById('pac-inputs');
     sbox         =  new google.maps.places.SearchBox(input_value);

  // search box function
  sbox.addListener('places_changed', function() {
    markerSearch.setMap(null);
    var places = sbox.getPlaces();

    document.getElementById("pac-inputs").value = "";

    markerSearch = new google.maps.Marker({
     position: new google.maps.LatLng(places[0].geometry.location.lat(), places[0].geometry.location.lng()),
     animation: google.maps.Animation.BOUNCE,
     map: map_goog,
 });

    //  console.log(' lat lan  '+places[0].geometry.location.lat(), places[0].geometry.location.lng())
    map_goog.setCenter(new google.maps.LatLng(places[0].geometry.location.lat(), places[0].geometry.location.lng()));
    map_goog.setZoom(13);

});

  map_goog.addListener('zoom_changed', function() {
      var zoomVal = map_goog.getZoom();
      $scope.customZoom = parseFloat(zoomVal);
  });

  map_goog.addListener('dragend', function() {
      var mapCentre=map_goog.getCenter();
      var mapLat=mapCentre.lat();
      var mapLng=mapCentre.lng();
      $scope.positionLng = parseFloat(mapLng);
      $scope.positionLat = parseFloat(mapLat);  
  });

  google.maps.event.addListener(map_goog, 'click', function(event) {
    if($scope.clickflag==true){
      if($scope.clickflagVal ==0){
        $scope.firstLoc = event.latLng;
        $scope.clickflagVal =1;
    }else if($scope.clickflagVal==1){
        $scope.drawLine($scope.firstLoc, event.latLng);
        $scope.firstLoc = event.latLng;
    }
}else if($scope.nearbyflag==true){
          // $('#status02').show();
          // $('#preloader02').show();
          var tempurl = GLOBAL.DOMAIN_NAME+'/getNearByVehicles?lat='+event.latLng.lat()+'&lng='+event.latLng.lng();

          $http.get(tempurl).success(function(data){
            $scope.nearbyLocs = data;
            // $('#status02').fadeOut();
            // $('#preloader02').delay(350).fadeOut('slow');
            if($scope.nearbyLocs.fromAddress==''){}else{
              $('.nearbyTable').delay(350).show();
          }
      });
      }
  });
  loadMapState()

};

function tilesLoaded() {
    google.maps.event.clearListeners(map_goog, 'tilesloaded');
    google.maps.event.addListener(map_goog, 'zoom_changed', saveMapState);
    google.maps.event.addListener(map_goog, 'dragend', saveMapState);
}  

// functions below

function saveMapState() {
    var mapZoom=map_goog.getZoom();
    var mapCentre=map_goog.getCenter();
    var mapLat=mapCentre.lat();
    var mapLng=mapCentre.lng();
    var cookiestring=mapLat+"_"+mapLng+"_"+mapZoom;
    setCookie("myMapCookie",cookiestring, 30);
 // resetTimer();
}

function loadMapState() {
    var gotCookieString=getCookie("myMapCookie");
    var splitStr = gotCookieString.split("_");
    var savedMapLat = parseFloat(splitStr[0]);
    var savedMapLng = parseFloat(splitStr[1]);
    var savedMapZoom = parseFloat(splitStr[2]);
    if ((!isNaN(savedMapLat)) && (!isNaN(savedMapLng)) && (!isNaN(savedMapZoom))) {
        map_goog.setCenter(new google.maps.LatLng(savedMapLat,savedMapLng));
        map_goog.setZoom(savedMapZoom);
    }
}

function setCookie(c_name,value,exdays) {
    var exdate=new Date();
    exdate.setDate(exdate.getDate() + exdays);
    var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
    document.cookie=c_name + "=" + c_value;
}

function getCookie(c_name) {
    var i,x,y,ARRcookies=document.cookie.split(";");
    for (i=0;i<ARRcookies.length;i++)
    {
      x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
      y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
      x=x.replace(/^\s+|\s+$/g,"");
      if (x==c_name)
      {
        return unescape(y);
    }
}
return "";
}

function googleCall(){

  var googleUrl  =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations?map=google';

  $http.get(googleUrl).success(function(data){

          //console.log(data);

      })  
  .error(function (error, status){

   console.log(error);
});

}

$scope.changeMap=function(map_no) {

 //console.log('change map...');
 document.getElementById("pac-inputs").value = "";
 document.getElementById("inputOsm").value   = "";


    // if(userName=='TPMS'){
    //   $scope.zoom  =  16;
    // }else{
      $scope.zoom  =  6;
    //}

    $scope.Filter    = $scope.vehicleStatus;
    $scope.maps_name = map_no;

    $scope.customZoom       =  0;
    $scope.positionLng      =  0;
    $scope.positionLat      =  0;

    if($scope.maps_name==0){

     // googleCall();

     document.getElementById("newLeafOsm").style.display = "none";
     document.getElementById("newLeaf").style.display   = "block";

     document.getElementById("map_osm").style.display  = "none";
     document.getElementById("maploc").style.display  = "block";

     localStorage.setItem('mapNo',0);
     map_change = 0;

     if(googSite == 0) {

      $scope.locations04[$scope.groupid].vehicleLocations   = [];
      $scope.locations04.vehicleLocations                 = [];

      $scope.initilize('maploc');
         //map_changeOsm = 0;
     } else {

        if($scope.vehicleStatus!='ALL'){

            $scope.clearMarkers();
            $scope.locations04[$scope.groupid].vehicleLocations = [];
            $scope.locations04.vehicleLocations                 = [];
            $scope.setMarkers($scope.data02);
        }
    }

    if($scope.siteExec==true && $scope.siteExec2==true){
        map_goog.setZoom(20);
        map_goog.setCenter($scope.markerPolysG[$scope.sitevalu].getPosition());
    }      

} else if($scope.maps_name==1) {

 document.getElementById("newLeaf").style.display     = "none";
 document.getElementById("newLeafOsm").style.display = "block";

 map_change = 1;

 document.getElementById("maploc").style.display="none";
 document.getElementById("map_osm").style.display="block";

 localStorage.setItem('mapNo',1);

 if(osmSite == 0) {

  if(map_osm!=null){
    map_osm.remove();
}

$scope.locations04[$scope.groupid].vehicleLocations = [];
$scope.locations04.vehicleLocations                 = [];

$scope.initilize_osm('map_osm');

} else {

    if($scope.vehicleStatus!='ALL'){

     $scope.clearMarkers_osm();
     $scope.locations04[$scope.groupid].vehicleLocations = [];
     $scope.locations04.vehicleLocations                 = [];
     $scope.setMarkers_osm($scope.data02);        
 }
}

if($scope.siteExec==true && $scope.siteExec2==true){
    map_osm.setZoom(20);
    map_osm.setView($scope.markerPolyss2[$scope.sitevalu].getLatLng());
}  
}
}

$scope.split_fcode = function(fcode){
    var str = $scope.fcode[0].group;
    var strFine = str.substring(str.lastIndexOf(':'));
    while(strFine.charAt(0)===':')
        strFine = strFine.substr(1);
    return strFine;
}


// $scope.getMailIdPhoneNo = function(vehi, days) {
//     //console.log('inside the methods')
//     var mailId = document.getElementById("mail").value;
//     var phone  = document.getElementById("phone").value;
//     if(vehi == 0 && days ==0)
//       console.log('select correctly');
//   else {
//       $scope.split_fcode($scope.fcode[0].group);
//       var f_code = $scope.split_fcode($scope.fcode[0].group);
//       var f_code_url =  GLOBAL.DOMAIN_NAME+'/getVehicleExp?vehicleId='+vehi+'&fcode='+f_code+'&days='+days+'&mailId='+mailId+'&phone='+phone;
//       var ecrypt_code_url = '';
//       $http.get(f_code_url).success(function(result){

//         //console.log(result);
//         var sp1 = ["",""];
//         var menuValue = JSON.parse(localStorage.getItem('userIdName'));

//         window.localStorage.setItem("userMasterName",menuValue);
//         if(menuValue!=null){
//             sp1 = menuValue.split(",");
//         }
//         var url='../public/track?vehicleId='+result.trim()+'&maps=track'+'&userID='+sp1[1];
//         window.open(url,'_blank');

//     });  
//   }
// }

$scope.getMailIdPhoneNo = function(vehi, days) {
    $scope.urlError='';
    //console.log('inside the methods')
    var mailId = document.getElementById("mail").value;
    var phone  = document.getElementById("phone").value;
    if(vehi == 0 && days ==0)
      console.log('select correctly');
  else {
      $scope.split_fcode($scope.fcode[0].group);
      var f_code = $scope.split_fcode($scope.fcode[0].group);
      var f_code_url =  GLOBAL.DOMAIN_NAME+'/getVehicleExp?vehicleId='+vehi+'&fcode='+f_code+'&days='+days+'&mailId='+mailId+'&phone='+phone;
      var ecrypt_code_url = '';
      $http.get(f_code_url).success(function(result){

        //console.log(result);
        var sp1 = ["",""];
        var menuValue = JSON.parse(localStorage.getItem('userIdName'));

        window.localStorage.setItem("userMasterName",menuValue);
        if(menuValue!=null){
            sp1 = menuValue.split(",");
        }
        if(mailId!='' || phone!=''){
           $scope.url=GLOBAL.DOMAIN_NAME+'/track?vehicleId='+result.trim()+'&maps=track'+'&userID='+sp1[1];
           $scope.showIcons=true;

       }
       if(mailId=='' && phone==''){
           $scope.urlError='Please enter email id or phone number to get url';
           $scope.showIcons=false;
       }


   });  
  }
}

$scope.copyThisUrl=function(){
    var copyTextarea = document.getElementById("copyUrl");
copyTextarea.select(); //select the text area
document.execCommand("copy"); //copy to clipboard
}



$scope.groupSelection = function(group, rowid){

   localStorage.setItem('groupSelection', 'yes');
        // siteclear();
        $scope.customZoom = 0;
        $scope.positionLng = 0;
        $scope.positionLat = 0;
        $scope.showSendCmd=false;
        $scope.res=0;
        $scope.command='SET-IP';

        startLoading();

        $scope.groupid        =  rowid;
      //$scope.locations04    =  [];
      $scope.vehicleStatus  =  "ALL";
      $scope.Filter         =  $scope.vehicleStatus;
        // $scope.zoom           =  6;
        // if(userName=='TPMS'){
        //   $scope.zoom  =  16;
        // }else{
          $scope.zoom  =  6;
        //}

        $http({
            method : "GET",
            url : GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group=' + group
        }).then(function mySuccess(response) {

            $scope.data      =  response.data;
          //$scope.data02    =  $scope.filterExpire(response.data[$scope.groupid].vehicleLocations);
          $scope.data02    =  response.data[$scope.groupid].vehicleLocations;
          var notncount =getParkedIgnitionAlert($scope.data02)[3];
          window.localStorage.setItem('totalNotifications',notncount);
          $scope.groupFuel      =  ($filter('filter')($scope.data02, {'fuel':"yes"}));
          $scope.vehiname  =  response.data[$scope.groupid].vehicleLocations[0].vehicleId;
          licenceExpiry  =  response.data[$scope.groupid].vehicleLocations[0].licenceExpiry;
          $scope.noOfTank = response.data[$scope.groupid].vehicleLocations[0].noOfTank;
          $scope.gName     =  response.data[$scope.groupid].group;
          sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
          setDeviceVolt(response.data[$scope.groupid].vehicleLocations[0]);
          $scope.totalVehicles   =  response.data[$scope.groupid]['totalVehicles'];
          $scope.vehicleOnline   =  response.data[$scope.groupid]['online'];
          $scope.attention       =  response.data[$scope.groupid]['attention'];
          $scope.parkedCount     =  response.data[$scope.groupid]['totalParkedVehicles'];
          $scope.movingCount     =  response.data[$scope.groupid]['totalMovingVehicles'];
          $scope.idleCount       =  response.data[$scope.groupid]['totalIdleVehicles'];
          $scope.overspeedCount  =  response.data[$scope.groupid]['topSpeed'];
          $scope.apiKeys         =  response.data[$scope.groupid].apiKey;
          $scope.companyName          =  response.data[$scope.groupid]['compName']; 
          $scope.gender               =  response.data[$scope.groupid]['gender'];
          $scope.systemName           =  response.data[$scope.groupid]['systemName'];
          $scope.locations04     =  [];
          $scope.data1    =  response.data[$scope.groupid].vehicleLocations;

          for(i=0;i<$scope.data.length;i++) {

             val = $scope.data[i];
             $scope.locations04.push({rowId:val.rowId,group:val.group,fcode:val.fcode});

             if(typeof $scope.locations04[$scope.groupid] != 'undefined') {
               $scope.locations04[$scope.groupid].vehicleLocations  =  [];
           }

           $scope.locations04.vehicleLocations  =  [];
       }


       if(map_change==0){

           $scope.marker = [];

           document.getElementById("map_osm").style.display="none";
           document.getElementById("maploc").style.display="block";

           $scope.initilize('maploc');
           //markerChange($scope.makerType);

       } else if(map_change==1){

           $scope.marker_osm=[];

           document.getElementById("maploc").style.display="none";
           document.getElementById("map_osm").style.display="block";

           if(map_osm!=null){
             map_osm.remove();
         }

         $scope.initilize_osm('map_osm');
            //markerChange_osm($scope.makerType);
        }

    }, function myError(response) {
        $scope.myWelcome = response.statusText;
    });
    };  

    $scope.onCategoryChange1 = function (vehicleStatus) {
  //alert('onCategoryChange');


  // if(userName=='TPMS'){
  //   $scope.zoom  =  16;
  // }else{
    $scope.zoom  =  6;
  //}
  $scope.vehicleStatus=vehicleStatus;
  // alert(vehicleStatus);
  if($scope.vehicleStatus=='N'){
    $scope.locations04.vehicleLocations                 = [];
    $scope.locations04.vehicleLocations    =  $scope.filterNotsync($scope.repData);
    setTimeout(function () {
        $('.selectpicker3').selectpicker('refresh');
    },500);
    if(map_change==0) {
     map_goog.setZoom(6);
     $scope.clearMarkers();

 } else if(map_change==1) {
     map_osm.setZoom(6);
     $scope.clearMarkers_osm();

 }
}else{
  if(map_change==0) {
     map_goog.setZoom(6);
     $scope.clearMarkers();
     $scope.Filter                                       = $scope.vehicleStatus;
     $scope.locations04[$scope.groupid].vehicleLocations = [];
     $scope.locations04.vehicleLocations                 = [];
     $scope.setMarkers($scope.data02);

 } else if(map_change==1) {
     map_osm.setZoom(6);
     $scope.clearMarkers_osm();
     $scope.Filter                                       = $scope.vehicleStatus;
     $scope.locations04[$scope.groupid].vehicleLocations = [];
     $scope.locations04.vehicleLocations                 = [];
     $scope.setMarkers_osm($scope.data02);

 }
}
};  

$scope.onCategoryChange = function () {

  // if(userName=='TPMS'){
  //   $scope.zoom  =  16;
  // }else{
    $scope.zoom  =  6;
  //}

  if(map_change==0) {
     map_goog.setZoom(6);
     $scope.clearMarkers();
     $scope.Filter                                       = $scope.vehicleStatus;
     $scope.locations04[$scope.groupid].vehicleLocations = [];
     $scope.locations04.vehicleLocations                 = [];
     $scope.setMarkers($scope.data02);

 } else if(map_change==1) {
     map_osm.setZoom(6);
     $scope.clearMarkers_osm();
     $scope.Filter                                       = $scope.vehicleStatus;
     $scope.locations04[$scope.groupid].vehicleLocations = [];
     $scope.locations04.vehicleLocations                 = [];
     $scope.setMarkers_osm($scope.data02);

 }
};  

$scope.clearMarkers = function() {
    for (var i = 0; i < $scope.marker.length; i++) {
      $scope.marker[i].setMap(null);
  }
  if($scope.markerCluster){
   $scope.markerCluster.clearMarkers();
}
$scope.markerCluster=null;
$scope.marker = [];
};

$scope.clearMarkers_osm = function() {

 for(var i=0;i<$scope.marker_osm.length;i++){
   map_osm.removeLayer($scope.marker_osm[i]);
}

$scope.marker_osm = [];  
};

function singleMarker_osm() {
  // console.log('singles.......');
  $("#single").hide();
  $("#cluster").show();

  $scope.groupMap  =  false;
  $scope.markerClusters.clearLayers();

  for(var i=0;i<$scope.marker_osm.length;i++){
   map_osm.removeLayer($scope.marker_osm[i]);
}

$scope.marker_osm = [];  
$scope.setMarkers_osm($scope.data[$scope.groupid].vehicleLocations);

}

function markerChange_osm(value) {

    if(assLabel=="true") {
      //console.log('marker change...');
      var iconn,imgs;
      angular.forEach($scope.data[$scope.groupid].vehicleLocations, function(valu, key){
         //img  = ($scope.makerType == 'markerChange')? 'assets/imgs/'+valu.vehicleType+'.png' : vamoservice.iconURL(valu)
         imgs  = ($scope.makerType == 'markerChange')? vamoservice.assetImage(valu) : vamoservice.iconURL(valu);

         iconn = L.icon({
            iconUrl: imgs,
            iconSize: [40,40],
            iconAnchor:[20,40],
                popupAnchor:[-1,-40] // scaledSize: new google.maps.Size(25, 25)
            });

         $scope.marker_osm[key].setIcon(iconn);
          //gmarkers[key].setMap($scope.map);
      });

  } else {

      if($scope.trvShow==true){

        //console.log('marker change...');
        var iconn,imgs;
        angular.forEach($scope.data[$scope.groupid].vehicleLocations, function(valu, key){
         //img  = ($scope.makerType == 'markerChange')? 'assets/imgs/'+valu.vehicleType+'.png' : vamoservice.iconURL(valu)
         imgs = ($scope.makerType == 'markerChange')? vamoservice.markerImage(valu) : vamoservice.trvIcon(valu);

         if($scope.makerType) {

            iconn = L.icon({
                iconUrl: imgs,
                iconSize: [30,30],
                iconAnchor:[20,40],
                    popupAnchor:[-1,-40] // scaledSize: new google.maps.Size(25, 25)
                });

        } else {

          iconn = L.icon({
              iconUrl: imgs,
              iconSize: [40,40],
              iconAnchor:[20,40],
                  popupAnchor:[-1,-40] // scaledSize: new google.maps.Size(25, 25)
              });
      }

      $scope.marker_osm[key].setIcon(iconn);
          //gmarkers[key].setMap($scope.map);
      });


    } else {

        //console.log('marker change...');
        var iconn,imgs;
        angular.forEach($scope.data[$scope.groupid].vehicleLocations, function(valu, key){
         //img  = ($scope.makerType == 'markerChange')? 'assets/imgs/'+valu.vehicleType+'.png' : vamoservice.iconURL(valu)
         imgs = ($scope.makerType == 'markerChange')? vamoservice.markerImage(valu) : vamoservice.iconURL(valu);

         if($scope.makerType) {

            iconn = L.icon({
                iconUrl: imgs,
                iconSize: [30,30],
                iconAnchor:[20,40],
                    popupAnchor:[-1,-40] // scaledSize: new google.maps.Size(25, 25)
                });

        } else {

            iconn = L.icon({
              iconUrl: imgs,
              iconSize: [40,40],
              iconAnchor:[20,40],
                  popupAnchor:[-1,-40] // scaledSize: new google.maps.Size(25, 25)
              });
        }

        $scope.marker_osm[key].setIcon(iconn);
          //gmarkers[key].setMap($scope.map);
      });


    }

}
}  

function markerChange(value) {

    if(assLabel=="true") {

        var icon , img;

        angular.forEach($scope.data[$scope.groupid].vehicleLocations, function(valu, key){
      //img = ($scope.makerType == 'markerChange')? 'assets/imgs/'+'Car2'+'.png' : vamoservice.iconURL(valu)
      img = ($scope.makerType == 'markerChange')? vamoservice.assetImage(valu) : vamoservice.iconURL(valu);

      if($scope.makerType == 'markerChange') {
            icon = {scaledSize: new google.maps.Size(30, 30),url: img,labelOrigin:  new google.maps.Point(25,40)} //scaledSize: new google.maps.Size(25, 25)
         // icon = {scaledSize: new google.maps.Size(30, 30),url: img} //scaledSize: new google.maps.Size(25, 25) valu.vehicleType
     } else {
      icon = {scaledSize: new google.maps.Size(30, 30), url: img,labelOrigin:  new google.maps.Point(25,40)}
  }

  $scope.marker[key].setIcon(icon);
  $scope.marker[key].setMap(map_goog);

        //gmarkers[key].setIcon(icon);
        //gmarkers[key].setMap($scope.map);
    });

    } else {

      if($scope.trvShow==true){

        var icon, img;

        angular.forEach($scope.data[$scope.groupid].vehicleLocations, function(valu, key) {

          //img = ($scope.makerType == 'markerChange')? 'assets/imgs/'+'Car2'+'.png' : vamoservice.iconURL(valu)
          img = ($scope.makerType == 'markerChange')? vamoservice.markerImage(valu) : vamoservice.trvIcon(valu);

          if($scope.makerType == 'markerChange'){
                icon = {scaledSize: new google.maps.Size(30, 30),url: img,labelOrigin:  new google.maps.Point(25,40)} //scaledSize: new google.maps.Size(25, 25)
             // icon = {scaledSize: new google.maps.Size(30, 30),url: img} //scaledSize: new google.maps.Size(25, 25) valu.vehicleType
         } else {
          icon = {scaledSize: new google.maps.Size(40, 40), url: img,labelOrigin:  new google.maps.Point(25,40)}
      }

      $scope.marker[key].setIcon(icon);
      $scope.marker[key].setMap(map_goog);
            //gmarkers[key].setIcon(icon);
            //gmarkers[key].setMap($scope.map);
        });


    } else {

        var icon, img;

        angular.forEach($scope.data[$scope.groupid].vehicleLocations, function(valu, key) {

          //img = ($scope.makerType == 'markerChange')? 'assets/imgs/'+'Car2'+'.png' : vamoservice.iconURL(valu)
          img = ($scope.makerType == 'markerChange')? vamoservice.markerImage(valu) : vamoservice.iconURL(valu);

          if($scope.makerType == 'markerChange'){
                icon = {scaledSize: new google.maps.Size(30, 30),url: img,labelOrigin:  new google.maps.Point(25,40)} //scaledSize: new google.maps.Size(25, 25)
             // icon = {scaledSize: new google.maps.Size(30, 30),url: img} //scaledSize: new google.maps.Size(25, 25) valu.vehicleType
         } else {
          icon = {scaledSize: new google.maps.Size(40, 40), url: img,labelOrigin:  new google.maps.Point(25,40)}
      }

      $scope.marker[key].setIcon(icon);
      $scope.marker[key].setMap(map_goog);
            //gmarkers[key].setIcon(icon);
            //gmarkers[key].setMap($scope.map);
        });

    }    
}
}

//view map
$scope.mapView  =   function(value) {
    switch(value){
        case 'listMap' :
        listMap();
        break;
        case 'home' :
        homeMap();
        break;
        case 'cluster' :
        $scope.enableCluster    = 1;
        if(map_change==0){
            clusterMarker();
        } else if(map_change==1){
            clusterMarker_osm()
        }
        break;
        case 'single' :
        $scope.enableCluster    = 0;
        if(map_change==0){
          singleMarker();
      } else if(map_change==1){
          singleMarker_osm()
      }
      break;
      case 'fscreen' :
      fullScreen();
      break;
      case 'escreen':
      exitScreen();
      break;
      case 'tablefull' :
      fulltable();
      break;
      case 'graphs':
      graphView();
      break;
      case 'markerChange':
      $scope.makerType =  "markerChange";
      if(map_change==0){
          changeMarker();
          markerChange();
      } else if(map_change==1) {
          changeMarker_osm();
          markerChange_osm();
      }
      break;
         /* case 'tollYes':
                tollMarkers();
            break;
            case 'tollNo':
                removeToll();
                break; */  
                case 'enableLabel':
                if(map_change==0){
                  enableLabel();
              } else if(map_change==1){
                  enableLabel_Osm();
              }
              break;
              case 'disableLabel':
              if(map_change==0){
                  disableLabel();
              } else if(map_change==1){
                  disableLabel_Osm();
              }
              break;        
              case 'undefined':
              $scope.makerType =  undefined;
              if(map_change==0){
                changeMarker();
                markerChange(undefined);
            } else if(map_change==1){
              changeMarker_osm();
              markerChange_osm(undefined);
          }
          default:
          break;
      }
  }

  function disableLabel() {

    //console.log('disable..');

    $("#enableLabel").show();
    $("#disableLabel").hide();

    $scope.labeldisplay = true;
    $scope.markLabss    = true;
    $scope.polyLabs     = true;

    for(var i=0;i<infoBoxs.length;i++){
        //console.log( infoBoxs[i]);
        infoBoxs[i].isHidden=true;
        infoBoxs[i].isHidden_=true;
        infoBoxs[i].close();
    }

    for(var i=0;i<infoBoxs2.length;i++){
        //console.log( infoBoxs[i]);
        infoBoxs2[i].isHidden=true;
        infoBoxs2[i].isHidden_=true;
        infoBoxs2[i].close();
    }

 /* $scope.tempMarker = $scope.marker;
    $scope.clearMarkers();
    $scope.marker = $scope.tempMarker;
    $scope.tempMarker = [];
    for(var i=0; i<$scope.marker.length; i++)
    {
      $scope.marker[i].setMap(map_goog);
      $scope.marker[i].setLabel("");
  }*/
}

var enabCircLab=0;

function enableLabel() {

    //if($scope.markerCluster==null){

        $("#disableLabel").show();
        $("#enableLabel").hide();

        $scope.labeldisplay = false;
        $scope.markLabss    = false;
        $scope.polyLabs     = false;

    //console.log('enable');

    if(enabCircLab==0){

      for(var i=0;i<infoBoxs.length;i++){
      //console.log( infoBoxs[i]);
      infoBoxs[i].isHidden=true;
      infoBoxs[i].isHidden_=true;
      infoBoxs[i].close();
  }

  for(var i=0;i<infoBoxs2.length;i++){
      //console.log( infoBoxs[i]);
      infoBoxs2[i].isHidden=true;
      infoBoxs2[i].isHidden_=true;
      infoBoxs2[i].close();
  }

  enabCircLab+=1;
}


for(var i=0;i<infoBoxs.length;i++){
        //console.log( infoBoxs[i]);
        infoBoxs[i].isHidden=false;
        infoBoxs[i].isHidden_=false;
        infoBoxs[i].open(map_goog);
    }

    for(var i=0;i<infoBoxs2.length;i++){
        //console.log( infoBoxs[i]);
        infoBoxs2[i].isHidden=false;
        infoBoxs2[i].isHidden_=false;
        infoBoxs2[i].open(map_goog);
    }

 /*  
    $scope.tempMarker = $scope.marker;
    $scope.clearMarkers();
    $scope.marker = $scope.tempMarker;
    $scope.tempMarker = [];
    for(var i=0; i<$scope.marker.length; i++) {
      $scope.marker[i].setMap(map_goog);
      $scope.marker[i].setLabel($scope.markerLabel[i]);
  }*/
   //}
}

function enableLabel_Osm() {

    $("#disableLabel").show();
    $("#enableLabel").hide();

    $scope.hideMe   = false;
    $scope.markLab  = false;

}

function disableLabel_Osm() {

    $("#enableLabel").show();
    $("#disableLabel").hide();

    $scope.hideMe   = true;
    $scope.markLab  = true;  

}

  // clusterMarker
  function clusterMarker()
  {
    $("#cluster").hide();
    $("#single").show();
    $scope.groupMap=true;
    // markerCluster  = new MarkerClusterer($scope.map, null, null)
    // mcOptions = {gridSize: 50,maxZoom: 15,styles: [ { height: 53, url: "assets/imgs/m1.png", width: 53}]}
    $scope.markerCluster = new MarkerClusterer(map_goog,  $scope.marker,
        {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
}

function clusterMarker_osm() {

    var img, icon;

//    console.log('cluster....');

$("#cluster").hide();
$("#single").show();

$scope.groupMap        =  true;
$scope.markerClusters  =  new L.MarkerClusterGroup();

  //console.log(markerClusters);
  //gmarkers[i].setView(map_osm);

  for(var i=0;i<$scope.marker_osm.length;i++){
      map_osm.removeLayer($scope.marker_osm[i]);
  }

  for(var i=0;i<$scope.marker_osm.length;i++){
    $scope.markerClusters.addLayer($scope.marker_osm[i]);
}

map_osm.addLayer($scope.markerClusters);
}


  //single
  function singleMarker()
  {
    $("#single").hide();
    $("#cluster").show();
    $scope.groupMap=false;
    $scope.tempMarker = $scope.marker;
    $scope.clearMarkers();
    $scope.marker = $scope.tempMarker;
    $scope.tempMarker = [];
    for(var i=0; i<$scope.marker.length; i++)
    {
      $scope.marker[i].setMap(map_goog);
  }
}

function changeMarker_osm(){
    if($scope.makerType == undefined)
    {
      $("#carMarker").show();
      $("#marker").hide();  
  } else if ($scope.makerType == 'markerChange'){
      $("#carMarker").hide();
      $("#marker").show();  
  }
}

  // changeMarker
  function changeMarker()
  {
    if($scope.makerType == undefined)
    {
      $("#carMarker").show();
      $("#marker").hide();  
  } else if ($scope.makerType == 'markerChange'){
      $("#carMarker").hide();
      $("#marker").show();  
  }

}
function fulltable()
{
    setId();
    $("#minmax").hide();
    document.getElementById($scope.idinvoke).setAttribute("id", "tablelist");
    document.getElementById('mapTable-mapList').setAttribute("id", "talist");      
    $('#talist').show(500);
}

function graphView()
{
    $('#graphsId').toggle(500);
}
function fullScreen(){
    setId();
    $("#efullscreen").show();
    $("#contentmin").show(1000);
    $("#sidebar-fullscreen").show(500);
    document.getElementById($scope.idinvoke).setAttribute("id", "sidebar-fullscreen");
}

function exitScreen() {
    setId();

    $("#fullscreen").show();

        // $("#listImg").show();
        // $("#homeImg").hide();
        $("#contentmin").show(1000);
        $("#sidebar-wrapper").show(500);
        document.getElementById($scope.idinvoke).setAttribute("id", "wrapper");
    }

    $('#mapTable-mapList').hide()
    $("#homeImg").hide();
    $("#listImg").show();
    $("#single").hide();
    $("#cluster").show();
    $("#efullscreen").hide();
    $("#fullscreen").show();
    //$('#graphsId').hide();
    $("#carMarker").show();
    $("#marker").hide();
    $("#tollYes").show();
    $("#tollNo").hide();
    $("#disableLabel").hide();
    $("#enableLabel").show();

    function listMap ()
    {
    // if($scope.zohoReports==undefined){
        setId();
        // $("#homeImg").show();
        $scope.tabView=1;
        $("#listImg").hide();
        $("#homeImg").show();
        $("#fullscreen").show();
        $('#mapTable-mapList').show(1000);
        //
        document.getElementById($scope.idinvoke).setAttribute("id", "mapList");
        if(document.getElementById('talist')!=null)
            document.getElementById('talist').setAttribute("id", "mapTable-mapList");
    //  }
}
    //return home
    function homeMap ()
    {
        setId();

        $scope.tabView=0;

        // $("#listImg").show();
        $("#homeImg").hide();
        $("#listImg").show();
        $("#fullscreen").show();

        $("#contentmin").show(1000);
        $("#sidebar-wrapper").show(500);
        // document.getElementById($scope.idinvoke).setAttribute("id", "mapList")
        document.getElementById($scope.idinvoke).setAttribute("id", "wrapper");
    }

    $scope.setMarkers_osm2 = function(req_data, address) {

    //console.log('setMarkers_osm');
      //console.log(req_data);

      $scope.markerss_osm = req_data.map(function(location, i) {

          if($scope.Filter != 'SINGLE' && $scope.Filter != 'ALL' && $scope.Filter != location.position && $scope.Filter != 'Y' && $scope.Filter != 'ON' && $scope.Filter != 'OFF')
              return
          else if($scope.Filter != 'SINGLE' && $scope.Filter != 'ALL' && (($scope.Filter == 'ON' || $scope.Filter == 'OFF') && location.status != $scope.Filter))
              return;
          else if($scope.Filter != 'SINGLE' && $scope.Filter != 'ALL' && ($scope.Filter == 'Y' && location.isOverSpeed != $scope.Filter))
              return;

          if($scope.Filter != 'SINGLE'){

             if(location.expired=="No") {

                $scope.locations04[$scope.groupid].vehicleLocations.push(location);
                location.gsmLevel=parseInt(location.gsmLevel);
                    //location.shortName=parseInt(location.shortName);
                    $scope.locations04.vehicleLocations.push(location);
                    setTimeout(function () {
                        $('.selectpicker3').selectpicker('refresh');
                    },500);

                } else if(location.expired=="Yes") {

                  $scope.locations04[$scope.groupid].vehicleLocations.push({status:location.status,rowId:location.rowId,shortName:location.shortName,vehicleId:location.vehicleId,vehicleType:location.vehicleType,color:location.color});
                  $scope.locations04.vehicleLocations.push({status:location.status,rowId:location.rowId,shortName:location.shortName,vehicleId:location.vehicleId,vehicleType:location.vehicleType,color:location.color});
                  setTimeout(function () {
                        $('.selectpicker3').selectpicker('refresh');
                    },500);
              }

          }

            //console.log($scope.locations04);

       /* if (typeof infowindow_osm != 'undefined') {
                infowindow_osm.close();
            } */

            if(location.expired=="No"){


              var latLngOsm      = L.latLng(location['latitude'], location['longitude']);
              var markertemp_osm = new L.marker(latLngOsm).bindLabel(location['shortName'], { noHide: true, className: 'markerLabels' });
              markertemp_osm.addTo(map_osm);

          //console.log(assLabel);

          if(assLabel=="true"){

              var icon = L.icon({
                 iconUrl: vamoservice.assetImage(location),
                 iconSize: [30,30],
                 iconAnchor:[20,40],
               popupAnchor:[-1,-40] //scaledSize: new google.maps.Size(25, 25)
           });

          } else {

              if($scope.trvShow==true){

                var icon = L.icon({
                 iconUrl: vamoservice.trvIcon(location),
                 iconSize: [40,40],
                 iconAnchor:[20,40],
                   popupAnchor:[-1,-40] //scaledSize: new google.maps.Size(25, 25)
               });  

            } else {

              var icon = L.icon({
                iconUrl: vamoservice.iconURL(location),
                iconSize: [40,40],
                iconAnchor:[20,40],
                    popupAnchor:[-1,-40] //scaledSize: new google.maps.Size(25, 25)
                });

          }  

      }

      markertemp_osm.setIcon(icon);


      if($scope.Filter != 'SINGLE'){

         markertemp_osm.on('click', function(ev) {


             //alert(ev.latlng);
             $scope.$apply(function () {
              $scope._editValue_con   =   true;
              $("#currenStatus").hide();
                //$scope.individualVehicle = location;
                $scope.assignValue(location);
            });

             if(ev.target._popup == undefined) {
              if(location.shortName != null)
                  localStorage.setItem('selectedVehicleId', location.shortName+"__"+location.rowId);  

              //console.log('info1..');

              infowindow_osm = new L.popup({maxWidth: 400,  
                maxHeight:170}).setContent($scope.infoContent(location)[0]);

              markertemp_osm.bindPopup(infowindow_osm);
              markertemp_osm.openPopup();
          }

      });
     }
     if($scope.Filter != 'SINGLE' && $scope.enableCluster == 1){
        clusterMarker_osm();
    }  

    if(address != undefined){

             //console.log('info2..');

             infowindow_osm = new L.popup({maxWidth: 400,  
                maxHeight:170}).setContent($scope.infoContent(location)[0]);

             markertemp_osm.bindPopup(infowindow_osm);
             markertemp_osm.openPopup();
         }



         $scope.marker_osm.push(markertemp_osm);

          //console.log($scope.zoom);

          if($scope.customZoom != 0) {                  
              $scope.zoom  = $scope.customZoom;
          }

          map_osm.setZoom($scope.zoom);


          if($scope.positionLng != 0) {        
            map_osm.setView([$scope.positionLat,$scope.positionLng],$scope.zoom );
        } else{
            map_osm.setView(markertemp_osm.getLatLng());
        }

        return markertemp_osm;

    }

});



}



$scope.setMarkers_osm = function(req_data, address) {

    //console.log('setMarkers_osm');
     // console.log(req_data);

     $scope.markerss_osm = req_data.map(function(location, i) {

      if($scope.Filter != 'SINGLE' && $scope.Filter != 'ALL' && $scope.Filter != location.position && $scope.Filter != 'Y' && $scope.Filter != 'ON' && $scope.Filter != 'OFF')
          return
      else if($scope.Filter != 'SINGLE' && $scope.Filter != 'ALL' && (($scope.Filter == 'ON' || $scope.Filter == 'OFF') && location.status != $scope.Filter))
          return;
      else if($scope.Filter != 'SINGLE' && $scope.Filter != 'ALL' && ($scope.Filter == 'Y' && location.isOverSpeed != $scope.Filter))
          return;

      if($scope.Filter != 'SINGLE'){

         if(location.expired=="No") {

            $scope.locations04[$scope.groupid].vehicleLocations.push(location);
            location.gsmLevel=parseInt(location.gsmLevel);
                    //location.shortName=parseInt(location.shortName);
                    $scope.locations04.vehicleLocations.push(location);
                    setTimeout(function () {
                        $('.selectpicker3').selectpicker('refresh');
                    },500);
                } else if(location.expired=="Yes") {

                  $scope.locations04[$scope.groupid].vehicleLocations.push({status:location.status,rowId:location.rowId,shortName:location.shortName,vehicleId:location.vehicleId,vehicleType:location.vehicleType,color:location.color});
                  $scope.locations04.vehicleLocations.push({status:location.status,rowId:location.rowId,shortName:location.shortName,vehicleId:location.vehicleId,vehicleType:location.vehicleType,color:location.color});
                  setTimeout(function () {
                    $('.selectpicker3').selectpicker('refresh');
                },500);
              }

          }

            //console.log($scope.locations04);

       /* if (typeof infowindow_osm != 'undefined') {
                infowindow_osm.close();
            } */

            if(location.expired=="No"){


              var latLngOsm      = L.latLng(location['latitude'], location['longitude']);
              var markertemp_osm = new L.marker(latLngOsm).bindLabel(location['shortName'], { noHide: true, className: 'markerLabels' });
              markertemp_osm.addTo(map_osm);

          //console.log(assLabel);

          if(assLabel=="true"){

              var icon = L.icon({
                 iconUrl: vamoservice.assetImage(location),
                 iconSize: [30,30],
                 iconAnchor:[20,40],
               popupAnchor:[-1,-40] //scaledSize: new google.maps.Size(25, 25)
           });

          } else {        

            if($scope.trvShow==true){

                var icon = L.icon({
                 iconUrl: vamoservice.trvIcon(location),
                 iconSize: [40,40],
                 iconAnchor:[20,40],
                   popupAnchor:[-1,-40] //scaledSize: new google.maps.Size(25, 25)
               });  

            } else {

              var icon = L.icon({
                iconUrl: vamoservice.iconURL(location),
                iconSize: [40,40],
                iconAnchor:[20,40],
                    popupAnchor:[-1,-40] //scaledSize: new google.maps.Size(25, 25)
                });

          }

      }

      markertemp_osm.setIcon(icon);


      if($scope.Filter != 'SINGLE'){

         markertemp_osm.on('click', function(ev) {


             //alert(ev.latlng);
             $scope.$apply(function () {
              $scope._editValue_con   =   true;
              $("#currenStatus").hide();
                //$scope.individualVehicle = location;
                $scope.assignValue(location);
            });

             if(ev.target._popup == undefined) {
              if(location.shortName != null)
                  localStorage.setItem('selectedVehicleId', location.shortName+"__"+location.rowId);  

              //console.log('info1..');

              infowindow_osm = new L.popup({maxWidth: 400,  
                maxHeight:250}).setContent($scope.infoContent(location)[0]);

              markertemp_osm.bindPopup(infowindow_osm);
              markertemp_osm.openPopup();
          }

      });
     }
     if($scope.Filter != 'SINGLE' && $scope.enableCluster == 1){
        clusterMarker_osm();
    }  

    if(address != undefined){

             //console.log('info2..');

             infowindow_osm = new L.popup({maxWidth: 400,  
                maxHeight:250}).setContent($scope.infoContent(location)[0]);

             markertemp_osm.bindPopup(infowindow_osm);
             markertemp_osm.openPopup();
         }

         $scope.marker_osm.push(markertemp_osm);

         if($scope.customZoom != 0) {                  
             $scope.zoom  = $scope.customZoom;
         }


         map_osm.setZoom($scope.zoom);


           //console.log($scope.positionLng);
           
           if($scope.positionLng != 0) {        
            map_osm.setView([$scope.positionLat,$scope.positionLng],$scope.zoom );
        }
        else{
            map_osm.setView(markertemp_osm.getLatLng());
        }



        return markertemp_osm;

    }

});

//console.log($scope.zoom);

}


$scope.setMarkers2 = function(req_data, address) {

    $scope.markers = req_data.map(function(location, i) {

        if($scope.Filter != 'SINGLE' && $scope.Filter != 'ALL' && $scope.Filter != location.position && $scope.Filter != 'Y' && $scope.Filter != 'ON' && $scope.Filter != 'OFF')
          return
      else if($scope.Filter != 'SINGLE' && $scope.Filter != 'ALL' && (($scope.Filter == 'ON' || $scope.Filter == 'OFF') && location.status != $scope.Filter))
          return;
      else if($scope.Filter != 'SINGLE' && $scope.Filter != 'ALL' && ($scope.Filter == 'Y' && location.isOverSpeed != $scope.Filter))
          return;

      if($scope.Filter != 'SINGLE'){

         if(location.expired=="No") {

            $scope.locations04[$scope.groupid].vehicleLocations.push(location);
            location.gsmLevel=parseInt(location.gsmLevel);
                    //location.shortName=parseInt(location.shortName);
                    $scope.locations04.vehicleLocations.push(location);
                    setTimeout(function () {
                        $('.selectpicker3').selectpicker('refresh');
                    },500);
                } else if(location.expired=="Yes") {

                  $scope.locations04[$scope.groupid].vehicleLocations.push({status:location.status,rowId:location.rowId,shortName:location.shortName,vehicleId:location.vehicleId,vehicleType:location.vehicleType,color:location.color});
                  $scope.locations04.vehicleLocations.push({status:location.status,rowId:location.rowId,shortName:location.shortName,vehicleId:location.vehicleId,vehicleType:location.vehicleType,color:location.color});
                  setTimeout(function () {
                        $('.selectpicker3').selectpicker('refresh');
                    },500);
              }
          }



                //var infowindow = null;

                if(typeof infowindow != 'undefined') {
                    infowindow.close();
                }

                if(location.expired=="No"){

                    if(assLabel=="true") {

                      var image = {
                         url: vamoservice.assetImage(location),
                         scaledSize: new google.maps.Size(30, 30),
                         labelOrigin:  new google.maps.Point(30,30)
                     };

                     var markertemp = new MarkerWithLabel({
                      position: new google.maps.LatLng(location['latitude'], location['longitude']),
                      map: map_goog,
                      icon: image,
                      labelContent :location['shortName'],
                      labelAnchor: new google.maps.Point(19, 0),
                      labelClass: "labels",
                      labelInBackground: false
                  });

                 } else {

                   if($scope.trvShow==true){

                       var markertemp = new MarkerWithLabel({
                        position: new google.maps.LatLng(location['latitude'], location['longitude']),
                        map: map_goog,
                        icon: vamoservice.trvIcon(location),
                        labelContent :location['shortName'],
                        labelAnchor: new google.maps.Point(19, 0),
                        labelClass: "labels",
                        labelInBackground: false
                    });

                   } else {

                      var markertemp = new MarkerWithLabel({
                        position: new google.maps.LatLng(location['latitude'], location['longitude']),
                        map: map_goog,
                        icon: vamoservice.iconURL(location),
                        labelContent :location['shortName'],
                        labelAnchor: new google.maps.Point(19, 0),
                        labelClass: "labels",
                        labelInBackground: false
                    });
                  }

              }

        /*  var markertemp = new MarkerWithLabel({
            position: new google.maps.LatLng(location['latitude'], location['longitude']),
            map: $scope.map,
            icon: vamoservice.iconURL(location),
            labelContent :location['shortName'],
            labelAnchor: new google.maps.Point(19, 0),
            labelClass: "labels",
            labelInBackground: false
        });*/
            /* var image;
            if(assLabel=="true") {

            image = {
               url: vamoservice.assetImage(location),
               scaledSize: new google.maps.Size(30, 30),
               labelOrigin:  new google.maps.Point(30,30)
            };

            } else {

                image = {
                url: vamoservice.iconURL(location),
                scaledSize: new google.maps.Size(40, 40),
                labelOrigin:  new google.maps.Point(30,50)
              };
            }
        var labelTemp = {
           text: location['shortName'],
           color: "blue",
           fontSize: "12px",
           fontWeight: "bold",
           };
       
        var markertemp = new google.maps.Marker({
           position: new google.maps.LatLng(location['latitude'], location['longitude']),
           map: map_goog,
           icon: image,
           label: labelTemp,
       }); */

       if($scope.Filter != 'SINGLE') {
        google.maps.event.addListener(markertemp, "click", function(e) {

          if (typeof infowindow != 'undefined') {
            infowindow.close();
        }

        $scope.$apply(function () {
           $scope._editValue_con   =   true;
           $("#currenStatus").hide();
               //$scope.individualVehicle = location;
               $scope.assignValue(location);
           });
        if(location.shortName != null)
         localStorage.setItem('selectedVehicleId', location.shortName+"__"+location.rowId);  

     infowindow = new InfoBubble({
      minWidth: 240,  
      maxHeight: 140,
      content: $scope.infoContent(location)[0],

  });
     infowindow.open(map_goog, markertemp);
          //$scope.genericFunction(location['vehicleId'], location['rowId']);
      });
    }        

    $scope.marker.push(markertemp);
        //$scope.markerLabel.push(labelTemp);

    /* if(polyLabs==true) {
         markertemp.setLabel('');
     } */

     if(address != undefined) {
        infowindow = new InfoBubble({
          minWidth: 240,  
          maxHeight: 140,
          content: $scope.infoContent(location)[0]
      });
        infowindow.open(map_goog, markertemp);
    }

    if($scope.customZoom != 0) {                  
     $scope.zoom  = $scope.customZoom;
 }

 map_goog.setZoom($scope.zoom);


 if($scope.positionLng != 0) {        
    map_goog.setCenter(new google.maps.LatLng($scope.positionLat,$scope.positionLng));
}
else{
    map_goog.setCenter(markertemp.getPosition());
}      

return markertemp;  
}

});

     //   console.log($scope.zoom);

     /* if($scope.Filter != 'SINGLE' && $scope.enableCluster == 1){
        clusterMarker();
    }*/
}  



$scope.setMarkers = function(req_data, address) {

    $scope.markers = req_data.map(function(location, i) {

        if($scope.Filter != 'SINGLE' && $scope.Filter != 'ALL' && $scope.Filter != location.position && $scope.Filter != 'Y' && $scope.Filter != 'ON' && $scope.Filter != 'OFF')
          return
      else if($scope.Filter != 'SINGLE' && $scope.Filter != 'ALL' && (($scope.Filter == 'ON' || $scope.Filter == 'OFF') && location.status != $scope.Filter))
          return;
      else if($scope.Filter != 'SINGLE' && $scope.Filter != 'ALL' && ($scope.Filter == 'Y' && location.isOverSpeed != $scope.Filter))
          return;

      if($scope.Filter != 'SINGLE'){

         if(location.expired=="No") {

            $scope.locations04[$scope.groupid].vehicleLocations.push(location);
            location.gsmLevel=parseInt(location.gsmLevel);
                    //location.shortName=parseInt(location.shortName);
                    $scope.locations04.vehicleLocations.push(location);
                    setTimeout(function () {
                        $('.selectpicker3').selectpicker('refresh');
                    },500);

                } else if(location.expired=="Yes") {

                  $scope.locations04[$scope.groupid].vehicleLocations.push({status:location.status,rowId:location.rowId,shortName:location.shortName,vehicleId:location.vehicleId,vehicleType:location.vehicleType,color:location.color});
                  $scope.locations04.vehicleLocations.push({status:location.status,rowId:location.rowId,shortName:location.shortName,vehicleId:location.vehicleId,vehicleType:location.vehicleType,color:location.color});
                  setTimeout(function () {
                        $('.selectpicker3').selectpicker('refresh');
                    },500);
              }
          }



                //var infowindow = null;

                if(typeof infowindow != 'undefined') {
                    infowindow.close();
                }

                if(location.expired=="No"){

                    if(assLabel=="true") {

                      var image = {
                         url: vamoservice.assetImage(location),
                         scaledSize: new google.maps.Size(30, 30),
                         labelOrigin:  new google.maps.Point(30,30)
                     };

                     var markertemp = new MarkerWithLabel({
                      position: new google.maps.LatLng(location['latitude'], location['longitude']),
                      map: map_goog,
                      icon: image,
                      labelContent :location['shortName'],
                      labelAnchor: new google.maps.Point(19, 0),
                      labelClass: "labels",
                      labelInBackground: false
                  });

                 } else {

                  if($scope.trvShow==true){

                   var markertemp = new MarkerWithLabel({
                    position: new google.maps.LatLng(location['latitude'], location['longitude']),
                    map: map_goog,
                    icon: vamoservice.trvIcon(location),
                    labelContent :location['shortName'],
                    labelAnchor: new google.maps.Point(19, 0),
                    labelClass: "labels",
                    labelInBackground: false
                });

               } else {

                  var markertemp = new MarkerWithLabel({
                    position: new google.maps.LatLng(location['latitude'], location['longitude']),
                    map: map_goog,
                    icon: vamoservice.iconURL(location),
                    labelContent :location['shortName'],
                    labelAnchor: new google.maps.Point(19, 0),
                    labelClass: "labels",
                    labelInBackground: false
                });
              }
          }

        /*  var markertemp = new MarkerWithLabel({
            position: new google.maps.LatLng(location['latitude'], location['longitude']),
            map: $scope.map,
            icon: vamoservice.iconURL(location),
            labelContent :location['shortName'],
            labelAnchor: new google.maps.Point(19, 0),
            labelClass: "labels",
            labelInBackground: false
        });*/
            /* var image;
            if(assLabel=="true") {

            image = {
               url: vamoservice.assetImage(location),
               scaledSize: new google.maps.Size(30, 30),
               labelOrigin:  new google.maps.Point(30,30)
            };

            } else {

                image = {
                url: vamoservice.iconURL(location),
                scaledSize: new google.maps.Size(40, 40),
                labelOrigin:  new google.maps.Point(30,50)
              };
            }
        var labelTemp = {
           text: location['shortName'],
           color: "blue",
           fontSize: "12px",
           fontWeight: "bold",
           };
       
        var markertemp = new google.maps.Marker({
           position: new google.maps.LatLng(location['latitude'], location['longitude']),
           map: map_goog,
           icon: image,
           label: labelTemp,
       }); */

       if($scope.Filter != 'SINGLE') {
        google.maps.event.addListener(markertemp, "click", function(e) {

          if (typeof infowindow != 'undefined') {
            infowindow.close();
        }

        $scope.$apply(function () {
           $scope._editValue_con   =   true;
           $("#currenStatus").hide();
               //$scope.individualVehicle = location;
               $scope.assignValue(location);
           });
        if(location.shortName != null)
         localStorage.setItem('selectedVehicleId', location.shortName+"__"+location.rowId);  

     infowindow = new InfoBubble({
      minWidth: 280,  
      maxHeight: 160,
      content: $scope.infoContent(location)[0],

  });
     infowindow.open(map_goog, markertemp);
          //$scope.genericFunction(location['vehicleId'], location['rowId']);
      });
    }        

    $scope.marker.push(markertemp);
        //$scope.markerLabel.push(labelTemp);

    /* if(polyLabs==true) {
         markertemp.setLabel('');
     } */

     if(address != undefined) {
        infowindow = new InfoBubble({
          minWidth: 280,  
          maxHeight: 160,
          content: $scope.infoContent(location)[0]
      });
        infowindow.open(map_goog, markertemp);
    }


    if($scope.customZoom != 0) {                  
     $scope.zoom  = $scope.customZoom;
 }

 map_goog.setZoom($scope.zoom);

         //map_goog.setZoom($scope.zoom);
         if($scope.positionLng != 0) {        
            map_goog.setCenter(new google.maps.LatLng($scope.positionLat,$scope.positionLng));
        }
        else{
            map_goog.setCenter(markertemp.getPosition());
        }      

        return markertemp;  
    }

});

       // console.log($scope.zoom);

       if($scope.Filter != 'SINGLE' && $scope.enableCluster == 1){
        clusterMarker();
    }
}  

function siteclear(){
  // if(localStorage.getItem('manualClick')!='yes'){
  //     $scope.SiteCheckbox.value1='NO';
  // }
  if(map_change==0){
    for(key in _mapsDetails) {
       _mapsDetails[key].setMap(null);
   }
   _mapsDetails = {};

}
else if(map_change==1){
 for(key in osm_mapsDetails) {
     map_osm.removeLayer(osm_mapsDetails[key]);
 }
 osm_mapsDetails = {};
}
}
$scope.commandbtnclicked = function(command) {
    $scope.isCommandModalOpen = true;
}

$scope.openModal=function(){
    $("#myModal").modal('show');
    $scope.showIcons=false;
    $scope.urlError='';
}
$scope.closeModal=function(){
    $("#myModal").modal('hide');
    $scope.days=0;
    $scope.days1=0;
}
getVehicleData=function(shortname){
    $scope._editValue._shiftNames=[];    
    $scope._editValue._shiftList=[];
    var shifts=[];
    var date = getTodayDate(new Date().setDate(new Date().getDate()));

    $(function () {
        $('#freezedKmDate').datetimepicker({
          format:'DD-MM-YYYY',
          useCurrent:true,
          pickTime: false,

          minDate: new Date,
          defaultDate : new Date,
      });
        
    });
    $(function () {
        $('#shiftDate').datetimepicker({
          format:'DD-MM-YYYY',
          useCurrent:true,
          pickTime: false,

          minDate: new Date,
          defaultDate : new Date,
      });
        
    });
    angular.forEach($scope.vehicleData,function(val, key){
    // $scope._editValue._freezedKmDate  =  $('#freezedKmDate').val();
    //   $scope._editValue._shiftDate  =  $('#shiftDate').val();
    
    if(val.shortName==shortname){
        $scope._editValue._shortName=shortname;

        $scope._editValue._regNo=val.regNo;
        $scope._editValue.vehiType=val.vehicleType;
        $scope._editValue.vehType       =  val.vehicleType;

        if(val.vehicleType=='heavyVehicle'){
            $('#vehitype span').text(translate('Heavy Vehicle'));
        }else if(val.vehicleType=='PassengerVan'){
            $('#vehitype span').text('Passenger Van');
        }else if(val.vehicleType=='Pickup'){
            $('#vehitype span').text('Pick-up');
        }  else {
            $('#vehitype span').text(val.vehicleType);
        }
        $scope._editValue.vehiModel=val.vehicleModel;
        $scope.index=$scope._editValue._vehiModelList.indexOf($scope._editValue.vehiModel);
        if($scope.index!=-1){
            if($scope._editValue.vehiModel=='Other'){
                $scope.index=$scope._editValue._vehiModelList.indexOf('Other');
                $scope._editValue.vehiModel =  $scope._editValue._vehiModelList[parseInt($scope.index)];
                $scope._editValue._vModel=val.vehicleModel;
                $scope._editValue._veModel=val.vehicleModel;
                $scope.showOthers=true;
            }
            else if($scope._editValue.vehiModel!=''){
                $scope._editValue.vehiModel =  $scope._editValue._vehiModelList[parseInt($scope.index)];
                $scope._editValue._vModel=$scope._editValue._vehiModelList[parseInt($scope.index)];
                $scope.showOthers=false;}

                else{

                    $(".selectpicker").selectpicker({
                        noneSelectedText : 'Please Select' 
                    });
                    $scope._editValue.vehiModel = $scope._editValue._vehiModelList[0];
                    $scope._editValue._vModel='-';
                    $scope.showOthers=false;

                }
            }         

            else {
                $(".selectpicker").selectpicker({
                    noneSelectedText : 'Please Select' 
                });
                $scope._editValue.vehiModel = $scope._editValue._vehiModelList[0];
                $scope._editValue._vModel='-';
                $scope.showOthers=false;
            }


            setTimeout(function () {
                $('.selectpicker').selectpicker('refresh');
            },500);

            setTimeout(function () {
                $('.selectpicker1').selectpicker('refresh');
            },500);
            $(".selectpicker1").selectpicker({
                noneSelectedText : 'Please Select' 
            });


            $scope._editValue._odoDista=val.odoMeter;
            if(val.maxSpeed==0 || val.maxSpeed==undefined || val.maxSpeed==null) {
             $scope._editValue._overSpeed  =  60;
         } else {
             $scope._editValue._overSpeed  =  val.maxSpeed;
         }
         $scope._editValue._driverName   =  val.driverName;
         if(val.freezedKm!=null && val.freezedKm!=undefined && val.freezedKm!=''){
             $scope._editValue._freezedKm =val.freezedKm;
             $scope.freezedKm=val.freezedKm;
         }
         else {
            $scope._editValue._freezedKm='';
            $scope.freezedKm='';
        }
        
        if(val.freezedKmDate==null || val.freezedKmDate==undefined || val.freezedKmDate==''){
            $scope._editValue._freezedKmDate=''; 
            $scope._editValue._freezekmdt=date;         
        }
        else {
           $scope._editValue._freezekmdt=val.freezedKmDate; 
           $scope._editValue._freezedKmDate=val.freezedKmDate;    
       }
       $scope.vehicleid=val.vehicleId;
       $scope._editValue._driverMobile =  val.driverMobile;
       $scope._editValue.licenceType    =  val.licenceType;
       $scope._editValue.routeName     =  val.routeName;
       $scope._editValue._rigMode =  val.rigMode;
       $scope._editValue._secondaryEngineOn =$scope.msToTime(val.secondaryEngineHours);
       $scope._editValue.madeIn         =  val.madeIn!=null?val.madeIn:'';
       $scope._editValue.manufactDate   =  val.manufacturingDate; 
       $scope._editValue.vehicleNumber     =  val.regNo; 
       $scope._editValue.chassisNo      =val.chassisNumber;
       $scope._editValue.devModel       =  val.deviceModel;
       $scope._editValue.deviceId       =  val.deviceId;
       $scope._editValue.gpsSimNo       =  val.gpsSimNo;
       $scope._editValue.onboardDate       =  val.onboardDate; 
       $scope._editValue.isFreezedKm       =  val.isFreezedKm;
       shifts=val.shiftData;
       var shiftn=[];


       angular.forEach(shifts,function(shift, key){
          shiftn.push(shift.shiftName);
      });
       $scope._editValue._shiftList=shiftn;
       if(val.shiftName!=null){
         $scope._editValue._shiftNames=val.shiftName;
         $scope.shiftnm=val.shiftName;
     }
     else {
        $scope._editValue._shiftNames=[];
        $scope.shiftnm='';
    }

    $scope._editValue._shiftName=$scope._editValue._shiftNames.toString()!=''?$scope._editValue._shiftNames.toString():'-';
    setTimeout(function () {
        $('.selectpicker2').selectpicker('refresh');
    },500);
    $(".selectpicker2").selectpicker({
        noneSelectedText : 'Please Select' 
    });
    if(val.shiftDate==null || val.shiftDate==undefined || val.shiftDate==''){
        $scope._editValue._shiftdt=date; 
        $scope._editValue._shiftDate='';  
    }
    else {
       $scope._editValue._shiftdt=val.shiftDate;
       $scope._editValue._shiftDate=val.shiftDate;    
   }
   $scope.description=val.description;

   if(val.safetyParking=='yes') {
      $scope.sparkType = 'Yes';
      $('#safePark span').text(translate('Yes'));
  } else if(val.safetyParking=='no') {
      $scope.sparkType = 'No';
      $('#safePark span').text(translate('No'));
  }
  if(localStorage.getItem('hideFuelProtocol')!="true"){
    $scope.enableLogShow = true;
    if(val.debug==true){

        $('#enablelog span').text('Enabled(24 hrs)');
    }
    else {

        $('#enablelog span').text('Disabled');
    }
}

}
});
$("#safeParkShow").show();
$('#viewable').show();
}

$scope.changeFreezeShift=function(type){
    var date = getTodayDate(new Date().setDate(new Date().getDate()));
    if(type=='freeze'){
        if($scope.freezedKm!=''){
            if($scope.freezedKm!=$scope._editValue._freezedKm){
                $('#freezedKmDate').val(date);      
            }
        }
        
        
    }
    else {
        var result=(JSON.stringify($scope._editValue._shiftNames)==JSON.stringify($scope.shiftnm));
        if($scope.shiftnm!=''){
            if(result==false){
                $('#shiftDate').val(date);
            }
        }
        
    }
}
$scope.toggleEdit=function(){
    $scope._editShow=true;
}

$scope.trimText=function(text){
    if(text!=undefined){
        return text.length > 20 ? text.substring(0, 20) + "..." : text;}
    }

    $scope.genericFunction = function(vehicleno, rowId, source,licenceExp,deviceModel,noOfTank,shortname) {
    //alert(source);
    //document.getElementById("searchBox").disabled = true;
    console.log('inside generic');
    $scope.showSendCmd=false;
    $scope.pwdErr = "";
    $scope.showSecondCommandInfo = false;
    $scope.selectedVehDeviceModel = "";
    $scope.res=0;
    $scope.command='SET-IP';
    if($scope.isCommandModalOpen){
        $scope.isCommandModalOpen = false;
        var link = document.getElementById('closeSendCmdModal');
        link.click();
    }
    if(source=='manualClick'||(localStorage.getItem('groupSelection')=='yes')){
      localStorage.setItem('manualClick', 'yes');
      $scope.noOfTank = noOfTank;
  }else{
     localStorage.setItem('manualClick', 'no');
 }
   // alert($scope.noOfTank);
   siteclear();
   $scope.vehiname=vehicleno;
   licenceExpiry=licenceExp;
   $scope.errMsg="";

    //alert($scope.vehiname);
    if(vehicleno == "null")
      return;
    //console.log('genericFunction..');
    //angular.forEach($scope.locations, function(value, key){
    //if($scope.zohoReports==undefined){  
    //console.log($scope.data02);
    localStorage.setItem('selectedVehicleId', vehicleno+"__"+rowId);
    sessionValue(vehicleno, $scope.gName,licenceExpiry);

    if(source) {
      $scope.customZoom  = 0;
      $scope.positionLng = 0;
      $scope.positionLat = 0;
      $scope.zoom        = 15;
  }
  $scope._editValue_con     =  true;
  $("#currenStatus").hide();
  individualVehicle         =  $filter('filter')($scope.data02, { vehicleId:vehicleno},true);
  setDeviceVolt(individualVehicle[0]);
      //alert($scope.SiteCheckbox.value1);
      if($scope.SiteCheckbox.value1=='YES'){
        polygenFunction($scope.data);

    }
    $scope.individualVehicle  =  individualVehicle[0];
    if($scope.individualVehicle.deviceModel=="GV05" || deviceModalList.includes($scope.individualVehicle.deviceModel)){
        if($scope.individualVehicle.status!='OFF'){
         $scope.showSendCmd=true;
     }
     $scope.selectedVehDeviceModel = $scope.individualVehicle.deviceModel;
     if($scope.individualVehicle.deviceModel=="GV05"){
        $scope.command='SET-IP';
    }else if(deviceModalList.includes($scope.individualVehicle.deviceModel)){
        $scope.showSecondCommandInfo = true;
        $scope.command='RESET';
    }
}
    //console.log(individualVehicle.length);

    if(individualVehicle.length>0) {

        if(individualVehicle.length==1){
            if(individualVehicle[0].fuel=="no"){
              document.getElementById("graphsId").style.width = "190px";
              $scope.vehiclFuel=false;
          }
        //$scope.clearMarkers();
        //console.log("Selected Value: " + $scope.vehicleStatus.id + "\nSelected Text: " + $scope.vehicleStatus.name);

        if(individualVehicle[0].position === "N" || individualVehicle[0].position === 'Z'){

          alert(individualVehicle[0].address);

      } else {

          if(individualVehicle[0].expired=="No"){
            $("#expiryMsg").modal('hide');

            $scope.singleVehicle  =  1;
              //console.log('zoom value..');

              $scope.Filter = 'SINGLE';
              //$scope.zoom   = 15;

              if($scope.customZoom != 0) {                  
                  $scope.zoom  = $scope.customZoom;
              }
              if(map_change==0) {
                 $scope.setMarkers(individualVehicle, individualVehicle[0].address);
             } else if(map_change==1) {
               $scope.setMarkers_osm(individualVehicle, individualVehicle[0].address);
           }

           $scope.assignValue(individualVehicle[0]);

       }
       else {
        $("#expiryMsg").modal('show');
    }
}


} else {

         // console.log('else...');

         for(var i=0; i<individualVehicle.length; i++) {

              // console.log(individualVehicle[i].shortName);
                 // console.log(vehicleno==individualVehicle[i].shortName);

                 if(vehicleno==individualVehicle[i].shortName) {

                    if(individualVehicle[i].position === "N" || individualVehicle[i].position === 'Z'){

                     alert(individualVehicle[i].address);

                 } else {

                  if(individualVehicle[0].expired=="No"){
                    $("#expiryMsg").modal('hide');

                    $scope.singleVehicle  =  1;

                    $scope.Filter  =  'SINGLE';
                    $scope.zoom    =  15;

                    if($scope.customZoom != 0) {                  
                        $scope.zoom  = $scope.customZoom;
                    }

                    var arrVal=[];
                    arrVal.push(individualVehicle[i]);

                    if(map_change==0) {
                     $scope.setMarkers(arrVal, individualVehicle[i].address);
                 } else if(map_change==1){
                   $scope.setMarkers_osm(arrVal, individualVehicle[i].address);
               }



               $scope.assignValue(individualVehicle[i]);
           }
           else {
            $("#expiryMsg").modal('show');
        }

    }


}
}

}

}
// getVehicleData(shortname);
    //document.getElementById("searchBox").disabled = false;
};

$('.nearbyTable').hide();
$scope.nearBy = function(){
    if($scope.nearbyflag == false){
      $('#myModal').modal();  
      $scope.nearbyflag=true;
  }else{
      $('.nearbyTable').hide();
      $scope.nearbyflag=false;
  }
}

$scope.setsTraffic = function(val) {
    if($scope.checkVal==false){
      document.getElementById(val).style.backgroundColor = "#fdfdb6"
      $scope.trafficLayer.setMap(map_goog);
      $scope.checkVal = true;
  }else{
      document.getElementById(val).style.backgroundColor = "#ffffff"
      $scope.trafficLayer.setMap(null);
      $scope.checkVal = false;
  }
}  

$scope.distance = function(val) {
  $scope.nearbyflag=false;
  $('.nearbyTable').hide();

  if($scope.clickflag==true){
    document.getElementById(val).style.backgroundColor = "#fdfdb6"
    $scope.clickflagVal = 0;
    $('#distanceVal').val(0);
    $scope.clickflag=false;
    for(var i=0; i<$scope.flightpathall.length; i++){
        $scope.flightpathall[i].setMap(null);
    }
} else {
 document.getElementById(val).style.backgroundColor = "yellow"
 $scope.clickflag=true;  
}
}

$scope.drawLine = function(loc1, loc2) {
    var flightPlanCoordinates = [loc1, loc2];
    $scope.flightPath = new google.maps.Polyline({
      path: flightPlanCoordinates,
      geodesic: true,
      strokeColor: "#ff0000",
      strokeOpacity: 1.0,
      strokeWeight:2
  });

    $scope.flightPath.setMap(map_goog);
    $scope.flightpathall.push($scope.flightPath);
    tempdistVal = parseFloat($('#distanceVal').val()) + parseFloat((google.maps.geometry.spherical.computeDistanceBetween(loc1,loc2)/1000).toFixed(2))
    $('#distanceVal').val(tempdistVal.toFixed(2));
}


$scope.getLocation = function(lat,lon, callback) {
     //console.log('getLocation.....');
     var latlng = new google.maps.LatLng(lat, lon);
     geocoderVar.geocode({'latLng': latlng}, function(results, status) {
          //console.log(results);
          if (status == google.maps.GeocoderStatus.OK) {
              if (results[1]) {
                if(typeof callback === "function") callback(results[1].formatted_address)
            }
        if(results[0]) {
            var newVals = vamoservice.googleAddress(results[0]);  
            saveAddressFunc(newVals,lat,lon);
        }
    }
});
 };


 $scope.infoContent = function(data){

  //console.log(data);

  var Name=translate("Name");
  var ACC_Status=translate("ACC_Status");
  var Loc_Time=translate("Loc_Time");
  var Comm_Time = translate("Comm_Time");
  var Today_Distance = translate("Today_Distance");
  var kms= translate("kms");
  var Reports=translate("Reports");
  var Track =translate("Track");
  var Site = translate("Site");
  var History = translate("History");
  var MultiTrack = translate("MultiTrack");
  var Load_Details= translate("Load_Details");
  var Temperature= translate("temperature");
  var tempshow=0;
  if(data.serial1=="temperature"){
        //alert('hhhh');
        tempshow=1;
    }

    var tempoTime = vamoservice.statusTime(data);
    if(data.ignitionStatus=='ON'){
        var classVal = 'green';
    }else{
        var classVal = 'red';
    }
    var isDGVehicle = (data.vehicleMode=='DG')?true:false;
    var isAc=(data.ac===true)?true:false;
    var islicenceTypeStarter = (data.licenceType=='Starter' || data.licenceType=='StarterPlus')?true:false;
    if(assLabel =="true") {
      if(data.licenceExpiry=="-"){
          var contentString = '<div style="width:auto;font-family:Lato;min-height:auto;">'
          +'<div><b class="_info_caption">'+$scope.vehiLabel+""+Name+'</b> - <span style="font-weight:bold;">'+data.shortName+'</span></div>'
        //+'<div><b >ODO Distance</b> - '+data.odoDistance+' <span style="font-size:10px;font-weight:bold;">kms</span></div>'
        +'<div ng-hide='+isDGVehicle+'><b class="_info_caption">'+Today_Distance+'</b> - '+data.distanceCovered+' <span style="font-size:10px;font-weight:bold;">'+kms+'</span></div>'
        +'<div><b class="_info_caption">'+translate(vamoservice.statusTime(data).tempcaption)+'</span></b> - '+vamoservice.statusTime(data).temptime+'</div>'
        +'<div><b class="_info_caption">'+ACC_Status+'</b> - <span style="color:'+classVal+'; font-weight:bold;">'+translate(data.ignitionStatus)+'</span> </div>'
        +'<div ng-if='+isAc+'><b class="_info_caption">'+ 'AC' + '</b> - <span style="color:'+classVal+'; font-weight:bold;">'+ (data.vehicleBusy === 'no'? 'OFF' : 'ON') +'</span> </div>'
        +'<div ng-show='+tempshow+'><b class="_info_caption">'+Temperature+'</b> - <span>'+data.temperature+' &#x2103;</span> </div>'
        +'<div><b class="_info_caption">'+Loc_Time+'</b> - <span>'+$filter('date')(data.date, "dd-MMM-yy HH:mm")+'</span> </div>'
        +'<div ><b class="_info_caption">'+Comm_Time+'</b> - <span>'+$filter('date')(data.lastComunicationTime, "dd-MMM-yy HH:mm")+'</span> </div>'
        +'<div style="padding-top:5px;"><a target="_blank" href="history?vid='+data.vehicleId+'&vg='+$scope.gName+'">'+Reports+'</a> &nbsp;&nbsp; <a href="../public/track?maps=replay&vehicleId='+data.vehicleId+'&gid='+$scope.gName+'" target="_blank">'+History+'</a> &nbsp;&nbsp; <a href="#" ng-click="addPoi('+data.latitude+','+data.longitude+')">'+Site+'</a>'+'</div>';
    }else{
       var contentString = '<div style="width:auto;font-family:Lato;min-height:auto;">'
       +'<div>'+ '<span style="color:red">'+data.licenceExpiry+'</span>'+'</div>'
       +'<div><b class="_info_caption">'+$scope.vehiLabel+""+Name+'</b> - <span style="font-weight:bold;">'+data.shortName+'</span></div>'
        //+'<div><b >ODO Distance</b> - '+data.odoDistance+' <span style="font-size:10px;font-weight:bold;">kms</span></div>'
        +'<div ng-hide='+isDGVehicle+'><b class="_info_caption">'+Today_Distance+'</b> - '+data.distanceCovered+' <span style="font-size:10px;font-weight:bold;">'+kms+'</span></div>'
        +'<div><b class="_info_caption">'+translate(vamoservice.statusTime(data).tempcaption)+'</span></b> - '+vamoservice.statusTime(data).temptime+'</div>'
        +'<div><b class="_info_caption">'+ACC_Status+'</b> - <span style="color:'+classVal+'; font-weight:bold;">'+translate(data.ignitionStatus)+'</span> </div>'
        +'<div ng-if='+isAc+'><b class="_info_caption">'+ 'AC' + '</b> - <span style="color:'+classVal+'; font-weight:bold;">'+ (data.vehicleBusy === 'no'? 'OFF' : 'ON') +'</span> </div>'
        +'<div ng-show='+tempshow+'><b class="_info_caption">'+Temperature+'</b> - <span>'+data.temperature+' &#x2103;</span> </div>'
        +'<div><b class="_info_caption">'+Lo_Time+'</b> - <span>'+$filter('date')(data.date, "dd-MMM-yy HH:mm")+'</span> </div>'
        +'<div ><b class="_info_caption">'+Comm_Time+'</b> - <span>'+$filter('date')(data.lastComunicationTime, "dd-MMM-yy HH:mm")+'</span> </div>'
        +'<div style="padding-top:5px;"><a target="_blank" href="history?vid='+data.vehicleId+'&vg='+$scope.gName+'">'+Reports+'</a> &nbsp;&nbsp; <a href="../public/track?maps=replay&vehicleId='+data.vehicleId+'&gid='+$scope.gName+'" target="_blank">'+History+'</a> &nbsp;&nbsp; <a href="#" ng-click="addPoi('+data.latitude+','+data.longitude+')">'+Site+'</a>'+'</div>';

    }


} else {
    if(data.licenceExpiry=="-"){
      if(data.report=="yes") {
          $scope.load=data.report;
          $scope.vehicleId=data.vehicleId;
          var contentString = '<div style="width:auto;font-family:Lato;min-height:auto;">'
          +'<div><b class="_info_caption">'+$scope.vehiLabel+ " "+Name+'</b> - <span style="font-weight:bold;">'+data.shortName+'</span></div>'
            //+'<div><b >ODO Distance</b> - '+data.odoDistance+' <span style="font-size:10px;font-weight:bold;">kms</span></div>'
            +'<div ng-hide='+isDGVehicle+'><b class="_info_caption">'+Today_Distance+'</b> - '+data.distanceCovered+' <span style="font-size:10px;font-weight:bold;">'+kms+'</span></div>'
            +'<div><b class="_info_caption">'+translate(vamoservice.statusTime(data).tempcaption)+'</span></b> - '+vamoservice.statusTime(data).temptime+'</div>'
            +'<div><b class="_info_caption">'+ACC_Status+'</b> - <span style="color:'+classVal+'; font-weight:bold;">'+translate(data.ignitionStatus)+'</span> </div>'
            +'<div ng-if='+isAc+'><b class="_info_caption">'+ 'AC' + '</b> - <span style="color:'+classVal+'; font-weight:bold;">'+ (data.vehicleBusy === 'no'? 'OFF' : 'ON') +'</span> </div>'
            +'<div ng-show='+tempshow+'><b class="_info_caption">'+Temperature+'</b> - <span>'+data.temperature+' &#x2103;</span> </div>'
            +'<div><b class="_info_caption">'+Loc_Time+'</b> - <span>'+$filter('date')(data.date, "dd-MMM-yy HH:mm")+'</span> </div>'
            +'<div ><b class="_info_caption">'+Comm_Time+'</b> - <span>'+$filter('date')(data.lastComunicationTime, "dd-MMM-yy HH:mm")+'</span> </div>'
            +'<div style="padding-top:5px;"><a target="_blank" href="history?vid='+data.vehicleId+'&vg='+$scope.gName+'">'+ Reports +'</a> &nbsp;&nbsp; <a href="../public/track?vehicleId='+data.vehicleId+'&track=single&maps=single" target="_blank">'+Track+'</a> &nbsp;&nbsp; <a href="../public/track?maps=replay&vehicleId='+data.vehicleId+'&gid='+$scope.gName+'" target="_blank">'+History+'</a> &nbsp;&nbsp; <a ng-if='+!islicenceTypeStarter+' href="../public/track?vehicleId='+data.vehicleId+'&vg='+$scope.gName+'&track=multiTrack&maps=mulitple" target="_blank">'+MultiTrack+'</a>&nbsp;&nbsp; <a href="#" ng-click="addPoi('+data.latitude+','+data.longitude+')">'+Site+'</a>&nbsp;&nbsp; <a href="#loadModal" data-toggle="modal" ng-click=getLoad(vehicleId) ng-show=(load=="yes")>'+Load_Details+'</a>'+'</div>';


        } else {
            var userNameVal       =  localStorage.getItem('userIdName').split(',');
            var userVals          =  userNameVal[1].split('"');
            var userName          =  userVals[0];
            var vehicleStatus=data.noDataStatus!=0?true:false;
            var nodatastatus=data.noDataStatus==1?'Power Disconnected':'Poor network connection';
            if(userName=='YASHRAJ'){
                var contentString = '<div style="width:auto;font-family:Lato;height:auto;">'
                +'<div><b class="_info_caption">'+$scope.vehiLabel+ " "+Name+'</b> - <span style="font-weight:bold;">'+data.shortName+'</span></div>'
            //+'<div><b >ODO Distance</b> - '+data.odoDistance+' <span style="font-size:10px;font-weight:bold;">kms</span></div>'
            +'<div ng-hide='+isDGVehicle+'><b class="_info_caption">'+Today_Distance+'</b> - '+data.distanceCovered+' <span style="font-size:10px;font-weight:bold;">'+kms+'</span></div>'
            +'<div><b class="_info_caption">'+translate(vamoservice.statusTime(data).tempcaption)+'</span></b> - '+vamoservice.statusTime(data).temptime+'</div>'
            +'<div ng-show='+vehicleStatus+'><b class="_info_caption">'+'Reason'+'</b> - <span style="font-size:12px;color:'+classVal+';">'+nodatastatus+'</span> </div>'

            +'<div><b class="_info_caption">'+ACC_Status+'</b> - <span style="color:'+classVal+'; font-weight:bold;">'+translate(data.ignitionStatus)+'</span> </div>'
            +'<div ng-if='+isAc+'><b class="_info_caption">'+ 'AC' + '</b> - <span style="color:'+classVal+'; font-weight:bold;">'+ (data.vehicleBusy === 'no'? 'OFF' : 'ON') +'</span> </div>'
             +'<div ng-show='+tempshow+'><b class="_info_caption">'+Temperature+'</b> - <span>'+data.temperature+' &#x2103;</span> </div>'
            +'<div><b class="_info_caption">'+Loc_Time+'</b> - <span>'+$filter('date')(data.date, "dd-MMM-yy HH:mm")+'</span> </div>'
            +'<div ><b class="_info_caption">'+Comm_Time+'</b> - <span>'+$filter('date')(data.lastComunicationTime, "dd-MMM-yy HH:mm")+'</span> </div>'
            +'<div style="padding-top:5px;"><a href="../public/track?vehicleId='+data.vehicleId+'&track=single&maps=single" target="_blank">'+Track+'</a>'+'</div>';
        }
        else {
            var contentString = '<div style="width:auto;font-family:Lato;height:auto;">'
            +'<div><b class="_info_caption">'+$scope.vehiLabel+ " "+Name+'</b> - <span style="font-weight:bold;">'+data.shortName+'</span></div>'
                //+'<div><b >ODO Distance</b> - '+data.odoDistance+' <span style="font-size:10px;font-weight:bold;">kms</span></div>'
                +'<div ng-hide='+isDGVehicle+'><b class="_info_caption">'+Today_Distance+'</b> - '+data.distanceCovered+' <span style="font-size:10px;font-weight:bold;">'+kms+'</span></div>'
                +'<div><b class="_info_caption">'+translate(vamoservice.statusTime(data).tempcaption)+'</span></b> - '+vamoservice.statusTime(data).temptime+'</div>'
                +'<div ng-show='+vehicleStatus+'><b class="_info_caption">'+'Reason'+'</b> - <span style="font-size:12px;color:'+classVal+';">'+nodatastatus+'</span> </div>'
                
                +'<div><b class="_info_caption">'+ACC_Status+'</b> - <span style="color:'+classVal+'; font-weight:bold;">'+translate(data.ignitionStatus)+'</span> </div>'
                +'<div ng-if='+isAc+'><b class="_info_caption">'+ 'AC' + '</b> - <span style="color:'+classVal+'; font-weight:bold;">'+ (data.vehicleBusy === 'no'? 'OFF' : 'ON') +'</span> </div>'
       +'<div ng-show='+tempshow+'><b class="_info_caption">'+Temperature+'</b> - <span>'+data.temperature+' &#x2103;</span> </div>'
                +'<div><b class="_info_caption">'+Loc_Time+'</b> - <span>'+$filter('date')(data.date, "dd-MMM-yy HH:mm")+'</span> </div>'
                +'<div ><b class="_info_caption">'+Comm_Time+'</b> - <span>'+$filter('date')(data.lastComunicationTime, "dd-MMM-yy HH:mm")+'</span> </div>'
                +'<div style="padding-top:5px;"><a target="_blank" href="history?vid='+data.vehicleId+'&vg='+$scope.gName+'">'+ Reports +'</a> &nbsp;&nbsp; <a href="../public/track?vehicleId='+data.vehicleId+'&track=single&maps=single" target="_blank">'+Track+'</a> &nbsp;&nbsp; <a href="../public/track?maps=replay&vehicleId='+data.vehicleId+'&gid='+$scope.gName+'" target="_blank">'+History+'</a> &nbsp;&nbsp; <a  ng-if='+!islicenceTypeStarter+' href="../public/track?vehicleId='+data.vehicleId+'&vg='+$scope.gName+'&track=multiTrack&maps=mulitple" target="_blank">'+MultiTrack+'</a>&nbsp;&nbsp; <a href="#" ng-click="addPoi('+data.latitude+','+data.longitude+')">'+Site+'</a>'+'</div>';
            }

        }
    }
    else{
      if(data.report=="yes") {
          $scope.load=data.report;
          $scope.vehicleId=data.vehicleId;
          var contentString = '<div style="width:auto;font-family:Lato;min-height:auto;">'
          +'<div>'+ '<span style="color:red">'+data.licenceExpiry+'</span>'+'</div>'
          +'<div><b class="_info_caption">'+$scope.vehiLabel+ " "+Name+'</b> - <span style="font-weight:bold;">'+data.shortName+'</span></div>'
            //+'<div><b >ODO Distance</b> - '+data.odoDistance+' <span style="font-size:10px;font-weight:bold;">kms</span></div>'
            +'<div ng-hide='+isDGVehicle+'><b class="_info_caption">'+Today_Distance+'</b> - '+data.distanceCovered+' <span style="font-size:10px;font-weight:bold;">'+kms+'</span></div>'
            +'<div><b class="_info_caption">'+translate(vamoservice.statusTime(data).tempcaption)+'</span></b> - '+vamoservice.statusTime(data).temptime+'</div>'
            +'<div><b class="_info_caption">'+ACC_Status+'</b> - <span style="color:'+classVal+'; font-weight:bold;">'+translate(data.ignitionStatus)+'</span> </div>'
            +'<div ng-if='+isAc+'><b class="_info_caption">'+ 'AC' + '</b> - <span style="color:'+classVal+'; font-weight:bold;">'+ (data.vehicleBusy === 'no'? 'OFF' : 'ON') +'</span> </div>'
             +'<div ng-show='+tempshow+'><b class="_info_caption">'+Temperature+'</b> - <span>'+data.temperature+' &#x2103;</span> </div>'
            +'<div><b class="_info_caption">'+Loc_Time+'</b> - <span>'+$filter('date')(data.date, "dd-MMM-yy HH:mm")+'</span> </div>'
            +'<div ><b class="_info_caption">'+Comm_Time+'</b> - <span>'+$filter('date')(data.lastComunicationTime, "dd-MMM-yy HH:mm")+'</span> </div>'
            +'<div style="padding-top:5px;"><a target="_blank" href="history?vid='+data.vehicleId+'&vg='+$scope.gName+'">'+ Reports +'</a> &nbsp;&nbsp; <a href="../public/track?vehicleId='+data.vehicleId+'&track=single&maps=single" target="_blank">'+Track+'</a> &nbsp;&nbsp; <a href="../public/track?maps=replay&vehicleId='+data.vehicleId+'&gid='+$scope.gName+'" target="_blank">'+History+'</a> &nbsp;&nbsp; <a  ng-if='+!islicenceTypeStarter+' href="../public/track?vehicleId='+data.vehicleId+'&vg='+$scope.gName+'&track=multiTrack&maps=mulitple" target="_blank">'+MultiTrack+'</a>&nbsp;&nbsp; <a href="#" ng-click="addPoi('+data.latitude+','+data.longitude+')">'+Site+'</a>&nbsp;&nbsp; <a href="#loadModal" data-toggle="modal" ng-click=getLoad(vehicleId) ng-show=(load=="yes")>'+Load_Details+'</a>'+'</div>';

            //+'<div style="padding-top:5px;"><a href="history?vid='+vehicleID+'&vg='+$scope.gName+'">Reports</a> &nbsp;&nbsp; <a href="../public/track?vehicleId='+vehicleID+'&track=single&maps=single" target="_blank">Track</a> &nbsp;&nbsp; <a href="../public/track?maps=replay&vehicleId='+vehicleID+'&gid='+$scope.gName+'" target="_blank">History</a> &nbsp;&nbsp; <a href="../public/track?vehicleId='+vehicleID+'&track=multiTrack&maps=mulitple" target="_blank">MultiTrack</a>&nbsp;&nbsp; <a href="#" ng-click="addPoi('+lat+','+lng+')">Save Site</a>'
            //+'<div style="overflow-wrap: break-word; border-top: 1px solid #eee">'+data.address+'</div>'


        } else {

          var contentString = '<div style="width:auto;font-family:Lato;min-height:auto;">'
          +'<div>'+ '<span style="color:red">'+data.licenceExpiry+'</span>' +'</div>'
          +'<div><b class="_info_caption">'+$scope.vehiLabel+ " "+Name+'</b> - <span style="font-weight:bold;">'+data.shortName+'</span></div>'
            //+'<div><b >ODO Distance</b> - '+data.odoDistance+' <span style="font-size:10px;font-weight:bold;">kms</span></div>'
            +'<div ng-hide='+isDGVehicle+'><b class="_info_caption">'+Today_Distance+'</b> - '+data.distanceCovered+' <span style="font-size:10px;font-weight:bold;">'+kms+'</span></div>'
            +'<div><b class="_info_caption">'+translate(vamoservice.statusTime(data).tempcaption)+'</span></b> - '+vamoservice.statusTime(data).temptime+'</div>'
            +'<div><b class="_info_caption">'+ACC_Status+'</b> - <span style="color:'+classVal+'; font-weight:bold;">'+translate(data.ignitionStatus)+'</span> </div>'
            +'<div ng-if='+isAc+'><b class="_info_caption">'+ 'AC' + '</b> - <span style="color:'+classVal+'; font-weight:bold;">'+ (data.vehicleBusy === 'no'? 'OFF' : 'ON') +'</span> </div>'
             +'<div ng-show='+tempshow+'><b class="_info_caption">'+Temperature+'</b> - <span>'+data.temperature+' &#x2103;</span> </div>'
            +'<div><b class="_info_caption">'+Loc_Time+'</b> - <span>'+$filter('date')(data.date, "dd-MMM-yy HH:mm")+'</span> </div>'
            +'<div ><b class="_info_caption">'+Comm_Time+'</b> - <span>'+$filter('date')(data.lastComunicationTime, "dd-MMM-yy HH:mm")+'</span> </div>'
            +'<div style="padding-top:5px;"><a target="_blank" href="history?vid='+data.vehicleId+'&vg='+$scope.gName+'">'+ Reports +'</a> &nbsp;&nbsp; <a href="../public/track?vehicleId='+data.vehicleId+'&track=single&maps=single" target="_blank">'+Track+'</a> &nbsp;&nbsp; <a href="../public/track?maps=replay&vehicleId='+data.vehicleId+'&gid='+$scope.gName+'" target="_blank">'+History+'</a> &nbsp;&nbsp; <a  ng-if='+!islicenceTypeStarter+' href="../public/track?vehicleId='+data.vehicleId+'&vg='+$scope.gName+'&track=multiTrack&maps=mulitple" target="_blank">'+MultiTrack+'</a>&nbsp;&nbsp; <a href="#" ng-click="addPoi('+data.latitude+','+data.longitude+')">'+Site+'</a>'+'</div>';

            //+'<div style="padding-top:5px;"><a href="history?vid='+vehicleID+'&vg='+$scope.gName+'">Reports</a> &nbsp;&nbsp; <a href="../public/track?vehicleId='+vehicleID+'&track=single&maps=single" target="_blank">Track</a> &nbsp;&nbsp; <a href="../public/track?maps=replay&vehicleId='+vehicleID+'&gid='+$scope.gName+'" target="_blank">History</a> &nbsp;&nbsp; <a href="../public/track?vehicleId='+vehicleID+'&track=multiTrack&maps=mulitple" target="_blank">MultiTrack</a>&nbsp;&nbsp; <a href="#" ng-click="addPoi('+lat+','+lng+')">Save Site</a>'
            //+'<div style="overflow-wrap: break-word; border-top: 1px solid #eee">'+data.address+'</div>'


        }
    }
}
var compiled = $compile(contentString)($scope);
     // var  drop1 = document.getElementById("ddlViewBy");
     // var drop_value1= drop1.options[drop1.selectedIndex].value;
     return compiled;
 };  


 function setId() {
    $("#minmax").show();
        // $("#efullscreen").hide();
        // $("#sidebar-wrapper").hide(500);
        $("#fullscreen").hide();

        $("#efullscreen").hide();
        $('#mapTable-mapList').hide(500);
        // $("#efullscreen").hide();

        $("#contentmin").hide(500);
        $("#sidebar-wrapper").hide(500);
        if(document.getElementById("wrapper")!=null)
            $scope.idinvoke = document.getElementById("wrapper").id;
        else if(document.getElementById("sidebar-fullscreen")!=null)
            $scope.idinvoke = document.getElementById("sidebar-fullscreen").id;
        else if(document.getElementById("mapList")!=null)
            $scope.idinvoke = document.getElementById("mapList").id;
        else if(document.getElementById("mapTable-mapList")!=null)
            $scope.idinvoke = document.getElementById("mapTable-mapList").id;
        else if(document.getElementById("tablelist")!=null)
            $scope.idinvoke = document.getElementById("tablelist").id;

    }

    $scope.starSplit    =   function(val){
      var splitVal;  

      if(val!=undefined){
          splitVal=val.split('<br>');
      }else{
          splitVal='No Address';
      }

      return splitVal;  
  }

  $("#notifyF").hide();
  $("#notifyS ").hide();

  $('#viewable').hide();
  $("#graphsId").hide();

  $("#safeParkShow").hide();
  $("#safEdits").show();
  $("#safeUps").hide();

  $("#safEdit").click(function(e){
    $('#safEdits').hide();
    $('#safeUps').show();
});

  $("#safeUp").click(function(e){
     $('#safEdits').show();
     $('#safeUps').hide();
 });

//added for enable protocol Report
$('#debugEdit').show();
$('#debugUpdate').hide();
$("#logEdit").click(function(e){
    $('#debugEdit').hide();
    $('#debugUpdate').show();
});
$("#logUpdate").click(function(e){
 $('#debugEdit').show();
 $('#debugUpdate').hide();
});

//added for notification
var getParkedIgnitionAlert=function(repData){  
   var ignition_list=[];
   var parked_list=[];
             var alarm_list=[];  //need to remove after alarm report
             var idle_list =[];
             for(i=0;i<repData.length;i++){
                if(repData[i].expired!="Yes"){
            //console.log(repData[i].position);
            //console.log(repData[i].parkedTime);
            var position=repData[i].position;
            var feedsData={}
            switch(position) {
             case "S":
             var time=repData[i].idleTime;
             status = "Idle";
             break;
             case "P":
             var time=repData[i].parkedTime;
             if (time<60000) {
                status= "Ignition OFF";
            }
            else {
               status = "Parked";
           }
           break;
           case "M":
           var time=repData[i].movingTime;
           if (time<60000) {
            status= "Ignition ON";
        }
        else {
           status = "Moving";
       }
       break;
       default:
       status="null";
       break;
   }
   if (status!="null"){
            //console.log(status);
            //console.log(convertMS(time));
            var duration=convertMS(time);
            var timeUTC=repData[i].date;
            var timeStr = timeUTC;
            //console.log(timeStr.toString());  //or use alert()
            feedsData['vehicleName'] = repData[i].shortName;
            feedsData['dateTime'] = timeStr;
            feedsData['location'] = repData[i].address;
            feedsData['duration']=duration;
            feedsData['status'] =status;
            if((status=="Ignition ON")||(status=="Ignition OFF"))
            {
               ignition_list.push(feedsData);
           }
           else if(status == "Parked"){
             parked_list.push(feedsData);
         }else if(status == "Idle"){
             idle_list.push(feedsData);
         }

     }
 }
}
  //alarm_list=alarmReport(repData); //need to add for alarm report
  //console.log("parked "+JSON.stringify(parked_list));
  //console.log("ignition "+JSON.stringify(ignition_list));
  var notifications=(parked_list).length+(ignition_list).length+(alarm_list).length+idle_list.length;
  //console.log(notifications);
  localStorage.setItem('totalnotification',notifications);
  console.log(localStorage.getItem('totalnotification'));
    // notifications=(parked_list).length+ignition_list+alarm_list;
    // console.log();
    return [ignition_list, parked_list,alarm_list,notifications,idle_list];
}
function convertMS(ms) {
  var d, h, m, s,ts;
  s = Math.floor(ms / 1000);
  m = Math.floor(s / 60);
  s = s % 60;
  h = Math.floor(m / 60);
  m = m % 60;
  d = Math.floor(h / 24);
  h = h % 24;
  t={ d: d, h: h, m: m, s: s };
  if(t.d!=0)
  {
      ts=d+"D:"+h+"H:"+m+"M";
  }
  else {
   if((t.m==0)&&(t.h==0))
   {
      ts=s+" sec";
  }
  else
  {
      ts=h+"H:"+m+"M:"+s+"S";
  }
}
return ts;
};

var gaugeOptions = {

    chart: {
        type: 'solidgauge',
        spacingBottom: -10,
        spacingTop: -40,
        spacingLeft: 0,
        spacingRight: 0,
    },

    title: null,

    pane: {
        center: ['50%', '90%'],
        size: '110%',
        startAngle: -90,
        endAngle: 90,
        background: {
            backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
            innerRadius: '60%',
            outerRadius: '100%',
            shape: 'arc'
        }
    },

    tooltip: {
        enabled: false
    },

    // the value axis
    yAxis: {
        stops: [
            [0.1, '#55BF3B'], // green
            [0.5, '#DDDF0D'], // yellow
            [0.9, '#DF5353'] // red
            ],
            lineWidth: 0,
            minorTickInterval: null,
        //tickAmount: 2,
        title: {
            y: -50
        },
        labels: {
            y: -100
        }
    },

    plotOptions: {
        solidgauge: {
            dataLabels: {
                y: 5,
                borderWidth: 0,
                useHTML: true
            }
        }
    }
};

$('#container-fuel').highcharts(Highcharts.merge(gaugeOptions, {
    yAxis: {
        min: 0,
        max: 300,
        title: { text: '' }
    },
    credits: { enabled: false },
    series: [{
        name: 'Speed',
        data: [fuelLtr],
        dataLabels: {
            format: '<div style="text-align:center"><span style="font-size:12px; font-weight:normal;color: #196481'+ '">Fuel - {y} Ltr</span><br/>',
                 // y: 25
             },
             tooltip: { valueSuffix: ' Ltr'}
         }]
     }));

setInterval(function () {
   if(sensor==1){
     var chartFuel = $('#container-fuel').highcharts(), point;
     if (chartFuel) {
      chartFuel.yAxis[0].update({max:tankSize});
      point = chartFuel.series[0].points[0];
      point.update(fuelLtr);
//            if(tankSize==0)
 //             tankSize =200;
 //           chartFuel.yAxis[0].update({
//          max: tankSize,
//      });

}
}
}, 1000);

//end controller

}])
.directive('tooltips', function ($document, $compile,$filter) {
  return {
    restrict: 'A',
    scope: true,
    link: function (scope, element, attrs) {
      var translate = $filter('translate');
      var tip = $compile('<span ng-class="tipClass">'+
        '<table class="tabStyles">'+
        '<tr ng-show="loc.expired==Yes">'+'<td colspan="4">'+translate('expired')+'</td></tr>'+
        '<tr ng-show=loc.licenceExpiry!="-" style="background-color: white;" ng-if="loc.expired!=Yes">'+'<td colspan="4"><span style="color:red">'+'{{loc.licenceExpiry}}'+'</span></td></tr>'+
        '<tr ng-hide="loc.expired==Yes">'+'<td colspan="2">'+'{{ loc.date | date:"yyyy-MM-dd HH:mm:ss" }}'+'</td>'+'<td colspan="2">'+'{{ loc.shortName }}'+'</td>'+'</tr>'+
        '<tr ng-hide="loc.expired==Yes">'+'<td>'+translate('Odo')+'</td>'+'<td>'+'{{ loc.odoDistance }}'+'</td>'+'<td>'+translate('Covered')+'</td>'+'<td>'+'{{ loc.distanceCovered}}'+'</td>'+'</tr>'+
        '<tr ng-hide="loc.expired==Yes">'+'<td>'+translate('Ignition')+'</td>'+'<td>'+'{{ loc.ignitionStatus }}'+'</td>'+'<td>'+translate('Max_Speed')+'</td>'+'<td>'+'{{ loc.overSpeedLimit }}'+'</td>'+'</tr>'+
        '<tr ng-hide="loc.expired==Yes">'+'<td>'+translate('Veh_Battery')+'</td>'+'<td>'+'{{ loc.powerStatus }}'+'</td>'+'<td>'+translate('Speed')+'</td>'+'<td>'+'{{loc.speed}}'+'</td>'+'</tr>'+
        '<tr ng-hide="loc.expired==Yes">'+'<td>'+translate('Dev_Battery')+'</td>'+'<td>'+'{{ loc.deviceStatus }}'+'%'+'</td>'+'<td>'+translate('Direction')+'</td>'+'<td>'+'{{loc.direction}}'+'</td>'+'</tr>'+
        '<tr ng-hide="loc.expired==Yes">'+'<td colspan="4">'+'{{ loc.address }}'+'</td></tr>'+
        '<tr ng-hide="loc.expired==Yes" ng-if=loc.expiryStatus!="no">'+'<td colspan="4"><span style="color:red">'+'{{ loc.expiryStatus }}'+'</span></td></tr>'+
        '</table>'+
        '</span>')(scope),
        tipClassName = 'tooltips',
        tipActiveClassName = 'tooltips-show';
        scope.tipClass = [tipClassName];
        scope.text = attrs.tooltips;

        if(attrs.tooltipsPosition) {
            scope.tipClass.push('tooltips-' + attrs.tooltipsPosition);
        }
        else {
         scope.tipClass.push('tooltips-down');
     }
      //$document.find('#sidebar-wrapper').append(tip);
      $document.find('#wrapper').append(tip);

      element.bind('mouseover', function (e) {
        tip.addClass(tipActiveClassName);

        var pos = e.target.getBoundingClientRect(),
        offset = tip.offset(),
        tipHeight = tip.outerHeight(),
        tipWidth = tip.outerWidth(),
        elWidth = pos.width || pos.right - pos.left,
        elHeight = pos.height || pos.bottom - pos.top,
        tipOffset = 10;

        if(tip.hasClass('tooltips-right')) {
          offset.top = pos.top - (tipHeight / 2) + (elHeight / 2);
          offset.left = 210;
      }
      else if(tip.hasClass('tooltips-left')) {
          offset.top = pos.top - (tipHeight / 2) + (elHeight / 2);
          offset.left = pos.left - tipWidth - tipOffset;
      }
      else if(tip.hasClass('tooltips-down')) {
          offset.top = pos.top + elHeight + tipOffset;
          offset.left = pos.left - (tipWidth / 2) + (elWidth / 2);
      }
      else {
          offset.top = pos.top - tipHeight - tipOffset;
          offset.left = pos.left - (tipWidth / 2) + (elWidth / 2);
      }

      tip.offset(offset);
  });

      element.bind('mouseout', function () {
        tip.removeClass(tipActiveClassName);
    });

      tip.bind('mouseover', function () {
        tip.addClass(tipActiveClassName);
    });

      tip.bind('mouseout', function () {
        tip.removeClass(tipActiveClassName);
    });


  }
}
});

$(document).ready(function(e) {

  $('.contentClose').click(function(){
    $('.topContent').fadeOut(100);
    $('.contentexpand').show();
});

  $('.contentexpand').click(function(){
    $('.topContent').fadeIn(100);
    $('.contentexpand').hide();
});
 // $( "#statusreport_dashboard" ).draggable();
 $( "#statusreport_dashboard" ).resizable({
    handles: {
        'n': '#handle'
    }
});

 $('.contentbClose').click(function(){ $('.bottomContent').fadeOut(100); });

 $(document).ready(function(){

    $('#minmax').click(function(){
        $('#contentmin').animate({
            height: 'toggle'
        },300);
            //alert($('#currenStatus').is(":visible"));
            if($('#viewable').is(":visible")==false){
              $("#currenStatus").show();
          }
          else if($('#currenStatus').is(":visible")==true){
             $("#currenStatus").hide();
         }else{
          $("#currenStatus").show();
      }


  });

});
 

 $(document).ready(function(){

  $('#draggable').hide();  
  $('#minmaxMarker').click(function(){

      $('#draggable').animate({
         height: 'toggle'
     },500);
  });
});
 

 


 /* var gaugeOptions = {
        chart: {
            type: 'solidgauge',
            // backgroundColor:'rgba(255, 255, 255, 0)',
            spacingBottom: -10,
          spacingTop: -40,
          spacingLeft: 0,
          spacingRight: 0,
        },
        title: null,
        pane: {
            center: ['50%', '90%'],
            size: '110%',
            startAngle: -90,
            endAngle: 90,
            background: {
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc',
            }
        },
        tooltip: {
            enabled: false
        },
        yAxis: {
            stops: [
                [0.1, '#55BF3B'],
                [0.5, '#DDDF0D'],
                [0.9, '#DF5353']
            ],
            lineWidth: 0,
            minorTickInterval: null,
            tickPixelInterval: 400,
            tickWidth: 0,
            title: {
                y: -50
            },
            labels: {
                y: -100
            }
        },
        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 5,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        }
    };
    */


});


apps.directive('ngEnter', function() {
    return function(scope, elem, attrs) {
      elem.bind("keydown keypress", function(event) {
        // 13 represents enter button
        if (event.which === 13) {
          scope.$apply(function() {
            scope.$eval(attrs.ngEnter);
        });

          event.preventDefault();
      }
  });
  };
});


function addr_search() {

  if(map_change==1){

    document.getElementById("searchOsm").style.display="block";
    osmSearch=1;

    var inp =  document.getElementById('inputOsm');

    $.getJSON('http://nominatim.openstreetmap.org/search?format=json&limit=5&q=' + inp.value, function(data) {

      var items = [];

      $.each(data, function(key, val) {
          items.push(
            "<li><a href='#' onclick='chooseAddr(" +
            val.lat + ", " + val.lon + ");return false;'>" + val.display_name +
            '</a></li>'
            );
      });

      $('#resultsOsm').empty();

      if (items.length != 0) {
          $('<p>', { html: "Search results:" }).appendTo('#resultsOsm');
          $('<ul/>', {
            'class': 'my-new-list',
            html: items.join('')
        }).appendTo('#resultsOsm');
      } else {
          $('<p>', { html: "No results found" }).appendTo('#resultsOsm');
      }

  });

}
}

document.getElementById("searchOsm").style.display="none";
document.getElementById("buttt").style.display="none";

document.getElementById("newLeaf").style.display="none";
document.getElementById("newLeafOsm").style.display="block";

var inputss = document.getElementById('inputOsm');

inputss.addEventListener("keyup", function(event) {
    event.preventDefault();
    if (event.keyCode === 13) {
        document.getElementById("buttt").click();
    }
});


function chooseAddr(lat, lng, type) {

  if(map_change==1) {

    document.getElementById("searchOsm").style.display="none";
    osmSearch=0;

    var location = new L.LatLng(lat, lng);
    map_osm.panTo(location);

    if (type == 'city' || type == 'administrative') {
      map_osm.setZoom(11);
  } else {
      map_osm.setZoom(13);
  }

}
} 
