//var app = angular.module('mapApp', ['ui.select']);
var speedSize='100%',speedCenter='100%',startangle=-90,endangle=90,sensor=1;
app.controller('mainCtrl',['$scope', '$http', 'vamoservice', '$q', '$filter','_global','$translate',function($scope, $http, vamoservice, $q, $filter, GLOBAL,$translate){
  var language=localStorage.getItem('lang');
  $scope.multiLang=language;
  $translate.use(language);
  $scope.videoLink="https://www.youtube.com/watch?v=dphG_zAhlJc&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=32";
  var translate = $filter('translate');
  var polygenColors;
  var polygon1,polygon2;
  var polygenColorsArr=[];
  var gmarkers             =  [];
  var ginfowindow          =  [];
  var gsmarker             =  [];
  var gsinfoWindow         =  [];
  var geoinfowindow        =  [];
  var contentString        =  [];
  var contentString01      =  [];
  var geomarker            =  [], geoinfo  =  [];
  var id;
  var osmSite              =  0;
  var gnumDeltas =10;
  var osmnumDeltas = 10;
  var fuelMachineUrl;
  $scope.noOfTank = 1;
  $scope.fuelLitreArr=[0,0,0,0,0];
  $scope.tankSizeArr=[0,0,0,0,0];
  $("#graphsId").hide();
  var vehicleChanged = 0;
  $scope.SiteCheckbox = {
   value1 : true,
   value2 : 'YES'
 }
 
 var userNameVal          =  localStorage.getItem('userIdName').split(',');
 var userVals             =  userNameVal[1].split('"');
 var userName             =  userVals[0];

 $scope.allroute=true;
 $scope.cityCirclecheck   =  false;

 $scope.locations         =  [];
 $scope.path              =  [];
 $scope.osmPath           =  [];
 $scope.polylinearr       =  [];
 $scope.polylineOsmColor  =  [];
 $scope.polyline1         =  [];
 $scope.tempadd01         =  '';
 $scope.cityCircle        =  [];
 $scope.geoMarkerDetails  =  {};
 var distance=0;
 var latLngdiff=0;
 var distanceInterval=0.5;
 var gDistanceInterval=0.5;
 var deg=0;
 var currZoom=13;
 var arrowMarker={};
 var removedArrow={};
 var gRemovedArrow ={};
 var gArrowMarker={};
 var gArrows={};
 var gzoom = 13;
 var gLatLngUrlData=[];
 $scope.hideArrows = {
   value1 : true,
   value2 : 'YES'
 }
 $scope.hideArrows.value1='YES';

 var assLabel             =  localStorage.getItem('isAssetUser');
 $scope.trvShow           =  localStorage.getItem('trackNovateView');

 $scope.vehiAssetView     =  true;

 $scope.vg       =  getParameterByName('gid').split(":")[0];
 console.log($scope.vg);

 if(assLabel=="true") {
  $scope.vehiLabel = "Asset";
  $scope.vehiAssetView  = false;
} else if(assLabel=="false") {
  $scope.vehiLabel = "Vehicle";
  $scope.vehiAssetView  = true;
} else {
  $scope.vehiLabel = "Vehicle";
  $scope.vehiAssetView  = true;
}

$scope.url              =   GLOBAL.DOMAIN_NAME+'/getVehicleLocations';
$scope.url_site         =   GLOBAL.DOMAIN_NAME+'/viewSite';
$scope.vehicle_list     =   [];
$scope.group_list = [];
$scope.markerValue      =   0;
$scope.popupmarker;

var vehicIcon           =   [];
var vehicsIcon          =   [];
var gmarkersOsm         =   [];
var infoWindowOsm       =   [];
var VehiType, myIcons, osmInterVal, polygonval, siteLength;

var siteDataCall        =   0;
var polygonInitOsm      =   0;
var polygonInitGoog     =   0;

$scope.lineCount_osm    =   0;
$scope.initGoogVal      =   0;
$scope.plotVal          =   0; 

  /* $('#pauseButton').hide();
     $('#playButton').hide();      
     $('#stopButton').hide();   
     $('#replayButton').hide(); */

     $('#playButton').prop('disabled', true);
     $('#replayButton').prop('disabled', true);
     $('#stopButton').prop('disabled', true);
     $('#pauseButton').prop('disabled', true);

     $scope.addTinyMarker_osm = function(data){
    //console.log(data);
    var tinyIcon, pinImage, latlng,addresss;

    angular.forEach(data.vehicleLocations,function(val,key){

      latlng = new L.LatLng(val.latitude,val.longitude);
      $scope.tinyMarkers  = new L.marker(latlng);
      //console.log(val.address);

      if(val.position =='P') {
        //pinImage = 'assets/imgs/'+pos.data.position+'.png';
        pinImage = 'assets/imgs/flag.png';
        posval = translate('Parked_Time');

        var contentString = '<h3 class="infoh3">'+$scope.shortVehiId+'</h3><div class="nearbyTable02">'
        +'<div><table cellpadding="0" cellspacing="0"><tbody>'
        +'<tr><td>{{ "Location" | translate }}</td><td>'+val.address+'</td></tr>'
        +'<tr><td>'+translate("Last_seen")+'</td><td>'+dateFormat(val.date)+'</td></tr>'
        +'<tr><td>'+posval+'</td><td>'+$scope.timeCalculate(val.parkedTime)+'</td></tr>'
        +'<tr><td>'+translate("trip_distance")+'</td><td>'+val.tripDistance+'</td></tr>'
        +'</table></div>';

      var infowindow = new L.popup({maxWidth:600/*,maxHeight: 170*/}).setContent(contentString);

      infoWindowOsm.push(infowindow);

      tinyIcon = L.icon({
        iconUrl: pinImage,
        iconAnchor:[11,20],
        popupAnchor: [0, -20],
      });

      $scope.tinyMarkers.bindPopup(infowindow);
      $scope.tinyMarkers.setIcon(tinyIcon);
    } 
      //else 

      if(val.position =='S') {

      //pinImage = 'assets/imgs/A_'+pos.data.direction+'.png';
      pinImage = 'assets/imgs/orange.png';
      posval = 'Idle Time';



      var contentString = '<h3 class="infoh3">'+$scope.shortVehiId+'</h3><div class="nearbyTable02">'
      +'<div><table cellpadding="0" cellspacing="0"><tbody>'
      +'<tr><td>'+translate("location")+'</td><td>'+val.address+'</td></tr>'
      +'<tr><td>'+translate("Last_seen")+'</td><td>'+dateFormat(val.date)+'</td></tr>'
      +'<tr><td>'+posval+'</td><td>'+$scope.timeCalculate(val.idleTime)+'</td></tr>'
      +'<tr><td>'+translate("trip_distance")+'</td><td>'+val.tripDistance+'</td></tr>'
      +'</table></div>';

    var infowindow = new L.popup({maxWidth:600/*,maxHeight: 170*/}).setContent(contentString);

    infoWindowOsm.push(infowindow);

    tinyIcon = L.icon({
      iconUrl: pinImage,
      iconAnchor:[11,20],
      popupAnchor: [0, -20],
    });

    $scope.tinyMarkers.bindPopup(infowindow);
    $scope.tinyMarkers.setIcon(tinyIcon);

  } 

      //else 

      if(val.position !='P' && val.position !='S') {
        pinImage = 'assets/imgs/trans.png';

        tinyIcon = L.icon({
         iconUrl: pinImage,
       });

        $scope.tinyMarkers.setIcon(tinyIcon);
      }

      gmarkersOsm.push($scope.tinyMarkers);
      $scope.tinyMarkers.addTo($scope.map_osm);

     // }

   });

  } 


  $scope.update = function(){
   startLoading();
   
   $scope.gName=$scope.groupSelected;
   var urlGroup = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+$scope.gName;
   console.log(urlGroup);
   $http.get(urlGroup).success(function(data){
     angular.forEach(data, function(val, key){
                  //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
                  //alert($scope.gName);
                  if($scope.gName == val.group){
                    $scope.gIndex = val.rowId;
                    $scope.groupid=$scope.gIndex;
                    $scope.vehiname   =  data[$scope.gIndex].vehicleLocations[0].vehicleId;
                    $scope.gName      =  data[$scope.gIndex].group;
                    console.log($scope.gName);
                    localStorage.setItem('user', JSON.stringify($scope.vehiname+','+$scope.gName)); 
                  }
                });

     var pageUrl='track?vehicleId='+$scope.vehiname+'&gid='+$scope.gName+'&maps=replay';
     console.log(pageUrl);
     $(location).attr('href',pageUrl);
   //stopLoading();
 });


 }




 $scope.addMarkerstart_osm = function(data) {

  var startFlagVal = new L.LatLng(data.latitude, data.longitude);
  $scope.map_osm.panTo(startFlagVal);
  var myIconStart = L.icon({
    iconUrl: 'assets/imgs/startflag.png',
    iconSize: [68,70], 
    iconAnchor:[43,68],
    popupAnchor: [0, -65],
  });

  var contentString_osm = '<h3 class="infoh3">'+$scope.vehiLabel+' Details ('+$scope.shortVehiId+') </h3><div class="nearbyTable02"><div>'
  +'<table cellpadding="0" cellspacing="0">'
  +'<tbody><!--<tr><td>'+translate("location")+'</td><td>'+$scope.tempadd+'</td></tr>-->'
  +'<tr><td>'+translate("Last_seen")+'</td><td>'+dateFormat(data.date)+'</td></tr>'
  +'<tr><td>'+translate("Parked_Time")+'</td><td>'+$scope.timeCalculate(data.parkedTime)+'</td></tr>'
  +'<tr><td>'+translate("trip_distance")+'</td><td>'+data.tripDistance+'</td></tr></table></div>';

  $scope.markerStart     = new L.marker();
  $scope.infowindowStart = new L.popup(/*{maxWidth:400,maxHeight: 170}*/);

  $scope.infowindowStart.setContent(contentString_osm);
  $scope.markerStart.bindPopup($scope.infowindowStart);
  $scope.markerStart.setIcon(myIconStart);
  $scope.markerStart.setLatLng(startFlagVal).addTo($scope.map_osm);

}  

$scope.endMarkerstart_osm = function(data){

  var endFlagval  = new L.LatLng(data.latitude, data.longitude); 

  var myIconEnd = L.icon({
    iconUrl: 'assets/imgs/endflag.png',
    iconSize: [68,70], 
    iconAnchor:[22,70],
    popupAnchor: [3, -65],
  });

  var contentString_osm2 = '<h3 class="infoh3">'+$scope.vehiLabel+' '+translate("Details")+' ('+$scope.shortVehiId+') </h3><div class="nearbyTable02"><div>'
  +'<table cellpadding="0" cellspacing="0">'
  +'<tbody><!--<tr><td>{{ "Location" | translate }}</td><td>'+$scope.tempadd+'</td></tr>-->'
  +'<tr><td>'+translate("Last_seen")+'</td><td>'+dateFormat(data.date)+'</td></tr>'
  +'<tr><td>'+translate("Parked_Time")+'</td><td>'+$scope.timeCalculate(data.parkedTime)+'</td></tr>'
  +'<tr><td>'+translate("trip_distance")+'</td><td>'+data.tripDistance+'</td></tr></table></div>';

  $scope.markerEnd     = new L.marker();
  $scope.infoWindowEnd = new L.popup(/*{maxWidth:400,maxHeight: 170}*/);

  $scope.infoWindowEnd.setContent(contentString_osm2);
  $scope.markerEnd.bindPopup($scope.infoWindowEnd);
  $scope.markerEnd.setIcon(myIconEnd);
  $scope.markerEnd.setLatLng(endFlagval).addTo($scope.map_osm);
} 


$scope.splitfuel= function(inputString) {
  const resultString = inputString.replace(/:/g, ',');
  return resultString;
}



function utcdateConvert(milliseconds){
        //var milliseconds=1440700484003;
        var offset='+10';
        var d = new Date(milliseconds);
        utc = d.getTime() + (d.getTimezoneOffset() * 60000);
        nd  = new Date(utc + (3600000*offset));
        var result=nd.toLocaleString();
        return result;
      }

      function currentTimes() {
        var date = new Date();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
          hours = hours ? hours : 12; // the hour '0' should be '12'
          minutes = minutes < 10 ? '0'+minutes : minutes;
          var strTimes = hours + ':' + minutes + ' ' + ampm;

          return strTimes;
        }

        function getTodayDatesss() {
          var date = new Date();
          return ("0" + (date.getDate())).slice(-2)+'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+date.getFullYear();
        };


        function getTimeVal(val) {
          var date      =  new Date(val);
          var hours     =  date.getHours();
          var minutes   =  date.getMinutes();
          var ampm      =  hours >= 12 ? 'PM' : 'AM';
          hours       =  hours % 12;
          hours       =  hours ? hours : 12; // the hour '0' should be '12'
          minutes     =  minutes < 10 ? '0'+minutes : minutes;
          var strTimes  =  hours + ':' + minutes + ' ' + ampm;

          return strTimes;
        }


        function formatAMPM(date) {
          var date = new Date(date);
          var hours = date.getHours();
          var minutes = date.getMinutes();
          var ampm = hours >= 12 ? 'PM' : 'AM';
          hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        minutes = minutes < 10 ? '0'+minutes : minutes;
        var strTime = hours + ':' + minutes + ' ' + ampm;
        return strTime;
      }


      $scope.initGoogle_Map = function(data) {
        if(data.vehicleLocations==null){
         $("#graphsId").hide(); 
       }else{
        $("#graphsId").show();

        //console.log('google init...');
        if(data.vehicleLocations.length != 0) {
                    //$scope.initGoogVal=1;
                    //console.log(parseInt(locs.tripDistance));
                    if(parseInt(data.tripDistance) > 200) {
                      $scope.zoomCtrlVal     =  true;
                      data.zoomLevel        =  12;
                      $scope.ZoomDisableBtn  =  true;
                      $scope.scrollsWheel    =  false;
                       // console.log(locs.tripDistance+"Active no Zoom");
                     } else{
                      $scope.zoomCtrlVal    = false;
                      $scope.ZoomDisableBtn = false;
                      $scope.scrollsWheel   = true;
                    }

                   /* var myOptions = {
                        zoom: Number(locs.zoomLevel),zoomControlOptions: { position: google.maps.ControlPosition.LEFT_TOP}, 
                        center: new google.maps.LatLng(data.vehicleLocations[0].latitude, data.vehicleLocations[0].longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                      //styles: [{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}]
                    }; */

                    var myOptions = {
                      zoom: 13, /*Number(data.zoomLevel)*/
                        // zoomControl: $scope.scrollsWheel,
                        zoomControl: true,
                        zoomControlOptions: { position: google.maps.ControlPosition.LEFT_TOP}, 
                        center: new google.maps.LatLng(data.vehicleLocations[0].latitude, data.vehicleLocations[0].longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        // disableDoubleClickZoom: $scope.zoomCtrlVal,
                        // scrollwheel: $scope.scrollsWheel,
                        // disableDefaultUI: $scope.ZoomDisableBtn,
                     // styles: [{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}]
                   };

                   $scope.map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
                   $scope.map.setOptions({maxZoom:/*For example*/25});

              //  }else{
              //    alert("Data Not Found!..");
              //    console.log('vehicleLocations not found!...'+data);
              //  }

              google.maps.event.addListener($scope.map, 'click', function(event) {
                $scope.clickedLatlng = event.latLng.lat() +','+ event.latLng.lng();
                $('#latinput').val($scope.clickedLatlng);
              });



              $scope.polylineCtrl();
               // console.log($scope.path.length);

               if($scope.getValue != undefined){

                $scope.intervalValue = setInterval($scope.getValueCheck($scope.getValue), 25000);
              }
              
              $scope.pointDistances = [];
              var sphericalLib     = google.maps.geometry.spherical;
              var pointZero        = $scope.path[0];
                //var wholeDist        = sphericalLib.computeDistanceBetween(pointZero, $scope.path[$scope.path.length - 1]);

                $('#replaybutton').removeAttr('disabled');

            //}else{
            //  $('.error').show();
            //  $('#lastseen').html('<strong>From Date & time :</strong> -');
            //  $('#lstseendate').html('<strong>To  &nbsp; &nbsp; Date & time :</strong> -');
          // }
        //  }

      }


      var url = GLOBAL.DOMAIN_NAME+'/getGeoFenceView?vehicleId='+$scope.trackVehID;

      $scope.createGeofence(url);

      if(userName=="LANDT" || userName=="CMTL" || userName=="RPTL"){
        landtDraw();
      } 
      stopLoading();

           //}

          // $scope.SiteCheckbox = {
          //    value1 : true,
          //    value2 : 'YES'
          // }
        }
      }


      var infoBoxs = [];
      function circleData(data) {
    //console.log(data.circlePoints);   
    var circData = data.rectanglePoints; 

    if(circData.length!=undefined){
      lastArr = "";
      prevLoc = ""; 
      for(var i=0; i<circData.length; i++) {
        //console.log(circData[i]);
        var secArr = circData[i];            
        //console.log(secArr.length); 
        var secArrLast = secArr.length-1;
        var  polygenList = [],circleList  = [], polygenList1 = [];

        for(var j=0; j<secArr.length; j++){

          if(secArr[j].location!=""){

            //console.log(secArr[j]);
            var valueSplit = secArr[j].location.split(",");
            var latCirc    = valueSplit[0];
            var lngCirc    = valueSplit[1];
            if(osmSite == 1)
            {
                /* var circle1 = new  L.circleMarker([latCirc, lngCirc], {
                    color: 'transparent',
                    fillColor: '#f03',
                    fillOpacity: 0,
                    radius: 50 
                  }).bindLabel(secArr[j].locationName, { noHide: true, className : "markerLabels" }).addTo($scope.map_osm);*/

                  var circle = new  L.circle([latCirc, lngCirc], 50,{
                    color: 'red',
                    fillColor: '#f03',
                    fillOpacity: 0,
                    radius: 1 
                  }).addTo($scope.map_osm);
                //circle.bindTooltip(secArr[j].locationName,{permanent:true,sticky:true}).openTooltip();
                circleList.push(circle);
                if(j==0) {
                  polygenList.push(  [circleList[0].getBounds().getSouthWest().lat+0.0001,circleList[0].getBounds().getCenter().lng+0.002] , 
                    [circleList[0].getBounds().getCenter().lat+0.002,circleList[0].getBounds().getSouthWest().lng+0.0001]  );
                } else if(j==secArrLast) {
                //console.log(secArrLast);
                polygenList.push( [ circleList[secArrLast].getBounds().getNorthEast().lat+0.0001,circleList[secArrLast].getBounds().getCenter().lng-0.002 ],
                  [ circleList[secArrLast].getBounds().getCenter().lat-0.002,circleList[secArrLast].getBounds().getNorthEast().lng+0.0001 ] );
              }

              if(j==1 && lastArr!="" && prevLoc == secArr[j].state)
              {
                polygenList1.push([ circleList[1].getBounds().getSouthWest().lat+0.0001,circleList[1].getBounds().getCenter().lng+0.002 ], 
                  [ circleList[1].getBounds().getCenter().lat+0.002,circleList[1].getBounds().getSouthWest().lng+0.0001 ]);

                polygenList1.push([ lastArr.getBounds().getNorthEast().lat+0.0001,lastArr.getBounds().getCenter().lng-0.002 ],
                  [ lastArr.getBounds().getCenter().lat-0.002,lastArr.getBounds().getNorthEast().lng+0.0001 ])
              }
              if(j==secArrLast)
              {
                lastArr = circleList[secArrLast-1];
                prevLoc = secArr[j].state;

              } 
            }
            else{
              var circleClass = new google.maps.Circle({
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0,
                map: $scope.map,
                center: new google.maps.LatLng(latCirc,lngCirc),
                radius: 50
              }); 

              var myOptions = {
                content: secArr[j].locationName,
                boxStyle: {
                  background: '#FFFFFF',
                  color: '#000000',
                  textAlign: "center",
                  fontSize: "8pt",
                  fontWeight:"bold",
                  width: "60px",
                  border:"0.5px solid black",
                  padding:"2px 2px 2px 2px",
                  borderRadius:"6px"
                },
                disableAutoPan: true,
                pixelOffset: new google.maps.Size(-25, -10), // left upper corner of the label
                position: new google.maps.LatLng(latCirc,lngCirc),
                closeBoxURL: "",
                isHidden: true,
                pane: "floatPane",
                zIndex: 100,
                enableEventPropagation: true
              };

              var ib = new InfoBox(myOptions);

              infoBoxs.push(ib); 
              ib.open($scope.map);

              circleList.push(circleClass);

             //console.log(j);
            // if(osmSite == 1)
             // {
             // }
             // else{ 
              if(j==0) {
                polygenList.push( new google.maps.LatLng( circleList[0].getBounds().getSouthWest().lat()+0.0001,circleList[0].getCenter().lng()+0.002 ), 
                  new google.maps.LatLng( circleList[0].getCenter().lat()+0.002,circleList[0].getBounds().getSouthWest().lng()+0.0001 ) );
              } else if(j==secArrLast) {
              //console.log(secArrLast);
              polygenList.push( new google.maps.LatLng( circleList[secArrLast].getBounds().getNorthEast().lat()+0.0001,circleList[secArrLast].getCenter().lng()-0.002 ),
                new google.maps.LatLng( circleList[secArrLast].getCenter().lat()-0.002,circleList[secArrLast].getBounds().getNorthEast().lng()+0.0001 ) );
            }

            if(j==1 && lastArr!="" && prevLoc == secArr[j].state)
            {
              polygenList1.push(new google.maps.LatLng( circleList[1].getBounds().getSouthWest().lat()+0.0001,circleList[1].getCenter().lng()+0.002 ), 
                new google.maps.LatLng( circleList[1].getCenter().lat()+0.002,circleList[1].getBounds().getSouthWest().lng()+0.0001 ));

              polygenList1.push(new google.maps.LatLng( lastArr.getBounds().getNorthEast().lat()+0.0001,lastArr.getCenter().lng()-0.002 ),
                new google.maps.LatLng( lastArr.getCenter().lat()-0.002,lastArr.getBounds().getNorthEast().lng()+0.0001 ))
            }
            if(j==secArrLast)
            {
              lastArr = circleList[secArrLast-1];
              prevLoc = secArr[j].state;

            }
          }
              //}
             /* if(j==0) {
                polygenList.push( new google.maps.LatLng( circleList[0].getBounds().getSouthWest().lat(),circleList[0].getCenter().lng() ), 
                  new google.maps.LatLng( circleList[0].getCenter().lat(),circleList[0].getBounds().getSouthWest().lng() ) );
                } else */
             /**** if(j==secArrLast) {
              //console.log(secArrLast);
                polygenList.push( new google.maps.LatLng( circleList[secArrLast].getBounds().getSouthWest().lat()+0.0001,circleList[0].getCenter().lng()+0.002 ),
                  new google.maps.LatLng( circleList[0].getCenter().lat()+0.002,circleList[0].getBounds().getSouthWest().lng()+0.0001 ) );

                polygenList.push( new google.maps.LatLng( circleList[0].getBounds().getNorthEast().lat()+0.0001,circleList[0].getCenter().lng()-0.002 ), 
                  new google.maps.LatLng( circleList[0].getCenter().lat()-0.002,circleList[0].getBounds().getNorthEast().lng()+0.0001 ) );
              }****/ /*else {
                console.log('else');
                  polygenList.push( new google.maps.LatLng( circleList[j].getBounds().getNorthEast().lat(),circleList[j].getCenter().lng()), 
                  new google.maps.LatLng( circleList[j].getBounds().getSouthWest().lat(), circleList[j].getCenter().lng() ) );
                }*/
              }
            }
            if(osmSite == 1)
            {
              var polygon = L.polygon(polygenList,{color: '#FFFF00',
                fillColor: '#f03',
                fillOpacity: 0
              }).addTo($scope.map_osm);
              var polygon = L.polygon(polygenList1,{color: '#BDD7EE',
                fillColor: '#f03',
                fillOpacity: 0
              }).addTo($scope.map_osm);
            }
            else{ 
              polygon1 = new google.maps.Polygon({
                path: polygenList,
                  strokeColor: "#FFFF00",//7e7e7e
                  strokeWeight: 3,
                  fillColor: "#000",
                  fillOpacity: 0,
                  map: $scope.map
                }); 

              polygon2 = new google.maps.Polygon({
                path: polygenList1,
                  strokeColor: "#BDD7EE",//7e7e7e
                  strokeWeight: 3,
                  fillColor: "#000",
                  fillOpacity: 0,
                  map: $scope.map
                }); 
              polygon1.setMap($scope.map);
              polygon2.setMap($scope.map);

            }
          }

        }
      }
      function landtDraw() {
      //var landtVar = 'http://188.166.244.126:9000/viewSiteForAssetLandt?userId=LANDT';
      console.log(GLOBAL.DOMAIN_NAME+'/viewSiteForAssetLandt');
      var landtVar = GLOBAL.DOMAIN_NAME+'/viewSiteForAssetLandt';

      $http({
        method : "GET",
        url : landtVar
      }).then(function mySuccess(response) {
        console.log(response.data);
        circleData(response.data);

      }, function myError(response) {
        console.log( response.statusText );
      });

    }
    $scope.initOsm_Map=function(data){

      if(polygonInitOsm==1){
        polygonInitOsm=0;
      }


      if($scope.map_osm!=undefined){

      // console.log("osm map undefined....");
      // $scope.map_osm.panTo(null);
      // window.clearInterval(timeInterval);  

      if($scope.markerss){
        $scope.map_osm.removeLayer($scope.markerss);
      }

      if($scope.polylines_osm){
       $scope.map_osm.removeLayer($scope.polylines_osm);
     }

      /* delete $scope.markerss;
         delete $scope.polylines_osm;
         delete myIcons; */
         
         $scope.map_osm.removeLayer($scope.tileOsm);
         $scope.map_osm.remove();

      /* delete $scope.map_osm;
         delete $scope.tileOsm;
         delete osmInterVal; */

         var osmInterVal;
         var myIcons;
         gmarkersOsm     = [];
         infoWindowOsm   = [];

         //stopLoading();
       } 
       
       // if($scope.map_osm ==undefined){
       // console.log('map_osm');

       var mapLink    =  '<a href="https://osm.vamosys.com/nominatim/lf.html">OpenStreeetMap</a>';
       $scope.map_osm = new L.map('map_canvas2',{ center: [13.0827,80.2707],/* minZoom: 4,*/zoom: 13,maxZoom: 25});

       $scope.tileOsm = new L.tileLayer(
         'https://osm.vamosys.com/osm_tiles/{z}/{x}/{y}.png', {
           attribution: '&copy; '+mapLink+' Contributors',
            // maxZoom: 18,
          }).addTo($scope.map_osm);
          //add no of arrows based on map zoom 
          var prevZoom = $scope.map_osm.getZoom();

          $scope.map_osm.on('zoomend',function(e){
      //alert('hello');
      debugger;
      currZoom = $scope.map_osm.getZoom();
      var diff = prevZoom - currZoom;
      markersLen=arrowMarker.length;
      for(key in arrowMarker) {
       $scope.map_osm.removeLayer(arrowMarker[key]);
     }
     arrowMarker = {};

     if(diff > 0){
        //alert('zoomed out'+diff);
        if(currZoom==13){
          distanceInterval=2;
            //if(estInt)distanceInterval=estInt;
            $scope.polylines_osm.setStyle({weight:4});
          }
          else{
            // if(currZoom>=13){
            //     distanceInterval=distanceInterval*2;
            // }else{
              distanceInterval=distanceInterval*2;
            //}
          }

        } else if(diff < 0) {
        //alert('zoomed in'+diff);
        if(currZoom==13){
          distanceInterval=2;
          $scope.polylines_osm.setStyle({weight:4});
              //if(estInt)distanceInterval=estInt;
            }
            else{
              //  if(currZoom>=13){
              //     distanceInterval=distanceInterval/2;
              // }else{
                distanceInterval=distanceInterval/2;
                  //alert(distanceInterval);
              //}
            }
          }
          prevZoom = currZoom;
        //before live arrow
       // var arrowIcon = L.icon({
       //            iconUrl: 'assets/imgs/arrow.png',
       //            iconSize: [15,15], 
       //                    iconAnchor:[5,10],
       //            popupAnchor: [0, -65],
       //            className: 'u-turn-icon'
       //            /*rotateWithView: true,
       //            rotation: 180*/

       //        });
       if(currZoom>=18){
        $scope.polylines_osm.setStyle({weight:2});
        var arrowIcon = L.icon({
          iconUrl: 'assets/imgs/arrow.png',
          iconSize: [10,10], 
          iconAnchor:[5,5],
          popupAnchor: [0, -65],
          className: 'u-turn-icon'
                      /*rotateWithView: true,
                      rotation: 180*/

                    });
      }else{    
        $scope.polylines_osm.setStyle({weight:4});
        var arrowIcon = L.icon({
          iconUrl: 'assets/imgs/arrow.png',
          iconSize: [13,13], 
          iconAnchor:[5,7],
          popupAnchor: [0, -65],
          className: 'u-turn-icon'
                  /*rotateWithView: true,
                  rotation: 180*/

                });
      }
      var latLngUrlData=$scope.hisloc.vehicleLocations;
      for (var i = 0; i < latLngUrlData.length; i++) {

        if(i!=0){
         var dx = latLngUrlData[i].latitude - latLngUrlData[i-1].latitude;
         var dy = latLngUrlData[i].longitude - latLngUrlData[i-1].longitude;
         deg = Math.atan2(dy, dx)* 180 / Math.PI;
       }


            //  if(i!=0&&currZoom>=7){
            //   var startFlagVal = new L.LatLng(latLngUrlData[i-1].latitude,latLngUrlData[i-1].longitude);
            //     $scope.markerStart     = new L.marker(startFlagVal,{icon: arrowIcon,rotationAngle: deg-45});
            //     latLngdiff=calcCrow(latLngUrlData[i-1].latitude,latLngUrlData[i-1].longitude,latLngUrlData[i].latitude,latLngUrlData[i].longitude);
            //     distance=parseFloat(latLngdiff)+parseFloat(distance);
            //     //alert(distance.toFixed(2));
            //     if(distance>distanceInterval){
            //         //alert(distance);
            //       //alert(distanceInterval);
            //       distance=0;
            //       // $scope.markerStart     = new L.marker(startFlagVal,{icon: arrowIcon,rotationAngle: deg-45});
            //       arrowMarker[i]=$scope.markerStart;
            //       if($scope.hideArrows.value1=='YES'){
            //       $scope.map_osm.addLayer($scope.markerStart);
            //        }
            //    }

            // }
            if(i!=0&&currZoom>=7){
              latLngdiff=calcCrow(latLngUrlData[i-1].latitude,latLngUrlData[i-1].longitude,latLngUrlData[i].latitude,latLngUrlData[i].longitude);
              distance=parseFloat(latLngdiff)+parseFloat(distance);
              var zoomlatlnd=middlePoint(latLngUrlData[i-1].latitude,latLngUrlData[i-1].longitude,latLngUrlData[i].latitude,latLngUrlData[i].longitude);
              var startFlagVal = new L.LatLng(zoomlatlnd[0],zoomlatlnd[1]);
              var startPos = new L.LatLng(latLngUrlData[i-1].latitude,latLngUrlData[i-1].longitude);
              var endPos =new L.LatLng(latLngUrlData[i].latitude,latLngUrlData[i].longitude);
                //var startFlagVal = new L.LatLng(latLngUrlData[i-1].latitude,latLngUrlData[i-1].longitude);
                $scope.markerStart     = new L.marker(startFlagVal,{icon: arrowIcon,rotationAngle: deg-45});
                
                //alert(distance.toFixed(2));

                if(currZoom>=18){
                  //alert('hello');
                  if((latLngdiff/distanceInterval)>=1){
                   multipleDistance=latLngdiff/(distanceInterval*4)
                   var numDeltas =  Math.floor(multipleDistance/2);
                     //alert(numDeltas)
                     var j = 0;
                     var deltaLat;
                     var deltaLng;
                     position = startPos;
                     if(numDeltas>=2){
                      miltiMarker(endPos);
                    }else{
                      if((latLngdiff/distanceInterval)>=1){
                        arrowMarker[i]=$scope.markerStart;
                        if($scope.hideArrows.value1=='YES'){
                          $scope.map_osm.addLayer($scope.markerStart);
                        }
                      }
                    }

                    function miltiMarker(result){
                     j = 1;
                     deltaLat = (result.lat - position.lat)/numDeltas;
                     deltaLng = (result.lng - position.lng)/numDeltas;
                     setMarker();
                   }

                   function setMarker(){
                     position.lat += deltaLat;
                     position.lng += deltaLng;
                               //var latlng = new google.maps.LatLng(position[0], position[1]);
                               //marker.setPosition(latlng);
                               //alert(position);
                               $scope.markerStart     = new L.marker(position,{icon: arrowIcon,rotationAngle: deg-45});
                               //$scope.markerStart.setLatLng([position.lat, position.lng]);
                               arrowMarker[j+'many'+i]=$scope.markerStart;
                               if($scope.hideArrows.value1=='YES'){
                                $scope.map_osm.addLayer($scope.markerStart);
                              }
                              if(j!=numDeltas){
                               j++;
                               setMarker();
                             }
                           }


                         }

                       }else{
                        if(distance>distanceInterval){
                          //alert(distance);
                        //alert(distanceInterval);
                        distance=0;
                        // $scope.markerStart     = new L.marker(startFlagVal,{icon: arrowIcon,rotationAngle: deg-45});
                        arrowMarker[i]=$scope.markerStart;
                        if($scope.hideArrows.value1=='YES'){
                          $scope.map_osm.addLayer($scope.markerStart);
                        }
                      }
                    }

                  }
                }


              });
    //end show arroe based on map zoom
      //added for arrow anable or disable 




    //end arrow enable disable

           // console.log(data);

           if($scope.hisloc.vehicleLocations!=null){     

            var vehType=$scope.hisloc.vehicleLocations[0].vehicleType;
            vehicsIcon = vehiclesChange(vehType);

            $scope.osmPath=[];

            for(var i=0;i<$scope.hisloc.vehicleLocations.length;i++){

            //  console.log(data.vehicleLocations[i].latitude+" "+data.vehicleLocations[i].longitude);
            $scope.osmPath.push(new L.LatLng($scope.hisloc.vehicleLocations[i].latitude, $scope.hisloc.vehicleLocations[i].longitude));

            if($scope.hisloc.vehicleLocations[i].isOverSpeed == 'Y'){
              var pscolorvals = '#ff0000';
            } else {
              var pscolorvals = '#6dd538';
            }

            $scope.polylineOsmColor.push(pscolorvals);
              //added for arrow

              if(i!=0){
                var arrowIcon = L.icon({
                  iconUrl: 'assets/imgs/arrow.png',
                  iconSize: [13,13], 
                  iconAnchor:[5,7],
                  popupAnchor: [0, -65],
                  className: 'u-turn-icon'
                                    /*rotateWithView: true,
                                    rotation: 180*/

                                  });
                                 //alert(calcCrow(59.3293371,13.4877472,59.3225525,13.4619422).toFixed(1));
                                 var dx = $scope.hisloc.vehicleLocations[i].latitude - $scope.hisloc.vehicleLocations[i-1].latitude;
                                 var dy = $scope.hisloc.vehicleLocations[i].longitude - $scope.hisloc.vehicleLocations[i-1].longitude;
                                 deg = Math.atan2(dy, dx)* 180 / Math.PI;
                                 var latlnd=middlePoint($scope.hisloc.vehicleLocations[i-1].latitude,$scope.hisloc.vehicleLocations[i-1].longitude,$scope.hisloc.vehicleLocations[i].latitude,$scope.hisloc.vehicleLocations[i].longitude);
                                //var nextMarkerVal = new L.LatLng($scope.hisloc.vehicleLocations[i-1].latitude,$scope.hisloc.vehicleLocations[i-1].longitude);
                                var nextMarkerVal = new L.LatLng(latlnd[0],latlnd[1]);
                                $scope.markerStart     = new L.marker(nextMarkerVal,{icon: arrowIcon,rotationAngle: deg-45});
                                latLngdiff=calcCrow($scope.hisloc.vehicleLocations[i-1].latitude,$scope.hisloc.vehicleLocations[i-1].longitude,$scope.hisloc.vehicleLocations[i].latitude,$scope.hisloc.vehicleLocations[i].longitude);
                                distance=parseFloat(latLngdiff)+parseFloat(distance);
                                //alert(distance.toFixed(2));
                                if(distance>distanceInterval){
                                  //alert(distance);
                                //alert(distanceInterval);
                                distance=0;
                                //$scope.markerStart     = new L.marker(startFlagVal,{icon: myIconStart,rotationAngle: deg-45});
                                arrowMarker[i]=$scope.markerStart;
                                //$scope.markerStart.addTo($scope.map_osm);
                                if($scope.hideArrows.value1=='YES'){
                                  $scope.map_osm.addLayer($scope.markerStart);
                                }
                              }
                                //$scope.map_osm.removeLayer($scope.markerStart);

                              }
                          //end arrow
                        } 

           // console.log($scope.osmPath.length);
           // console.log($scope.osmPath);
           // console.log(vamoservice.iconURL($scope.hisloc.vehicleLocations[0]));
           $scope.markerss     = new L.marker();
           $scope.infowindowss = new L.popup({maxWidth:400,maxHeight: 170});

             // $scope.startFlagval = new L.LatLng($scope.hisloc.vehicleLocations[0].latitude, $scope.hisloc.vehicleLocations[0].longitude);
             
             var latLength   = $scope.hisloc.vehicleLocations.length-1;
             $scope.addMarkerstart_osm($scope.hisloc.vehicleLocations[0]);
             $scope.endMarkerstart_osm($scope.hisloc.vehicleLocations[latLength]);
             $scope.polylines_osm = L.polyline($scope.osmPath, {stroke:true, color:'#2b2b2b', weight:4, fillColor:'#2b2b2b'}).addTo($scope.map_osm);

             // $scope.hisloc2=data;

             if( $scope.lineCount_osm != 0){
              $scope.lineCount_osm = 0;
            }
            osmSite = 1;
            if(userName=="LANDT" || userName=="CMTL" || userName=="RPTL"){
              landtDraw();
            } 
            stopLoading();

            $scope.osmInterVal  = setInterval(function(){ osmPolyLine() },600);


          }else{

            stopLoading();
          }


        //}
      }


      function osmPolyLine(){

    //console.log('poly liness....');

    /*  $scope.polyline1[lineCount] = new google.maps.Polyline({
            map: $scope.map,
            path: [$scope.path[lineCount], $scope.path[lineCount+1]],
            strokeColor: $scope.polylinearr[lineCount],
            strokeOpacity: 0,
            //strokeOpacity: 1,
            strokeWeight: 3,
            icons: [{
                    icon: doubleLine,
                    offset: '50%',
                    repeat: '15px'
                }],
            clickable: true
          }); */

       // console.log(lineCount_osm);
       // console.log($scope.osmPath.length);
       // console.log($scope.hisloc2.vehicleLocations.length);

          //start fuel and speed values
          $('#graphsId #speed').text($scope.hisloc.vehicleLocations[$scope.lineCount_osm].speed);
          total     =  parseInt($scope.hisloc.vehicleLocations[$scope.lineCount_osm].speed);
          if($scope.noOfTank==1){
           // $('#graphsId #fuel').text($scope.hisloc.vehicleLocations[$scope.lineCount_osm].tankSize);
           fuelLtr   =  parseInt($scope.hisloc.vehicleLocations[$scope.lineCount_osm].fuelLitre);
         }else{
          var mulFuelLitres = $scope.hisloc.vehicleLocations[$scope.lineCount_osm].fuelLitres;
          if(mulFuelLitres!="0"){
            $scope.fuelLitreArr = mulFuelLitres.split(":");
          }else{
            $scope.fuelLitreArr = "0:0:0";
          } 
        }
        setFuelAndSpeed();
          //end fuel and speed values

          if($scope.osmPath.length == $scope.lineCount_osm+1) {
         // console.log('Interval.......');

         clearInterval($scope.osmInterVal);

         $('#playButton').prop('disabled',  true);
         $('#replayButton').prop('disabled',false);
         $('#stopButton').prop('disabled', true);
         $('#pauseButton').prop('disabled', true);
               //$("#graphsId").hide();
               $scope.speedVal=1; 

          // $scope.endMarkerstart_osm($scope.endFlagval);
        }
        
        if($scope.osmPath.length != $scope.lineCount_osm+1) {

          /*  $scope.markerhead.setIcon({
                path:vehicIcon[0],
                scale:vehicIcon[1],
                strokeWeight: 1,
                fillColor: $scope.polylinearr[lineCount],
                fillOpacity: 1,
                anchor:vehicIcon[2],
                rotation: rotationd,
              });

              // var myIcon = L.icon({iconUrl: vamoservice.iconURL(pos.data)}); */
           // var myIcons = L.icon({iconUrl: vamoservice.iconURL($scope.hisloc.vehicleLocations[0])});
           // $scope.markerss  = new L.marker({icon:myIcons});

         //  if(lineCount_osm==0){   

         //   $scope.markerss.addTo($scope.map_osm);
        //  }

             // $scope.markerss.setLatLng($scope.osmPath[lineCount_osm]);
             if($scope.vehiclFuel==true){
              var contenttsString = '<div style="padding:2px; padding-top:3px; width:175px;">'
              +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate('LocTime')+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+dateFormat($scope.hisloc.vehicleLocations[$scope.lineCount_osm].date)+'</span> </div>'
              +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate('Speed')+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+ $scope.hisloc.vehicleLocations[$scope.lineCount_osm].speed+'</span><span style="font-size:10px;padding-left:10px;">'+translate("kmph")+'</span></div>'
              +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate('DistCov')+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+$scope.hisloc.vehicleLocations[$scope.lineCount_osm].distanceCovered+'</span><span style="font-size:10px;padding-left:10px;">'+translate("kms")+'</span></div>'
              +'<div  style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate('Fuel(Ltrs)')+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+ (($scope.noOfTank >1)? $scope.splitfuel($scope.hisloc.vehicleLocations[$scope.lineCount_osm].fuelLitres) : $scope.hisloc.vehicleLocations[$scope.lineCount_osm].fuelLitre)+'</span><span style="font-size:10px;padding-left:10px;">'+translate("ltrs")+'</span></div>'
              +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate('odokms')+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+$scope.hisloc.vehicleLocations[$scope.lineCount_osm].odoDistance+'</span><span style="font-size:10px;padding-left:10px;">'+translate("kms")+'</span></div>'
              +'<div style="text-align:center;padding-top:3px;"><span style="font-size:10px;width:100px;">'+$scope.trimComma($scope.hisloc.vehicleLocations[$scope.lineCount_osm].address)+'</span></div>'
   // +'<div style="overflow-wrap: break-word; border-top: 1px solid #eee">'+data.address+'</div>'
   +'</div>';}
   if($scope.vehiclFuel==false){
    var contenttsString = '<div style="padding:2px; padding-top:3px; width:175px;">'
    +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate('LocTime')+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+dateFormat($scope.hisloc.vehicleLocations[$scope.lineCount_osm].date)+'</span> </div>'
    +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate('Speed')+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+ $scope.hisloc.vehicleLocations[$scope.lineCount_osm].speed+'</span><span style="font-size:10px;padding-left:10px;">'+translate("kmph")+'</span></div>'
    +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate('DistCov')+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+$scope.hisloc.vehicleLocations[$scope.lineCount_osm].distanceCovered+'</span><span style="font-size:10px;padding-left:10px;">'+translate("kms")+'</span></div>'
    +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate('odokms')+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+$scope.hisloc.vehicleLocations[$scope.lineCount_osm].odoDistance+'</span><span style="font-size:10px;padding-left:10px;">'+translate("kms")+'</span></div>'
    +'<div style="text-align:center;padding-top:3px;"><span style="font-size:10px;width:100px;">'+$scope.trimComma($scope.hisloc.vehicleLocations[$scope.lineCount_osm].address)+'</span></div>'
   // +'<div style="overflow-wrap: break-word; border-top: 1px solid #eee">'+data.address+'</div>'
   +'</div>';}

   if($scope.vehiLabel == "Asset"){

    myIcons = L.icon({
      iconUrl: "assets/imgs/asMarker.png",
      iconSize: [30,30], 
      iconAnchor: [15,30],
      popupAnchor: [-1, -32],
    }); 

  } else {

    if( $scope.trvShow == 'true' ){

     myIcons = L.icon({
      iconUrl: vamoservice.trvIcon($scope.hisloc.vehicleLocations[$scope.lineCount_osm]),
      iconSize: [40,40], 
      iconAnchor: [20,40],
      popupAnchor: [-1, -40],
    });

   } else {

     myIcons = L.icon({
      iconUrl: vamoservice.iconURL($scope.hisloc.vehicleLocations[$scope.lineCount_osm]),
      iconSize: [40,40], 
      iconAnchor: [20,40],
      popupAnchor: [-1, -40],
    });

   }


 }
 var position;
 $scope.infowindowss.setContent(contenttsString);
 $scope.markerss.bindPopup($scope.infowindowss);
 $scope.markerss.setIcon(myIcons);
 if($scope.timeDelay==180||$scope.timeDelay==150||($scope.map_osm.getZoom()>18)){
  $scope.markerss.setLatLng($scope.osmPath[$scope.lineCount_osm]);
}

        // setting Fuel values
          //console.log($scope.hisloc.vehicleLocations[$scope.lineCount_osm]);


     var delay = 3; //milliseconds
     // if($scope.timeDelay==180){ delay=1; numDeltas = 30; }
     // if($scope.timeDelay==150){delay=1; numDeltas = 10; }
     var i = 0;
     var deltaLat;
     var deltaLng;
     if($scope.lineCount_osm==0){
       position = $scope.osmPath[$scope.lineCount_osm];
       $scope.map_osm.panTo($scope.osmPath[$scope.lineCount_osm]);
     }else if($scope.lineCount_osm>0){
       position = $scope.osmPath[$scope.lineCount_osm-1];
     }
     if($scope.timeDelay!=180&&$scope.timeDelay!=150&&($scope.map_osm.getZoom()<=18)){
       transition($scope.osmPath[$scope.lineCount_osm]);
     }

     function transition(result){
       i = 1;
       deltaLat = (result.lat - position.lat)/osmnumDeltas;
       deltaLng = (result.lng - position.lng)/osmnumDeltas;
       moveMarker();
     }

     function moveMarker(){

       position.lat += deltaLat;
       position.lng += deltaLng;
                   //var latlng = new google.maps.LatLng(position[0], position[1]);
                   //marker.setPosition(latlng);
                   $scope.markerss.setLatLng([position.lat, position.lng]);
                   // $scope.map_osm.panTo([position.lat, position.lng]);
                   if(i!=osmnumDeltas){
                     i++;
                     if(!$scope.movemarker){
                      setTimeout(moveMarker, delay);
                    }
                  }

                }
                if($scope.lineCount_osm==0){   
                  $scope.markerss.addTo($scope.map_osm);
                  $scope.markerss.openPopup();

                  $scope.addTinyMarker_osm($scope.hisloc);
                  $scope.tinyMarkers.addEventListener('click', function(e) {
                    $scope.tinyMarkers.openPopup();             
                  });

                }

           // var nVar      = [$scope.osmPath[lineCount_osm],$scope.osmPath[lineCount_osm+1]];
           // var polylines = L.polyline(nVar, {color: $scope.polylineOsmColor[lineCount_osm]}).addTo($scope.map_osm);
           // $scope.addTinyMarker_osm($scope.osmPath[lineCount_osm],$scope.hisloc.vehicleLocations[lineCount_osm]);

               // for OSM map center changes
              //$scope.map_osm.panTo($scope.osmPath[$scope.lineCount_osm]);

              var currZoom = $scope.map_osm.getZoom();
              var interval=25-currZoom;
              //alert(interval);
              if($scope.lineCount_osm%interval==0){
                $scope.map_osm.panTo($scope.osmPath[$scope.lineCount_osm]);
              }
           // console.log($scope.osmPath[lineCount_osm]);

           $scope.lineCount_osm++;
         }

       }


       function drawPolygon(){

         if(polygonval==0){

          if(polygonInitGoog==0){

            polygenList =[];
            var latLanlist, seclat, seclan, sp; 

            function centerMarker(listMarker){
              var bounds = new google.maps.LatLngBounds();
              for (i = 0; i < listMarker.length; i++) {
                bounds.extend(listMarker[i]);
              }
              return bounds.getCenter()
            }

            if($scope.siteData.siteParent!=null){
              siteLength = $scope.siteData.siteParent.length;
              polygenList.push(new google.maps.LatLng(11, 11));
            }

            for(var listSite = 0; listSite < siteLength; listSite++) {

              var len = $scope.siteData.siteParent[listSite].site.length;
              for (var k = 0; k < len; k++) {
                    // if(response.siteParent[i].site.length)
                    // {
                      var orgName = $scope.siteData.siteParent[listSite].site[k].siteName;
                      var splitLatLan = $scope.siteData.siteParent[listSite].site[k].latLng.split(",");
                      
                      polygenList = [];
                      for(var j = 0; splitLatLan.length>j; j++){
                        sp  = splitLatLan[j].split(":");
                        polygenList.push(new google.maps.LatLng(sp[0], sp[1]));
                            // console.log(sp[0]+' ---- '+ sp[1])
                            // latlanList.push(sp[0]+":"+sp[1]);
                            // seclat        = sp[0];
                            // seclan        = sp[1];
                          }

                          polygenColors = new google.maps.Polygon({
                            path: polygenList,
                            strokeColor: "#282828",
                            strokeWeight: 1,
                            fillColor: '#808080',
                            fillOpacity: 0.50,
                            //map: scope.map
                          });
                          polygenColorsArr[orgName] = polygenColors;
                          polygenColors.setMap($scope.map);
                          labelAnchorpos = new google.maps.Point(19, 0);  //12, 37

                          markers = new MarkerWithLabel({

                            position: centerMarker(polygenList), 
                         // map: scope.map,
                         icon: 'assets/imgs/area_img.png',
                         labelContent: orgName,
                         labelAnchor: labelAnchorpos,
                         labelClass: "labels", 
                         labelInBackground: false
                       });
                          
                          markers.setMap($scope.map);
                          polygonInitGoog=1;
                        // scope.map.setCenter(centerMarker(polygenList)); 
                        // scope.map.setZoom(14); 
                        // }
                      }
                    }
                  }

                } else if(polygonval==1) {

                  console.log('osm polygon');

                  if(polygonInitOsm==0){

                    var latLngPoly, sp_osm;

                    var iconPoly = L.icon({
                      iconUrl: 'assets/imgs/trans.png',
                      iconAnchor:[0,0],
                      labelAnchor: [-15,10],
                    }); 

                    if($scope.siteData.siteParent!=null) {
                      siteLength = $scope.siteData.siteParent.length;
                    }

                    for(var l=0;l<siteLength;l++){

                      var lengths = $scope.siteData.siteParent[l].site.length;

                      for(var k=0;k<lengths;k++){

                    //if(response.siteParent[i].site.length){

                      var orgNam     = $scope.siteData.siteParent[l].site[k].siteName;
                      var spLatLangs = $scope.siteData.siteParent[l].site[k].latLng.split(",");
                      var polygenList_Osm =[];

                      for(var j=0;spLatLangs.length>j;j++) {
                        sp_osm = spLatLangs[j].split(":");
                        latLngPoly = new L.LatLng(sp_osm[0],sp_osm[1]);
                        polygenList_Osm.push(latLngPoly);
                      }

                      $scope.polygons = L.polygon(polygenList_Osm,{ className: 'polygonOsm' }).addTo($scope.map_osm);
                      $scope.markerPoly = new L.marker(latLngPoly);
                      $scope.markerPoly.setIcon(iconPoly).bindLabel(orgNam, { noHide: true, className: 'polygonLabel', clickable: true, direction:'auto' });
                      $scope.markerPoly.addTo($scope.map_osm);
                      polygonInitOsm=1;
                    }
                  }

                // $scope.map_osm.fitBounds($scope.polygons.getBounds());
              }
            }

          }


          $scope.getValueCheck = function(getStatus){

            if($scope.intervalValue){
             clearInterval($scope.intervalValue);
           }

           $scope.getValue = getStatus;

           if($scope.getValue == 'YES') {
            $scope.hideMe = false;
          //$scope.hideMarker= false;
          for(key in polygenColorsArr) {
           polygenColorsArr[key].setMap($scope.map);
         }
         (function(){

          if(siteDataCall==0){
            $http.get($scope.url_site).success(function(response){
              $scope.siteData=response;
              siteDataCall=1;
              drawPolygon();
            });
            
          } else if(siteDataCall==1){
            if( $scope.siteData != undefined && $scope.siteData!=null ){
             drawPolygon();
           }
         }

       }());

       } else if ($scope.getValue == 'NO') {

        $scope.hideMe = true;
        if(polygenColors){
          for(key in polygenColorsArr) {
           polygenColorsArr[key].setMap(null);
         }
         polygenColors.setMap(null);
       }
      // if(polygon1){
      //   polygenColors.setMap(null);
      // }
      // if(polygon2){
      //   polygenColors.setMap(null);
      // }
      
      
    }
  }

  var osmInitsVal=0;
// $scope.$watch("hisurl", function (val) {

  $scope.initMap  = function(vals,initVal){

  //console.log('map init...');
  //console.log(initVal);
  startLoading();

  if(vals == 0){
    polygonval = 0;
  } else if(vals == 1){
    polygonval = 1;
  } 

    // alert(initVal);
    // alert($scope.SiteCheckbox.value1);
    // alert($scope.hideMarker.value1);
    //if($scope.SiteCheckbox.value1=='YES')
      //$scope.SiteCheckbox.value1 = 'YES';

      if(initVal==true) {

        $scope.path   = [];
        $scope.hisloc = [];

        var locs;
        var geoUrl  = GLOBAL.DOMAIN_NAME+'/viewSite';
        var labelAnchorpos,markers;

        var todayTime       =  new Date();
        todayTime.setHours(todayTime.getHours()-1);
        todayTime.setMinutes(todayTime.getMinutes()-59);
        var curtimeShow  =  $filter('date')(todayTime, 'hh:mm a');
        var fDate  =  document.getElementById("dateFrom").value;
        var fTime  =  document.getElementById("timeFrom").value;
        var tDate =  document.getElementById("dateTo").value;
        var tTime  =  document.getElementById("timeTo").value;

        if(fDate==''||fTime==''||tDate==''||tTime=='') {
          $scope.fromdate   =  getTodayDatesss();
          $scope.todate     =  $scope.fromdate;
          $scope.fromtime   =  curtimeShow;
          $scope.totime     =  currentTimes();

          var d = new Date();
          var tUtc = d.getTime();
              //d.setHours(0,0,0,0);
              var fUtc = d.getTime()-7140000;

              $scope.hisurl = GLOBAL.DOMAIN_NAME+'/getVehicleHistory?vehicleId='+$scope.selVehiId+'&fromDateUTC='+fUtc+'&toDateUTC='+tUtc;
              fuelMachineUrl  =  GLOBAL.DOMAIN_NAME+'/getFuelDetailForMachinery?vehicleId='+$scope.selVehiId+'&fromDateTime='+fUtc+'&toDateTime='+tUtc;
            }else{
             fuelMachineUrl  =  GLOBAL.DOMAIN_NAME+'/getFuelDetailForMachinery?vehicleId='+$scope.selVehiId+'&fromDateTime='+utcFormat($scope.fromdate,$scope.fromtime)+'&toDateTime='+utcFormat($scope.todate,$scope.totime);
           }


           $http.get($scope.hisurl).success(function(data){
        //added for speed and fuel Tank widget
        
        $scope.hideButton = false;
        $scope.noOfTank = data.noOfTank;
        tankSize  =  parseInt(data.tankSize);
        $scope.vehiclFuel=graphChange(data.fuel); 
        
          //
          locs           = data;
          $scope.hisloc  = data;
        //console.log($scope.hisloc );
        if($scope.hisloc.vehicleLocations==null ){
          stopLoading();
        }else{
            //alert('getFuelDetail');
            getFuelDetail(vals);
          }
          $scope._tableValue(locs);

          


          // var fTime  =  getParameterByName('fromTime');
          // var tTime  =  getParameterByName('toTime');

          console.log(fTime);
          console.log(tTime);

          
          if(data.fromDateTime=='' || data.fromDateTime==undefined || data.fromDateTime=='NaN-aN-aN'){ 
            if(data.error==null){}else{
              $('.alert-danger').show();
              // $('#myModal').modal();
            }
            $('#lastseen').html('<strong>'+translate('From Date')+' & '+translate('time')+ ':'+'</strong> -');
            $('#lstseendate').html('<strong>'+translate('To Date')+' & '+ translate('time')+' : '+'</strong> -');

            // if(fTime!=''&&tTime!='') {

            //   $scope.fromdate   =  getDateVal(fTime);
            //   $scope.todate     =  getDateVal(tTime);
            //   $scope.fromtime   =  getTimeVal(tTime);
            //   $scope.totime     =  getTimeVal(tTime);

            // } else {

            //   $scope.fromdate   =  getTodayDatesss();
            //   $scope.todate     =  $scope.fromdate;
            //   $scope.fromtime   =  '12:00 AM';
            //   $scope.totime     =  currentTimes();

            // }

          } else {

            $('.alert-danger').hide();

            if(data.error==null){

              $scope.fromNowTS = data.fromDateTimeUTC;
              $scope.toNowTS   = data.toDateTimeUTC; 

              $scope.fromtime  = formatAMPM($scope.fromNowTS);
              $scope.totime    = formatAMPM($scope.toNowTS);
              $scope.fromdate  = $scope.getTodayDate($scope.fromNowTS);
              $scope.todate    = $scope.getTodayDate($scope.toNowTS);
              
              $('#vehiid h3').text(locs.shortName);
              $('#lastseen').html('<strong>'+translate('From Date')+' & '+translate('time')+' :</strong> '+ new Date(data.fromDateTimeUTC).toString().split('GMT')[0]);
              $('#lstseendate').html('<strong>'+translate('To Date')+ ' & '+ translate('time')+' : '+'</strong> '+ new Date(data.toDateTimeUTC).toString().split('GMT')[0]);

            }
          } 

          if(vals==0){


            $('#playButton').prop('disabled', true);
            $('#replayButton').prop('disabled', false);
            $('#stopButton').prop('disabled', false);
            $('#pauseButton').prop('disabled', false);

       // console.log("gooooogle...........");

       clearInterval($scope.osmInterVal);

       document.getElementById("map_canvas").style.display="block"; 
       document.getElementById("map_canvas2").style.display="none"; 

       if($scope.initGoogVal==0){
         $scope.initGoogle_Map($scope.hisloc);
       }else if($scope.initGoogVal==1){
        if(data.vehicleLocations==null){
         $("#graphsId").hide(); 
       }else{
        $("#graphsId").show();
      }
      $scope.polylineCtrl();
    }

    $scope.googleMap=true;
    $scope.osmMap=false;

  } else if(vals==1){

    if(data.vehicleLocations==null){
     $("#graphsId").hide(); 
   }else{
    $("#graphsId").show();
  }
  $('#playButton').prop('disabled', true);
  $('#replayButton').prop('disabled', false);
  $('#stopButton').prop('disabled', false);
  $('#pauseButton').prop('disabled', false);

   /*  $('#pauseButton').show();
       $('#playButton').hide();      
    // $('#stopButton').show();   
    $('#replayButton').show();*/

     // console.log('osm mapsssssss...');

     clearInterval($scope.osmInterVal);

     document.getElementById("map_canvas").style.display="none"; 
     document.getElementById("map_canvas2").style.display="block"; 

        //  if(osmInitsVal==0){
          $scope.initOsm_Map();
          if(vals == 1&&$scope.SiteCheckbox.value1=='YES'){
              //alert('hello');
              $scope.getValueCheck($scope.getValue);
            }
          //}

          $scope.googleMap=false;
          $scope.osmMap=true;
         // osmInitsVal++;
       }

     }).error(function(){ 
      stopLoading();
    }); 

   } else {



    if(vals==0){
        //alert('google');
       // console.log('google...');

       $('#playButton').prop('disabled', true);
       $('#replayButton').prop('disabled', false);
       $('#stopButton').prop('disabled', false);
       $('#pauseButton').prop('disabled', false);

        //console.log("gooooogle elseee...........");

        clearInterval($scope.osmInterVal); 

        document.getElementById("map_canvas").style.display="block"; 
        document.getElementById("map_canvas2").style.display="none"; 

         // console.log($scope.hisloc);

        // $scope.initGoogVal=1; 
       //  $scope.plotVal=0;

       if( $scope.hisloc.vehicleLocations !=null){

         pcount = 0; 
         //$scope.initGoogle_Map($scope.hisloc);

      // $scope.initGoogle_Map($scope.hisloc);
      $scope.polylineCtrl();

    }else{
      stopLoading();
    }

    $scope.googleMap=true;
    $scope.osmMap=false;

  } else if(vals==1){
        //alert('osm');

        $('#playButton').prop('disabled', true);
        $('#replayButton').prop('disabled', false);
        $('#stopButton').prop('disabled', false);
        $('#pauseButton').prop('disabled', false);

        // $scope.initGoogVal=1; 
        // $scope.plotVal=0;
        // console.log('osm mapsssssss elseeee...');

        clearInterval($scope.osmInterVal);

        document.getElementById("map_canvas").style.display="none"; 
        document.getElementById("map_canvas2").style.display="block"; 

        $scope.initOsm_Map();
        if(vals == 1&&$scope.SiteCheckbox.value1=='YES'){
          //alert('hello');
          $scope.getValueCheck($scope.getValue);
        }
        $scope.googleMap = false;
        $scope.osmMap    = true;

      }
      //added for fuel marker
      fuelFillTheft(vals);
        //end

      }



    }

// });


  /* $scope.changeMap = function(val) {

    if(val==0){

      $scope.googleMap=false;
        $scope.osmMap=true;

      document.getElementById("map_canvas").style.display="block"; 
        document.getElementById("map_canvas2").style.display="none"; 

      $scope.initGoogle_Map($scope.hisloc);

    } else if(val==1){

       $scope.googleMap=false;
         $scope.osmMap=true;

        document.getElementById("map_canvas").style.display="none"; 
          document.getElementById("map_canvas2").style.display="block"; 

        $scope.initOsm_Map();
    }
  } */

  $scope.getTodayDate  =  function(date) {
    var date = new Date(date);
    return ("0" + (date.getDate())).slice(-2)+'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+date.getFullYear();
  }

  $scope.parseInts=function(data){
   return parseInt(data);
 }

 $scope.alladdress    = [];
 $scope.moveaddress   = [];
//$scope.overaddress   = [];
$scope.parkaddress   = [];
$scope.idleaddress   = [];
//$scope.fueladdress   = [];
$scope.igniaddress   = [];
//$scope.acc_address   = [];
$scope.stop_address  = [];

  // loading start function
  // $scope.startLoading    = function () {
  //  $('#status').show(); 
  //  $('#preloader').show();
  // };

  // //loading stop function
  // $scope.stopLoading   = function () {
  //  $('#status').fadeOut(); 
  //  $('#preloader').delay(350).fadeOut('slow');
  //  $('body').delay(350).css({'overflow':'visible'});
  // };

  function sessionValue(vid, gname){
    localStorage.setItem('user', JSON.stringify(vid+','+gname));
    $("#testLoad").load("../public/menu");
  }

  $scope.loading  = true;
  
  function _pairFilter(_data, _yes, _no, _status)
  {
    var _checkStatus =_no ,_pairList  = [];
    angular.forEach(_data, function(value, key){

      if(_pairList.length <= 0){
        if(value[_status] == _yes)
          _pairList.push(value)
      } else if(_pairList.length >0 )
      {
        if(value[_status] == _checkStatus){
          _pairList.push(value)
          if(_pairList[_pairList.length-1][_status] == _yes)
            _checkStatus = _no;
          else
            _checkStatus = _yes
        }
      }

    });

    if(_pairList.length>1)
      if(_pairList.length%2==0)
        return _pairList;
      else{
       _pairList.pop();
       return _pairList;
     }

   }

/*  function filter(obj){
      var _returnObj = [];
      if(obj)
        angular.forEach(obj,function(val, key){
          if(val.fuelLitre >0)
            _returnObj.push(val)
        })
      return _returnObj;
    } */

    function getFuelDetail(vals){
      //added for getting Fuel fill and fuel theft
      try{

        startLoading();
        console.log( fuelMachineUrl );
        
        $http.get( fuelMachineUrl ).success(function(data) { 

          console.log( data );    

          $scope.fuelMachineData = data;

          console.log( $scope.fuelMachineData );
          fuelFillTheft(vals);

          stopLoading();  

        });
      }catch ( e ) {
        stopLoading();
        console.log('Error on getFuelDetailForMachinery '+e);
      }

          //end fuel fill and theft
        }

        function fuelFillTheft(vals){
    //added for fuel marker
    angular.forEach($scope.fuelMachineData.fuelFills, function(fillvalue, key){
                    //alert(fillvalue.lat);
                    if(vals==0){
                      $scope.fuelFillMarker(fillvalue);
                    } else if(vals==1){
                      $scope.fuelFillMarker_osm(fillvalue);
                    } 
                  });
    angular.forEach($scope.fuelMachineData.fuelDrops, function(theftvalue, key){

      if(vals==0){
        $scope.fuelTheftMarker(theftvalue,$scope.fuelMachineData.sensorMode);
      } else if($scope.osmMap==true && $scope.googleMap==false){
        $scope.fuelTheftMarker_osm(theftvalue,$scope.fuelMachineData.sensorMode);
      } 
    });
        //end
      }

      $scope.fuelFillMarker_osm = function(data) {

        var startFlagVal = new L.LatLng(data.lat, data.lng);
        var myIconStart = L.icon({
          iconUrl: 'assets/imgs/fuelfill.png',
          iconSize: [20,25], 
          iconAnchor:[10,15],
          popupAnchor: [0, -10],
        });

        var contentString_osm = '<h3 class="infoh3">'+translate("fuel_fill")+' Details ('+$scope.shortVehiId+') </h3><div class="nearbyTable02"><div>'
        +'<table cellpadding="0" cellspacing="0">'
        +'<tbody>'
        +'<tr><td>'+translate("start_time")+'</td><td>'+dateFormat(data.startTime)+'</td></tr>'
        +'<tr><td>'+translate("end_time")+'</td><td>'+dateFormat(data.time)+'</td></tr>'
        +'<tr><td>'+translate("previous_fuel")+'</td><td>'+data.pFuel+' '+translate("ltrs")+'</td></tr>'
        +'<tr><td>'+translate("current_fuel")+'</td><td>'+data.cFuel+' '+translate("ltrs")+'</td></tr>'
        +'<tr><td>'+translate("fuel_fill")+'</td><td>'+ parseFloat(data.lt).toFixed( 2 )+' '+translate("ltrs")+'</td></tr>'
        +'<tr><td>'+translate("Odo")+'</td><td>'+data.odo+'</td></tr>'
        +'<tr><td>'+translate("Location")+'</td><td><a href="https://www.google.com/maps?q=loc:'+data.lat+','+data.lng+'" target="_blank">'+data.address+'</a></td></tr>'
        +'</table></div>';

        $scope.markerStart     = new L.marker();
        $scope.infowindowStart = new L.popup(/*{maxWidth:400,maxHeight: 170}*/);

        $scope.infowindowStart.setContent(contentString_osm);
        $scope.markerStart.bindPopup($scope.infowindowStart);
        $scope.markerStart.setIcon(myIconStart);
        $scope.markerStart.setLatLng(startFlagVal).addTo($scope.map_osm);


      } 
      $scope.fuelTheftMarker_osm = function(data,vehiclemode) {

        if(vehiclemode!='Dispenser'){
          var startFlagVal = new L.LatLng(data.lat, data.lng);
          var myIconStart = L.icon({
            iconUrl: 'assets/imgs/fueltheft.png',
            iconSize: [20,20], 
            iconAnchor:[14,16],
            popupAnchor: [0, -20],
          });

          var contentString_osm = '<h3 class="infoh3">'+translate("fuel_theft")+' Details ('+$scope.shortVehiId+') </h3><div class="nearbyTable02"><div>'
          +'<table cellpadding="0" cellspacing="0">'
          +'<tbody>'
          +'<tr><td>'+translate("start_time")+'</td><td>'+dateFormat(data.startTime)+'</td></tr>'
          +'<tr><td>'+translate("end_time")+'</td><td>'+dateFormat(data.time)+'</td></tr>'
          +'<tr><td>'+translate("previous_fuel")+'</td><td>'+data.pFuel+' '+translate("ltrs")+'</td></tr>'
          +'<tr><td>'+translate("current_fuel")+'</td><td>'+data.cFuel+' '+translate("ltrs")+'</td></tr>'
          +'<tr><td>'+translate("fuel_theft")+'</td><td>'+ parseFloat(data.lt).toFixed( 2 )+' '+translate("ltrs")+'</td></tr>'
          +'<tr><td>'+translate("Odo")+'</td><td>'+data.odo+'</td></tr>'
          +'<tr><td>'+translate("Location")+'</td><td><a href="https://www.google.com/maps?q=loc:'+data.lat+','+data.lng+'" target="_blank">'+data.address+'</a></td></tr>'
          +'</table></div>';

          $scope.markerStart     = new L.marker();
          $scope.infowindowStart = new L.popup(/*{maxWidth:400,maxHeight: 170}*/);

          $scope.infowindowStart.setContent(contentString_osm);
          $scope.markerStart.bindPopup($scope.infowindowStart);
          $scope.markerStart.setIcon(myIconStart);
          $scope.markerStart.setLatLng(startFlagVal).addTo($scope.map_osm);
        }

      }  


      $scope.fuelFillMarker= function(data){
   // alert(data.lat);
   if(data.error!='Got zero records. Please change the dates and try.'){
     var myLatlng = new google.maps.LatLng(data.lat, data.lng);
     pinImage = 'assets/imgs/fuelfill.png';

     $scope.markerFill = new MarkerWithLabel({
       position: myLatlng, 
       map: $scope.map,
       icon:  {url:pinImage, scaledSize: new google.maps.Size(20, 25)}
     });

     var contentString = '<h3 class="infoh3">'+translate("fuel_fill")+' Details ('+$scope.shortVehiId+') </h3><div class="nearbyTable02"><div>'
     +'<table cellpadding="0" cellspacing="0">'
     +'<tbody>'
     +'<tr><td>'+translate("start_time")+'</td><td>'+dateFormat(data.startTime)+'</td></tr>'
     +'<tr><td>'+translate("end_time")+'</td><td>'+dateFormat(data.time)+'</td></tr>'
     +'<tr><td>'+translate("previous_fuel")+'</td><td>'+data.pFuel+' '+translate("ltrs")+'</td></tr>'
     +'<tr><td>'+translate("current_fuel")+'</td><td>'+data.cFuel+' '+translate("ltrs")+'</td></tr>'
     +'<tr><td>'+translate("fuel_fill")+'</td><td>'+ parseFloat(data.lt).toFixed( 2 )+' '+translate("ltrs")+'</td></tr>'
     +'<tr><td>'+translate("Odo")+'</td><td>'+data.odo+'</td></tr>'
     +'<tr><td>'+translate("Location")+'</td><td><a href="https://www.google.com/maps?q=loc:'+data.lat+','+data.lng+'" target="_blank">'+data.address+'</a></td></tr>'
     +'</table></div>';
     var infoWindow = new google.maps.InfoWindow({content: contentString});

     google.maps.event.addListener($scope.markerFill, "click", function(e){
      infoWindow.open($scope.map, $scope.markerFill);  
    });
   }
   else {

   }


 }
 $scope.fuelTheftMarker= function(data,vehiclemode){
    //alert(data.lat);
    if(vehiclemode!='Dispenser' ){
      var myLatlng = new google.maps.LatLng(data.lat, data.lng);
      pinImage = 'assets/imgs/fueltheft.png';

      $scope.markerTheft = new MarkerWithLabel({
       position: myLatlng, 
       map: $scope.map,
       icon:  {url:pinImage, scaledSize: new google.maps.Size(20, 20)}
     });

      var contentString = '<h3 class="infoh3">'+translate("fuel_theft")+' Details ('+$scope.shortVehiId+') </h3><div class="nearbyTable02"><div>'
      +'<table cellpadding="0" cellspacing="0">'
      +'<tbody>'
      +'<tr><td>'+translate("start_time")+'</td><td>'+dateFormat(data.startTime)+'</td></tr>'
      +'<tr><td>'+translate("end_time")+'</td><td>'+dateFormat(data.time)+'</td></tr>'
      +'<tr><td>'+translate("previous_fuel")+'</td><td>'+data.pFuel+' '+translate("ltrs")+'</td></tr>'
      +'<tr><td>'+translate("current_fuel")+'</td><td>'+data.cFuel+' '+translate("ltrs")+'</td></tr>'
      +'<tr><td>'+translate("fuel_theft")+'</td><td>'+ parseFloat(data.lt).toFixed( 2 )+' '+translate("ltrs")+'</td></tr>'
      +'<tr><td>'+translate("Odo")+'</td><td>'+data.odo+'</td></tr>'
      +'<tr><td>'+translate("Location")+'</td><td><a href="https://www.google.com/maps?q=loc:'+data.lat+','+data.lng+'" target="_blank">'+data.address+'</a></td></tr>'
      +'</table></div>';
      var infoWindow = new google.maps.InfoWindow({content: contentString});

      google.maps.event.addListener($scope.markerTheft, "click", function(e){
        infoWindow.open($scope.map, $scope.markerTheft);  
      });
    }
  }


  function filter(obj,name){
    var _returnObj = [];
    /*  if(name=='fuel'){
        angular.forEach(obj,function(val, key){

          if(val.fuelLitre >0)
          {
            _returnObj.push(val)
          }
        })
      } */
      
      if(name=='stoppage'){

        angular.forEach(obj,function(val, key){

          if(val.stoppageTime >0)
          {
            _returnObj.push(val)
          }
        })
      }
    /*  else if(name=='ovrspd'){

        angular.forEach(obj,function(val, key){

          if(val.overSpeedTime >0)
          {
            _returnObj.push(val)
          }
        })
      }*/
      return _returnObj;
    }

/*    $scope.fuelChart  =   function(data){
    var ltrs    = [];
    var fuelDate  = [];
    try{
      if(data.length)
        for (var i = 0; i < data.length; i++) {
          if(data[i].fuelLitre !='0' || data[i].fuelLitre !='0.0')
          {
            ltrs.push(data[i].fuelLitre);
            var dar = $filter('date')(data[i].date, "dd/MM/yyyy HH:mm:ss");
            fuelDate.push(dar)
          }
        };
    }
    catch (err){
      console.log(err.message)
    }
    
  $(function () {
   
        $('#container').highcharts({
            chart: {
                zoomType: 'x'
            },
            title: {
                text: 'Fuel Report'
            },
          
             xAxis: {
            categories: fuelDate
            },
            
            yAxis: {
                title: {
                    text: 'Fuel'
                }
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },

            series: [{
                type: 'area',
                name: 'Fuel Level',
                data: ltrs
            }]
        });

});
  }


  $scope.speedKms   =   function(data){
    var ltrs    = [];
    var fuelDate  = [];
    try{
      if(data.length)
        for (var i = 0; i < data.length; i++) {
          if(data[i].speed !='0')
          {
            ltrs.push(data[i].speed);
            var dar = $filter('date')(data[i].date, "dd/MM/yyyy HH:mm:ss");
            fuelDate.push(dar)
          }
        };
    }
    catch (err){
      console.log(err.message)
    }
    
  $(function () {
   
        $('#speedGraph').highcharts({
            chart: {
                zoomType: 'x'
            },
        title: {
            text: ''
        },
       subtitle: {
            text: 'Speed km/h'
        },
        xAxis: {
            categories: fuelDate
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: {
                            x1: 0,
                            y1: 0,
                            x2: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    },
                    marker: {
                        radius: 2
                    },
                    lineWidth: 1,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },
        // plotOptions: {
        //     line: {
        //         dataLabels: {
        //             enabled: false
        //         },
        //         enableMouseTracking: true
        //     }
        // },
        // legend: {
        //     enabled: false
        // },

        series: [{
          type: 'area',
            name: 'km/h',
            data: ltrs
        }]
        });

  });
}
*/
$scope._tableValue = function(_value){
//if(_value && _value.vehicleLocations != null){
  $scope.moveaddress   = [];
  $scope.alladdress    = [];
  //$scope.overaddress   = [];
  $scope.parkaddress   = [];
  $scope.idleaddress   = [];
  //$scope.fueladdress   = [];
  $scope.igniaddress   = [];
  //$scope.acc_address   = [];
  $scope.stop_address  = [];
  $scope.parkeddata    = [];
  $scope.overspeeddata = [];
  $scope.allData       = [];
  $scope.movementdata  = [];
  $scope.idlereport    = [];
  $scope.ignitValue    = [];
  //$scope.acReport      = [];
  $scope.stopReport    = [];
  //$scope.fuelValue     = [];

  if(_value && _value.vehicleLocations != null) {

    var ignitionValue     = ($filter('filter')(_value.vehicleLocations, {'ignitionStatus': "!undefined"}))
    $scope.parkeddata     = ($filter('filter')(_value.vehicleLocations, {'position':"P"}));
    $scope.overspeeddata  = ($filter('filter')(_value.vehicleLocations, {'isOverSpeed':"Y"}));
  //  $scope.overspeeddata  = filter(_value.vehicleLocations,'ovrspd');
  $scope.allData        = ($filter('filter')(_value.vehicleLocations, {}));
  $scope.movementdata   = ($filter('filter')(_value.vehicleLocations, {'position':"M"}));
  $scope.idlereport     = ($filter('filter')(_value.vehicleLocations, {'position':"S"}));
  $scope.ignitValue     = _pairFilter(ignitionValue, 'ON', 'OFF', 'ignitionStatus');
    //$scope.acReport       = _pairFilter(_value.vehicleLocations, 'yes', 'no', 'vehicleBusy');
    //$scope.fuelValue      =  filter(_value.vehicleLocations);
    //$scope.fuelValue      =  filter(_value.vehicleLocations,'fuel');
    $scope.stopReport     =  filter(_value.vehicleLocations,'stoppage');

      // console.log($scope.ignitValue);
    }
  // $scope.speedKms($scope.movementdata);
  // $scope.fuelChart($scope.fuelValue);
}

function google_api_call(tempurlMo, index1, _stat) {
  // console.log(' temperature ')
  // $scope.av           = "adasdas"
  // $scope.moveaddress  = [];
  // $scope.overaddress  = [];
  // $scope.parkaddress  = [];
  // $scope.idleaddress  = [];
  // $scope.fueladdress  = [];
  // $scope.igniaddress  = [];
  // $scope.acc_address  = [];
  $http.get(tempurlMo).success(function(data){
      //  $.ajax({
           // async: false,
           // method: 'GET', 
           // url: tempurlMo,
           // success: function (data) {
              // console.log(data)
              // aUthName = response;
              // localStorage.setItem('apiKey', JSON.stringify(aUthName[0]));
              // localStorage.setItem('userIdName', JSON.stringify('username'+","+aUthName[1]));
              switch (_stat){
                case 'all':
                console.log(_stat);
                $scope.alladdress[index1] = data.results[0].formatted_address;
                break;
                case 'movement':
                console.log(_stat);
                $scope.moveaddress[index1] = data.results[0].formatted_address;
                break;
                case 'parked':
                console.log(_stat);
                $scope.parkaddress[index1] = data.results[0].formatted_address;
                break;
                case 'idle':
                console.log(_stat);
                $scope.idleaddress[index1] = data.results[0].formatted_address;
                break;
                case 'ignition':
                console.log(_stat);
                $scope.igniaddress[index1] = data.results[0].formatted_address;
                break;
                case 'stoppage':
                console.log(_stat);
                $scope.stop_address[index1] = data.results[0].formatted_address;
                break;
              }
        //     }
        // })

      // $scope.moveaddress[index1] = data.results[0].formatted_address;
      // var t = vamo_sysservice.geocodeToserver(latMo,lonMo,data.results[0].formatted_address);
    })
};

var queue1 = [];
var delaying = (function () {

  function processQueue1() {
    if (queue1.length > 0) {
      setTimeout(function () {
        queue1.shift().cb();
        processQueue1();
      }, queue1[0].delay);
    }
  }

  return function delayed(delay, cb) {
    queue1.push({ delay: delay, cb: cb });

    if (queue1.length === 1) {
      processQueue1();
    }
  };

}());

$scope.recursive   = function(location_over, _stat, _address){
      // console.log(va)
      var indexs = 0;
      angular.forEach(location_over, function(value, primaryKey){
        indexs = primaryKey;
        if(location_over[indexs].address == undefined && _address[indexs] == undefined)
        {
          //console.log(' address over speed'+indexs)
          var latOv    =  location_over[indexs].latitude;
          var lonOv    =  location_over[indexs].longitude;
          var tempurlOv  =  "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latOv+','+lonOv+"&sensor=true";
          //console.log(' in overspeed '+indexs)
          delaying(3000, function (indexs) {
            return function () {
              google_api_call(tempurlOv, indexs, _stat);
            };
          }(indexs));
        }
      })
    }

    $scope.addressResolve   = function(tabVal){
      queue1 = [];
      switch (tabVal){
        case 'all':
        $scope.recursive($scope.allData, tabVal, $scope.alladdress);
        break;

        case 'movement':
        $scope.recursive($scope.movementdata, tabVal, $scope.moveaddress);

        break;

        case 'parked': 
        $scope.recursive($scope.parkeddata, tabVal, $scope.parkaddress);
        break;

        case 'idle':
        $scope.recursive($scope.idlereport, tabVal, $scope.idleaddress);
        break;

        case 'ignition':
        $scope.recursive($scope.ignitValue, tabVal, $scope.igniaddress);
        break;

        case 'stoppage':
        $scope.recursive($scope.stopReport, tabVal, $scope.stop_address);
        break;
      }

    }

  // $http.get($scope.url).success(function(data){

  //  $scope.locations = data;
  //  $scope.groupname = data[0].group;
  //  $scope.vehicleId = data[0].vehicleLocations[0].vehicleId;
  //  sessionValue($scope.vehicleId, $scope.groupname)
  //  if(getParameterByName('vehicleId')=='' && getParameterByName('gid')==''){
  //    $scope.trackVehID =$scope.locations[0].vehicleLocations[3].vehicleId;
  //    $scope.shortVehiId =$scope.locations[0].vehicleLocations[3].shortName;
  //    $scope.selected=0;
  //  }else{
  //    $scope.trackVehID =getParameterByName('vehicleId');
  //    for(var i=0; i<$scope.locations[0].vehicleLocations.length;i++){
  //      if($scope.locations[0].vehicleLocations[i].vehicleId==$scope.trackVehID){
  //        $scope.selected=i;
  //      }
  //    }
  //  }
  // }).error(function(){ /*alert('error'); */});

  $scope.getOrd   = function()
  {
    $scope.error = "";
    $scope.routeName = "";
    $scope.selectedOrdId = "";

    getRouteNames();
    $scope.getMap($scope.routedValue[0]);
  }

  function showRoutes(setRoute){

    if(setRoute != '' && setRoute == 'routes'){
      $("#myModal1").modal();
      $scope.getOrd();
    }
  }

  function addZero(i) {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }

  function timeNow24Hrs(){

   var d = new Date();
   var h = addZero(d.getHours());
   var m = addZero(d.getMinutes());
   var s = addZero(d.getSeconds());

    //return h + ":" + m + ":" + s;
    return h+":00:00";
  }

  function getTodayDatess() {
   var date = new Date();
   return ("0" + (date.getDate())).slice(-2)+'-'+("0" + (date.getMonth() + 1)).slice(-2)+'-'+date.getFullYear();
 };


 function filterGroup(groups){
  var filter = [];
  var splitValue ='';
  angular.forEach(groups, function(value){
    if (value.group)
      splitValue = value.group;
    console.log(splitValue);
    filter.push(splitValue)
      //console.log(splitValue);
    })
  return filter
  console.log(filter);
}

(function init(){
   // startLoading();

   var mapsVal=localStorage.getItem('mapNo');
   if(mapsVal==null)
   {
    mapsVal=1;
  }
    //console.log(mapsVal);

    if(mapsVal==0) {

     $scope.maps_no    =  0;
     $scope.mapsHist   =  0;
     $scope.googleMap  =  true;
     $scope.osmMap     =  false;

     document.getElementById("map_canvas2").style.display="none"; 
     document.getElementById("map_canvas").style.display="block"; 

   } else if(mapsVal==1) {

    $scope.maps_no    =  1;
    $scope.mapsHist   =  1;
    $scope.osmMap     =  true;
    $scope.googleMap  =  false;

    document.getElementById("map_canvas").style.display="none"; 
    document.getElementById("map_canvas2").style.display="block"; 
  }

  console.log('hellloooooooo');

  var url;
  $scope.groupname = getParameterByName('gid');
  console.log($scope.groupname);
  url = (getParameterByName('vehicleId')=='' && getParameterByName('gid')=='')?$scope.url:$scope.url+'?group='+$scope.groupname;

  console.log(url);

  $http.get(url).success(function(response){

    console.log('responsssee.....');

    $scope.locations  = response;

    console.log(response);

    if (getParameterByName('gid') == '' && getParameterByName('vehicleId') == '') {

      $scope.groupname    = response[0].group;
      console.log($scope.groupname);
      $scope.trackVehID   = $scope.locations[0].vehicleLocations[0].vehicleId;
      $scope.shortVehiId  = $scope.locations[0].vehicleLocations[0].shortName;
      $scope.selected     = 0;
      $scope.gIndex = 0;
          //$('#vehiid h3').text($scope.shortVehiId);
          VehiType = $scope.locations[0].vehicleLocations[0].vehicleType;

        } else { 

          console.log('else..');

          $scope.trackVehID   = getParameterByName('vehicleId');
          $scope.sliceGroup = filterGroup(response);
          angular.forEach(response, function(value, key) {

            if($scope.groupname   == value.group) { 
              $scope.gIndex = value.rowId;
              angular.forEach(value.vehicleLocations, function(val, k) {

                $scope.vehicle_list.push({'vehiID' : val.vehicleId, 'vName' : val.shortName, 'vGroup' : val.groupName});


                if($scope.trackVehID=='undefined') {

                  console.log('hellooo2222');

                  if(k==0){
                    $scope.selected   = k;
                    $scope.shortVehiId  = val.shortName;
                    $scope.selVehiId    = val.vehicleId;
                  //$('#vehiid h3').text(val.shortName);
                  VehiType = val.vehicleType;
                }

              } else {

                if((val.vehicleId).trim() == $scope.trackVehID){
                  $scope.selected   = k;
                  $scope.shortVehiId  = val.shortName;
                  $scope.selVehiId    = val.vehicleId;
                      //$('#vehiid h3').text(val.shortName);
                      VehiType = val.vehicleType;
                    }    
                  }

                });


      // console.log($scope.sliceGroup);
      // groupName   = value[0].group.split(':');
      // $scope.fcode  = groupName[1];
      //     $scope.locations = response;
    } 



  });

        }
        getSpeedandFuel($scope.selVehiId);
        sessionValue($scope.selVehiId, $scope.groupname);

        var fTime  =  getParameterByName('fromTime');
        var tTime  =  getParameterByName('toTime');

        console.log(fTime);
        console.log(tTime);

     //GLOBAL.DOMAIN_NAME+'/getVehicleHistory?vehicleId='+$scope.trackVehID+'&fromDate='+fromdate+'&fromTime='+fromtime+'&toDate='+todate+'&toTime='+totime+'&interval=1'+'&fromDateUTC='+utcFormat(fromdate,fromtime)+'&toDateUTC='+utcFormat(todate,totime);

     if(fTime!=''&&tTime!='') {


      $scope.hisurl = GLOBAL.DOMAIN_NAME+'/getVehicleHistory?vehicleId='+$scope.selVehiId+'&fromDateUTC='+fTime+'&toDateUTC='+tTime;

      if($scope.googleMap==true && $scope.osmMap==false){
        $scope.initMap(0,true);
      } else if($scope.osmMap==true && $scope.googleMap==false){
        $scope.initMap(1,true);
      }



    } else {

          //$scope.hisurl       =  GLOBAL.DOMAIN_NAME+'/getVehicleHistory?vehicleId='+$scope.trackVehID;
          var todayTime       =  new Date();
          var currentTimeRaw  =  new Date();

          todayTime.setHours(todayTime.getHours()-1);
          todayTime.setMinutes(todayTime.getMinutes()-59);
          var timeShow  =  $filter('date')(todayTime, 'HH:mm:ss');

          $scope.fromTimes  =  timeShow; 
          //$scope.toTimes    =  timeNow24Hrs();
          $scope.fromDates  =  getTodayDatess();
          //$scope.toDates    =  getTodayDatess();

          if((checkXssProtection($scope.fromDates) == true) && (checkXssProtection($scope.fromTimes) == true)) {
           //$scope.hisurl = GLOBAL.DOMAIN_NAME+'/getVehicleHistory?vehicleId='+$scope.trackVehID+'&fromDate='+$scope.fromDates+'&fromTime='+$scope.fromTimes+'&toDate='+$scope.toDates+'&toTime='+$scope.toTimes+'&fromDateUTC='+utcFormat($scope.fromDates,$scope.fromTimes)+'&toDateUTC='+utcFormat($scope.toDates,$scope.toTimes);
           $scope.hisurl = GLOBAL.DOMAIN_NAME+'/getVehicleHistory?vehicleId='+$scope.selVehiId+'&fromDate='+$scope.fromDates+'&fromTime='+$scope.fromTimes+'&fromDateUTC='+utcFormat($scope.fromDates,$scope.fromTimes);

           if($scope.googleMap==true && $scope.osmMap==false){
             $scope.initMap(0,true);
           } else if($scope.osmMap==true && $scope.googleMap==false){
             $scope.initMap(1,true);
           }
         }

       }  

       $('.nav-second-level li').eq(0).children('a').addClass('active');
      //stopLoading();

    });

}());

$scope.trimColon = function(textVal){
  return textVal.split(":")[0].trim();
}

$scope.createGeofence = function(url) {

  console.log('--->'+url);

  if($scope.cityCirclecheck==false){
    $scope.cityCirclecheck=true;
  }
  if($scope.cityCirclecheck==true){

    for(var i=0; i<$scope.cityCircle.length; i++){
      $scope.cityCircle[i].setMap(null);

    }

    $scope.cityCircle = [];

    for(var i=0; i<geomarker.length; i++){
      geomarker[i].setMap(null);
       // geoinfo[i].setMap(null);
     }
     
   }
   var defdata = $q.defer();
   $http.get(url).success(function(data){
      //console.log(' url '+data)
      $scope.geoloc = defdata.resolve(data);
      $scope.geoStop = data;
      geomarker=[];
      if (typeof(data.geoFence) !== 'undefined' ) { 

        if(data.geoFence!=null){

          for(var i=0; i<data.geoFence.length; i++){
            if(data.geoFence[i]!=null){
              var populationOptions = {
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0.02,
                map: $scope.map,
                center: new google.maps.LatLng(data.geoFence[i].latitude,data.geoFence[i].longitude),
                radius: parseInt(data.geoFence[i].proximityLevel)
              };

              $scope.cityCircle[i] = new google.maps.Circle(populationOptions);
              var centerPosition = new google.maps.LatLng(data.geoFence[i].latitude, data.geoFence[i].longitude);
              var labelText = data.geoFence[i].poiName;
              $scope.infowin(data.geoFence[i]);

              var myOptions = {
                content: labelText,
                boxStyle: {textAlign: "center", fontSize: "8pt", fontColor: "#0031c4", width: "100px"
              },
              disableAutoPan: true,
              pixelOffset: new google.maps.Size(-50, 0),
              position: centerPosition,
              closeBoxURL: "",
              isHidden: false,
              pane: "mapPane",
              enableEventPropagation: true
            };

            var labelinfo = new InfoBox(myOptions);
            labelinfo.open($scope.map);
            labelinfo.setPosition($scope.cityCircle[i].getCenter());
          }
        }
      }
    }
  });
 }

 $scope.goeValueChange = function()
 {
    //$scope.geoStops;
    //console.log(' ---- >'+$scope.geoStops)
    $scope.map.setZoom(13)
    if($scope.geoStops!=0)
    {
      for(var i = 0; i < $scope.geoStop.geoFence.length; i++)
      {
        if($scope.geoStops==$scope.geoStop.geoFence[i].stopName)
        {
          //$scope.map.setZoom(18);
          $scope.map.setCenter(geomarker[i].getPosition());
          animateMapZoomTo($scope.map, 20);
        }
      }
    }
  }

 // getting Org ids
 $scope.$watch("url_site", function (val) {

  $http.get($scope.url_site).success(function(response){
    $scope.orgIds   = response.orgIds;
    showRoutes(getParameterByName('rt'))
  });
});

 function getRouteNames(){

  if($scope.orgIds){
    $.ajax({

      async: false,
      method: 'GET', 
      url: "storeOrgValues/val",
      data: {"orgId":$scope.orgIds},
      success: function (response) {
        $scope.routedValue = response;
      }
    });
  }
}



$scope.getMap = function(routesMap){

  $scope.windowRouteName=routesMap;

  if(!routesMap =='' && $scope.orgIds.length>0 && !$scope.orgIds==''){
    $.ajax({
      async:false,
      method:'GET',
      url:"storeOrgValues/mapHistory",
      data:{"maproute":routesMap,"organId":$scope.orgIds},
      success:function(response){
        $scope.mapValues =response;
        getMapArray($scope.mapValues);                       }
      });
  }        
}

var latLanpath=[];
var markerList=[];
var pathCoords=[];


function clearMap(path){

  for (var i=0; i<latLanpath.length; i++){
    latLanpath[i].setMap(null);
  }

  for (var i = 0; i < markerList.length; i++) {
    markerList[i].setMap(null);
  }
}

function getMapArray(values){

  var latSplit ;
  var latLangs=values;
  clearMap(pathCoords);
  pathCoords=[];

  for(i=0;i<latLangs.length;i++){

    latSplit = latLangs[i].split(",");
    pathCoords.push({"lat": latSplit[0],"lng": latSplit[1]});
  }

  dvMap.setCenter(new google.maps.LatLng(pathCoords[0].lat,pathCoords[0].lng)); 
  autoRefresh(dvMap);
}

function myMap(){

  var mapCanvas=document.getElementById("dvMap");
  var mapOptions={
    center: new google.maps.LatLng(0,0),
    zoom: 8,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  dvMap=new google.maps.Map(mapCanvas,mapOptions);
}
myMap();

$('#myModal2').on('shown.bs.modal', function () {
  google.maps.event.trigger(dvMap, "resize");
});

function moveMarker(marker, latlng) {

  marker.setPosition(latlng);
}

function autoRefresh(map) {
  var i, route, marker;
  
  route = new google.maps.Polyline({
    path: [],
    geodesic : true,
    strokeColor: '#FF0000',
    strokeOpacity: 1.0,
    strokeWeight: 2,
    editable: false,
    map:map
  });

  latLanpath.push(route);
  marker = new google.maps.Marker({map:map,icon:""});
  markerList.push(marker);
  
  for(i=0; i<pathCoords.length; i++) {

    setTimeout(function (coords){
      var latlng = new google.maps.LatLng(coords.lat, coords.lng);
      route.getPath().push(latlng);
      moveMarker( marker, latlng);
      map.panTo(latlng);

    }, 0* i, pathCoords[i]);
  }
}

  /*
    show table in view
    */

    $( "#historyDetails" ).hide();

    $scope.hideShowTable  = function(){
      var btValue = ($("#btnValue").text()=='ShowDetails')?'HideDetails':'ShowDetails';
      $('#btnValue').text(btValue);
      $( "#historyDetails" ).fadeToggle("slow");
    }

  /*
    Store the routes in redis
    */
    $scope.routesSubmit =  function(){
      $scope.error = "";
      console.log(' get org ids ')
      var fromdate  = document.getElementById('dateFrom').value;
      var todate    = document.getElementById('dateTo').value;
      var fromtime  = document.getElementById('timeFrom').value;
      var totime    = document.getElementById('timeTo').value;
      if((checkXssProtection(fromdate) == true) && (checkXssProtection(todate) == true) && (checkXssProtection(fromtime) == true) && (checkXssProtection(totime) == true) && (checkXssProtection($scope.routeName) == true))
        try{
          $scope.error = (!fromdate || !todate || !fromtime || !totime)? "Please enter the Route Name":  "";
          if($scope.error == "")
          {
            var utcFrom   = utcFormat(fromdate, $scope.timeconversion(fromtime));
            var utcTo     = utcFormat(todate, $scope.timeconversion(totime));
            var _routeUrl   = GLOBAL.DOMAIN_NAME+'/addRoutesForOrg?vehicleId='+$scope.trackVehID+'&fromDateUTC='+utcFrom+'&toDateUTC='+utcTo+'&routeName='+removeSpace_Join($scope.routeName);
            if(!$scope.trackVehID == "" && !$scope.routeName == "")
              $http.get(_routeUrl).success(function(response){
                if(response.trim()== "false"){
                  $scope.error = "* " + translate("Route Name already exists");
                } else if (response.trim()== "true"){
                  $scope.error = "* "+translate("Successfully saved");
                  getRouteNames();
                } else
                $scope.error = "* "+translate("Try again");
              })
            else
              throw $scope.error;

          } else 
          throw $scope.error;
          getRouteNames();
        }catch(err){
          console.log(' error --> '+err)
          $scope.error  = "* "+translate("Please enter the Route Name");
        }
      }

      $scope.popUpMarkerNull =function()
      {
        if($scope.popupmarker !== undefined){
          $scope.popupmarker.setMap(null);
          $scope.infowindow.close();
          $scope.infowindow.setMap(null);
        }
      }


      $scope.markerPoup   =   function(val) {

       if($scope.googleMap==true){

        $scope.popUpMarkerNull();
        $scope.popupmarker = new google.maps.Marker({
          icon: 'assets/imgs/popup.png',
        });

        var latLngs = new google.maps.LatLng(val.latitude, val.longitude);

        $scope.popupmarker.setMap($scope.map);
        $scope.popupmarker.setPosition(latLngs);

        var contentString = '<div style="padding:2px; padding-top:3px; width:190px;">'
        +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;color:#666463;">'+translate("Date&Time")+'</b> <span style="padding-left:9px;">-</span> <span style="font-size:10px;padding-left:3px;color:#666463;font-weight:bold;border-bottom:0.5px;">'+dateFormat(val.date)+'</span></div>'
        +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;color:#666463;">'+translate("Speed")+'</b> <span style="padding-left:9px;">-</span> <span style="font-size:10px;padding-left:3px;color:#666463;font-weight:bold;border-bottom:0.5px;">'+ val.speed +'</span><span style="font-size:10px;padding-left:10px;border-bottom:0.5px;">'+translate("kmph")+'</span></div>'
        +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;color:#666463;">'+translate("OdoDist")+'</b> <span style="padding-left:9px;">-</span> <span style="font-size:10px;padding-left:3px;color:#666463;font-weight:bold;border-bottom:0.5px;">'+val.odoDistance+'</span><span style="font-size:10px;padding-left:10px;border-bottom:0.5px;">'+translate("kms")+'</span></div>'
        +'<div style="text-align:center;padding-top:3px;"><span style="font-size:10px;width:100px;">'+$scope.trimComma(val.address)+'</span></div>'
 // +'<div style="overflow-wrap: break-word; border-top: 1px solid #eee">'+data.address+'</div>'
 +'</div>';

 /* $scope.infowindow = new InfoBubble({
      maxWidth: 400,  
      maxHeight:170,
      content: contentString,
    });*/

    $scope.infowindow = new google.maps.InfoWindow({ maxWidth: 200, maxHeight:150 });

    $scope.infowindow.setContent(contentString); 
    $scope.infowindow.setPosition(latLngs);
    $scope.infowindow.open($scope.map, $scope.popupmarker);

    google.maps.event.addListener($scope.infowindow,'closeclick',function(){
     $scope.popUpMarkerNull();
   }); 

  } else if($scope.osmMap==true){

    if($scope.popupmarker_osm!=undefined){
     $scope.map_osm.removeLayer($scope.popupmarker_osm);
   }


   var latlng_osm = new L.LatLng(val.latitude, val.longitude);

   var popupIcon = L.icon({
    iconUrl: 'assets/imgs/popup.png',
    iconSize: [40,40], 
    iconAnchor: [20,40],
    popupAnchor: [-1, -40],
  });

   var contentString_osm = '<div style="padding:2px; padding-top:3px; width:190px;">'
   +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;color:#666463;">'+translate("Date&Time")+'</b> <span style="padding-left:9px;">-</span> <span style="font-size:10px;padding-left:3px;color:#666463;font-weight:bold;border-bottom:0.5px;">'+dateFormat(val.date)+'</span></div>'
   +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;color:#666463;">'+translate("Speed")+'</b> <span style="padding-left:9px;">-</span> <span style="font-size:10px;padding-left:3px;color:#666463;font-weight:bold;border-bottom:0.5px;">'+ val.speed +'</span><span style="font-size:10px;padding-left:10px;border-bottom:0.5px;">'+translate("kmph")+'</span></div>'
   +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;color:#666463;">'+translate("OdoDist")+'</b> <span style="padding-left:9px;">-</span> <span style="font-size:10px;padding-left:3px;color:#666463;font-weight:bold;border-bottom:0.5px;">'+val.odoDistance+'</span><span style="font-size:10px;padding-left:10px;border-bottom:0.5px;">'+translate("kms")+'</span></div>'
   +'<div style="text-align:center;padding-top:3px;"><span style="font-size:10px;width:100px;">'+$scope.trimComma(val.address)+'</span></div>'
 // +'<div style="overflow-wrap: break-word; border-top: 1px solid #eee">'+data.address+'</div>'
 +'</div>';


 var infoPopup_osm = new L.popup({maxWidth: 400,  
  maxHeight:170}).setContent(contentString_osm);

 $scope.popupmarker_osm  =  new L.marker();
 $scope.popupmarker_osm.bindPopup(infoPopup_osm);
 $scope.popupmarker_osm.setIcon(popupIcon);
 $scope.popupmarker_osm.setLatLng(latlng_osm).addTo($scope.map_osm);
 $scope.popupmarker_osm.openPopup();

 $scope.popupmarker_osm.addEventListener('click', function(e) {
  $scope.popupmarker_osm.openPopup();
});


   /*       myIcons = L.icon({
            iconUrl: vamoservice.iconURL($scope.hisloc.vehicleLocations[lineCount_osm]),
            iconSize: [40,40], 
            iconAnchor: [20,40],
            popupAnchor: [-1, -40],
        });

        $scope.infowindowss.setContent(contenttsString);
        $scope.markerss.bindPopup($scope.infowindowss);
        $scope.markerss.setIcon(myIcons);
        $scope.markerss.setLatLng($scope.osmPath[lineCount_osm]);

        if(lineCount_osm==0){   
          $scope.markerss.addTo($scope.map_osm);
          $scope.markerss.openPopup();

          */
        }

      }

      $scope.callValue = function(val){
        vehicleChanged=1;
        console.log(val);
        $scope.trackVehID   =  val.vehiID;
        $scope.selVehiId    =  val.vehiID;
        $scope.shortVehiId  =  val.vName;
        $scope.groupname = val.vGroup;
        console.log($scope.groupname);
        $scope.hideButton   =  false;
        $scope.btn5Hrs      =  false;
        $scope.btn12Hrs     =  false;
        $scope.btn1Day      =  false;
        $scope.btn2Day      =  false;
      }

      $scope.deleteRouteName  = function(deleteValue){
   // $scope.routeName = deleteValue;
   console.log(deleteValue);
   try{

    if(!deleteValue =='' && $scope.orgIds.length>0 && !$scope.orgIds=='')
      $.ajax({

        async: false,
        method: 'GET', 
        url: "storeOrgValues/deleteRoutes",
        data: {"delete":deleteValue, "orgIds":$scope.orgIds},
        success: function (response) {

                // $scope.routedValue = response;

              }
            })
    getRouteNames();
    $scope.error =  "* "+translate("Deleted Successfully");

  } catch (err){

    $scope.error =  "* "+translate("Not Deleted Successfully");
  }
    // console.log($(this).parent().parent().find('td').text())
    // console.log($('.editAction').closest('tr').children('td:eq(0)').text());

  }

  

  $('.dynData').on("click", "#editAction", function(event){
    var target    = $(this).closest('tr').children('td:eq(0)')
    $scope.error  = ""
    // $(target).html($('<input />',{'value' : target.text()}).val(target.text()));

    // $(this).siblings().each(
    //     function(){
            // if the td elements contain any input tag
            if(!target.text() == '');
            if ($(target).find('input').length){
                // sets the text content of the tag equal to the value of the input
                $(target).text($(target).find('input').val());
              }
              else {
                // removes the text, appends an input and sets the value to the text-value
                var t = $(target).text();
                $(target).html($('<input />',{'value' : target.text()}).val(target.text()));
              }
        // });

      });

  $('.dynData').on("change", $(this).closest('tr').children('td:eq(0) input'), function(event){
    console.log(' new value '+event.target.value)
    $scope.error =  ""
    var _newValue = event.target.value;
    var _oldValue = event.target.defaultValue;
    try{

      if(!_newValue =='' && $scope.orgIds.length>0 && !$scope.orgIds=='')
        $.ajax({

          async: false,
          method: 'GET', 
          url: "storeOrgValues/editRoutes",
          data: {"newValue":_newValue, "oldValue":_oldValue, "orgIds":$scope.orgIds},
          success: function (response) {

                // $scope.routedValue = response;
                $scope.error =  "* "+translate("Edited Successfully");

              }
            })

    } catch (err){

      $scope.error =  "* "+translate("Not Edited Successfully");
    }
    

  })

 // $(document).ready(function() {
 //                $('.dynData table tbody tr td input').change(function() {
 //                    var rowEdit = $(this).parents('tr');
 //                    alert(rowEdit);
 //                    console.log($(rowEdit));
 //                    $(rowEdit).children('.sub').html('Success');
 //                })
 //            })

 function animateMapZoomTo(map, targetZoom) {
  var currentZoom = arguments[2] || map.getZoom();
  if (currentZoom != targetZoom) {
    google.maps.event.addListenerOnce(map, 'zoom_changed', function (event) {
      animateMapZoomTo(map, targetZoom, currentZoom + (targetZoom > currentZoom ? 1 : -1));
    });
    setTimeout(function(){ map.setZoom(currentZoom) }, 80);
  }
}

function smoothZoom (map, max, cnt) {
  if (cnt >= max) {
    return;
  }
  else {
    z = google.maps.event.addListener(map, 'zoom_changed', function(event){
      google.maps.event.removeListener(z);
      smoothZoom(map, max, cnt + 1);
    });
    setTimeout(function(){map.setZoom(cnt)}, 80);
  }
}  

$scope.infowin = function(data){

  console.log(data);

  var centerPosition = new google.maps.LatLng(data.latitude, data.longitude);
  var labelText = data.poiName;
  var stop = data.stopName;
  var image = 'assets/imgs/busgeo.png';

  var beachMarker = new google.maps.Marker({
    position: centerPosition,
    title: stop,
    map: $scope.map,
    icon: image
  });
  geomarker.push(beachMarker);
          // var myOptions = { content: labelText, boxStyle: {textAlign: "center", fontSize: "9pt", fontColor: "#ff0000", width: "100px"},
            // disableAutoPan: true,
            // pixelOffset: new google.maps.Size(-50, 0),
            // position: centerPosition,
            // closeBoxURL: "",
            // isHidden: false,
            // pane: "mapPane",
            // enableEventPropagation: true
          // };
          // var labelinfo = new InfoBox(myOptions);
          // labelinfo.open($scope.map);simple
          // labelinfo.setPosition($scope.cityCircle[i].getCenter());
          // geoinfo.push(labelinfo);

          var contentString1 = '<h3 class="infoh3">'+translate("location")+'</h3>'
          +'<div class="nearbyTable02"><div><table cellpadding="0" cellspacing="0"><tbody>'
          +'<tr><td>'+translate("Latlong")+'</td><td>'+data.geoLocation+' </td></tr>'+'<tr><td>'+ translate("StopName")+'</td><td>'+data.stopName+' </td></tr>'
          +'</table></div>';
          var infoWindow = new google.maps.InfoWindow({content:contentString1});
          geoinfowindow.push(infoWindow);
          
          (function(marker) {
            google.maps.event.addListener(beachMarker, "click", function(e) {
              for(var j=0; j<geoinfowindow.length;j++){
                geoinfowindow[j].close();
              }
              infoWindow.open($scope.map, marker);
              
            }); 
          })(beachMarker);
        }


        $scope.genericFunction = function(a,b,shortName){
          startLoading();
          $scope.path = [];
          gmarkers=[];
          ginfowindow=[];
          contentString = [];
          $scope.trackVehID = a;
          $scope.shortVehiId = shortName;
          $scope.selected = b;
          $scope.plotting();
          sessionValue($scope.trackVehID, $scope.groupname);
    // stopLoading();
  }
  
  $scope.groupSelection = function(groupname, groupid){

    startLoading();
    $scope.selected=0;
    $scope.url = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group=' + groupname;
    $scope.gIndex = groupid;
    gmarkers=[];
    ginfowindow=[];
    $scope.loading = true;

     /*for(var i=0; i<gmarkers.length; i++){
        gmarkers[i].setMap(null);
      }
      */
      if($scope.polyline){

        for(var i=0; i<$scope.polyline1.length; i++){
          $scope.polyline1[i].setMap(null);
        }

        $scope.polyline.setMap(null);
      }

      if($scope.markerstart){

        $scope.markerstart.setMap(null);
        $scope.markerend.setMap(null);
        $scope.path = [];
        $scope.polylinearr = [];
        gmarkers=[];
        ginfowindow=[];
        contentString = [];
        gsmarker=[];
        gsinfoWindow=[];
        window.clearInterval(id);
        $('#replaybutton').attr('disabled','disabled');
        $('#lastseen').html('<strong>'+translate("From Date")+' & '+translate('time')+' :</strong> -');
        $('#lstseendate').html('<strong>'+translate("To Date")+' & '+translate('time')+' :</strong> -');
      }

      $http.get($scope.url).success(function(data){

       $scope.locations = data;

       if(data.length)
        $scope.vehiname = data[$scope.gIndex].vehicleLocations[0].vehicleId;
      $scope.groupname  = data[$scope.gIndex].group;
      $scope.trackVehID   = $scope.locations[$scope.gIndex].vehicleLocations[$scope.selected].vehicleId;
      sessionValue($scope.trackVehID, $scope.groupname);
      $scope.hisurl = GLOBAL.DOMAIN_NAME+'/getVehicleHistory?vehicleId='+$scope.selVehiId;
      $('.nav-second-level li').eq(0).children('a').addClass('active');
      $scope.loading  = false;

                //stopLoading();
              }).error(function(){ stopLoading();});
    }

    $scope.pointMarker=function(data){

      var line0Symbol = {
        path: google.maps.SymbolPath.CIRCLE,
        scale:3,
        strokeColor: '#0645AD',
        strokeWeight: 5
      };
      
      var marker = new google.maps.Marker({
        map: $scope.map,
        position: new google.maps.LatLng(data.latitude, data.longitude),
        icon:line0Symbol
      });

      gsmarker.push(marker);

      var contentString01 = '<div class="nearbyTable02"><div><table cellpadding="0" cellspacing="0"><tbody></tbody></table></div>';
      var infoWindow = new google.maps.InfoWindow({content: contentString01});
      gsinfoWindow.push(infoWindow);  
      
      (function(marker) {
        google.maps.event.addListener(marker, "click", function(e) {
          $scope.infowindowShow={};
          $scope.infowindowShow['dataTempVal'] =  data;
          $scope.infowindowShow['currinfo'] = infoWindow;
          $scope.infowindowShow['currmarker'] = marker;
          for(var j=0; j<gsinfoWindow.length;j++){
            gsinfoWindow[j].close();
          }
          $scope.infowindowshowFunc();
        });  
      })(marker);
    }

    $scope.infowindowshowFunc = function(){
      for(var j=0; j<gsinfoWindow.length;j++){
        gsinfoWindow[j].close();
      }
      geocoder = new google.maps.Geocoder();
      var latlng = new google.maps.LatLng(+$scope.infowindowShow.dataTempVal.latitude, +$scope.infowindowShow.dataTempVal.longitude);
      geocoder.geocode({'latLng': latlng}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          if (results[1]) {
            var contentString = '<div class="nearbyTable02"><div><table cellpadding="0" cellspacing="0"><tbody>'
            +'<tr><td>'+translate("Speed")+' </td><td>'+$scope.infowindowShow.dataTempVal.speed+'</td></tr>'
            +'<tr><td>'+translate("date&time")+'</td><td>'+dateFormat($scope.infowindowShow.dataTempVal.date)+'</td></tr>'
            +'<tr><td>'+translate("trip_distance")+'</td><td>'+$scope.infowindowShow.dataTempVal.tripDistance+'</td></tr>'
            +'<tr><td style="widthpx">'+translate("location")+'</td><td style="width:100px">'+results[1].formatted_address+'</td></tr>'
            +'</tbody></table></div>';
            $scope.infowindowShow.currinfo.setContent(contentString);
            $scope.infowindowShow.currinfo.open($scope.map,$scope.infowindowShow.currmarker);
          }

        }
      });
    }

    $scope.pointinfowindow=function(map, marker, data){

      if(gsmarker.length>0){
      }
    }


    function saveAddressFunc(val, lat, lan){
    //console.log(val);
    var saveAddUrl = GLOBAL.DOMAIN_NAME+'/saveAddress?address='+encodeURIComponent(val)+'&lattitude='+lat+'&longitude='+lan+'&status=web';
    //console.log(saveAddUrl);

    $http({
      method: 'GET',
      url: saveAddUrl
    }).then(function successCallback(response) {
      if(response.status==200){
        console.log("Save address successfully!..");
      }
    }, function errorCallback(response) {
     console.log(response.status);
   });

  }


  $scope.locationname="";
  $scope.getLocation = function(lat,lon, callback){

    var tempurl01 =  "https://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+','+lon;
    var t = $.ajax({
      dataType:"json",
      url: tempurl01,
      success:function(data){

        if(data.results[0]!=undefined){

          var newVals = vamoservice.googleAddress(data.results[0]);   
          saveAddressFunc(newVals, lat, lon);

          if(data.results[0].formatted_address==undefined){
            if(typeof callback === "function") callback('');
          }else{
            if(typeof callback === "function") callback(data.results[0].formatted_address);
          }
        }else{
          if(typeof callback === "function") callback('');
        }
      },
      error:function(xhr, status, error){
        //if(typeof callback === "function") callback('');
        console.log('error:' + status + error);
      }
    });
    
  };  

  $scope.timeCalculate = function(duration){
    var milliseconds = parseInt((duration%1000)/100), seconds = parseInt((duration/1000)%60);
    var minutes = parseInt((duration/(1000*60))%60), hours = parseInt((duration/(1000*60*60))%24);
    
    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;
    temptime = hours + " H : " + minutes +' M : ' +seconds +' S';

    var s=duration;
    function addZ(n) {
      return (n<10? '0':'') + n;
    }

    var ms = s % 1000;
    s = (s - ms) / 1000;
    var secs = s % 60;
    s = (s - secs) / 60;
    var mins = s % 60;
    var hrs = (s - mins) / 60;

    var tempTim = addZ(hrs) + ' H : ' + addZ(mins) + ' M : ' + addZ(secs) + ' S '; //+ ms;
    return tempTim;
  }

  // function parseDate(str) {
  //     var mdy = str.split('/');
  //     return new Date(mdy[2], mdy[0]-1, mdy[1]);
  // }
  
  $scope.msToTime   = function(ms) {
    days = Math.floor(ms / (24*60*60*1000));
    daysms=ms % (24*60*60*1000);
    hours = Math.floor((ms)/(60*60*1000));
    hoursms=ms % (60*60*1000);
    minutes = Math.floor((hoursms)/(60*1000));
    minutesms=ms % (60*1000);
    sec = Math.floor((minutesms)/(1000));
    hours = (hours<10)?"0"+hours:hours;
    minutes = (minutes<10)?"0"+minutes:minutes;
    sec = (sec<10)?"0"+sec:sec;
    return hours+":"+minutes+":"+sec;
  }

  function utcdateConvert(milliseconds){
    //var milliseconds=1440700484003;
    var offset='+10';
    var d = new Date(milliseconds);
    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
    nd = new Date(utc + (3600000*offset));
    var result=nd.toLocaleString();
    return result;
  }

  $scope.timeconversion= function(time){
    var time = time;
    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AMPM = time.match(/\s(.*)$/)[1];
    if(AMPM == "PM" && hours<12) hours = hours+12;
    if(AMPM == "AM" && hours==12) hours = hours-12;
    var sHours = hours.toString();
    var sMinutes = minutes.toString();
    if(hours<10) sHours = "0" + sHours;
    if(minutes<10) sMinutes = "0" + sMinutes;
    return sHours+":"+sMinutes+":00";
  }
  $scope.timeconversion2= function(time){
    var time = time;
    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AMPM = time.match(/\s(.*)$/)[1];
    if(AMPM == "PM" && hours<12) hours = hours+12;
    if(AMPM == "AM" && hours==12) hours = hours-12;
    var sHours = hours.toString();
    var sMinutes = minutes.toString();
    if(hours<10) sHours = "0" + sHours;
    if(minutes<10) sMinutes = "0" + sMinutes;
    return sHours+":"+sMinutes+":59";
  }

  $scope.showPlot = function(totime,todate,fromtime,fromdate)
  {
    $scope.hideButton = false;
  }

  $scope.startLoading = function(){
    startLoading();
  }
  function setTimePickerValue(fromtime,totime,fromdate,todate){
    $scope.fromtime  = fromtime;
    $scope.totime    = totime;
    $scope.fromdate  = fromdate;
    $scope.todate    = todate;
  }

  $scope.plotting = function(val,parameter){
    //console.log("Plotting button pressed");

   // $scope.cityCirclecheck=false;
   
   getSpeedandFuel($scope.selVehiId);
   distanceInterval=2;
   gDistanceInterval=2;
   $scope.plotVal=1; 

   clearInterval(timeInterval);
   clearInterval($scope.osmInterVal);

   startLoading();

   var allDataOk     = 0;
   $scope.hideButton = true;

    // (function(){
    //   $scope.hideButton = false;
    // },5000);

    if (val != 1){
      var fromdate = document.getElementById('dateFrom').value;
      var todate = document.getElementById('dateTo').value;
    }

    if((checkXssProtection(fromdate) == true) && (checkXssProtection(todate) == true) && (checkXssProtection(document.getElementById('timeTo').value) == true) && (checkXssProtection(document.getElementById('timeFrom').value) == true)){
      startLoading();
      $scope.hideOverlay = true;

      //$scope.popUpMarkerNull();
      $scope.hisurlold = $scope.hisurl;

      switch (val){
        case 1: {

          $scope.showLoader = true;
          $scope.btn5Hrs = true;
          $scope.btn12Hrs= false;
          $scope.btn1Day = false;
          $scope.btn2Day = false;

          var todayTime = new Date();
          var currentTimeRaw = new Date();
          var currentTime = $filter('date')(currentTimeRaw,'HH:mm:ss');
          var fromDateRaw = $filter('date')(currentTimeRaw,'dd-MM-yyyy');
          todayTime.setHours(todayTime.getHours()-4);
          todayTime.setMinutes(todayTime.getMinutes()-59);
          $scope.HHmmss = $filter('date')(todayTime, 'HH:mm:ss');
          
          var fromtime = $scope.HHmmss;
          var totime = currentTime ;
          var fromdate = fromDateRaw;
          var todate = fromDateRaw;
          setTimePickerValue($filter('date')(todayTime, 'hh:mm a'),$filter('date')(currentTimeRaw,'hh:mm a'),fromdate,todate);

          break;

        }case 2: {

          $scope.showLoader = true;
          $scope.btn5Hrs = false;
          $scope.btn12Hrs= true;
          $scope.btn1Day = false;
          $scope.btn2Day = false;

          var todayTime  = new Date();
          var newYesDate = new Date();

          var currentTimeRaw = new Date();
          var currentTime = $filter('date')(currentTimeRaw,'HH:mm:ss');
          var fromDateRaw = $filter('date')(currentTimeRaw,'dd-MM-yyyy');
          todayTime.setHours(todayTime.getHours()-11);
          todayTime.setMinutes(todayTime.getMinutes()-59);
          var previousDate = newYesDate.setDate(newYesDate.getDate()-1);
          $scope.HHmmss = $filter('date')(todayTime, 'HH:mm:ss');
        // alert($scope.HHmmss);
        var fromtime = $scope.HHmmss;
        var totime = currentTime ;
        var fromdate = $filter('date')(previousDate,'dd-MM-yyyy');
        var todate = fromDateRaw;
        setTimePickerValue($filter('date')(todayTime, 'hh:mm a'),$filter('date')(currentTimeRaw,'hh:mm a'),fromdate,todate);

        break;
        
      }case 3:  {

        $scope.showLoader = true;
        $scope.btn5Hrs = false;
        $scope.btn12Hrs= false;
        $scope.btn1Day = true;
        $scope.btn2Day = false;
        var todayTime  = new Date();
        var newYesDate = new Date();

        var currentTimeRaw = new Date();
        var currentTime = $filter('date')(currentTimeRaw,'HH:mm:ss');
        var fromDateRaw = $filter('date')(currentTimeRaw,'dd-MM-yyyy');
        todayTime.setHours(todayTime.getHours()-23);
        todayTime.setMinutes(todayTime.getMinutes()-59);
        var previousDate = newYesDate.setDate(newYesDate.getDate());
        $scope.HHmmss = $filter('date')(todayTime, 'HH:mm:ss');
        // alert($scope.HHmmss);

        var fromtime = '00:00:00';//$scope.HHmmss;
        var totime = currentTime ;
        var fromdate = $filter('date')(previousDate,'dd-MM-yyyy');
        var todate = fromDateRaw;
        setTimePickerValue("12:00 AM",$filter('date')(todayTime, 'hh:mm a'),fromdate,todate);

        break;

      }case 4:  {

        $scope.showLoader = true;
        $scope.btn5Hrs = false;
        $scope.btn12Hrs= false;
        $scope.btn1Day = false;
        $scope.btn2Day = true;

        var todayTime  = new Date();
        var newYesDate = new Date();

        var currentTimeRaw = new Date();
        var currentTime = $filter('date')(currentTimeRaw,'HH:mm:ss');
        var fromDateRaw = $filter('date')(currentTimeRaw,'dd-MM-yyyy');
        todayTime.setHours(todayTime.getHours()-1);
        todayTime.setMinutes(todayTime.getMinutes()-59);
        var previousDate = newYesDate.setDate(newYesDate.getDate()-1);
        var fromYesDate = newYesDate.setDate(newYesDate.getDate()-1);
        $scope.HHmmss = $filter('date')(todayTime, 'HH:mm:ss');
        var fromtime ="00:00:00";
        var totime = "23:59:59";
        var fromdate = $filter('date')(previousDate,'dd-MM-yyyy');
        var todate = $filter('date')(previousDate,'dd-MM-yyyy');
        setTimePickerValue("12:00 AM","11:59 PM",fromdate,todate)

        break;
      }
      default:  {

        if(document.getElementById('timeFrom').value==''){
         var fromtime = "00:00:00";
       }else{
         var fromtime = $scope.timeconversion(document.getElementById('timeFrom').value);
       }
       if(document.getElementById('timeTo').value==''){
         var totime = "00:00:00";
       }else{
         var totime = $scope.timeconversion2(document.getElementById('timeTo').value);
       }
     }
   }


   if(document.getElementById('dateFrom').value==''){

    if(document.getElementById('dateTo').value==''){

     $scope.hisurl = GLOBAL.DOMAIN_NAME+'/getVehicleHistory?vehicleId='+$scope.selVehiId;

     if($scope.googleMap==true && $scope.osmMap==false){
       $scope.initMap(0,true);
     } else if($scope.osmMap==true && $scope.googleMap==false){
       $scope.initMap(1,true);
     } 
   }

 } else {

  if(document.getElementById('dateTo').value==''){
    $scope.hisurl = GLOBAL.DOMAIN_NAME+'/getVehicleHistory?vehicleId='+$scope.selVehiId+'&fromDate='+fromdate+'&fromTime='+fromtime;
    if($scope.googleMap==true && $scope.osmMap==false){
     $scope.initMap(0,true);
   } else if($scope.osmMap==true && $scope.googleMap==false){
     $scope.initMap(1,true);
   } 

 } else {

  var changedfdate=moment(fromdate,'DD-MM-YYYY').format('YYYY-MM-DD');
  var changedtdate=moment(todate,'DD-MM-YYYY').format('YYYY-MM-DD');


  var days =daydiff(new Date(changedfdate), new Date(changedtdate));

  if (changedfdate == changedtdate)
  {
    if (fromtime ==  totime)
    {
      stopLoading();
    } else {

      $scope.hisurl = GLOBAL.DOMAIN_NAME+'/getVehicleHistory?vehicleId='+$scope.selVehiId+'&fromDate='+changedfdate+'&fromTime='+fromtime+'&toDate='+changedtdate+'&toTime='+totime+'&fromDateUTC='+utcFormat(fromdate,fromtime)+'&toDateUTC='+utcFormat(todate,totime);
      if($scope.googleMap==true && $scope.osmMap==false){
       $scope.initMap(0,true);
     } else if($scope.osmMap==true && $scope.googleMap==false){
       $scope.initMap(1,true);
     }

   }
 } 
 else
 {
  var fdate=new Date(changedfdate);
  var tdate=new Date(changedtdate);
  if(days<=3 && days>0){

    $scope.hisurl = GLOBAL.DOMAIN_NAME+'/getVehicleHistory?vehicleId='+$scope.selVehiId+'&fromDate='+changedfdate+'&fromTime='+fromtime+'&toDate='+changedtdate+'&toTime='+totime+'&fromDateUTC='+utcFormat(fromdate,fromtime)+'&toDateUTC='+utcFormat(todate,totime);
    if($scope.googleMap==true && $scope.osmMap==false){
      $scope.initMap(0,true);
    } else if($scope.osmMap==true && $scope.googleMap==false){
      $scope.initMap(1,true);
    }

  }

  else if(fdate>tdate){
    alert('Please select valid date range'); 

    $scope.hideButton=false;
    stopLoading();
  }

  else if(days < 7) {

    $scope.hisurl = GLOBAL.DOMAIN_NAME+'/getVehicleHistory?vehicleId='+$scope.selVehiId+'&fromDate='+changedfdate+'&fromTime='+fromtime+'&toDate='+changedtdate+'&toTime='+totime+'&interval=1'+'&fromDateUTC='+utcFormat(fromdate,fromtime)+'&toDateUTC='+utcFormat(todate,totime);
    if($scope.googleMap==true && $scope.osmMap==false){
     $scope.initMap(0,true);
   } else if($scope.osmMap==true && $scope.googleMap==false){
     $scope.initMap(1,true);
   }

 } else {
  alert(translate('Please select date range within 7 days.'));
  

  $scope.hideButton=false;
  stopLoading();
}

}
}
}

if($scope.hisurlold!=$scope.hisurl){        

  for(var i=0; i<gmarkers.length; i++){
    gmarkers[i].setMap(null);
  }

  for(var i=0; i<$scope.polyline1.length; i++){
    $scope.polyline1[i].setMap(null);
  }

  $scope.path = [];
  $scope.polylinearr = [];
  gmarkers=[];
  ginfowindow=[];
  contentString = [];
  gsmarker=[];
  gsinfoWindow=[];

  if($scope.mhValue==1){
             //window.clearInterval(intervalPoly);  
             if($scope.markerValue != 1){
               //$scope.infosWindows.close();
               $scope.infosWindows=null;
               $scope.markerheads.setMap(null);
             }
           }  

           if($scope.markerValue==1){
               // window.clearInterval(timeInterval);
               $scope.polyline.setMap(null);
               $scope.markerstart.setMap(null);
               $scope.markerend.setMap(null);
               $scope.infosWindow.close();
               $scope.infosWindow=null;
               $scope.markerheads.setMap(null);
               $scope.markerValue=0;
             }

      //  $('#replaybutton').attr('disabled','disabled');
      $('#lastseen').html('<strong>'+translate("From Date")+' &'+translate("time")+' :</strong> -');
      $('#lstseendate').html('<strong>'+translate("To Date")+' &'+translate("time")+' :</strong> -');

    }else{
      if($scope.hisloc.error!=null || $scope.hisloc.error == undefined){

          // $('#myModal').modal();
          // alert('Please selected date before 7 days / No data found')
          //stopLoading();
        }
      }
    }
    
  //stopLoading();
}

$scope.addMarker= function(pos) {

  var myLatlng        =  new google.maps.LatLng(pos.lat, pos.lng);
  var labelAnchorpos  =  new google.maps.Point(12, 37);
  
  /*  if(pos.data.insideGeoFence =='Y'){
      //pinImage = 'assets/imgs/F_'+pos.data.direction+'.png';
        pinImage = 'assets/imgs/trans.png';
    }
    else{  */
      if(pos.data.position =='P') {
      //pinImage = 'assets/imgs/'+pos.data.position+'.png';
      pinImage = 'assets/imgs/flag.png';

      $scope.marker = new MarkerWithLabel({
       position: pos.path, 
       map: $scope.map,
       icon: pinImage,
       labelContent: '',/*pos.data.position,*/
      // labelAnchor: labelAnchorpos,
      // labelClass: "labels", 
      labelInBackground: true
    });

      gmarkers.push($scope.marker);  
    } 
    //else 
    if(pos.data.position =='S') {

     // pinImage = 'assets/imgs/A_'+pos.data.direction+'.png';
     pinImage = 'assets/imgs/orange.png';


     $scope.marker = new MarkerWithLabel({
       position: pos.path, 
       map: $scope.map,
       icon: pinImage,
       labelContent: '',/*pos.data.position,*/
        // labelAnchor: labelAnchorpos,
        // labelClass: "labels", 
        labelInBackground: true
      });

     gmarkers.push($scope.marker);  
   }
  //}
  /* if(pos.data.position !='P' &&  pos.data.position !='S'){
       pinImage = 'assets/imgs/trans.png';
     } */

   } 


   $scope.geocoder = function(lat, lng, callback){
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          if(results[1].formatted_address==undefined){
            if(typeof callback === "function") callback("No Result");
          }else{
            if(typeof callback === "function") callback(results[1].formatted_address);
          }
        } else {
         console.log('No results found');
       }
     } else {
       console.log('Geocoder failed due to: ' + status);
     }
   });
  }

  $scope.addMarkerstart= function(pos){

    var myLatlng = new google.maps.LatLng(pos.lat, pos.lng);
    $scope.map.setCenter(myLatlng);
    pinImage = 'assets/imgs/startflag.png';

    $scope.markerstart = new MarkerWithLabel({
     position: pos.path, 
     map: $scope.map,
     icon: pinImage
   });
    
    $scope.geocoder(pos.lat, pos.lng, function(count){
      $scope.tempadd = count;
    });

    var contentString = '<h3 class="infoh3">'+$scope.vehiLabel+' '+translate("Details")+' ('+$scope.shortVehiId+') </h3><div class="nearbyTable02"><div><table cellpadding="0" cellspacing="0"><tbody><!--<tr><td>'+translate("location")+'</td><td>'+$scope.tempadd+'</td></tr>--><tr><td>'+translate("Last_seen")+' </td><td>'+dateFormat(pos.data.date)+'</td></tr><tr><td>'+translate("Parked_Time")+'</td><td>'+$scope.timeCalculate(pos.data.parkedTime)+'</td></tr><tr><td>'+translate("trip_distance")+'</td><td>'+pos.data.tripDistance+'</td></tr></table></div>';
    var infoWindow = new google.maps.InfoWindow({content: contentString});
    
    google.maps.event.addListener($scope.markerstart, "click", function(e){
      infoWindow.open($scope.map, $scope.markerstart);  
    });
    
    (function(marker, data, contentString) {
      google.maps.event.addListener(marker, "click", function(e) {
        infoWindow.open($scope.map, marker);
      }); 
    })($scope.markerstart, pos.data);
  }

  $scope.addMarkerend= function(pos){
    var myLatlng = new google.maps.LatLng(pos.lat, pos.lng);
    pinImage = 'assets/imgs/endflag.png';
    $scope.markerend = new MarkerWithLabel({
     position: pos.path, 
     map: $scope.map,
     icon: pinImage
   });
    $scope.geocoder(pos.lat, pos.lng, function(count){
      if(count!=undefined){
        $scope.tempadd = count;
      }else{
        $scope.tempadd = 'No Result';
      }
      
      var contentString = '<h3 class="infoh3">'+$scope.vehiLabel+''+translate("Details")+' ('+$scope.shortVehiId+') </h3>'
      +'<div class="nearbyTable02"><div><table cellpadding="0" cellspacing="0"><tbody>'
      +'<tr><td>'+translate("location")+' </td><td>'+$scope.tempadd+'</td></tr><tr><td>'+translate("Last_seen")+'</td><td>'+dateFormat(pos.data.date)+'</td>'
      +'</tr><tr><td>'+translate("Parked_Time")+'</td><td>'+$scope.timeCalculate(pos.data.parkedTime)+'</td></tr><tr><td>'+translate("trip_distance")+'</td>'
      +'<td>'+pos.data.tripDistance+'</td></tr></table></div>';
      
      var infoWindow = new google.maps.InfoWindow({content: contentString});
      google.maps.event.addListener($scope.markerend, "click", function(e){
        infoWindow.open($scope.map, $scope.markerend);  
      });
      
      (function(marker, data, contentString) {
        google.maps.event.addListener(marker, "click", function(e) {
          infoWindow.open($scope.map, marker);
        }); 
      })($scope.markerend, pos.data);
    });
  }
  $scope.tempadd="";

  function dateFormat(date) { return $filter('date')(date, "dd-MM-yyyy HH:mm:ss");}

  $scope.printadd = function( a, c, d, marker, map, data) {
    var posval='';
    var b=0;
    
    if(data.position=='S'){
      posval = 'Idle Time';
      b=data.idleTime;

    }else if(data.position=='P'){
      posval = 'Parked Time';
      b=data.parkedTime;
    }
    
    var contentString = '<h3 class="infoh3">'+$scope.vehiLabel+' '+translate("Details")+' ('+$scope.shortVehiId+') </h3><div class="nearbyTable02">'
    +'<div><table cellpadding="0" cellspacing="0"><tbody>'
    +'<tr><td>'+translate("location")+'</td><td>'+d+'</td></tr>'
    +'<tr><td>'+translate("Last_seen")+' </td><td>'+a+'</td></tr>'
    +'<tr><td>'+posval+'</td><td>'+$scope.timeCalculate(b)+'</td></tr>'
    +'<tr><td>'+translate("trip_distance")+' </td><td>'+c+'</td></tr>'
    +'</table></div>';
    var infoWindow = new google.maps.InfoWindow({content: contentString});
    ginfowindow.push(infoWindow);
    google.maps.event.addListener(marker, "click", function(e){
      for(j=0;j<ginfowindow.length;j++){
        ginfowindow[j].close();
      }
      infoWindow.open(map, marker); 
    });
    // (function(marker, data, contentString) {
    //   google.maps.event.addListener(marker, "click", function(e) {
    //    for(j=0;j<ginfowindow.length;j++){

    //    ginfowindow[j].close();
    //  }
    //  infoWindow.open(map, marker);
    //   });  
    // })(marker, data);
  }

  $scope.imgsrc = function(img){
   return img;
 }

 $scope.infoBox = function(map, marker, data) {

  if( data.address==null || data.address==undefined || data.address==" " || data.address=="_" ) {

    var t  =  $scope.getLocation(data.latitude, data.longitude, function( count ) {

     $scope.printadd(dateFormat(data.date), data.tripDistance, count, marker, map, data); 

   });

  } else {

   $scope.printadd(dateFormat(data.date), data.tripDistance, data.address, marker, map, data); 

 }

}


$scope.trimComma = function(textVal){

  var strValues;
    //console.log(textVal);

    if(textVal!=undefined){
      var splitValue = textVal.split(/[,]+/);
      var strLen=splitValue.length;

      switch(strLen){
        case 0:
        strValues='No Data';
        break;
        case 1:
        strValues=splitValue[0];
        break;
        case 2:
        strValues=splitValue[0]+','+splitValue[1];
        break;
        default:
        strValues=splitValue[0]+','+splitValue[1];
        break;
      }
    }else{
      strValues='No Address';
    }

    return  strValues;
  }

  $scope.hideMarkerVal = true;
  $scope.hideMarker = {
    value1 : true,
    value2 : 'YES'
  }

  $scope.addRemoveMarkers=function(val) {

   if (val == 'YES') {
     $scope.hideMarkerVal = false;
   } else {
     $scope.hideMarkerVal = true;
   }
 }
 $scope.addRemoveArrows = function(getStatus){

  if(getStatus == 'YES') {
   if($scope.mapsHist==0){
     gArrows=gArrowMarker;
     for(key in gArrows) {
      gArrows[key].setMap($scope.map);
    }
    gArrows = {};
  }else if($scope.mapsHist==1){
    arrows=arrowMarker;
    for(key in arrows) {
     $scope.map_osm.addLayer(arrows[key]);
   }
   arrows = {};
 }

} else if (getStatus == 'NO') {

  arrowclear();
}
}

function arrowclear(){
  $scope.hideArrows.value1='NO';
  if($scope.mapsHist==0){
   gArrows=gArrowMarker;
   for(key in gArrows) {
    gArrows[key].setMap(null);
  }
  gArrows = {};
}else if($scope.mapsHist==1){
  arrows=arrowMarker;
  for(key in arrows) {
   $scope.map_osm.removeLayer(arrows[key]);

 }
 arrows = {};
}

}

var intervalPoly;
var timeInterval;

$scope.polylineCheck  = true;
$scope.markerPark     = [];

function myStopFunction() {
  clearInterval(intervalPoly);
}

function markerClear(){

  for(var i=0; i<gmarkers.length; i++){
    gmarkers[i].setMap(null);
  }
  if($scope.polylineLoad)
    if($scope.polylineLoad.length >0)
      for (var i = $scope.polylineLoad[0].length - 1; i >= 0; i--) {
       $scope.polylineLoad[0][i].setMap(null);
     }
   }


   var pcount        =  0;
   var lenCount2     =  0;
   $scope.timeDelay  =  500;
   $scope.speedVal   =  0;
   $scope.pasVal     =  0;


   function googleMarkerHistory(){

    if($scope.hisloc.vehicleLocations.length!= pcount+1){
           // setting Fuel values
           if(!pcount){
             //console.log(pcount);
             $("#graphsId").show();
             if($scope.vehiclFuel==true){
              if($scope.noOfTank==1){
                document.getElementById("graphsId").style.width = "360px";
              }
              else {
               if($scope.noOfTank==2){
                 document.getElementById("graphsId").style.width = "550px";
               }else if($scope.noOfTank==3){
                 document.getElementById("graphsId").style.width = "630px";
               }else if($scope.noOfTank==4){
                 document.getElementById("graphsId").style.width = "785px";
               }else if($scope.noOfTank==5){
                 document.getElementById("graphsId").style.width = "940px";
               }
             }
           } else {
            document.getElementById("graphsId").style.width = "190px";
          }
        }
          //console.log($scope.hisloc.vehicleLocations[pcount]);
          $('#graphsId #speed').text($scope.hisloc.vehicleLocations[pcount].speed);
          total     =  parseInt($scope.hisloc.vehicleLocations[pcount].speed);
          if($scope.noOfTank==1){
            //tankSize  =  parseInt($scope.hisloc.vehicleLocations[pcount].tankSize);
            fuelLtr   =  parseInt($scope.hisloc.vehicleLocations[pcount].fuelLitre);
          }else{
            var mulFuelLitres = $scope.hisloc.vehicleLocations[pcount].fuelLitres;
            if(mulFuelLitres!="0"){
              $scope.fuelLitreArr = mulFuelLitres.split(":");
            }else{
              $scope.fuelLitreArr = "0:0:0";
            } 
          }
          setFuelAndSpeed();
          //end fuel and speed values

          var latlngs = new google.maps.LatLng($scope.hisloc.vehicleLocations[pcount+1].latitude,$scope.hisloc.vehicleLocations[pcount+1].longitude);
       // if(($scope.hisloc.vehicleLocations[pcount].latitude+''+$scope.hisloc.vehicleLocations[pcount].longitude)!=($scope.hisloc.vehicleLocations[pcount+1].latitude+''+$scope.hisloc.vehicleLocations[pcount+1].latitude)){
         $scope.rotationsd = getBearing($scope.hisloc.vehicleLocations[pcount].latitude,$scope.hisloc.vehicleLocations[pcount].longitude,$scope.hisloc.vehicleLocations[pcount+1].latitude,$scope.hisloc.vehicleLocations[pcount+1].longitude);                

         if($scope.vehiLabel == "Asset") {

          var  icon = {scaledSize: new google.maps.Size(30, 30),url:"assets/imgs/asMarker.png",labelOrigin:  new google.maps.Point(25,40)} ;
          $scope.markerheads.setIcon(icon);

        } else {
                          //console.log(vehicIcon);
                          $scope.markerheads.setIcon({
                               //path:vehicIcon[0],
                               //scale:vehicIcon[1],
                               url : ( $scope.trvShow == 'true' )? vamoservice.trvIcon($scope.hisloc.vehicleLocations[pcount]) : vamoservice.iconURL($scope.hisloc.vehicleLocations[pcount]),
                               strokeWeight: 1,
                               fillColor:$scope.polylinearr[pcount+1],
                               fillOpacity: 1,
                               //anchor:vehicIcon[2],
                               //rotation:$scope.rotationsd,
                               //origin: new google.maps.Point(0,0), // origin
                               anchor: new google.maps.Point(20, 40),
                               scaledSize: new google.maps.Size(40, 40), // scaled size

                             });

                        }

                        if(pcount==0){
                          $scope.markerheads.setMap($scope.map);
                          $scope.markerheads.setPosition(latlngs);
                        }
    //              var result=[$scope.hisloc.vehicleLocations[0].latitude,$scope.hisloc.vehicleLocations[0].longitude];
    // //alert(result[0]);
    // var position = [$scope.hisloc.vehicleLocations[30].latitude,$scope.hisloc.vehicleLocations[30].longitude];

    var result=[$scope.hisloc.vehicleLocations[pcount].latitude,$scope.hisloc.vehicleLocations[pcount].longitude];
    //alert(result[0]);
    var position = [$scope.hisloc.vehicleLocations[pcount+1].latitude,$scope.hisloc.vehicleLocations[pcount+1].longitude];
    
    
    
    var delay = 5; //milliseconds
    var i = 0;
    var deltaLat;
    var deltaLng;
    if(($scope.timeDelay==180||$scope.timeDelay==150)&&pcount>0){
      $scope.markerheads.setPosition(latlngs);
    }else if(pcount>0){
        //alert(pcount);
        transition(result);
      }

      function transition(result){
        i = 1;
        deltaLat = (parseFloat(result[0]) - parseFloat(position[0]))/gnumDeltas;
        deltaLng = (parseFloat(result[1]) - parseFloat(position[1]))/gnumDeltas;
        moveMarker();
      }

      function moveMarker(){
        position[0]=parseFloat(result[0])+parseFloat(deltaLat);
        position[1] =parseFloat(result[1])+ parseFloat(deltaLng);
        var moveMarkerlatlng = new google.maps.LatLng(position[0], position[1]);
        $scope.markerheads.setPosition(moveMarkerlatlng);
        if(i!=gnumDeltas){
          i++;
          if(!$scope.movemarker){
            setTimeout(moveMarker, delay);
          }
        }
      }


      if($scope.vehiclFuel==true){
        var contenttString = '<div style="padding:2px; padding-top:3px; width:175px;">'
        +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate("LocTime")+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+dateFormat($scope.hisloc.vehicleLocations[pcount+1].date)+'</span> </div>'
        +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate("Speed")+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+ $scope.hisloc.vehicleLocations[pcount+1].speed+'</span><span style="font-size:10px;padding-left:10px;">'+translate("kmph")+'</span></div>'
        +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate("DistCov")+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+$scope.hisloc.vehicleLocations[pcount+1].distanceCovered+'</span><span style="font-size:10px;padding-left:10px;">'+translate("kms")+'</span></div>'
        +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate("Fuel(Ltrs)")+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+(($scope.noOfTank > 1)? $scope.splitfuel($scope.hisloc.vehicleLocations[pcount+1].fuelLitres) : $scope.hisloc.vehicleLocations[pcount+1].fuelLitre)+'</span><span style="font-size:10px;padding-left:10px;">'+translate("ltrs")+'</span></div>'
        +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate("odokms")+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+$scope.hisloc.vehicleLocations[pcount+1].odoDistance+'</span><span style="font-size:10px;padding-left:10px;">'+translate("kms")+'</span></div>'
        +'<div style="text-align:center;padding-top:3px;"><span style="font-size:10px;width:100px;">'+$scope.trimComma($scope.hisloc.vehicleLocations[pcount+1].address)+'</span></div>'
      // +'<div style="overflow-wrap: break-word; border-top: 1px solid #eee">'+data.address+'</div>'
      +'</div>';}

      if($scope.vehiclFuel==false){
        var contenttString = '<div style="padding:2px; padding-top:3px; width:175px;">'
        +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate("LocTime")+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+dateFormat($scope.hisloc.vehicleLocations[pcount+1].date)+'</span> </div>'
        +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate("Speed")+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+ $scope.hisloc.vehicleLocations[pcount+1].speed+'</span><span style="font-size:10px;padding-left:10px;">'+translate("kmph")+'</span></div>'
        +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate("DistCov")+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+$scope.hisloc.vehicleLocations[pcount+1].distanceCovered+'</span><span style="font-size:10px;padding-left:10px;">'+translate("kms")+'</span></div>'
        +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate("odokms")+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+$scope.hisloc.vehicleLocations[pcount+1].odoDistance+'</span><span style="font-size:10px;padding-left:10px;">'+translate("kms")+'</span></div>'
        +'<div style="text-align:center;padding-top:3px;"><span style="font-size:10px;width:100px;">'+$scope.trimComma($scope.hisloc.vehicleLocations[pcount+1].address)+'</span></div>'
      // +'<div style="overflow-wrap: break-word; border-top: 1px solid #eee">'+data.address+'</div>'
      +'</div>';}


          //console.log(contenttString);

          $scope.infosWindow.setContent(contenttString);
          $scope.infosWindow.setPosition(latlngs);

          if(pcount==0){
            $scope.infosWindow.open($scope.map,$scope.markerheads);
              //$scope.infosWindow.close();
            }   

                   // console.log(latlngs);

                //for google map center changes
               //$scope.map.panTo(latlngs);
               var googleCurrZoom = $scope.map.getZoom();
               var interval=25-googleCurrZoom;
              //alert(googleCurrZoom);
              if(pcount%interval==0){
                $scope.map.panTo(latlngs);
              }
    // }
    pcount++;
    
  } else {

   if(lenCount2 == 0 ){

    if($scope.hisloc.vehicleLocations.length==1){

      var latlngs = new google.maps.LatLng($scope.hisloc.vehicleLocations[0].latitude,$scope.hisloc.vehicleLocations[0].longitude);
          //  if(($scope.hisloc.vehicleLocations[pcount].latitude+''+$scope.hisloc.vehicleLocations[pcount].longitude)!=($scope.hisloc.vehicleLocations[pcount+1].latitude+''+$scope.hisloc.vehicleLocations[pcount+1].latitude)){
            //  $scope.rotationsd = getBearing($scope.hisloc.vehicleLocations[pcount].latitude,$scope.hisloc.vehicleLocations[pcount].longitude,$scope.hisloc.vehicleLocations[pcount+1].latitude,$scope.hisloc.vehicleLocations[pcount+1].longitude);                

            if($scope.vehiLabel == "Asset"){

              var  icon = {scaledSize: new google.maps.Size(30, 30),url:"assets/imgs/asMarker.png",labelOrigin:  new google.maps.Point(25,40)} ;
              $scope.markerheads.setIcon(icon);

            } else {

              $scope.markerheads.setIcon({
                                    //path:vehicIcon[0],
                                    url : ( $scope.trvShow == 'true' )? vamoservice.trvIcon($scope.hisloc.vehicleLocations[$scope.lineCount_osm]) : vamoservice.iconURL($scope.hisloc.vehicleLocations[$scope.lineCount_osm]),
                                    scale:40,
                                    strokeWeight: 1,
                                    fillColor:$scope.polylinearr[0],
                                    fillOpacity: 1,
                                    anchor: new google.maps.Point(20, 40),
                                    scaledSize: new google.maps.Size(40, 40), // scaled size
                                    //anchor:vehicIcon[2],
                                    //rotation:$scope.rotationsd,
                                  });

            }      

            $scope.markerheads.setMap($scope.map);
            $scope.markerheads.setPosition(latlngs);

            if($scope.vehiclFuel==true){
              var contenttString = '<div style="padding:2px; padding-top:3px; width:175px;">'
              +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate("LocTime")+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+dateFormat($scope.hisloc.vehicleLocations[0].date)+'</span> </div>'
              +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate("Speed")+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+ $scope.hisloc.vehicleLocations[0].speed+'</span><span style="font-size:10px;padding-left:10px;">'+translate("kmph")+'</span></div>'
              +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate("DistCov")+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+$scope.hisloc.vehicleLocations[0].distanceCovered+'</span><span style="font-size:10px;padding-left:10px;">'+translate("kms")+'</span></div>'
              +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate("Fuel(Ltrs)")+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+(($scope.noOfTank > 1)? $scope.splitfuel($scope.hisloc.vehicleLocations[0].fuelLitres) : $scope.hisloc.vehicleLocations[0].fuelLitre)+'</span><span style="font-size:10px;padding-left:10px;">'+translate("ltrs")+'</span></div>'
              +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate("odokms")+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+$scope.hisloc.vehicleLocations[0].odoDistance+'</span><span style="font-size:10px;padding-left:10px;">'+translate("kms")+'</span></div>'
              +'<div style="text-align:center;padding-top:3px;"><span style="font-size:10px;width:100px;">'+$scope.trimComma($scope.hisloc.vehicleLocations[0].address)+'</span></div>'
      // +'<div style="overflow-wrap: break-word; border-top: 1px solid #eee">'+data.address+'</div>'
      +'</div>';}
      if($scope.vehiclFuel==false){
        var contenttString = '<div style="padding:2px; padding-top:3px; width:175px;">'
        +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate("LocTime")+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+dateFormat($scope.hisloc.vehicleLocations[0].date)+'</span> </div>'
        +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate("Speed")+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+ $scope.hisloc.vehicleLocations[0].speed+'</span><span style="font-size:10px;padding-left:10px;">'+translate("kmph")+'</span></div>'
        +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate("DistCov")+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+$scope.hisloc.vehicleLocations[0].distanceCovered+'</span><span style="font-size:10px;padding-left:10px;">'+translate("kms")+'</span></div>'
        +'<div style="font-size=11px;border-bottom: 0.5px solid #f3eeeb;"><b class="_info_caption2" style="font-size:11px;padding-right:3px;color:#666463;">'+translate("odokms")+'</b> - <span style="font-size:10px;padding-left:2px;color:green;font-weight:bold;">'+$scope.hisloc.vehicleLocations[0].odoDistance+'</span><span style="font-size:10px;padding-left:10px;">'+translate("kms")+'</span></div>'
        +'<div style="text-align:center;padding-top:3px;"><span style="font-size:10px;width:100px;">'+$scope.trimComma($scope.hisloc.vehicleLocations[0].address)+'</span></div>'
      // +'<div style="overflow-wrap: break-word; border-top: 1px solid #eee">'+data.address+'</div>'
      +'</div>';
    }

    $scope.infosWindow.setContent(contenttString);
    $scope.infosWindow.setPosition(latlngs);
    $scope.infosWindow.open($scope.map,$scope.markerheads);
                  //$scope.infosWindow.close();
                }

                lenCount2++;
              }

              if(pcount==$scope.hisloc.vehicleLocations.length-1){

                clearInterval(timeInterval);

                $('#playButton').prop('disabled',  true);
                $('#replayButton').prop('disabled',false);
                $('#stopButton').prop('disabled', true);
                $('#pauseButton').prop('disabled', true);
                $("#graphsId").hide();
                $scope.speedVal=1; 
              }

            } 
          }

          $scope.polylineCheck = {
            value1: 'YES',
            value2 : 'NO'
          }

          $scope.hideAllDetail = function(val) {

            if(val == 'NO') {

              $scope.hideAllDetailVal = true;
              $scope.hideMe           = true;
              $scope.hideMarkerVal    = true;
        /*  $('.hideClass').hide();
            $('#pauseButton').hide();
            $('#playButton').hide();      
            $('#stopButton').hide();   
            $('#replayButton').hide();*/

            $('#playButton').prop('disabled', true);
            $('#replayButton').prop('disabled', true);
            $('#stopButton').prop('disabled', true);
            $('#pauseButton').prop('disabled', true);

          } else if(val == 'YES') {

            $scope.hideAllDetailVal = false;
            $scope.hideMe = false;
            $scope.hideMarkerVal = false;

         /* $('.hideClass').show();
            $('#pauseButton').show();
         // $('#playButton').show();      
            $('#stopButton').show();   
            $('#replayButton').show(); */

            $('#playButton').prop('disabled', false);
            $('#replayButton').prop('disabled', false);
            $('#stopButton').prop('disabled', false);
            $('#pauseButton').prop('disabled', false);
          } 
        }




        $scope.polylineCtrl   = function(){

          startLoading();

  //console.log('polylineCtrl...');


  if($scope.hisloc.vehicleLocations!=null) {

    if($scope.initGoogVal==0){
     $scope.initGoogVal=1;
      // console.log('initGoogVal==0...');

      if($scope.map==undefined){

           // console.log('google init...');

           if($scope.hisloc.vehicleLocations.length != 0) {
                    //console.log(parseInt(locs.tripDistance));
                    if(parseInt($scope.hisloc.tripDistance) > 200) {
                      $scope.zoomCtrlVal     =  true;
                      $scope.hisloc.zoomLevel        =  12;
                      $scope.ZoomDisableBtn  =  true;
                      $scope.scrollsWheel    =  false;
                       // console.log(locs.tripDistance+"Active no Zoom");
                     } else{
                      $scope.zoomCtrlVal    = false;
                      $scope.ZoomDisableBtn = false;
                      $scope.scrollsWheel   = true;
                    }

                   /* var myOptions = {
                        zoom: Number(locs.zoomLevel),zoomControlOptions: { position: google.maps.ControlPosition.LEFT_TOP}, 
                        center: new google.maps.LatLng(data.vehicleLocations[0].latitude, data.vehicleLocations[0].longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                      //styles: [{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}]
                    }; */

                    var myOptions = {
                      zoom: 13,/*Number($scope.hisloc.zoomLevel)*/
                        // zoomControl: $scope.scrollsWheel,
                        zoomControl: true,
                        zoomControlOptions: { position: google.maps.ControlPosition.LEFT_TOP}, 
                        center: new google.maps.LatLng($scope.hisloc.vehicleLocations[0].latitude, $scope.hisloc.vehicleLocations[0].longitude),
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                        // disableDoubleClickZoom: $scope.zoomCtrlVal,
                        // scrollwheel: $scope.scrollsWheel,
                        // disableDefaultUI: $scope.ZoomDisableBtn,
                     // styles: [{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#f7f1df"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"color":"#d0e3b4"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry","stylers":[{"color":"#fbd3da"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#bde6ab"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffe15f"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#efd151"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"color":"black"}]},{"featureType":"transit.station.airport","elementType":"geometry.fill","stylers":[{"color":"#cfb2db"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#a2daf2"}]}]
                   };

                   $scope.map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
                   $scope.map.setOptions({maxZoom:/*For example*/25});
              //  }else{
              //    alert("Data Not Found!..");
              //    console.log('vehicleLocations not found!...'+data);
              //  }


              google.maps.event.addListener($scope.map, 'click', function(event) {
                $scope.clickedLatlng = event.latLng.lat() +','+ event.latLng.lng();
                $('#latinput').val($scope.clickedLatlng);
              });


              $scope.path=[];
              $scope.polylinearr=[];

              for(var i=0;i<$scope.hisloc.vehicleLocations.length;i++){

                $scope.path.push(new google.maps.LatLng($scope.hisloc.vehicleLocations[i].latitude, $scope.hisloc.vehicleLocations[i].longitude));

                if($scope.hisloc.vehicleLocations[i].isOverSpeed=='Y'){
                  var pscolorval = '#ff0000';
                } else {
                  var pscolorval = '#6dd538';
                }

                $scope.polylinearr.push(pscolorval);
              }

               // console.log($scope.path.length);

             //  var myVar = setInterval(myTimer, 20000);

             if($scope.getValue != undefined) {


              $scope.intervalValue = setInterval($scope.getValueCheck($scope.getValue), 25000);
            }

            $scope.pointDistances  =  [];
            var sphericalLib       =  google.maps.geometry.spherical;
            var pointZero          =  $scope.path[0];
                //var wholeDist          =  sphericalLib.computeDistanceBetween(pointZero, $scope.path[$scope.path.length - 1]);

                $('#replaybutton').removeAttr('disabled');

            //}else{
            //  $('.error').show();
            //  $('#lastseen').html('<strong>From Date & time :</strong> -');
            //  $('#lstseendate').html('<strong>To  &nbsp; &nbsp; Date & time :</strong> -');
          // }
        //  }

      }


      var url = GLOBAL.DOMAIN_NAME+'/getGeoFenceView?vehicleId='+$scope.trackVehID;

      $scope.createGeofence(url);
      stopLoading();

           //}

          // $scope.SiteCheckbox = {
          //    value1 : true,
          //    value2 : 'YES'
          // }

        } else {
         $scope.path=[];
         $scope.polylinearr=[];
         for(var i=0;i<$scope.hisloc.vehicleLocations.length;i++){

          $scope.path.push(new google.maps.LatLng($scope.hisloc.vehicleLocations[i].latitude, $scope.hisloc.vehicleLocations[i].longitude));

          if($scope.hisloc.vehicleLocations[i].isOverSpeed=='Y'){
            var pscolorval = '#ff0000';
          } else {
            var pscolorval = '#6dd538';
          }

          $scope.polylinearr.push(pscolorval);
        }
      }
   //arrowmarker added

   for(var i=0;i<$scope.hisloc.vehicleLocations.length;i++){
    if(i!=0){
      var response=$scope.hisloc.vehicleLocations;
      var glatlnd=middlePoint(response[i-1].latitude ,response[i-1].longitude ,response[i].latitude ,response[i].longitude);
      markerLatlng = new google.maps.LatLng(glatlnd[0],glatlnd[1]);
      var markerLatlng = new google.maps.LatLng(response[i].latitude,response[i].longitude);
      var dx = response[i].latitude - response[i-1].latitude;
      var dy = response[i].longitude - response[i-1].longitude;
      rotationAngle = Math.atan2(dy, dx)* 180 / Math.PI;
      latLngdiff=calcCrow(response[i-1].latitude ,response[i-1].longitude ,response[i].latitude ,response[i].longitude  );
      distance=parseFloat(latLngdiff)+parseFloat(distance);
                      //alert(distance.toFixed(2));
                      if(distance>gDistanceInterval){
                        distance=0;
                        var beachMarker = new google.maps.Marker({
                          position: markerLatlng,
                            icon: {scaledSize: new google.maps.Size(80, 80),/* url: image,
                            rotation:180*/ path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,scale: 2,rotation: rotationAngle/*, anchor: new google.maps.Point(38,40)*/}
                          });
                        //beachMarker.setMap(null);

                        gArrowMarker[i]=beachMarker;
                        if($scope.hideArrows.value1=='YES'){
                         beachMarker.setMap($scope.map);
                       }

                     }
                   }

                 }
            //markermarker end
    //google map zoom start 
    google.maps.event.addListener($scope.map,'zoom_changed',function(){
      gArrows=gArrowMarker;
      for(key in gArrows) {
        gArrows[key].setMap(null);
      }
      gArrows = {};
      gArrowMarker={};
      if($scope.map.getZoom() > gzoom) {
                      //alert('You just zoomed in.');
                      if($scope.map.getZoom()==13){
                        gDistanceInterval=2;

                      }
                      else{
                        gDistanceInterval=gDistanceInterval/2;

                      }
                    }else if($scope.map.getZoom() < gzoom) {
                      //alert('You just zoomed out.');
                      if($scope.map.getZoom()==13){
                        gDistanceInterval=2;
                      }
                      else{

                       gDistanceInterval=gDistanceInterval*2;


                     }
                   }
                   gzoom = $scope.map.getZoom();

                   
                   gLatLngUrlData=$scope.hisloc.vehicleLocations;
                   for (var i = 0; i < gLatLngUrlData.length; i++) 
                   {
                     if(i!=0){
                       var dx = gLatLngUrlData[i].latitude - gLatLngUrlData[i-1].latitude;
                       var dy = gLatLngUrlData[i].longitude - gLatLngUrlData[i-1].longitude;
                       rotationAngle = Math.atan2(dy, dx)* 180 / Math.PI;
                     }

                     var markerLatlng = new google.maps.LatLng(gLatLngUrlData[i].latitude,gLatLngUrlData[i].longitude);
                     if(i!=0&&gzoom>=7){
                      latLngdiff=calcCrow(gLatLngUrlData[i-1].latitude ,gLatLngUrlData[i-1].longitude ,gLatLngUrlData[i].latitude ,gLatLngUrlData[i].longitude  );
                      distance=parseFloat(latLngdiff)+parseFloat(distance);
                      var glatlnd=middlePoint(gLatLngUrlData[i-1].latitude ,gLatLngUrlData[i-1].longitude ,gLatLngUrlData[i].latitude ,gLatLngUrlData[i].longitude);
                      var gstartPos = new L.LatLng(gLatLngUrlData[i-1].latitude ,gLatLngUrlData[i-1].longitude );
                      var gendPos =new L.LatLng(gLatLngUrlData[i].latitude ,gLatLngUrlData[i].longitude);
                      markerLatlng = new google.maps.LatLng(glatlnd[0],glatlnd[1]);
                      var beachMarker = new google.maps.Marker({
                        position: markerLatlng,
                                      icon: {scaledSize: new google.maps.Size(80, 80),/* url: image,
                                      rotation:180*/ path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,scale: 2,rotation: rotationAngle/*, anchor: new google.maps.Point(38,40)*/}
                                    });
                                //alert(distance.toFixed(2));
                                if($scope.map.getZoom()>=18){
                                  var miltiMarkers = new google.maps.Marker({
                                    position: markerLatlng,
                                                    icon: {scaledSize: new google.maps.Size(80, 80),/* url: image,
                                                    rotation:180*/ path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,scale: 1.5,rotation: rotationAngle/*, anchor: new google.maps.Point(38,40)*/}
                                                  });
                                      //alert('hello');
                                      if((latLngdiff/gDistanceInterval)>=1){
                                       multipleDistance=latLngdiff/(gDistanceInterval*4);
                                       var numDeltas =  Math.floor(multipleDistance/2);
                                         //alert(numDeltas)
                                         var j = 0;
                                         var deltaLat;
                                         var deltaLng;
                                         position = gstartPos;
                                         if(numDeltas>=2){
                                          miltiMarker(gendPos);
                                        }else{
                                          if((latLngdiff/gDistanceInterval)>=1){
                                            miltiMarkers.setPosition(markerLatlng);
                                            gArrowMarker[i]=miltiMarkers;
                                            if($scope.hideArrows.value1=='YES'){
                                              miltiMarkers.setMap($scope.map);
                                            }
                                          }
                                        }

                                        function miltiMarker(result){
                                         j = 1;
                                         deltaLat = (result.lat - position.lat)/numDeltas;
                                         deltaLng = (result.lng - position.lng)/numDeltas;
                                         setMarker();
                                       }

                                       function setMarker(){
                                         position.lat += deltaLat;
                                         position.lng += deltaLng;
                                         markerLatlng = new google.maps.LatLng(position.lat, position.lng);
                                                   //marker.setPosition(latlng);
                                                   //alert(position);
                                                   miltiMarkers.setPosition(markerLatlng);
                                                   gArrowMarker[j+'many'+i]=miltiMarkers;
                                                   if($scope.hideArrows.value1=='YES'){
                                                    miltiMarkers.setMap($scope.map);
                                                  }
                                                  if(j!=numDeltas){
                                                   j++;
                                                   setMarker();
                                                 }
                                               }


                                             }

                                           }else{
                                            if(distance>gDistanceInterval){
                                              distance=0;

                                                  //beachMarker.setMap(null);
                                                  beachMarker.setPosition(markerLatlng);
                                                  gArrowMarker[i]=beachMarker;
                                                  if($scope.hideArrows.value1=='YES'){
                                                    beachMarker.setMap($scope.map);
                                                  }
                                                  
                                                }
                                              }
                                            }

                                          }

                                        });
              //google map zoom end

  //console.log('vehicle locations...');
  myStopFunction();


  markerClear();
  gmarkers=[];
  ginfowindow=[];
  var j =0;

  $('.radioBut').hide(0).delay(5000).show(100);

  vehicIcon=vehiclesChange(VehiType);

      //    console.log('google mapsss....');

      if($scope.markerheads){
                 // $scope.infosWindows.close();
                 $scope.markerstart.setMap(null);
                 $scope.markerend.setMap(null);
                 $scope.infosWindows=null;
                 $scope.markerheads.setMap(null);
                 $scope.markerheads=null;
               }

               var tempFlag=false;

               for(var k=0;k<$scope.hisloc.vehicleLocations.length;k++){
                if($scope.hisloc.vehicleLocations[k].position =='M' && tempFlag==false){
                  var firstval = k;
                  tempFlag=true;
                }
              }

              if(firstval==undefined){
                firstval=0;
              }

              var lastval = $scope.hisloc.vehicleLocations.length-1;
              $scope.addMarkerstart({ lat: $scope.hisloc.vehicleLocations[firstval].latitude, lng: $scope.hisloc.vehicleLocations[firstval].longitude , data: $scope.hisloc.vehicleLocations[firstval], path:$scope.path[firstval]});
              $scope.addMarkerend({ lat: $scope.hisloc.vehicleLocations[lastval].latitude, lng: $scope.hisloc.vehicleLocations[lastval].longitude, data: $scope.hisloc.vehicleLocations[lastval], path:$scope.path[lastval] });
              
              $scope.polyline = new google.maps.Polyline({
                map: $scope.map,
                path: $scope.path,
                strokeColor: '#068b03',
                strokeOpacity: 0.7,
                strokeWeight: 3,
                /*icons: [{
                        icon: lineSymbol,
                       offset: '100%'
                     }],*/
                     clickable: true
                   });


              $scope.markerheads = new google.maps.Marker();
              $scope.infosWindow = new google.maps.InfoWindow({maxWidth:180});
             // console.log('infowindow..');

             $scope.markerheads.addListener('click', function() {
               $scope.infosWindow.open($scope.map,$scope.markerheads);
             });

             $scope.markerValue=1;

             pcount       = 0;
             lenCount2    = 0;
             timeInterval = setInterval(function(){ googleMarkerHistory() }, $scope.timeDelay);


             var latLngBounds = new google.maps.LatLngBounds();
             for(var i = 0; i < $scope.path.length; i++) {
              latLngBounds.extend($scope.path[i]);
              if($scope.hisloc.vehicleLocations[i]){
               if($scope.hisloc.vehicleLocations[i].position!=undefined){
                   // if($scope.hisloc.vehicleLocations[i].position=='P' || $scope.hisloc.vehicleLocations[i].position=='S' || $scope.hisloc.vehicleLocations[i].insideGeoFence=='Y' ){
                     if($scope.hisloc.vehicleLocations[i].position=='P' || $scope.hisloc.vehicleLocations[i].position=='S' ){ 
                      $scope.addMarker({ lat: $scope.hisloc.vehicleLocations[i].latitude, lng: $scope.hisloc.vehicleLocations[i].longitude , data: $scope.hisloc.vehicleLocations[i], path:$scope.path[i]});
                      $scope.infoBox($scope.map, gmarkers[j], $scope.hisloc.vehicleLocations[i]);
                      //$scope.markerPark.push($scope.marker);
                      j++;
                    }
                    
                  }
                }
              }

              var url = GLOBAL.DOMAIN_NAME+'/getGeoFenceView?vehicleId='+$scope.trackVehID;

              $scope.createGeofence(url);

              stopLoading();  


            }else if($scope.plotVal==1){

   // console.log('plotVal==1');


   $scope.path=[];
   for(var i=0;i<$scope.hisloc.vehicleLocations.length;i++) {

    $scope.path.push(new google.maps.LatLng($scope.hisloc.vehicleLocations[i].latitude, $scope.hisloc.vehicleLocations[i].longitude));

    if($scope.hisloc.vehicleLocations[i].isOverSpeed=='Y'){
      var pscolorval = '#ff0000';
    } else {
      var pscolorval = '#6dd538';
    }

    $scope.polylinearr.push(pscolorval);
  }
  $scope.plotVal=0; 
   // console.log('vehicle locations...');

   myStopFunction();
   markerClear();
   gmarkers=[];
   ginfowindow=[];

   var j =0;
   $('.radioBut').hide(0).delay(5000).show(100);

   vehicIcon=vehiclesChange(VehiType);

  //console.log('google mapsss....');

  if($scope.markerheads){
             //$scope.infosWindows.close();
             $scope.markerstart.setMap(null);
             $scope.markerend.setMap(null);
             $scope.infosWindows=null;
             $scope.markerheads.setMap(null);
             $scope.markerheads=null;
           }

           var tempFlag=false;

           for(var k=0;k<$scope.hisloc.vehicleLocations.length;k++){
            if($scope.hisloc.vehicleLocations[k].position =='M' && tempFlag==false){
              var firstval = k;
              tempFlag=true;
            }
          }

          if(firstval==undefined){
            firstval=0;
          }

          var lastval = $scope.hisloc.vehicleLocations.length-1;
          $scope.addMarkerstart({ lat: $scope.hisloc.vehicleLocations[firstval].latitude, lng: $scope.hisloc.vehicleLocations[firstval].longitude , data: $scope.hisloc.vehicleLocations[firstval], path:$scope.path[firstval]});
          $scope.addMarkerend({ lat: $scope.hisloc.vehicleLocations[lastval].latitude, lng: $scope.hisloc.vehicleLocations[lastval].longitude, data: $scope.hisloc.vehicleLocations[lastval], path:$scope.path[lastval] });


          $scope.polyline = new google.maps.Polyline({
            map: $scope.map,
            path: $scope.path,
            strokeColor: '#068b03',
            strokeOpacity: 0.7,
            strokeWeight: 3,
                /*icons: [{
                        icon: lineSymbol,
                       offset: '100%'
                     }],*/
                     clickable: true
                   });

          $scope.markerheads = new google.maps.Marker();
          $scope.infosWindow = new google.maps.InfoWindow({maxWidth:180});
          //console.log('infowindow..');

          $scope.markerheads.addListener('click', function() {
            $scope.infosWindow.open($scope.map,$scope.markerheads);
          });

          $scope.markerValue = 1;
          pcount             = 0;
          lenCount2          = 0;
          clearInterval(timeInterval);
          timeInterval       = setInterval(function(){ googleMarkerHistory() }, $scope.timeDelay);

          var latLngBounds = new google.maps.LatLngBounds();

          for(var i = 0; i < $scope.path.length; i++) {
            latLngBounds.extend($scope.path[i]);
            if($scope.hisloc.vehicleLocations[i]){
             if($scope.hisloc.vehicleLocations[i].position!=undefined){
                   // if($scope.hisloc.vehicleLocations[i].position=='P' || $scope.hisloc.vehicleLocations[i].position=='S' || $scope.hisloc.vehicleLocations[i].insideGeoFence=='Y' ){

                     if($scope.hisloc.vehicleLocations[i].position=='P' || $scope.hisloc.vehicleLocations[i].position=='S' ){

                      $scope.addMarker({ lat: $scope.hisloc.vehicleLocations[i].latitude, lng: $scope.hisloc.vehicleLocations[i].longitude , data: $scope.hisloc.vehicleLocations[i], path:$scope.path[i]});
                      $scope.infoBox($scope.map, gmarkers[j], $scope.hisloc.vehicleLocations[i]);
                        //$scope.markerPark.push($scope.marker);
                        j++;
                      }

                    }
                  }
                }

                var url = GLOBAL.DOMAIN_NAME+'/getGeoFenceView?vehicleId='+$scope.trackVehID;

                $scope.createGeofence(url);

                stopLoading();  

              } else {

        //console.log('pol lin else...');
       
        timeInterval = setInterval(function(){ googleMarkerHistory() }, $scope.timeDelay);

        stopLoading(); 

        var url = GLOBAL.DOMAIN_NAME+'/getGeoFenceView?vehicleId='+$scope.trackVehID;   
        $scope.createGeofence(url);
      }


    }
  }
  

  $scope.replays= function(){
    startLoading();
    $scope.movemarker=0;
    /*  $('#playButton').hide();
        $('#replayButton').show();
        $('#stopButton').show();  
        $('#pauseButton').show(); */

        $('#playButton').prop('disabled', true);
        $('#replayButton').prop('disabled', false);
        $('#stopButton').prop('disabled', false);
        $('#pauseButton').prop('disabled', false);

        if($scope.googleMap==true && $scope.osmMap==false){

         window.clearInterval(timeInterval);               
         pcount = 0; 
         timeInterval = setInterval(function(){ googleMarkerHistory() }, $scope.timeDelay);

       } else if($scope.osmMap==true && $scope.googleMap==false){

         clearInterval($scope.osmInterVal);
         $scope.lineCount_osm = 0;
         $scope.osmInterVal  = setInterval(function(){ osmPolyLine() },$scope.timeDelay);
       }
       stopLoading();

     }

     $scope.pausehis= function(){
       $scope.movemarker=1;
       $scope.pasVal=1; 

    /* $('#stopButton').hide(); 
       $('#pauseButton').hide();
       $('#replayButton').hide();
       $('#playButton').show(); */

       $('#playButton').prop('disabled', false);
       $('#replayButton').prop('disabled', false);
       $('#stopButton').prop('disabled', true);
       $('#pauseButton').prop('disabled', true);

       if($scope.googleMap==true && $scope.osmMap==false){

        window.clearInterval(timeInterval);

      } else if($scope.osmMap==true && $scope.googleMap==false){

        clearInterval($scope.osmInterVal);
      }

    }

    $scope.speedchange=function(){

      if($scope.speedVal!==1){
        $('#playButton').prop('disabled', true);
        $('#replayButton').prop('disabled', false);
        $('#stopButton').prop('disabled', false);
        $('#pauseButton').prop('disabled', false);

      /* $('#playButton').hide();
         $('#replayButton').show();
         $('#stopButton').show();  
         $('#pauseButton').show(); */
       }

       if($scope.pasVal==1){
      /* $('#playButton').hide();
         $('#replayButton').show();
         $('#stopButton').show();  
         $('#pauseButton').show(); */

         $('#playButton').prop('disabled', true);
         $('#replayButton').prop('disabled', false);
         $('#stopButton').prop('disabled', false);
         $('#pauseButton').prop('disabled', false);

         $scope.pasVal=0;
       }

       if($scope.googleMap==true && $scope.osmMap==false){

        window.clearInterval(timeInterval);
        timeInterval=setInterval(function(){ googleMarkerHistory() }, $scope.timeDelay );

      } else if($scope.osmMap==true && $scope.googleMap==false){

        clearInterval($scope.osmInterVal);
        $scope.osmInterVal  = setInterval(function(){ osmPolyLine() },$scope.timeDelay);
      }

    }

    $scope.playhis=function(){

    /*  $('#playButton').hide();
        $('#replayButton').show();
        $('#stopButton').show();  
        $('#pauseButton').show();  */

        $('#playButton').prop('disabled', true);
        $('#replayButton').prop('disabled', false);
        $('#stopButton').prop('disabled', false);
        $('#pauseButton').prop('disabled', false);

        if($scope.googleMap==true && $scope.osmMap==false){

          window.clearInterval(timeInterval);     
          timeInterval=setInterval(function(){ googleMarkerHistory() }, $scope.timeDelay );

        } else if($scope.osmMap==true && $scope.googleMap==false){

          clearInterval($scope.osmInterVal);
          $scope.osmInterVal  = setInterval(function(){ osmPolyLine() },$scope.timeDelay);
        }
      }

      $scope.stophis=function(){

    /*  $('#stopButton').hide(); 
        $('#pauseButton').hide();
        $('#replayButton').show();
        $('#playButton').show();  */

        $('#playButton').prop('disabled', true);
        $('#replayButton').prop('disabled', false);
        $('#stopButton').prop('disabled', true);
        $('#pauseButton').prop('disabled', true);

        if($scope.googleMap==true && $scope.osmMap==false){

          window.clearInterval(timeInterval);
        } else if($scope.osmMap==true && $scope.googleMap==false){

          clearInterval($scope.osmInterVal);
        }
      }

      $(document).ready(function(){
        $('#minmax1').click(function(){
          $('#contentreply').animate({
            height: 'toggle'
          },500);
        });
      });

      $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
      });

      $(function () {
        $('#dateFrom, #dateTo').datetimepicker({
          format:'DD-MM-YYYY',
          useCurrent:true,
          pickTime: false,
          maxDate: new Date,
          minDate: new Date(2015, 12, 1),
          defaultDate : new Date,
        });
        $('#timeFrom').datetimepicker({
          pickDate: false,
          useCurrent:true,
        });
        $('#timeTo').datetimepicker({
          useCurrent:true,
          pickDate: false
        });
      });

      $(document).ready(function(){
        $('#editAction').click(function(){
            // $('#contentmin').animate({
            //     height: 'toggle'
            // },500);
            console.log(' edit action ')
          });
      });
      $('.legendlist').hide()
      $(document).ready(function() {
        $('.viewList').click(function(){
          $('.legendlist').animate({
            height: 'toggle'
          })
        })
      });

      $('ul.tabs').each(function(){
  // For each set of tabs, we want to keep track of
  // which tab is active and its associated content
  var $active, $content, $links = $(this).find('a');

  // If the location.hash matches one of the links, use that as the active tab.
  // If no match is found, use the first link as the initial active tab.
  $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
  $active.addClass('current');

  $content = $($active[0].hash);

  // Hide the remaining content
  $links.not($active).each(function () {
    $(this.hash).hide();
  });

  // Bind the click event handler
  $(this).on('click', 'a', function(e){
    // Make the old tab inactive.
    $active.removeClass('current');
    $content.hide();

    // Update the variables with the new link and content
    $active = $(this);
    $content = $(this.hash);

    // Make the tab active.
    $active.addClass('current');
    $content.show();

    // Prevent the anchor's default click action
    e.preventDefault();
  });
});

  //This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
  function calcCrow(lat1, lon1, lat2, lon2) 
  {
      var R = 6371; // km
      var dLat = toRad(lat2-lat1);
      var dLon = toRad(lon2-lon1);
      var lat1 = toRad(lat1);
      var lat2 = toRad(lat2);

      var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
      var d = R * c;
      return d;
    }
    // Converts numeric degrees to radians
    function toRad(Value) 
    {
      return Value * Math.PI / 180;
    }   
      /*
 * Find midpoint between two coordinates points
 * Source : http://www.movable-type.co.uk/scripts/latlong.html
 */

//-- Define radius function
// if (typeof (Number.prototype.toRad) === "undefined") {
//     Number.prototype.toRad = function () {
//         return this * Math.PI / 180;
//     }
// }

//-- Define degrees function
if (typeof (Number.prototype.toDeg) === "undefined") {
  Number.prototype.toDeg = function () {
    return this * (180 / Math.PI);
  }
}

//-- Define middle point function
function middlePoint(lat1, lng1, lat2, lng2) {

    //-- Longitude difference
    var dLng =toRad(lng2 - lng1);

    //-- Convert to radians
    lat1 = toRad(lat1);
    lat2 = toRad(lat2);
    lng1 = toRad(lng1);

    var bX = Math.cos(lat2) * Math.cos(dLng);
    var bY = Math.cos(lat2) * Math.sin(dLng);
    var lat3 = Math.atan2(Math.sin(lat1) + Math.sin(lat2), Math.sqrt((Math.cos(lat1) + bX) * (Math.cos(lat1) + bX) + bY * bY));
    var lng3 = lng1 + Math.atan2(bY, Math.cos(lat1) + bX);

    //-- Return result
    return [lat3.toDeg(),lng3.toDeg() ];
  }
  function getSpeedandFuel(selVehiId){
    //$scope.locations.find(item => item.id === 2); 
    console.log($scope.locations);
    //alert('hello');
    var dataVal = $scope.locations[$scope.gIndex].vehicleLocations.find(data => data.vehicleId === selVehiId);
    //dataVal = $scope.locations[$scope.gIndex].vehicleLocations[$scope.selected];
    $('#graphsId #speed').text(dataVal.speed);
    $('#graphsId #fuel').text(dataVal.tankSize);
    tankSize  =  parseInt(dataVal.tankSize);
    fuelLtr   =  parseInt(dataVal.fuelLitre);
    total     =  parseInt(dataVal.speed);
    sensor = dataVal.noOfTank;
    $scope.noOfTank =  dataVal.noOfTank;
    $scope.tankSizeArr = dataVal.nTankSize.split(":");
    getFuelTank(dataVal.fuelLitres);
    if($scope.noOfTank==1){
     //alert('one')
     speedSize='100%';speedCenter='100%';startangle=-90;endangle=90;
   }
   else if($scope.noOfTank==2){
      //alert('two')
      speedSize='68%';speedCenter='78%';startangle=-150;endangle=150;
    }else {
      speedSize='60%';speedCenter='75%';startangle=-150;endangle=150;
    }

    $scope.vehiclFuel=graphChange(dataVal.fuel); 
    //alert($scope.noOfTank)

    if($scope.vehiclFuel==true){
      if($scope.noOfTank==1){
        document.getElementById("graphsId").style.width = "360px";
      }
      else {
       if($scope.noOfTank==2){
         document.getElementById("graphsId").style.width = "550px";
       }else if($scope.noOfTank==3){
         document.getElementById("graphsId").style.width = "630px";
       }else if($scope.noOfTank==4){
         document.getElementById("graphsId").style.width = "785px";
       }else if($scope.noOfTank==5){
         document.getElementById("graphsId").style.width = "940px";
       }
     }
   } else {
    document.getElementById("graphsId").style.width = "190px";
  }
}
//multiple sensor
function getFuelTank(fuelLitres){
  if($scope.noOfTank>1){
    if(fuelLitres!="0"){
      $scope.fuelLitreArr = fuelLitres.split(":");
    }else{
      $scope.fuelLitreArr = "0:0:0";
    } 
    fuelGraph();
  }
}

function fuelGraph(){
    //START
    var widthValue=180;
    var center2 ='90%';
    if($scope.noOfTank>2){
      widthValue = 150;
      center2 = '80%';
    }
    var sensorGaugeOptions = {
      chart: {
        type: 'solidgauge',
        height: 100 ,
        width : widthValue,
      },
      credits: { enabled: false },
      title: null,

      pane: {
        center: ['50%',center2],
        size: widthValue+'%',
        startAngle: -90,
        endAngle: 90,
        background: {
          backgroundColor:
          Highcharts.defaultOptions.legend.backgroundColor || '#EEE',
          innerRadius: '60%',
          outerRadius: '100%',
          shape: 'arc'
        }
      },

      exporting: {
        enabled: false
      },

      tooltip: {
        enabled: false
      },

      // the value axis
      yAxis: {
        stops: [
              [0.1, '#55BF3B'], // green
              [0.5, '#DDDF0D'], // yellow
              [0.9, '#DF5353'] // red
              ],
              lineWidth: 0,
              tickWidth: 0,
              minorTickInterval: null,
              tickAmount: 2,
              title: {
                y: -70
              },
              labels: {
                enabled: false
              }
            },

            plotOptions: {
              solidgauge: {
                dataLabels: {
                  y: 5,
                  borderWidth: 0,
                  useHTML: true
                }
              }
            }
          };

          var chartFuelSensor=[];
          for (var i = 0; i < $scope.noOfTank; i++) {
    //alert(i);
      // The speed gauge
      var id='container-fuelSensor'+(i+1);
  //     alert(id)
  //        if(document.getElementById(id)){
  //     alert("Element exists");
  // } else {
  //     alert("Element does not exist");
  // }
  chartFuelSensor[i]= $('#'+id).highcharts(Highcharts.merge(sensorGaugeOptions, {
    yAxis: {
      min: 0,
      max: $scope.tankSizeArr[i],
      title: {
        text: ''
      }
    },

    credits: {
      enabled: false
    },

    series: [{
      name: 'Fuel',
      data: [$scope.fuelLitreArr[i]],
      dataLabels: {
        format:
        '<div style="text-align:center"><span style="font-size:12px; font-weight:normal;color: #196481'+ '">Fuel - {y} Ltr</span><br/>'

      },
      tooltip: {
        valueSuffix: ' km/h'
      }
    }]

  }))
}

$('#container-speed').highcharts({

  chart: {
    type: 'gauge',
    plotBackgroundColor: null,
    plotBackgroundImage: null,
    plotBorderWidth: 0,
    plotShadow: false,
    spacingBottom: 10,
    spacingTop: -60,
    spacingLeft: -20,
    spacingRight: -20,
  },

  title: {
    text: ''
  },

  pane: {
            // startAngle: -150,
            // endAngle: 150,
            // // startAngle: -90,
            // // endAngle: 90,
            // center:['50%', '78%'],
            // size: '68%',
            startAngle: startangle,
            endAngle: endangle,
            center:['50%', speedCenter],
            size: speedSize,
            background: [{
              backgroundColor: {
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                stops: [
                [0, '#FFF'],
                [1, '#333']
                ]
              },
              borderWidth: 0,
              outerRadius: '109%'
            }, {
              backgroundColor: {
                linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
                stops: [
                [0, '#333'],
                [1, '#FFF']
                ]
              },
              borderWidth: 1,
              outerRadius: '107%'
            }, {
                // default background
              }, {
                backgroundColor: '#DDD',
                borderWidth: 0,
                outerRadius: '105%',
                innerRadius: '103%'
              }]
            },
            credits: { enabled: false },
        // the value axis
        yAxis: {
          min: 0,
          max: 200,

          minorTickInterval: 'auto',
          minorTickWidth: 1,
          minorTickLength: 10,
          minorTickPosition: 'inside',
          minorTickColor: '#666',

          tickPixelInterval: 30,
          tickWidth: 2,
          tickPosition: 'inside',
          tickLength: 10,
          tickColor: '#666',
          labels: {
            step: 2,
            rotation: 'auto'
          },
          title: {
                // text: 'km/h'
              },
              plotBands: [{
                from: 0,
                to: 120,
                color: '#55BF3B' // green
              }, {
                from: 120,
                to: 160,
                color: '#DDDF0D' // yellow
              }, {
                from: 160,
                to: 200,
                color: '#DF5353' // red
              }]        
            },

            series: [{
              name: 'Speed',
              data: [total],
              tooltip: {
                valueSuffix: ' km/h'
              }
            }]

          });

    // Bring life to the dials
    setInterval(function () {


    }, 1000);
    //END

  }
  function setFuelAndSpeed(){

    var chart = $('#container-speed').highcharts(), point;
    if (chart) {
      point = chart.series[0].points[0];
      point.update(total);
    }
    if(sensor==1){
     var chartFuel = $('#container-fuel').highcharts(), point;
     if (chartFuel) {
      chartFuel.yAxis[0].update({max:tankSize});
      point = chartFuel.series[0].points[0];
      point.update(fuelLtr);
    }
  }else{
      //Multiple Sensor
      var point;
      if($scope.noOfTank>1){
        for (var i = 0; i < $scope.noOfTank ; i++) {
         // alert('cxvf');
         var fuelId='#container-fuelSensor'+(i+1);
         var chartFuelSensor = $(fuelId).highcharts(), point;
         if (chartFuelSensor) {
          chartFuelSensor.yAxis[0].update({max:$scope.tankSizeArr[i]});
          point = chartFuelSensor.series[0].points[0];
          point.update(parseInt($scope.fuelLitreArr[i]));
        }
      }
    }
  }
}


//end controller


}])
.directive('map', ['$http', '_global', function($http, GLOBAL) {
  return {
    restrict: 'E',
    replace: true,
    template: '<div></div>',
    link: function(scope, element, attrs) {


    }
  };
}]);

app.directive('maposm', function($http, vamoservice) {
  return {
    restrict: 'E',
    replace: true,
    template: '<div></div>',
    link: function(scope, element, attrs){

         // console.log('osm map directive...');

      /*  var mapLink = '<a href="http://207.154.194.241/nominatim/lf.html">OpenStreeetMap</a>'
          var map_osm = new L.map('map_canvas2',{ center: [13.0827,80.2707],/* minZoom: 4,zoom: 8 });

          new L.tileLayer(
            'http://207.154.194.241/osm_tiles/{z}/{x}/{y}.png', {
             attribution: '&copy; '+mapLink+' Contributors',
          // maxZoom: 18,
        }).addTo(map_osm); */
      }
    };

  }); 
$(document).ready(function(e) {
  $('#container-speed').highcharts({

    chart: {
      type: 'gauge',
      plotBackgroundColor: null,
      plotBackgroundImage: null,
      plotBorderWidth: 0,
      plotShadow: false,
      spacingBottom: 10,
      spacingTop: -60,
      spacingLeft: -20,
      spacingRight: -20,
    },

    title: {
      text: ''
    },

    pane: {
      startAngle: -90,
      endAngle: 90,
      center:['50%', '100%'],
      size: '100%',
      background: [{
        backgroundColor: {
          linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
          stops: [
          [0, '#FFF'],
          [1, '#333']
          ]
        },
        borderWidth: 0,
        outerRadius: '109%'
      }, {
        backgroundColor: {
          linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
          stops: [
          [0, '#333'],
          [1, '#FFF']
          ]
        },
        borderWidth: 1,
        outerRadius: '107%'
      }, {
              // default background
            }, {
              backgroundColor: '#DDD',
              borderWidth: 0,
              outerRadius: '105%',
              innerRadius: '103%'
            }]
          },
          credits: { enabled: false },
      // the value axis
      yAxis: {
        min: 0,
        max: 200,

        minorTickInterval: 'auto',
        minorTickWidth: 1,
        minorTickLength: 10,
        minorTickPosition: 'inside',
        minorTickColor: '#666',

        tickPixelInterval: 30,
        tickWidth: 2,
        tickPosition: 'inside',
        tickLength: 10,
        tickColor: '#666',
        labels: {
          step: 2,
          rotation: 'auto'
        },
        title: {
              // text: 'km/h'
            },
            plotBands: [{
              from: 0,
              to: 120,
              color: '#55BF3B' // green
            }, {
              from: 120,
              to: 160,
              color: '#DDDF0D' // yellow
            }, {
              from: 160,
              to: 200,
              color: '#DF5353' // red
            }]        
          },

          series: [{
            name: 'Speed',
            data: [total],
            tooltip: {
              valueSuffix: ' km/h'
            }
          }]

        });




  var gaugeOptions = {

    chart: {
      type: 'solidgauge',
      spacingBottom: -10,
      spacingTop: -40,
      spacingLeft: 0,
      spacingRight: 0,
    },

    title: null,

    pane: {
      center: ['50%', '90%'],
      size: '110%',
      startAngle: -90,
      endAngle: 90,
      background: {
        backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
        innerRadius: '60%',
        outerRadius: '100%',
        shape: 'arc'
      }
    },

    tooltip: {
      enabled: false
    },

    // the value axis
    yAxis: {
      stops: [
            [0.1, '#55BF3B'], // green
            [0.5, '#DDDF0D'], // yellow
            [0.9, '#DF5353'] // red
            ],
            lineWidth: 0,
            minorTickInterval: null,
            title: {
              y: -50
            },
            labels: {
              y: -100
            }
          },

          plotOptions: {
            solidgauge: {
              dataLabels: {
                y: 5,
                borderWidth: 0,
                useHTML: true
              }
            }
          }
        };


        $('#container-fuel').highcharts(Highcharts.merge(gaugeOptions, {
          yAxis: {
            min: 0,
            max: 300,
            title: { text: '' }
          },
          credits: { enabled: false },
          series: [{
            name: 'Speed',
            data: [fuelLtr],
            dataLabels: {
              format: '<div style="text-align:center"><span style="font-size:12px; font-weight:normal;color: #196481'+ '">Fuel - {y} Ltr</span><br/>',
             // y: 25
           },
           tooltip: { valueSuffix: ' Ltr'}
         }]
       }));


      });


