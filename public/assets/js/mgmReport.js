app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global', function($scope, $http, vamoservice, $filter, GLOBAL){
	
  //global declaration
	$scope.uiDate 				=	{};
	$scope.uiValue	 			= 	{};
  //$scope.sort                 =   sortByDate('startTime');
    $scope.interval             =   '';
    $scope.mgmData = [];
    var tab = getParameterByName('tn');
    var licenceExpiry="";
    var strExpired='expired';
    if(localStorage.getItem('licenceExpiry'))licenceExpiry=localStorage.getItem('licenceExpiry');
    $scope.errMsg="";
    $scope.trvShow       =  localStorage.getItem('trackNovateView');
    $scope.sort = sortByDate('numOfCycle',true);
       
	function getParameterByName(name) {
    	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	//global declartion
	$scope.locations = [];
	$scope.url = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
	$scope.gIndex =0;

  //$scope.locations01 = vamoservice.getDataCall($scope.url);
	
	function sessionValue(vid, gname,licenceExpiry){
		localStorage.setItem('user', JSON.stringify(vid+','+gname));
		localStorage.setItem('licenceExpiry', licenceExpiry);
		$("#testLoad").load("../public/menu");
	}
	

    function webCall(){
      var newmonVal=$scope.fromMonth.split('/');
      $scope.month=parseInt(newmonVal[0]);
      $scope.year=newmonVal[1]; 
      mgmReportUrl   =  GLOBAL.DOMAIN_NAME+"/getMgmReport?year="+$scope.year+"&month="+$scope.month+"&groupName="+$scope.uiGroup;

      console.log(mgmReportUrl);

       
        $http.get(mgmReportUrl).success(function(data) {
			var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
			const monthFullNames = ["January", "February", "March", "April", "May", "June","July", "August", "September", "October", "November", "December"];
			$scope.selMonthYear = monthFullNames[$scope.month-1] + ' - '+ $scope.year ;
            $scope.allData = data;
            console.log($scope.allData)
            $scope.dayArr=[];
            $scope.daysInMonth=new Date($scope.year, $scope.month, 0).getDate();
            var today=new Date()
            if($scope.month==today.getMonth()+1&&today.getFullYear()==$scope.year){
            	$scope.daysInMonth = today.getDate()-1;
            }
           for (var i = 0; i <= $scope.daysInMonth-1; i++) {
           	$scope.dayArr[i]= (i+1)+'-'+monthNames[$scope.month-1];
           }
           setTimeout(function(){ stopLoading(); enhance(); }, 100);
		}); 
   
    }

	// initial method

	$scope.$watch("url", function (val) {
		vamoservice.getDataCall($scope.url).then(function(data) {
           
          //startLoading();
            $scope.selectVehiData = [];
			$scope.vehicle_group=[];
			$scope.vehicle_list = data;

			if(data.length){
				$scope.vehiname	= getParameterByName('vid');
				$scope.uiGroup 	= $scope.trimColon(getParameterByName('vg'));
				$scope.gName 	= getParameterByName('vg');
				angular.forEach(data, function(val, key){
                  //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
					if($scope.gName == val.group){
						$scope.gIndex = val.rowId;
 
						angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys){

                            $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

							if($scope.vehiname == value.vehicleId){
							    $scope.shortNam	= value.shortName;
							    $scope.vehIds   = value.vehicleId;
							    licenceExpiry=value.licenceExpiry;
						    }
						})
						
					}
			    })

		  //console.log($scope.selectVehiData);
		    sessionValue($scope.vehiname, $scope.gName,licenceExpiry)
		    $scope.notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
            $('#notncount').text($scope.notncount);
            window.localStorage.setItem('totalNotifications',$scope.notncount);
            
			}   
		    startLoading();
		    //get month/year initially
		    var today = new Date();
			var mm = today.getMonth()+1; 
			var yyyy = today.getFullYear();
			if(mm<10) 
			{
			    mm='0'+mm;
			} 
			$scope.fromMonth = mm+'/'+yyyy;
		    webCall();
		  //stopLoading();
		});	
	});

    
   
  	$scope.groupSelection 	= function(groupName, groupId) {
		startLoading();
		undo();
		licenceExpiry="";
        $scope.errMsg="";
		$scope.gName 	= 	groupName;
		$scope.uiGroup 	= 	$scope.trimColon(groupName);
		$scope.gIndex	=	groupId;
		var url  		= 	GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+groupName;

		vamoservice.getDataCall(url).then(function(response){
		
			$scope.vehicle_list = response;
			$scope.shortNam		= response[$scope.gIndex].vehicleLocations[0].shortName;
			$scope.vehiname		= response[$scope.gIndex].vehicleLocations[0].vehicleId;
			licenceExpiry   = response[$scope.gIndex].vehicleLocations[0].licenceExpiry;
			sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
            $scope.selectVehiData=[];
            //console.log(response);
            	angular.forEach(response, function(val, key){
					if($scope.gName == val.group){
					//	$scope.gIndex = val.rowId;
                        angular.forEach(response[$scope.gIndex].vehicleLocations, function(value, keys){

                            $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

							if($scope.vehiname == value.vehicleId)
							 $scope.shortNam	= value.shortName;
							 $scope.vehIds   = value.vehicleId;
						})
				    }
				})

            $scope.notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
            $('#notncount').text($scope.notncount);
            window.localStorage.setItem('totalNotifications',$scope.notncount);
	        webCall();
		  //webServiceCall();
	      //stopLoading();
		});

	}


	$scope.genericFunction 	= function (vehid, index,shortname,position, address,groupName,licenceExp) {
		startLoading();
		licenceExpiry=licenceExp;
		$scope.errMsg="";
		$scope.vehiname  =  vehid;
		sessionValue($scope.vehiname, $scope.gName,licenceExpiry)
		angular.forEach($scope.vehicle_list[$scope.gIndex].vehicleLocations, function(val, key){
			if(vehid == val.vehicleId){
				$scope.shortNam	= val.shortName;
			    $scope.vehIds   = val.vehicleId;
			}
		});
        webCall();
	}
	$scope.changeMonth=function(){
	  //alert(document.getElementById("monthFrom").value);
	  startLoading();
      $scope.fromMonth=document.getElementById("monthFrom").value;
	  console.log($scope.fromMonth);
	  webCall();
	}

 
	$scope.trimColon = function(textVal){
			return textVal.split(":")[0].trim();
		}


	$scope.exportData = function (data) {
		// console.log(data);
		var blob = new Blob([document.getElementById(data).innerHTML], {
           	type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, data+".xls");
    };

    $scope.exportDataCSV = function (data) {
		// console.log(data);
		CSV.begin('#'+data).download(data+'.csv').go();
    };
    
	$('#minus').click(function(){
		$('#menu').toggle(1000);
	})

}]);
