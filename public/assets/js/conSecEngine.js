app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global', function($scope, $http, vamoservice, $filter, GLOBAL){
	$scope.videoLink="https://www.youtube.com/watch?v=RM09hu_mvME&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=7";

  //global declaration
	$scope.uiDate 				=	{};
	$scope.uiValue	 			= 	{};
  

    var tab = getParameterByName('tn');
       
	function getParameterByName(name) {
    	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	//global declartion
	$scope.locations = [];
	$scope.url = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
	$scope.gIndex =0;

  //$scope.locations01 = vamoservice.getDataCall($scope.url);
	$scope.trimColon = function(textVal){
		return textVal.split(":")[0].trim();
	}

	function sessionValue(vid, gname){
		localStorage.setItem('user', JSON.stringify(vid+','+gname));
		$("#testLoad").load("../public/menu");
	}

	function setBtnEnable(btnName){
   $scope.yesterdayDisabled = false;
   $scope.todayDisabled = false;

   switch(btnName){

    case 'yesterday':
      $scope.yesterdayDisabled = true;
      break;
     case 'today':
      $scope.todayDisabled = true;
      break;
   }


}
setBtnEnable("init");
$scope.durationFilter    =   function(duration){
  //alert('inside function');
  startLoading();
  var now = new Date();
  $scope.uiDate.todate      = getTodayDate(now.setDate(now.getDate() - 1));
  switch(duration){
    
    case 'yesterday':
       setBtnEnable('yesterday');
       var d = new Date();
       $scope.uiDate.fromdate= getTodayDate(d.setDate(d.getDate() - 1));
       $scope.uiDate.totime  = '11:59 PM';
      // datechange();
     break;
    case 'today':
       setBtnEnable('today');
       var d = new Date();
       $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() ));
       $scope.uiDate.todate       = getTodayDate(d.setDate(d.getDate() ));
       $scope.uiDate.totime       = formatAMPM(d);
      //datechange();
     break;
  }
  webCall();
}


$scope.msToTime = function (duration) {
	  var  seconds = Math.floor((duration / 1000) % 60),
	    minutes = Math.floor((duration / (1000 * 60)) % 60),
	    hours = Math.floor((duration / (1000 * 60 * 60)) % 24);

	  hours = (hours < 10) ? "0" + hours : hours;
	  minutes = (minutes < 10) ? "0" + minutes : minutes;
	  seconds = (seconds < 10) ? "0" + seconds : seconds;

	  return hours + ":" + minutes + ":" + seconds;
  }

    function webCall(){
      let isAllStarter = $scope.vehicle_list[$scope.gIndex].vehicleLocations.every(function(v){
        return v.licenceType == 'Starter';
      })
      console.log(isAllStarter);
      if(isAllStarter){
      	   stopLoading();
           $scope.conSecEngData = {
							execReportData: [],
							error:'This feature is not privileged for selected group' 
						};
			$scope.execReportData=[];
      }else{
      	var fromDate = moment($scope.uiDate.fromdate, "DD-MM-YYYY").format("YYYY-MM-DD");
        var toDate = moment($scope.uiDate.todate, "DD-MM-YYYY").format("YYYY-MM-DD");
        var conSecEngUrl    = GLOBAL.DOMAIN_NAME+'/getExecutiveReport?groupId='+$scope.gName+'&fromDate='+fromDate+'&toDate='+toDate;
        console.log(conSecEngUrl);
        $scope.conSecEngData=[];

        $http.get(conSecEngUrl).success(function(data){
            $scope.conSecEngData=data;
            $scope.execReportData = $scope.conSecEngData.execReportData;

         // console.log($scope.tollData);
          
          stopLoading();
		});

      }
    	 
    }



	//get the value from the ui
	function getUiValue(){
		$scope.uiDate.fromdate 		=	$('#dateFrom').val();
	  	$scope.uiDate.todate		=	$('#dateTo').val();
	}

 

	$scope.$watch("url", function (val) {
		vamoservice.getDataCall($scope.url).then(function(data) {
           
          //startLoading();
            $scope.selectVehiData = [];
			$scope.vehicle_group=[];
			$scope.vehicle_list = data;

			if(data.length){
				$scope.vehiname	= getParameterByName('vid');
				$scope.uiGroup 	= $scope.trimColon(getParameterByName('vg'));
				$scope.gName 	= getParameterByName('vg');
				angular.forEach(data, function(val, key){
                  //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
					if($scope.gName == val.group){
						$scope.gIndex = val.rowId;
 
						angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys){

                            $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

							if($scope.vehiname == value.vehicleId)
							$scope.shortNam	= value.shortName;
						})
						
					}
			    })

		  //console.log($scope.selectVehiData);
		    sessionValue($scope.vehiname, $scope.gName)
            $scope.notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
            $('#notncount').text($scope.notncount);
            window.localStorage.setItem('totalNotifications',$scope.notncount);

			}
			var dateObj 			= 	new Date();
			$scope.fromNowTS		=	new Date(dateObj.setDate(dateObj.getDate()-1));
			$scope.uiDate.fromdate 		=	getTodayDate($scope.fromNowTS);
		  	$scope.uiDate.todate		=	getTodayDate($scope.fromNowTS);
		    startLoading();
		    webCall();
		  //stopLoading();
		});	
	});

    
   
  	$scope.groupSelection 	= function(groupName, groupId) {
		startLoading();
		$scope.gName 	= 	groupName;
		$scope.uiGroup 	= 	$scope.trimColon(groupName);
		$scope.gIndex	=	groupId;
		var url  		= 	GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+groupName;

		vamoservice.getDataCall(url).then(function(response){
		
			$scope.vehicle_list = response;
			$scope.shortNam		= response[$scope.gIndex].vehicleLocations[0].shortName;
			$scope.vehiname		= response[$scope.gIndex].vehicleLocations[0].vehicleId;
			sessionValue($scope.vehiname, $scope.gName);
            $scope.selectVehiData=[];
            //console.log(response);
            	angular.forEach(response, function(val, key){
					if($scope.gName == val.group){
					//	$scope.gIndex = val.rowId;
                        angular.forEach(response[$scope.gIndex].vehicleLocations, function(value, keys){

                            $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

							if($scope.vehiname == value.vehicleId)
							$scope.shortNam	= value.shortName;
						})
				    }
				})
				
			$scope.notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
            $('#notncount').text($scope.notncount);
            window.localStorage.setItem('totalNotifications',$scope.notncount);

			getUiValue();
			webCall();
		  //webServiceCall();
	      //stopLoading();
		});

	}



  	$scope.submitFunction 	=	function(){
  		$scope.yesterdayDisabled = false;
	   $scope.todayDisabled = false;
	    startLoading();
	    getUiValue();
	    webCall();
	
	}

	$scope.exportData = function (data) {
		// console.log(data);
		var blob = new Blob([document.getElementById(data).innerHTML], {
           	type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
        });
        saveAs(blob, data+".xls");
    };

    $scope.exportDataCSV = function (data) {
		// console.log(data);
		CSV.begin('#'+data).download(data+'.csv').go();
    };

	$('#minus').click(function(){
		$('#menu').toggle(1000);
	})
  

}]);
