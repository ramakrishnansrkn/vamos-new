app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global', '$translate' ,function($scope, $http, vamoservice, $filter, GLOBAL,$translate){
//global declaration
$scope.videoLink="https://www.youtube.com/watch?v=yb9iTyry2zo&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=39";
$scope.uiDate       =  {};
$scope.uiValue      =  {};
var language=localStorage.getItem('lang');
$translate.use(language);
var translate = $filter('translate');
$scope.sort         =  sortByDate('startTime', false);
$scope.interval     =  "";
$scope.fuelDataMsg  =  "";
var licenceExpiry="";
var strExpired='expired';
if(localStorage.getItem('licenceExpiry'))licenceExpiry=localStorage.getItem('licenceExpiry');
$scope.errMsg="";
var tab = getParameterByName('tn');
var movingAvg = getParameterByName('movingAvg');
var dgAvg = getParameterByName('dgAvg');
var fuelDataVals;
$scope.sensorCount   = 1;
$scope.vehicleMode   = "";
$scope.sensor = 1;
$scope.millage =""
$scope.ltphValue = "";
$scope.ignitionOnPer = "";
$scope.position='';
$scope.minFuelTheft = "";
$scope.customMinFuelTheftFilterValue = "";
$scope.showFueltheftFilter = false;
$scope.theftBeta = false;
var d = new Date();
$scope.day = d.getDay();
var expiryDays='';


$scope.range = function(min, max, step) {
  step = step || 1;
  var input = [];
  for (var i = min; i <= max; i += step) {
    input.push(i);
  }
  return input;
};


function getParameterByName(name) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
  results = regex.exec(location.search);
  return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

  //global declartion
  $scope.locations = [];
  $scope.url       = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
  $scope.gIndex    = 0;

  //$scope.locations01 = vamoservice.getDataCall($scope.url);
  $scope.trimColon = function(textVal) {

    if(textVal){
     var spltVal = textVal.split(":");
     return spltVal[0];
   }
 }

 function sessionValue(vid, gname,licenceExpiry){
  localStorage.setItem('user', JSON.stringify(vid+','+gname));
  localStorage.setItem('licenceExpiry', licenceExpiry);
  $("#testLoad").load("../public/menu");
}



$scope.roundOffDecimal = function(val) {
  return Number.parseFloat(val).toFixed(2);
}

function convert_to_24h(time_str) {
    //console.log(time_str);
    var str       =  time_str.split(' ');
    var stradd    =  str[0].concat(":00");
    var strAMPM   =  stradd.concat(' '+str[1]);
    var time      =  strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
    var hours     =  Number(time[1]);
    var minutes   =  Number(time[2]);
    var seconds   =  Number(time[2]);
    var meridian  =  time[4].toLowerCase();

    if (meridian == 'p' && hours < 12) {
      hours = hours + 12;
    }
    else if (meridian == 'a' && hours == 12) {
      hours = hours - 12;
    }     
    var marktimestr = ''+hours+':'+minutes+':'+seconds;      
    return marktimestr;
  };

    // millesec to day, hours, min, sec
    $scope.msToTime = function(ms) {
      days = Math.floor(ms / (24 * 60 * 60 * 1000));
      daysms = ms % (24 * 60 * 60 * 1000);
      hours = Math.floor((ms) / (60 * 60 * 1000));
      hoursms = ms % (60 * 60 * 1000);
      minutes = Math.floor((hoursms) / (60 * 1000));
      minutesms = ms % (60 * 1000);
      seconds = Math.floor((minutesms) / 1000);
    // if(days==0)
    //  return hours +" h "+minutes+" m "+seconds+" s ";
    // else
    return hours +":"+minutes+":"+seconds;
  }

  $scope.msToTime2 = function(ms) {

    days       =  Math.floor(ms / (24 * 60 * 60 * 1000));
    daysms     =  ms % (24 * 60 * 60 * 1000);
    hours      =  Math.floor((daysms) / (60 * 60 * 1000));
    hoursms    =  ms % (60 * 60 * 1000);
    minutes    =  Math.floor((hoursms) / (60 * 1000));
    minutesms  =  ms % (60 * 1000);
    seconds    =  Math.floor((minutesms) / 1000);

    if(days>1) {
      return days+":"+hours+":"+minutes+":"+seconds;
    } else if(days==1) {
      return days+":"+hours+":"+minutes+":"+seconds;
    } else if(days==0) {
      return hours +":"+minutes+":"+seconds;
    }
  }

  var delayed4 = (function () {
    var queue = [];

    function processQueue() {
      if (queue.length > 0) {
        setTimeout(function () {
          queue.shift().cb();
          processQueue();
        }, queue[0].delay);
      }
    }

    return function delayed(delay, cb) {
      queue.push({ delay: delay, cb: cb });

      if (queue.length === 1) {
        processQueue();
      }
    };
  }());

  function google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent) {
    vamoservice.getDataCall(tempurlEvent).then(function(data) {
      $scope.addressEvent[index4] = data.results[0].formatted_address;
      //console.log(' address '+$scope.addressEvent[index4])
      //var t = vamo_sysservice.geocodeToserver(latEvent,lonEvent,data.results[0].formatted_address);
    })
  };

  $scope.recursiveEvent   =   function(locationEvent, indexEvent){
    var index4 = 0;
    angular.forEach(locationEvent, function(value ,primaryKey){
    //console.log(' primaryKey '+primaryKey)
    index4 = primaryKey;
    if(locationEvent[index4].address == undefined)
    {
      var latEvent     =  locationEvent[index4].latitude;
      var lonEvent     =  locationEvent[index4].longitude;
      var tempurlEvent =  "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latEvent+','+lonEvent+"&sensor=true";
      delayed4(2000, function (index4) {
        return function () {
          google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent);
        };
      }(index4));
    }
  })
  }


  function formatAMPM(date) {
    var date = new Date(date);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
      return strTime;
    }

    //get the value from the ui
    function getUiValue(){
      $scope.uiDate.fromdate   =  $('#dateFrom').val();
      $scope.uiDate.fromtime   =  $('#timeFrom').val();
      $scope.uiDate.todate     =  $('#dateTo').val();
      $scope.uiDate.totime     =  $('#timeTo').val();
      if(localStorage.getItem('timeTochange')!='yes'){
       updateToTime();
       $scope.uiDate.totime    =   localStorage.getItem('toTime');
     }
     if(tab=="fuelDispenser"){
      $scope.uiDate.fromdate   =  $('#dtFrom').val();

      $scope.uiDate.todate     =  $('#dtTo').val();
      
    }
    if(tab!="fuelDispenser"){
      $scope.uiDate.fromtimes  =  convert_to_24h($scope.uiDate.fromtime);
      $scope.uiDate.totimes    =  convert_to_24h($scope.uiDate.totime);}
    }

    function setBtnEnable(btnName){
     $scope.yesterdayDisabled = false;
     $scope.weekDisabled = false;
     $scope.todayDisabled = false;
     $scope.thisweekDisabled = false;

     switch(btnName){

      case 'yesterday':
      $scope.yesterdayDisabled = true;
      break;
      case 'today':
      $scope.todayDisabled = true;
      break;
      case 'thisweek':
      $scope.thisweekDisabled = true;
      break;
      case 'lastweek':
      $scope.weekDisabled = true;
      break;
    }


  }
  setBtnEnable("init");
  $scope.durationFilter    =   function(duration){
  //alert('inside function');
  startLoading();
  var now = new Date();
  $scope.uiDate.todate      = getTodayDate(now.setDate(now.getDate() - 1));
  switch(duration){

    case 'yesterday':
    setBtnEnable('yesterday');
    var d = new Date();
    $scope.uiDate.fromdate= getTodayDate(d.setDate(d.getDate() - 1));
    $scope.uiDate.totime  = '11:59 PM';
      // datechange();
      break;
      case 'thisweek':
      setBtnEnable('thisweek');
      var d=new Date();
      var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1);
      var firstday= new Date(d.getFullYear(), d.getMonth(), diff);
      $scope.uiDate.fromdate      = getTodayDate(firstday.setDate(firstday.getDate() ));
      $scope.uiDate.todate= getTodayDate(new Date().setDate(new Date().getDate()-1 ));

      $scope.uiDate.totime       = formatAMPM(d);
      //datechange();
      break;
      case 'lastweek':
      setBtnEnable('lastweek');
      var d = new Date();
      var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1)-7;
      var diff1=d.getDate() - day + (day == 0 ? -6:1)-1;
      var firstday= new Date(d.getFullYear(), d.getMonth(), diff);
      var lastday= new Date(d.getFullYear(), d.getMonth(), diff1);
      $scope.uiDate.fromdate      = getTodayDate(firstday.setDate(firstday.getDate() ));
      $scope.uiDate.todate       = getTodayDate(lastday.setDate(lastday.getDate() ));
      $scope.uiDate.totime  = '11:59 PM';
      //datechange();
      break;
      case 'today':
      setBtnEnable('today');
      var d = new Date();
      $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() ));
      $scope.uiDate.todate       = getTodayDate(d.setDate(d.getDate() ));
      $scope.uiDate.totime       = formatAMPM(d);
      //datechange();
      break;

    }
    webCall();
  }

  function webCall() {

     /*   var urlAllow    =  true; 
        var fromTms     =  utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime));
        var toTms       =  utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
        var totalTms    =  toTms-fromTms;                       
        var splitTimes  =  $scope.msToTime2(totalTms).split(':');
        var daysDiff    =  0;

          if( splitTimes.length == 4 ) {
            daysDiff = splitTimes[0];
          }
                        
      //alert(daysDiff);

      if( daysDiff == 0 ) { */


       $scope.millage = "";
       $scope.ltphValue = "";
       $scope.ignitionOnPer = "";
       $scope.minFuelTheft = "";
       $scope.customMinFuelTheftFilterValue = "";
       var movingAvgURL= "",dgAvgURL= "";
       if(movingAvg){
        movingAvgURL = "&movingAvg="+movingAvg;
      }
      if(dgAvg){
        dgAvgURL = "&dgAvg="+dgAvg;
      }
      additionalURL = movingAvgURL+dgAvgURL;


      if((checkXssProtection($scope.uiDate.fromdate) == true) && ((checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true))) {                 

         //var fuelRawUrl      =  GLOBAL.DOMAIN_NAME+'/getFuelRawData?vehicleId='+$scope.vehIds+'&fromTimeUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toTimeUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));        
         //var fuelRawUrl      =  'http://128.199.159.130:9000/getFuelRawData?userId=naplogi&vehicleId=NAPLOGI-AP16TE3458&fromTimeUtc=1533037324617&toTimeUtc=1533195298000';     
         //var fuelMachineUrl  =  'http://209.97.163.4:9000/getFuelDetailForMachinery?userId=SENTINI&vehicleId=SENTINI_AP16TY5191&fromDateTime=1536049434000&toDateTime=1536567851000';           

         var fuelMachineUrl  = "";
         if(tab=="fuelDispenser"){
          fuelMachineUrl =GLOBAL.DOMAIN_NAME+'/getExecutiveFuelReport?groupId='+$scope.gName+'&fromDate='+moment($scope.uiDate.fromdate, "DD-MM-YYYY").format("YYYY-MM-DD")+'&toDate='+moment($scope.uiDate.todate, "DD-MM-YYYY").format("YYYY-MM-DD")+'&fuelDispenser='+true;
        }
        else {
          if($scope.sensorCount>1){
            if($scope.sensor=='All'){
              fuelMachineUrl  =  GLOBAL.DOMAIN_NAME+'/getFuelDetailForAll?vehicleId='+$scope.vehIds+'&fromDateTime='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateTime='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
            }else{
              fuelMachineUrl  =  GLOBAL.DOMAIN_NAME+'/getFuelDetailForMachinery?vehicleId='+$scope.vehIds+'&fromDateTime='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateTime='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime))+"&sensor="+$scope.sensor;
            }
          }else{
           fuelMachineUrl  =  GLOBAL.DOMAIN_NAME+'/getFuelDetailForMachinery?vehicleId='+$scope.vehIds+'&fromDateTime='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toDateTime='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
         }}
         fuelMachineUrl = fuelMachineUrl+additionalURL;
         console.log( fuelMachineUrl );
         if(tab!='fuelDispenser'){
           var expdate=moment().add(expiryDays,'days').format('DD-MM-YYYY'),
           convertedexpdate=utcFormat(expdate,convert_to_24h('11:59 PM')),
           convertedtodate=utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime)),
           convertedfromdate=utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime));
         }
         var apicall=tab!='fuelDispenser'?convertedtodate<=convertedexpdate && convertedfromdate<=convertedexpdate:true;
         if(apicall){
          $http.get( fuelMachineUrl ).success(function(data) { 
            stopLoading();  
            if(data=='Failed'){
              $scope.fuelMachineData = { 
                'error' : translate("curl_error"),
                fuelFills: [ ],
                fuelDrops: [ ],
                fuelFillBeta: [ ],
                fuelDropBeta: [ ],
                fuelRunningTheft: [ ],
              };
            }else{
              if($scope.sensor==1&&document.getElementById("sensor1")){
                document.getElementById("sensor1").disabled = true;
              }
              console.log( data );   

              $scope.totalFuelDispener=0.0; 

              $scope.fuelMachineData = data;

              if(tab == "fuelDispenser"){
               angular.forEach(data, function(val, key) {
                if(val!=null){
                  $scope.totalFuelDispener    =  $scope.totalFuelDispener+(val.fuelDispenser);
                }
              });
               if($scope.fuelMachineData.sensorMode == "Dispenser"){
                console.log('Dispenser')
              }else{
                $scope.fuelMachineData.fuelDrops = null;
              }
            }

                  // added for running theft filter start
                  if(!data.error){
                    if($scope.sensor!='All'){
                      //$scope.fuelMachineData.fuelRunningTheft = data.fuelRunningTheft.filter(fuel => fuel.fuelTheftBeta > data.minFuelTheft );
                      totalFuelTheftCalculation($scope.fuelMachineData.fuelRunningTheft);
                      // added for default filter
                      $scope.millage = data.expectedMileage;
                      $scope.ltphValue = data.expectedMileage;
                      $scope.applyFilter();
                      if(data.excessConsumptionFilter=='yes'){
                        $scope.showFueltheftFilter = true;
                        $scope.handleFuelFilter('');
                      }else{
                        $scope.showFueltheftFilter = false;
                      }
                      
                    }
                    if($scope.theftBeta == true){
                      $scope.sort  =  sortByDate('startTime', false);
                    }
                  }

                  console.log( $scope.fuelMachineData );
                }
              });
        }else{
          stopLoading();
          $scope.errMsg=licenceExpiry;
          $scope.showErrMsg = true;
        } 

      }  else {

       stopLoading();
     }

    /*  } else {

          $scope.fuelRawData = [];
          document.getElementById("container").innerHTML = '';

          alert('Plaese select less than 1 day.');
          stopLoading(); 
  
        }*/
      }


      function fuelGraph(data) {

        var Ltrs         =  [];
        var fuelDate     =  [];
  //var distCovered  =  [];
  //var spdVals      =  [];
  var tankSize = $scope.tankSize;

  console.log(data);

  console.log( tankSize );

  try {

    if(data.length) {
      for(var i = 0; i < data.length; i++) {
        if(data[i].fuelLitr !='0' || data[i].fuelLitr !='0.0') {
          Ltrs.push( { y:parseFloat(data[i].fuelLitr), odo:data[i].odoMeterReading, ignition:data[i].ignitionStatus } );
          var dat = $filter('date')(data[i].dt, "dd/MM/yyyy HH:mm:ss");
          fuelDate.push(dat);
              //distCovered.push(data[i].distanceCovered);
              //spdVals.push(data[i].speed);
            }
          }
        }
      } catch(err) {
        console.log(err.message);
      }

      console.log( Ltrs );

      $(function () {

        $('#container').highcharts({
          chart: {
            zoomType: 'x',
              //alignTicks: true
            },
            title: {
              text: 'Fuel'
            },
            credits: {
              enabled: false
            },
            xAxis: {
             categories: fuelDate,
             title: {
              text: 'Date & Time'
            }

          },

          yAxis: {
            title: {
              text: 'Fuel (Ltrs)'
            },   
              //tickInterval: 10,         
              max: parseFloat($scope.tankSize),
              min: 0,
              endOnTick: false              
            },
            tooltip: {
              formatter: function() {
                var s, a = 0;
                //console.log(this.points);
                $.each(this.points, function() {
                      //console.log(this.point);
                      s = '<b>'+this.x+'</b>'+'</b>'+'<br>'+' '+'</br>'+
                      '--------------------'+'<br>'+' '+'</br>'+
                      'Fuel     : ' +'  '+'<b>' + this.point.y + '</b>'+' Ltrs'+'<br>'+' '+'</br>'+
                      /*'Speed    : ' +'  '+'<b>' + this.point.speed + '</b>'+' Kmph'+'<br>'+' '+'</br>'+*/
                      'Ignition : ' +'  '+'<b>' + this.point.ignition + '</b>'+'<br>'+' '+'</br>'+
                      'Odo      : ' +'  '+'<b>' + this.point.odo + '</b>';                             
                    });
                return s;
              },
              shared: true
            },
            legend: {
              enabled: false
            },
            plotOptions: {
              area: {
                fillColor: {
                  linearGradient: {
                    x1: 0,
                    y1: 0,
                    x2: 0,
                    y2: 1
                  },
                  stops: [
                  [0, Highcharts.getOptions().colors[0]],
                  [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                  ]
                },
                marker: {
                  radius: 2
                },
                lineWidth: 1,
                states: {
                  hover: {
                    lineWidth: 1
                  }
                },
                threshold: null,
                turboThreshold: 0
              }
            },

            series: [{
              type: 'area',
              name: 'Fuel Level',
              data: Ltrs
            }]
          });

      });

    }


  // initial method
  $scope.$watch("url", function(val) {
    vamoservice.getDataCall($scope.url).then(function(data) {

    //startLoading();
    //$scope.selectVehiData = [];
    //alert('fuel machine..');
    $scope.vehicle_group  =  [];
    $scope.vehicle_list   =  data;

    if(data.length) {
      //alert('fuel machine 2..');
      $scope.vehiname  =  getParameterByName('vid');
      $scope.uiGroup   =  $scope.trimColon(getParameterByName('vg'));
      $scope.gName     =  getParameterByName('vg');

      console.log( $scope.gName );

      angular.forEach(data, function(val, key){
        //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
        if($scope.gName == val.group){

            //$scope.gIndex = 0;
            $scope.gIndex = val.rowId;
            //alert( $scope.gName );
            angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys){
          //$scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});
          if($scope.vehiname == value.vehicleId){
           $scope.shortNam = value.shortName;
           $scope.vehIds   = value.vehicleId;
           licenceExpiry=value.licenceExpiry;
           $scope.sensorCount   = value.sensorCount;
           $scope.vehicleMode   = value.vehicleMode;
           $scope.position=val.position;
           expiryDays=value.expiryDays;
         }
       });
          }
        });

      //console.log($scope.selectVehiData);
      sessionValue($scope.vehiname, $scope.gName,licenceExpiry)
      $scope.notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
      $('#notncount').text($scope.notncount);
      window.localStorage.setItem('totalNotifications',$scope.notncount);
    }
    var d=new Date();
    var day = d.getDay(),
    diff = d.getDate() - day + (day == 0 ? -6:1);
    var firstday= new Date(d.getFullYear(), d.getMonth(), diff);
    $scope.uiDate.fromdate      = getTodayDate(firstday.setDate(firstday.getDate() ));
    $scope.uiDate.todate= getTodayDate(new Date().setDate(new Date().getDate()));    
    
    $scope.uiDate.fromtime    = localStorage.getItem('fromTime');
    $scope.uiDate.totime       = formatAMPM(d);
    
    if(localStorage.getItem('timeTochange')!='yes'){
      updateToTime();
      $scope.uiDate.totime    =   formatAMPM(d);
    }
      //alert($scope.uiDate.totime);
      $scope.uiDate.fromtimes  =   convert_to_24h($scope.uiDate.fromtime);
      $scope.uiDate.totimes    =   convert_to_24h($scope.uiDate.totime);
    //$scope.uiDate.totime     =   '11:59 PM';
    
    startLoading();
    webCall();
    //stopLoading();
  }); 
  });


  $scope.groupSelection = function(groupName, groupId) {
    startLoading();
    clearMultipleSensor(); // added for multiple sensor clear
    licenceExpiry="";
    $scope.errMsg="";
    $scope.sensor = 1;
    $scope.gName    =  groupName;

    console.log( $scope.gName );

    $scope.uiGroup  =  $scope.trimColon(groupName);
    $scope.gIndex   =  groupId;
    var url         =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+groupName;

    vamoservice.getDataCall(url).then(function(response) {

      $scope.vehicle_list = response;
      $scope.shortNam     = response[$scope.gIndex].vehicleLocations[0].shortName;
      $scope.vehiname     = response[$scope.gIndex].vehicleLocations[0].vehicleId;
      licenceExpiry   = response[$scope.gIndex].vehicleLocations[0].licenceExpiry;
      expiryDays   = response[$scope.gIndex].vehicleLocations[0].expiryDays;
      $scope.sensorCount   = response[$scope.gIndex].vehicleLocations[0].sensorCount;
      $scope.vehicleMode   = response[$scope.gIndex].vehicleLocations[0].vehicleMode;
      sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
      $scope.notncount =getParkedIgnitionAlert($scope.vehicle_list[$scope.gIndex].vehicleLocations)[3];
      $('#notncount').text($scope.notncount);
      window.localStorage.setItem('totalNotifications',$scope.notncount);
      $scope.selectVehiData = [];
    //console.log(response);
    angular.forEach(response, function(val, key) {
      if($scope.gName == val.group) {
          //$scope.gIndex = val.rowId;
          angular.forEach(response[$scope.gIndex].vehicleLocations, function(value, keys) {

            $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

            if($scope.vehiname == value.vehicleId) {
              $scope.shortNam = value.shortName;
              $scope.vehIds   = value.vehicleId;
            }
          });
        }
      });


    getUiValue();
    webCall();
    
      //stopLoading();
    });

  }


  $scope.genericFunction  = function (vehid, index,shortname,position, address,groupName,licenceExp){
    if(tab!="fuelDispenser"){
      startLoading();
    clearMultipleSensor(); // added for multiple sensor clear
    licenceExpiry=licenceExp;
    $scope.errMsg="";
    $scope.sensor = 1;
    //licenceExpiry="gfd hgdhghf hbfg";
    $scope.vehiname = vehid;
    sessionValue($scope.vehiname, $scope.gName,licenceExpiry)
    angular.forEach($scope.vehicle_list[$scope.gIndex].vehicleLocations, function(val, key){
      if(vehid == val.vehicleId){                                         
        $scope.shortNam = val.shortName;
        $scope.vehIds   = val.vehicleId;
        $scope.sensorCount   = val.sensorCount;
        $scope.vehicleMode   = val.vehicleMode;
        $scope.position=val.position;
      }
    });
    getUiValue();
    webCall();
  }
  else {

  }
}
function clearMultipleSensor(){
  if($scope.sensorCount>1){
    if($scope.sensor=="All"){
     document.getElementById("sensorAll").disabled = false;
   }else{
    for (var i = 1; i <= $scope.sensorCount ; i++) {
      if(i==$scope.sensor){
        document.getElementById("sensor"+i).disabled = false;
      }
    }
  }
}
}
$scope.sensorChange   = function(sensorNo){
  console.log("sensor  " + sensorNo);
  startLoading();
  for (var i = 1; i <= $scope.sensorCount ; i++) {
    if(i==sensorNo){
     document.getElementById("sensor"+i).disabled = true;
   }else{
     document.getElementById("sensor"+i).disabled = false;
   }
 }
 if(sensorNo=='All'){
  document.getElementById("sensorAll").disabled = true;
}else{
  document.getElementById("sensorAll").disabled = false;
}
$scope.sensor=sensorNo;
webCall();
}

$scope.submitFunction   = function(){
  $scope.yesterdayDisabled = false;
  $scope.weekDisabled = false;
  $scope.todayDisabled = false;
  $scope.thisweekDisabled = false;
  startLoading();
    //$scope.interval="";
    getUiValue();
    webCall();
  //webServiceCall();
    //stopLoading();
  }

  $scope.exportData = function (data) {
    //console.log(data);
    var blob = new Blob([document.getElementById(data).innerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, data+".xls");
  };
  function totalFuelTheftCalculation(fuelRunningTheft){
    $scope.totalFuelTheftBeta = 0;
    angular.forEach(fuelRunningTheft, function(val, key){ $scope.totalFuelTheftBeta +=val.fuelTheftBeta });
  }
  $scope.handleFuelTheftBeta = function (val) {
    if(val=='yes'){
      $scope.theftBeta = true;
      $scope.sort         =  sortByDate('startTime', false);
    }else{
      $scope.theftBeta = false;
      $scope.sort         =  sortByDate('dt', false);
    }

  }
  $scope.millageFilter = function () {
    if($scope.fuelMachineData.fuelRunningTheft!=null){
      if(($scope.fuelMachineData.sensorMode == 'DG'||$scope.fuelMachineData.sensorMode == 'Machinery')){
        $scope.fuelRunningTheft = $scope.fuelMachineData.fuelRunningTheft.filter(fuel => fuel.ignitionOnPer <= $scope.ignitionOnPer && fuel.ltrsphr > $scope.ltphValue && fuel.fuelTheftBeta >= $scope.minFuelTheft );
      }else{
        $scope.fuelRunningTheft = $scope.fuelMachineData.fuelRunningTheft.filter(fuel => fuel.ignitionOnPer <= $scope.ignitionOnPer && fuel.fuelMileage < $scope.millage && fuel.fuelTheftBeta >= $scope.minFuelTheft );
      }
      totalFuelTheftCalculation($scope.fuelRunningTheft);
    }
    
  }
  $scope.handleFuelFilter = function (action){
    if(action=='btnClick'){
      $scope.showFueltheftFilter = !$scope.showFueltheftFilter;
    }
    if(!$scope.showFueltheftFilter){
     $scope.minFuelTheft = "";
     $scope.customMinFuelTheftFilterValue = "";
   }else{
    if($scope.fuelMachineData.minFuelTheft=="5"||$scope.fuelMachineData.minFuelTheft=="10"||$scope.fuelMachineData.minFuelTheft=="20"||$scope.fuelMachineData.minFuelTheft=="30"||$scope.fuelMachineData.minFuelTheft=="40"||$scope.fuelMachineData.minFuelTheft=="50"){
      $scope.minFuelTheft = $scope.fuelMachineData.minFuelTheft;
    }else{
      $scope.minFuelTheft = 'others';
      $scope.customMinFuelTheftFilterValue = $scope.fuelMachineData.minFuelTheft;
    }
  }
  $scope.applyFilter();
}
$scope.applyFilter = function () {
  $scope.fuelRunningTheft = $scope.fuelMachineData.fuelRunningTheft;
  if($scope.fuelMachineData.fuelRunningTheft!=null){
    if(($scope.fuelMachineData.sensorMode == 'DG'||$scope.fuelMachineData.sensorMode == 'Machinery')){
      if($scope.ltphValue!=""){
        $scope.fuelRunningTheft = $scope.fuelMachineData.fuelRunningTheft.filter(fuel=>fuel.ltrsphr > $scope.ltphValue);
      }
      if($scope.ignitionOnPer!=""){
       $scope.fuelRunningTheft = $scope.fuelRunningTheft.filter(fuel => fuel.ignitionOnPer <= $scope.ignitionOnPer );
     }
     if($scope.minFuelTheft!="" && $scope.minFuelTheft!="others"){
       $scope.fuelRunningTheft = $scope.fuelRunningTheft.filter(fuel => fuel.fuelTheftBeta >= $scope.minFuelTheft );
     }
     if($scope.customMinFuelTheftFilterValue!=""&&$scope.minFuelTheft=="others"){
      $scope.fuelRunningTheft = $scope.fuelRunningTheft.filter(fuel => fuel.fuelTheftBeta >= $scope.customMinFuelTheftFilterValue );
    }

  }else{
    if($scope.millage==""){
      $scope.fuelRunningTheft='';
    }
    if($scope.millage!=""){
      $scope.fuelRunningTheft = $scope.fuelMachineData.fuelRunningTheft.filter(fuel=>fuel.fuelMileage < $scope.millage);
    }
    if($scope.ignitionOnPer!=""){
     $scope.fuelRunningTheft = $scope.fuelRunningTheft.filter(fuel => fuel.ignitionOnPer <= $scope.ignitionOnPer );
   }
   if($scope.minFuelTheft!="" && $scope.minFuelTheft!="others"){
     $scope.fuelRunningTheft = $scope.fuelRunningTheft.filter(fuel => fuel.fuelTheftBeta >= $scope.minFuelTheft );
   }
   if($scope.customMinFuelTheftFilterValue!=""&&$scope.minFuelTheft=="others"){
    $scope.fuelRunningTheft = $scope.fuelRunningTheft.filter(fuel => fuel.fuelTheftBeta >= $scope.customMinFuelTheftFilterValue );
  }
}
totalFuelTheftCalculation($scope.fuelRunningTheft);
}
}




$scope.exportDataCSV = function (data) {
    //console.log(data);
    CSV.begin('#'+data).download(data+'.csv').go();
  };

  $('#minus').click(function(){
    $('#menu').toggle(1000);
  });

  if(tab=="fuelDispenser"){

    $("#dtFrom").on("change paste keyup", function() {
      console.log('dtFrom')
      var changedDate = moment($(this).val(), "DD-MM-YYYY").format("YYYY-MM-DD");
      var d1 = Date.parse(changedDate);
      var d2 = "";
      if (document.getElementById("dtTo")) {
       d2 = Date.parse(moment($("#dtTo").val(), "DD-MM-YYYY").format("YYYY-MM-DD"));
       if (d1 > d2) {
         $scope.uiDate.todate       = $(this).val();
       }
     }
   });
    $("#dtTo").on("change paste keyup", function() {
     var changedDate = moment($(this).val(), "DD-MM-YYYY").format("YYYY-MM-DD");
     var d1 = Date.parse(changedDate);
     var d2 = "";
     if (document.getElementById("dtFrom")) {
       d2 = Date.parse(moment($("#dtFrom").val(), "DD-MM-YYYY").format("YYYY-MM-DD"));
       if (d1 < d2) {
        $scope.uiDate.fromdate    = $(this).val();
      }
    }
  });
  }


}]);
