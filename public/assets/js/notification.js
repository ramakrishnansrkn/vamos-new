app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global', function($scope, $http, vamoservice, $filter, GLOBAL){
	
  //global declaration
	$scope.uiDate 				=	{};
	$scope.uiValue	 			= 	{};
  $scope.sosAlarmClicked =  0;
  //$scope.sort                 = sortByDate('startTime');

  $scope.trvShow       =  localStorage.getItem('trackNovateView');

  var tab = getParameterByName('tn');
    // $scope.words=[];
    // $scope.words.push({'word':'w1','Eword':'Ew1'});
	function getParameterByName(name) {
    	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}

	//global declartion
	$scope.locations = [];
	$scope.url = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
  $scope.gName  = getParameterByName('vg');
  var oldGroupSelectionName=getParameterByName('vg'); 
  
	$scope.gIndex =0;

    $scope.trimColon = function(textVal){
		return textVal.split(":")[0].trim();
	}
	
  $("#testLoad").load("../public/menu");
	$scope.$watch("url", function (val) {
		vamoservice.getDataCall($scope.url).then(function(data) {
           
          //startLoading();
      $scope.repData=[];
      $scope.selectVehiData = [];
			$scope.vehicle_group=[];
      $scope.vehicle_list = data;
			

			if(data.length){
				$scope.vehiname	= getParameterByName('vid');
        console.log($scope.vehiname)
				$scope.uiGroup 	= $scope.trimColon(getParameterByName('vg'));
				
				angular.forEach(data, function(val, key){
                  //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
					if($scope.gName == val.group){
						$scope.gIndex = val.rowId;
            if($scope.vehiname=='undefined' || $scope.vehiname==''){
                $scope.vehiname  =  data[$scope.gIndex].vehicleLocations[0].vehicleId; 
              } 
						angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys){

							  if($scope.vehiname == value.vehicleId){
                   $scope.shortNam  = value.shortName;
                   $scope.vehicleId = value.vehicleId;
                   console.log('inside loop '+$scope.vehicleId);
                }
							    
						  })
						
					   }
			    })
           
          $scope.repData = data[$scope.gIndex].vehicleLocations;
          getAlerts($scope.repData);
          $scope.vehAarmReport();

			}
		   
		  stopLoading();
		});	
	});


$scope.vehAarmReport=function(){
  $scope.sos_list =[];  
  $scope.sosAlarmClicked = 1;
  var url='getAlarmReport?vehicleId='+$scope.vehicleId;
  startLoading();
  console.log(url);
  $http.get(url)
     .success(function (response) {
            
            stopLoading();
            alarmData  = response;
            console.log(alarmData.alarmList);
            if(alarmData.alarmList!=null){
                   $scope.sos_list      =  ($filter('filter')(alarmData.alarmList, {'alarmType':"SOS"}));
               }
      })
    .error(function (response) {
            console.log("fail");
    });    
  

}

function  getAlerts(repData){
    var alerts =getParkedIgnitionAlert(repData);
    $scope.ignition_list=alerts[0];
    $scope.parked_list=alerts[1];
    // $scope.alarm_list=alerts[2];
    $scope.notifications=alerts[3];
    $scope.idle_list = alerts[4];
    $('#notncount').text($scope.notifications);
    window.localStorage.setItem('parked_list',$scope.parked_list);
    window.localStorage.setItem('totalNotifications',$scope.notifications);
}
    
  $scope.deleteNotn = function(index,notn){
      if(notn=='parked'){
           $scope.parked_list.splice(index, 1);
           console.log($scope.parked_list);
      }else if(notn=='ign'){
           $scope.ignition_list.splice(index, 1);
      }
      else if(notn=='idle'){
           $scope.idle_list.splice(index, 1);
      }
      else if(notn=='sos'){
           $scope.sos_list.splice(index, 1);
      }
  }


  $scope.groupSelection = function(groupname, groupid){

       $scope.groupSelections=1;
       
       newGroupSelectionName=groupname;
      if(oldGroupSelectionName != newGroupSelectionName){
        oldGroupSelectionName=newGroupSelectionName;
        $scope.gIndex  = groupid;
        $scope.gName = groupname;
        $scope.url      =   GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+$scope.gName;
     }
  }

  $scope.genericFunction = function(vehid, index, shortname,position, address,group) {
       startLoading();
       $scope.vehicleId = vehid;
       $scope.vehAarmReport();
  }


}]);