app.controller('mainCtrl',['$scope','$http','vamoservice','$filter', '_global', function($scope, $http, vamoservice, $filter, GLOBAL){
  //global declaration
  $scope.videoLink="https://www.youtube.com/watch?v=LlQmx1Yus3E&list=PLu_lMbp6iY5X29NyAGTQ-soyoFryJL6m5&index=2";
  $scope.uiDate        =  {};
  $scope.uiValue       =  {};

  $scope.interval      =  "";
  $scope.fuelDataMsg   =  "";
  $scope.portNoFilter  =  false;
  var licenceExpiry="";
  var strExpired='expired';
  if(localStorage.getItem('licenceExpiry'))licenceExpiry=localStorage.getItem('licenceExpiry');
  $scope.errMsg="";
  var expiryDays="";
  var assLabel          =  localStorage.getItem('isAssetUser');
  $scope.vehiAssetView  =  true;
  $scope.trvShow        =  localStorage.getItem('trackNovateView');

  if(assLabel=="true") {
    $scope.vehiLabel = "Asset";
    $scope.vehiImage = true;
    $scope.vehiAssetView = false;
  } else if(assLabel=="false") {
    $scope.vehiLabel = "Vehicle";
    $scope.vehiImage = false;
    $scope.vehiAssetView = true;
  } else {
    $scope.vehiLabel = "Vehicle";
    $scope.vehiImage = false;
    $scope.vehiAssetView = true;
  }

  var tab = getParameterByName('tn');
  var fuelDataVals;

  function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  }

  //global declartion
  $scope.locations = [];
  $scope.url       = GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+getParameterByName('vg');
  $scope.gIndex    = 0;

  //$scope.locations01 = vamoservice.getDataCall($scope.url);
  $scope.trimColon = function(textVal) {

    if(textVal){
     var spltVal = textVal.split(":");
     return spltVal[0];
   }
 }

 function sessionValue(vid, gname,licenceExpiry){
  localStorage.setItem('user', JSON.stringify(vid+','+gname));
  localStorage.setItem('licenceExpiry', licenceExpiry);
  $("#testLoad").load("../public/menu");
}

function convert_to_24h(time_str) {
  //console.log(time_str);
  var str      = time_str.split(' ');
  var stradd   = str[0].concat(":00");
  var strAMPM  = stradd.concat(' '+str[1]);
  var time     = strAMPM.match(/(\d+):(\d+):(\d+) (\w)/);
  var hours    = Number(time[1]);
  var minutes  = Number(time[2]);
  var seconds  = Number(time[2]);
  var meridian = time[4].toLowerCase();
  
  if (meridian == 'p' && hours < 12) {
    hours = hours + 12;
  }
  else if (meridian == 'a' && hours == 12) {
    hours = hours - 12;
  }     
  var marktimestr =''+hours+':'+minutes+':'+seconds;      
  return marktimestr;
};

$scope.roundOff= function(val) {
      //var sptVar = val.split('.');
        var num = Number(val) // The Number() only visualizes the type and is not needed
        var roundedString = num.toFixed(2);
        var rounded = Number(roundedString);
        return rounded; 
      }

    // millesec to day, hours, min, sec
    $scope.msToTime = function(ms) {
      days = Math.floor(ms / (24 * 60 * 60 * 1000));
      daysms = ms % (24 * 60 * 60 * 1000);
      hours = Math.floor((ms) / (60 * 60 * 1000));
      hoursms = ms % (60 * 60 * 1000);
      minutes = Math.floor((hoursms) / (60 * 1000));
      minutesms = ms % (60 * 1000);
      seconds = Math.floor((minutesms) / 1000);
    // if(days==0)
    //  return hours +" h "+minutes+" m "+seconds+" s ";
    // else
    return hours +":"+minutes+":"+seconds;
  }

  var delayed4 = (function () {
    var queue = [];

    function processQueue() {
      if (queue.length > 0) {
        setTimeout(function () {
          queue.shift().cb();
          processQueue();
        }, queue[0].delay);
      }
    }

    return function delayed(delay, cb) {
      queue.push({ delay: delay, cb: cb });

      if (queue.length === 1) {
        processQueue();
      }
    };
  }());

  function google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent) {
    vamoservice.getDataCall(tempurlEvent).then(function(data) {
      $scope.addressEvent[index4] = data.results[0].formatted_address;
      //console.log(' address '+$scope.addressEvent[index4])
      //var t = vamo_sysservice.geocodeToserver(latEvent,lonEvent,data.results[0].formatted_address);
    })
  };

  $scope.recursiveEvent   =   function(locationEvent, indexEvent){
    var index4 = 0;
    angular.forEach(locationEvent, function(value ,primaryKey){
    //console.log(' primaryKey '+primaryKey)
    index4 = primaryKey;
    if(locationEvent[index4].address == undefined)
    {
      var latEvent     =  locationEvent[index4].latitude;
      var lonEvent     =  locationEvent[index4].longitude;
      var tempurlEvent =  "https://maps.googleapis.com/maps/api/geocode/json?latlng="+latEvent+','+lonEvent+"&sensor=true";
      delayed4(2000, function (index4) {
        return function () {
          google_api_call_Event(tempurlEvent, index4, latEvent, lonEvent);
        };
      }(index4));
    }
  })
  }


  function formatAMPM(date) {
    var date = new Date(date);
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? '0'+minutes : minutes;
      var strTime = hours + ':' + minutes + ' ' + ampm;
      return strTime;
    }

    //get the value from the ui
    function getUiValue(){
      $scope.uiDate.fromdate   = $('#dateFrom').val();
      $scope.uiDate.fromtime   = $('#timeFrom').val();
      $scope.uiDate.todate     = $('#dateTo').val();
      $scope.uiDate.totime     = $('#timeTo').val();
      if(localStorage.getItem('timeTochange')!='yes'){
       updateToTime();
       $scope.uiDate.totime    =   localStorage.getItem('toTime');
     }
     $scope.uiDate.fromtimes  = convert_to_24h($scope.uiDate.fromtime);
     $scope.uiDate.totimes    = convert_to_24h($scope.uiDate.totime);
   }
   $scope.sort = sortByDate('deviceTime',false);

   function setBtnEnable(btnName){
     $scope.yesterdayDisabled = false;
     $scope.weekDisabled = false;
     $scope.monthDisabled = false;
     $scope.todayDisabled = false;
     $scope.lastmonthDisabled = false;
     $scope.thisweekDisabled = false;

     switch(btnName){

      case 'yesterday':
      $scope.yesterdayDisabled = true;
      break;
      case 'today':
      $scope.todayDisabled = true;
      break;
      case 'thisweek':
      $scope.thisweekDisabled = true;
      break;
      case 'lastweek':
      $scope.weekDisabled = true;
      break;
      case 'month':
      $scope.monthDisabled = true;
      break;
      case 'lastmonth':
      $scope.lastmonthDisabled = true;
      break;
    }


  }
  setBtnEnable("init");
  $scope.durationFilter    =   function(duration){
  //alert('inside function');
  startLoading();
  var now = new Date();
  $scope.uiDate.todate      = getTodayDate(now.setDate(now.getDate() - 1));
  switch(duration){

    case 'yesterday':
    setBtnEnable('yesterday');
    var d = new Date();
    $scope.uiDate.fromdate= getTodayDate(d.setDate(d.getDate() - 1));
    $scope.uiDate.totime  = '11:59 PM';
      // datechange();
      break;
      case 'thisweek':
      setBtnEnable('thisweek');
      var d=new Date();
      var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1);
      var firstday= new Date(d.getFullYear(), d.getMonth(), diff);
      $scope.uiDate.fromdate      = getTodayDate(firstday.setDate(firstday.getDate() ));
      $scope.uiDate.todate= getTodayDate(new Date().setDate(new Date().getDate() ));

      $scope.uiDate.totime       = formatAMPM(d);
      //datechange();
      break;
      case 'lastweek':
      setBtnEnable('lastweek');
      var d = new Date();
      var day = d.getDay(),
      diff = d.getDate() - day + (day == 0 ? -6:1)-7;
      var diff1=d.getDate() - day + (day == 0 ? -6:1)-1;
      var firstday= new Date(d.getFullYear(), d.getMonth(), diff);
      var lastday= new Date(d.getFullYear(), d.getMonth(), diff1);
      $scope.uiDate.fromdate      = getTodayDate(firstday.setDate(firstday.getDate() ));
      $scope.uiDate.todate       = getTodayDate(lastday.setDate(lastday.getDate() ));
      $scope.uiDate.totime  = '11:59 PM';
      //datechange();
      break;
      case 'month':
      setBtnEnable('month');
      var date = new Date();
      var firstdate = new Date(date.getFullYear(), date.getMonth(), 1);
      $scope.uiDate.fromdate       = getTodayDate(firstdate.setDate(firstdate.getDate() ));
      $scope.uiDate.todate       = getTodayDate(date.setDate(date.getDate() ));

      $scope.uiDate.totime      = formatAMPM(date);
       //datechange();
       break;
       case 'today':
       setBtnEnable('today');
       var d = new Date();
       $scope.uiDate.fromdate       = getTodayDate(d.setDate(d.getDate() ));
       $scope.uiDate.todate       = getTodayDate(d.setDate(d.getDate() ));
       $scope.uiDate.totime       = formatAMPM(d);
      //datechange();
      break;
      case 'lastmonth':
      setBtnEnable('lastmonth');
      var date = new Date();
      var firstdate = new Date(date.getFullYear(), date.getMonth()-1, 1);
      var lastdate = new Date(date.getFullYear(), date.getMonth(), 0);
      $scope.uiDate.fromdate       = getTodayDate(firstdate.setDate(firstdate.getDate() ));
      $scope.uiDate.todate      = getTodayDate(lastdate.setDate(lastdate.getDate() ));

      $scope.uiDate.totime   = '11:59 PM';
      //datechange();
      break;
    }
    webCall();
  }
  function webCall(){

    if((checkXssProtection($scope.uiDate.fromdate) == true) && ((checkXssProtection($scope.uiDate.fromtime) == true) && (checkXssProtection($scope.uiDate.todate) == true) && (checkXssProtection($scope.uiDate.totime) == true))) {

       // var fuelTheftUrl  =  GLOBAL.DOMAIN_NAME+'/getFuelRawDataLog?vehicleId='+$scope.vehIds+'&fromTimeUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toTimeUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));
           // var routeDevUrl  = 'http://188.166.244.126:9000/getFuelRawDataLog?userId=naplogi&vehicleId=NAPLOGI-AP16TE0111&fromTimeUtc=1535394600000&toTimeUtc=1535480999000';

           var routeDevUrl  = GLOBAL.DOMAIN_NAME+'/getRouteDeviationData?vehicleId='+$scope.vehIds+'&fromTimeUtc='+utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime))+'&toTimeUtc='+utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime));           
           console.log(routeDevUrl);
           var expdate=moment().add(expiryDays,'days').format('DD-MM-YYYY'),
           convertedexpdate=utcFormat(expdate,convert_to_24h('11:59 PM')),
           convertedtodate=utcFormat($scope.uiDate.todate,convert_to_24h($scope.uiDate.totime)),
           convertedfromdate=utcFormat($scope.uiDate.fromdate,convert_to_24h($scope.uiDate.fromtime));
           if(convertedtodate<=convertedexpdate && convertedfromdate<=convertedexpdate){
            $http.get(routeDevUrl).success(function(data) {

              $scope.routeDevData=[];
              $scope.routeDevData = data;
              console.log( $scope.routeDevData[0] );

              stopLoading();
            });
          }else{
            stopLoading();
            $scope.errMsg=licenceExpiry;
            $scope.showErrMsg = true;
          } 
        }
      } 



  // initial method
  $scope.$watch("url", function (val) {
    vamoservice.getDataCall($scope.url).then(function(data) {
    //startLoading();
    $scope.selectVehiData = [];
    $scope.vehicle_group=[];
    $scope.vehicle_list = data;

    if(data.length) {
      $scope.vehiname = getParameterByName('vid');
      $scope.uiGroup  = $scope.trimColon(getParameterByName('vg'));
      $scope.gName  = getParameterByName('vg');
      angular.forEach(data, function(val, key) {
        //$scope.vehicle_group.push({vgName:val.group,vgId:val.rowId});
        if($scope.gName == val.group) {
          $scope.gIndex = val.rowId;

          angular.forEach(data[$scope.gIndex].vehicleLocations, function(value, keys){

            $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

            if($scope.vehiname == value.vehicleId){
              $scope.shortNam = value.shortName;
              $scope.vehIds   = value.vehicleId;
              licenceExpiry=value.licenceExpiry;
              expiryDays=value.expiryDays;
            }
          })
        }
      });

      //console.log($scope.selectVehiData);
      sessionValue($scope.vehiname, $scope.gName,licenceExpiry)
    }

    $scope.uiDate.fromdate    = localStorage.getItem('fromDate');
    $scope.uiDate.fromtime    = localStorage.getItem('fromTime');
    $scope.uiDate.todate      = localStorage.getItem('toDate');
    $scope.uiDate.totime      =  localStorage.getItem('toTime');
    if(localStorage.getItem('timeTochange')!='yes'){
      updateToTime();
      $scope.uiDate.totime    =   localStorage.getItem('toTime');
    }
    $scope.uiDate.fromtimes  =  convert_to_24h($scope.uiDate.fromtime);
    $scope.uiDate.totimes    =  convert_to_24h($scope.uiDate.totime);
    //$scope.uiDate.totime     =  '11:59 PM';
    
    startLoading();
    webCall();
    //stopLoading();
  }); 
  });


  $scope.groupSelection   = function(groupName, groupId) {
    startLoading();
    licenceExpiry="";
    $scope.errMsg="";
    $scope.gName    =  groupName;
    $scope.uiGroup  =  $scope.trimColon(groupName);
    $scope.gIndex   =  groupId;
    var url         =  GLOBAL.DOMAIN_NAME+'/getVehicleLocations?group='+groupName;

    vamoservice.getDataCall(url).then(function(response){

      $scope.vehicle_list  =  response;
      $scope.shortNam      =  response[$scope.gIndex].vehicleLocations[0].shortName;
      $scope.vehiname      =  response[$scope.gIndex].vehicleLocations[0].vehicleId;
      licenceExpiry   = response[$scope.gIndex].vehicleLocations[0].licenceExpiry;
      expiryDays   = response[$scope.gIndex].vehicleLocations[0].expiryDays;
      sessionValue($scope.vehiname, $scope.gName,licenceExpiry);
      $scope.selectVehiData = [];
    //console.log(response);
    angular.forEach(response, function(val, key){
      if($scope.gName == val.group){
          //$scope.gIndex = val.rowId;
          angular.forEach(response[$scope.gIndex].vehicleLocations, function(value, keys){

            $scope.selectVehiData.push({label:value.shortName,id:value.vehicleId});

            if($scope.vehiname == value.vehicleId) {

              $scope.shortNam = value.shortName;
              $scope.vehIds   = value.vehicleId;

            }
          });
        }
      });

    getUiValue();
    webCall();
      //stopLoading();
    });

  }

  
  $scope.genericFunction  = function (vehid, index,shortname,position, address,groupName,licenceExp) {
    startLoading();
    licenceExpiry=licenceExp;
    $scope.errMsg="";
    $scope.vehiname = vehid;
      //alert($scope.vehiname);
      console.log($scope.vehiname);

      sessionValue($scope.vehiname, $scope.gName,licenceExpiry);

      angular.forEach($scope.vehicle_list[$scope.gIndex].vehicleLocations, function(val, key){

          //console.log(val);

          if( vehid == val.vehicleId ) {

            $scope.shortNam = val.shortName;
            $scope.vehIds   = val.vehicleId;
            expiryDays   = val.expiryDays;
          }

        });

      getUiValue();
      webCall();
    }


    $scope.submitFunction   = function(){
      $scope.yesterdayDisabled = false;
      $scope.weekDisabled = false;
      $scope.monthDisabled = false;
      $scope.todayDisabled = false;
      $scope.lastmonthDisabled = false;
      $scope.thisweekDisabled = false;
      startLoading();
      getUiValue();
      webCall();
  //webServiceCall();
    //stopLoading();
  }

  $scope.exportData = function (data) {
    //console.log(data);
    var blob = new Blob([document.getElementById(data).innerHTML], {
      type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
    });
    saveAs(blob, data+".xls");
  };

  $scope.exportDataCSV = function (data) {
    //console.log(data);
    CSV.begin('#'+data).download(data+'.csv').go();
  };

  $('#minus').click(function(){
    $('#menu').toggle(1000);
  });


}]);
